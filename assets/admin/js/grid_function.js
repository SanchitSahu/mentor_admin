$(document).ready(function() {
    $("#checkall").click(function() {
        var selected = new Array();
        $(dTable.fnGetNodes()).find(':checkbox').each(function() {
            $this = $(this);
            if ($('#checkall').is(':checked')) {
                $this.attr('checked', 'checked');
                selected.push($this.val());
            } else {
                $this.removeAttr('checked');
                selected.pop($this.val());
            }
        });
        // convert to a string
        var mystring = selected.join();
        //alert(mystring);
    });
});

function activeInactiveAll(status) {
    var selected = new Array();
    $(dTable.fnGetNodes()).find(':checkbox').each(function() {
        $this = $(this);
        if ($(this).is(':checked')) {
            selected.push($this.val());
        }
    });
    var ids = selected.join();
    if (ids != '') {
        $.ajax({
            type: "POST",
            url: activeAjaxSource,
            data: "id=" + ids + "&status=" + status + "&mode=all",
            success: function(data) {
                statusMessage();
                dTable.fnDraw(true);
                $('#checkall').prop('checked', false);
            }
        });
    } else {
        alert('Please select at least one record.');
    }
}

function deleteAll() {
    var selected = new Array();
    $(dTable.fnGetNodes()).find(':checkbox').each(function() {
        $this = $(this);
        if ($(this).is(':checked')) {
            selected.push($this.val());
        }
    });
    var ids = selected.join();
    if (ids != '') {
        if (confirm("Are you sure you want to delete?")) {
            $.ajax({
                type: "POST",
                url: deleteAjaxSource,
                data: "id=" + ids + "&mode=all",
                success: function(data) {
                    if (data == 1) {
                        deleteMessage();
                        $('#checkall').prop('checked', false);
                    }
                    dTable.fnDraw(true);
                }
            });
        }
        dTable.fnDraw();
    } else {
        alert('Please select at least one record.');
    }
}
function deleterecord(id) {

    if (confirm("Are you sure you want to delete?")) {
        $.ajax({
            type: "POST",
            url: deleteAjaxSource,
            data: "id=" + id,
            success: function(data) {
                if (data == 1) {
                    dTable.fnDraw(true);
                    deleteMessage();
                    $('#checkall').prop('checked', false);
                }
            }
        });
    }
}

function activerecord(id, status) {
    $.ajax({
        type: "POST",
        url: activeAjaxSource,
        data: "id=" + id + "&status=" + status,
        success: function(data) {
            if (data == 1) {
                dTable.fnDraw(true);
                if (status == '2') {
                    deleteMessage();
                } else {
                    statusMessage();
                }
                $('#checkall').prop('checked', false);
            }
        }
    });
}

function deleteMessage() {
    var delmsg = '<div class="alert alert-success">';
    delmsg += '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
    delmsg += 'Data has been deleted successfully.';
    delmsg += '</div>';
    $("div.alert-success").remove();
    $("div.panel-info").before(delmsg);
}

function statusMessage() {
    var delmsg = '<div class="alert alert-success">';
    delmsg += '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
    delmsg += 'Status has been changed successfully.';
    delmsg += '</div>';
    $("div.alert-success").remove();
    $("div.panel-info").before(delmsg);
}

function verifyrecord(id) {
    $.ajax({
        type: "POST",
        url: verifyAjaxSource,
        data: "id=" + id,
        success: function(data) {
            if (data == 1) {
                dTable.fnDraw(true);
                verifyMessage();
                $('#checkall').prop('checked', false);
            }
        }
    });
}

function verifyMessage() {
    var delmsg = '<div class="alert alert-success">';
    delmsg += '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
    delmsg += 'Verification has been completed successfully.';
    delmsg += '</div>';
    $("div.alert-success").remove();
    $("div.panel-info").before(delmsg);
}