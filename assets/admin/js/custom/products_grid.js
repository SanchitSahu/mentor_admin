$(document).ready(function() {
    dTable = $('#productsTable').dataTable({
        responsive: false,
        bJQueryUI: false,
        bProcessing: true,
        bServerSide: true,
        bFilter: true,
        //multipleSelection: true,
        iDisplayLength: 10,
        sAjaxSource: AjaxSource,
        aoColumns: [
            {"sName": "Product"},
            {"sName": "Category"},
            {"sName": "ProductCategory"},
            {"sName": "ShowOnHomePage", "bSearchable": false},
            {"sName": "Price"},
            {"sName": "Status"},
            {"sName": "Id", "bSearchable": false, "bSortable": false}
        ],
        aoColumnDefs: [
            {
                "mRender": function(data, type, row) {
                   return '<a href="'+addEditSource+'/'+row[6]+'" class="" title="Edit">Edit</a>';
                },
                "aTargets": [6]
            }
        ],
       // aaSorting: [[7, 'desc']],
        sPaginationType: "full_numbers"});

        $("input").addClass("form-control");
        $("select").addClass("form-control");

   /* 
    $('#productsTable').dataTable().columnFilter({ 	
        //sPlaceHolder: "head:after",
        aoColumns: [
                { type: "text", sSelector: "#product" },
                { type: "select", sSelector: "#category" },
                { type: "select", sSelector: "#productCategory" },
                null,
                null,
                { type: "select", sSelector: "#price"},
                { type: "select", sSelector: "#status", values: ['Active', 'Inactive'] }
            ]
    });

    $("input").addClass("form-control");
    $("select").addClass("form-control");
    */
//    $("form.form-horizontal .form-control").css("width", "110%");


});