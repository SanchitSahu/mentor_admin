$(document).ready(function() {
    dTable = $('#customerTable').dataTable({
        responsive: false,
        bJQueryUI: false,
        bProcessing: true,
        bServerSide: true,
        bFilter: true,
        //multipleSelection: true,
        iDisplayLength: 10,
        sAjaxSource: AjaxSource,
        aoColumns: [
            {"sName": "CustomerName"},
            {"sName": "Email"},
            {"sName": "Gender"},
            {"sName": "CellPhone"},
            {"sName": "Id", "bSearchable": false, "bSortable": false}
        ],
        aoColumnDefs: [
            {
                "mRender": function(data, type, row) {
                   return '<a href="'+addEditSource+'/'+row[4]+'" style="cursor:pointer;"><i class="livicon" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="I17" style="width: 16px; height: 16px;"></i></a>';
                },
                "aTargets": [4]
            }
        ],
       // aaSorting: [[7, 'desc']],
        sPaginationType: "full_numbers"});

        $("input").addClass("form-control");
        $("select").addClass("form-control");

   /* 
    $('#productsTable').dataTable().columnFilter({ 	
        //sPlaceHolder: "head:after",
        aoColumns: [
                { type: "text", sSelector: "#product" },
                { type: "select", sSelector: "#category" },
                { type: "select", sSelector: "#productCategory" },
                null,
                null,
                { type: "select", sSelector: "#price"},
                { type: "select", sSelector: "#status", values: ['Active', 'Inactive'] }
            ]
    });

    $("input").addClass("form-control");
    $("select").addClass("form-control");
    */
//    $("form.form-horizontal .form-control").css("width", "110%");


});