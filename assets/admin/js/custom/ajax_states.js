$(document).ready(function() {
    $("#iCountryId").change(function() {
        
        var countryId = $("#iCountryId").val();
        
        if (countryId != 0) {
            $("#state_loader").show();
            $.ajax({
                type: 'POST',
                url: stateSource,
                dataType: "html",
                data: { countryId: countryId },
                success: function(states) {
                    $("#iStateId").empty();
                    $('#iStateId').html(states);
                   // $("#state_loader").hide();
//                    $.each(states, function(id, state) {
//                        var opt = $('<option />');
//                        opt.val(id);
//                        opt.text(state);
//                    });
                }
            });
        } else {
            $("#iStateId").empty();
        }
    });
});