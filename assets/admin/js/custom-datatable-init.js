var oTable;

$(document).ready(function() {
    //var disp_field = $("#"+dattTableName+" tr th").eq(0).text().toLowerCase().trim();
    var disp_fieldIndex = 0;
    $("#"+dattTableName+" tr th").each(function(index) {
		if ($(this).text().toLowerCase().trim() == 'display order') {
			disp_fieldIndex = index;
		}
	});

    oTable = $('#'+dattTableName).dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": sAjaxSource,
                "bJQueryUI": true,
                "oLanguage": {
            "sProcessing": "<img src='"+baseUrl+"assets/admin/images/ajax-loader_dark.gif'>"
        },      
        "fnInitComplete": function() {
                //oTable.fnAdjustColumnSizing();
         },
         //"aaSorting": [[ disp_fieldIndex, "asc" ],[ 0, "asc" ]],
         "aaSorting": (disp_fieldIndex != '') ? [[ disp_fieldIndex, "asc" ],[ 0, "asc" ]] : [[ 0, "asc" ]],
         //"order": [[ 1, "desc" ]],
        'fnServerData': function(sSource, aoData, fnCallback)
            {
              $.ajax
              ({
                'dataType': 'json',
                'type'    : 'POST',
                'url'     : sSource,
                'data'    : aoData,
                'success' : fnCallback
              });
            }
    } );
    //console.log(oTable);
});


$(document).ready(
    function(){
        $('#submit').attr('disabled',true);
        $('input:file').change(function(){
            if ($(this).val()){
                $('#submit').removeAttr('disabled'); 
            } else {
                $('#submit').attr('disabled',true);
            }
        });
    });