<?php

class ClientSource extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/clientSource_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // Mentee Source LISTING 
    //=========================================
    public function index() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );

        $res['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Source Name', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/clientsource/list', $res);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $this->datatables->select("MenteeSourceID, MenteeSourceName, Weight", FALSE)
                ->unset_column('MenteeSourceID')
                ->unset_column('Actions')
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/clientSource'), 'MenteeSourceID')
                ->from('menteesourcenames')
                ->where('Status', '1');

        echo $this->datatables->generate();
    }

    //=========================================
    // Mentee Source ADD 
    //=========================================
    public function add($id = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('MenteeSourceName', 'Source Name', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {

            $data = array(
                'MenteeSourceName' => $this->input->post('MenteeSourceName'),
                'Weight' => $this->input->post('Weight')
            );
            if ($id == 0) {
                $result = $this->db->insert('menteesourcenames', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', 'Client Source Added Successfully');
            } else {
                $this->db->where('MenteeSourceID', $id);
                $result = $this->db->update('menteesourcenames', $data);
                $this->session->set_flashdata('success', 'Client Source Updated Successfully');
            }
            redirect('admin/clientSource');
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('menteesourcenames');
        $userSource = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $userSource[$field] = '';
            }
        }

        if ($id) {
            $userSource = $this->clientSource_model->get_source($id);
        }
        $data['id'] = $id;
        $data['clientSource'] = $userSource;

        $this->template->content->view('admin/clientsource/add', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // Mentee Source EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    //=========================================
    // Mentee Source DELETE 
    //=========================================
    public function delete($id) {
        $ds = $this->clientSource_model->delete_source($id);
        $this->session->set_flashdata('success', 'Client Source Delete Successfully');
        redirect('admin/clientSource');
    }

    public function checkMenteeSourceExist() {
        if ($this->input->is_ajax_request()) {
            $MenteeSourceName = $this->input->post('MenteeSourceName');
            $MenteeSourceID = $this->input->post('MenteeSourceID');
            if ($MenteeSourceID == "") {
                $flag = $this->clientSource_model->checkMenteeSource($MenteeSourceName);
            } else {
                $flag = $this->clientSource_model->checkMenteeSourceEdit($MenteeSourceName, $MenteeSourceID);
            }
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

}
