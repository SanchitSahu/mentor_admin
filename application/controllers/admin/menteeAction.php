<?php

class MenteeAction extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/menteeaction_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // Mentee Defined Action LISTING 
    //=========================================
    public function index() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );

        $res['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Action Name', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/menteeAction/list', $res);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $this->datatables->select("MenteeActionID, MenteeActionName, Weight", FALSE)
                ->unset_column('MenteeActionID')
                ->unset_column('Actions')
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/menteeAction'), 'MenteeActionID')
                ->from('menteedefinedactions')
                ->where('Status !=', '0');

        echo $this->datatables->generate();
    }

    //=========================================
    // Mentee Defined Action ADD 
    //=========================================
    public function add($id = '') {
        $data['config'] = $this->settings_model->getConfig();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('MenteeActionName', $data['config'][0]->menteeName . ' Action Name', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {

            $data = array(
                'MenteeActionName' => $this->input->post('MenteeActionName'),
                'Weight' => $this->input->post('Weight')
            );
            if ($id == 0) {
                $result = $this->db->insert('menteedefinedactions', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', $data['config'][0]->menteeName . 'Action Added Successfully');
            } else {
                $this->db->where('MenteeActionID', $id);
                $result = $this->db->update('menteedefinedactions', $data);
                $this->session->set_flashdata('success', $data['config'][0]->menteeName . ' Action Updated Successfully');
            }
            redirect('admin/menteeAction');
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('menteedefinedactions');
        $menteedefinedactions = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $menteedefinedactions[$field] = '';
            }
        }

        if ($id) {
            $menteedefinedactions = $this->menteeaction_model->get_menteedefinedactions($id);
        }
        $data['id'] = $id;
        $data['menteedefinedactions'] = $menteedefinedactions;

        $this->template->content->view('admin/menteeAction/add', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // Mentee Defined Action EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    public function checkMenteeActionExist() {
        if ($this->input->is_ajax_request()) {
            $MenteeActionName = $this->input->post('MenteeActionName');
            $MenteeActionID = $this->input->post('MenteeActionID');
            if ($MenteeActionID == "") {
                $flag = $this->menteeaction_model->checkMenteeAction($MenteeActionName);
            } else {
                $flag = $this->menteeaction_model->checkMenteeActionEdit($MenteeActionName, $MenteeActionID);
            }
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    //=========================================
    // Mentee Defined Action DELETE 
    //=========================================
    public function delete($id) {
        $res['config'] = $this->settings_model->getConfig();
        $ds = $this->menteeaction_model->delete_menteedefinedactions($id);
        $this->session->set_flashdata('success', $data['config'][0]->menteeName . ' Action Delete Successfully');
        redirect('admin/menteeAction');
    }

}
