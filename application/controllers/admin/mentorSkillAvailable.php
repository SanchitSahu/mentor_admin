<?php

//error_reporting(E_ALL);
//ini_set("display_errors", "ON");

class MentorSkillAvailable extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/clientskillavailable_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        //print_r($res);exit;
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            //print_r($x);exit;
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // SKILL LISTING 
    //=========================================
    public function index() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );

        $res['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->mentorName . ' Name', 'Skill Name', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/clientSkillAvailable/list', $res);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
		if($this->input->post('sSearch') != ""){
			$this->datatables->select("mm.MentorName,mss.MentorSkillSetID,mss.MentorID,ss.SkillName, mss.Weight", FALSE)
                ->unset_column('mss.MentorSkillSetID')
                ->unset_column('mss.MentorID')
                ->unset_column('Actions')
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/mentorSkillAvailable'), 'mss.MentorID')
                ->from('mentor mm')
				->join('mentorskillset mss', 'mm.MentorID = mss.MentorID', 'left')
                ->join('skill ss', 'ss.SkillID = mss.SkillID', 'left')
                ->group_by('mss.MentorID')
                ->where('mss.Status', '1');
			echo $this->datatables->generate();
		}else{
			$this->datatables->select("mm.MentorName,mss.MentorSkillSetID,mss.MentorID,GROUP_CONCAT(' ', ss.SkillName), mss.Weight", FALSE)
                ->unset_column('mss.MentorSkillSetID')
                ->unset_column('mss.MentorID')
                ->unset_column('Actions')
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/mentorSkillAvailable'), 'mss.MentorID')
                ->from('mentor mm')
				->join('mentorskillset mss', 'mm.MentorID = mss.MentorID', 'left')
                ->join('skill ss', 'ss.SkillID = mss.SkillID', 'left')
                ->group_by('mss.MentorID')
                ->where('mss.Status', '1');
			echo $this->datatables->generate();
		}
    }

    //=========================================
    // SKILL ADD 
    //=========================================
    public function add($id = '') {
        $this->load->library('form_validation');
        // $this->form_validation->set_rules('MenteeProgrammeName', 'Programme Name', 'required');
        //$this->form_validation->set_rules('Weight', 'Weight', '');
        // if ($this->form_validation->run() === TRUE) {
	
        if ($this->input->post()) {
			$skill_array = array();
			$mentor = $this->input->post('mentor');
			$skill_array = $this->input->post('skills');
			if ($mentor != "0") {	
            if (!empty($skill_array)) {
                if ($id == 0) {
                    $id = $this->input->post('mentor');
                    $this->db->where('MentorID', $id);
                    $this->db->delete('mentorskillset');


                    foreach ($skill_array as $skill) {
                        $data = array("MentorID" => $id, "SkillID" => $skill, "Status" => 1, "Weight" => 1000);
                        $this->db->where('MentorID', $id);
                        $result = $this->db->insert('mentorskillset', $data);
                    }
                    $this->session->set_flashdata('success', 'Skill Added Successfully');
                } else {

                    $this->db->where('MentorID', $id);
                    $this->db->delete('mentorskillset');


                    foreach ($skill_array as $skill) {
                        $data = array("MentorID" => $id, "SkillID" => $skill, "Status" => 1, "Weight" => 1000);
                        $this->db->where('MentorID', $id);
                        $result = $this->db->insert('mentorskillset', $data);
                    }
                    $this->session->set_flashdata('success', 'Skill Updated Successfully');
                }
                redirect('admin/mentorSkillAvailable');
            } else {
                $this->session->set_flashdata('error', 'No Skill Selected');
                if($id != "")
					redirect('admin/mentorSkillAvailable/edit/'.$id);
				else
					redirect('admin/mentorSkillAvailable/add');
            }
			} else {
				$res['config'] = $this->settings_model->getConfig();
                $this->session->set_flashdata('error', 'No'.' '.$res['config'][0]->mentorName.' '.'Selected');
                if($id != "")
					redirect('admin/mentorSkillAvailable/edit/'.$id);
				else
					redirect('admin/mentorSkillAvailable/add');
            }
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('menteeprogrammes');
        $userSource = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $userSource[$field] = '';
            }
        }
        $client_skills = "";
        if ($id) {
            $client_skills = $this->clientskillavailable_model->get_clientSkills($id);
        }
        $data['id'] = $id;
        $data['clientskillavailable'] = $client_skills;
        $data['skills'] = $this->clientskillavailable_model->get_Skills();
        $data['mentor'] = $this->clientskillavailable_model->get_Mentor();
        $this->template->content->view('admin/clientSkillAvailable/add', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // user Roles EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    //=========================================
    // user Roles DELETE 
    //=========================================
    public function delete($id) {
        $ds = $this->clientskillavailable_model->delete_client_skill($id);
        $this->session->set_flashdata('success', 'Client Skill Delete Successfully');
        redirect('admin/mentorSkillAvailable');
    }
	
	public function ajaxGetMentorSkills(){
		$id = $this->input->post('id');
		$array = array();
		$this->db->select("SkillID");
		$this->db->where("MentorID",$id);
		$this->db->where("Status","1");
		$query = $this->db->get('mentorskillset');
		$result = $query->result_array();
		foreach($result as $res){
			$array[] = $res['SkillID'];
		}
		$skills = implode(",",$array);
		echo $skills;
	}

}
