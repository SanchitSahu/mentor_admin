<?php

class Index extends Admin_Controller {

    function __construct() {
        parent::__construct();
		$res	=	loadDatabase();
		if($res != 0){
			$x	=	$this->load->database($res,TRUE);
			$this->db	=	$x;
			//$query = $this->db->select('MentorName')->get('mentor');
			//$res	=	$query->result();print_r($res);
			
		}else{
			redirect("400.shtml");exit;
		}
		//	echo $db_name = $this->db->database;
		if(checkDomain()){//echo "index file";
		}
		$this->load->model('superadmin/dashboard_model');
    }

	//check user validation
    function index() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'Email',
                'label' => 'Email',
                'rules' => 'required|valid_email'
            ),
            array(
                'field' => 'Password',
                'label' => 'Password',
                'rules' => 'required'
            )
        );
		
		if(isset($_GET['callFromApp']) == true) {
			$_POST = $_GET;
			$this->load->model('admin/Login_model', 'user');
			if($this->user->subadminlogin(true)) {
				redirect("admin/$_GET[Page]/?callFromApp=$_GET[callFromApp]&loggedInMentor=$_GET[loggedInMentor]");
			} else {
				$this->load->view('includes/header_login');
				$this->load->view('admin/index');
				$this->load->view('includes/footer_login');
				exit;
			}
			
		}
		$this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
			$userSession = $this->session->userdata('admin_data');

            $this->load->view('includes/header_login');
            $this->load->view('admin/index');
            $this->load->view('includes/footer_login');
        } else {
            $this->load->model('admin/Login_model', 'user');
            if ($this->user->login()) {
                redirect('superadmin/dashboard');
            }else if($this->user->subadminlogin()) {
                redirect('admin/dashboard');
            }else {
                $data['error_message'] = "Please enter valid email address and password";
                $this->load->view('includes/header_login');
                $this->load->view('admin/index', $data);
                $this->load->view('includes/footer_login');
            }
        }
    }

	//user logout
    public function logout() {
        $this->session->sess_destroy();
        redirect('admin');
    }
	
	public function logout_admin() {
		$superAdminData = $this->session->userdata('superadmin');	
		
		$superAdminData['returnFromAdmin']	=	"true";
		//print_r($superAdminData);exit;
		$this->session->unset_userdata('superadmin');
		$this->session->set_userdata('admin_data', $superAdminData);
		$url	=	"http://melstm.net/mentor_staging/superadmin/dashboard";
		redirect($url);
		die();
    }
	
	function verifyRegistration()
	{
		$data['verificationCode']	=	$this->uri->segment(4);
		$data['email']	=	$this->uri->segment(5);
		$res	=	$this->dashboard_model->checkVerificationCode($data['verificationCode'],$data['email']);
		if($res == 1){ 
			$this->load->view('superadmin/updatePassword',$data);	
		}else 
			echo "Error occurred while verifying the registration. Please try again";
	}
	
	public function updatePassword()
	{
		//print_r($_REQUEST);exit;
        $deleteRes	=	$this->dashboard_model->deleteVerificationCode($_REQUEST['verificationCode'],$_REQUEST['email'],$_REQUEST['password']);
		if($deleteRes == 1){
			echo "Registration Verified";
		}else{
			echo "Error occurred while verifying the registration. Please try again";
		}
		
	}

	//change user password
    function userChangePassword(){
        
        $this->load->view('includes/header_common');
        $this->load->view('admin/banner/add',$data);
        $this->load->view('includes/footer_common');
    }
	
	public function forgotpassword() {
		$this->load->model('admin/employee_model');
        if ($this->input->post()) {
			$host = $_SERVER['HTTP_HOST'];
			$subdomain = strstr(str_replace("www.","",$host), ".",true);
			$email = $this->input->post('email');
			
			$id	=	$this->employee_model->getIdByHost($subdomain,$email);
			//print_r($id);exit;
			if(empty($id)){
				echo "This Email Id is not registered with this school";
			}else{
				if($subdomain == "melstm"){
					$mainId	=	$id['AdminID'];
					$userDetails = $this->employee_model->get_employee($mainId);
				}else{
					$mainId	=	$id['subdomain_id'];
					$userDetails = $this->employee_model->get_employeeAdmin($mainId);
				}
				//echo "here";print_r($userDetails);exit;
				
				$this->load->model('admin/emailtemplate_model');
				if (count($userDetails) > 0) {

					$emailtemplate = $this->emailtemplate_model->get_emailtempate_details(3);
					//print_r($emailtemplate);exit;
					if ($emailtemplate) {
						$emailData = array();
						$emailData['toEmail'] = $userDetails['Email'];
						$resetLink = '<a href="'. base_url() .'admin/index/resetPassword/'.  $mainId.'">Reset Password</a>';
						$arrSearch = array('##FIRST_NAME##', "##EMAIL##", "##PASSWORD##","##RESETLINK##", "##SCHOOLNAME##");
						if($userDetails['FirstName'] == "" &&  $userDetails['LastName'] == "")
								$userName = "User";
						else	
								$userName	=	$userDetails['FirstName'] . ' ' . $userDetails['LastName'];
						$arrReplace = array(ucwords($userName),$userDetails['Email'] , fnDecrypt($userDetails['Password'], $this->config->item('mentorKey')),$resetLink,ucfirst($userDetails['subdomain_name']));

						$subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
						$emailData['subject'] = $subject;

						$body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
						$emailData['body'] = $body;

						$emailData['fromEmail'] = 'noreply@melstm.net';

                        send_mail($emailData);
						echo "mail-sent";
						die;
					}
				}else{
					echo "There is some problem in sending email. Please try again later!";
				}
			}
			
        }
    }
	
	public function resetPassword($id){
		$data['id']	=	$id;
		$this->load->view('admin/updatePassword',$data);
		
	}
	
	public function updateMentorpassword() {
		$this->load->model('admin/employee_model');
        if ($this->input->post()) {
			$host = $_SERVER['HTTP_HOST'];
			$subdomain = strstr(str_replace("www.","",$host), ".",true);
			$id = $this->input->post('mainId');
			$password = $this->input->post('password');
			$password = fnEncrypt($password, $this->config->item('mentorKey'));
			
			//print_r($id);exit;
			if(empty($id)){
				echo "Something went wrong try again...(__FILE__@__LINE__ in __FUNCTION__)";
			}else{		
				if($subdomain == 'melstm'){
					$data = $this->employee_model->resetpassword($id,$password,'melstm');
				}else{
					$userDetails = $this->employee_model->get_employeeAdmin($id);
					$data = $this->employee_model->resetpassword($id,$password,$userDetails['subdomain_name']);
					//echo "here";print_r($userDetails);exit;
				}
				
				
				$this->load->model('admin/emailtemplate_model');
				if ($data== true) {
					echo "Password reset successfully";
				}else{
					echo "There is some problem in sending email. Please try again later!";
				}
			}
			
        }
    }
}
