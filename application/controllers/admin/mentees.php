<?php

class Mentees extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/mentees_model');
		$this->load->model('admin/mentors_model');
		$this->load->model('admin/emailtemplate_model');
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if(!loginCheck()){redirect('admin');}
		$res	=	loadDatabase();
		//print_r($res);exit;
		if($res != 0){
			$x	=	$this->load->database($res,TRUE);
			//print_r($x);exit;
			$this->db	=	$x;	
		}else{
			redirect("400.shtml");exit;
		}
		
    }
    
    //=========================================
    // MENTEES LISTING  
    //=========================================
    public function index() {
		$res['config']	=	$this->settings_model->getConfig();
		$res['error']	=	"";
         $tmpl = array ( 
            'table_open'  => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open'  => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">' 
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->menteeName.' Name',$res['config'][0]->menteeName.' Phone',$res['config'][0]->menteeName.' Email',$res['config'][0]->menteeName.' Programme',$res['config'][0]->menteeName.' Source','Display Order','Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        
        $this->template->content->view('admin/mentees/list',$res);
        $this->template->publish_admin();
    }
    
    //=========================================
    //function to handle callbacks
    //=========================================
    function datatable()
    {
        $this->datatables->select('mci.MenteeContactID,m.MenteeName,mci.MenteePhone,mci.MenteeEmail,p.ProgrammeName,ms.MenteeSourceName,mci.Weight')
        ->unset_column('mci.MenteeContactID')
        ->unset_column('Actions')
        //->add_column('Actions', get_buttons('$1','admin/mentees'),'mci.MenteeContactID')
        ->add_column('Actions', get_Edit_DeleteMentees_Buttons('$1','admin/mentees'),'mci.MenteeContactID')
        ->from('menteecontact mci')
        ->join('mentee m', 'm.MenteeID = mci.MenteeId','left')
		->join('menteetoprogramme mp', 'mp.MenteeID = mci.MenteeID','left')
		->join('menteeprogrammes p', 'mp.ProgrammeID = p.ProgrammeID','left')
		->join('menteetosource us', 'us.MenteeID = mci.MenteeID','left')
		->join('menteesourcenames ms', 'us.MenteeSourceID = ms.MenteeSourceID','left')	
        ->where('m.Status','1')
		->group_by('m.MenteeID');
        
        echo $this->datatables->generate();
    }

    //=========================================
    // MENTEES ADD  
    //=========================================
    public function add($id = '') {  
		$data['config']	=	$this->settings_model->getConfig();
        $this->form_validation->set_rules('MenteeName', $data['config'][0]->menteeName.' Name', 'required');
        //$this->form_validation->set_rules('MenteePhone', $data['config'][0]->menteeName.' Phone', 'required');
        $this->form_validation->set_rules('MenteeEmail', $data['config'][0]->menteeName.' Email', 'required');
		//$this->form_validation->set_rules('programmeID', $data['config'][0]->mentorName.' Programme', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {
		$getDeparment	=	$this->mentors_model->getDeparment();

            if ($id == 0) {
            	$menteeName  = $this->input->post('MenteeName');
            	if(isset($menteeName) && $menteeName != ""){
            		$data2 = array(
						'MenteeName'  => $this->input->post('MenteeName'),
						'Weight' => $this->input->post('Weight')
					);
            	}
				$nameresult = $this->db->insert('mentee', $data2);
				
				$filename	=	"";
				if ($nameresult) {
					if ($_FILES['photo']['error'] == 0) {
						$pathMain = $this->config->item('base_images');
						$pathThumb = $this->config->item('base_images');
						$imageNameTime = time();
						// $imageNamePrefix = "tmp_";
						//$orgFileName =  $imageNamePrefix.$imageNameTime; 
						$path = $_SERVER['DOCUMENT_ROOT'] . "/mentor/assets/admin/images/admin/" . $imageNameTime. ".png";
						$filename = $imageNameTime . ".png";
						if (move_uploaded_file($_FILES["photo"]["tmp_name"], $path)) {
							$status = 1;
						} else {
							$status = 0;
						}
						
						@unlink($pathMain . $uploadedFileName);
					}
				}
				
				$menteeid = $this->db->insert_id();
				#$AccessToken = randomString(6);
				$AccessToken = mt_rand(100000,999999);
				$contactdata = array(
					'MenteeId'	=> $menteeid,
					'MenteePhone'  	=> $this->input->post('MenteePhone'),
					'MenteeEmail'   => $this->input->post('MenteeEmail'),
					'Password'	=> fnEncrypt($AccessToken, $this->config->item('mentorKey')),
					'Weight'  => $this->input->post('Weight'),
					'ExpiryDate'	=> date("Y-m-d",strtotime(date("Y-m-d")."+7 day")),
					'DisableSMS'  => $this->input->post('MenteePhoneSMS')
					//'MenteeImage'	=> $filename
				);
				if($filename != '') {
                    $contactdata['MenteeImage'] = $filename;
                }
                $result = $this->db->insert('menteecontact', $contactdata);
                $id = $this->db->insert_id();
				
				
				$newRegister = array(
                    'UserID' => $menteeid,
                    'UserType' => 1,
                    'DateRegistered' => date('Y-m-d H:i:s'),
                    'DateUpdated' => date('Y-m-d H:i:s'),
                    'UserName' => $menteeName,
                    'UserEmail' => $this->input->post('MenteeEmail')
                );
                $this->db->insert('newregistrations',$newRegister);
				
				
				$menteetoprogramme = array(
					'MenteeID'  	=> $menteeid,
					'ProgrammeID'   => $this->input->post('programmeID')
				);
				$result = $this->db->insert('menteetoprogramme', $menteetoprogramme);
				
				$menteetosource = array(
						'MenteeID'  	=> $menteeid,
						'MenteeSourceID'   => $this->input->post('MenteeSourceID')
					);
					$result = $this->db->insert('menteetosource', $menteetosource);	
					
				// SEND EMAIL TO MENTEE
				$emailtemplate =  $this->emailtemplate_model->get_emailtempate_details(1);
				$host = $_SERVER['HTTP_HOST'];
				$subdomain = strstr(str_replace("www.","",$host), ".",true);
				if($emailtemplate){
					$emailData = array();
					$emailData['toEmail'] = $this->input->post('MenteeEmail');
					
					$arrSearch = array('##FIRST_NAME##', '##EMAIL_ADDRESS##','##ACCESS_TOKEN##', '##USER_TYPE##',"##SCHOOLNAME##","##DEPARTMENTNAME##","##MENTOR##","##MENTEE##");
					$arrReplace = array($this->input->post('MenteeName'),$this->input->post('MenteeEmail'),$AccessToken,'Mentee',$subdomain,$getDeparment['DepartmentName'],$data['config'][0]->mentorName,$data['config'][0]->menteeName);
					
					$subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
					$emailData['subject'] = $subject;
					
					$body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
					$emailData['body'] = $body;
					
					send_mail($emailData);
				}
                // END SEND EMAIL TO MENTEE
            	$this->session->set_flashdata('success', $data['config'][0]->menteeName.' Added Successfully');
            }else{
            	$menteeName  = $this->input->post('MenteeName');
            	$menteeID  = $this->input->post('MenteeID');
            	if(isset($menteeName) && $menteeName != ""){
            		$data2 = array(
		        	'MenteeName'  => $menteeName,
					'Weight'  => $this->input->post('Weight')
	        	);
            	}
            	$this->db->where('MenteeID', $menteeID);
                $nameresult = $this->db->update('mentee', $data2);
				
				$filename = "";
				if ($nameresult) {
					if ($_FILES['photo']['error'] == 0) {
						$pathMain = $this->config->item('base_images');
						$pathThumb = $this->config->item('base_images');
						$imageNameTime = time();
						// $imageNamePrefix = "tmp_";
						//$orgFileName =  $imageNamePrefix.$imageNameTime; 
						$path = $_SERVER['DOCUMENT_ROOT'] . "/mentor/assets/admin/images/admin/" . $imageNameTime. ".png";
						$filename = $imageNameTime . ".png";
						if (move_uploaded_file($_FILES["photo"]["tmp_name"], $path)) {
							$status = 1;
						} else {
							$status = 0;
						}
						
						@unlink($pathMain . $uploadedFileName);
					}
				}
                
            	$contactdata = array(
					'MenteeID'	=> $menteeID,
					'MenteePhone'  	=> $this->input->post('MenteePhone'),
					//'Weight'  => $this->input->post('Weight'),
					'MenteeEmail'   => $this->input->post('MenteeEmail'),
					'DisableSMS'  => $this->input->post('MenteePhoneSMS')
				);
				if($filename != '') {
                    $contactdata['MenteeImage'] = $filename;
                }
                $this->db->where('MenteeID', $id);
                $result = $this->db->update('menteecontact', $contactdata);
                
                $sql = $this->db->get_where('menteecontact', array('MenteeEmail' => $this->input->post('MenteeEmail')));
                $menteeData = $sql->row_array();
                
				$isProgramme = $this->mentees_model->get_menteeProgramme($menteeID);
				if(empty($isProgramme)){
					$menteetoprogramme = array(
						'MenteeID'  	=> $this->input->post('MenteeID'),
						'ProgrammeID'   => $this->input->post('programmeID')
					);
					$result = $this->db->insert('menteetoprogramme', $menteetoprogramme);
				}else{
					$menteetoprogramme = array(
						'MenteeID'  	=> $this->input->post('MenteeID'),
						'ProgrammeID'   => $this->input->post('programmeID')
					);
					$this->db->where('MenteeToProgramme', $isProgramme['MenteeToProgramme']);
					$result = $this->db->update('menteetoprogramme', $menteetoprogramme);
						
				}
				
				$isSource = $this->mentees_model->get_menteesSource($menteeID);
				if(empty($isSource)){
					$menteetosource = array(
						'MenteeID'  	=> $this->input->post('MenteeID'),
						'MenteeSourceID'   => $this->input->post('MenteeSourceID')
					);
					$result = $this->db->insert('menteetosource', $menteetosource);
				}else{
					$menteetosource = array(
						'MenteeID'  	=> $this->input->post('MenteeID'),
						'MenteeSourceID'   => $this->input->post('MenteeSourceID')
					);
					$this->db->where('menteeToSourceID', $isSource['menteeToSourceID']);
					$result = $this->db->update('menteetosource', $menteetosource);
						
				}
            	$this->session->set_flashdata('success',$data['config'][0]->menteeName.' Updated Successfully');
            }
			
			if(isset($_POST['callFromAppHidden'])) {
                redirect('admin/mentees/add?callFromApp=true');
            } else {
                redirect('admin/mentees');
            }
        }
		
		if(isset($_GET['callFromApp'])) {
            $data['callFromApp'] = $_GET['callFromApp'];
        }
         
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');
        
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-fileupload/bootstrap-fileupload.css');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-fileupload/bootstrap-fileupload.js');  
        
       
        $fields = get_table_fields('menteecontact');
        $mentees = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $mentees[$field] = '';
            }
            $mentees['MenteeName'] = '';
            $mentees['MenteeID'] = '';
			$mentees['ProgrammeID'] = '';
			$mentees['MenteeSourceID'] = '';
        }
        if ($id) {
             $mentees = $this->mentees_model->get_mentees($id);
        }
        $data['id'] = $id;
        $data['mentees'] = $mentees;
		$data['programmeData'] = $this->mentees_model->programmeData();
        $data['sourceData'] = $this->mentees_model->sourceData();
        $this->template->content->view('admin/mentees/add', $data);
        $this->template->publish_admin();
    }
    
    //=========================================
    // MENTEES EDIT  
    //=========================================
    public function edit($id){ 
       $this->add($id);
    }
    
    //=========================================
    // MENTEES DELETE 
    //=========================================
    public function delete($id) {
        $this->mentees_model->delete_mentees($id);

        $this->session->set_flashdata('success', get_message(SUCCESS_DELETED, 'mantee'));
        redirect('admin/mentees');
    }
    
    
    //=========================================
    // MENTEES DELETE 
    //=========================================
    public function deletesment($id){ 
         $this->mentees_model->delete_mentees($id);
         $this->session->set_flashdata('success', 'Mentees Delete Successfully');
         redirect('admin/mentees');
         
    }
	
	//=========================================
    // EAIL CHECK
    //=========================================
    public function checkEmailExist() {
        if ($this->input->is_ajax_request()) {
            $MenteeEmail = $this->input->post('MenteeEmail');
			$MenteeID = $this->input->post('MenteeID');
			if($MenteeID == "") {
				$flag = $this->mentees_model->checkuser_mail($MenteeEmail);
			} else {
				$flag = $this->mentees_model->checkuser_mail_edit($MenteeID, $MenteeEmail);
			}
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }
	
//	public function checkMenteeNameReq($key) {
//           // $name = $this->input->post('name');
//            $flag = $this->mentees_model->checkMenteeName($key);
//			if ($flag > 0){
//				$this->form_validation->set_message('checkMenteeNameReq', 'exuxs"');
//				return FALSE;
//			}
//			else{
//				return true;
//			}
//           // return ($flag > 0) ? "true" : "false";
//    }
	
	public function checkMenteeName() {
        if ($this->input->is_ajax_request()) {
            $MenteeName = $this->input->post('MenteeName');
			$MenteeID = $this->input->post('MenteeID');
			if($MenteeID == "") {
				$flag = $this->mentees_model->checkMenteeName($MenteeName);
			} else {
				$flag = $this->mentees_model->checkMenteeNameEdit($MenteeID, $MenteeName);
			}
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }
	
//	public function checkMenteeNameEdit() {
//		 if ($this->input->is_ajax_request()) {
//            $email = $this->input->post('name');
//            $id = $this->input->post('id');
//            
//            echo ($flag > 0) ? "true" : "false";
//        } else {
//            show_error("Access Denied");
//        }
//	}
    
	//=========================================
    // CHECK EMAIL EDIT TIME
    //=========================================
    //public function checkEmailEditExist() {
    //    if ($this->input->is_ajax_request()) {
    //        $email = $this->input->post('email_address');
    //        $id = $this->input->post('id');
    //        $flag = $this->mentees_model->checkuser_mail_edit($id, $email);
    //        echo ($flag > 0) ? "false" : "true";
    //    } else {
    //        show_error("Access Denied");
    //    }
    //}   
}
