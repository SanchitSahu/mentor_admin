<?php

class MentorAction extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/mentorAction_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        //print_r($res);exit;
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            //print_r($x);exit;
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // SKILL LISTING 
    //=========================================
    public function index() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );

        $res['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Action Name', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/mentorAction/list', $res);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $this->datatables->select("MentorActionID, MentorActionName, Weight", FALSE)
                ->unset_column('MentorActionID')
                ->unset_column('Actions')
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/mentorAction'), 'MentorActionID')
                ->from('mentordefinedactions')
                ->where('Status', '1');

        echo $this->datatables->generate();
    }

    //=========================================
    // SKILL ADD 
    //=========================================
    public function add($id = '') {
        $data['config'] = $this->settings_model->getConfig();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('MentorActionName', $data['config'][0]->mentorName . ' Action Name', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {

            $data = array(
                'MentorActionName' => $this->input->post('MentorActionName'),
                'Weight' => $this->input->post('Weight')
            );
            if ($id == 0) {
                $result = $this->db->insert('mentordefinedactions', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', $data['config'][0]->mentorName . ' Action Added Successfully');
            } else {
                $this->db->where('MentorActionID', $id);
                $result = $this->db->update('mentordefinedactions', $data);
                $this->session->set_flashdata('success', $data['config'][0]->mentorName . ' Action Updated Successfully');
            }
            redirect('admin/mentorAction');
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('mentordefinedactions');
        $mentordefinedactions = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $mentordefinedactions[$field] = '';
            }
        }

        if ($id) {
            $mentordefinedactions = $this->mentorAction_model->get_mentordefinedactions($id);
        }
        $data['id'] = $id;
        $data['mentordefinedactions'] = $mentordefinedactions;

        $this->template->content->view('admin/mentorAction/add', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // SKILL EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    public function checkMentorActionExist() {
        if ($this->input->is_ajax_request()) {
            $MentorActionName = $this->input->post('MentorActionName');
            $MentorActionID = $this->input->post('MentorActionID');
            if ($MentorActionID == "") {
                $flag = $this->mentorAction_model->checkMentorAction($MentorActionName);
            } else {
                $flag = $this->mentorAction_model->checkMentorActionEdit($MentorActionName, $MentorActionID);
            }
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    //=========================================
    // SKILL DELETE 
    //=========================================
    public function delete($id) {
        $data['config'] = $this->settings_model->getConfig();
        $ds = $this->mentorAction_model->delete_mentordefinedactions($id);
        $this->session->set_flashdata('success', $data['config'][0]->mentorName . ' Action Delete Successfully');
        redirect('admin/mentorAction');
    }

}
