<?php

class Reports extends Admin_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        $this->load->model('admin/report_model');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        //print_r($res);exit;
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            //print_r($x);exit;
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // CITY LISTING   
    //=========================================
    public function index() {
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('City Name', 'State Name', 'Country Name', 'Status', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/city/list');
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function datatable() {
        $this->datatables->select('ci.iCityId, ci.vCityName, s.vStateName, c.vCountryName,ci.eStatus', FALSE)
                ->unset_column('ci.iCityId')
                ->unset_column('ci.eStatus')
                ->add_column('Status', getStatusIcon('$1', '$2', 'admin/city'), 'ci.iCityId,ci.eStatus')
                ->add_column('Actions', get_buttons('$1', 'admin/city'), 'ci.iCityId')
                ->from('city as ci')
                ->join('state as s', 's.iStateId = ci.iStateId', 'left')
                ->join('country as c', 'c.iCountryId = ci.iCountryId', 'left');

        echo $this->datatables->generate();
    }

    //=========================================
    // MENTOR REPORT
    //=========================================
    public function mentorreport() {
        $res['config'] = $this->settings_model->getConfig();
        $res['role'] = $this->report_model->getRole();
        $res['source'] = $this->report_model->getSource();
        $res['programme'] = $this->report_model->getProgramme();
        $res['menteesource'] = $this->report_model->getMenteeSource();
        $res['semester'] = $this->report_model->getSemester();
        
        $tmpl = array(
            'table_open' => '<table id="dynamic_table2" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->mentorName . ' Name', $res['config'][0]->menteeName . ' Name', 'Number of meetings', 'Total Time(Minutes)', $res['config'][0]->mentorName . ' Role', $res['config'][0]->mentorName . ' Source', $res['config'][0]->menteeName . ' Programme', $res['config'][0]->menteeName . ' Source');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');
        
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.css');
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.date.css');
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.time.css');
        $this->template->javascript->add($this->config->item('base_js') . 'pickadate.js');

        $this->template->content->view('admin/reports/mentorreport', $res);
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function mentordatatable() {
        $this->datatables->select('m.MentorName,me.MenteeName,count(m.MentorID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName, mp.ProgrammeName as menteeProgrammeName, mes.MenteeSourceName as menteeSourceName', FALSE)
                ->from('meetinginfo mi')
                ->join('mentor m', 'mi.MentorID=m.MentorID', 'left')
                ->join('mentee me', 'mi.MenteeID=me.MenteeID', 'left')
                ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                
                ->join('menteetoprogramme mtp', 'mtp.MenteeID = me.MenteeID', 'left')
                ->join('menteeprogrammes mp', 'mp.ProgrammeID = mtp.ProgrammeID', 'left')        
                ->join('menteetosource mts', 'mts.MenteeID = me.MenteeID', 'left')
                ->join('menteesourcenames mes', 'mes.MenteeSourceID = mts.MenteeSourceID', 'left')
                
                ->where('m.Status', 1)
                ->where('me.Status', 1)
                ->group_by("m.MentorID")
				->group_by("me.MenteeID");

        echo $this->datatables->generate();
    }

    public function mentordetailreport() {
        $res['config'] = $this->settings_model->getConfig();
        $res['role'] = $this->report_model->getRole();
        $res['source'] = $this->report_model->getSource();
        $res['semester'] = $this->report_model->getSemester();
        
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->mentorName . ' Name', $res['config'][0]->menteeName . ' Name', 'Meeting Start', 'Meeting End', 'Elapsed Time (hh:mm)', 'Feedback', $res['config'][0]->mentorName . ' Comment', 'Satisfaction Index', 'Role', 'Source');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');
        
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.css');
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.date.css');
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.time.css');
        $this->template->javascript->add($this->config->item('base_js') . 'pickadate.js');

        $this->template->content->view('admin/reports/mentordetailreport', $res);
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function mentordetaildatatable() {
        $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, TIME_FORMAT(SEC_TO_TIME(mi.MeetingElapsedTime*60),"%H:%i" ) AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as RoleName,ms.MentorSourceName as SourceName', FALSE)
                ->from('meetinginfo mi')
                ->join('mentor m', 'm.MentorID = mi.MentorID')
                ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
				->group_by('mi.MeetingID');

        echo $this->datatables->generate();
    }

    //=========================================
    // MENTEE REPORT
    //=========================================
    public function menteereport() {
        $res['config'] = $this->settings_model->getConfig();
        $res['role'] = $this->report_model->getRole();
        $res['source'] = $this->report_model->getSource();
        
        $res['programme'] = $this->report_model->getProgramme();
        $res['menteesource'] = $this->report_model->getMenteeSource();
        
        $res['semester'] = $this->report_model->getSemester();
         
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->menteeName . ' Name', $res['config'][0]->mentorName . ' Name', 'Number of meetings', 'Total Time(Minutes)', $res['config'][0]->mentorName . ' Role', $res['config'][0]->mentorName . ' Source', $res['config'][0]->menteeName . ' Programme', $res['config'][0]->menteeName . ' Source');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');
        
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.css');
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.date.css');
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.time.css');
        $this->template->javascript->add($this->config->item('base_js') . 'pickadate.js');

        $this->template->content->view('admin/reports/menteereport', $res);
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function menteedatatable() {
        $this->datatables->select('me.MenteeName,m.MentorName,count(me.MenteeID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName, mp.ProgrammeName as menteeProgrammeName, mes.MenteeSourceName as menteeSourceName', FALSE)
                ->from('meetinginfo mi')
                ->join('mentor m', 'mi.MentorID=m.MentorID', 'left')
                ->join('mentee me', 'mi.MenteeID=me.MenteeID', 'left')
                ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                
                ->join('menteetoprogramme mtp', 'mtp.MenteeID = me.MenteeID', 'left')
                ->join('menteeprogrammes mp', 'mp.ProgrammeID = mtp.ProgrammeID', 'left')        
                ->join('menteetosource mts', 'mts.MenteeID = me.MenteeID', 'left')
                ->join('menteesourcenames mes', 'mes.MenteeSourceID = mts.MenteeSourceID', 'left')
                
                ->where('m.Status', 1)
                ->where('me.Status', 1)
				->group_by("m.MentorID")
                ->group_by("me.MenteeID");

        echo $this->datatables->generate();
    }

    public function menteedetailreport() {
        $res['config'] = $this->settings_model->getConfig();
        $res['role'] = $this->report_model->getRole();
        $res['source'] = $this->report_model->getSource();
        $res['semester'] = $this->report_model->getSemester();
        $res['mentors'] = $this->report_model->getMentors();
		$res['mentees'] = $this->report_model->getMentees();
        
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->menteeName . ' Name', $res['config'][0]->mentorName . ' Name', 'Meeting Start', 'Meeting End', 'Elapsed Time (hh:mm)', 'Feedback', $res['config'][0]->menteeName . ' Comment', 'Care C', 'Care A', 'Care R', 'Care E');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');
        
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.css');
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.date.css');
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.time.css');
        $this->template->javascript->add($this->config->item('base_js') . 'pickadate.js');

        $this->template->content->view('admin/reports/menteedetailreport', $res);
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function menteedetaildatatable() {
        $this->datatables->select('me.MenteeName AS MenteeName,m.MentorName AS MentorName,  mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime,TIME_FORMAT(SEC_TO_TIME(mi.MeetingElapsedTime*60),"%H:%i" ) AS MeetingElapsedTime, mi.MeetingFeedback, mc.MenteeComment AS MenteeComment, mc.CareC AS CareC, mc.CareA AS CareA, mc.CareR AS CareR, mc.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                ->from('meetinginfo mi')
                ->join('mentor m', 'm.MentorID = mi.MentorID')
                ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
				->group_by('mi.MeetingID');

        echo $this->datatables->generate();
    }

    //=========================================
    // GLOBAL REPORT
    //=========================================
    public function globalreport() {
        $res['config'] = $this->settings_model->getConfig();
        $res['role'] = $this->report_model->getRole();
        $res['source'] = $this->report_model->getSource();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        //, 'Mentee Comment', 'Care C', 'Care A', 'Care R', 'Care E
        $this->table->set_heading($res['config'][0]->mentorName . ' Name', $res['config'][0]->menteeName . ' Name', 'Meeting Start', 'Meeting End', 'Elapsed Time', 'Feedback', $res['config'][0]->mentorName . ' Comment', 'Satisfaction Index', 'Role', 'Source');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/reports/globalreport', $res);
        $this->template->publish_admin();
    }

    public function mentorMeetingReport() {
        $res['config'] = $this->settings_model->getConfig();
        $res['role'] = $this->report_model->getRole();
        $res['source'] = $this->report_model->getSource();
		$res['mentors'] = $this->report_model->getMentors();
		$res['mentees'] = $this->report_model->getMentees();
        $res['semester'] = $this->report_model->getSemester();
        
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        //, 'Mentee Comment', 'Care C', 'Care A', 'Care R', 'Care E
        $this->table->set_heading($res['config'][0]->mentorName . ' Name', $res['config'][0]->menteeName . ' Name', 'Meeting Start', 'Meeting End', 'Elapsed Time (hh:mm)', 'Feedback', $res['config'][0]->mentorName . ' Comment', 'Satisfaction Index', 'Role', 'Source');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');
        
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.css');
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.date.css');
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.time.css');
        $this->template->javascript->add($this->config->item('base_js') . 'pickadate.js');

        $this->template->content->view('admin/reports/mentorMeetingReport', $res);
        $this->template->publish_admin();
    }

    public function menteeMeetingReport() {
        $res['config'] = $this->settings_model->getConfig();
        $res['programme'] = $this->report_model->getProgramme();
        $res['source'] = $this->report_model->getSource();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        //, 'Mentee Comment', 'Care C', 'Care A', 'Care R', 'Care E'
        $this->table->set_heading($res['config'][0]->menteeName . ' Name', $res['config'][0]->mentorName . ' Name', 'Meeting Start', 'Meeting End', 'Elapsed Time', 'Feedback', $res['config'][0]->menteeName . ' Comment', 'Care C', 'Care A', 'Care R', 'Care E', 'Programme', 'Source');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/reports/menteeMeetingReport', $res);
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function globaldatatable() {
        //mec.MenteeComment AS MenteeComment, mec.CareC AS CareC, mec.CareA AS CareA, mec.CareR AS CareR, mec.CareE AS CareE
        $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                ->from('meetinginfo mi')
                ->join('mentor m', 'm.MentorID = mi.MentorID')
                ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left');
        //->join('menteecomment mec', 'mec.MeetingID = mi.MeetingID','inner');

        echo $this->datatables->generate();
    }

    function menteeMeetingReportDatatable() {
        //mec.MenteeComment AS MenteeComment, mec.CareC AS CareC, mec.CareA AS CareA, mec.CareR AS CareR, mec.CareE AS CareE
        $this->datatables->select('me.MenteeName AS MenteeName, m.MentorName AS MentorName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mec.CareC AS CareC, mec.CareA AS CareA, mec.CareR AS CareR, mec.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                ->from('meetinginfo mi')
                ->join('mentor m', 'm.MentorID = mi.MentorID')
                ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                ->join('menteecomment mec', 'mec.MeetingID = mi.MeetingID', 'inner');

        echo $this->datatables->generate();
    }

    function mentorMeetingReportDatatable() {
        //mec.MenteeComment AS MenteeComment, mec.CareC AS CareC, mec.CareA AS CareA, mec.CareR AS CareR, mec.CareE AS CareE
        $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, TIME_FORMAT(SEC_TO_TIME(mi.MeetingElapsedTime*60),"%H:%i" ) AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,CASE WHEN mci.IsSubadmin=1 THEN CONCAT(ur.UserRoleName,"+Subadmin") ELSE ur.UserRoleName END AS userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                ->from('meetinginfo mi')
                ->join('mentor m', 'm.MentorID = mi.MentorID', 'left')
                ->join('mentorcontactinfo mci', 'mci.MentorID = mi.MentorID', 'left')
                ->join('mentee me', 'me.MenteeID = mi.MenteeID', 'left')
                ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
				->group_by('mi.MeetingID');		
        //->join('menteecomment mec', 'mec.MeetingID = mi.MeetingID','inner');

        echo $this->datatables->generate();
    }

    //=========================================
    // mentorskill REPORT
    //=========================================
    public function mentorskillreport() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->mentorName . ' Name', 'Skills ' . $res['config'][0]->mentorName . ' Provides');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/reports/mentorskillreport', $res);
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function mentorskilldatatable() {
        if($this->input->post('sSearch') != ""){
			$this->datatables->select("mm.MentorName,ss.SkillName", FALSE)
                ->from('mentor mm')
				->join('mentorskillset mss', 'mm.MentorID = mss.MentorID', 'left')
                ->join('skill ss', 'ss.SkillID = mss.SkillID', 'left')
                ->group_by('mss.MentorID')
                ->where('mm.Status', '1')
                ->where('mss.Status', '1');
		}else{
			$this->datatables->select("mm.MentorName,GROUP_CONCAT(ss.SkillName ORDER BY ss.SkillName ASC SEPARATOR ', ') AS SkillName", FALSE)
                ->from('mentor mm')
				->join('mentorskillset mss', 'mm.MentorID = mss.MentorID', 'left')
                ->join('skill ss', 'ss.SkillID = mss.SkillID', 'left')
                ->group_by('mss.MentorID')
                ->where('mm.Status', '1')
                ->where('mss.Status', '1');
		}

        echo $this->datatables->generate();
    }

    //=========================================
    // menteeskill REPORT
    //=========================================
    public function menteeskillreport() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->menteeName . ' Name', 'Needed Skills');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/reports/menteeskillreport', $res);
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function menteeskilldatatable() {
        if($this->input->post('sSearch') != ""){
			$this->datatables->select("m.MenteeName,s.SkillName", FALSE)
                ->from('mentee m')
				->join('menteeskill ms', 'm.MenteeID = ms.MenteeID', 'left')
                ->join('skill s', 's.SkillID = ms.SkillID', 'left')
                ->group_by('ms.MenteeID')
                ->where('m.Status', '1')
                ->where('ms.Status', '1');
		}else{
			$this->datatables->select("m.MenteeName,GROUP_CONCAT(s.SkillName ORDER BY s.SkillName ASC SEPARATOR ', ') AS SkillName ", FALSE)
                ->from('mentee m')
				->join('menteeskill ms', 'm.MenteeID = ms.MenteeID', 'left')
                ->join('skill s', 's.SkillID = ms.SkillID', 'left')
                ->group_by('ms.MenteeID')
                ->where('m.Status', '1')
                ->where('ms.Status', '1');
		}
        //$this->datatables->select('m.MenteeName,s.skillName', FALSE)
        //        ->from('mentee m')
        //        ->join('menteeskill ms', 'm.MenteeID = ms.MenteeID', 'left')
        //        ->join('skill s', 'ms.SkillID = s.skillID', 'left')
        //        ->where('m.Status', 1);
        //$this->db->group_by("m.MentorID"); 
        //$this->db->group_by("m.MentorName"); 

        echo $this->datatables->generate();
    }

    //=========================================
    // mentorRole REPORT
    //=========================================
    public function mentorrolereport() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->mentorName . ' Name', 'Role');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');
        
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.css');
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.date.css');
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.time.css');
        $this->template->javascript->add($this->config->item('base_js') . 'pickadate.js');

        $this->template->content->view('admin/reports/mentorrolereport', $res);
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function mentorroledatatable() {
        $mentorFinal = array();

        $this->datatables->select('
                m.MentorName,
                CASE WHEN mci.IsSubadmin=1 THEN CONCAT(ur.UserRoleName,"+Subadmin") ELSE ur.UserRoleName END AS UserRoleName', FALSE)
                ->from('mentor m')
                ->join('mentorcontactinfo mci', 'mci.MentorID = m.MentorID', 'left')
                ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->where('m.Status', 1);
        //$this->db->group_by("m.MentorID"); 
        //$this->db->group_by("m.MentorName"); 

        echo $this->datatables->generate();
    }

    //=========================================
    // mentorSource REPORT
    //=========================================
    public function mentorsourcereport() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->mentorName . ' Name', 'Source');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/reports/mentorsourcereport', $res);
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function mentorsourcedatatable() {
        $mentorFinal = array();

        $this->datatables->select('m.MentorName,ms.MentorSourceName', FALSE)
                ->from('mentor m')
                ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                ->where('m.Status', 1)
                ->group_by("m.MentorID"); 
        //$this->db->group_by("m.MentorName"); 

        echo $this->datatables->generate();
    }

    //=========================================
    // menteeProgramme REPORT
    //=========================================
    public function mentorprogrammereport() {
        $res['config'] = $this->settings_model->getConfig();
        $res['programme'] = $this->report_model->getProgramme();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->menteeName . ' Name', 'Programme');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/reports/menteeprogrammereport', $res);
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function menteeprogrammedatatable() {
        $mentorFinal = array();

        $this->datatables->select('m.MenteeName,p.ProgrammeName', FALSE)
                ->from('mentee m')
                ->join('menteetoprogramme mp', 'mp.MenteeID = m.MenteeID', 'left')
                ->join('menteeprogrammes p', 'mp.ProgrammeID = p.ProgrammeID', 'left')
                ->where('m.Status', 1);
        //$this->db->group_by("m.MentorID"); 
        //$this->db->group_by("m.MentorName"); 

        echo $this->datatables->generate();
    }

    //=========================================
    // menteeSource REPORT
    //=========================================
    public function menteesourcereport() {
        //error_reporting(E_ALL);
        //ini_set("display_errors","ON");
        $res['config'] = $this->settings_model->getConfig();
        $res['source'] = $this->report_model->getMenteeSource();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->menteeName . ' Name', 'Source');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/reports/menteesourcereport', $res);
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function menteesourcedatatable() {
        $mentorFinal = array();

        $this->datatables->select('m.MenteeName,ms.MenteeSourceName', FALSE)
                ->from('mentee m')
                ->join('menteetosource us', 'us.MenteeID = m.MenteeID', 'INNER')
                ->join('menteesourcenames ms', 'us.MenteeSourceID = ms.MenteeSourceID', 'INNER')
                ->where('m.Status', 1)
                ->group_by("m.MenteeID"); 
        //$this->db->group_by("m.MentorName"); 

        echo $this->datatables->generate();
    }

    //=========================================
    //EXPORT REPORTS
    //=========================================
    function exporttoexcel() {
        $this->load->view("admin/reports/exporttoexcel");
    }
    
    
    //=========================================
    // FOLLOWUP REPORT
    //=========================================
    public function followUpReport(){
        $res['config'] = $this->settings_model->getConfig();
        $res['role'] = $this->report_model->getRole();
        $res['source'] = $this->report_model->getSource();
        $res['mentors'] = $this->report_model->getMentors();
        $res['mentees'] = $this->report_model->getMentees();
        $res['semester'] = $this->report_model->getSemester();
        
        $tmpl = array(
            'table_open' => '<table id="followupmeetingtable" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        //, 'Mentee Comment', 'Care C', 'Care A', 'Care R', 'Care E
        $this->table->set_heading($res['config'][0]->mentorName . ' Name', $res['config'][0]->menteeName . ' Name', 'Topic name', 'Number of consecutive meetings', 'Total elapsed time in consecutive meetings');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');
        //
        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        //$this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/reports/followupmeetingreport', $res);
        $this->template->publish_admin();
        
        //$this->load->view("admin/reports/followupmeetingreport", $res);
    }
    
    public function followUpReportData() {
        $meetings = $this->report_model->get_meetings();
        $followUpMeeting = array();
        if (count($meetings) > 0) {
            $key = '';
            $temp = array();
            $last_key = '';

            foreach ($meetings as $index => $data) {
                $key = $data->MentorID . '-' . $data->MenteeID . '-' . $data->MeetingTopicID;
                if (isset($temp[$key]) && count($temp[$key]) > 0) {
                    $temp[$key][] = $data;
                } else {
                    if (isset($temp[$last_key]) && count($temp[$last_key]) > 1) {
                        $followUpMeeting[$last_key] = $temp[$last_key];
                    }
                    $last_key = $key;
                    $temp = array();
                    $temp[$key][] = $data;
                }
                if ($index == (count($meetings) - 1)) {
                    if (isset($temp[$last_key]) && count($temp[$last_key]) > 1) {
                        $followUpMeeting[$last_key] = $temp[$last_key];
                    }
                }
                
                if($data->FollowUpMeetingFor > 0) {
                    
                    foreach ($meetings as $key => $value) {
                        if($data->FollowUpMeetingFor == $value->MeetingID) {
                            $tkey = $value->MentorID . '-' . $value->MenteeID . '-' . $value->MeetingTopicID;
                            
                            if(isset($followUpMeeting[$tkey])) {
                                $count = 0;
                                foreach ($followUpMeeting[$tkey] as $k => $v) {
                                    if($v->MeetingID == $value->MeetingID) {
                                        $count += 1;
                                    }
                                }
                                if($count == 0) {
                                    $followUpMeeting[$tkey][] = $data;
                                }
                            } else {
                                $followUpMeeting[$tkey][] = $data;
                            }
                        }
                    }
                }
            }
        }
        
        $total_count = count($followUpMeeting);

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $total_count,
            "iTotalDisplayRecords" => $total_count,
            "aaData" => array()
        );
        
        if ($followUpMeeting) {
            foreach ($followUpMeeting as $meeting) {
                $elapsed_time = 0;
                foreach ($meeting as $eachMeeting) {
                    $elapsed_time += $eachMeeting->MeetingElapsedTime;
                }
                $mentor_detail = $this->report_model->getUserDetails($meeting[0]->MentorID, 'mentor');
                $mentee_detail = $this->report_model->getUserDetails($meeting[0]->MenteeID, 'mentee');
                $topic_detail = $this->report_model->get_topic_details($meeting[0]->MeetingTopicID);

				$row = array('Mentor' . $meeting[0]->MentorID, 'Mentee'.$meeting[0]->MenteeID, $topic_detail[0]->TopicDescription, count($meeting), $elapsed_time);
                //$row = array($mentor_detail[0]->MentorName, $mentee_detail[0]->MenteeName, $topic_detail[0]->TopicDescription, count($meeting), $elapsed_time);
                $output['aaData'][] = $row;
            }
        }
        die(json_encode($output));
    }
    
    //=========================================
    // FOLLOWUP Details REPORT
    //=========================================
    public function followUpDetailsReport(){
        $res['config'] = $this->settings_model->getConfig();
        $res['role'] = $this->report_model->getRole();
        $res['source'] = $this->report_model->getSource();
        $res['mentors'] = $this->report_model->getMentors();
        $res['mentees'] = $this->report_model->getMentees();
        $res['semester'] = $this->report_model->getSemester();
        
        $tmpl = array(
            'table_open' => '<table id="followupmeetingdetailtable" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->mentorName . ' Name', $res['config'][0]->menteeName . ' Name', 'Topic name', 'Date of meeting', 'Elapsed time in meeting');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');
        //
        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');

        $this->template->content->view('admin/reports/followupmeetingdetailreport', $res);
        $this->template->publish_admin();
    }
    
    public function followUpReportDetailsData() {
        $meetings = $this->report_model->get_meetings();
        $followUpMeeting = array();
        if (count($meetings) > 0) {
            $key = '';
            $temp = array();
            $last_key = '';
            foreach ($meetings as $index => $data) {
                $key = $data->MentorID . '-' . $data->MenteeID . '-' . $data->MeetingTopicID;
                if (isset($temp[$key]) && count($temp[$key]) > 0) {
                    $temp[$key][] = $data;
                } else {
                    if (isset($temp[$last_key]) && count($temp[$last_key]) > 1) {
                        $followUpMeeting[$last_key] = $temp[$last_key];
                    }
                    $last_key = $key;
                    $temp = array();
                    $temp[$key][] = $data;
                }
                if ($index == (count($meetings) - 1)) {
                    if (isset($temp[$last_key]) && count($temp[$last_key]) > 1) {
                        $followUpMeeting[$last_key] = $temp[$last_key];
                    }
                }
                
                if($data->FollowUpMeetingFor > 0) {
                    
                    foreach ($meetings as $key => $value) {
                        if($data->FollowUpMeetingFor == $value->MeetingID) {
                            $tkey = $value->MentorID . '-' . $value->MenteeID . '-' . $value->MeetingTopicID;
                            
                            if(isset($followUpMeeting[$tkey])) {
                                $count = 0;
                                foreach ($followUpMeeting[$tkey] as $k => $v) {
                                    if($v->MeetingID == $value->MeetingID) {
                                        $count += 1;
                                    }
                                }
                                if($count == 0) {
                                    $followUpMeeting[$tkey][] = $data;
                                }
                            } else {
                                $followUpMeeting[$tkey][] = $data;
                            }
                        }
                    }
                }
            }
        }
        
        $total_count = count($followUpMeeting);

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $total_count,
            "iTotalDisplayRecords" => $total_count,
            "aaData" => array()
        );
        
        if ($followUpMeeting) {
            foreach ($followUpMeeting as $meeting) {
                $mentor_detail = $this->report_model->getUserDetails($meeting[0]->MentorID, 'mentor');
                $mentee_detail = $this->report_model->getUserDetails($meeting[0]->MenteeID, 'mentee');
                $topic_detail = $this->report_model->get_topic_details($meeting[0]->MeetingTopicID);
                //print_r($meeting[0]);
                //$row = array($mentor_detail[0]->MentorName, $mentee_detail[0]->MenteeName, $topic_detail[0]->TopicDescription, $meeting[0]->MeetingStartDatetime, ($meeting[0]->MeetingElapsedTime > 0) ? $meeting[0]->MeetingElapsedTime : 0);
				$row = array('Mentor '.$meeting[0]->MentorID, 'Mentee '.$meeting[0]->MenteeID, $topic_detail[0]->TopicDescription, $meeting[0]->MeetingStartDatetime, ($meeting[0]->MeetingElapsedTime > 0) ? $meeting[0]->MeetingElapsedTime : 0);
                $output['aaData'][] = $row;
                $elapsed_time = 0;
                foreach ($meeting as $key => $eachMeeting) {
                    $elapsed_time += $eachMeeting->MeetingElapsedTime;
                    if($key == 0) continue;
                    $row = array('', '', '', $eachMeeting->MeetingStartDatetime, ($eachMeeting->MeetingElapsedTime > 0) ? $eachMeeting->MeetingElapsedTime : 0);
                    $output['aaData'][] = $row;
                    if($key == (count($meeting) - 1)) {
                        $row = array('<b>Mentor '.$meeting[0]->MentorID.'</b>', '<b>Mentee '.$meeting[0]->MenteeID.'</b>', '<b>'.$topic_detail[0]->TopicDescription.'</b>', '<b>TOTAL</b>', '<b>'.$elapsed_time.'</b>');
                        $output['aaData'][] = $row;
                    }
                }
            }
        }
        die(json_encode($output));
    }


	//=========================================
    // TEAM REPORT
    //=========================================
    public function teamreport() {
		//, $res['config'][0]->mentorName . ' Role', $res['config'][0]->mentorName . ' Source'
        $res['config'] = $this->settings_model->getConfig();
        $res['role'] = $this->report_model->getRole();
        $res['source'] = $this->report_model->getSource();
                 
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->teamManager. ' Name', $res['config'][0]->mentorName . ' Name', 'Number of meetings', 'Total Time(Minutes)', $res['config'][0]->mentorName . ' Role', $res['config'][0]->mentorName . ' Source','Member ' . $res['config'][0]->mentorName,'Member ' . $res['config'][0]->menteeName);

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');
        
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.css');
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.date.css');
        $this->template->stylesheet->add($this->config->item('base_css') . 'pickadate/default.time.css');
        $this->template->javascript->add($this->config->item('base_js') . 'pickadate.js');

        $this->template->content->view('admin/reports/teamreport', $res);
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function teamdatatable() {
        $this->datatables->select('t.TeamName,m.MentorName,count(DISTINCT mi.TeamID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName,group_concat(m.MentorName) as MentorMember,group_concat(me.MenteeName) as MenteeMember', FALSE)
                ->from('meetinginfo mi')
				->join('team t', 't.TeamId = mi.TeamID', 'left')
				->join('teammember tm', 'tm.TeamID = t.TeamId  AND tm.ApprovalStatus = "joined"', 'left')
                ->join('mentor m', 'mi.MentorID=m.MentorID AND m.MentorID = tm.UserID AND tm.UserType = 0 AND m.Status=1', 'left')
                ->join('mentee me', 'mi.MenteeID=me.MenteeID AND mi.MenteeID = tm.UserID AND tm.UserType = 1 AND me.Status=1', 'left')
                ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                
                
				->where('t.TeamStatus', 'active')
				->group_by('tm.TeamId');
        echo $this->datatables->generate();
    }


	//=========================================
    // SUB ADMIN REPORT
    //=========================================
    public function subadminreport() {
        $res['config'] = $this->settings_model->getConfig();
        $res['role'] = $this->report_model->getRole();
        $res['source'] = $this->report_model->getSource();
        $res['mentors'] = $this->report_model->getMentors();
        $res['mentees'] = $this->report_model->getMentees();
        
        $tmpl = array(
            'table_open' => '<table id="subadminReport" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->mentorName . ' Name', 'Meeting With name', 'Date of meeting', 'Elapsed time in meeting');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');
        //
        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');

        $this->template->content->view('admin/reports/subadminreport', $res);
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function subadminatatable() {
		$meetings = $this->report_model->get_meetings();
		$subadmin = array();
		//print_r($meetings);

		if (count($meetings) > 0) {
		    $key = '';
		    $temp = array();
		    $last_key = '';

		    foreach ($meetings as $index => $data) {
		        $submentor_detail = $this->report_model->getSubmentorDetails($data->MentorID);

		        if ($submentor_detail !== false) {
		            $key = $submentor_detail[0]->AddedByMentorId;

		            if (isset($temp[$key]) && count($temp[$key]) > 0) {
		                $temp[$key][] = $data;
		            } else {
		                if (isset($temp[$last_key]) && count($temp[$last_key]) > 0) {
		                    $subadmin[$last_key] = $temp[$last_key];
		                }
		                $last_key = $key;
		                $temp = array();
		                $temp[$key][] = $data;
		            }
		            if ($index == (count($meetings) - 1)) {
		                if (isset($temp[$last_key]) && count($temp[$last_key]) > 0) {
		                    $subadmin[$last_key] = $temp[$last_key];
		                }
		            }
		        }
		    }
		}

		$total_count = count($subadmin);

		$output = array(
		    "sEcho" => intval($_GET['sEcho']),
		    "iTotalRecords" => $total_count,
		    "iTotalDisplayRecords" => $total_count,
		    "aaData" => array()
		);

		//print_r($subadmin);
		if ($subadmin) {
//echo '<pre>$subadmin';
//print_r($subadmin);
//echo "<br>";
		    foreach ($subadmin as $key2=>$meeting) {
//echo "key2=>$key2<br>";
//echo "meeting<br>";
//print_r($meeting);
//echo "<br>";
				//echo $key;
				$getMainMentor = $this->report_model->getMentorName($key2);
		        $mentor_detail = $this->report_model->getUserDetails($meeting[0]->MentorID, 'mentor');
		        $mentee_detail = $this->report_model->getUserDetails($meeting[0]->MenteeID, 'mentee');
				$getSubmentor = $this->report_model->getSubmentorDetails($meeting[0]->MentorID);
				$elapsed_time = 0;
		        foreach ($meeting as $key => $eachMeeting) {
					$mentorName = $this->report_model->getMentorName($eachMeeting->MentorID);

					if($eachMeeting->MenteeID != 0){
						$menteeName = $this->report_model->getMenteeName($eachMeeting->MenteeID);
					}else{
						$menteeName = $this->report_model->getTeamName($eachMeeting->TeamID);
					}

					if($menteeName != "") {
						$meetingWith = $mentorName." with ".$menteeName;
		            
		            

		            //foreach ($meeting as $key => $eachMeeting) {

		                $elapsed_time += $eachMeeting->MeetingElapsedTime;
		                //if ($key == 0) {
							//echo "continue";
		                    //continue;
						//}
						//echo "continue else";

						if($key == 0) {
							$row = array($getMainMentor, $meetingWith, $meeting[0]->MeetingStartDatetime, ($meeting[0]->MeetingElapsedTime > 0) ? $meeting[0]->MeetingElapsedTime : 0);
						} else {
							$row = array('', $meetingWith, $eachMeeting->MeetingStartDatetime, ($eachMeeting->MeetingElapsedTime > 0) ? $eachMeeting->MeetingElapsedTime : 0);
						}
						$output['aaData'][] = $row;
		                
					}
		                if ($key == (count($meeting) - 1) || count($meeting) == 1) {
							//echo '$key == (count($meeting) - 1)';
		                    $row = array('<b>' . $getMainMentor . '</b>','' , '<b>TOTAL</b>', '<b>' . $elapsed_time . '</b>');
		                    $output['aaData'][] = $row;
		                } else {
							//echo '$key == (count($meeting) - 1) else';
						}
		            //}
					
		        }
		    }
		}
//exit;
		die(json_encode($output));
	}
}
