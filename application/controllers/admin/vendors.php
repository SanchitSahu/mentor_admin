<?php

class Vendors extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/vendors_model');

        $this->load->model('admin/employee_model');
        $this->load->model('admin/country_model');
        $this->load->model('admin/state_model');
        $this->load->model('admin/city_model');
        $this->load->model('admin/vendorservicecms_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if(!loginCheck()){redirect('admin');}
		$res	=	loadDatabase();
		//print_r($res);exit;
		if($res != 0){
			$x	=	$this->load->database($res,TRUE);
			//print_r($x);exit;
			$this->db	=	$x;	
		}else{
			redirect("400.shtml");exit;
		}
    }

    public function index() {
        /*  $data['vendorsData'] = $this->vendors_model->vendorsList();
          $this->load->view('includes/header_common');
          $this->load->view('admin/vendors/list',$data);
          $this->load->view('includes/footer_common'); */

        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('Vendor Name', 'Firm Name', 'Username', 'Email', 'Gender', 'Cellphone', 'Phone No.', 'Address', 'Status', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/vendors/list');
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function datatable() {
        $userSession = $this->session->userdata('admin_data');
        if ($userSession['UserType'] == "Admin") { # FOR ADMIN LOGIN
            $this->datatables->select('iVendorId, CONCAT(vFirstName," ",vLastName) as FullName, vFirmName, vOwnerName, vEmail, eGender, vCellPhone, vPhoneNumber1, vAddress1, eStatus', false)
                ->unset_column('iVendorId')
                ->unset_column('eStatus')
                ->add_column('Status', getStatusIcon('$1', '$2', 'admin/vendors'), 'iVendorId,eStatus')
                ->add_column('Actions', get_vendor_buttons('$1', 'admin/vendors', 'admin/vendorgallery/gallerylist'), 'iVendorId')
                ->from('vendordetails');
        } else { # FOR VENDOR LOGIN
            $this->datatables->select('iVendorId, CONCAT(vFirstName," ",vLastName) as FullName, vFirmName, vOwnerName, vEmail, eGender, vCellPhone, vPhoneNumber1, vAddress1, eStatus', false)
                ->unset_column('iVendorId')
                ->unset_column('eStatus')
                ->add_column('Status', getStatusIcon('$1', '$2', 'admin/vendors'), 'iVendorId,eStatus')
                ->add_column('Actions', get_vendor_buttons('$1', 'admin/vendors', 'admin/vendorgallery/gallerylist'), 'iVendorId')
                ->from('vendordetails')
                ->where('iVendorId',$userSession['id']);
        }
        echo $this->datatables->generate();
    }

    public function add($id = "") {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('vFirstName', 'First Name', '');
        $this->form_validation->set_rules('vLastName', 'Last Name', '');
        $this->form_validation->set_rules('vOwnerName', 'Owner Name', 'required');
        $this->form_validation->set_rules('vFirmName', 'Firm Name', '');
        // $this->form_validation->set_rules('vPictureURL', 'Picture URL', '');
        // $this->form_validation->set_rules('vUserName', 'User Name', 'required');
        if ($id == "") {
            $this->form_validation->set_rules('vPassword', 'Password', '');
            $this->form_validation->set_rules('vcPassword', 'Confrim Password', '');
        }
        $this->form_validation->set_rules('vEmail', 'Email', '');
        $this->form_validation->set_rules('vPersonalEmail', 'Personal Email', '');
        $this->form_validation->set_rules('eGender', 'Gender', '');
        $this->form_validation->set_rules('dBirthDate', 'BirthDate', '');
        $this->form_validation->set_rules('dAnniversaryDate', 'AnniversaryDate', '');
        $this->form_validation->set_rules('vCellPhone', 'CellPhone', '');
        $this->form_validation->set_rules('vPhoneNumber1', 'PhoneNumber', '');
        $this->form_validation->set_rules('vPhoneNumber2', 'PhoneNumber', '');
        $this->form_validation->set_rules('vBlock', 'vBlock', '');
        $this->form_validation->set_rules('vAddress1', 'Address1', 'required');
        $this->form_validation->set_rules('vAddress2', 'Address2', '');
        $this->form_validation->set_rules('iCountryId', 'Country', 'required');
        $this->form_validation->set_rules('iStateId', 'State', 'required');
        $this->form_validation->set_rules('vCity', 'City', 'required');
        $this->form_validation->set_rules('vPostCode', 'Post Code', '');
        $this->form_validation->set_rules('vTimeOpration', 'Time Operation', '');
        $this->form_validation->set_rules('vWebsite', 'Website', '');
        $this->form_validation->set_rules('eStatus', 'Status', '');

        if ($this->form_validation->run() == TRUE) {

            if ($this->input->post()) {
                $userData = $this->session->userdata('admin_data');
                $vendors = $this->input->post();
                if (isset($vendors['vPassword']) && $vendors['vPassword'] == $vendors['vcPassword']) {
                    $vendors['vPassword'] = fnEncrypt($vendors['vPassword'], $this->config->item('sSecretKey'));
                    unset($vendors['vcPassword']);
                }
                if (isset($vendors['eStatus']) && $vendors['eStatus'] == "1") {
                    $vendors['eStatus'] = "1";
                } else {
                    $vendors['eStatus'] = "0";
                }

                $countryName = $this->country_model->get_country_details($vendors['iCountryId']);
                $stateName = $this->state_model->get_state_details($vendors['iStateId']);


                $address = str_replace(' ', '+', $vendors['vAddress1'] . '+' . $vendors['vAddress2'] . '+' . $countryName['vCountryName'] . '+' . $stateName['vStateName'] . '+' . $vendors['vPostCode']);
                $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=India";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_a = json_decode($response);
                if (isset($response_a->results[0]->geometry->location->lat) && $response_a->results[0]->geometry->location->lat != "") {
                    $vendors['dLatitude'] = $response_a->results[0]->geometry->location->lat;
                }
                if (isset($response_a->results[0]->geometry->location->lng) && $response_a->results[0]->geometry->location->lng != "") {
                    $vendors['dLongitude'] = $response_a->results[0]->geometry->location->lng;
                }
                if (isset($vendors['vBlock']) && $vendors['vBlock'] != "") {
                    $vendors['vAddress1'] = $vendors['vBlock'] . ', ' . $vendors['vAddress1'];
                    
                }
                unset($vendors['vBlock']);
                if ($id != "") {
                    $vendors['iVendorId'] = $vendors['vendorid'];
                    unset($vendors['vendorid']);
                    $vendors['dModifiedDate'] = date('Y-m-d h:i:s');
                    $vendors['iModifiedById'] = $userData['id'];

                    $this->data['id'] = $this->vendors_model->editVendors($vendors);
                } else {
                    unset($vendors['vendorid']);
                    $vendors['dCreatedDate'] = date('Y-m-d h:i:s');
                    $vendors['iCreatedById'] = $userData['id'];

                    $this->data['id'] = $this->vendors_model->addVendors($vendors);
                }

                if ($_FILES['iPictureURLId']['error'] == 0) {
                    $id = $this->data['id'];
                    $pathMain = VENDOR_COVER_PATH;
                    $pathThumb = VENDOR_COVER_THUMB_PATH;
                    $imageNameTime = time();
                    $filename = $imageNameTime . "_" . $id;
                    $result = do_upload("iPictureURLId", $pathMain, $filename);

                    if ($result['status'] == 1) {
                        $vendorDetail = $this->vendors_model->get_vendor($id);
                        if (isset($vendorDetail['iPictureURLId']) && $vendorDetail['iPictureURLId'] != "") {
                            @unlink($this->config->item('base_uploads') . "vendor" . DIRECTORY_SEPARATOR . "thumb" . DIRECTORY_SEPARATOR . $vendorDetail['iPictureURLId']);
                            @unlink($this->config->item('base_uploads') . "vendor" . DIRECTORY_SEPARATOR . $vendorDetail['iPictureURLId']);
                        }
                        $uploadedFileName = $result['upload_data']['file_name'];
                        resize_image($pathMain . $uploadedFileName, $pathThumb . $uploadedFileName, VENDOR_COVER_THUMB_WIDTH, VENDOR_COVER_THUMB_HEIGHT);
                        $this->db->where('iVendorId', $id);
                        $result = $this->db->update('vendordetails', array('iPictureURLId' => $uploadedFileName));

                        // RESIZE ORIGINAL IMAGE TO 1080x960
                        resize_image($pathMain . $uploadedFileName, $pathMain . $uploadedFileName, VENDOR_COVER_WIDTH, VENDOR_COVER_HEIGHT);
                    }
                }

                if ($_FILES['iLogoURLID']['error'] == 0) {
                    $pathMain = VENDOR_LOGO_PATH;
                    $pathThumb = VENDOR_LOGO_THUMB_PATH;
                    $imageNameTime = time();
                    $filename = $imageNameTime . "_" . $id;
                    $result = do_upload("iLogoURLID", $pathMain, $filename);

                    if ($result['status'] == 1) {
                        $vendorDetail = $this->vendors_model->get_vendor($id);
                        if (isset($vendorDetail['iLogoURLID']) && $vendorDetail['iLogoURLID'] != "") {
                            @unlink($this->config->item('base_uploads') . "vendor/thumb/" . $vendorDetail['iLogoURLID']);
                            @unlink($this->config->item('base_uploads') . "vendor/" . $vendorDetail['iLogoURLID']);
                        }
                        // RESIZE ORIGINAL IMAGE TO THUMB 150X150
                        $uploadedFileName = $result['upload_data']['file_name'];
                        resize_image($pathMain . $uploadedFileName, $pathThumb . $uploadedFileName, VENDOR_LOGO_THUMB_WIDTH, VENDOR_LOGO_THUMB_HEIGHT);
                        $this->db->where('iVendorId', $id);
                        $result = $this->db->update('vendordetails', array('iLogoURLID' => $uploadedFileName));

                        // RESIZE ORIGINAL IMAGE TO 500X500
                        resize_image($pathMain . $uploadedFileName, $pathMain . $uploadedFileName, VENDOR_LOGO_WIDTH, VENDOR_LOGO_HEIGHT);
                    }
                }

                redirect($this->config->item('base_url') . 'admin/vendors');
                exit;
            }
        }

        $data['eGender'] = array("" => "Select Gender", "Male" => "Male", "Female" => "Female");
        $data['countryData'] = $this->country_model->countryList();
        $data['stateData'] = $this->state_model->stateList();
//        $this->load->view('includes/header_common');
//        $this->load->view('admin/vendors/add',$data);
//        $this->load->view('includes/footer_common');
//      
        if ($id != "") {
            $vendorId = $this->uri->segment(4);
            $data['vendors'] = $this->vendors_model->get_vender_details($vendorId);
        } else {
            $data['vendors'] = null;
        }

        //Check Vendor service details exists or not 
        /* $vendorservices = $this->vendorservicecms_model->get_vscbyvendor($vendorId);
          if(empty($vendorservices)){
          $vendorservicefields = get_table_fields('vendorservicecms');
          $vendorservices= array();
          if (isset($vendorservicefields) && !empty($vendorservicefields)) {
          foreach ($vendorservicefields as $field) {
          $vendorservices[$field] = '';
          }
          }
          }
          $data['vendorservices'] = $vendorservices;
         */
        $data['id'] = $id;
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-fileupload/bootstrap-fileupload.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-fileupload/bootstrap-fileupload.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->content->view('admin/vendors/add', $data);
        $this->template->publish_admin();
    }

    public function edit() {

        $vendorId = $this->uri->segment(4);
        $this->add($vendorId);
//        
//        $data['vendors'] = $this->vendors_model->get_vender_details($vendorId);
//        $data['eGender'] = array("" => "Select Gender", "Male" => "Male", "Female" => "Female");
//        $data['countryData'] = $this->country_model->countryList();
//        $data['stateData'] = $this->state_model->stateList();
//        $this->load->library('form_validation');
//        $this->form_validation->set_rules('vFirstName', 'vFirstName', 'required');
//        $this->form_validation->set_rules('vLastName', 'vLastName', 'required');
//        $this->form_validation->set_rules('vOwnerName', 'vOwnerName', 'required');
//        $this->form_validation->set_rules('vFirmName', 'vFirmName', 'required');
//        $this->form_validation->set_rules('iPictureURLId', 'iPictureURLId', '');
//        $this->form_validation->set_rules('vUserName', 'vUserName', 'required');
//        $this->form_validation->set_rules('vPassword', 'vPassword', '');
//        $this->form_validation->set_rules('vcPassword', 'vcPassword', '');
//        $this->form_validation->set_rules('vEmail', 'vEmail', 'required');
//        $this->form_validation->set_rules('vPersonalEmail', 'vPersonalEmail', 'required');
//        $this->form_validation->set_rules('eGender', 'eGender', 'required');
//        $this->form_validation->set_rules('dBirthDate', 'dBirthDate', '');
//        $this->form_validation->set_rules('dAnniversaryDate', 'dAnniversaryDate', '');
//        $this->form_validation->set_rules('vCellPhone', 'vCellPhone', 'required');
//        $this->form_validation->set_rules('vPhoneNumber1', 'vPhoneNumber1', '');
//        $this->form_validation->set_rules('vAddress1', 'vAddress1', 'required');
//        $this->form_validation->set_rules('vAddress2', 'vAddress2', '');
//        $this->form_validation->set_rules('iCountryId', 'iCountryId', 'required');
//        $this->form_validation->set_rules('iStateId', 'iStateId', 'required');
//        $this->form_validation->set_rules('vCity', 'vCity', 'required');
//        $this->form_validation->set_rules('vPostCode', 'vPostCode', 'required');
//        $this->form_validation->set_rules('eStatus', 'eStatus', '');
//        if ($this->form_validation->run() == TRUE) {
//            if ($this->input->post()) {
//                $vendors = $this->input->post();
//                if (isset($vendors['vPassword']) && $vendors['vPassword'] == $vendors['vcPassword']) {
//                    $vendors['vPassword'] = md5($vendors['vPassword']);
//                    unset($vendors['vcPassword']);
//                }
//                if ($vendors['vPassword'] == "") {
//                    $vendors['vPassword'] = $vendors['vendors']['vPassword'];
//                    unset($vendors['vcPassword']);
//                }
//                if (isset($vendors['eStatus']) && $vendors['eStatus'] == "Active") {
//                    $vendors['eStatus'] = "Active";
//                } else {
//                    $vendors['eStatus'] = "InActive";
//                }
//                $vendors['dModifiedDate'] = date('Y-m-d h:i:s');
//                $vendors['iVendorId'] = $vendorId;
//                $this->vendors_model->editVendors($vendors);
////                $this->data['labeldsf'] = 'Update';
////                $this->session->set_flashdata('message', "Update");
//                redirect($this->data['base_url'] . 'admin/vendors');
//                exit;
//            }
//        }
//        $this->load->view('includes/header_common');
//        $this->load->view('admin/vendors/add', $data);
//        $this->load->view('includes/footer_common');
    }

    public function checkUnameExist() {
        if ($this->input->is_ajax_request()) {
            $uname = $this->input->post('uname');
            $flag = $this->vendors_model->checkuser_uname($uname);
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    public function checkUnameEditExist() {
        if ($this->input->is_ajax_request()) {
            $uname = $this->input->post('uname');
            $id = $this->input->post('id');
            $flag = $this->vendors_model->checkuser_uname_edit($id, $uname);
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    public function checkEmailExist() {
        if ($this->input->is_ajax_request()) {
            $email = $this->input->post('email_address');
            $flag = $this->vendors_model->checkuser_mail($email);
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    public function checkEmailEditExist() {
        if ($this->input->is_ajax_request()) {
            $email = $this->input->post('email_address');
            $id = $this->input->post('id');
            $flag = $this->vendors_model->checkuser_mail_edit($id, $email);
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    public function checkPersonalEmailExist() {
        if ($this->input->is_ajax_request()) {
            $email = $this->input->post('email_address');
            $flag = $this->vendors_model->checkuser_mail_Personal($email);
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    public function checkPersonalEmailEditExist() {
        if ($this->input->is_ajax_request()) {
            $email = $this->input->post('email_address');
            $id = $this->input->post('id');
            $flag = $this->vendors_model->checkuser_mail_Personal_edit($id, $email);
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    public function changepassword() {
        $session = $this->session->userdata;
        $this->form_validation->set_rules('OldPassword', 'old password', 'required');
        $this->form_validation->set_rules('NewPassword', 'new password', 'required|min_length[8]|max_length[25]');
        $this->form_validation->set_rules('ConfirmPassword', 'confirm password', 'required|matches[NewPassword]');

        if ($this->form_validation->run() == TRUE) {
		if ($this->input->post()) {
	                $sess = $session['admin_data'];
	                $emp = $this->vendors_model->get_vendor($sess['id']);
	                $oldPassword = fnEncrypt($this->input->post('OldPassword'), $this->config->item('mentorKey'));
	                if ($emp['Password'] != $oldPassword) {
	                    $this->session->set_flashdata('error', 'Please enter correct old password.');
	                    redirect('admin/vendors/changepassword');
	                } else {
	                    $newpwd = $this->input->post('NewPassword');
	
	                    $emp = $this->vendors_model->updatepwd($newpwd, $sess['id']);
	
	                    if ($emp) {
	                        $this->session->set_flashdata('success', 'You have successfully changed your password.');
	                        redirect('admin/vendors/changepassword');
	                    }
	                }
            	}            
        }
        $this->template->content->view('admin/vendors/changepassword');
        $this->template->publish_admin();
    }

    public function profile() {
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-fileupload/bootstrap-fileupload.css');
        $data = array();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('firstname', 'First Name', 'required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'required');
        $this->form_validation->set_rules('dBirthDate', 'dBirthDate', '');
        $this->form_validation->set_rules('dAnniversaryDate', 'dAnniversaryDate', '');

        $session = $this->session->userdata;
        $sess = $session['admin_data'];


        if ($this->form_validation->run() == TRUE) {
            if ($this->input->post()) {


                $this->vendors_model->setVendors($sess['id']);
                $this->session->set_flashdata('success', get_message(SUCCESS_UPDATED, 'vendors'));
                redirect('admin/vendors/profile');
            }
        }

        $arrCountry = $this->country_model->countryList();
        $country = array('' => 'Select Country');
        foreach ($arrCountry as $key => $value) {
            $country[$value['iCountryId']] = $value['vCountryName'];
        }

        $arrState = $this->state_model->stateList();
        $state = array('' => 'Select State');
        foreach ($arrState as $key => $value) {
            $state[$value['iStateId']] = $value['vStateName'];
        }

        $arrCity = $this->city_model->cityList();

        $fields = get_table_fields('vendordetails');
        $emp = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $emp[$field] = '';
            }
        }

        if ($sess['id']) {
            $emp = $this->vendors_model->get_vender_details($sess['id']);
        }

        $data['id'] = $sess['id'];
        $data['product'] = $emp;
        $data['emp'] = $emp;
        $data['country'] = $country;
        $data['state'] = $state;

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-fileupload/bootstrap-fileupload.css');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-fileupload/bootstrap-fileupload.js');

        $this->template->content->view('admin/vendors/profile', $data);
        $this->template->publish_admin();
    }

    public function loadState() {
        $loadId = $this->input->post("loadId");

        $result = $this->state_model->get_states($loadId);
        $HTML = "";

        if (count($result) > 0) {
            foreach ($result as $list) {
                $HTML.="<option value='" . $list['iStateId'] . "'>" . ucwords($list['vStateName']) . "</option>";
            }
        }
        echo $HTML;
    }

}