<?php

class Skill extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/skill_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // SKILL LISTING 
    //=========================================
    public function index() {

        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );

        $error['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Skill Name', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/skill/list', $error);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $this->datatables->select("SkillID, SkillName, Weight", FALSE)
                ->unset_column('SkillID')
                ->unset_column('Actions')
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/skill'), 'SkillID')
                ->from('skill')
                ->where('Status', '1');

        echo $this->datatables->generate();
    }

    //=========================================
    // SKILL ADD 
    //=========================================
    public function add($id = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('SkillName', 'Skill Name', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {

            $data = array(
                'SkillName' => $this->input->post('SkillName'),
                'Weight' => $this->input->post('Weight')
            );
            if ($id == 0) {
                $result = $this->db->insert('skill', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', 'Skill Added Successfully');
            } else {
                $this->db->where('SkillID', $id);
                $result = $this->db->update('skill', $data);
                $this->session->set_flashdata('success', 'Skill Updated Successfully');
            }
            redirect('admin/skill');
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('skill');
        $skill = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $skill[$field] = '';
            }
        }

        if ($id) {
            $skill = $this->skill_model->get_skill($id);
        }
        $data['id'] = $id;
        $data['skill'] = $skill;

        $this->template->content->view('admin/skill/add', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // SKILL EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    public function checkSkillExist() {
        if ($this->input->is_ajax_request()) {
            $SkillName = $this->input->post('SkillName');
            $SkillID = $this->input->post('SkillID');
            if ($SkillID == "") {
                $flag = $this->skill_model->checkSkill($SkillName);
            } else {
                $flag = $this->skill_model->checkSkillEdit($SkillName, $SkillID);
            }
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    //=========================================
    // SKILL DELETE 
    //=========================================
    public function delete($id) {
        $ds = $this->skill_model->delete_skill($id);
        $this->session->set_flashdata('success', 'Skill Delete Successfully');
        redirect('admin/skill');
    }

}
