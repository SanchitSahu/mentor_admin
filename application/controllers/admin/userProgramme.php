<?php

class UserProgramme extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/userProgramme_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // Programme LISTING 
    //=========================================
    public function index() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $res['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Programme Name', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/userprogramme/list', $res);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $this->datatables->select("ProgrammeID, ProgrammeName, Weight", FALSE)
                ->unset_column('ProgrammeID')
                ->unset_column('Actions')
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/userProgramme'), 'ProgrammeID')
                ->from('menteeprogrammes')
                ->where('Status', '1');

        echo $this->datatables->generate();
    }

    //=========================================
    // Programme ADD 
    //=========================================
    public function add($id = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('ProgrammeName', 'Programme Name', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {

            $data = array(
                'ProgrammeName' => $this->input->post('ProgrammeName'),
                'Weight' => $this->input->post('Weight')
            );
            if ($id == 0) {
                $result = $this->db->insert('menteeprogrammes', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', 'User Programme Added Successfully');
            } else {
                $this->db->where('ProgrammeID', $id);
                $result = $this->db->update('menteeprogrammes', $data);
                $this->session->set_flashdata('success', 'User Programme Updated Successfully');
            }
            redirect('admin/userProgramme');
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('menteeprogrammes');
        $userProgramme = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $userProgramme[$field] = '';
            }
        }

        if ($id) {
            $userProgramme = $this->userProgramme_model->get_programme($id);
        }
        $data['id'] = $id;
        $data['userProgramme'] = $userProgramme;

        $this->template->content->view('admin/userprogramme/add', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // Programme EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    public function checkProgrammeExist() {
        if ($this->input->is_ajax_request()) {
            $ProgrammeName = $this->input->post('ProgrammeName');
            $ProgrammeID = $this->input->post('ProgrammeID');
            if ($ProgrammeID == "") {
                $flag = $this->userProgramme_model->checkProgramme($ProgrammeName);
            } else {
                $flag = $this->userProgramme_model->checkProgrammeEdit($ProgrammeName, $ProgrammeID);
            }
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    //=========================================
    // Programme DELETE 
    //=========================================
    public function delete($id) {
        $ds = $this->userProgramme_model->delete_programme($id);
        $this->session->set_flashdata('success', 'User Programme Delete Successfully');
        redirect('admin/userProgramme');
    }

}
