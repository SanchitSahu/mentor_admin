<?php

class Mentors extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/mentors_model');
        $this->load->model('admin/emailtemplate_model');
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        //print_r($res);exit;
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            //print_r($x);exit;
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // MENTORS LISTING  
    //=========================================
    public function index() {
        $res['config'] = $this->settings_model->getConfig();
        if(isset($_GET['callFromApp'])) {
            $mentor_details = $this->mentors_model->getMentorDetail($_GET['loggedInMentor']);
            $res['callFromApp'] = $_GET['callFromApp'];
            $res['mentor_details'] = $mentor_details;
            $res['config'][0]->mentorName = 'Sub' . $res['config'][0]->mentorName;
        }
        $res['error'] = "";
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->mentorName . ' Name', $res['config'][0]->mentorName . ' Phone', $res['config'][0]->mentorName . ' Email', $res['config'][0]->mentorName . ' Role', $res['config'][0]->mentorName . ' Source', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');


        $this->template->content->view('admin/mentors/list', $res);
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function datatable() {
        $this->datatables->select('m.MentorID,mci.MentorContactInfoID, m.MentorName,mci.MentorPhone,mci.MentorEmail, CASE WHEN mci.IsSubadmin=1 THEN CONCAT(ur.userRoleName,"+Subadmin") ELSE ur.userRoleName END AS userRoleName,ms.MentorSourceName,m.Weight', false)
                ->unset_column('mci.MentorContactInfoID')
                ->unset_column('Actions')
                ->unset_column('m.MentorID')
                ->add_column('Actions', get_Edit_DeleteMentors_Buttons('$1', 'admin/mentors'), 'm.MentorID')
                ->from('mentorcontactinfo mci')
                ->join('mentor m', 'm.MentorID = mci.MentorID', 'left')
                ->join('usertorole utr', 'utr.MentorID = mci.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->join('usertosource us', 'us.MentorID = mci.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                ->where('m.Status', '1')
                ->group_by('m.MentorID');
                
        if(isset($_GET['callFromApp']) && $_GET['callFromApp'] == true) {
            $this->datatables->where('mci.AddedByMentorId', $_GET['userId']);
        }
        echo $this->datatables->generate();
    }

    //=========================================
    // MENTORS ADD  
    //=========================================
    public function add($id = '') {
        $data['config'] = $this->settings_model->getConfig();
        $this->form_validation->set_rules('MentorName', $data['config'][0]->mentorName . ' Name', 'required');
        //$this->form_validation->set_rules('MentorPhone', $data['config'][0]->mentorName . ' Phone', 'required');
        $this->form_validation->set_rules('MentorEmail', $data['config'][0]->mentorName . ' Email', 'required');
        //$this->form_validation->set_rules('userRoleID', $data['config'][0]->mentorName.' Role', 'required');
        //$this->form_validation->set_rules('MentorSourceID', $data['config'][0]->mentorName.' Source', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {
            $getDeparment = $this->mentors_model->getDeparment();

            if ($id == 0) {
                $mentorName = $this->input->post('MentorName');
                if (isset($mentorName) && $mentorName != "") {
                    $data2 = array(
                        'MentorName' => $this->input->post('MentorName'),
                        'Weight' => $this->input->post('Weight')
                    );
                }
                $nameresult = $this->db->insert('mentor', $data2);
                $filename = "";
                if ($nameresult) {
                    if ($_FILES['photo']['error'] == 0) {
                        $pathMain = $this->config->item('base_images');
                        $pathThumb = $this->config->item('base_images');
                        $imageNameTime = time();
                        // $imageNamePrefix = "tmp_";
                        //$orgFileName =  $imageNamePrefix.$imageNameTime; 
                        $path = $_SERVER['DOCUMENT_ROOT'] . "/mentor/assets/admin/images/admin/" . $imageNameTime . ".png";
                        $filename = $imageNameTime . ".png";
                        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $path)) {
                            $status = 1;
                        } else {
                            $status = 0;
                        }

                        @unlink($pathMain . $uploadedFileName);
                    }
                }

                $mentorid = $this->db->insert_id();
                $AccessToken = mt_rand(100000, 999999);
                $contactdata = array(
                    'MentorID' => $mentorid,
                    'MentorPhone' => $this->input->post('MentorPhone'),
                    'MentorEmail' => $this->input->post('MentorEmail'),
                    'Password' => fnEncrypt($AccessToken, $this->config->item('mentorKey')),
                    //'Weight' => $this->input->post('Weight'),
                    'ExpiryDate' => date("Y-m-d", strtotime(date("Y-m-d") . "+7 day")),
                    'DisableSMS'  => $this->input->post('MentorPhoneSMS'),
                    'IsSubadmin' => $this->input->post('MentorIsSubadmin')
                    //'MentorImage' => $filename
                );
                if($filename != '') {
                    $contactdata['MentorImage'] = $filename;
                }
                if(isset($_POST['IsSubmentor'])) {
                    $contactdata['IsSubmentor'] = $_POST['IsSubmentor'];
                }
                $result = $this->db->insert('mentorcontactinfo', $contactdata);
                $id = $this->db->insert_id();
                
                $newRegister = array(
                    'UserID' => $mentorid,
                    'UserType' => 0,
                    'DateRegistered' => date('Y-m-d H:i:s'),
                    'DateUpdated' => date('Y-m-d H:i:s'),
                    'UserName' => $mentorName,
                    'UserEmail' => $this->input->post('MentorEmail')
                );
                $this->db->insert('newregistrations',$newRegister);
                
                $usertorole = array(
                    'MentorID' => $mentorid,
                    'userRoleID' => $this->input->post('userRoleID')
                );
                $result = $this->db->insert('usertorole', $usertorole);

                $usertosource = array(
                    'MentorID' => $mentorid,
                    'MentorSourceID' => $this->input->post('MentorSourceID')
                );
                $result = $this->db->insert('usertosource', $usertosource);
                // SEND EMAIL TO MENTOR
                $emailtemplate = $this->emailtemplate_model->get_emailtempate_details(1);
                //print_r($emailtemplate);exit;
                $host = $_SERVER['HTTP_HOST'];
                $subdomain = strstr(str_replace("www.", "", $host), ".", true);
                if ($emailtemplate) {
                    $emailData = array();
                    $emailData['toEmail'] = $this->input->post('MentorEmail');

                    $arrSearch = array('##FIRST_NAME##', '##EMAIL_ADDRESS##', '##ACCESS_TOKEN##', '##USER_TYPE##', "##SCHOOLNAME##", "##DEPARTMENTNAME##", "##MENTOR##", "##MENTEE##");
                    $arrReplace = array($this->input->post('MentorName'), $this->input->post('MentorName'), $AccessToken, 'Mentor', $subdomain, $getDeparment['DepartmentName'], $data['config'][0]->mentorName, $data['config'][0]->menteeName);

                    $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                    $emailData['subject'] = $subject;

                    $body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                    $emailData['body'] = $body;

                    send_mail($emailData);
                }
                // END SEND EMAIL TO MENTOR
                $this->session->set_flashdata('success', $data['config'][0]->mentorName . ' Added Successfully');
            } else {
                $mentorName = $this->input->post('MentorName');
                $mentorID = $this->input->post('MentorID');
                if (isset($mentorName) && $mentorName != "") {
                    $data2 = array(
                        'MentorName' => $mentorName,
                        'Weight' => $this->input->post('Weight')
                    );
                }

                $this->db->where('MentorID', $mentorID);
                $nameresult = $this->db->update('mentor', $data2);

                if ($nameresult) {
                    $filename = "";
                    if ($_FILES['photo']['error'] == 0) {
                        $pathMain = $this->config->item('base_images');
                        $pathThumb = $this->config->item('base_images');
                        $imageNameTime = time() ;
                        $path = $_SERVER['DOCUMENT_ROOT'] . "/mentor/assets/admin/images/admin/" . $imageNameTime . ".png";
                        $filename = $imageNameTime . ".png";
                        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $path)) {
                            $status = 1;
                        } else {
                            $status = 0;
                        }
                       

                        @unlink($pathMain . $uploadedFileName);
                    }
                }
                $contactdata = array(
                    'MentorID' => $mentorID,
                    'MentorPhone' => $this->input->post('MentorPhone'),
                    //'Weight' => $this->input->post('Weight'),
                    'MentorEmail' => $this->input->post('MentorEmail'),
                    'DisableSMS'  => $this->input->post('MentorPhoneSMS'),
                    'IsSubadmin' => $this->input->post('MentorIsSubadmin')
                );
                if($filename != '') {
                    $contactdata['MentorImage'] = $filename;
                }
                $this->db->where('MentorID', $id);
                $result = $this->db->update('mentorcontactinfo', $contactdata);

                $sql = $this->db->get_where('mentorcontactinfo', array('MentorEmail' => $this->input->post('MentorEmail')));
                $mentorData = $sql->row_array();

                $isRole = $this->mentors_model->get_mentorsRole($mentorID);
                if (empty($isRole)) {
                    $usertorole = array(
                        'MentorID' => $this->input->post('MentorID'),
                        'userRoleID' => $this->input->post('userRoleID')
                    );
                    $result = $this->db->insert('usertorole', $usertorole);
                } else {
                    $usertorole = array(
                        'MentorID' => $this->input->post('MentorID'),
                        'userRoleID' => $this->input->post('userRoleID')
                    );
                    $this->db->where('userToRoleID', $isRole['userToRoleID']);
                    $result = $this->db->update('usertorole', $usertorole);
                }

                $isSource = $this->mentors_model->get_mentorsSource($mentorID);
                if (empty($isSource)) {
                    $usertosource = array(
                        'MentorID' => $this->input->post('MentorID'),
                        'MentorSourceID' => $this->input->post('MentorSourceID')
                    );
                    $result = $this->db->insert('usertosource', $usertosource);
                } else {
                    $usertosource = array(
                        'MentorID' => $this->input->post('MentorID'),
                        'MentorSourceID' => $this->input->post('MentorSourceID')
                    );
                    $this->db->where('userToSourceID', $isSource['userToSourceID']);
                    $result = $this->db->update('usertosource', $usertosource);
                }
                $this->session->set_flashdata('success', $data['config'][0]->mentorName . ' Updated Successfully');
            }

            if(isset($_POST['callFromAppHidden'])) {
                redirect('admin/mentors/add?callFromApp=true');
            } else {
                redirect('admin/mentors');
            }
        }
        
        if(isset($_GET['callFromApp'])) {
            $data['callFromApp'] = $_GET['callFromApp'];
            $mentor_details = $this->mentors_model->getMentorDetail($_GET['loggedInMentor']);
            $data['mentor_details'] = $mentor_details;
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-fileupload/bootstrap-fileupload.css');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-fileupload/bootstrap-fileupload.js');

        $fields = get_table_fields('mentorcontactinfo');
        $mentors = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $mentors[$field] = '';
            }
            $mentors['MentorName'] = '';
            $mentors['MentorID'] = '';
            $mentors['MentorImage'] = '';
            $mentors['userRoleID'] = '';
            $mentors['MentorSourceID'] = '';
            $mentors['IsSubadmin'] = '';
        }
        if ($id) {
            $mentors = $this->mentors_model->get_mentors($id);
            //echo "<pre>";print_r($mentors);echo "</pre>";
        }
        $data['id'] = $id;
        $data['mentors'] = $mentors;
        $data['roleData'] = $this->mentors_model->roleData();
        $data['sourceData'] = $this->mentors_model->sourceData();
        $this->template->content->view('admin/mentors/add', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // MENTORS EDIT  
    //=========================================
    public function edit($id) {
        $this->add($id);
    }
    
    public function send_test_mail($id='') {
        echo $id;
        print_r($_REQUEST);
        
        
        $emailData = array();
        if(isset($id) && $id != '') {
            $emailData['toEmail'] = 'sanchit@solulab.com';
        } else if(isset($_REQUEST['Digits']) && $_REQUEST['Digits'] == '3') {
            $emailData['toEmail'] = 'sanchit.sahu03@gmail.com';
        }
        if($emailData['toEmail'] !=''){
            $emailData['subject'] = "Message from twilio";
            $emailData['body'] = "Test IVR message from twilio";
    
            send_mail($emailData);
        }
    }

    //=========================================
    // MENTORS DELETE 
    //=========================================
    public function deletes($id) {
        $this->mentors_model->delete_mentors($id);
        $this->session->set_flashdata('success', 'Mentors Delete Successfully');
        redirect('admin/mentors');
    }

    //=========================================
    // MENTORS DELETE 
    //=========================================
    public function delete($id) {
        $this->mentors_model->delete_mentors($id);

        $this->session->set_flashdata('success', get_message(SUCCESS_DELETED, 'mentor'));
        redirect('admin/mentors');
    }

    //=========================================
    // CHECK EMAIL 
    //=========================================
    public function checkEmailExist() {
        if ($this->input->is_ajax_request()) {
            $MentorEmail = $this->input->post('MentorEmail');
            $MentorID = $this->input->post('MentorID');
            if ($MentorID == "") {
                $flag = $this->mentors_model->checkuser_mail($MentorEmail);
            } else {
                $flag = $this->mentors_model->checkuser_mail_edit($MentorID, $MentorEmail);
            }
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    public function checkMentorName() {
        if ($this->input->is_ajax_request()) {
            $MentorName = $this->input->post('MentorName');
            $MentorID = $this->input->post('MentorID');
            if ($MentorID == "") {
                $flag = $this->mentors_model->checkMentorName($MentorName);
            } else {
                $flag = $this->mentors_model->checkMentorNameEdit($MentorID, $MentorName);
            }
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

}
