<?php

class Comments extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/comments_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    public function listAccept() {

        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );

        $error['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Response Text', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/comments/listAccept', $error);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function acceptDatatable() {
        $this->datatables->select("ResponseID, ResponseName, Weight", FALSE)
                ->unset_column('ResponseID')
                ->unset_column('Actions')
                ->add_column('Actions', get_Edit_Delete_Buttons_Accept('$1', 'admin/comments'), 'ResponseID')
                ->from('accept_responses')
                ->where('Status', '1');

        echo $this->datatables->generate();
    }

    //=========================================
    // comments ADD 
    //=========================================
    public function addAccept($id = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('ResponseName', 'Response Name', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {

            $data = array(
                'ResponseName' => $this->input->post('ResponseName'),
                'Weight' => $this->input->post('Weight')
            );
            if ($id == 0) {
                $result = $this->db->insert('accept_responses', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', 'Response Text Added Successfully');
            } else {
                $this->db->where('ResponseID', $id);
                $result = $this->db->update('accept_responses', $data);
                $this->session->set_flashdata('success', 'Response Text Updated Successfully');
            }
            redirect('admin/comments/listAccept');
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('accept_responses');
        $comments = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $comments[$field] = '';
            }
        }

        if ($id) {
            $comments = $this->comments_model->get_acceptCommnets($id);
        }
        $data['id'] = $id;
        $data['comments'] = $comments;

        $this->template->content->view('admin/comments/addAccept', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // comments EDIT 
    //=========================================
    public function editAccept($id) {
        $this->addAccept($id);
    }

    //=========================================
    // comments DELETE 
    //=========================================
    public function deleteAccept($id) {
        $ds = $this->comments_model->delete_accept($id);
        $this->session->set_flashdata('success', 'Response Text Delete Successfully');
        redirect('admin/comments/listAccept');
    }

    /* reject comment */

    public function listReject() {

        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );

        $error['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Response Text', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/comments/listReject', $error);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function rejectDatatable() {
        $this->datatables->select("ResponseID, ResponseName, Weight", FALSE)
                ->unset_column('ResponseID')
                ->unset_column('Actions')
                ->add_column('Actions', get_Edit_Delete_Buttons_Reject('$1', 'admin/comments'), 'ResponseID')
                ->from('decline_responses')
                ->where('Status', '1');

        echo $this->datatables->generate();
    }

    //=========================================
    // comments ADD 
    //=========================================
    public function addReject($id = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('ResponseName', 'Response Name', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {

            $data = array(
                'ResponseName' => $this->input->post('ResponseName'),
                'Weight' => $this->input->post('Weight')
            );
            if ($id == 0) {
                $result = $this->db->insert('decline_responses', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', 'Response Text Added Successfully');
            } else {
                $this->db->where('ResponseID', $id);
                $result = $this->db->update('decline_responses', $data);
                $this->session->set_flashdata('success', 'Response Text Updated Successfully');
            }
            redirect('admin/comments/listReject');
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('decline_responses');
        $comments = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $comments[$field] = '';
            }
        }

        if ($id) {
            $comments = $this->comments_model->get_rejectCommnets($id);
        }
        $data['id'] = $id;
        $data['comments'] = $comments;

        $this->template->content->view('admin/comments/addReject', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // comments EDIT 
    //=========================================
    public function editReject($id) {
        $this->addReject($id);
    }

    //=========================================
    // comments DELETE 
    //=========================================
    public function deleteReject($id) {
        $ds = $this->comments_model->delete_reject($id);
        $this->session->set_flashdata('success', 'Response Text Delete Successfully');
        redirect('admin/comments/listReject');
    }

    public function checkResponseExists() {
        if ($this->input->is_ajax_request()) {
            $ResponseName = $this->input->post('ResponseName');
            $ResponseID = $this->input->post('ResponseID');
            $Response = $this->input->post('Response');
            if ($ResponseID == "") {
                $flag = $this->comments_model->checkMeetingType($Response, $ResponseName);
            } else {
                $flag = $this->comments_model->checkMeetingTypeEdit($Response, $ResponseName, $ResponseID);
            }
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

}
