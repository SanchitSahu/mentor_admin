<?php

class Subtopic extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/subtopic_model');
        $this->load->model('admin/topic_model');
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        //print_r($res);exit;
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            //print_r($x);exit;
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // SUBTOPIC LISTING 
    //=========================================
    public function index() {

        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $error['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Topic Description', 'Sub Topic Description', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/subtopic/list', $error);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $this->datatables->select("st.SubTopicID, t.TopicID, t.TopicDescription, st.SubTopicDescription, st.Weight", FALSE)
                ->unset_column('st.SubTopicID')
                ->unset_column('t.TopicID')
                // ->add_column('Actions', get_buttons('$1','admin/subtopic'),'st.SubTopicID')
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/subtopic'), 'st.SubTopicID')
                ->from('subtopic st')
                ->where('st.Status', '1')
                ->join('topic t', 't.TopicID = st.TopicID', 'left');

        echo $this->datatables->generate();
    }

    //=========================================
    // SUBTOPIC ADD 
    //=========================================
    public function add($id = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('TopicID', 'Topic Description', 'required');
        $this->form_validation->set_rules('SubTopicDescription', 'Sub Topic Description', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {

            $data = array(
                'TopicID' => $this->input->post('TopicID'),
                'SubTopicDescription' => $this->input->post('SubTopicDescription'),
                'Weight' => $this->input->post('Weight')
            );
            if ($id == 0) {
                $result = $this->db->insert('subtopic', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', 'Sub Topic Added Successfully');
            } else {
                $this->db->where('SubTopicID', $id);
                $result = $this->db->update('subtopic', $data);
                $this->session->set_flashdata('success', 'Sub Topic Updated Successfully');
            }
            redirect('admin/subtopic');
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('subtopic');
        $subtopic = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $subtopic[$field] = '';
            }
        }

        if ($id) {
            $subtopic = $this->subtopic_model->get_subtopic($id);
        }
        $data['id'] = $id;
        $data['subtopic'] = $subtopic;
        $data['topicData'] = $this->topic_model->topicList();
        $this->template->content->view('admin/subtopic/add', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // SUBTOPIC EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    public function checkSubtopicExist() {
        if ($this->input->is_ajax_request()) {
            $SubTopicDescription = $this->input->post('SubTopicDescription');
            $SubTopicID = $this->input->post('SubTopicID');
            $TopicID = $this->input->post('TopicID');
            if ($SubTopicID == "") {
                $flag = $this->subtopic_model->checkSubtopic($SubTopicDescription, $TopicID);
            } else {
                $flag = $this->subtopic_model->checkSubtopicEdit($SubTopicDescription, $TopicID, $SubTopicID);
            }
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    //=========================================
    // SUBTOPIC DELETE 
    //=========================================
    public function delete($id) {
        $this->subtopic_model->delete_subtopic($id);
        $this->session->set_flashdata('success', 'Subtopic Delete Successfully');
        redirect('admin/subtopic');
    }

}
