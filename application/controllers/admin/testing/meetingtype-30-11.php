<?php

class Meetingtype extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/meetingtype_model');
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if(!loginCheck()){redirect('admin');}
    }

    //=========================================
    // Meetingtype LISTING 
    //=========================================
    public function index() {

        $tmpl = array ( 
            'table_open'  => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open'  => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">' 
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('Meeting Type Name', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');
                
        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');
       
        $this->template->content->view('admin/meetingtype/list');
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable()
    {
        $this->datatables->select("MeetingTypeID, MeetingTypeName, Weight",FALSE)
        ->unset_column('MeetingTypeID')
        ->add_column('Actions', get_Edit_Delete_Buttons('$1','admin/meetingtype'),'MeetingTypeID')
        ->from('meetingplace')
        ->where('Status','1');
		$this->db->order_by("Weight", "ASC");
        
        echo $this->datatables->generate();
    }


    //=========================================
    // Meetingtype ADD 
    //=========================================
    public function add($id = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('MeetingTypeName', 'Meeting Type Name', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {
            
            $data = array(
                'MeetingTypeName'  => $this->input->post('MeetingTypeName'),
                'Weight'  => $this->input->post('Weight')
            );
            if ($id == 0) {
                $result = $this->db->insert('meetingplace', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', 'Meeting Type Added Successfully');
            }else{
                $this->db->where('MeetingTypeID', $id);
                $result = $this->db->update('meetingplace', $data);
                $this->session->set_flashdata('success', 'Meeting Type Updated Successfully');
            }
            redirect('admin/meetingtype');
        }
         
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');
        
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');
        
       
        $fields = get_table_fields('meetingplace');
        $meetingtype = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $meetingtype[$field] = '';
            }
        }
        
        if ($id) {
             $meetingtype = $this->meetingtype_model->get_meetingtype($id);
        }
        $data['id'] = $id;
        $data['meetingtype'] = $meetingtype;
        
        $this->template->content->view('admin/meetingtype/add', $data);
        $this->template->publish_admin();

    }
    
    //=========================================
    // Meetingtype EDIT 
    //=========================================
    public function edit($id){ 
       $this->add($id);
    }
    
    //=========================================
    // Delete Meetingtype 
    //=========================================
    public function delete($id){ 
         $this->meetingtype_model->delete_meetingtype($id);
         $this->session->set_flashdata('success', 'Meetingtype Delete Successfully');
         redirect('admin/meetingtype');
         
    }

}