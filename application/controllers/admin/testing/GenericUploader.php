<?php
function genericUploader()
{
    $tbl_name      = $_POST['tableName'];
    $imageNameTime = time();
    $csvfile       = $_SERVER['DOCUMENT_ROOT'] . "/mentor/assets/admin/csv/" . $imageNameTime . ".csv";
    $filename      = $imageNameTime . ".csv";
    //print("<pre>[".__FILE__.' @ '. __LINE__."]: _FILES[userfile][name]='".$_FILES['userfile']['name']."'<br/></pre>\n");
    //print("<pre>[".__FILE__.' @ '. __LINE__."]: tbl_name='".$tbl_name."'<br/></pre>\n");
    //print("<pre>[".__FILE__.' @ '. __LINE__."]: imageNameTime='".$imageNameTime."'<br/></pre>\n");
    //print("<pre>[".__FILE__.' @ '. __LINE__."]: csvfile='".$csvfile."'<br/></pre>\n");
    //print("<pre>[".__FILE__.' @ '. __LINE__."]: filename='".$filename."'<br/></pre>\n");
    //exit;
    
    if (move_uploaded_file($_FILES["userfile"]["tmp_name"], $csvfile)) {
        //$data = array('upload_data' => $this->upload->data());
        $this->load->library('csvreader');
        
        list($cvskeys,$result)    = $this->csvreader->parse_file($csvfile);
        //print("<pre>[".__FILE__.' @ '. __LINE__."]: Parser output=<br/>");print_r($result);print("</pre>\n");
        $errorData = $result['error'];
        $values    = array_values($result['content']);
        //print("<pre>[".__FILE__.' @ '. __LINE__."]: values=<br/>");print_r($values);print("</pre>\n");
        
        $data['csvData'] = $values;
        
        $csv_headerss = array_keys($data['csvData'][0]);
        //print("<pre>[".__FILE__.' @ '. __LINE__."]: csv_headerss=<br/>");print_r($csv_headerss);print("</pre>\n");
        
        $table_headers = $this->bulkupload_model->getGenericHeaders($tbl_name);
        //print("<pre>[".__FILE__.' @ '. __LINE__."]: table_headers=<br/>");print_r($table_headers);print("</pre>\n");
        
        foreach ($table_headers as $table_header) {
            $columns[$table_header->COLUMN_NAME] = $table_header->COLUMN_NAME;
        }
        
        //print("<pre>[".__FILE__.' @ '. __LINE__."]: columns=<br/>");print_r($columns);print("</pre>\n");
        array_shift($columns);
        $invalid_headers = array_diff_key($csv_headerss, $columns);
        //print("<pre>[".__FILE__.' @ '. __LINE__."]: invalid_headers=<br/>");print_r($invalid_headers);print("</pre>\n");
        
        if (!empty($invalid_headers) || (count($csv_headerss) > count($columns))) {
            $this->issue_incompatible_csv_input_error_msg($columns, $csv_headerss, __FILE__, __LINE__);
            redirect('admin/' . $_POST['urlPath']);
            
        } else {
            
            $result = array_values($result);
            
            //print("<pre>[".__FILE__.' @ '. __LINE__."]: result=<br/>");print_r($result);print("</pre>\n");
            //print("<pre>[".__FILE__.' @ '. __LINE__."]: tbl_name='".$tbl_name."'<br/></pre>\n");
            $res = $this->bulkupload_model->saveDataFromCSV($result, $tbl_name);
            //print("<pre>[".__FILE__.' @ '. __LINE__."]: res='".vardump($res)."'<br/></pre>\n");
            
            $error = 'Table Data Successfully added';
            $this->session->set_flashdata('success', $error);
            $type  = 'xls';
            $title = 'Table_upload_status';
            //echo "<pre>";print_r($result);
            
            $csv_reader = new CSVReader;
            list($csvkeys, $csvdata) = $csv_reader->parse_file($csvfile);
            
            $csv = $csv_array['VALUES'];
            foreach ($csv as $key => $csv_val) {
                foreach ($result as $res) {
                    $newArray = array_keys($csv_val); //get keys of any table dynamically
                    $oldArray = array_keys($res); //get keys of any table dynamically
                    if ($csv_val[$newArray[0]] == $res[$newArray[0]]) {
                        $csv[$key]['success'] = "uploaded successfully";
                        $new2[$key]           = $csv[$key]; // catch it
                        break;
                    } else {
                        $csv[$key]['success'] = "Not uploaded";
                        $new2[$key]           = $csv[$key]; // catch it	
                    }
                }
            }
            
            $k = 0;
            foreach ($new2[$k] as $r_key => $r_value) {
                $k++;
            }
            $type  = 'xls';
            $title = 'Data_upload_status';
            
            $this->export_generic($new2, $dataHeader, $type, NULL, $title, $_POST['urlPath']);
            
        }
        redirect('admin/' . $_POST['urlPath']);
    } else {
        $error = "Failed to upload " . $_POST['tablename'] . "(1)";
        $this->session->set_flashdata('error', $error);
        redirect('admin/' . $_POST['urlPath']);
    }
}

