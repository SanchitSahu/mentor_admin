	function genericUploader()
	{
		$tbl_name = $_POST['tableName'];
		$imageNameTime = time();
		$cvsfile = $_SERVER['DOCUMENT_ROOT']."/mentor/assets/admin/csv/".$imageNameTime.".csv";
		$filename	=	$imageNameTime.".csv";
//print("<pre>[".__FILE__.' @ '. __LINE__."]: _FILES[userfile][name]='".$_FILES['userfile']['name']."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: tbl_name='".$tbl_name."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: imageNameTime='".$imageNameTime."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: csvfile='".$csvfile."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: filename='".$filename."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: _FILES=<br/></pre>");
//print_r($_FILES);
//exit;
		if (move_uploaded_file($_FILES["userfile"]["tmp_name"], $csvfile)) {
			//$data = array('upload_data' => $this->upload->data());
			$this->load->library('csvreader');
			
			$result =   $this->csvreader->parse_file($cvsfile);
//print("<pre>[".__FILE__.' @ '. __LINE__."]: result=<br/></pre>");
//print_r($result);
			$errorData	=	$result['error'];
			$result = array_values($result['VALUES']);
			
			$data['csvData'] =  $result;
			
			$csvheaders = $data['csvData'][0];
			
			$csvcolheaders = array();
			foreach($csvheaders as $csvheaderkey => $value)
			{
				$csvcolheaders[] = $csvheaderkey;
			}
			
			$table_headers = $this->bulkupload_model->getGenericHeaders($tbl_name);
			$revertfile = $result;
			
			foreach($table_headers as $table_header)
			{
				$columns[$table_header->COLUMN_NAME] = 	$table_header->COLUMN_NAME;
			}
			
			$revertHeaders = $columns;
			array_shift($columns);
			$invalid_headers = array_diff_key ( $csvheaders , $columns );
//echo "<pre>";print("invalid_headers");
//echo '<pre>';print_r($invalid_headers);

			if(!empty($invalid_headers) || (count($csvheaders) > count($columns))){
				$this->issue_incompatible_csv_input_error_msg($columns, $csvheaders, __FILE__, __LINE__);
				redirect('admin/'.$_POST['urlPath']);

			}else{
				
				$result = array_values($result);
			
				//echo '<pre>';print_r($result);
				//echo '<pre>';print_r($revertfile);
				$k=0;
				foreach($revertfile[$k] as $r_key=>$r_value)
				{
					$revertHeaders[$k] = $r_key;
					$k++;	
				}
				//echo '<pre>';print_r($revertHeaders);
				//echo count($result);exit;
				$res = $this->bulkupload_model->saveDataFromCSV($result,$tbl_name);
				
				$error = 'Table Data Successfully added';
				$this->session->set_flashdata('success', $error);
				$type= 'xls';
				$title = 'Table_upload_status';
				//echo "<pre>";print_r($result);
				
				$csv_reader = new CSVReader;
				$csv_array = $csv_reader->parse_file($csvfile);
				$csv = $csv_array['VALUES'];
print("<pre>[".__FILE__.' @ '. __LINE__."]: $csv=<br/></pre>");print_r($csv);
exit;
				foreach($csv as $key => $csv_val){
					foreach($result as $res){
						$newArray = array_keys($csv_val);	//get keys of any table dynamically
						$oldArray = array_keys($res);		//get keys of any table dynamically
						if($csv_val[$newArray[0]] == $res[$newArray[0]]){
							//echo $csv_val['MeetingTypeName'];
							//echo $res['MeetingTypeName']."<br>";
							$csv[$key]['success'] = "uploaded successfully";
							$new2[$key] = $csv[$key];	// catch it
							break;
						}else{
							$csv[$key]['success'] = "Not uploaded";
							$new2[$key] = $csv[$key];	// catch it	
						}
					}
				}
				
				//print_r($new2);;exit;	
				
				$k=0;
			  foreach($new2[$k] as $r_key=>$r_value)
			  {
			   $dataHeader[$k] = $r_key;
			   $k++; 
			  }
//print_r($dataHeader);
				$type= 'xls';
				$title = 'Data_upload_status';
				
				$this->export_generic($new2,$dataHeader,$type,NULL,$title,$_POST['urlPath']);

			}
//	print_r($data['csvData']);
			
			redirect('admin/'.$_POST['urlPath']);

			//$this->load->view('master/uploadSuccess', $data);
		} else {
			$error = "Failed to upload " . $_POST['tablename']."(1)";
			$this->session->set_flashdata('error', $error);
			redirect('admin/'.$_POST['urlPath']);
		}			
	}
