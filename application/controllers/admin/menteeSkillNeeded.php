<?php

//error_reporting(E_ALL);
//ini_set("display_errors", "ON");

class MenteeSkillNeeded extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/clientskillneeded_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        //print_r($res);exit;
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            //print_r($x);exit;
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // SKILL LISTING 
    //=========================================
    public function index() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );

        $res['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->menteeName . ' Name', 'Skill Name', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/clientSkillNeeded/list', $res);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
		if($this->input->post('sSearch') != ""){
			$this->datatables->select("mm.MenteeName,ss.SkillName,mss.Weight", FALSE)
				->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/menteeSkillNeeded'), 'mss.MenteeID')
                ->from('mentee mm')
				->join('menteeskill mss', 'mm.MenteeID = mss.MenteeID', 'left')
                ->join('skill ss', 'ss.SkillID = mss.SkillID', 'left')
                ->where('mss.Status', '1')
				->group_by('mss.MenteeID');
		} else {
			$this->datatables->select("mm.MenteeName,GROUP_CONCAT(' ', ss.SkillName) as SkillName,mss.Weight", FALSE)
				->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/menteeSkillNeeded'), 'mss.MenteeID')
                ->from('mentee mm')
				->join('menteeskill mss', 'mm.MenteeID = mss.MenteeID', 'left')
                ->join('skill ss', 'ss.SkillID = mss.SkillID', 'left')
                ->where('mss.Status', '1')
				->group_by('mss.MenteeID');
		}
		echo $this->datatables->generate();
    }

    //=========================================
    // SKILL ADD 
    //=========================================
    public function add($id = '') {
        $this->load->library('form_validation');
        // $this->form_validation->set_rules('MenteeProgrammeName', 'Programme Name', 'required');
        //$this->form_validation->set_rules('Weight', 'Weight', '');
        // if ($this->form_validation->run() === TRUE) {
	
        if ($this->input->post()) {
			$skill_array = array();
			$mentee = $this->input->post('mentee');
			$skill_array = $this->input->post('skills');	
			if ($mentee != "0") {
            if (!empty($skill_array)) {
                if ($id == 0) {
                    $id = $this->input->post('mentee');
                    $this->db->where('MenteeID', $id);
                    $this->db->delete('menteeskill');


                    foreach ($skill_array as $skill) {
                        $data = array("MenteeID" => $id, "SkillID" => $skill, "Status" => 1, "Weight" => 1000);
                        $this->db->where('MenteeID', $id);
                        $result = $this->db->insert('menteeskill', $data);
                    }
                    $this->session->set_flashdata('success', 'Skill Added Successfully');
                } else {

                    $this->db->where('MenteeID', $id);
                    $this->db->delete('menteeskill');


                    foreach ($skill_array as $skill) {
                        $data = array("MenteeID" => $id, "SkillID" => $skill, "Status" => 1, "Weight" => 1000);
                        $this->db->where('MenteeID', $id);
                        $result = $this->db->insert('menteeskill', $data);
                    }
                    $this->session->set_flashdata('success', 'Skill Updated Successfully');
                }
                redirect('admin/menteeSkillNeeded');
            } else {
                $this->session->set_flashdata('error', 'Select atleast one skill');
				if($id != "")
					redirect('admin/menteeSkillNeeded/edit/'.$id);
				else
					redirect('admin/menteeSkillNeeded/add');
            }
			
			} else {
				$res['config'] = $this->settings_model->getConfig();
                $this->session->set_flashdata('error', 'No'.' '.$res['config'][0]->menteeName.' '.'Selected');
                if($id != "")
					redirect('admin/menteeSkillNeeded/edit/'.$id);
				else
					redirect('admin/menteeSkillNeeded/add');
            }
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('menteeprogrammes');
        $userSource = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $userSource[$field] = '';
            }
        }
        $client_skills = "";
        if ($id) {
            $client_skills = $this->clientskillneeded_model->get_clientSkills($id);
        }
        $data['id'] = $id;
        $data['clientskillneeded'] = $client_skills;
        $data['skills'] = $this->clientskillneeded_model->get_Skills();
        $data['mentee'] = $this->clientskillneeded_model->get_Mentee();
        $this->template->content->view('admin/clientSkillNeeded/add', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // user Roles EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    //=========================================
    // user Roles DELETE 
    //=========================================
    public function delete($id) {
        $ds = $this->clientskillneeded_model->delete_client_skill($id);
        $this->session->set_flashdata('success', 'Client Skill Delete Successfully');
        redirect('admin/menteeSkillNeeded');
    }
	
	public function ajaxGetMenteeSkills(){
		$id = $this->input->post('id');
		$array = array();
		$this->db->select("SkillID");
		$this->db->where("MenteeID",$id);
		$this->db->where("Status","1");
		$query = $this->db->get('menteeskill');
		$result = $query->result_array();
		foreach($result as $res){
			$array[] = $res['SkillID'];
		}
		$skills = implode(",",$array);
		echo $skills;
	}

}
