<?php

class UserRoles extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/userRole_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        //print_r($res);exit;
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            //print_r($x);exit;
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // SKILL LISTING 
    //=========================================
    public function index() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );

        $res['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Role Name', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/userrole/list', $res);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $this->datatables->select("userRoleID, UserRoleName, Weight", FALSE)
                ->unset_column('userRoleID')
                ->unset_column('Actions')
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/userRoles'), 'userRoleID')
                ->from('user_roles')
                ->where('Status', '1');

        echo $this->datatables->generate();
    }

    //=========================================
    // SKILL ADD 
    //=========================================
    public function add($id = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('UserRoleName', 'Role Name', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {

            $data = array(
                'UserRoleName' => $this->input->post('UserRoleName'),
                'Weight' => $this->input->post('Weight')
            );
            if ($id == 0) {
                $result = $this->db->insert('user_roles', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', 'User Role Added Successfully');
            } else {
                $this->db->where('userRoleID', $id);
                $result = $this->db->update('user_roles', $data);
                $this->session->set_flashdata('success', 'User Role Updated Successfully');
            }
            redirect('admin/userRoles');
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('user_roles');
        $userRoles = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $userRoles[$field] = '';
            }
        }

        if ($id) {
            $userRoles = $this->userRole_model->get_roles($id);
        }
        $data['id'] = $id;
        $data['userRoles'] = $userRoles;

        $this->template->content->view('admin/userrole/add', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // user Roles EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    public function checkUserRoleExist() {
        if ($this->input->is_ajax_request()) {
            $UserRoleName = $this->input->post('UserRoleName');
            $userRoleID = $this->input->post('userRoleID');
            if ($userRoleID == "") {
                $flag = $this->userRole_model->checkUserRole($UserRoleName);
            } else {
                $flag = $this->userRole_model->checkUserRoleEdit($UserRoleName, $userRoleID);
            }
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    //=========================================
    // user Roles DELETE 
    //=========================================
    public function delete($id) {
        $ds = $this->userRole_model->delete_roles($id);
        $this->session->set_flashdata('success', 'User Role Delete Successfully');
        redirect('admin/userRoles');
    }

}
