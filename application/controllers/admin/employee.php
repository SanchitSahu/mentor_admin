<?php

class Employee extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/employee_model');
       // $this->load->model('admin/country_model');
        $this->load->model('admin/state_model');
        $this->load->model('admin/city_model');        
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if(!loginCheck()){redirect('admin');}
		$res	=	loadDatabase();
		//print_r($res);exit;
		if($res != 0){
			$x	=	$this->load->database($res,TRUE);
			//print_r($x);exit;
			$this->db	=	$x;	
		}else{
			redirect("400.shtml");exit;
		}
    }
    
    //=========================================
    // PRODUCT LISTING  
    //=========================================
    public function index() {
        
        $tmpl = array ( 
            'table_open'  => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open'  => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">' 
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('First Name','Last Name','User Name','Email','Gender','Cell Phone','Phone Number','Address','Status','Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');
       
         
        $this->template->content->view('admin/employee/list');
        $this->template->publish_admin();
    }

    //=========================================
    //function to handle callbacks
    //=========================================
    function datatable()
    {
        $this->datatables->select('iEmployeeId,vFirstName,vLastName,vUserName,vEmail,eGender,vCellPhone, vPhoneNumber, vAddress1, eStatus')
        ->unset_column('iEmployeeId')
        ->add_column('Actions', get_buttons('$1','admin/employee'),'iEmployeeId')
        ->from('employeedetails');
        
        echo $this->datatables->generate();
    }
    
    //=========================================
    // PRODUCT ADD  
    //=========================================
    public function add($id = '') {    
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-fileupload/bootstrap-fileupload.css');
        $data = array();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('firstname', 'First Name', 'required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'required');
        $this->form_validation->set_rules('empcode', 'Emp code', 'required');
        $this->form_validation->set_rules('username', 'User Name', 'required');
        if ($id == "") {
            $this->form_validation->set_rules('password', 'Password', 'required');
        }
        
        
        if ($this->form_validation->run() == TRUE) {
            if ($this->input->post()) {
                if (!$id) {
                     $this->employee_model->setEmployee();
                     $this->session->set_flashdata('success', get_message(SUCCESS_CREATED, 'employee'));
                }else{
                    $this->employee_model->setEmployee($id);
                    $this->session->set_flashdata('success', get_message(SUCCESS_UPDATED, 'employee'));
                }
                redirect('admin/employee');
            }
        }
        
        
        /*$arrCountry = $this->country_model->countryList();
        $country = array('' => 'Select Country');
        foreach ($arrCountry as $key => $value) {
            $country[$value['iCountryId']] = $value['vCountryName'];
        }*/
        $data['countryData'] = $this->country_model->countryList();
        $arrState = $this->state_model->stateList();
        $state = array('' => 'Select State');
        foreach ($arrState as $key => $value) {
            $state[$value['iStateId']] = $value['vStateName'];
        }        
        
        $arrCity = $this->city_model->cityList();
        $city = array('' => 'Select City');
        if (!empty($arrCity)) {
            foreach ($arrCity as $key => $value) {
                $city[$value['iCityId']] = $value['vCityName'];
            }
        }
        
        $fields = get_table_fields('employeedetails');
        $emp = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $emp[$field] = '';
            }
        }        

        
        if ($id) {
             $emp = $this->employee_model->get_employee($id);

        }
        $data['id'] = $id; 
        $data['product'] = $emp;
        #$data['country'] = $country;
        $data['state'] = $state;
        $data['city'] = $city;

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-fileupload/bootstrap-fileupload.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-fileupload/bootstrap-fileupload.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');     
        
        $this->template->content->view('admin/employee/add', $data);
        $this->template->publish_admin();
    }
    
    //=========================================
    // PRODUCT ADD  
    //=========================================
    public function edit($id){
       $this->add($id);
    }
    
    
    public function profile(){      
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-fileupload/bootstrap-fileupload.css');
        $data = array();
       /* $this->load->library('form_validation');
        $this->form_validation->set_rules('UserName', 'First Name', 'required');
       // $this->form_validation->set_rules('LastName', 'Last Name', 'required');
        //$this->form_validation->set_rules('Gender', 'Gender', 'required');
        $this->form_validation->set_rules('Address', 'Address', '');
        $this->form_validation->set_rules('City', 'City', '');     
        $this->form_validation->set_rules('PostCode', 'PostCode', '');
        $this->form_validation->set_rules('Cellphone', 'Cellphone', '');
        $this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', '');   
        */
        $session = $this->session->userdata;
        $sess = $session['admin_data'];               
        //print_r($sess);exit;
        /*if ($this->form_validation->run() == TRUE) {*/
            if ($this->input->post()) {
                $this->employee_model->setEmployee($sess['id'],$sess['UserType']);
                $this->session->set_flashdata('success', get_message(SUCCESS_UPDATED, 'admin'));                
                redirect('admin/employee/profile');
            }
        //}
         
		if($sess['UserType']	==	"Admin"){
			$fields = get_table_fields('subdomains');
			$emp = array();
			if (isset($fields) && !empty($fields)) {
				foreach ($fields as $field) {
					$emp[$field] = '';
				}
			}        
			
			if ($sess['id']) {
				$emp = $this->employee_model->get_employeeAdmin($sess['id']);

			}
			
			$data['id'] = $sess['id']; 
			$data['product'] = $emp;
			$data['emp'] = $emp;

		}else{
			$fields = get_table_fields('admindetails');
			$emp = array();
			if (isset($fields) && !empty($fields)) {
				foreach ($fields as $field) {
					$emp[$field] = '';
				}
			}        
			
			if ($sess['id']) {
				 $emp = $this->employee_model->get_employee($sess['id']);

			}
			
			$data['id'] = $sess['id']; 
			$data['product'] = $emp;
			$data['emp'] = $emp;

		}
        
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-fileupload/bootstrap-fileupload.css');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-fileupload/bootstrap-fileupload.js');        
        
        $this->template->content->view('admin/employee/profile', $data);
        $this->template->publish_admin();        
    }
    
    
    public function changepassword() {     
        $session= $this->session->userdata;
        //$this->form_validation->set_rules('OldPassword', 'old password', 'required');
        $this->form_validation->set_rules('NewPassword', 'new password', 'required|min_length[8]|max_length[25]');
        $this->form_validation->set_rules('ConfirmPassword', 'confirm password', 'required|matches[NewPassword]');
        
        if ($this->form_validation->run() === FALSE) {
            $this->template->content->view('admin/employee/changepassword');
            $this->template->publish_admin();
        } else {
            if ($this->input->post()) {
                $sess = $session['admin_data'];    
				//print_r($sess);exit;
				if($sess['UserType']	==	"Admin")
					$emp = $this->employee_model->get_employeeAdmin($sess['id']);
				else
					$emp = $this->employee_model->get_employee($sess['id']);  
				//print_r($emp);exit;
              //  $oldPassword = fnEncrypt($this->input->post('OldPassword'), $this->config->item('mentorKey'));
               /* if ($emp['Password'] != $oldPassword) {
                    $this->session->set_flashdata('error', 'Please enter correct old password.');                    
                    redirect('admin/employee/changepassword');
                } else { */
                    $newpwd = $this->input->post('NewPassword');
                
				if($sess['UserType']	==	"Admin")	
					$emp = $this->employee_model->updatepwdAdmin($newpwd,$sess['id']);
				else
                    $emp = $this->employee_model->updatepwd($newpwd,$sess['id']);
                    
                    if ($emp) {
                        $this->session->set_flashdata('success', 'You have successfully changed your password.');
                        redirect('admin/dashboard');
                    }
                //}
            }
        }
    }    
    
    
    public function  forgotpassword() {
            $this->form_validation->set_rules('email_id', 'email', 'required|valid_email');
            if ($this->form_validation->run() === FALSE) {
                $this->template->content->view('admin/employee/forgotpassword');
                $this->template->publish_admin();
            } else {
                if ($this->input->post()) {

                    $email = $this->input->post('email_id');
                    
                    $userDetails = $this->employee_model->get_employeeByemail($email);

                    if(count($userDetails) > 0) {
                        
                            $emailTemplate = $this->site_emails_model->get_email_by_key('FORGOT_PASSWORD_EMAIL');

                            $resetLink = '<a href="'. base_url() .'reset-password/'.  encrypt($userDetails['Id'], ENC_DEC_KEY).'">Reset Password</a>';

                            $arrSearch = array('##FIRST_NAME##', '##CHANGE_PASSWORD_LINK##');
                            $arrReplace = array($userDetails['FirstName'].' '.$userDetails['LastName'], $resetLink);
                            $body = str_replace($arrSearch, $arrReplace, $emailTemplate['Body']);

                            $emailData['toEmail'] = $userDetails['Email'];
                            $emailData['subject'] = $emailTemplate['Subject'];
                            $emailData['body'] = $body;

                            if (send_mail($emailData)) {
        //                    echo $this->email->print_debugger();
                                $json_data['message'] = 'The mail has been successfully sent to the email address!';
                                $json_data['user_id'] = $userDetails['Id'];
                                $json_data['success'] = true;
                            } else {
                                $json_data['message'] = 'There is some problem in sending email. Please try again later!';
                                $json_data['success'] = false;
                            }
                      
                    } else {
                            $this->session->set_flashdata('error', 'Please enter correct email address.');                    
                            redirect('admin/employee/changepassword');
                    }
                    
                   }
                }            
            }
        
    
    //=========================================
    // PRODUCT DELETE 
    //=========================================
    public function delete($id) {
//        $this->product_model->delete_product($id);
//
//        $this->session->set_flashdata('success', get_message(SUCCESS_DELETED, 'product'));
//        redirect('admin/products');
    }
	
	function checkemail()
	{
		$email			=	$_REQUEST['email'];
		$currentMail	=	$_REQUEST['currentMail'];
		$res			=	$this->employee_model->checkemail($email,$currentMail);
		if($res > 0){
			echo "email is already exists";
		}
	}
    
}