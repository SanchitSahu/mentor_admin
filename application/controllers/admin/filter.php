<?php

class Filter extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $this->load->model('admin/relationship_model');
		$this->load->model('admin/report_model');
		$this->load->model('admin/settings_model');
        $res = loadDatabase();
        //print_r($res);exit;
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            //print_r($x);exit;
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    // load dashboard
    public function index() {
        
    }

    public function getDrpDwn() {
        $filterType = $_REQUEST['filterType'];
        if ($filterType == "Days") {
            echo "<option value='0'>Select</option>";
            for ($i = 1; $i <= 31; $i++) {
                echo "<option value='$i'>";
                echo $i;
                echo "</option>";
            }
        } else if ($filterType == "Week") {
            echo "<option value='0'>Select</option>";
            for ($i = 1; $i <= 52; $i++) {
                echo "<option value='$i'>";
                echo $i;
                echo "</option>";
            }
        } else if ($filterType == "Month") {
            echo "<option value='0'>Select</option>";
            for ($i = 1; $i <= 12; $i++) {
                echo "<option value='$i'>";
                echo $i;
                echo "</option>";
            }
        } else if ($filterType == "Quarter") {
            echo "<option value='0'>Select</option>";
            for ($i = 1; $i <= 40; $i++) {
                echo "<option value='$i'>";
                echo $i;
                echo "</option>";
            }
        } else if ($filterType == "Year") {
            echo "<option value='0'>Select</option>";
            for ($i = 1; $i <= 10; $i++) {
                echo "<option value='$i'>";
                echo $i;
                echo "</option>";
            }
        } else if ($filterType == "FYE") {
            echo "<option value='0'>Select</option>";
            for ($i = 1; $i <= 10; $i++) {
                echo "<option value='$i'>";
                echo $i;
                echo "</option>";
            }
        }
		else if ($filterType == "FP") {
            echo "<option value='0'>Select</option>";
            for ($i = 1; $i <= 10; $i++) {
                echo "<option value='$i'>";
                echo $i;
                echo "</option>";
            }
        }
    }

    function getWeek($week, $year) {
        $dto = new DateTime();
        $result['start'] = $dto->setISODate($year, $week, 1)->format('Y-m-d');
        $result['end'] = $dto->setISODate($year, $week, 7)->format('Y-m-d');
        return $result;
    }
	
	function getNextDay() {
        $datetime = new DateTime('tomorrow');	
        $result['end'] = $datetime->format('Y-m-d');
        return $result;
    }

    public function filterDataByType() {
        $filterType = $_REQUEST['filterType'];
        $number = $_REQUEST['number'];
        $page = $_REQUEST['page'];
        $role = $_REQUEST['role'];
        $source = $_REQUEST['source'];
		$semester = $_REQUEST['semester'];
		
        $startDate = "";
        $endDate = "";

		$mentor = (isset($_REQUEST['mentor'])) ? $_REQUEST['mentor'] : 0;
		$mentee = (isset($_REQUEST['mentee'])) ? $_REQUEST['mentee'] : 0;
        //echo $filterType;//echo $number;echo $page;echo $role;echo $source;exit;
        //$query	=	"";
        //if($filterType != 0){
        //echo "dfjd".$filterType;
		if ($filterType == "Today") {
            $currDate = date('Y-m-d');
			
			$result = $this->getNextDay();
            $startDate = $currDate;
            $endDate = $result['end'];
            //$this->page($startDate,$endDate,$page);
        }else if ($filterType == "Week") {
            $currDate = date('Y-m-d');
            $currweek = date("W", strtotime($currDate));
            $year = date("Y", strtotime($currDate));
            $currentweek = date("W");

            $result = $this->getWeek($currweek, $year);
            //echo "Week:".$currweek." Start date:".$result['start']." End date:".$result['end']."<br>";
            $startDate = $result['start'];
            $endDate = $result['end'];
            //$this->page($startDate,$endDate,$page);
        } else if ($filterType == "Month") {
            $startDate = date('Y-m-01'); // hard-coded '01' for first day
            $endDate = date('Y-m-t');
            //$this->page($startDate,$endDate,$page);
        } else if ($filterType == "Quarter") {
            $current_month = date('m');
            $current_year = date('Y');
            if ($current_month >= 1 && $current_month <= 3) {
                $startDate = $current_year . '-01-1';  // timestamp or 1-Januray 12:00:00 AM
                $endDate = $current_year . '-04-1';  // timestamp or 1-April 12:00:00 AM means end of 31 March
                //$this->page($startDate,$endDate,$page);
            } else if ($current_month >= 4 && $current_month <= 6) {
                $startDate = $current_year . '-04-1';  // timestamp or 1-April 12:00:00 AM
                $endDate = $current_year . '-07-1';  // timestamp or 1-July 12:00:00 AM means end of 30 June
                //$this->page($startDate,$endDate,$page);
            } else if ($current_month >= 7 && $current_month <= 9) {
                $startDate = $current_year . '-07-1';  // timestamp or 1-July 12:00:00 AM
                $endDate = $current_year . '-10-1';  // timestamp or 1-October 12:00:00 AM means end of 30 September
                //$this->page($startDate,$endDate,$page);
            } else if ($current_month >= 10 && $current_month <= 12) {
                $startDate = $current_year . '-10-1';  // timestamp or 1-October 12:00:00 AM
                $endDate = ($current_year + 1) . '-01-1';  // timestamp or 1-January Next year 12:00:00 AM means end of 31 December this year
                //$this->page($startDate,$endDate,$page);
            }
        } else if ($filterType == "Year") {
            $year = date('Y'); // Get current year 
            $startDate = "{$year}-01-1";
            $endDate = "{$year}-12-31";
            //$this->page($startDate,$endDate,$page);
        } else if ($filterType == "FYE") {
            $year = date('Y'); // Get current year 
            $startDate = "{$year}-01-1";
            $endDate = "{$year}-12-31";
            //$this->page($startDate,$endDate,$page);
        } else if ($filterType == "YTD") {
            $year = date('Y'); // Get current year 
            $startDate = "{$year}-01-1";
            $endDate = "{$year}-12-31";
            //$this->page($startDate,$endDate,$page);
        } else if ($filterType == "FP") {
            $year = date('Y'); // Get current year 
            $startDate = "{$year}-01-1";
            $endDate =  date('Y-m-d');
            //$this->page($startDate,$endDate,$page);
        }
        //	$query	=	$query ."->where('mi.MeetingStartDatetime >=',$startDate)->where('mi.MeetingEndDatetime <=',$endDate)";
        //}
        //echo "startDate ".$startDate;
        /* if($role != 0){
          $query	=	$query ."->join('usertorole utr', 'utr.MentorID = m.MentorID','left')
          ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID','left')";
          }

          if($source	!= 0){
          $query	=	$query ."->join('usertosource us', 'us.MentorID = m.MentorID','left')
          ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID','left')";
          } */

        $this->page($page, $startDate, $endDate, $role, $source,$mentor,$mentee,$semester);
    }

    public function page($page = "", $startDate = "", $endDate = "", $role = 0, $source = 0, $mentor =0 , $mentee = 0, $semester = 0) {
		//echo $page . " " . $startDate . " " . $endDate . " " . $role . " " . $source;exit;
        if ($page == "" && $startDate == "" && $endDate == "" && $role == 0 && $source == 0) {
            $page = $_REQUEST['page'];
            $role = $_REQUEST['role'];
            $source = $_REQUEST['source'];
			$programme = isset($_REQUEST['programme']) ? $_REQUEST['programme'] : 0;
            $menteesource = isset($_REQUEST['menteesource']) ? $_REQUEST['menteesource'] : 0;
			$satisfaction = isset($_REQUEST['satisfaction']) ? $_REQUEST['satisfaction'] : 0;
            $startDate = $_REQUEST['startDate'];
            $endDate = $_REQUEST['endDate'];
			$mentor = (isset($_REQUEST['mentor'])) ? $_REQUEST['mentor'] : 0;
			$mentee = (isset($_REQUEST['mentee'])) ? $_REQUEST['mentee'] : 0;
			$semester = (isset($_REQUEST['semester'])) ? $_REQUEST['semester'] : 0;
			
			
        }
		if(isset($_REQUEST['semester']) && $_REQUEST['semester'] != 0 ){
			$semesterDates = $this->report_model->getSemesterDates($_REQUEST['semester']);
			//print_r($semesterDates);exit;
			$semStart	=	$semesterDates[0]->SemesterStartDate;
			$semEnd	    =	$semesterDates[0]->SemesterEndDate;
		}else{
			$semStart = "";
			$semEnd = "";
		}
        //echo $page;exit;
        if ($page == "mentordatatable") {
            $this->mentordatatable2($startDate, $endDate, $role, $source, $semStart, $semEnd, $programme, $menteesource);
        } else if ($page == "menteedatatable") {
            $this->menteedatatable($startDate, $endDate, $role, $source, $semStart, $semEnd, $programme, $menteesource);
        } else if ($page == "globaldatatable") {
            $this->globaldatatable($startDate, $endDate, $role, $source);
        } else if ($page == "mentordetaildatatable") {
            $this->mentordetaildatatable($startDate, $endDate, $role, $source, $semStart, $semEnd, $satisfaction);
        } else if ($page == "menteedetaildatatable") {
            $this->menteedetaildatatable($startDate, $endDate, $role, $source, $semStart, $semEnd);
        } else if ($page == "mentorMeetingReportDatatable") {
            $this->mentorMeetingReportDatatable($startDate, $endDate, $role, $source, $mentor, $mentee, $semStart, $semEnd);
        } else if ($page == "menteeMeetingReportDatatable") {
            $this->menteeMeetingReportDatatable($startDate, $endDate, $role, $source, $semStart, $semEnd);
        } else if ($page == "teamdatatable") {
            $this->teamdatatable($startDate, $endDate, $role, $source);
        }		
    }

    public function mentordatatable2($startDate, $endDate, $role, $source, $semStart, $semEnd, $programme, $menteesource) {
        //echo $startDate," ",$endDate;exit;
		
		$this->datatables->select('m.MentorName,me.MenteeName,count(me.MenteeID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName, mp.ProgrammeName as menteeProgrammeName, mes.MenteeSourceName as menteeSourceName', FALSE)
			->from('meetinginfo mi')
			->join('mentor m', 'mi.MentorID=m.MentorID', 'left')
			->join('mentee me', 'mi.MenteeID=me.MenteeID', 'left')
			->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
			->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
			->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
			->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
			
			->join('menteetoprogramme mtp', 'mtp.MenteeID = me.MenteeID', 'left')
			->join('menteeprogrammes mp', 'mp.ProgrammeID = mtp.ProgrammeID', 'left')        
			->join('menteetosource mts', 'mts.MenteeID = me.MenteeID', 'left')
			->join('menteesourcenames mes', 'mes.MenteeSourceID = mts.MenteeSourceID', 'left')
				
			->where('m.Status', 1)
			->where('me.Status', 1)
			->group_by("m.MentorID")
			->group_by("me.MenteeID");
		
		if($role != 0){
			$this->datatables->where('utr.userRoleID ', $role);
		}
		if($source != 0){
			$this->datatables->where('ms.MentorSourceID ', $source);
		}
		
		if($programme != 0){
			$this->datatables->where('mtp.ProgrammeID ', $programme);
		}
		if($menteesource != 0){
			$this->datatables->where('mts.MenteeSourceID ', $menteesource);
		}
		
		
		if($semStart != "" && $semEnd != ""){
			$this->datatables->where('mi.MeetingStartDatetime >=', $semStart);
			$this->datatables->where('mi.MeetingEndDatetime <=', $semEnd);
		}
		
		if($startDate != ""){
            $this->db->where('mi.MeetingStartDatetime >=', $startDate);
		}
		
		if($endDate != ""){
			$this->db->where('mi.MeetingEndDatetime <=', $endDate);
		}
		
		
        /*if ($startDate != "" && $endDate != "" && $role == 0 && $source != 0) {
				
            $this->datatables->select('*,m.MentorName,me.MenteeName,count(me.MenteeID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
					->from('meetinginfo mi')
					->join('mentor m', 'mi.MentorID=m.MentorID', 'left')
					->join('mentee me', 'mi.MenteeID=me.MenteeID', 'left')
					->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
					->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
					->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
					->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
					->where('m.Status', 1)
					->where('me.Status', 1)
					->group_by("m.MentorID")
					->group_by("me.MenteeID")
                    ->where('ms.MentorSourceID ', $source);
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source != 0) {
            $this->datatables->select('m.MentorName,me.MenteeName,count(me.MenteeID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
					->from('meetinginfo mi')
					->join('mentor m', 'mi.MentorID=m.MentorID', 'left')
					->join('mentee me', 'mi.MenteeID=me.MenteeID', 'left')
					->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
					->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
					->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
					->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
					->where('m.Status', 1)
					->where('me.Status', 1)
                    ->where('ms.MentorSourceID ', $source)
                    ->where('utr.userRoleID ', $role)
					->group_by("m.MentorID")
					->group_by("me.MenteeID");
        } else if ($startDate != "" && $endDate != "" && $role == 0 && $source == 0) {
            $this->datatables->select('m.MentorName,me.MenteeName,count(me.MenteeID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
					->from('meetinginfo mi')
					->join('mentor m', 'mi.MentorID=m.MentorID', 'left')
					->join('mentee me', 'mi.MenteeID=me.MenteeID', 'left')
					->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
					->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
					->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
					->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
					->where('m.Status', 1)
					->where('me.Status', 1)
					->group_by("m.MentorID")
					->group_by("me.MenteeID")
                    ->where('mi.MeetingStartDatetime >=', $startDate)
                    ->where('mi.MeetingEndDatetime <=', $endDate);
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source == 0) {
            $this->datatables->select('m.MentorName,me.MenteeName,count(me.MenteeID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
					->from('meetinginfo mi')
					->join('mentor m', 'mi.MentorID=m.MentorID', 'left')
					->join('mentee me', 'mi.MenteeID=me.MenteeID', 'left')
					->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
					->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
					->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
					->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
					->where('m.Status', 1)
					->where('me.Status', 1)
					->group_by("m.MentorID")
					->group_by("me.MenteeID")
                    ->where('mi.MeetingStartDatetime >=', $startDate)
                    ->where('mi.MeetingEndDatetime <=', $endDate)
                    ->where('utr.userRoleID ', $role);
        } else if ($startDate != "" && $endDate != "" && $role == 0 && $source != 0) {
            $this->datatables->select('m.MentorName,me.MenteeName,count(me.MenteeID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
					->from('meetinginfo mi')
					->join('mentor m', 'mi.MentorID=m.MentorID', 'left')
					->join('mentee me', 'mi.MenteeID=me.MenteeID', 'left')
					->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
					->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
					->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
					->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
					->where('m.Status', 1)
					->where('me.Status', 1)
					->group_by("m.MentorID")
					->group_by("me.MenteeID")
                    ->where('mi.MeetingStartDatetime >=', $startDate)
                    ->where('mi.MeetingEndDatetime <=', $endDate)
                    ->where('ms.MentorSourceID ', $source);
        } else {
           $this->datatables->select('m.MentorName,me.MenteeName,count(me.MenteeID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
					->from('meetinginfo mi')
					->join('mentor m', 'mi.MentorID=m.MentorID', 'left')
					->join('mentee me', 'mi.MenteeID=me.MenteeID', 'left')
					->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
					->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
					->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
					->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
					->where('m.Status', 1)
					->where('me.Status', 1)
					->group_by("m.MentorID")
					->group_by("me.MenteeID")
                    ->where('mi.MeetingStartDatetime >=', $startDate)
                    ->where('mi.MeetingEndDatetime <=', $endDate);
        }*/

        echo $this->datatables->generate();
    }

    function menteedatatable($startDate, $endDate, $role, $source, $semStart, $semEnd, $programme, $menteesource) {
		 $this->datatables->select('me.MenteeName,m.MentorName,count(me.MenteeID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName, mp.ProgrammeName as menteeProgrammeName, mes.MenteeSourceName as menteeSourceName', FALSE)
			->from('meetinginfo mi')
			->join('mentor m', 'mi.MentorID=m.MentorID', 'left')
			->join('mentee me', 'mi.MenteeID=me.MenteeID', 'left')
			->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
			->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
			->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
			->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
			
			->join('menteetoprogramme mtp', 'mtp.MenteeID = me.MenteeID', 'left')
			->join('menteeprogrammes mp', 'mp.ProgrammeID = mtp.ProgrammeID', 'left')        
			->join('menteetosource mts', 'mts.MenteeID = me.MenteeID', 'left')
			->join('menteesourcenames mes', 'mes.MenteeSourceID = mts.MenteeSourceID', 'left')
			
			->where('m.Status', 1)
			->where('me.Status', 1)
			->group_by("m.MentorID")
			->group_by("me.MenteeID");
		
		if($role != 0){
			$this->datatables->where('utr.userRoleID ', $role);
		}
		if($source != 0){
			$this->datatables->where('ms.MentorSourceID ', $source);
		}
		
		if($programme != 0){
			$this->datatables->where('mtp.ProgrammeID ', $programme);
		}
		if($menteesource != 0){
			$this->datatables->where('mts.MenteeSourceID ', $menteesource);
		}		
		
		if($semStart != "" && $semEnd != ""){
			$this->datatables->where('mi.MeetingStartDatetime >=', $semStart);
			$this->datatables->where('mi.MeetingEndDatetime <=', $semEnd);
		}
		
		if($startDate != ""){
            $this->db->where('mi.MeetingStartDatetime >=', $startDate);
		}
		
		if($endDate != ""){
			$this->db->where('mi.MeetingEndDatetime <=', $endDate);
		}
		
        /*if ($startDate != "" && $endDate != "" && $role == 0 && $source != 0) {
            $this->datatables->select('me.MenteeName AS MenteeName,m.MentorName AS MentorName,  mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MenteeComment AS MenteeComment, mc.CareC AS CareC, mc.CareA AS CareA, mc.CareR AS CareR, mc.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'inner')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'inner')
                    ->where('ms.MentorSourceID ', $source);
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source != 0) {
            $this->datatables->select('me.MenteeName AS MenteeName,m.MentorName AS MentorName,  mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MenteeComment AS MenteeComment, mc.CareC AS CareC, mc.CareA AS CareA, mc.CareR AS CareR, mc.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'inner')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'inner')
                    ->where('ms.MentorSourceID ', $source)
                    ->where('utr.userRoleID ', $role);
        } else if ($startDate != "" && $endDate != "" && $role == 0 && $source == 0) {
            $this->datatables->select('me.MenteeName AS MenteeName,m.MentorName AS MentorName,  mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MenteeComment AS MenteeComment, mc.CareC AS CareC, mc.CareA AS CareA, mc.CareR AS CareR, mc.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->where('mi.MeetingStartDatetime >=', $startDate)
                    ->where('mi.MeetingEndDatetime <=', $endDate)
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left');
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source == 0) {
            $this->datatables->select('me.MenteeName AS MenteeName,m.MentorName AS MentorName,  mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MenteeComment AS MenteeComment, mc.CareC AS CareC, mc.CareA AS CareA, mc.CareR AS CareR, mc.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->where('mi.MeetingStartDatetime >=', $startDate)
                    ->where('mi.MeetingEndDatetime <=', $endDate)
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                    ->where('utr.userRoleID ', $role);
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source == 0) {
            $this->datatables->select('me.MenteeName AS MenteeName,m.MentorName AS MentorName,  mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MenteeComment AS MenteeComment, mc.CareC AS CareC, mc.CareA AS CareA, mc.CareR AS CareR, mc.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                    ->where('utr.userRoleID ', $role);
        } else {
            $this->datatables->select('me.MenteeName AS MenteeName,m.MentorName AS MentorName,  mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MenteeComment AS MenteeComment, mc.CareC AS CareC, mc.CareA AS CareA, mc.CareR AS CareR, mc.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'inner')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'inner')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'inner');
        }*/
        echo $this->datatables->generate();
    }

    function globaldatatable($startDate, $endDate, $role, $source) {
        if ($startDate != "" && $endDate != "" && $role == 0 && $source != 0) {
            $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'inner')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'inner')
                    ->where('ms.MentorSourceID ', $source);
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source != 0) {
            $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'inner')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'inner')
                    ->where('ms.MentorSourceID ', $source)
                    ->where('utr.userRoleID ', $role);
        } else if ($startDate != "" && $endDate != "" && $role == 0 && $source == 0) {
            $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->where('mi.MeetingStartDatetime >=', $startDate)
                    ->where('mi.MeetingEndDatetime <=', $endDate)
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left');
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source == 0) {
            $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->where('mi.MeetingStartDatetime >=', $startDate)
                    ->where('mi.MeetingEndDatetime <=', $endDate)
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                    ->where('utr.userRoleID ', $role);
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source == 0) {
            $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                    ->where('utr.userRoleID ', $role);
        } else {
            //mec.MenteeComment AS MenteeComment, mec.CareC AS CareC, mec.CareA AS CareA, mec.CareR AS CareR, mec.CareE AS CareE
            $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'inner')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'inner')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'inner');
            //->join('menteecomment mec', 'mec.MeetingID = mi.MeetingID','inner');
            /* ->where('mi.MeetingStartDatetime >=',$startDate)
              ->where('mi.MeetingEndDatetime <=',$endDate) */
        }
        echo $this->datatables->generate();
    }

    function mentordetaildatatable($startDate, $endDate, $role, $source,$semStart, $semEnd, $satisfaction) {
		$this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mmc.MentorComment AS MentorComment, mmc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName',FALSE)
			->from('meetinginfo mi')
			->join('mentor m', 'm.MentorID = mi.MentorID')
			->join('mentee me', 'me.MenteeID = mi.MenteeID')
			->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
			->join('mentorcomment mmc', 'mmc.MeetingID = mi.MeetingID','left')
			->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
			->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
			->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
			->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
			->where('m.Status', 1)
			->where('me.Status', 1);
		
		if($role != 0){
			$this->datatables->where('utr.userRoleID ', $role);
		}
		if($source != 0){
			$this->datatables->where('ms.MentorSourceID ', $source);
		}
		if($satisfaction != 0){
			$this->datatables->where('mmc.SatisfactionIndex ', $satisfaction);
		}
		
		
		if($semStart != "" && $semEnd != ""){
			$this->datatables->where('mi.MeetingStartDatetime >=', $semStart);
			$this->datatables->where('mi.MeetingEndDatetime <=', $semEnd);
		}
		
		if($startDate != ""){
            $this->db->where('mi.MeetingStartDatetime >=', $startDate);
		}
		
		if($endDate != ""){
			$this->db->where('mi.MeetingEndDatetime <=', $endDate);
		}
		
       /* if ($startDate != "" && $endDate != "" && $role == 0 && $source != 0) {
            $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mmc.MentorComment AS MentorComment, mmc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName',FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
					->join('mentorcomment mmc', 'mmc.MeetingID = mi.MeetingID','left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'inner')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'inner')
                    ->where('ms.MentorSourceID ', $source)
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source != 0) {
            $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mmc.MentorComment AS MentorComment, mmc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName',FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
					->join('mentorcomment mmc', 'mmc.MeetingID = mi.MeetingID','left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'inner')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'inner')
                    ->where('ms.MentorSourceID ', $source)
                    ->where('utr.userRoleID ', $role)
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        } else if ($startDate != "" && $endDate != "" && $role == 0 && $source == 0) {
            $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName',FALSE)
                ->from('meetinginfo mi')
                ->join('mentor m', 'm.MentorID = mi.MentorID')
                ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                ->where('mi.MeetingStartDatetime >=', $startDate)
                ->where('mi.MeetingEndDatetime <', $endDate)
                ->where('m.Status', 1)
                ->where('me.Status', 1);
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source == 0) {
            $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mmc.MentorComment AS MentorComment, mmc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName',FALSE)
                    ->from('meetinginfo mi')
                    ->where('mi.MeetingStartDatetime >=', $startDate)
                    ->where('mi.MeetingEndDatetime <', $endDate)
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
					->join('mentorcomment mmc', 'mmc.MeetingID = mi.MeetingID','left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                    ->where('utr.userRoleID ', $role)
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source == 0) {
            $this->datatables->select('m.MentorName,me.MenteeName,count(me.MenteeID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
					->join('mentorcomment mmc', 'mmc.MeetingID = mi.MeetingID','left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                    ->where('utr.userRoleID ', $role)
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        } else {
            $this->datatables->select('m.MentorName,me.MenteeName,count(me.MenteeID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'mi.MentorID=m.MentorID', 'left')
                    ->join('mentee me', 'mi.MenteeID=me.MenteeID', 'left')
                    /* ->where('mi.MeetingStartDatetime >=',$startDate)
                      ->where('mi.MeetingEndDatetime <=',$endDate) */
                  /*  ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'inner')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'inner')
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        }*/



        echo $this->datatables->generate();
    }

    function menteedetaildatatable($startDate, $endDate, $role, $source,$semStart, $semEnd) {
		$this->datatables->select('me.MenteeName AS MenteeName,m.MentorName AS MentorName,  mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MenteeComment AS MenteeComment, mc.CareC AS CareC, mc.CareA AS CareA, mc.CareR AS CareR, mc.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName',FALSE)
			->from('meetinginfo mi')
			->join('mentor m', 'm.MentorID = mi.MentorID')
			->join('mentee me', 'me.MenteeID = mi.MenteeID')
			->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
			->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
			->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
			->join('usertosource us', 'us.MentorID = m.MentorID', 'inner')
			->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'inner')
			->where('m.Status', 1)
			->where('me.Status', 1);
		
		if($role != 0){
			$this->datatables->where('utr.userRoleID ', $role);
		}
		
		if($source != 0){
			$this->datatables->where('ms.MentorSourceID ', $source);
		}
		
		
		if($semStart != "" && $semEnd != ""){
			$this->datatables->where('mi.MeetingStartDatetime >=', $semStart);
			$this->datatables->where('mi.MeetingEndDatetime <=', $semEnd);
		}
		
		if($startDate != ""){
            $this->db->where('mi.MeetingStartDatetime >=', $startDate);
		}
		
		if($endDate != ""){
			$this->db->where('mi.MeetingEndDatetime <=', $endDate);
		}
		
        /*if ($startDate != "" && $endDate != "" && $role == 0 && $source != 0) {
            $this->datatables->select('me.MenteeName AS MenteeName,m.MentorName AS MentorName,  mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MenteeComment AS MenteeComment, mc.CareC AS CareC, mc.CareA AS CareA, mc.CareR AS CareR, mc.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName',FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'inner')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'inner')
                    ->where('ms.MentorSourceID ', $source)
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source != 0) {
            $this->datatables->select('me.MenteeName AS MenteeName,m.MentorName AS MentorName,  mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MenteeComment AS MenteeComment, mc.CareC AS CareC, mc.CareA AS CareA, mc.CareR AS CareR, mc.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName',FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'inner')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'inner')
                    ->where('ms.MentorSourceID ', $source)
                    ->where('utr.userRoleID ', $role)
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        } else if ($startDate != "" && $endDate != "" && $role == 0 && $source == 0) {
            $this->datatables->select('me.MenteeName AS MenteeName,m.MentorName AS MentorName,  mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MenteeComment AS MenteeComment, mc.CareC AS CareC, mc.CareA AS CareA, mc.CareR AS CareR, mc.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName',FALSE)
                    ->from('meetinginfo mi')
                    ->where('mi.MeetingStartDatetime >=', $startDate)
                    ->where('mi.MeetingEndDatetime <=', $endDate)
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source == 0) {
            $this->datatables->select('me.MenteeName AS MenteeName,m.MentorName AS MentorName,  mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MenteeComment AS MenteeComment, mc.CareC AS CareC, mc.CareA AS CareA, mc.CareR AS CareR, mc.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName',FALSE)
                    ->from('meetinginfo mi')
                    ->where('mi.MeetingStartDatetime >=', $startDate)
                    ->where('mi.MeetingEndDatetime <=', $endDate)
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                    ->where('utr.userRoleID ', $role)
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source == 0) {
            $this->datatables->select('m.MentorName,me.MenteeName,count(me.MenteeID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'm.MentorID = mi.MentorID')
                    ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                    ->join('menteecomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                    ->where('utr.userRoleID ', $role)
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        } else {
            $this->datatables->select('me.MenteeName,m.MentorName,count(me.MenteeID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                    ->from('meetinginfo mi')
                    ->join('mentor m', 'mi.MentorID=m.MentorID', 'left')
                    ->join('mentee me', 'mi.MenteeID=me.MenteeID', 'left')
                    /* ->where('mi.MeetingStartDatetime >=',$startDate)
                      ->where('mi.MeetingEndDatetime <=',$endDate) */
                /*    ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'inner')
                    ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'inner')
                    ->join('usertosource us', 'us.MentorID = m.MentorID', 'inner')
                    ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'inner')
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        }*/



        echo $this->datatables->generate();
    }

	function mentorMeetingReportDatatable($startDate, $endDate, $role, $source, $mentor, $mentee,$semStart, $semEnd) {
		 $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                ->from('meetinginfo mi')
                ->join('mentor m', 'm.MentorID = mi.MentorID')
                ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
				->where('mi.MeetingStartDatetime >=', $startDate)
                ->where('mi.MeetingEndDatetime <=', $endDate)
				->where('m.Status', 1)
                ->where('me.Status', 1)
				->group_by('mi.MeetingID');
		
		if($role != 0){
			$this->datatables->where('utr.userRoleID ', $role);
		}
		
		if($source != 0){
			$this->datatables->where('ms.MentorSourceID ', $source);
		}
		
		if($mentor != 0){
			$this->datatables->where('m.MentorID ', $mentor);
		}
		
		if($mentee != 0){
			$this->datatables->where('me.MenteeID ', $mentee);
		}
		
		if($semStart != "" && $semEnd != ""){
			$this->datatables->where('mi.MeetingStartDatetime >=', $semStart);
			$this->datatables->where('mi.MeetingEndDatetime <=', $semEnd);
		}
		
        /*if ($startDate != "" && $endDate != "" && $role == 0 && $source != 0) {
            $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                ->from('meetinginfo mi')
                ->join('mentor m', 'm.MentorID = mi.MentorID')
                ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                ->where('ms.MentorSourceID ', $source)
                ->where('m.Status', 1)
                ->where('me.Status', 1)
				->group_by('mi.MeetingID');
            //$this->db->group_by("me.MenteeID");
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source != 0) {
            $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                ->from('meetinginfo mi')
                ->join('mentor m', 'm.MentorID = mi.MentorID')
                ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                ->where('ms.MentorSourceID ', $source)
                ->where('utr.userRoleID ', $role)
                ->where('m.Status', 1)
                ->where('me.Status', 1)
				->group_by('mi.MeetingID');
            //$this->db->group_by("me.MenteeID");
        } else if ($startDate != "" && $endDate != "" && $role == 0 && $source == 0) {
           $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
					->from('meetinginfo mi')
					->where('mi.MeetingStartDatetime >=', $startDate)
                    ->where('mi.MeetingEndDatetime <=', $endDate)
					->join('mentor m', 'm.MentorID = mi.MentorID')
					->join('mentee me', 'me.MenteeID = mi.MenteeID')
					->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
					->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
					->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
					->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
					->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                    ->where('m.Status', 1)
                    ->where('me.Status', 1)
					->group_by('mi.MeetingID');
            //$this->db->group_by("me.MenteeID");
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source == 0) {
            $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
					->from('meetinginfo mi')
					->where('mi.MeetingStartDatetime >=', $startDate)
                    ->where('mi.MeetingEndDatetime <=', $endDate)
					->join('mentor m', 'm.MentorID = mi.MentorID')
					->join('mentee me', 'me.MenteeID = mi.MenteeID')
					->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
					->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
					->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
					->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
					->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                    ->where('utr.userRoleID ', $role)
                    ->where('m.Status', 1)
                    ->where('me.Status', 1)
					->group_by('mi.MeetingID');
            //$this->db->group_by("me.MenteeID");
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source == 0) {
            $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
					->from('meetinginfo mi')
					->join('mentor m', 'm.MentorID = mi.MentorID')
					->join('mentee me', 'me.MenteeID = mi.MenteeID')
					->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
					->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
					->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
					->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
					->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                    ->where('utr.userRoleID ', $role)
                    ->where('m.Status', 1)
                    ->where('me.Status', 1)
					->group_by('mi.MeetingID');
            //$this->db->group_by("me.MenteeID");
        } else {
            $this->datatables->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
					->from('meetinginfo mi')
					->join('mentor m', 'm.MentorID = mi.MentorID')
					->join('mentee me', 'me.MenteeID = mi.MenteeID')
					->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
					->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
					->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
					->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
					->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                    ->where('m.Status', 1)
                    ->where('me.Status', 1)
					->group_by('mi.MeetingID');
            //$this->db->group_by("me.MenteeID");
        }*/



        echo $this->datatables->generate();
    }
    
	function menteeMeetingReportDatatable($startDate, $endDate, $role, $source) {
        if ($startDate != "" && $endDate != "" && $role == 0 && $source != 0) {
             $this->datatables->select('me.MenteeName AS MenteeName, m.MentorName AS MentorName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mec.CareC AS CareC, mec.CareA AS CareA, mec.CareR AS CareR, mec.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
					->from('meetinginfo mi')
					->join('mentor m', 'm.MentorID = mi.MentorID')
					->join('mentee me', 'me.MenteeID = mi.MenteeID')
					->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
					->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
					->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
					->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
					->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
					->join('menteecomment mec', 'mec.MeetingID = mi.MeetingID', 'inner')
                    ->where('ms.MentorSourceID ', $source)
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source != 0) {
             $this->datatables->select('me.MenteeName AS MenteeName, m.MentorName AS MentorName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mec.CareC AS CareC, mec.CareA AS CareA, mec.CareR AS CareR, mec.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
					->from('meetinginfo mi')
					->join('mentor m', 'm.MentorID = mi.MentorID')
					->join('mentee me', 'me.MenteeID = mi.MenteeID')
					->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
					->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
					->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
					->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
					->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
					->join('menteecomment mec', 'mec.MeetingID = mi.MeetingID', 'inner')
                    ->where('ms.MentorSourceID ', $source)
                    ->where('utr.userRoleID ', $role)
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        } else if ($startDate != "" && $endDate != "" && $role == 0 && $source == 0) {
             $this->datatables->select('me.MenteeName AS MenteeName, m.MentorName AS MentorName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mec.CareC AS CareC, mec.CareA AS CareA, mec.CareR AS CareR, mec.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
                ->from('meetinginfo mi')
                ->join('mentor m', 'm.MentorID = mi.MentorID')
                ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
                ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                ->join('menteecomment mec', 'mec.MeetingID = mi.MeetingID', 'inner')
                ->where('mi.MeetingStartDatetime >=', $startDate)
                ->where('mi.MeetingEndDatetime <=', $endDate)
                ->where('m.Status', 1)
                ->where('me.Status', 1);
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source == 0) {
             $this->datatables->select('me.MenteeName AS MenteeName, m.MentorName AS MentorName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mec.CareC AS CareC, mec.CareA AS CareA, mec.CareR AS CareR, mec.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
					->from('meetinginfo mi')
					->where('mi.MeetingStartDatetime >=', $startDate)
					->where('mi.MeetingEndDatetime <=', $endDate)
					->join('mentor m', 'm.MentorID = mi.MentorID')
					->join('mentee me', 'me.MenteeID = mi.MenteeID')
					->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
					->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
					->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
					->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
					->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
					->join('menteecomment mec', 'mec.MeetingID = mi.MeetingID', 'inner')
                    ->where('utr.userRoleID ', $role)
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        } else if ($startDate != "" && $endDate != "" && $role != 0 && $source == 0) {
             $this->datatables->select('me.MenteeName AS MenteeName, m.MentorName AS MentorName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mec.CareC AS CareC, mec.CareA AS CareA, mec.CareR AS CareR, mec.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
					->from('meetinginfo mi')
					->join('mentor m', 'm.MentorID = mi.MentorID')
					->join('mentee me', 'me.MenteeID = mi.MenteeID')
					->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
					->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
					->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
					->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
					->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
					->join('menteecomment mec', 'mec.MeetingID = mi.MeetingID', 'inner')
                    ->where('utr.userRoleID ', $role)
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        } else {
            $this->datatables->select('me.MenteeName AS MenteeName, m.MentorName AS MentorName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mec.CareC AS CareC, mec.CareA AS CareA, mec.CareR AS CareR, mec.CareE AS CareE,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName', FALSE)
					->from('meetinginfo mi')
					->join('mentor m', 'm.MentorID = mi.MentorID')
					->join('mentee me', 'me.MenteeID = mi.MenteeID')
					->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left')
					->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
					->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
					->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
					->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
					->join('menteecomment mec', 'mec.MeetingID = mi.MeetingID', 'inner')
                    ->where('m.Status', 1)
                    ->where('me.Status', 1);
            //$this->db->group_by("me.MenteeID");
        }



        echo $this->datatables->generate();
    }
	
	function filterNDataByType() {
        $filterType = $_REQUEST['filterType'];
        $number = $_REQUEST['number'];
        $page = $_REQUEST['page'];
        $role = $_REQUEST['role'];
        $source = $_REQUEST['source'];
        $startDate = "";
        $endDate = "";
		$mentor = (isset($_REQUEST['mentor'])) ? $_REQUEST['mentor'] : 0;
		$mentee = (isset($_REQUEST['mentee'])) ? $_REQUEST['mentee'] : 0;
		$semester = $_REQUEST['semester'];

        if ($filterType == "Days") {
            $startDate = date('Y-m-d', strtotime('today - ' . $number . ' days'));
            $endDate = date('Y-m-d');
            //$this->page($startDate,$endDate,$page);
        } else if ($filterType == "Week") {
            $currDate = date('Y-m-d');
            $currweek = date("W", strtotime($currDate));
            $startweek = $currweek - $number;
            $year = date("Y", strtotime($currDate));
            $resultStart = $this->getWeek($startweek, $year);
            $resultEnd = $this->getWeek($currweek, $year);
            //echo "Week:".$currweek." Start week:".$startweek;
            $startDate = $resultStart['start'];
            $endDate = $resultEnd['end'];
            //$this->page($startDate,$endDate,$page);
        } else if ($filterType == "Month") {
            $monthNumber = date('m');
           // $prevMonth = $monthNumber - $number;
			$startDate	=	date('Y-m-01',strtotime("-".$number." month"));
          // echo " ".$startDate = date('Y-' . $prevMonth . '-01'); // hard-coded '01' for first day
            $endDate = date('Y-m-t');
            $this->page($startDate, $endDate, $page);
        } else if ($filterType == "Quarter") {
            $current_month = date('m');
            $current_year = date('Y');
            $prevMonth = $number * 3;
            $fromDate = date("Y-m-d", strtotime("-" . $prevMonth . " months"));
            $startDate = $fromDate;
            $endDate = date("Y-m-d");
            //$this->page($startDate,$endDate,$page);
        } else if ($filterType == "Year") {
            $year = date('Y'); // Get current year 	
            $prevYear = $year - $number;

            $startDate = "{$prevYear}-01-1";
            $endDate = "{$year}-12-31";
            //$this->page($startDate,$endDate,$page);
        } else if ($filterType == "FYE") {
            $year = date('Y'); // Get current year 	
            $prevYear = $year - $number;

            $startDate = "{$prevYear}-01-1";
            $endDate = "{$year}-12-31";
            //$this->page($startDate,$endDate,$page);
        } else if ($filterType == "FP") {
			$getFiscalPeriods = $this->settings_model->getConfig();

			$fiscalYear = $getFiscalPeriods[0]->fiscalYear;
			$fiscalPeriod = $getFiscalPeriods[0]->noOfPeriod;
			if($fiscalPeriod == 0) $fiscalPeriod = 1;
			$getQuarter = (int)(12/$fiscalPeriod); //addmonth
			$monthAdd = 12/$getQuarter; //for loop
			$quearters =array();
			for($i=0;$i<$number;$i++){
				$q = date("Y-m-d", strtotime("-" . $getQuarter . " months", strtotime($fiscalYear))); 
				$fiscalYear = $q;
				array_push($quearters,$q);
			}
			$count = count($quearters);
			$startDate = isset($quearters[$count-1])? $quearters[$count-1] : date('Y-m-d');
			$endDate = $getFiscalPeriods[0]->fiscalYear;
            //print_r($quearters);
            //$this->page($startDate,$endDate,$page);
        }
        $this->page($page, $startDate, $endDate, $role, $source, $mentor, $mentee, $semester);
    }
	
	function menteeprogrammedatatable() {
		
		$programme	=	$_REQUEST['programme'];
        $this->datatables->select('m.MenteeName,p.ProgrammeName', FALSE)
                ->from('mentee m')
                ->join('menteetoprogramme mp', 'mp.MenteeID = m.MenteeID', 'left')
                ->join('menteeprogrammes p', 'mp.ProgrammeID = p.ProgrammeID', 'left')
				->where('m.Status', 1);
				
		if($programme	!=0)		
				$this->datatables->where('p.ProgrammeID', $programme);
                
        //$this->db->group_by("m.MentorID"); 
        //$this->db->group_by("m.MentorName"); 

        echo $this->datatables->generate();
    }
	
	
	function menteesourcedatatable() {
        $source	=	$_REQUEST['source'];

        $this->datatables->select('m.MenteeName,ms.MenteeSourceName', FALSE)
                ->from('mentee m')
                ->join('menteetosource us', 'us.MenteeID = m.MenteeID', 'INNER')
                ->join('menteesourcenames ms', 'us.MenteeSourceID = ms.MenteeSourceID', 'INNER')
                ->where('m.Status', 1);
       
		if($source	!=0)		
				$this->datatables->where('us.MenteeSourceID', $source);
                

        echo $this->datatables->generate();
    }

	function teamdatatable($startDate, $endDate, $role, $source) {
		 $this->datatables->select('t.TeamName,m.MentorName,count(DISTINCT mi.TeamID) as total,sum(mi.MeetingElapsedTime) as totaltime,ur.UserRoleName as userRoleName,ms.MentorSourceName as MentorSourceName,group_concat(m.MentorName) as MentorMember,group_concat(me.MenteeName) as MenteeMember', FALSE)
                ->from('meetinginfo mi')
				->join('team t', 't.TeamId = mi.TeamID', 'left')
				->join('teammember tm', 'tm.TeamID = t.TeamId  AND tm.ApprovalStatus = "joined"', 'left')
                ->join('mentor m', 'mi.MentorID=m.MentorID AND m.MentorID = tm.UserID AND tm.UserType = 0 AND m.Status=1', 'left')
                ->join('mentee me', 'mi.MenteeID=me.MenteeID AND mi.MenteeID = tm.UserID AND tm.UserType = 1 AND me.Status=1', 'left')
                ->join('usertorole utr', 'utr.MentorID = m.MentorID', 'left')
                ->join('user_roles ur', 'utr.userRoleID = ur.userRoleID', 'left')
                ->join('usertosource us', 'us.MentorID = m.MentorID', 'left')
                ->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID', 'left')
                
                
				->where('t.TeamStatus', 'active')
				->group_by('tm.TeamId');
		
		if($role != 0){
			$this->datatables->where('utr.userRoleID ', $role);
		}
		if($source != 0){
			$this->datatables->where('ms.MentorSourceID ', $source);
		}
		
		
		if($startDate != ""){
            $this->db->where('mi.MeetingStartDatetime >=', $startDate);
		}
		
		if($endDate != ""){
			$this->db->where('mi.MeetingEndDatetime <=', $endDate);
		}
		echo $this->datatables->generate();
	}	

}
