<?php

class Teams extends Admin_Controller {

    function __construct() {
		
		error_reporting(E_ALL);
		ini_set('display_errors', "ON");
        parent::__construct();
        $this->load->model('admin/teams_model');
		$this->load->model('admin/clientskillavailable_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // Teams Listing LISTING 
    //=========================================
    public function index() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );

        $res['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading($res['config'][0]->teamManager.' Admin','Team Name','Team Avatar', 'Team Description', 'Total Members', 'Max Members',$res['config'][0]->mentorName.'s Member' ,$res['config'][0]->menteeName.'s Member', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/teams/list', $res);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $this->datatables->select("mtt.TeamId,IF(mtt.TeamOwnerUserType = 0, (select mt.MentorName from mentor_mentor mt where mt.MentorID=mtt.TeamOwnerUserID), (select me.MenteeName from mentor_mentee me where me.MenteeID=mtt.TeamOwnerUserID)) AS TeamOwner, mtt.TeamName,IF(mtt.TeamImage = '','<img width=\"100px\" src=\"http://melstm.net/mentor/assets/admin/images/admin/group_avatar.png\"/>',CONCAT('<img width=\"100px\" src=\"http://melstm.net/mentor/assets/admin/images/admin/', mtt.TeamImage,' \"/>')) AS TeamImage,mtt.TeamDescription,  (COUNT( mtm.TeamMemberID ) +1) AS UserCount,mtt.MaxMembers,CASE WHEN group_concat(mt.MentorName) IS NOT NULL  then (group_concat(mt.MentorName SEPARATOR ', ')) else 'No Mentor Added' END  as MentorName,CASE WHEN group_concat(me.MenteeName) IS NOT NULL  then (group_concat(me.MenteeName SEPARATOR ', ')) else 'No Mentee Added' END  as MenteeName", FALSE)
                ->unset_column('Actions')
                ->unset_column('mtt.TeamId')
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/teams'), 'mtt.TeamId')
                ->from('mentor_team mtt')
                ->join('mentor_teammember mtm', 'mtm.TeamId = mtt.TeamId AND mtm.ApprovalStatus = "joined"', 'left')
				->join('mentor_mentor mt', 'mt.MentorID = mtm.UserID AND mtm.UserType = 0', 'left')
				->join('mentor_mentee me', 'me.MenteeID = mtm.UserID AND mtm.UserType = 1', 'left')
                ->where('mtt.TeamStatus =', 'active')
                ->group_by('mtt.TeamId');

        echo $this->datatables->generate();
    }

    //=========================================
    // Abbreviation ADD 
    //=========================================
    public function add($id = '') {
        /*if (!empty($_POST)) {
           echo "<pre>";
            print_r($_POST);
            exit;
        }*/
        $data['config'] = $this->settings_model->getConfig();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('TeamName', $data['config'][0]->teamManager.' Name', 'required');
		//$this->form_validation->set_rules('MaxMembers', 'Max Members', 'required|numeric');
		//$this->form_validation->set_rules('ownerType', 'Owner Type', 'required');
		//$this->form_validation->set_rules('Mentors', 'Mentors', 'required');
		//$this->form_validation->set_rules('Mentees', 'Mentees', 'required');	
		//$this->form_validation->set_rules('skills[]', 'skills', 'required');
        //$this->form_validation->set_rules('Weight', 'Weight', '');

		
        if ($this->form_validation->run() === TRUE) {
			$skills = implode(",",$this->input->post('skills'));

			if($this->input->post('ownerType') == 0){
				$TeamOwnerUserID = 	$this->input->post('Mentors');
			}else{
				$TeamOwnerUserID = 	$this->input->post('Mentees');
			}
            $data = array(
                'TeamName' => $this->input->post('TeamName'),
				'TeamDescription' => $this->input->post('TeamDescription'),
				'TeamOwnerUserID' => $TeamOwnerUserID,
				'MaxMembers' => $this->input->post('MaxMembers'),
				'TeamOwnerUserType' => $this->input->post('ownerType'),
				'TeamSkills' => $skills,	
                'Weight' => $this->input->post('Weight'),
                'UpdatedDate' => date('Y-m-d H:i:s')
            );
            if ($id == 0) {
				$filename = "";
					//print_r($_FILES['photo']);exit;
                    if ($_FILES['photo']['error'] == 0) {
                        $pathMain = $this->config->item('base_images');
                        $pathThumb = $this->config->item('base_images');
                        $imageNameTime = time();
                        $path = $_SERVER['DOCUMENT_ROOT'] . "/mentor/assets/admin/images/admin/" . $imageNameTime . ".png";
                        $filename = $imageNameTime . ".png";
                        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $path)) {
							
                            $status = 1;
                        } else {
                            $status = 0;
                        }
	                        @unlink($pathMain . $uploadedFileName);
                    }

				
				$data['TeamImage'] = $filename;
                $data['CreatedDate'] = date('Y-m-d H:i:s');
                $result = $this->db->insert('team', $data);
                $id = $this->db->insert_id();

                if (!empty($this->input->post('TeamMentors'))) {
                    foreach ($this->input->post('TeamMentors') as $mentors) {
                        $this->teams_model->addMentorToTeam($id, $mentors);
                    }
                }

                if (!empty($this->input->post('TeamMentees'))) {
                    foreach ($this->input->post('TeamMentees') as $mentees) {
                        $this->teams_model->addMenteeToTeam($id, $mentees);
                    }
                }


                $this->session->set_flashdata('success', 'Team Added Successfully');
            } else {

				$filename = "";
                    if ($_FILES['photo']['error'] == 0) {
                        $pathMain = $this->config->item('base_images');
                        $pathThumb = $this->config->item('base_images');
                        $imageNameTime = time();
                        $path = $_SERVER['DOCUMENT_ROOT'] . "/mentor/assets/admin/images/admin/" . $imageNameTime . ".png";
                        $filename = $imageNameTime . ".png";
                        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $path)) {
							
                            $status = 1;
                        } else {
                            $status = 0;
                        }
	                        @unlink($pathMain . $uploadedFileName);
                    }

				
				if($filename != '') {
                    $data['TeamImage'] = $filename;
                }
                $this->db->where('TeamId', $id);
                $result = $this->db->update('team', $data);

                if (!empty($this->input->post('TeamMentors'))) {
                    $this->db->delete('teammember', array('TeamId' => $id, 'UserType' => 0));
                    foreach ($this->input->post('TeamMentors') as $mentors) {
                        $this->teams_model->addMentorToTeam($id, $mentors);
                    }
                }

                if (!empty($this->input->post('TeamMentees'))) {
                    $this->db->delete('teammember', array('TeamId' => $id, 'UserType' => 1));
                    foreach ($this->input->post('TeamMentees') as $mentees) {
                        $this->teams_model->addMenteeToTeam($id, $mentees);
                    }
                }

                $this->session->set_flashdata('success', 'Team Updated Successfully');
            }
            redirect('admin/teams');
        }

		
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');

        $teamData = array();

        if ($id) {
            $teamData = array();
            $teams = $this->teams_model->getTeamDetails($id);
            if ($teams) {
                $teamUsers = $this->teams_model->getTeamDetails($id, true);
                $teamData['TeamName'] = $teams[0]->TeamName;
				$teamData['TeamDescription'] = $teams[0]->TeamDescription;
				$teamData['TeamOwnerUserID'] = $teams[0]->TeamOwnerUserID;
				$teamData['TeamOwnerUserType'] = $teams[0]->TeamOwnerUserType;
				$teamData['MaxMembers'] = $teams[0]->MaxMembers;
				$teamData['TeamImage'] = $teams[0]->TeamImage;
                $teamData['TeamID'] = $teams[0]->TeamId;
                $teamData['Weight'] = $teams[0]->Weight;
                $teamData['TeamMentorsID'] = array();
                $teamData['TeamMenteesID'] = array();
                foreach ($teamUsers as &$users) {
                    if ($users->UserType == 0) {
                        $teamData['TeamMentors'][] = $this->teams_model->getMentorMenteeDetails($users);
                        $teamData['TeamMentorsID'][] = $users->UserID;
                    } else if ($users->UserType == 1) {
                        $teamData['TeamMentees'][] = $this->teams_model->getMentorMenteeDetails($users);
                        $teamData['TeamMenteesID'][] = $users->UserID;
                    }
                }
            }
        } else {
            $teamData['TeamName'] = '';
			$teamData['TeamDescription'] = '';
			$teamData['TeamOwnerUserType'] = '';
			$teamData['TeamOwnerUserID'] = '';
			$teamData['MaxMembers'] = '';
			$teamData['TeamImage'] = '';
            $teamData['TeamID'] = '';
            $teamData['Weight'] = '';
            $teamData['TeamMentors'][] = '';
            $teamData['TeamMentees'][] = '';
            $teamData['TeamMentorsID'][] = '';
            $teamData['TeamMenteesID'][] = '';
        }

        $teamData['MentorsList'] = $this->teams_model->getAllMentors();
        $teamData['MenteesList'] = $this->teams_model->getAllMentees();


		$teamData['teamMembersMentor'] 	= $this->teams_model->getTeamMembersMentor();	 
		$teamData['MentorsListDrp'] = $this->teams_model->getAllMentorsDrp();
        $teamData['MenteesListDrp'] = $this->teams_model->getAllMenteesDrp();

		//echo "<pre>";print_r($teamData['teamMembersMentor']);exit;
        $data['id'] = $id;
        $data['teams'] = $teamData;

		$team_skills = array();
        if ($id) {
            $team_skills = $this->teams_model->get_teamSkills($id);
			$data['teamSkill'] = explode(",",$team_skills[0]['TeamSkills']);
        }else
			$data['teamSkill'] = $team_skills;
        $data['id'] = $id;
        
		$data['skills'] = $this->clientskillavailable_model->get_Skills();

		//echo "<pre>";print_r($data['teamSkill']);exit;

        $this->template->content->view('admin/teams/add', $data);
        $this->template->publish_admin();
    }

    /*public function testGroup($id = '') {
        $data = array();
        $groups = $this->groups_model->getGroupDetails($id);
        if ($groups) {
            $groupUsers = $this->groups_model->getGroupDetails($id, true);
            $data['GroupName'] = $groups[0]->GroupName;
            $data['GroupID'] = $groups[0]->GroupID;
            foreach ($groupUsers as &$users) {
                if ($users->UserType == 1) {
                    $data['GroupMentors'][] = $this->groups_model->getMentorMenteeDetails($users);
                } else if ($users->UserType == 0) {
                    $data['GroupMentees'][] = $this->groups_model->getMentorMenteeDetails($users);
                }
            }
        }
        echo "<pre>";
        print_r($data);
        exit;
    }*/

    //=========================================
    // Team EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    //=========================================
    // Team DELETE 
    //=========================================
    public function delete($id) {
        $ds = $this->teams_model->delete_team($id);
        $this->session->set_flashdata('success', 'Team Deleted Successfully');
        redirect('admin/teams');
    }

	public function getMentors(){
		$MentorsList = $this->teams_model->getAllMentors();
		$mentorList =array();
		foreach($MentorsList as $val){
			$temparr = array($val->MentorId=>$val->MentorName);
			array_push($mentorList,$temparr);
		}
		echo json_encode($mentorList);
	}

}
