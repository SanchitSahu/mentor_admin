<?php

class Emailtemplate extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/emailtemplate_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        //print_r($res);exit;
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            //print_r($x);exit;
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // Email Template LISTING 
    //=========================================
    public function index() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );

        $res['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Subject', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/emailtemplate/list', $res);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $this->datatables->select("EmailTemplateId, EmailTemplateTitle, Weight", FALSE)
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/emailtemplate'), 'EmailTemplateId')
                ->unset_column('EmailTemplateId')
                ->from('emailtemplate')
                ->where('Status !=', '0');

        echo $this->datatables->generate();
    }

    //=========================================
    // Email Template ADD 
    //=========================================
    public function add($id = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('TemplateSubject', 'Template Subject', 'required');
        $this->form_validation->set_rules('TemplateMessage', 'Template Message', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {

            $data = array(
                'EmailTemplateTitle' => $this->input->post('TemplateSubject'),
                'Content' => $this->input->post('TemplateMessage'),
                'Weight' => $this->input->post('Weight'),
                'ModifiedDate' => date('Y-m-d H:i:s')
            );
            if ($id == 0) {
                $data['CreatedDate'] = date('Y-m-d H:i:s');
                $result = $this->db->insert('emailtemplate', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', 'Email Template Added Successfully');
            } else {
                $this->db->where('EmailTemplateId', $id);
                $result = $this->db->update('emailtemplate', $data);
                $this->session->set_flashdata('success', 'Email Template Updated Successfully');
            }
            redirect('admin/emailtemplate');
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('emailtemplate');
        $emailTemplate = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $emailTemplate[$field] = '';
            }
        }

        if ($id) {
            $emailTemplate = $this->emailtemplate_model->get_emailtempate_details($id);
        }
        $data['id'] = $id;
        $data['emailtemplate'] = $emailTemplate;

        $this->template->content->view('admin/emailtemplate/add', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // Email Template EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    //=========================================
    // Email Template DELETE 
    //=========================================
    public function delete($id) {
        $ds = $this->emailtemplate_model->delete_emailtemplate($id);
        $this->session->set_flashdata('success', 'Client Source Delete Successfully');
        redirect('admin/emailtemplate');
    }
    
    public function test_mail($id) {
        $email = $this->emailtemplate_model->get_emailtempate_details($id);
        //echo "<pre>";print_r($email);exit;
		$to	=	array('khushbu.20490@gmail.com','sanchit.sahu03@gmail.com');
        $emailArr = array(
            'toEmail' => $to,
            'subject' => $email['EmailTemplateTitle'],
            'body' => $email['Content']
        );
        send_mail($emailArr);
    }

}
