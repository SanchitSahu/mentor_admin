<?php

class CareCriteria extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/careCriteria_model');
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // Careweighting LISTING 
    //=========================================
    public function index() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $res['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Name', 'Importance', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/careCriteria/list', $res);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $this->datatables->select("CareCriteriaID, Name, CareValues, Weight", FALSE)
                ->unset_column('CareCriteriaID')
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/careCriteria'), 'CareCriteriaID')
                ->from('careCriteria')
                ->where('Status', '1');
        echo $this->datatables->generate();
    }

    //=========================================
    // careCriteria ADD 
    //=========================================
    public function add($id = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('Name', 'Name', 'required');
        $this->form_validation->set_rules('CareValues', 'Care Values', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {

            $data = array(
                'Name' => $this->input->post('Name'),
                'CareValues' => $this->input->post('CareValues'),
                'Weight' => $this->input->post('Weight')
            );
            if ($id == 0) {
                $result = $this->db->insert('careCriteria', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', 'Care Criteria Added Successfully');
            } else {
                $this->db->where('CareCriteriaID', $id);
                $result = $this->db->update('careCriteria', $data);
                $this->session->set_flashdata('success', 'Care Criteria Updated Successfully');
            }
            redirect('admin/careCriteria');
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('careCriteria');
        $careweighting = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $careweighting[$field] = '';
            }
        }

        if ($id) {
            $careweighting = $this->careCriteria_model->get_careweighting($id);
        }
        $data['id'] = $id;
        $data['careweighting'] = $careweighting;

        $this->template->content->view('admin/careCriteria/add', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // careCriteria EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    public function checkCareCriteriaExist() {
        if ($this->input->is_ajax_request()) {
            $Name = $this->input->post('Name');
            $CareCriteriaID = $this->input->post('CareCriteriaID');
            if ($CareCriteriaID == "") {
                $flag = $this->careCriteria_model->checkCareCriteria($Name);
            } else {
                $flag = $this->careCriteria_model->checkCareCriteriaEdit($Name, $CareCriteriaID);
            }
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    //=========================================
    // Delete careCriteria   
    //=========================================
    public function delete($id) {
        $this->careCriteria_model->delete_careweighting($id);
        $this->session->set_flashdata('success', 'careCriteria Delete Successfully');
        redirect('admin/careCriteria');
    }

}
