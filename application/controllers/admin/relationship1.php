<?php
class Relationship extends Admin_Controller {

    function __construct() {
        parent::__construct();
		if(!loginCheck()){redirect('admin');}
		$this->load->model('admin/relationship_model');
		$res	=	loadDatabase();
		//print_r($res);exit;
		if($res != 0){
			$x	=	$this->load->database($res,TRUE);
			//print_r($x);exit;
			$this->db	=	$x;	
		}else{
			redirect("400.shtml");exit;
		}
    }

	// load dashboard
    public function index(){ 
	
		$res['config']	=	$this->settings_model->getConfig();
		$mentorFinal	=	array();
		$menteeFinal	=	array();
		$res['skill']	=	$this->relationship_model->get_skills();
		//$res['menteeSkill']	=	$this->relationship_model->get_skills_mentee();
		$mentor	=	$this->relationship_model->get_mentors();
		
		foreach($mentor as $val){
			$mentorID	=	$val->MentorID;
			$mentorRes	=	$this->relationship_model->get_mentors_skill($mentorID);
			//echo "<pre>";print_r($mentorRes);exit;
			$skillArr	=	array();
			foreach($mentorRes as $val2){
				
				array_push($skillArr,$val2->SkillName);
			}
			$test	=	implode(",",$skillArr);
			//echo "<pre>";print_r($skillArr);
			/*add new key value pair for skill */
			
			$getMetor	=	$this->relationship_model->getAssingedMentee($mentorID);
			//echo "<pre>";print_r($getMetor);exit;
			$assignedMentee	=	array();
			foreach($getMetor as $val2){
				
				array_push($assignedMentee,$val2->MenteeName);
			}
			$assignedMentee	=	implode(",",$assignedMentee);
			
			$val->skill = $test;
			$val->assignedMentee = $assignedMentee;
            $mentorFinal[] = $val;
		}
		//exit;
		//echo "<pre>";print_r($mentorFinal);exit;
		
		//$getAssingedMentee	=	array();
		
		
		
		$res['mentor']	=	$mentorFinal;
		$mentee	=	$this->relationship_model->get_mentees();
		
		
		foreach($mentee as $val){
			$menteeID	=	$val->MenteeID;
			$menteeRes	=	$this->relationship_model->get_mentees_skill($menteeID);
			//echo "<pre>";print_r($mentorRes);exit;
			$skillArr	=	array();
			foreach($menteeRes as $val2){
				
				array_push($skillArr,$val2->SkillName);
			}
			$test	=	implode(",",$skillArr);
			//echo "<pre>";print_r($skillArr);
			/*add new key value pair for skill */
			
			$getMentee	=	$this->relationship_model->getAssingedMentor($menteeID);
			//echo "<pre>";print_r($getMetor);exit;
			$assignedMentor	=	array();
			foreach($getMentee as $val2){
				
				array_push($assignedMentor,$val2->MentorName);
			}
			$assignedMentor	=	implode(",",$assignedMentor);
			
			$val->skill = $test;
			$val->assignedMentor = $assignedMentor;
            $menteeFinal[] = $val;
		}
		$res['mentee']	=	$menteeFinal;

		
		
		//$this->template->publish_admin();
		$this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');     
        
        $this->template->content->view('admin/relationship/relationship',$res);
        $this->template->publish_admin();
		
    }
	
	public function addRelation()
	{
		$config	=	$this->settings_model->getConfig();
		$mentor	=	$_REQUEST['mentor'];
		$mentee	=	$_REQUEST['mentee'];
		if($mentor != "" && $mentor !=0 && $mentee != "" && $mentee !=0){	
			$getMentorName	=	$this->relationship_model->getMentorName($mentor);
			$mentorName		=	$getMentorName[0]->MentorName;
			
			$getMenteeName	=	$this->relationship_model->getMenteeName($mentee);
			$menteeName		=	$getMenteeName[0]->MenteeName;
			
			$checkRelation	=	$this->relationship_model->checkRelation($mentor,$mentee);
			if($checkRelation == 1)
				echo "This ".$config[0]->mentorName." ".$mentorName."is already assigned to this ".$config[0]->menteeName." ".$menteeName;
			else{
				$res	=	$this->relationship_model->addRelation($mentor,$mentee);
				if($res != 0)
					echo $config[0]->mentorName." ".$mentorName." is assigned to ".$config[0]->menteeName." ".$menteeName." successfully";
				else
					echo "Could not assign  ".$config[0]->mentorName." ".$mentorName." to ".$config[0]->menteeName." ".$menteeName;
			}	
		}else{
			echo "Please select ".$config[0]->mentorName." and ".$config[0]->menteeName." to create this relationship";
		}	
	}
	
	public function getMentorBySkill()
	{
		$config	=	$this->settings_model->getConfig();
		$skillID	=	$_REQUEST['skillID'];
		$res		=	$this->relationship_model->getMentorBySkill($skillID);
		//print_r($res);exit;
		echo '<div class="user-slider">
              <div>
                <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
                  <!-- Indicators --> 
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">';
		$j=0;
		if(empty($res)){ ?>
				<div class="item <?php if($j == 0) echo "active"?>" id="0">
				  <div class="user-img-new">No current <?php echo $config[0]->mentorName?> has the selected skills  <!--<img src="<?php //echo base_url()?>/assets/admin/images/no-mentee1.jpg" class="img-circle">--></div>
				  <div class="user-name"><span><a href="#">(0)
					<div class="connect-friend">  
					</div>
					</a> </span> </div>
				  <form>
					<textarea name="skill" placeholder="No skills specified yet"></textarea>
				   
				  </form>
				</div>
		<?php }else{
			
			foreach($res as $val){
				$mentorID	=	$val->MentorID;
				$mentorRes	=	$this->relationship_model->get_mentors_skill($mentorID);
				//echo "<pre>";print_r($mentorRes);exit;
				$skillArr	=	array();
				foreach($mentorRes as $val2){
					
					array_push($skillArr,$val2->SkillName);
				}
				$test	=	implode(",",$skillArr);
				//echo "<pre>";print_r($skillArr);
				
				$getMetor	=	$this->relationship_model->getAssingedMentee($mentorID);
				//echo "<pre>";print_r($getMetor);exit;
				$assignedMentee	=	array();
				foreach($getMetor as $val2){
					
					array_push($assignedMentee,$val2->MenteeName);
				}
				$assignedMentee	=	implode(",",$assignedMentee);
				
				/*add new key value pair for skill */
				$val->skill = $test;
				$val->assignedMentee = $assignedMentee;
				$mentorFinal[] = $val;
			}
			//exit;
			//echo "<pre>";print_r($mentorFinal);exit;
			$mentor	=	$mentorFinal;
			
			
		foreach($mentor as $val){ 	
			$assignedMentee	=	explode(",",$val->assignedMentee); 	
					if($assignedMentee[0]	!=	"")
						$cnt			=	count($assignedMentee);	
					else
						$cnt	=0;
		?>
			
			<div class="item <?php if($j == 0) echo "active"?>" id="<?php echo $val->MentorID?>">
			  <div class="user-img"><img src="<?php echo base_url()?>/assets/admin/images/<?php echo "mentor".$val->MentorID.".jpg";?>" class="img-circle"></div>
			  <div class="user-name"><?php echo $val->MentorName ;?><span><a href="#">(<?php echo $cnt;?>)
				<div class="connect-friend">
				  <ul>
					<?php if($val->assignedMentee == ""){ ?>
									<li>No <?php echo $config[0]->menteeName?> Assigned</li>
							<?php }else {
								
								foreach($assignedMentee as $s) { ?>
									<li><?php echo $s;?></li>
							<?php } }?>
				  </ul>
				</div>
				</a> </span> </div>
			  <form>
				<?php $skillArr	=	explode(",",$val->skill); ?>
                        <textarea name="skill" cols="" rows="" placeholder="No skills specified yet"><?php foreach($skillArr as $s)echo $s."\n" ?></textarea>
			   
			  </form>
			</div>
                  
		<?php $j++; }
		}
		echo '</div>
                  
                  <!-- Left and right controls --> 
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <span class="left-errow" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <span class="right-errow" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
              </div>
            </div>|';
		
			if(empty($res)){
				echo "<option value='0'>select ".$config[0]->mentorName."</option>";
			}else{	
				foreach($res as $val){
					echo "<option value=".$val->MentorID.">".$val->MentorName."</option>";
				} 
			}	

		?>	
		<script type="text/javascript">
		$('.carousel').carousel({
			pause: true,
			interval: false
		});
		</script>	
		<?php 		
			
	}
	
	public function getMenteeBySkill()
	{
		$config	=	$this->settings_model->getConfig();
		$skillID	=	$_REQUEST['skillID'];
		$res		=	$this->relationship_model->getMenteeBySkill($skillID);
		//print_r($res);exit;
		echo '<div class="user-slider">
              <div>
                <div id="myCarousel1" class="carousel slide" data-ride="carousel"> 
                  <!-- Indicators --> 
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">';
		$j=0;
		
		if(empty($res)){ ?>
				<div class="item <?php if($j == 0) echo "active"?>" id="0">
				  <div class="user-img-new">No current <?php echo $config[0]->menteeName?> needs the selected skills  <!--<img src="<?php //echo base_url()?>/assets/admin/images/no-mentee1.jpg" class="img-circle">--></div>
				  <div class="user-name"><span><a href="#">(0)
					<div class="connect-friend">
					  
					</div>
					</a> </span> </div>
				  <form>
					<textarea name="skill" placeholder="No needed skills specified yet"></textarea>
				   
				  </form>
				</div>
		<?php }else{
			
			
			
			foreach($res as $val){
				$menteeID	=	$val->MenteeID;
				$menteeRes	=	$this->relationship_model->get_mentees_skill($menteeID);
				//echo "<pre>";print_r($mentorRes);exit;
				$skillArr	=	array();
				foreach($menteeRes as $val2){
					
					array_push($skillArr,$val2->SkillName);
				}
				$test	=	implode(",",$skillArr);
				//echo "<pre>";print_r($skillArr);
				$getMentee	=	$this->relationship_model->getAssingedMentor($menteeID);
				//echo "<pre>";print_r($getMetor);exit;
				$assignedMentor	=	array();
				foreach($getMentee as $val2){
					
					array_push($assignedMentor,$val2->MentorName);
				}
				$assignedMentor	=	implode(",",$assignedMentor);
				
				/*add new key value pair for skill */
				$val->skill = $test;
				$val->assignedMentor = $assignedMentor;
				$menteeFinal[] = $val;
			}
			$mentee	=	$menteeFinal;
			
			foreach($res as $val){ 	
				$assignedMentor	=	explode(",",$val->assignedMentor); 
						if($assignedMentor[0]	!=	"")
							$cnt	=	count($assignedMentor);
						else
							$cnt	=0;
			?>
				
				<div class="item <?php if($j == 0) echo "active"?>" id="<?php echo $val->MenteeID?>">
				  <div class="user-img"><img src="<?php echo base_url()?>/assets/admin/images/<?php echo "mentee".$val->MenteeID.".jpg";?>" class="img-circle"></div>
				  <div class="user-name"><?php echo $val->MenteeName ;?><span><a href="#">(<?php echo $cnt;?>)
					<div class="connect-friend">
					  <ul>
						 <?php if($val->assignedMentor == ""){ ?>
									<li>No <?php echo $config[0]->mentorName?> Assigned</li>
							<?php }else {
								
								foreach($assignedMentor as $s) { ?>
									<li><?php echo $s;?></li>
							<?php } }?>
					  </ul>
					</div>
					</a> </span> </div>
				  <form>
					<?php $skillArr	=	explode(",",$val->skill); ?>
                        <textarea name="skill" cols="" rows="" placeholder="No skills specified yet"><?php foreach($skillArr as $s)echo $s."\n" ?></textarea>
				   
				  </form>
				</div>
					  
			<?php $j++; }
		}
			echo '</div>
					  
					  <!-- Left and right controls --> 
					   <a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev"> <span class="left-errow" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next"> <span class="right-errow" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
				  </div>
				</div>|';
			
			if(empty($res)){
				echo "<option value='0'>select ".$config[0]->menteeName."</option>";
			}else{	
				foreach($res as $val){
					echo "<option value=".$val->MenteeID.">".$val->MenteeName."</option>";
				}
			}
		?>	
		<script type="text/javascript">
		$('.carousel').carousel({
			pause: true,
			interval: false
		});
		</script>	
		<?php 	
	}

	public function checkRelation()
	{
		$mentor	=	$_REQUEST['mentor'];
		$mentee	=	$_REQUEST['mentee'];
		
		$res	=	$this->relationship_model->checkAssignedRelation($mentor,$mentee);
		echo $res;
	}
	
	public function removeRelation()
	{
		$config	=	$this->settings_model->getConfig();
		$mentor	=	$_REQUEST['mentor'];
		$mentee	=	$_REQUEST['mentee'];
		
		$getMentorName	=	$this->relationship_model->getMentorName($mentor);
		$mentorName		=	$getMentorName[0]->MentorName;
		
		$getMenteeName	=	$this->relationship_model->getMenteeName($mentee);
		$menteeName		=	$getMenteeName[0]->MenteeName;
		
		if($mentor != "" && $mentor !=0 && $mentee != "" && $mentee !=0){	
			$res	=	$this->relationship_model->removeRelation($mentor,$mentee);
			if($res != 0)
				echo $config[0]->mentorName." ".$mentorName." is removed from ".$config[0]->menteeName." ".$menteeName." successfully";
			else
				echo "Could not remove ".$config[0]->mentorName." ".$mentorName." to ".$config[0]->menteeName." ".$menteeName;	
		}else{
			echo "Please select ".$config[0]->mentorName." and ".$config[0]->menteeName." to remove this relationship";
		}	
	}
	
	public function getDummyProfile()
	{
		$config	=	$this->settings_model->getConfig();
		/*echo '<div class="user-slider">
              <div>
                <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
                  <!-- Indicators --> 
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">';*/
		?>
				<div class="item active" id="noMentor">
				  <div class="user-img"><img src="<?php echo base_url()?>/assets/admin/images/no-mentor.jpg" class="img-circle"></div>
				  <div class="user-name"><span><a href="#">(0)
					<div class="connect-friend">
					  <ul>
						<li>No <?php echo $config[0]->menteeName ?> Assigned</li>
					  </ul>
					</div>
					</a> </span> </div>
				  <form>
					<textarea name="skill" placeholder="No skills specified yet"></textarea>
				   
				  </form>
				</div>      
		<?php 
		echo "|";
		/*echo  '</div>
                  
                  <!-- Left and right controls --> 
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <span class="left-errow" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <span class="right-errow" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
              </div>
            </div>|<div class="user-slider">
              <div>
                <div id="myCarousel1" class="carousel slide" data-ride="carousel"> 
                  <!-- Indicators --> 
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">';*/
		?>
				<div class="item active" id="noMentee">
				  <div class="user-img"><img src="<?php echo base_url()?>/assets/admin/images/no-mentee.jpg" class="img-circle"></div>
				  <div class="user-name"><span><a href="#">(0)
					<div class="connect-friend">
					  <ul>
						<li>No <?php echo $config[0]->mentorName?> Assigned</li>
					  </ul>
					</div>
					</a> </span> </div>
				  <form>
					<textarea name="skill" placeholder="No needed skills specified yet"></textarea>
				   
				  </form>
				</div>
		<?php 
			/*echo '</div>
					  
					  <!-- Left and right controls --> 
					   <a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev"> <span class="left-errow" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next"> <span class="right-errow" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
				  </div>
				</div>';*/	

				
	}
	
	public function getAllMentor()
	{
		$config	=	$this->settings_model->getConfig();
		$res	=	$this->relationship_model->getAllMentor();
		echo "<option value='0'>select ".$config[0]->mentorName."</option>";
		foreach($res as $val){
					echo "<option value=".$val->MentorID.">".$val->MentorName."</option>";
		} 
	}
	
}
