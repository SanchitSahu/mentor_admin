<?php

class Bulkupload extends Admin_Controller {

    var $fields;/** columns names retrieved after parsing */
    var $line_separator = "\n";/** separator used to explode each line */
    var $field_separator = ";";/** character used to separate field names and field values */
    var $enclosure = '"';/** quotes surrounding field names and values */
    var $max_row_size = 4096;/** maximum row size to be used for decoding */
    var $header_line_no = 0; //Line number at which header is present, used for error reporting
    var $errors_array = array(); //array of errors to be issues to user
    var $csv_key = array();
    var $csv_array = array();

    function __construct() {
        parent::__construct();
        $this->load->model('admin/bulkupload_model');
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
        $db_name = $this->db->database;
    }

    // <editor-fold defaultstate="collapsed" desc="Error issuing functions">

    /**
     * issue_missing_required_fields_error_msg 
     * 
     * @param array $missing_field_names array of missing fields
     * @param array $expected_count count of expected values
     * @param array $actual_count count of actual values
     * @param array $session User session
     * 
     * @return null
     */
    function issue_missing_required_fields_error_msg($missing_field_names, $expected_count, $actual_count, $session, $file = "", $lineno = 0, $tablename = "current", $return_result = false) {
        if ($tablename != "current")
            $tablename = $_POST['tableName'];
        $msg = "";

        $lineno = $lineno + $this->header_line_no + 1;

        $msg .= "<p class='alertify-csv-error-text'>The format of line #$lineno is incompatible with the $tablename table format.<br/><br/>" .
                "<span class='alertify-csv-error-label'>Expected a minimum of </span>" . $expected_count . " values<br/>" .
                "<span class='alertify-csv-error-label'>But read only&nbsp;&nbsp;</span>" . $actual_count . " values<br/>" .
                "<span class='alertify-csv-error-label'>Missing values for fields: </span>" . implode(',', $missing_field_names) . "<br/><br/>" .
                "Please see the Sample File for the correct CSV format.<br/><br/></p>";
        if ($return_result) {
            return $msg;
        }
		$EF	=	implode(",", $missing_field_names);	
		$addError = $this->bulkupload_model->addError($msg, $tableName,$lineNo,$missing_field_names,'Missing values for fields');		
        $session->set_flashdata('error', $msg);
        redirect('admin/' . $_POST['urlPath']);
    }

    function issue_missing_fields_error($required_fields, $given_fields, $file = '', $tableName = 'current', $lineNo = '', $returnError = false) {
        $error = "";
        if ($tableName != "current")
            $tableName = $_POST['tableName'];

        $missing_fields = array_diff($required_fields, $given_fields);

//        $lineno = $lineno + $this->header_line_no + 1;
//        $msg .= "<p class='alertify-csv-error-text'>The format of the record you tried to upload is incompatible with the " . $tablename . " table.<br/><br/>" .
        $error .= "<p class='alertify-csv-error-text'>The format of line #$lineNo is incompatible with the $tableName table format.<br/><br/>" .
                "<span class='alertify-csv-error-label'>Expected a minimum of </span>" . implode(',', $required_fields) . " values<br/>" .
                "<span class='alertify-csv-error-label'>But read only&nbsp;&nbsp;</span>" . implode(',', $given_fields) . " values<br/>" .
                "<span class='alertify-csv-error-label'>Missing values for fields: </span>" . implode(',', $missing_fields) . "<br/><br/>" .
                "Please see the Sample File for the correct CSV format.<br/><br/></p>";
        if ($returnError) {
            return $error;
        }
		$EF	=	implode(",", $missing_fields);	
		$addError = $this->bulkupload_model->addError($error, $tableName,$lineNo,$EF,'Missing values for fields');		
        $this->session->set_flashdata('error', $error);
        redirect('admin/' . $_POST['urlPath']);
    }

    function issue_unknown_fields_error($required_fields = '', $given_fields = '', $unknown_fields = '', $file = '', $tableName = 'current', $lineNo = '', $returnError = false) {
        $error = "";

        if (!empty($unknown_fields)) {
            $extra_fields = $unknown_fields;
        } else if (!empty($required_fields) && !empty($given_fields)) {
            $extra_fields = array_diff($given_fields, $required_fields);
        }
        //echo "<pre>";
        //print_r($expected_headers);
        //print_r($given_headers);
        //print_r($extra_fileds);
        //exit;

        $error .= "<p class='alertify-csv-error-text'>The format of line #$lineNo is incompatible with the $tableName table format.<br/><br/>" .
                "<span class='alertify-csv-error-label'>Unknown heading(s) is/are: </span>" . implode(",", $extra_fields) . "<br/>" .
                //"<span class='alertify-csv-error-label'>but read:&nbsp;&nbsp;</span>" . implode(",", $given_headers) . "<br/></br>" .
                "Please see the Sample File for the correct CSV format.<br/><br/></p>";
		$EF	=	implode(",", $extra_fields);
		$addError = $this->bulkupload_model->addError($error, $tableName,$lineNo,$EF,'Unknown heading');		
        $this->session->set_flashdata('error', $error);
        redirect('admin/' . $_POST['urlPath']);
    }

    function issue_extra_fields_error($required_fields, $given_fields, $file = '', $tableName = 'current', $lineNo = '', $returnError = false) {
        $error = "";

        $error .= "<p class='alertify-csv-error-text'>The Line with #" . $lineNo . " has too many fields in it and is ignored:<br/><br/>" .
                "<span class='alertify-csv-error-label'>" . implode(", ", $given_fields) . "</span><br/><br/>" .
                "Please see the Sample File for the correct CSV format.<br/><br/></p>";
		$EF	=	implode(",", $given_fields);	
		$addError = $this->bulkupload_model->addError($error, $tableName,$lineNo,$EF,'Unknown fields');			
        $this->session->set_flashdata('error', $error);
        redirect('admin/' . $_POST['urlPath']);
    }
    
    /**
     * issue_too_many_vars_error_msg - this message can be issued from more than one place
     * @param array $max_values_values_expected - maximum number of values expected
     * @param array $actual_values_count_given - actual # of values received
     * @return null 
     */
    function issue_too_many_vars_error_msg($max_values_count_expected, $actual_values_count_given, $session, $file = "", $lineno = 0, $tablename = "current", $return_result = false) {
        if ($tablename != "current")
            $tablename = $_POST['tableName'];
        $msg = "";
        if ($file) {
            //$msg = "<p><span class='alertify-csv-error-label'>Warning</span> in " . $file . "#" . $lineno . ":<br/><br/></p>";
        }
        $lineno = $lineno + $this->header_line_no + 1;
        $msg .= "<p class='alertify-csv-error-text'>The format of the file you tried to upload is incompatible with the " . $tablename . " table. Too many values given at line $lineno.<br/><br/>" .
                "<span class='alertify-csv-error-label'>Expected: </span>A maximum of " . $max_values_count_expected .
                " values expected<br/>" .
                "<span class='alertify-csv-error-label'>But read:&nbsp;&nbsp;</span>" . $actual_values_count_given . " values.<br/></br>" .
                "Please see the Sample File for the correct CSV format.<br/><br/></p>";
		$addError = $this->bulkupload_model->addError($error, $tableName,$lineNo,$actual_values_count_given,'Too many values given');			
        $session->set_flashdata('error', $msg);

        if ($return_result) {
            return $msg;
        }
        redirect('admin/' . $_POST['urlPath']);
    }

// </editor-fold>


    /* analyze_header - analyze CSV header and break into required and optional fields
     *
     * Argument: $header_line
     * Returns:  An array containing required and optional fields.
     */
    function analyze_header($header_line) {
        $line = trim($header_line);
        $names_of_required_fields = array();
        $names_of_optional_fields = array();
        $space_pos = strpos($line, " ");
        if ($space_pos == FALSE) {
            $names_of_required_fields = str_getcsv($line, ";");
        } else {
            $names_of_required_fields = str_getcsv(substr($line, 0, $space_pos - 1), ";");
            $names_of_optional_fields = str_getcsv(substr($line, $space_pos + 1), ";");
        }
        $header_field_names = array(
            "REQUIRED" => $names_of_required_fields,
            "OPTIONAL" => $names_of_optional_fields
        );
        return $header_field_names;
    }

    /**
     * readCSV - read CSV formatted file and delete all comment lines
     * 
     * @param file $csvfile CSV formatted file
     * @param boolean $header_exists TRUE if the file has a line of field names
     * @return  null
     */
    function readCSV($csvfile, $header_exists = TRUE) {
        if (!file_exists($csvfile) || !is_readable($csvfile))
            return FALSE;

        $raw_lines = file($csvfile);

        $header_found = FALSE;
        $header_field_names = array();
        $rows_of_field_values = array();
		//echo "<pre>";print_R($raw_lines);exit;
        foreach ($raw_lines as $row_key => $raw_line) {
            $line = trim($raw_line);
            if (substr($line, 0, 1) == '"' || substr($line, 0, 1) == "#" || strlen($line) == 0) { // ignore comment and blank lines
                continue;
            }

            if (!$header_exists || $header_found) {
                $field_values = str_getcsv($line, ";", '"');
				//print("<pre>");print_r($field_values);print("</pre>");
				$field_valuesNew	=	array();
				//$new	=	rtrim($field_values[0],',');
				//array_push($field_valuesNew,$new);
				
				//print("<pre>");print_r($field_valuesNew);print("</pre>");exit;
                $rows_of_field_values[] = $field_values;
            } else { // csv header found
                $header_found = TRUE;
                $this->header_line_no = $row_key + 1;
				//$lineNew	=	rtrim($line,',');
				//print_R($lineNew);exit;
                $header_field_names = $this->analyze_header($line);
            }
        }
        return array(
            "KEYS" => $header_field_names,
            "VALUES" => $rows_of_field_values
        );
    }

    /*
     * parse_file - tokenize contents of csv file
     *
     * This functions, unlike the older version of this function does not do any semantic checking of input.
     * However, it does separate the csv header into keys and the rest of the file into an array of rows of 
     * arrays of values.
     *
     * Returns:
     */

    function parse_file($file, $session, $csv_type) {
        $data = array();
        $data['error'] = array();
        $content = array();
        $result = array();

        $rows = $this->readCSV($file);
		
		//print("<pre>");print_r($rows);print("</pre>");exit;
        $rows_count = count($rows);
        $keys_array = $rows['KEYS'];
        $keys_count = count($keys_array);
        $raw_values_array = $rows["VALUES"];
        $values_array = array();

        foreach ($raw_values_array as $row) {
            $values_array[] = $this->escape_string($row);
        }
        list($csv_header, $rekeyed_values) = $this->relate_field_names_to_field_values($keys_array, $values_array, $session, $csv_type);

//echo "<b>function parse_file</b> ".__FILE__." line# ".__line__;
//print("<pre>");print_r($csv_header);print("</pre>");
//print("<pre>");print_r($rekeyed_values);print("</pre>");
        return array($csv_header, $rekeyed_values);
    }

    function escape_string($data) {
        $result = array();
        foreach ($data as $row) {
            $result[] = str_replace('"', '', $row);
        }
        return $result;
    }

    /* parse_file_relate_keys_to_values - tries to do a 1-to-1 match between csv header field names and field values
     *
     * Returns an array with keys from the csv header and field values from the remaining rows in the file.
     *
     */

    function relate_field_names_to_field_values($field_names, $field_values, $session, $csv_type) {
        $ekeyed_array = array();
        $required_field_names_count = count($field_names['REQUIRED']);
        $all_field_names_array = array_merge($field_names["REQUIRED"], $field_names["OPTIONAL"]);
        $all_field_names_count = count($all_field_names_array);
        $value_rows_count = count($field_values);

        switch ($csv_type) {
            case 'careCriteria':
                $table_header_columns = json_decode(careCriteria, TRUE);
                break;
            case 'meetingplace':
                $table_header_columns = json_decode(meetingplace, TRUE);
                break;
            case 'meetingtype':
                $table_header_columns = json_decode(meetingtype, TRUE);
                break;
            case 'menteeAction':
                $table_header_columns = json_decode(menteeAction, TRUE);
                break;
            case 'userProgramme':
                $table_header_columns = json_decode(userProgramme, TRUE);
                break;
            case 'clientSource':
                $table_header_columns = json_decode(menteeSource, TRUE);
                break;
            case 'mentorAction':
                $table_header_columns = json_decode(mentorAction, TRUE);
                break;
            case 'userSource':
                $table_header_columns = json_decode(userSource, TRUE);
                break;
            case 'skill':
                $table_header_columns = json_decode(skill, TRUE);
                break;
            case 'topic':
                $table_header_columns = json_decode(topic, TRUE);
                break;
            case 'userRoles':
                $table_header_columns = json_decode(userRoles, TRUE);
                break;
            case 'comments/listAccept':
            case 'comments/listReject':
                $table_header_columns = json_decode(acceptrejectComments, TRUE);
                break;
            case 'abbreviations':
                $table_header_columns = json_decode(abbreviations, TRUE);
                break;
            case 'mentors':
                $table_header_columns = json_decode(MentorHeader, TRUE);
                break;
            case 'mentees':
                $table_header_columns = json_decode(MenteeHeader, TRUE);
                break;
            case 'subtopic':
                $table_header_columns = json_decode(SubtopicHeader, TRUE);
                break;
            case 'mentorSkillAvailable':
                $table_header_columns = json_decode(MentorHavingSkills, TRUE);
                break;
            case 'menteeSkillNeeded':
                $table_header_columns = json_decode(MenteeNeedsSkills, TRUE);
                break;
            default :
                break;
        }

        $sql_required = $table_header_columns['REQUIRED'];
        $sql_optional = $table_header_columns['OPTIONAL'];
        $csv_required = $field_names['REQUIRED'];
        $csv_optional = $field_names['OPTIONAL'];
        $csv_values = $field_values;

//echo "<b>function relate_field_names_to_field_values</b> ".__FILE__." line# ".__line__;
//echo "<pre>";
//print("<b>field_values</b><br>");print_r($field_values);
//print("<b>table_header_columns</b><br>");print_r($table_header_columns);
//print("<b>sql_required</b><br>");print_r($sql_required);
//print("<b>sql_options</b><br>");print_r($sql_optional);
//print("<b>csv_required</b><br>");print_r($csv_required);
//print("<b>csv_optional</b><br>");print_r($csv_optional);
//echo "</pre>";
//exit;

        if (count($csv_required) < count($sql_required)) {
            //echo "<pre>csv_required";
            //print_r($csv_required);
            //echo "sql_required";
            //print_r($sql_required);
            //echo "</pre>";
            //exit;
            $this->issue_missing_fields_error($sql_required, $csv_required, '', $_POST['tableName'], $this->header_line_no);
        } else if (count($csv_required) > count($sql_required)) {
            //$this->issue_extra_fields_error($sql_required, $csv_required, '', $_POST['tableName'], $this->header_line_no);
            $this->issue_unknown_fields_error($sql_required, $csv_required, '', '', $_POST['tableName'], $this->header_line_no);
        } else if (count($csv_required) == count($sql_required)) {
            $invalid_required_head = array_diff($sql_required, $csv_required);
            $invalid_required_csvhead = array_diff($csv_required, $sql_required);
            $invalid_required = array_merge($invalid_required_csvhead, $invalid_required_head);

            if (count($invalid_required) > 0) {
                $this->issue_unknown_fields_error('', '', $invalid_required, '', $_POST['tableName'], $this->header_line_no);
            }
        }
        if (count($csv_optional) > count($sql_optional)) {
            $this->issue_unknown_fields_error($sql_optional, $csv_optional, '', '', $_POST['tableName'], $this->header_line_no);
        } else if ((count($csv_optional) < count($sql_optional)) || (count($csv_optional) == count($sql_optional))) {
            $unknown_optionals = array_diff($csv_optional, $sql_optional);
            if (count($invalid_required) > 0) {
                $this->issue_unknown_fields_error('', '', $unknown_optionals, '', $_POST['tableName'], $this->header_line_no);
            }
        }
//exit;



        $truncated_field_names_array = array();
        $rekeyed_array = array();
        $this->errors_array = array();
        $proccessable_rows = array();
        $skip_execution = false;

        foreach ($csv_values as $key_row => $value_row) {
            if ($skip_execution)
                continue;
            $field_values_count = count($value_row);

            // set up default values
            $truncated_field_names_array = $all_field_names_array;
            $truncated_field_values_array = $value_row;

            //echo $field_values_count;
            //echo $all_field_names_count;
            //print_R($truncated_field_names_array);
            //print_R($truncated_field_values_array);
            //exit;
            if ($all_field_names_count < $field_values_count) {  // more values than names
                $this->errors_array[] = $this->issue_too_many_vars_error_msg($all_field_names_count, $field_values_count, $session, __FILE__, ($key_row + 1), $_POST['tableName'], true);
                $skip_execution = true;
                continue;     // ignore this row of values
            } else if ($all_field_names_count == $field_values_count) { // same number of names and values
                // do nothing since size of arrays the same
            } else {       // not enough values for required fields
                if ($required_field_names_count > $field_values_count) {
                    $missing_value_names = array_values(array_diff_key($field_names['REQUIRED'], $value_row));
                    $this->errors_array[] = $this->issue_missing_required_fields_error_msg($missing_value_names, $required_field_names_count, $field_values_count, $session, __FILE__, ($key_row + 1), $_POST['tableName'], true);
                    $skip_execution = true;
                    continue;     // ignore this row of values
                }
            }
            $values_count = count($truncated_field_values_array);
            $header_count = count($truncated_field_names_array);
            for ($i = 0; $i < ($header_count - $values_count); $i++) {
                $truncated_field_values_array[] = '';
            }
            $rekeyed_array[] = array_combine($truncated_field_names_array, $truncated_field_values_array);
            //echo "<br>errors_array";print_r($errors_array);
            //echo "<br>proccessable_rows";print_r($proccessable_rows);
            //echo "<br>truncated_field_values_array";print_r($truncated_field_values_array);
            //echo "<br>rekeyed_array";print_r($rekeyed_array);
        }


        //exit;
        //if (!empty($errors_array) && count($errors_array) > 0) {
        //    $session->set_flashdata('error', implode("<br><br>", $errors_array));
        //    redirect('admin/' . $_POST['urlPath']);
        //}
        //echo "<br>truncated_field_values_array";print_r($truncated_field_values_array);
        //echo "<br>rekeyed_array";print_r($rekeyed_array);
        //exit;
//echo "<b>function relate_field_names_to_field_values</b> ".__FILE__." line# ".__line__;
//echo "<pre>";
//print("<b>truncated_field_names_array</b><br>");print_r($truncated_field_names_array);
//print("<b>rekeyed_array</b><br>");print_r($rekeyed_array);
//echo "</pre>";
//exit;
        return array($truncated_field_names_array, $rekeyed_array);
    }

    function processData($result, $tbl_name, $csvfile) {
        $result = array_values($result);

        //print_r($result);
        //print_r($tbl_name);
        //print_r($csvfile);
        //print_r($this->errors_array);
        //exit;

        $res = $this->bulkupload_model->saveDataFromCSV($result, $tbl_name);

        if (!empty($this->errors_array) && count($this->errors_array) > 0) {
            $this->session->set_flashdata('error', implode("<br><br>", $this->errors_array));
            redirect('admin/' . $_POST['urlPath']);
        }

        $msg = 'Table Data Successfully added';   // default db status  message
        $this->session->set_flashdata('success', $msg);
        $type = 'xls';
        $title = 'Table_upload_status';

        $csv_array = $this->csv_array;
        $csvkeys = $this->csv_key;
//        list($csvkeys, $csv_array) = $this->parse_file($csvfile, $this->session);
//        print_r($csvkeys);
//        print_r($csv_array);
//        exit;
        $csv = $csv_array;
        $new2 = array();
        foreach ($csv as $key => $csv_val) {
            foreach ($result as $res) {
                $newArray = array_keys($csv_val); //get keys of any table dynamically
                $oldArray = array_keys($res);  //get keys of any table dynamically
                if ($csv_val[$newArray[0]] == $res[$newArray[0]]) {
                    $csv[$key]['success'] = "uploaded successfully";
                    $new2[$key] = $csv[$key]; // catch it
                    break;
                } else {
                    $csv[$key]['success'] = "Not uploaded";
                    $new2[$key] = $csv[$key]; // catch it	
                }
            }
        }

        $k = 0;
        foreach ($new2[$k] as $r_key => $r_value) {
            $dataHeader[$k] = $r_key;
            $k++;
        }
        $type = 'xls';
        $title = 'Data_upload_status';
        redirect('admin/' . $_POST['urlPath']);
        exit;

        $this->export_generic($new2, $dataHeader, $type, NULL, $title, $_POST['urlPath']);
    }

    public function export_generic($export_data, $headers, $xls = NULL, $pdf = NULL, $title, $urlPath) {
        $this->load->library('excel');

        $heading = $headers;

        $no_of_cols = count($heading);
        //Loop Heading
        $rowNumberH = 1;
        $colH = 'A';
        $data = $export_data;
        //Loop Result
        $row = 2;
        $no = 1;

        $columns = array('0' => 'A', '1' => 'B', '2' => 'C', '3' => 'D', '4' => 'E', '5' => 'F', '6' => 'G', '7' => 'H', '8' => 'I', '9' => 'J', '10' => 'K', '11' => 'L', '12' => 'M', '13' => 'N', '14' => 'O', '15' => 'P', '16' => 'Q', '17' => 'R', '18' => 'S', '19' => 'T', '20' => 'U', '21' => 'V', '22' => 'W', '23' => 'X', '24' => 'Y', '25' => 'Z');

        //Freeze pane
        if (!empty($data)) {
            $this->excel->setActiveSheetIndex(0);
            foreach ($heading as $h) {
                $this->excel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
                $this->excel->getActiveSheet()->getColumnDimension($colH)->setWidth(25);
                $colH++;
            }
            $row = 2;
            $sQty = 0;
            $pQty = 0;
            $sAmt = 0;
            $pAmt = 0;
            $pl = 0;
            foreach ($data as $data_row) {
                for ($i = 0; $i < $no_of_cols; $i++) {
                    $this->excel->getActiveSheet()->setCellValue($columns[$i] . $row, $data_row[$heading[$i]]);
                }
                $row++;
            }

            $filename = $title;
            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            if ($pdf != NULL) {
                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );
                $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                require_once(APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php");
                $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                $rendererLibrary = 'MPDF';
                $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                    die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' . PHP_EOL . ' as appropriate for your directory structure');
                }

                header('Content-Type: application/pdf');
                header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                header('Cache-Control: max-age=0');

                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                $objWriter->save('php://output');
                exit();
            }
            if ($xls != NULL) {
                $this->excel->getActiveSheet()->getStyle('C2:G' . $row)->getAlignment()->setWrapText(true);
                ob_clean();
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                header('Cache-Control: max-age=0');
                header('Refresh: 0; url=' . $urlPath);
                ob_clean();
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                var_dump($objWriter->save('php://output'));
                exit;
            }
        }
    }

    function genericUploader() {
        //error_reporting(E_ALL);
        //ini_set("display_errors","ON");
        $tbl_name = $_POST['tableName'];
        $imageNameTime = time();
		
		$tempFolders	=	$_SERVER['DOCUMENT_ROOT'] . "/mentor/assets/admin/csv/". date('mdY')."/";
		
		if (!file_exists($tempFolders)) {
			mkdir($tempFolders, 0777, true);
		}
		
        $csvfile = $tempFolders . $tbl_name . "_" . $imageNameTime . ".csv";
        $filename = $imageNameTime . ".csv";

        if (move_uploaded_file($_FILES["userfile"]["tmp_name"], $csvfile)) {
            list($csv_header, $rekeyed_values) = $this->parse_file($csvfile, $this->session, $_POST['urlPath']);

            $this->csv_key = $csv_header;
            $this->csv_array = $rekeyed_values;


            switch ($_POST['urlPath']) {
                case 'mentors':$this->processMentorData($rekeyed_values, $tbl_name, $csvfile);
                    break;
                case 'mentees':$this->processMenteeData($rekeyed_values, $tbl_name, $csvfile);
                    break;
                case 'subtopic':$this->processSubtopicData($rekeyed_values, $tbl_name, $csvfile);
                    break;
                case 'mentorSkillAvailable':$this->processMentorSkillData($rekeyed_values, $tbl_name, $csvfile);
                    break;
                case 'menteeSkillNeeded':$this->processMenteeSkillData($rekeyed_values, $tbl_name, $csvfile);
                    break;
                default : $this->processData($rekeyed_values, $tbl_name, $csvfile);
                    break;
            }
        } else {
            $error = "Failed to upload " . $tbl_name . "(1)";
            $this->session->set_flashdata('error', $error);
            redirect('admin/' . $_POST['urlPath']);
        }
    }

    function processMentorData($result, $tbl_name, $csvfile) {

        $uploadArray = array();
        foreach ($result as $val) {
            $RoleCheck = $val['MentorRole'];
            $SourceCheck = $val['MentorSource'];
            $mentorName = $val['MentorName'];
            $MentorPhone = $val['MentorPhone'];
            $MentorEmail = $val['MentorEmail'];
            $MentorStatus = !empty($val['Status']) ? $val['Status'] : '';
            $MentorWeight = !empty($val['Weight']) ? $val['Weight'] : '';
			
			$IsSubadmin = !empty($val['IsSubadmin']) ? $val['IsSubadmin'] : '';
			
            $roleres = $this->bulkupload_model->checkRoleExists($RoleCheck);
            $sourceres = $this->bulkupload_model->checkSourceExists($SourceCheck);
            
            if(empty($roleres)){
                $roleres[0]->userRoleID = 0;
            }
            if(empty($sourceres)){
                $sourceres[0]->MentorSourceID = 0;
            }

            if (!empty($roleres) && !empty($sourceres)) {
                $roleId = $roleres[0]->userRoleID;
                $sourceId = $sourceres[0]->MentorSourceID;
                $addMentor = $this->bulkupload_model->addMentor($mentorName, $MentorStatus, $MentorWeight);
                if ($addMentor != 0 && !is_array($addMentor)) {
                    $addContact = $this->bulkupload_model->addContact($addMentor, $MentorPhone, $MentorEmail, $mentorName, $IsSubadmin);
                    $addRole = $this->bulkupload_model->addRole($addMentor, $roleId);
                    $addSource = $this->bulkupload_model->addSource($addMentor, $sourceId);
                    $dataUploded = array(
                        "MentorName" => $mentorName,
                        "MentorPhone" => $MentorPhone,
                        "MentorEmail" => $MentorEmail,
                        "MentorRole" => $RoleCheck,
                        "MentorSource" => $SourceCheck,
						"IsSubadmin" => $IsSubadmin,
                        "Upload Status" => "Uploaded successfully"
                    );
                    array_push($uploadArray, $dataUploded);
                } else if(!empty($addMentor)){
					$MentorID	=	$addMentor[0]->MentorID;
					$addContact = $this->bulkupload_model->updateContact($MentorID, $MentorPhone, $MentorEmail);
                    $addRole = $this->bulkupload_model->updateRole($MentorID, $roleId);
                    $addSource = $this->bulkupload_model->updateSource($MentorID, $sourceId);
                    $dataUploded = array(
                        "MentorName" => $mentorName,
                        "MentorPhone" => $MentorPhone,
                        "MentorEmail" => $MentorEmail,
                        "MentorRole" => $RoleCheck,
                        "MentorSource" => $SourceCheck,
                        "Upload Status" => "Updated successfully"
                    );
					array_push($uploadArray, $dataUploded);
				} else {
                    $dataUploded = array(
                        "MentorName" => $mentorName,
                        "MentorPhone" => $MentorPhone,
                        "MentorEmail" => $MentorEmail,
                        "MentorRole" => $RoleCheck,
                        "MentorSource" => $SourceCheck,
                        "Upload Status" => "Upload failed as Advisor could not be created"
                    );
                    array_push($uploadArray, $dataUploded);
                }
            } else {

                $dataUploded = array(
                    "MentorName" => $mentorName,
                    "MentorPhone" => $MentorPhone,
                    "MentorEmail" => $MentorEmail,
                    "MentorRole" => $RoleCheck,
                    "MentorSource" => $SourceCheck
                );
                if (empty($roleres) && empty($sourceres)) {
                    $dataUploded['Upload Status'] = 'Specified Role and Source could not be found';
                } else if (empty($roleres)) {
                    $dataUploded['Upload Status'] = 'Specified Role could not be found';
                } else if (empty($sourceres)) {
                    $dataUploded['Upload Status'] = 'Specified Source could not be found';
                }
                array_push($uploadArray, $dataUploded);
            }
        }

        if (!empty($this->errors_array) && count($this->errors_array) > 0) {
            //$session->set_flashdata('error', implode("<br><br>", $errors_array));
            $this->session->set_flashdata('error', implode("<br><br>", $errors_array));
            redirect('admin/' . $_POST['urlPath']);
        }

        $msg = 'Table Data Successfully added';   // default db status  message
        $this->session->set_flashdata('success', $msg);
        $type = 'xls';
        $title = 'Table_upload_status';

        foreach ($result as $key => $csv_val) {
            //print_r($csv_val);
            foreach ($uploadArray as $res) {
                //print_r($res);exit;
                $newArray = array_keys($csv_val); //get keys of any table dynamically
                $oldArray = array_keys($res); //get keys of any table dynamically
                //echo "<pre>";
                //print_r($newArray);
                //print_r($oldArray);exit;
                if ($csv_val[$newArray[0]] == $res[$newArray[0]]) {
                    //echo $csv_val[$newArray[0]];
                    //echo $res[$newArray[0]]."<br>";
                    //$csv[$key]['status'] = "uploaded successfully";
                    $new2[$key] = $uploadArray[$key]; // catch it
                    break;
                } else {
                    //$csv[$key]['status'] = "Not uploaded";
                    $new2[$key] = $uploadArray[$key]; // catch it	
                }
            }
        }

        $k = 0;
        foreach ($new2[$k] as $r_key => $r_value) {
            $dataHeader[$k] = $r_key;
            $k++;
        }
        //print_r($dataHeader);
        //exit;
        $type = 'xls';
        $title = 'Data_upload_status';
        redirect('admin/' . $_POST['urlPath']);
        exit;

        $this->export_generic($new2, $dataHeader, $type, NULL, $title, $_POST['urlPath']);
    }

    function processMenteeData($result, $tbl_name, $csvfile) {
        $uploadArray = array();
        foreach ($result as $val) {
            $RoleCheck = $val['MenteeProgramme'];
            $SourceCheck = $val['MenteeSource'];
            $menteeName = $val['MenteeName'];
            $MenteePhone = $val['MenteePhone'];
            $MenteeEmail = $val['MenteeEmail'];
            $MenteeStatus = !empty($val['Status']) ? $val['Status'] : '';
            $MenteeWeight = !empty($val['Weight']) ? $val['Weight'] : '';
            $roleres = $this->bulkupload_model->checkRoleExistsMentee($RoleCheck);
            $sourceres = $this->bulkupload_model->checkSourceExistsMentee($SourceCheck);
            
            if(empty($roleres)){
                $roleres[0]->ProgrammeID = 0;
            }
            if(empty($sourceres)){
                $sourceres[0]->MenteeSourceID = 0;
            }

            if (!empty($roleres) && !empty($sourceres)) {
                $roleId = $roleres[0]->ProgrammeID;
                $sourceId = $sourceres[0]->MenteeSourceID;
                $addMentee = $this->bulkupload_model->addMentee($menteeName, $MenteeStatus, $MenteeWeight);
                if ($addMentee != 0 && !is_array($addMentee)) {
                    $addContact = $this->bulkupload_model->addMenteeContact($addMentee, $MenteePhone, $MenteeEmail,$menteeName);
                    $addRole = $this->bulkupload_model->addMenteeRole($addMentee, $roleId);
                    $addSource = $this->bulkupload_model->addMenteeSource($addMentee, $sourceId);
                    $dataUploded = array(
                        "MenteeName" => $menteeName,
                        "MenteePhone" => $MenteePhone,
                        "MenteeEmail" => $MenteeEmail,
                        "MenteeProgramme" => $RoleCheck,
                        "MenteeSource" => $SourceCheck,
                        "Upload Status" => "Uploaded Successfully"
                    );
                    array_push($uploadArray, $dataUploded);
                } else if(!empty($addMentee)){
					$MenteeID	=	$addMentee[0]->MenteeID;
					$addContact = $this->bulkupload_model->updateMenteeContact($MenteeID, $MenteePhone, $MenteeEmail);
                    $addRole = $this->bulkupload_model->updateMenteeRole($MenteeID, $roleId);
                    $addSource = $this->bulkupload_model->updateMenteeSource($MenteeID, $sourceId);
                    $dataUploded = array(
                        "MenteeName" => $menteeName,
                        "MenteePhone" => $MenteePhone,
                        "MenteeEmail" => $MenteeEmail,
                        "MenteeProgramme" => $RoleCheck,
                        "MenteeSource" => $SourceCheck,
                        "Upload Status" => "Updated Successfully"
                    );
                    array_push($uploadArray, $dataUploded);
				} else {
                    $dataUploded = array(
                        "MenteeName" => $menteeName,
                        "MenteePhone" => $MenteePhone,
                        "MenteeEmail" => $MenteeEmail,
                        "MenteeProgramme" => $RoleCheck,
                        "MenteeSource" => $SourceCheck,
                        "Upload Status" => "Upload failed as Entrepreneur could not be created"
                    );
                    array_push($uploadArray, $dataUploded);
                }
            } else {

                $dataUploded = array(
                    "MenteeName" => $menteeName,
                    "MenteePhone" => $MenteePhone,
                    "MenteeEmail" => $MenteeEmail,
                    "MenteeProgramme" => $RoleCheck,
                    "MenteeSource" => $SourceCheck
                );
                if (empty($roleres) && empty($sourceres)) {
                    $dataUploded['Upload Status'] = 'Specified Programme and Source could not be found';
                } else if (empty($roleres)) {
                    $dataUploded['Upload Status'] = 'Specified Programme could not be found';
                } else if (empty($sourceres)) {
                    $dataUploded['Upload Status'] = 'Specified Source could not be found';
                }
                array_push($uploadArray, $dataUploded);
            }
        }

        if (!empty($this->errors_array) && count($this->errors_array) > 0) {
            $session->set_flashdata('error', implode("<br><br>", $errors_array));
            redirect('admin/' . $_POST['urlPath']);
        }

        $error = 'Table Data Successfully added';
        $this->session->set_flashdata('success', $error);
        $type = 'xls';
        $title = 'Table_upload_status';

        foreach ($result as $key => $csv_val) {
            foreach ($uploadArray as $res) {
                $newArray = array_keys($csv_val); //get keys of any table dynamically
                $oldArray = array_keys($res); //get keys of any table dynamically
                if ($csv_val[$newArray[0]] == $res[$newArray[0]]) {
                    $new2[$key] = $uploadArray[$key]; // catch it
                    break;
                } else {
                    $new2[$key] = $uploadArray[$key]; // catch it	
                }
            }
        }

        $k = 0;
        foreach ($new2[$k] as $r_key => $r_value) {
            $dataHeader[$k] = $r_key;
            $k++;
        }
        $type = 'xls';
        $title = 'Data_upload_status';
        redirect('admin/' . $_POST['urlPath']);
        exit;

        $this->export_generic($new2, $dataHeader, $type, NULL, $title, $_POST['urlPath']);
    }

    function processSubtopicData($result, $tbl_name, $csvfile) {
        $uploadArray = array();
        foreach ($result as $val) {
            $TopicCheck = $val['Topic'];
            $subTopicName = $val['SubTopicDescription'];
            $Status = (isset($val['Status']) && $val['Status'] != '') ? $val['Status'] : '';
            $Weight = (isset($val['Weight']) && $val['Weight'] !='') ? $val['Weight'] : '';
            $topicres = $this->bulkupload_model->checkTopicExists($TopicCheck);

            if (!empty($topicres)) {
                $topicId = $topicres[0]->TopicID;
                $addSubTopic = $this->bulkupload_model->addSubTopic($subTopicName, $topicId, $Status, $Weight);
                if ($addSubTopic != 0) {
                    $dataUploded = array(
                        "Topic" => $TopicCheck,
                        "SubTopicDescription" => $subTopicName,
                        "Upload Status" => "Uploaded Successfully"
                    );
                    array_push($uploadArray, $dataUploded);
                } else {
                    $dataUploded = array(
                        "Topic" => $TopicCheck,
                        "SubTopicDescription" => $subTopicName,
                        "Upload Status" => "Upload failed as SubTopic could not be created"
                    );
                    array_push($uploadArray, $dataUploded);
                }
            } else {
                $dataUploded = array(
                    "Topic" => $TopicCheck,
                    "SubTopicDescription" => $subTopicName,
                    "Upload Status" => "Specified Topic could not be found"
                );
                array_push($uploadArray, $dataUploded);
            }
        }

        $error = 'Table Data Successfully added';
        $this->session->set_flashdata('success', $error);
        $type = 'xls';
        $title = 'Table_upload_status';

        foreach ($result as $key => $csv_val) {
            foreach ($uploadArray as $res) {
                $newArray = array_keys($csv_val); //get keys of any table dynamically
                $oldArray = array_keys($res); //get keys of any table dynamically
                if ($csv_val[$newArray[0]] == $res[$newArray[0]]) {
                    $new2[$key] = $uploadArray[$key]; // catch it
                    break;
                } else {
                    $new2[$key] = $uploadArray[$key]; // catch it	
                }
            }
        }

        $k = 0;
        foreach ($new2[$k] as $r_key => $r_value) {
            $dataHeader[$k] = $r_key;
            $k++;
        }

        $type = 'xls';
        $title = 'Data_upload_status';
        redirect('admin/' . $_POST['urlPath']);
        exit;

        $this->export_generic($new2, $dataHeader, $type, NULL, $title, $_POST['urlPath']);
    }
    
    function processMentorSkillData($result, $tbl_name, $csvfile) {
        $uploadArray = array();
        foreach ($result as $val) {
            $MentorName = $val['MentorName'];
            $SkillName = $val['SkillName'];
            $Status = (isset($val['Status']) && $val['Status'] != '') ? $val['Status'] : '';
            $Weight = (isset($val['Weight']) && $val['Weight'] !='') ? $val['Weight'] : '';

            $skillres = $this->bulkupload_model->checkSkillExists($SkillName);
            $mentorres = $this->bulkupload_model->checkMentorExists($MentorName);

            $dataUploded = array(
                "MentorName" => $MentorName,
                "SkillName" => $SkillName,
                "Status" => $Status,
                "Weight" => $Weight
            );

            if (!empty($mentorres)) {
                if (!empty($skillres)) {
                    $skillId = $skillres[0]->SkillID;
                    $mentorId = $mentorres[0]->MentorID;
                    $addSkillToUser = $this->bulkupload_model->addSkillToUser($mentorId, $skillId, 'mentor', $Status, $Weight);
                    if ($addSkillToUser === FALSE) {
                        $dataUploded["Upload Status"] = 'Upload failed as Mentor already has specified Skill.';
                        array_push($uploadArray, $dataUploded);
                    } else {
                        $dataUploded["Upload Status"] = 'Uploaded Successfully';
                        array_push($uploadArray, $dataUploded);
                    }
                } else {
                    $dataUploded["Upload Status"] = 'Specified Skill could not be found';
                    array_push($uploadArray, $dataUploded);
                }
            } else {
                $dataUploded["Upload Status"] = 'Specified Mentor could not be found';
                array_push($uploadArray, $dataUploded);
            }
        }

        $error = 'Table Data Successfully added';
        $this->session->set_flashdata('success', $error);
        $type = 'xls';
        $title = 'Table_upload_status';

        foreach ($result as $key => $csv_val) {
            foreach ($uploadArray as $res) {
                $newArray = array_keys($csv_val); //get keys of any table dynamically
                $oldArray = array_keys($res); //get keys of any table dynamically
                if ($csv_val[$newArray[0]] == $res[$newArray[0]]) {
                    $new2[$key] = $uploadArray[$key]; // catch it
                    break;
                } else {
                    $new2[$key] = $uploadArray[$key]; // catch it	
                }
            }
        }

        $k = 0;
        foreach ($new2[$k] as $r_key => $r_value) {
            $dataHeader[$k] = $r_key;
            $k++;
        }

        $type = 'xls';
        $title = 'Data_upload_status';
        redirect('admin/' . $_POST['urlPath']);
        exit;

        $this->export_generic($new2, $dataHeader, $type, NULL, $title, $_POST['urlPath']);
    }

    function processMenteeSkillData($result, $tbl_name, $csvfile) {
        $uploadArray = array();
        foreach ($result as $val) {
            $MenteeName = $val['MenteeName'];
            $SkillName = $val['SkillName'];
            $Status = (isset($val['Status']) && $val['Status'] != '') ? $val['Status'] : '';
            $Weight = (isset($val['Weight']) && $val['Weight'] !='') ? $val['Weight'] : '';

            $skillres = $this->bulkupload_model->checkSkillExists($SkillName);
            $menteeres = $this->bulkupload_model->checkMenteeExists($MenteeName);

            $dataUploded = array(
                "MenteeName" => $MenteeName,
                "SkillName" => $SkillName,
                "Status" => $Status,
                "Weight" => $Weight
            );
            if (!empty($menteeres)) {
                if (!empty($skillres)) {
                    $skillId = $skillres[0]->SkillID;
                    $menteeId = $menteeres[0]->MenteeID;
                    $addSkillToUser = $this->bulkupload_model->addSkillToUser($menteeId, $skillId, 'mentee', $Status, $Weight);
					
                    if ($addSkillToUser === FALSE) {
						$error = 'Upload failed as Mentee already needs specified Skill.';
                        $dataUploded['Upload Status'] = "Upload failed as Mentee already needs specified Skill.";
                        array_push($uploadArray, $dataUploded);
                    } else {
						/*$res = $this->bulkupload_model->saveDataFromCSV($result, $tbl_name);
						$error = 'Table Data Successfully added';
                        $dataUploded['Upload Status'] = "Uploaded Successfully.";
                        array_push($uploadArray, $dataUploded);*/
                    }
                } else {
					$error = 'Specified Skill could not be found.';
                    $dataUploded['Upload Status'] = "Specified Skill could not be found.";
                    array_push($uploadArray, $dataUploded);
                }
            } else {
				$error = 'Specified Mentee could not be found.';
                $dataUploded['Upload Status'] = "Specified Mentee could not be found.";
				
                array_push($uploadArray, $dataUploded);
            }
        }
       // $error = 'Table Data Successfully added';
        $this->session->set_flashdata('success', $error);
        $type = 'xls';
        $title = 'Table_upload_status';

        foreach ($result as $key => $csv_val) {
            foreach ($uploadArray as $res) {
                $newArray = array_keys($csv_val); //get keys of any table dynamically
                $oldArray = array_keys($res); //get keys of any table dynamically
                if ($csv_val[$newArray[0]] == $res[$newArray[0]]) {
                    $new2[$key] = $uploadArray[$key]; // catch it
                    break;
                } else {
                    $new2[$key] = $uploadArray[$key]; // catch it	
                }
            }
        }

        $k = 0;
        foreach ($new2[$k] as $r_key => $r_value) {
            $dataHeader[$k] = $r_key;
            $k++;
        }

        $type = 'xls';
        $title = 'Data_upload_status';
        redirect('admin/' . $_POST['urlPath']);
        exit;

        $this->export_generic($new2, $dataHeader, $type, NULL, $title, $_POST['urlPath']);
    }

}
