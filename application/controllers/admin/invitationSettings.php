<?php

class InvitationSettings extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/invitation_model');
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if(!loginCheck()){redirect('admin');}
		$res	=	loadDatabase();
		//print_r($res);exit;
		if($res != 0){
			$x	=	$this->load->database($res,TRUE);
			//print_r($x);exit;
			$this->db	=	$x;	
		}else{
			redirect("400.shtml");exit;
		}
    }

    //=========================================
    // Get InvitationSettings
    //=========================================
    public function index() {

       $invitation['invitation']	=	$this->invitation_model->getInvitationSetting();
        $this->template->content->view('admin/invitationSettings/add', $invitation);
        $this->template->publish_admin();
    }

    //=========================================
    // InvitationSettings ADD 
    //=========================================
    public function add($id = '') {
        	$this->load->library('form_validation');
        	$this->form_validation->set_rules('days', 'Days', 'required');
			$this->form_validation->set_rules('gap', 'Gap', 'required');

        if ($this->form_validation->run() === TRUE) {
            
            $data = array(
                'SendReminderDay'  => $this->input->post('days'),
                'SendReminderGap'  => $this->input->post('gap')
            );
            
            $res	=	$this->invitation_model->checkInvitation($this->input->post('days'),$this->input->post('gap'));
            if ($res == 0) {
                $result = $this->db->insert('mentor_reminder_settings', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', 'Mentor Configuration Added Successfully');
            }else{
            	
                $this->db->where('ConfigID', 1);
                $result = $this->db->update('mentor_reminder_settings', $data);
                $this->session->set_flashdata('success', 'Mentor Configuration Updated Successfully');
            }
            redirect('admin/invitationSettings');
        }
       
        $invitation['invitation']	=	$this->invitation_model->getInvitationSetting(); 
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');
        
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');
        
        $this->template->content->view('admin/invitationSettings/add', $invitation);
        $this->template->publish_admin();

    }
    
	
	function checkExpiryTime(){
		$data	=	$this->invitation_model->getSubDomains(); 
		//echo "<pre>";print_r($data);
		foreach($data as $val){
			$name	=	$val->subdomain_name;
			$getExpiryTime	=	$this->invitation_model->getExpiryTime($name); 
			//echo "<pre>";print_r($getExpiryTime);
			$expiryTime	=	explode(".",$getExpiryTime[0]->expiryTime);
			//echo "<pre>";print_r($expiryTime);
			//echo $expiryTime[0];echo "<br>";echo $expiryTime[1];echo "<br>";
			
				
			//echo $invitationExpiry;echo "<br>";
			//get Invitation data
			$getInvitationData	=	$this->invitation_model->getInvitationData($name); 
			//echo "<pre>";print_r($getInvitationData);
			if(empty($getInvitationData)) continue;
			foreach($getInvitationData as $invite){
				//echo "<pre>";print_r($invite);
				$inviteTime	=	$invite->inviteTime;
				
				if(isset($expiryTime[1])) {
					$invitationExpiry = date("Y-m-d H:i:s", strtotime($inviteTime ."+".$expiryTime[1]." minutes"));echo "<br>";
				}
				else {
					$invitationExpiry = date("Y-m-d H:i:s", strtotime($inviteTime ."+".$expiryTime[0]." hours"));
				}
				//echo $invitationExpiry;echo "<br>";
				//echo date('Y-m-d H:i:s');echo "<br>";
				
				
				if( strtotime($invitationExpiry) <= strtotime(date('Y-m-d H:i:s'))){
					//delete Invitation
					echo "delete Invitation";
					//echo "<pre>";print_r($invite);
					$getInvitationData	=	$this->invitation_model->deleteInvitation($name,$invite->InvitationId); 	
				}
			}
		}
	}
}