<?php

class Bulkupload_backup extends Admin_Controller {

    var $fields;/** columns names retrieved after parsing */
    var $line_separator = "\n";/** separator used to explode each line */
    var $field_separator = ";";/** character used to separate field names and field values */
    var $enclosure = '"';/** quotes surrounding field names and values */
    var $max_row_size = 4096;/** maximum row size to be used for decoding */
    var $header_line_no = 0; //Line number at which header is present, used for error reporting

    function __construct() {
        parent::__construct();
        $this->load->model('admin/bulkupload_model');
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        //print_r($res);exit;
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            //print_r($x);exit;
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
        $db_name = $this->db->database;
        //define("DB_NAME",$db_name);
    }

    // <editor-fold defaultstate="collapsed" desc="error issuing functions">

    /**
     * issue_too_many_vars_error_msg - this message can be issued from more than one place
     * @param array $max_values_values_expected - maximum number of values expected
     * @param array $actual_values_count_given - actual # of values received
     * @return null 
     */
    function issue_too_many_vars_error_msg($max_values_count_expected, $actual_values_count_given, $session, $file = "", $lineno = 0, $tablename = "current", $return_result = false) {
        if ($tablename != "current")
            $tablename = $_POST['tableName'];
        $msg = "";
        if ($file) {
            //$msg = "<p><span class='alertify-csv-error-label'>Warning</span> in " . $file . "#" . $lineno . ":<br/><br/></p>";
        }
        $lineno = $lineno + $this->header_line_no + 1;
        $msg .= "<p class='alertify-csv-error-text'>The format of the file you tried to upload is incompatible with the " . $tablename . " table. Too many values given at line $lineno.<br/><br/>" .
                "<span class='alertify-csv-error-label'>Expected: </span>A maximum of " . $max_values_count_expected .
                " values expected<br/>" .
                "<span class='alertify-csv-error-label'>But read:&nbsp;&nbsp;</span>" . $actual_values_count_given . " values.<br/></br>" .
                "Please see the Sample File for the correct CSV format.<br/><br/></p>";
        $session->set_flashdata('error', $msg);

        if ($return_result) {
            return $msg;
        }
        redirect('admin/' . $_POST['urlPath']);
    }

    /**
     * issue_missing_required_fields_error_msg 
     * 
     * @param array $missing_field_names array of missing fields
     * @param array $expected_count count of expected values
     * @param array $actual_count count of actual values
     * @param array $session User session
     * 
     * @return null
     */
    function issue_missing_required_fields_error_msg($missing_field_names, $expected_count, $actual_count, $session, $file = "", $lineno = 0, $tablename = "current", $return_result = false) {
        if ($tablename != "current")
            $tablename = $_POST['tableName'];
        $msg = "";
        if ($file) {
            //$msg = "<p><span class='alertify-csv-error-label'>Warning</span> in " . $file . "#" . $lineno . "<br/><br/></p>";
        }
        $lineno = $lineno + $this->header_line_no + 1;
//        $msg .= "<p class='alertify-csv-error-text'>The format of the record you tried to upload is incompatible with the " . $tablename . " table.<br/><br/>" .
        $msg .= "<p class='alertify-csv-error-text'>The format of line #$lineno is incompatible with the $tablename table format.<br/><br/>" .
                "<span class='alertify-csv-error-label'>Expected a minimum of </span>" . $expected_count . " values<br/>" .
                "<span class='alertify-csv-error-label'>But read only&nbsp;&nbsp;</span>" . $actual_count . " values<br/>" .
                "<span class='alertify-csv-error-label'>Missing values for fields: </span>" . implode(',', $missing_field_names) . "<br/><br/>" .
                "Please see the Sample File for the correct CSV format.<br/><br/></p>";
        if ($return_result) {
            return $msg;
        }
        $session->set_flashdata('error', $msg);
        redirect('admin/' . $_POST['urlPath']);
    }

    /**
     * issue_incompatible_csv_input_error_msg - this message can be issued from more than one place
     * 
     * @param array $expected_headers Array of expected headers
     * @param array $given_headers Array of given headers in csv file
     * 
     * @return type null
     */
    function issue_incompatible_csv_input_error_msg($expected_headers, $given_headers, $file = "", $lineno = 0, $tablename = "current") {
        if ($tablename != "current")
            $tablename = $_POST['tableName'];
        $error = "";
        if ($file) {
//            $error = "<p><span class='alertify-csv-error-label'>Detected: </span>" . $file . "#" . $lineno . "<br/><br/></p>";
        }
        $extra_fileds = array_diff($given_headers, $expected_headers);
        //echo "<pre>";
        //print_r($expected_headers);
        //print_r($given_headers);
        //print_r($extra_fileds);
        //exit;
//        $error .= "<p class='alertify-csv-error-text'>The format of the file you tried to upload is incompatible with the " . $tablename . " table.<br/><br/>" .
        $error .= "<p class='alertify-csv-error-text'>The format of line #$lineno is incompatible with the $tablename table format.<br/><br/>" .
                "<span class='alertify-csv-error-label'>Unknown heading(s) is/are: </span>" . implode(",", $extra_fileds) . "<br/>" .
                //"<span class='alertify-csv-error-label'>but read:&nbsp;&nbsp;</span>" . implode(",", $given_headers) . "<br/></br>" .
                "Please see the Sample File for the correct CSV format.<br/><br/></p>";
        $this->session->set_flashdata('error', $error);
        redirect('admin/' . $_POST['urlPath']);
    }

    /**
     * issue_too_many_csv_input_fields_error_msg - this message can be issued from more than one place
     * 
     * @param type $long_lines
     * @param type $file
     * @param type $lineno
     * @param type $tablename
     * @return type
     */
    function issue_too_many_csv_input_fields_error_msg($long_lines, $file = "", $lineno = 0, $tablename = "current") {
        if ($tablename != "current")
            $tablename = $_POST['tableName'];
        $error = "";
        if ($file) {
//            $error = "<p><span class='alertify-csv-error-label'>Detected: </span>" . $file . "#" . $lineno . "<br/><br/></p>";
        }
        $error .= "<p class='alertify-csv-error-text'>The following " . count($long_lines) .
                " lines had too many fields in them and were ignored: " .
                implode(", ", $long_lines) . "<br/><br/></p>" .
                "Please see the Sample File for the correct CSV format.<br/><br/></p>";
        $this->session->set_flashdata('error', $error);
        redirect('admin/' . $_POST['urlPath']);
    }

    // </editor-fold>

    /* analyze_header - analyze CSV header and break into required and optional fields
     *
     * Argument: $header_line
     * Returns:  An array containing required and optional fields.
     */

    function analyze_header($header_line) {
        $line = trim($header_line);
        $names_of_required_fields = array();
        $names_of_optional_fields = array();
        $space_pos = strpos($line, " ");
        if ($space_pos == FALSE) {
            $names_of_required_fields = str_getcsv($line, ";");
        } else {
            $names_of_required_fields = str_getcsv(substr($line, 0, $space_pos - 1), ";");
            $names_of_optional_fields = str_getcsv(substr($line, $space_pos + 1), ";");
        }
        $header_field_names = array(
            "REQUIRED" => $names_of_required_fields,
            "OPTIONAL" => $names_of_optional_fields
        );
        return $header_field_names;
    }

    /**
     * readCSV - read CSV formatted file and delete all comment lines
     * 
     * @param file $csvfile CSV formatted file
     * @param boolean $header_exists TRUE if the file has a line of field names
     * @return  null
     */
    function readCSV($csvfile, $header_exists = TRUE) {
        if (!file_exists($csvfile) || !is_readable($csvfile))
            return FALSE;

        $raw_lines = file($csvfile);

        $header_found = FALSE;
        $header_field_names = array();
        $rows_of_field_values = array();
        $file_handle = fopen($csvfile, 'r');
        foreach ($raw_lines as $row_key => $raw_line) {
            $line = trim($raw_line);
            if (substr($line, 0, 1) == "#") // ignore comment lines
                continue;
            if (strlen($line) == 0) // ignore empty lines
                continue;
            if (!$header_exists || $header_found) {
                $field_values = str_getcsv($line, ";", '"');
                $rows_of_field_values[] = $field_values;
            } else { // csv header found
                $header_found = TRUE;
                $this->header_line_no = $row_key + 1;
                $header_field_names = $this->analyze_header($line);
            }
            //echo "@ Line #$this->header_line_no";echo $line."<br>";
        }
        //exit;
        return array(
            "KEYS" => $header_field_names,
            "VALUES" => $rows_of_field_values
        );
    }

    /*
     * parse_file - tokenize contents of csv file
     *
     * This functions, unlike the older version of this function does not do any semantic checking of input.
     * However, it does separate the csv header into keys and the rest of the file into an array of rows of 
     * arrays of values.
     *
     * Returns:
     */

    function parse_file($file, $session, $csv_type = '') {
//print("<pre>[".__FILE__." @ ".__LINE__."]: this->line_separator='".$this->line_separator."'<br/></pre>");
//print("<pre>[".__FILE__." @ ".__LINE__."]: this->field_separator='".$this->field_separator."'<br/></pre>");
        $data = array();
        $data['error'] = array();
        $content = array();
        $result = array();

        $rows = $this->readCSV($file);
        //echo "<pre>";print("<pre>[".__FILE__." @ ".__LINE__."]: data=<br/>");print_r($rows);print("</pre>\n");exit;
        $rows_count = count($rows);
        $keys_array = $rows['KEYS'];
        $keys_count = count($keys_array);
        $raw_values_array = $rows["VALUES"];
        $values_array = array();

        foreach ($raw_values_array as $row) {
            $values_array[] = $this->escape_string($row);
        }
        //print("<pre>[".__FILE__." @ ".__LINE__."]: values_array=<br/>");print_r($values_array);print("</pre>\n");
        list($csv_header, $rekeyed_values) = $this->relate_field_names_to_field_values($keys_array, $values_array, $session, $csv_type);

//echo "<pre>";print("<pre>[".__FILE__." @ ".__LINE__."]: data=<br/>");print_r($csv_header);print_r($rekeyed_values);print("</pre>\n");exit;
        return array($csv_header, $rekeyed_values);
    }

    function escape_string($data) {
        $result = array();
        foreach ($data as $row) {
            $result[] = str_replace('"', '', $row);
        }
        return $result;
    }

    /* parse_file_relate_keys_to_values - tries to do a 1-to-1 match between csv header field names and field values
     *
     * Returns an array with keys from the csv header and field values from the remaining rows in the file.
     *
     */

    function relate_field_names_to_field_values($field_names, $field_values, $session, $csv_type = '') {
        $ekeyed_array = array();
        $required_field_names_count = count($field_names['REQUIRED']);
        //$required_field_names_array = $field_names['REQUIRED'];
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: required_field_names_count=");print($required_field_names_count);print("</pre>\n");
        $all_field_names_array = array_merge($field_names["REQUIRED"], $field_names["OPTIONAL"]);
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: all_field_names_array=<br/>");print_r($all_field_names_array);print("</pre>\n");exit;
        $all_field_names_count = count($all_field_names_array);
        $value_rows_count = count($field_values);


        switch ($csv_type) {
            case 'mentorCsv':
                $table_header_columns = json_decode(MentorHeader);
                break;
            case 'menteeCsv':
                $table_header_columns = json_decode(MenteeHeader);
                break;
            case 'subTopicCsv':
                $table_header_columns = json_decode(SubtopicHeader);
                break;
            default :
                $table_header = $this->bulkupload_model->getGenericHeaders($_POST['tableName']);
                $table_header_columns = array_map(function($v) {
                    return $v->COLUMN_NAME;
                }, $table_header);
                array_shift($table_header_columns); // ignore auto-increment ID since user never specifies it
        }

//print("<pre>[".__FILE__.' @ '. __LINE__."]: table_header=<br/>");print_r($table_header);print("</pre>\n");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: csv_header=<br/>");print_r($csv_header);print("</pre>\n");
        //$invalid_header = array_diff($csv_header, $table_header);

        if ($all_field_names_array === array_intersect($all_field_names_array, $table_header_columns) && $table_header_columns === array_intersect($table_header_columns, $all_field_names_array)) {
            
        } else {
            $invalid_headers_head = array_diff($table_header_columns, $all_field_names_array);
            $invalid_headers_csvhead = array_diff($all_field_names_array, $table_header_columns);
            $invalid_headers = array_merge($invalid_headers_csvhead, $invalid_headers_head);
        }
        //echo "<pre>";print_r($invalid_headers);exit;
        //echo "<pre>";
        //print_r($invalid_headers);
        //print_r($all_field_names_array);
        //print_r($table_header_columns);
        //exit;
        if (!empty($invalid_headers) || (count($all_field_names_array) > count($table_header_columns))) {
            $this->issue_incompatible_csv_input_error_msg($table_header_columns, $all_field_names_array, __FILE__, $this->header_line_no);
            redirect('admin/' . $_POST['urlPath']);
        }



//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: all_field_names_count=" . $all_field_names_count . "<br/></pre>\n");
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: initial value_rows_count=" . $value_rows_count . "<br/></pre>\n");
        $truncated_field_names_array = array();
        $rekeyed_array = array();
        $errors_array = array();
        foreach ($field_values as $key_row => $value_row) {
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: value_row=<br/>");print_r($value_row);print("</pre>\n");
            $field_values_count = count($value_row);
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: field_value_rows_count=" . $field_values_count . "<br/></pre>\n");
            // set up default values
            $truncated_field_names_array = $all_field_names_array;
            //$truncated_field_names_array = $required_field_names_array;
            $truncated_field_values_array = $value_row;

            if ($all_field_names_count < $field_values_count) {  // more values than names
                $errors_array[] = $this->issue_too_many_vars_error_msg($all_field_names_count, $field_values_count, $session, __FILE__, ($key_row + 1), $_POST['tableName'], true);
                //$excess_values = array_slice($field_values, $all_field_names_count, $field_values_count);
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: ** Too many values. Only first ".$all_field_names_count." values used. Excess values are<br/>".print_r($excess_values)."</pre>\n");
                //$truncated_field_values_array = array_slice($value_row, 0, $all_field_names_count, TRUE);
                continue;
            } else if ($all_field_names_count == $field_values_count) { // same number of names and values
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: arrays same size<br/></pre>\n");
;       // do nothing since size of arrays the same
            } else {       // not enough values for required fields
                if ($required_field_names_count > $field_values_count) {
//                    print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: ** Missing required field_values **<br/></pre>\n");
//                    print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: field_names[REQUIRED]=<br/>");
//                    print_r($field_names['REQUIRED']);
//                    print("</pre>\n");
//                    print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: array_slice=<br/>");
//                    print_r(array_slice($field_names['REQUIRED'], 0, $field_values_count));
//                    print("</pre>\n");
//                    print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: missing_value_names=<br/>");
//                    print_r($missing_value_names);
//                    print("</pre>\n");
//                    exit;
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: Names of missing required fields=<br/>");print_r($missing_value_names);print("</pre>\n");
//                    $missing_value_names = array_diff($field_names['REQUIRED'], array_slice($field_names['REQUIRED'], 0, $field_values_count));
                    $missing_value_names = array_diff($field_names['REQUIRED'], $value_row);

                    $missing_value_names = array_slice($field_names['REQUIRED'], $field_values_count);
//                    echo "<pre>";print_r($missing_value_names);exit;
                    $errors_array[] = $this->issue_missing_required_fields_error_msg($missing_value_names, $required_field_names_count, $field_values_count, $session, __FILE__, ($key_row + 1), $_POST['tableName'], true);
                    continue;     // ignore this row of values
                }
                //$truncated_field_names_array = array_slice($all_field_names_array, 0, $field_values_count, TRUE);
            }
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: truncated_field_names_array=<br/>");print_r($truncated_field_names_array);print("</pre>\n");
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: truncated_field_values_array=<br/>");print_r($truncated_field_values_array);print("</pre>\n");
//exit;
            $values_count = count($truncated_field_values_array);
            $header_count = count($truncated_field_names_array);
            for ($i = 0; $i < ($header_count - $values_count); $i++) {
                $truncated_field_values_array[] = '';
            }
            $rekeyed_array[] = array_combine($truncated_field_names_array, $truncated_field_values_array);
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: rekeyed_array=<br/>");print_r($truncated_field_names_array);print("</pre>\n");
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: rekeyed_array=<br/>");print_r($truncated_field_values_array);print("</pre>\n");
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: rekeyed_array=<br/>");print_r($rekeyed_array);print("</pre>\n");
//exit;
        }
        if (!empty($errors_array) && count($errors_array) > 0) {
            $session->set_flashdata('error', implode("<br><br>", $errors_array));
            redirect('admin/' . $_POST['urlPath']);
        }
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: rekeyed_array=<br/>");print_r($rekeyed_array);print("</pre>\n");
        return array($truncated_field_names_array, $rekeyed_array);
    }

    function processData($result, $tbl_name, $csvfile) {
        $result = array_values($result);

//print("<pre>[".__FILE__.' @ '. __LINE__."]: processData initial result=<br/>");print_r($result);print("</pre>\n");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: tbl_name='".$tbl_name."'<br/></pre>\n");
        $res = $this->bulkupload_model->saveDataFromCSV($result, $tbl_name);
//print("<pre>[".__FILE__.' @ '. __LINE__."]: res='".vardump($res)."'<br/></pre>\n");

        $msg = 'Table Data Successfully added';   // default db status  message
        $this->session->set_flashdata('success', $msg);
        $type = 'xls';
        $title = 'Table_upload_status';

        $csv_array = array();
//        $csv_reader = new CSVReader;
        list($csvkeys, $csv_array) = $this->parse_file($csvfile, $this->session);

        //$csv = $csv_array['VALUES'];
        $csv = $csv_array;
        $new2 = array();
        foreach ($csv as $key => $csv_val) {
            foreach ($result as $res) {
                $newArray = array_keys($csv_val); //get keys of any table dynamically
                $oldArray = array_keys($res);  //get keys of any table dynamically
                if ($csv_val[$newArray[0]] == $res[$newArray[0]]) {
                    $csv[$key]['success'] = "uploaded successfully";
                    $new2[$key] = $csv[$key]; // catch it
                    break;
                } else {
                    $csv[$key]['success'] = "Not uploaded";
                    $new2[$key] = $csv[$key]; // catch it	
                }
            }
        }

        $k = 0;
        foreach ($new2[$k] as $r_key => $r_value) {
            $dataHeader[$k] = $r_key;
            $k++;
        }
        $type = 'xls';
        $title = 'Data_upload_status';

        $this->export_generic($new2, $dataHeader, $type, NULL, $title, $_POST['urlPath']);
    }

    public function export_generic($export_data, $headers, $xls = NULL, $pdf = NULL, $title, $urlPath) {
        $this->load->library('excel');


        $heading = $headers;

        $no_of_cols = count($heading);
        //Loop Heading
        $rowNumberH = 1;
        $colH = 'A';
        $data = $export_data;
        //Loop Result
        //$totn=$nilai->num_rows();
        //$maxrow=$totn+1;
        //$nil=$nilai->result();
        $row = 2;
        $no = 1;

        $columns = array('0' => 'A', '1' => 'B', '2' => 'C', '3' => 'D', '4' => 'E', '5' => 'F', '6' => 'G', '7' => 'H', '8' => 'I', '9' => 'J', '10' => 'K', '11' => 'L', '12' => 'M', '13' => 'N', '14' => 'O', '15' => 'P', '16' => 'Q', '17' => 'R', '18' => 'S', '19' => 'T', '20' => 'U', '21' => 'V', '22' => 'W', '23' => 'X', '24' => 'Y', '25' => 'Z');

        //echo '<pre>';print_r($columns);exit;	
        //Freeze pane
        if (!empty($data)) {
            $this->excel->setActiveSheetIndex(0);
            foreach ($heading as $h) {
                $this->excel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
                $this->excel->getActiveSheet()->getColumnDimension($colH)->setWidth(25);
                $colH++;
            }




            $row = 2;
            $sQty = 0;
            $pQty = 0;
            $sAmt = 0;
            $pAmt = 0;
            $pl = 0;
            foreach ($data as $data_row) {

                for ($i = 0; $i < $no_of_cols; $i++) {
                    $this->excel->getActiveSheet()->setCellValue($columns[$i] . $row, $data_row[$heading[$i]]);
                }

                $row++;
            }


            $filename = $title;
            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            if ($pdf != NULL) {
                //echo 'pdf';
                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );
                $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                require_once(APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php");
                $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                $rendererLibrary = 'MPDF';
                $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                    die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                            PHP_EOL . ' as appropriate for your directory structure');
                }

                header('Content-Type: application/pdf');
                header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                header('Cache-Control: max-age=0');

                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                $objWriter->save('php://output');
                exit();
            }
            if ($xls != NULL) {
                //echo 'xls';
                $this->excel->getActiveSheet()->getStyle('C2:G' . $row)->getAlignment()->setWrapText(true);
                ob_clean();
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                header('Cache-Control: max-age=0');
                header('Refresh: 0; url=' . $urlPath);
                ob_clean();
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                var_dump($objWriter->save('php://output'));
                exit;
            }
        }
    }

    function genericUploader() {
        $tbl_name = $_POST['tableName'];
        $imageNameTime = time();
        $csvfile = $_SERVER['DOCUMENT_ROOT'] . "/mentor/assets/admin/csv/" . $imageNameTime . ".csv";
        $filename = $imageNameTime . ".csv";
//print("<pre>[".__FILE__.' @ '. __LINE__."]: _FILES[userfile][name]='".$_FILES['userfile']['name']."'<br/></pre>\n");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: tbl_name='".$tbl_name."'<br/></pre>\n");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: imageNameTime='".$imageNameTime."'<br/></pre>\n");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: csvfile='".$csvfile."'<br/></pre>\n");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: filename='".$filename."'<br/></pre>\n");
//exit;

        if (move_uploaded_file($_FILES["userfile"]["tmp_name"], $csvfile)) {
//            $this->load->library('csvreader');

            list($csv_header, $rekeyed_values) = $this->parse_file($csvfile, $this->session);
//print("<pre>[".__FILE__.' @ '. __LINE__."]: Parser csv_header=<br/>");print_r($csv_header);print("</pre>\n");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: Parser rekeyed_values=<br/>");print_r($rekeyed_values);print("</pre>\n");

            $table_header = $this->bulkupload_model->getGenericHeaders($tbl_name);
            $table_header_columns = array_map(function($v) {
                return $v->COLUMN_NAME;
            }, $table_header);
//print("<pre>[".__FILE__.' @ '. __LINE__."]: table_header=<br/>");print_r($table_header);print("</pre>\n");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: csv_header=<br/>");print_r($csv_header);print("</pre>\n");
            array_shift($table_header_columns); // ignore auto-increment ID since user never specifies it
            //$invalid_header = array_diff($csv_header, $table_header);

            if ($csv_header === array_intersect($csv_header, $table_header_columns) && $table_header_columns === array_intersect($table_header_columns, $csv_header)) {
                
            } else {
                $invalid_headers_head = array_diff($table_header_columns, $csv_header);
                $invalid_headers_csvhead = array_diff($csv_header, $table_header_columns);
                $invalid_headers = array_merge($invalid_headers_csvhead, $invalid_headers_head);
            }

            //print("<pre>[" . __FILE__ . ' @ ' . __LINE__ . "]: invalid_header=<br/>");
            //print_r($invalid_headers);
            //print("</pre>\n<pre>[" . __FILE__ . ' @ ' . __LINE__ . "]: Parsed csv_header='");
            //print_r($csv_header);
            //print("<br/></pre>\n<pre>[" . __FILE__ . ' @ ' . __LINE__ . "]: Parser table_header_columns=<br/>");
            //print_r($table_header_columns);
            //print("</pre>\n");

            if (!empty($invalid_headers) || (count($csv_header) > count($table_header_columns))) {
                $this->issue_incompatible_csv_input_error_msg($table_header_columns, $csv_header, __FILE__, $this->header_line_no);
                redirect('admin/' . $_POST['urlPath']);
            }

//print("<pre>[".__FILE__.' @ '. __LINE__."]: invalid_header=<br/>");print_r($invalid_header);print("</pre>\n");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: tbl_name='".$tbl_name."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: Parser rekeyed_values=<br/>");print_r($rekeyed_values);print("</pre>\n");
            $this->processData($rekeyed_values, $tbl_name, $csvfile);
            //redirect('admin/' . $_POST['urlPath']);
        } else {
            $error = "Failed to upload " . $tbl_name . "(1)";
            $this->session->set_flashdata('error', $error);
            redirect('admin/' . $_POST['urlPath']);
        }
    }

    function genericUploaderMentor() {
        $tbl_name = $_POST['tableName'];
        $imageNameTime = time();
        $csvfile = $_SERVER['DOCUMENT_ROOT'] . "/mentor/assets/admin/csv/" . $imageNameTime . ".csv";
        $filename = $imageNameTime . ".csv";
//print("<pre>[".__FILE__.' @ '. __LINE__."]: _FILES[userfile][name]='".$_FILES['userfile']['name']."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: tbl_name='".$tbl_name."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: imageNameTime='".$imageNameTime."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: csvfile='".$csvfile."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: filename='".$filename."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: _FILES=<br/>);print_r($_FILES);print("</pre>");echo "\n";
//exit;
        if (move_uploaded_file($_FILES["userfile"]["tmp_name"], $csvfile)) {

            list($csv_header, $rekeyed_values) = $this->parse_file($csvfile, $this->session, 'mentorCsv');
            //echo "<pre>";
            //print("<pre>[" . __FILE__ . ' @ ' . __LINE__ . "]: result=<br/></pre>");
            //print_r($csv_keys);
            //print_r($csv_values);
            //exit;
            //$data['csvData'] = $csv_values;

            $table_header_columns = json_decode(MentorHeader);  //$this->bulkupload_model->getGenericHeadersMentor($tbl_name);
            //$headers = array("MentorName", "MentorPhone", "MentorEmail", "MentorStatus", "Status", "Weight", "MentorImage");
//            print_r($headers);
//            exit;

            if ($csv_header === array_intersect($csv_header, $table_header_columns) && $table_header_columns === array_intersect($table_header_columns, $csv_header)) {
                
            } else {
                $invalid_headers_head = array_diff_key($table_header_columns, $csv_header);
                $invalid_headers_csvhead = array_diff_key($csv_header, $table_header_columns);
                $invalid_headers = array_merge($invalid_headers_csvhead, $invalid_headers_head);
            }

            //$invalid_headers = array_diff_key ($headers, $csvKeyHeader);
            if (!empty($invalid_headers) || (count($csv_header) > count($table_header_columns))) {
                $this->issue_incompatible_csv_input_error_msg($table_header_columns, $csv_header, __FILE__, $this->header_line_no);
                redirect('admin/' . $_POST['urlPath']);
            } else {

                //$result = array_values($result);
                $result = $rekeyed_values;

                //echo '<pre>';print_r($rekeyed_values);exit;
                $uploadArray = array();
                //$RoleCheck = 'Advisor'; // remove after debugging
                //$SourceCheck = 'Staff'; // remove after debugging
                //**foreach($result as $val){
                //echo "<pre>";print_r($rekeyed_values);exit;
                foreach ($rekeyed_values as $val) {
                    $RoleCheck = $val['MentorRole'];
                    $SourceCheck = $val['MentorSource'];
                    $mentorName = $val['MentorName'];
                    $MentorPhone = $val['MentorPhone'];
                    $MentorEmail = $val['MentorEmail'];
                    $MentorStatus = !empty($val['Status']) ? $val['Status'] : '';
                    $MentorWeight = !empty($val['Weight']) ? $val['Weight'] : '';
                    $roleres = $this->bulkupload_model->checkRoleExists($RoleCheck);
                    $sourceres = $this->bulkupload_model->checkSourceExists($SourceCheck);

                    if (!empty($roleres) && !empty($sourceres)) {
                        $roleId = $roleres[0]->userRoleID;
                        $sourceId = $sourceres[0]->MentorSourceID;
                        $addMentor = $this->bulkupload_model->addMentor($mentorName, $MentorStatus, $MentorWeight);
                        if ($addMentor != 0) {
                            $addContact = $this->bulkupload_model->addContact($addMentor, $MentorPhone, $MentorEmail);
                            $addRole = $this->bulkupload_model->addRole($addMentor, $roleId);
                            $addSource = $this->bulkupload_model->addSource($addMentor, $sourceId);
                            $dataUploded = array(
                                "MentorName" => $mentorName,
                                "MentorPhone" => $MentorPhone,
                                "MentorEmail" => $MentorEmail,
                                "MentorRole" => $RoleCheck,
                                "MentorSource" => $SourceCheck,
                                "Upload Status" => "Uploaded successfully"
                            );
                            array_push($uploadArray, $dataUploded);
                        } else {
                            $dataUploded = array(
                                "MentorName" => $mentorName,
                                "MentorPhone" => $MentorPhone,
                                "MentorEmail" => $MentorEmail,
                                "MentorRole" => $RoleCheck,
                                "MentorSource" => $SourceCheck,
                                "Upload Status" => "Upload failed as Advisor could not be created"
                            );
                            array_push($uploadArray, $dataUploded);
                        }
                    } else {

                        $dataUploded = array(
                            "MentorName" => $mentorName,
                            "MentorPhone" => $MentorPhone,
                            "MentorEmail" => $MentorEmail,
                            "MentorRole" => $RoleCheck,
                            "MentorSource" => $SourceCheck
                        );
                        if (empty($roleres) && empty($sourceres)) {
                            $dataUploded['Upload Status'] = 'Specified Role and Source could not be found';
                        } else if (empty($roleres)) {
                            $dataUploded['Upload Status'] = 'Specified Role could not be found';
                        } else if (empty($sourceres)) {
                            $dataUploded['Upload Status'] = 'Specified Source could not be found';
                        }
                        array_push($uploadArray, $dataUploded);
                    }
                }

                //echo count($result);exit;
                //$res = $this->bulkupload_model->saveDataFromCSV($result,$tbl_name);

                $error = 'Table Data Successfully added';
                $this->session->set_flashdata('success', $error);
                $type = 'xls';
                $title = 'Table_upload_status';
                //echo "<pre>";print_r($uploadArray);exit;
//                $csv_reader = new CSVReader;
                //**$csv_array = $csv_reader->parse_file($csvfile, $this->session);
                //**$csv = $csv_array['content'];
//                list($csv_keys, $csv_values) = $this->parse_file($csvfile, $this->session, 'mentorCsv');
                $csv = $rekeyed_values;
                //echo '<pre>';print_r($csv);echo '</pre>';
                //echo '<pre>';print_r($uploadArray);echo '</pre>';


                foreach ($csv as $key => $csv_val) {
                    //print_r($csv_val);
                    foreach ($uploadArray as $res) {
                        //print_r($res);exit;
                        $newArray = array_keys($csv_val); //get keys of any table dynamically
                        $oldArray = array_keys($res); //get keys of any table dynamically
                        //echo "<pre>";
                        //print_r($newArray);
                        //print_r($oldArray);exit;
                        if ($csv_val[$newArray[0]] == $res[$newArray[0]]) {
                            //echo $csv_val[$newArray[0]];
                            //echo $res[$newArray[0]]."<br>";
                            //$csv[$key]['status'] = "uploaded successfully";
                            $new2[$key] = $uploadArray[$key]; // catch it
                            break;
                        } else {
                            //$csv[$key]['status'] = "Not uploaded";
                            $new2[$key] = $uploadArray[$key]; // catch it	
                        }
                    }
                }

                //print_r($new2);exit;	

                $k = 0;
                foreach ($new2[$k] as $r_key => $r_value) {
                    $dataHeader[$k] = $r_key;
                    $k++;
                }
                //print_r($dataHeader);
                //exit;
                $type = 'xls';
                $title = 'Data_upload_status';

                $this->export_generic($new2, $dataHeader, $type, NULL, $title, $_POST['urlPath']);
            }
//print_r($data['csvData']);
//exit;
//echo $base_url();exit;
            redirect('admin/' . $_POST['urlPath']);
//exit;
            //$this->load->view('master/uploadSuccess', $data);
        } else {
            $error = "Failed to upload " . $tbl_name . "(2)";
            $this->session->set_flashdata('error', $error);
            redirect('admin/' . $_POST['urlPath']);
        }
    }

    function genericUploaderMentee() {
        $tbl_name = $_POST['tableName'];
        $imageNameTime = time();
        $csvfile = $_SERVER['DOCUMENT_ROOT'] . "/mentor/assets/admin/csv/" . $imageNameTime . ".csv";
        $filename = $imageNameTime . ".csv";
//print("<pre>[".__FILE__.' @ '. __LINE__."]: _FILES[userfile][name]='".$_FILES['userfile']['name']."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: tbl_name='".$tbl_name."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: imageNameTime='".$imageNameTime."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: csvfile='".$csvfile."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: filename='".$filename."'<br/></pre>");
//print("<pre>[".__FILE__.' @ '. __LINE__."]: _FILES=<br/></pre>");
//print_r($_FILES);
//exit;
        if (move_uploaded_file($_FILES["userfile"]["tmp_name"], $csvfile)) {
            //$data = array('upload_data' => $this->upload->data());
            //$this->load->library('csvreader');
            //**$result =   $this->csvreader->parse_file($csvfile, $this->session);
            //$csv_keys = array();
            //$csv_values = array();

            list($csv_header, $rekeyed_values) = $this->parse_file($csvfile, $this->session, 'menteeCsv');
            //echo "<pre>";print_r($csv_keys);
            //echo "<pre>";print_r($csv_values);exit;
            //$errorData	=	$result['error'];
            //$result = array_values($result['content']);
            //$csv_values = array_values($result['content']);
            //$data['csvData'] =  $result;
            //$data['csvData'] = $csv_values;
            //
            //$csv_headers = $data['csvData'][0];
            //foreach ($csv_headers as $csv_headerskey => $value) {
            //    $csvcolheaders[] = $csv_headerskey;
            //}

            $table_header_columns = json_decode(MenteeHeader);  //$this->bulkupload_model->getGenericHeadersMentor($tbl_name);
            //foreach($headers as $table_header)
            //{
            //	$columns[$table_header->COLUMN_NAME] = 	$table_header->COLUMN_NAME;
            //}
            //print_r($headers);exit;

            if ($csv_header === array_intersect($csv_header, $table_header_columns) && $table_header_columns === array_intersect($table_header_columns, $csv_header)) {
                
            } else {
                $invalid_headers_head = array_diff_key($table_header_columns, $csv_header);
                $invalid_headers_csvhead = array_diff_key($csv_header, $table_header_columns);
                $invalid_headers = array_merge($invalid_headers_csvhead, $invalid_headers_head);
            }

            //echo '<pre>';print_r($invalid_headers);exit;

            if (!empty($invalid_headers) || (count($csv_header) > count($table_header_columns))) {
                $this->issue_incompatible_csv_input_error_msg($table_header_columns, $csv_header, __FILE__, $this->header_line_no);
                redirect('admin/' . $_POST['urlPath']);
            } else {

                //$result = array_values($result);
                $result = $rekeyed_values;

                //echo '<pre>';print_r($result);exit;
                $uploadArray = array();
                foreach ($result as $val) {
                    $RoleCheck = $val['MenteeProgramme'];
                    $SourceCheck = $val['MenteeSource'];
                    $menteeName = $val['MenteeName'];
                    $MenteePhone = $val['MenteePhone'];
                    $MenteeEmail = $val['MenteeEmail'];
                    $roleres = $this->bulkupload_model->checkRoleExistsMentee($RoleCheck);
                    $sourceres = $this->bulkupload_model->checkSourceExistsMentee($SourceCheck);
                    //echo "dfgdg";echo '<pre>';print_r($sourceres);print_r($roleres);
                    //continue;
                    if (!empty($roleres) && !empty($sourceres)) {
                        $roleId = $roleres[0]->ProgrammeID;
                        $sourceId = $sourceres[0]->MenteeSourceID;
                        $addMentee = $this->bulkupload_model->addMentee($menteeName);
                        if ($addMentee != 0) {
                            $addContact = $this->bulkupload_model->addMenteeContact($addMentee, $MenteePhone, $MenteeEmail);
                            $addRole = $this->bulkupload_model->addMenteeRole($addMentee, $roleId);
                            $addSource = $this->bulkupload_model->addMenteeSource($addMentee, $sourceId);
                            $dataUploded = array(
                                "MenteeName" => $menteeName,
                                "MenteePhone" => $MenteePhone,
                                "MenteeEmail" => $MenteeEmail,
                                "MenteeProgramme" => $RoleCheck,
                                "MenteeSource" => $SourceCheck,
                                "Upload Status" => "Uploaded Successfully"
                            );
                            array_push($uploadArray, $dataUploded);
                        } else {
                            $dataUploded = array(
                                "MenteeName" => $menteeName,
                                "MenteePhone" => $MenteePhone,
                                "MenteeEmail" => $MenteeEmail,
                                "MenteeProgramme" => $RoleCheck,
                                "MenteeSource" => $SourceCheck,
                                "Upload Status" => "Upload failed as Entrepreneur could not be created"
                            );
                            array_push($uploadArray, $dataUploded);
                        }
                    } else {

                        $dataUploded = array(
                            "MenteeName" => $menteeName,
                            "MenteePhone" => $MenteePhone,
                            "MenteeEmail" => $MenteeEmail,
                            "MenteeProgramme" => $RoleCheck,
                            "MenteeSource" => $SourceCheck
                        );
                        if (empty($roleres) && empty($sourceres)) {
                            $dataUploded['Upload Status'] = 'Specified Programme and Source could not be found';
                        } else if (empty($roleres)) {
                            $dataUploded['Upload Status'] = 'Specified Programme could not be found';
                        } else if (empty($sourceres)) {
                            $dataUploded['Upload Status'] = 'Specified Source could not be found';
                        }
                        array_push($uploadArray, $dataUploded);
                    }
                }
                //echo count($result);
                //exit;
                //$res = $this->bulkupload_model->saveDataFromCSV($result,$tbl_name);

                $error = 'Table Data Successfully added';
                $this->session->set_flashdata('success', $error);
                $type = 'xls';
                $title = 'Table_upload_status';
                //echo "<pre>";print_r($result);
//                $csv_reader = new CSVReader;
                //**$csv_array = $csv_reader->parse_file($csvfile, $this->session);
                //**$csv = $csv_array['content'];
                $csv = $rekeyed_values;
                //echo '<pre>';print_r($csv);echo '</pre>';
                //echo '<pre>';print_r($uploadArray);echo '</pre>';exit;
                //echo '<pre>';
                //print_r($csv);
                //print_r($uploadArray);
                //echo '</pre>';
                //exit;

                foreach ($csv as $key => $csv_val) {
                    foreach ($uploadArray as $res) {
                        $newArray = array_keys($csv_val); //get keys of any table dynamically
                        $oldArray = array_keys($res); //get keys of any table dynamically
                        if ($csv_val[$newArray[0]] == $res[$newArray[0]]) {
                            //echo $csv_val[$newArray[0]];
                            //echo $res[$newArray[0]]."<br>";
                            //$csv[$key]['success'] = "uploaded successfully";
                            $new2[$key] = $uploadArray[$key]; // catch it
                            break;
                        } else {
                            //$csv[$key]['success'] = "Not uploaded";
                            $new2[$key] = $uploadArray[$key]; // catch it	
                        }
                    }
                }

                //print_r($new2);exit;	

                $k = 0;
                foreach ($new2[$k] as $r_key => $r_value) {
                    $dataHeader[$k] = $r_key;
                    $k++;
                }
                //print_r($dataHeader);
                //exit;
                $type = 'xls';
                $title = 'Data_upload_status';

                $this->export_generic($new2, $dataHeader, $type, NULL, $title, $_POST['urlPath']);
            }
            //print_r($data['csvData']);
            //exit;
            //echo $base_url();exit;
            redirect('admin/' . $_POST['urlPath']);
            //exit;
            //$this->load->view('master/uploadSuccess', $data);
        } else {
            $error = "Failed to upload " . $tbl_name . "(3)";
            $this->session->set_flashdata('error', $error);
            redirect('admin/' . $_POST['urlPath']);
        }
    }

    function genericUploaderSubtopics() {
        $tbl_name = $_POST['tableName'];
        $imageNameTime = time();
        $csvfile = $_SERVER['DOCUMENT_ROOT'] . "/mentor/assets/admin/csv/" . $imageNameTime . ".csv";
        $filename = $imageNameTime . ".csv";

        if (move_uploaded_file($_FILES["userfile"]["tmp_name"], $csvfile)) {
            //$data = array('upload_data' => $this->upload->data());
            //$this->load->library('csvreader');
            //**$result =   $this->csvreader->parse_file($csvfile, $this->session);
            //$csv_keys = array();
            //$csv_values = array();

            list($csv_header, $rekeyed_values) = $this->parse_file($csvfile, $this->session, 'subTopicCsv');
            //echo "<pre>";print_r($csv_keys);
            //echo "<pre>";print_r($csv_values);exit;
            //$errorData	=	$result['error'];
            //$result = array_values($result['content']);
            //$csv_values = array_values($result['content']);
            //$data['csvData'] =  $result;
            //$data['csvData'] = $csv_values;
            //
            //$csv_headers = $data['csvData'][0];
            //foreach ($csv_headers as $csv_headerskey => $value) {
            //    $csvcolheaders[] = $csv_headerskey;
            //}

            $table_header_columns = json_decode(SubtopicHeader);  //$this->bulkupload_model->getGenericHeadersMentor($tbl_name);
            //foreach($headers as $table_header)
            //{
            //	$columns[$table_header->COLUMN_NAME] = 	$table_header->COLUMN_NAME;
            //}
            //print_r($headers);exit;

            if ($csv_header === array_intersect($csv_header, $table_header_columns) && $table_header_columns === array_intersect($table_header_columns, $csv_header)) {
                
            } else {
                $invalid_headers_head = array_diff_key($table_header_columns, $csv_header);
                $invalid_headers_csvhead = array_diff_key($csv_header, $table_header_columns);
                $invalid_headers = array_merge($invalid_headers_csvhead, $invalid_headers_head);
            }

            //echo '<pre>';print_r($invalid_headers);exit;

            if (!empty($invalid_headers) || (count($csv_header) > count($table_header_columns))) {
                $this->issue_incompatible_csv_input_error_msg($table_header_columns, $csv_header, __FILE__, $this->header_line_no);
                redirect('admin/' . $_POST['urlPath']);
            } else {

                //$result = array_values($result);
                $result = $rekeyed_values;

                //echo '<pre>';print_r($result);exit;
                $uploadArray = array();
                foreach ($result as $val) {
                    $TopicCheck = $val['Topic'];
                    $subTopicName = $val['SubTopicDescription'];
                    $topicres = $this->bulkupload_model->checkTopicExists($TopicCheck);
                    //echo "dfgdg";echo '<pre>';print_r($sourceres);print_r($roleres);
                    if (!empty($topicres)) {
                        $topicId = $topicres[0]->TopicID;
                        $addSubTopic = $this->bulkupload_model->addSubTopic($subTopicName, $topicId);
                        if ($addSubTopic != 0) {
                            $dataUploded = array(
                                "Topic" => $TopicCheck,
                                "SubTopicDescription" => $subTopicName,
                                "Upload Status" => "Uploaded Successfully"
                            );
                            array_push($uploadArray, $dataUploded);
                        } else {
                            $dataUploded = array(
                                "Topic" => $TopicCheck,
                                "SubTopicDescription" => $subTopicName,
                                "Upload Status" => "Upload failed as SubTopic could not be created"
                            );
                            array_push($uploadArray, $dataUploded);
                        }
                    } else {
                        $dataUploded = array(
                            "Topic" => $TopicCheck,
                            "SubTopicDescription" => $subTopicName,
                            "Upload Status" => "Specified Topic could not be found"
                        );
                        array_push($uploadArray, $dataUploded);
                    }
                }
                //echo count($result);
                //exit;
                //$res = $this->bulkupload_model->saveDataFromCSV($result,$tbl_name);

                $error = 'Table Data Successfully added';
                $this->session->set_flashdata('success', $error);
                $type = 'xls';
                $title = 'Table_upload_status';
                //echo "<pre>";print_r($result);
//                $csv_reader = new CSVReader;
                //**$csv_array = $csv_reader->parse_file($csvfile, $this->session);
                //**$csv = $csv_array['content'];
                $csv = $rekeyed_values;
                //echo '<pre>';print_r($csv);echo '</pre>';
                //echo '<pre>';print_r($uploadArray);echo '</pre>';exit;
                //echo '<pre>';
                //print_r($csv);
                //print_r($uploadArray);
                //echo '</pre>';
                //exit;

                foreach ($csv as $key => $csv_val) {
                    foreach ($uploadArray as $res) {
                        $newArray = array_keys($csv_val); //get keys of any table dynamically
                        $oldArray = array_keys($res); //get keys of any table dynamically
                        if ($csv_val[$newArray[0]] == $res[$newArray[0]]) {
                            //echo $csv_val[$newArray[0]];
                            //echo $res[$newArray[0]]."<br>";
                            //$csv[$key]['success'] = "uploaded successfully";
                            $new2[$key] = $uploadArray[$key]; // catch it
                            break;
                        } else {
                            //$csv[$key]['success'] = "Not uploaded";
                            $new2[$key] = $uploadArray[$key]; // catch it	
                        }
                    }
                }

                //print_r($new2);exit;	

                $k = 0;
                foreach ($new2[$k] as $r_key => $r_value) {
                    $dataHeader[$k] = $r_key;
                    $k++;
                }
                //print_r($dataHeader);
                //exit;
                $type = 'xls';
                $title = 'Data_upload_status';

                $this->export_generic($new2, $dataHeader, $type, NULL, $title, $_POST['urlPath']);
            }
            //print_r($data['csvData']);
            //exit;
            //echo $base_url();exit;
            redirect('admin/' . $_POST['urlPath']);
            //exit;
            //$this->load->view('master/uploadSuccess', $data);
        } else {
            $error = "Failed to upload " . $tbl_name . "(3)";
            $this->session->set_flashdata('error', $error);
            redirect('admin/' . $_POST['urlPath']);
        }
    }

}
