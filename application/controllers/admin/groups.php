<?php

class Groups extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/groups_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // Groups Listing LISTING 
    //=========================================
    public function index() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );

        $res['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('GroupName', 'Total Members','Mentor Name','Mentee Name', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/groups/list', $res);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $this->datatables->select("mg.GroupID, mg.GroupName, COUNT(mgu.GroupUserID) AS UserCount,group_concat(mt.MentorName) as MentorName,group_concat(me.MenteeName) as MenteeName", FALSE)
                ->unset_column('Actions')
                ->unset_column('mg.GroupID')
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/groups'), 'mg.GroupID')
                ->from('mentor_groups mg')
                ->join('mentor_groupusers mgu', 'mgu.GroupID = mg.GroupID', 'left')
				->join('mentor_mentor mt', 'mt.MentorID = mgu.UserID AND mgu.UserType = 0', 'left')
				->join('mentor_mentee me', 'me.MenteeID = mgu.UserID AND mgu.UserType = 1', 'left')
                ->where('mg.Status !=', '0')
                ->group_by('mgu.GroupID');

        echo $this->datatables->generate();
    }

    //=========================================
    // Abbreviation ADD 
    //=========================================
    public function add($id = '') {
//        if (!empty($_POST)) {
//            echo "<pre>";
//            print_r($_POST);
//            exit;
//        }
        $data['config'] = $this->settings_model->getConfig();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('GroupName', 'Group Name', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {
            $data = array(
                'GroupName' => $this->input->post('GroupName'),
                'Weight' => $this->input->post('Weight'),
                'UpdatedDate' => date('Y-m-d H:i:s')
            );
            if ($id == 0) {
                $data['CreatedDate'] = date('Y-m-d H:i:s');
                $result = $this->db->insert('groups', $data);
                $id = $this->db->insert_id();

                if (!empty($this->input->post('GroupMentors'))) {
                    foreach ($this->input->post('GroupMentors') as $mentors) {
                        $this->groups_model->addMentorToGroup($id, $mentors);
                    }
                }

                if (!empty($this->input->post('GroupMentees'))) {
                    foreach ($this->input->post('GroupMentees') as $mentees) {
                        $this->groups_model->addMenteeToGroup($id, $mentees);
                    }
                }


                $this->session->set_flashdata('success', 'Group Added Successfully');
            } else {
                $this->db->where('GroupID', $id);
                $result = $this->db->update('groups', $data);

                if (!empty($this->input->post('GroupMentors'))) {
                    $this->db->delete('groupusers', array('GroupID' => $id, 'UserType' => 0));
                    foreach ($this->input->post('GroupMentors') as $mentors) {
                        $this->groups_model->addMentorToGroup($id, $mentors);
                    }
                }

                if (!empty($this->input->post('GroupMentees'))) {
                    $this->db->delete('groupusers', array('GroupID' => $id, 'UserType' => 1));
                    foreach ($this->input->post('GroupMentees') as $mentees) {
                        $this->groups_model->addMenteeToGroup($id, $mentees);
                    }
                }

                $this->session->set_flashdata('success', 'Group Updated Successfully');
            }
            redirect('admin/groups');
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');

        $groupData = array();

        if ($id) {
            $groupData = array();
            $groups = $this->groups_model->getGroupDetails($id);
            if ($groups) {
                $groupUsers = $this->groups_model->getGroupDetails($id, true);
                $groupData['GroupName'] = $groups[0]->GroupName;
                $groupData['GroupID'] = $groups[0]->GroupID;
                $groupData['Weight'] = $groups[0]->Weight;
                $groupData['GroupMentorsID'] = array();
                $groupData['GroupMenteesID'] = array();
                foreach ($groupUsers as &$users) {
                    if ($users->UserType == 0) {
                        $groupData['GroupMentors'][] = $this->groups_model->getMentorMenteeDetails($users);
                        $groupData['GroupMentorsID'][] = $users->UserID;
                    } else if ($users->UserType == 1) {
                        $groupData['GroupMentees'][] = $this->groups_model->getMentorMenteeDetails($users);
                        $groupData['GroupMenteesID'][] = $users->UserID;
                    }
                }
            }
        } else {
            $groupData['GroupName'] = '';
            $groupData['GroupID'] = '';
            $groupData['Weight'] = '';
            $groupData['GroupMentors'][] = '';
            $groupData['GroupMentees'][] = '';
            $groupData['GroupMentorsID'][] = '';
            $groupData['GroupMenteesID'][] = '';
        }

        $groupData['MentorsList'] = $this->groups_model->getAllMentors();
        $groupData['MenteesList'] = $this->groups_model->getAllMentees();

        $data['id'] = $id;
        $data['groups'] = $groupData;

        $this->template->content->view('admin/groups/add', $data);
        $this->template->publish_admin();
    }

    public function testGroup($id = '') {
        $data = array();
        $groups = $this->groups_model->getGroupDetails($id);
        if ($groups) {
            $groupUsers = $this->groups_model->getGroupDetails($id, true);
            $data['GroupName'] = $groups[0]->GroupName;
            $data['GroupID'] = $groups[0]->GroupID;
            foreach ($groupUsers as &$users) {
                if ($users->UserType == 1) {
                    $data['GroupMentors'][] = $this->groups_model->getMentorMenteeDetails($users);
                } else if ($users->UserType == 0) {
                    $data['GroupMentees'][] = $this->groups_model->getMentorMenteeDetails($users);
                }
            }
        }
        echo "<pre>";
        print_r($data);
        exit;
    }

    //=========================================
    // Group EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    //=========================================
    // Group DELETE 
    //=========================================
    public function delete($id) {
        $ds = $this->groups_model->delete_group($id);
        $this->session->set_flashdata('success', 'Group Deleted Successfully');
        redirect('admin/groups');
    }

}
