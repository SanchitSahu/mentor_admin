<?php

class Meetingplace extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/meetingplace_model');
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        //print_r($res);exit;
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            //print_r($x);exit;
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // Meetingplace LISTING 
    //=========================================
    public function index() {

        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $error['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Meeting Place Name', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/meetingplace/list', $error);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $this->datatables->select("MeetingPlaceID, MeetingPlaceName, Weight", FALSE)
                ->unset_column('MeetingPlaceID')
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/meetingplace'), 'MeetingPlaceID')
                ->from('meetingplace')
                ->where('Status', '1');
        //$this->db->order_by("Weight", "ASC");

        echo $this->datatables->generate();
    }

    //=========================================
    // Meetingplace ADD 
    //=========================================
    public function add($id = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('MeetingPlaceName', 'Meeting Place Name', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {

            $data = array(
                'MeetingPlaceName' => $this->input->post('MeetingPlaceName'),
                'Weight' => $this->input->post('Weight')
            );
            if ($id == 0) {
                $result = $this->db->insert('meetingplace', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', 'Meeting Place Added Successfully');
            } else {
                $this->db->where('MeetingPlaceID', $id);
                $result = $this->db->update('meetingplace', $data);
                $this->session->set_flashdata('success', 'Meeting Place Updated Successfully');
            }
            redirect('admin/meetingplace');
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('meetingplace');
        $meetingplace = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $meetingplace[$field] = '';
            }
        }

        if ($id) {
            $meetingplace = $this->meetingplace_model->get_meetingplace($id);
        }
        $data['id'] = $id;
        $data['meetingplace'] = $meetingplace;

        $this->template->content->view('admin/meetingplace/add', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // Meetingplace EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    public function checkMeetingPlaceExist() {
        if ($this->input->is_ajax_request()) {
            $MeetingPlaceName = $this->input->post('MeetingPlaceName');
            $MeetingPlaceID = $this->input->post('MeetingPlaceID');
            if ($MeetingPlaceID == "") {
                $flag = $this->meetingplace_model->checkMeetingPlace($MeetingPlaceName);
            } else {
                $flag = $this->meetingplace_model->checkMeetingPlaceEdit($MeetingPlaceName, $MeetingPlaceID);
            }
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    //=========================================
    // Delete Meetingplace 
    //=========================================
    public function delete($id) {
        $this->meetingplace_model->delete_meetingplace($id);
        $this->session->set_flashdata('success', 'Meeting Place Delete Successfully');
        redirect('admin/meetingplace');
    }

}
