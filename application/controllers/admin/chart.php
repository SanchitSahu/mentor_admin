<?php
class Chart extends Admin_Controller {

    function __construct() {
        parent::__construct();
		if(!loginCheck()){redirect('admin');}
		$this->load->model('admin/chart_model');
		$res	=	loadDatabase();
		//print_r($res);exit;
		if($res != 0){
			$x	=	$this->load->database($res,TRUE);
			//print_r($x);exit;
			$this->db	=	$x;	
		}else{
			redirect("400.shtml");exit;
		}
    }

	// load dashboard
    public function index(){ 
	
		$results['chart_data'] = $this->chart_model->get_chart_data();
        
		$this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');     
        
        $this->template->content->view('admin/chart/chart',$results);
        $this->template->publish_admin();
		
		//$this->template->publish_admin();
		//$this->load->view('admin/chart/chart',$results);
    }
	
	public function menteeSkillchart()
	{
		$results['chart_data'] = $this->chart_model->menteeSkillchart();
		
		$this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');     
        
		$this->template->content->view('admin/chart/menteeSkillchart',$results);
        $this->template->publish_admin();
		
		
	}
	
	public function mentorDetailchart()
	{
		$chart_data = $this->chart_model->mentorDetailchart();
		$newMentor	=	"";
		$menteelist = "";
		$arrayFinal	=	array();
		$mentorIdArr	=	array();
		//echo "<pre>";print_r($chart_data);
		$results['MenteeList']	=	$this->chart_model->getMenteeList();
		foreach($chart_data as $val){
			$MentorID	=	$val->MentorID;
			
			//echo "<pre>";print_r($mentorIdArr);
			if(!in_array($MentorID,$mentorIdArr)){
				$key	=	$val->MentorID;
				$arrayFinal[$key][0]	=	array("MentorName"=>$val->MentorName,"MenteeName"=>$val->MenteeName,"total"=>$val->total,"totaltime"=>$val->totaltime,"MentorID"=>$val->MentorID);
				array_push($mentorIdArr,$MentorID);
			}else{
				$newArr	=	array("MentorName"=>$val->MentorName,"MenteeName"=>$val->MenteeName,"total"=>$val->total,"totaltime"=>$val->totaltime,"MentorID"=>$val->MentorID);
				$metorKey	=	$MentorID;
				$arrayFinal[$metorKey][] = $newArr;
				//array_push($newArr,$arrayFinal[$metorKey]);
			}
			$newMentor	=	$MentorID;
		}
		$newMentor	=	"";
		//echo "<pre>";print_r($arrayFinal);exit;
		$results['chart_data']	=	$arrayFinal;
		
		$this->template->content->view('admin/chart/mentorDetailchart',$results);
		$this->template->publish_admin();
	}
	
}
