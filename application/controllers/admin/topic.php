<?php

class Topic extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/topic_model');
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        //print_r($res);exit;
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            //print_r($x);exit;
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // TOPIC LISTING 
    //=========================================
    public function index() {

        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $error['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Topic Description', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/topic/list', $error);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $this->datatables->select("TopicID, TopicDescription, Weight", FALSE)
                ->unset_column('TopicID')
                //->add_column('Actions', get_buttons('$1','admin/topic'),'TopicID')
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/topic'), 'TopicID')
                ->from('topic')
                ->where('Status !=', '0');


        echo $this->datatables->generate();
    }

    //=========================================
    // TOPIC ADD 
    //=========================================
    public function add($id = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('TopicDescription', 'Topic Description', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {

            $data = array(
                'TopicDescription' => $this->input->post('TopicDescription'),
                'Weight' => $this->input->post('Weight')
            );
            if ($id == 0) {
                $result = $this->db->insert('topic', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', 'Topic Added Successfully');
            } else {
                $this->db->where('TopicID', $id);
                $result = $this->db->update('topic', $data);
                $this->session->set_flashdata('success', 'Topic Updated Successfully');
            }
            redirect('admin/topic');
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('topic');
        $topic = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $topic[$field] = '';
            }
        }

        if ($id) {
            $topic = $this->topic_model->get_topic($id);
        }
        $data['id'] = $id;
        $data['topic'] = $topic;

        $this->template->content->view('admin/topic/add', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // TOPIC EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    public function checkTopicExist() {
        if ($this->input->is_ajax_request()) {
            $TopicDescription = $this->input->post('TopicDescription');
            $TopicID = $this->input->post('TopicID');
            if ($TopicID == "") {
                $flag = $this->topic_model->checkTopic($TopicDescription);
            } else {
                $flag = $this->topic_model->checkTopicEdit($TopicDescription, $TopicID);
            }
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

    //=========================================
    // TOPIC DELETE 
    //=========================================
    public function delete($id) {
        $this->topic_model->delete_topic($id);
        $this->session->set_flashdata('success', 'Topic Delete Successfully');
        redirect('admin/topic');
    }

}
