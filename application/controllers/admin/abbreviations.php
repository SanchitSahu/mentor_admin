<?php

class Abbreviations extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/abbreviations_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // Abbreviations LISTING 
    //=========================================
    public function index() {
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );

        $res['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Abbreviation', 'Full Form', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/abbreviations/list', $res);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $this->datatables->select("abbreviationId,abbreviationKey, abbreviationFullform, Weight", FALSE)
                ->unset_column('Actions')
                ->add_column('Actions', get_Edit_Delete_Buttons('$1', 'admin/abbreviations'), 'abbreviationId')
                ->unset_column('abbreviationId')
                ->from('abbreviations')
                ->where('Status !=', '0');

        echo $this->datatables->generate();
    }

    //=========================================
    // Abbreviation ADD 
    //=========================================
    public function add($id = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('AbbreviationKey', 'Abbreviation Key', 'required');
        $this->form_validation->set_rules('AbbreviationFullform', 'Abbreviation Fullform', 'required');
        $this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {

            $data = array(
                'abbreviationKey' => $this->input->post('AbbreviationKey'),
                'abbreviationFullform' => $this->input->post('AbbreviationFullform'),
                'Weight' => $this->input->post('Weight')
            );
            if ($id == 0) {
                $result = $this->db->insert('abbreviations', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', 'Abbreviation Added Successfully');
            } else {
                $this->db->where('abbreviationId', $id);
                $result = $this->db->update('abbreviations', $data);
                $this->session->set_flashdata('success', 'Abbreviation Updated Successfully');
            }
            redirect('admin/abbreviations');
        }

        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');

        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');


        $fields = get_table_fields('abbreviations');
        $abbreviations = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $abbreviations[$field] = '';
            }
        }

        if ($id) {
            $abbreviations = $this->abbreviations_model->get_abbreviation($id);
        }
        $data['id'] = $id;
        $data['abbreviations'] = $abbreviations;

        $this->template->content->view('admin/abbreviations/add', $data);
        $this->template->publish_admin();
    }

    //=========================================
    // Abbreviation EDIT 
    //=========================================
    public function edit($id) {
        $this->add($id);
    }

    //=========================================
    // Abbreviation DELETE 
    //=========================================
    public function delete($id) {
        $ds = $this->abbreviations_model->delete_abbreviation($id);
        $this->session->set_flashdata('success', 'Abbreviations Delete Successfully');
        redirect('admin/abbreviations');
    }

    public function checkAbbreviationsExist() {
        if ($this->input->is_ajax_request()) {
            $AbbreviationKey = $this->input->post('AbbreviationKey');
            $AbbreviationId = $this->input->post('AbbreviationId');
            if ($AbbreviationId == "") {
                $flag = $this->abbreviations_model->checkAbbreviations($AbbreviationKey);
            } else {
                $flag = $this->abbreviations_model->checkAbbreviationsEdit($AbbreviationKey, $AbbreviationId);
            }
            echo ($flag > 0) ? "false" : "true";
        } else {
            show_error("Access Denied");
        }
    }

}
