<?php

class Newusers extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/newusers_model');
		$this->load->model('admin/emailtemplate_model');
		$this->load->model('admin/mentors_model');
		$this->load->model('admin/settings_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        //print_r($res);exit;
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            //print_r($x);exit;
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // SKILL LISTING 
    //=========================================
    public function index() {
		//error_reporting(E_ALL);
        //ini_set("display_errors","ON");
        $res['config'] = $this->settings_model->getConfig();
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );

        $res['error'] = "";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Username','User Email','User Type', 'Last Login Date', 'Date Registered', 'Date Registered','Send Notice');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
		$this->template->javascript->add('https://cdn.datatables.net/plug-ins/1.10.12/api/fnReloadAjax.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('admin/newusers/list', $res);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $from = (isset($_GET['from'])) ? $_GET['from'] : '';
        $to = (isset($_GET['to'])) ? $_GET['to'] : '';
        $yes = (isset($_GET['yes_value'])) ? $_GET['yes_value'] : '';
        $no = (isset($_GET['no_value'])) ? $_GET['no_value'] : '';
        $showInactive = (isset($_GET['showInactive'])) ? $_GET['showInactive'] : 1;

        $config = $this->settings_model->getConfig();

        /* SELECT nr.RegistrationID, IF(UserType = "1", me.MenteeName, m.MentorName) UserName, IF(UserType = "1", mec.MenteeEmail, mrc.MentorEmail) UserEmail, IF(UserType = "1", "'.$config[0]->menteeName.'", "'.$config[0]->mentorName.'") UserType, IF(UserType = "1", mec.LastLogin, mrc.LastLogin) LastLogin, nr.DateRegistered, IF(nr.DateUpdated = "0000-00-00 00:00:00", "-- Not Yet Notified --", nr.DateUpdated) AS DateUpdated
          FROM mentor_newregistrations nr
          LEFT JOIN mentor_mentor  m
          ON nr.UserID = m.MentorID
          LEFT JOIN mentor_mentorcontactinfo  mrc
          ON nr.UserID = mrc.MentorID
          LEFT JOIN mentor_mentee me
          ON nr.UserID = me.MenteeID
          LEFT JOIN mentor_menteecontact mec
          ON nr.UserID = mec.MenteeID; */

        $this->datatables->select('nr.RegistrationID, IF(UserType = "1", me.MenteeName, m.MentorName) AS UserName, IF(UserType = "1", mec.MenteeEmail, mrc.MentorEmail) AS UserEmail, IF(UserType = "1", "' . $config[0]->menteeName . '", "' . $config[0]->mentorName . '") AS UserType, IF(UserType = "1", mec.LastLogin, mrc.LastLogin) AS LastLogin, nr.DateRegistered, IF(nr.DateUpdated = "0000-00-00 00:00:00", "-- Not Yet Notified --", nr.DateUpdated) AS DateUpdated,IF(UserType = "1", IF(me.Status=1,1,0), IF(m.Status=1,1,0)) AS UserStatus', FALSE)
                ->from('mentor_newregistrations nr')
                ->join('mentor_mentor m', 'nr.UserID = m.MentorID', 'left')
                ->join('mentor_mentorcontactinfo mrc', 'nr.UserID = mrc.MentorID', 'left')
                ->join('mentor_mentee me', 'nr.UserID = me.MenteeID', 'left')
                ->join('mentor_menteecontact mec', 'nr.UserID = mec.MenteeID', 'left')
                ->unset_column('nr.RegistrationID')
                ->unset_column('UserStatus')
                ->add_column('NotificationStatus', get_CheckBox('$1'), 'nr.RegistrationID');

        if (($yes == "true" && $no == "true") || ($yes == "false" && $no == "false")) {
            
        } else if ($yes == "true" && $no == "false") {

            $this->datatables->where('nr.DateUpdated !=', "0000-00-00 00:00:00");
        } else if ($yes == "false" && $no == "true") {

            $this->datatables->where('nr.DateUpdated', "0000-00-00 00:00:00");
        }

        if ($from != "") {
            $this->datatables->where('DATE(nr.DateRegistered) >=', $from);
        }

        if ($to != "") {
            $this->datatables->where('DATE(nr.DateRegistered) <=', $to);
        }

        if ($showInactive == "1") {
			$this->datatables->where('IF(UserType = "1", me.Status=1, m.Status=1)', '', false);
        } else if ($showInactive == "0") {
            $this->datatables->where('IF(UserType = "1", me.Status=0, m.Status=0)', '', false);
        } else if ($showInactive == "all") {
            $this->datatables->where('IF(UserType = "1", me.Status=0 OR me.Status=1, m.Status=0 OR m.Status=1)', '', false);
        }

        echo $this->datatables->generate();
		//echo $this->db->last_query();
    }
	
	function getregisteredUser(){
		$from	=	$_REQUEST['from'];
		$to	=	$_REQUEST['to'];
		$yes	=	$_REQUEST["yes_value"];
		$no		=	$_REQUEST["no_value"];
		
		$config = $this->settings_model->getConfig();
		
		$this->datatables->select('nr.RegistrationID, IF(UserType = "1", me.MenteeName, m.MentorName) AS UserName, IF(UserType = "1", mec.MenteeEmail, mrc.MentorEmail) AS UserEmail, IF(UserType = "1", "'.$config[0]->menteeName.'", "'.$config[0]->mentorName.'") AS UserType, IF(UserType = "1", mec.LastLogin, mrc.LastLogin) AS LastLogin, nr.DateRegistered, IF(nr.DateUpdated = "0000-00-00 00:00:00", "-- Not Yet Notified --", nr.DateUpdated) AS DateUpdated',FALSE)
			->from('mentor_newregistrations nr')
			->join('mentor_mentor m', 'nr.UserID = m.MentorID', 'left')
			->join('mentor_mentorcontactinfo mrc', 'nr.UserID = mrc.MentorID', 'left')
			->join('mentor_mentee me', 'nr.UserID = me.MenteeID', 'left')
			->join('mentor_menteecontact mec', 'nr.UserID = mec.MenteeID', 'left')
			->unset_column('nr.RegistrationID')
			->add_column('NotificationStatus', get_CheckBoxChecked('$1'), 'nr.RegistrationID');
		
		if(($yes == "true" && $no == "true")  || ($yes == "false" && $no == "false")) {
			
			//$this->datatables->where('DATE(DateRegistered) >=', $from);
			//$this->datatables->where('DATE(DateRegistered) <=', $to);
			
		} else if($yes == "true" && $no == "false") {
			$this->datatables->where('nr.DateUpdated !=', "0000-00-00 00:00:00");
			
		} else if($yes == "false" && $no == "true") {
			$this->datatables->where('nr.DateUpdated', "0000-00-00 00:00:00");
		}
		
		if($from != "") {
			$this->datatables->where('DATE(nr.DateRegistered) >=', $from);
		}
		
		if($to != ""){
			$this->datatables->where('DATE(nr.DateRegistered) <=', $to);
		}
		
		$this->datatables->group_by('nr.UserEmail');
        echo $this->datatables->generate();
		//echo $this->db->last_query();
	}
	
	function getNotifiedUsers(){
		$config = $this->settings_model->getConfig();
		$yes	=	$_REQUEST["yes_value"];
		$no		=	$_REQUEST["no_value"];
		
		$from	=	$_REQUEST['from'];
		$to	=	$_REQUEST['to'];
		
		if($from ==	"" && $to == ""){
			if ($yes == "true" && $no == "false") {
				
				$this->datatables->select('RegistrationID, UserName, UserEmail, IF(UserType = 1, "'.$config[0]->mentorName.'", "'.$config[0]->menteeName.'") AS UserType, DateRegistered',FALSE)
				->unset_column('RegistrationID')
				->add_column('NotificationStatus', get_CheckBox('$1'), 'RegistrationID')
				->from('mentor_newregistrations')
				->where('DateUpdated !=', "0000-00-00 00:00:00");
				
			} else if($yes == "false" && $no == "true") {
				$this->datatables->select('RegistrationID, UserName, UserEmail, IF(UserType = 1, "'.$config[0]->mentorName.'", "'.$config[0]->menteeName.'") AS UserType, DateRegistered',FALSE)
				->unset_column('RegistrationID')
				->add_column('NotificationStatus', get_CheckBox('$1'), 'RegistrationID')
				->from('mentor_newregistrations')
				->where('DateUpdated', "0000-00-00 00:00:00");
				
			} else {
				$this->datatables->select('RegistrationID, UserName, UserEmail, IF(UserType = 1, "'.$config[0]->mentorName.'", "'.$config[0]->menteeName.'") AS UserType, DateRegistered',FALSE)
				->unset_column('RegistrationID')
				->add_column('NotificationStatus', get_CheckBox('$1'), 'RegistrationID')
				->from('mentor_newregistrations');
			}
		} else {
			if ($yes == "true" && $no == "false") {
				$this->datatables->select('RegistrationID, UserName, UserEmail, IF(UserType = 1, "'.$config[0]->mentorName.'", "'.$config[0]->menteeName.'") AS UserType, DateRegistered',FALSE)
				->unset_column('RegistrationID')
				->add_column('NotificationStatus', get_CheckBox('$1'), 'RegistrationID')
				->from('mentor_newregistrations')
				->where('DATE(DateRegistered) >=', $from)
				->where('DATE(DateRegistered) <=', $to)
				->where('DateUpdated !=', "0000-00-00 00:00:00");
				
			} else if($yes == "false" && $no == "true") {
				$this->datatables->select('RegistrationID, UserName, UserEmail, IF(UserType = 1, "'.$config[0]->mentorName.'", "'.$config[0]->menteeName.'") AS UserType, DateRegistered',FALSE)
				->unset_column('RegistrationID')
				->add_column('NotificationStatus', get_CheckBox('$1'), 'RegistrationID')
				->from('mentor_newregistrations')
				->where('DATE(DateRegistered) >=', $from)
				->where('DATE(DateRegistered) <=', $to)
				->where('DateUpdated', "0000-00-00 00:00:00");
			} else {
				$this->datatables->select('RegistrationID, UserName, UserEmail, IF(UserType = 1, "'.$config[0]->mentorName.'", "'.$config[0]->menteeName.'") AS UserType, DateRegistered',FALSE)
				->unset_column('RegistrationID')
				->add_column('NotificationStatus', get_CheckBox('$1'), 'RegistrationID')
				->where('DATE(DateRegistered) >=', $from)
				->where('DATE(DateRegistered) <=', $to)
				->from('mentor_newregistrations');
			}
		}
		

        echo $this->datatables->generate();
	}
	
	function getUnNotyfiedUsers(){
		$config = $this->settings_model->getConfig();
		$this->datatables->select('RegistrationID, UserName, UserEmail, IF(UserType = 1, "'.$config[0]->mentorName.'", "'.$config[0]->menteeName.'") AS UserType, DateRegistered',FALSE)
		->unset_column('RegistrationID')
		->add_column('NotificationStatus', get_CheckBox('$1'), 'RegistrationID')
		->from('mentor_newregistrations')
		->where('DateUpdated', "0000-00-00 00:00:00");
        echo $this->datatables->generate();
	}

	function NotifyUser(){
		$data['config'] = $this->settings_model->getConfig();
		$users	=	explode(",",$_REQUEST['notifyUserList']);
		
		if($_REQUEST['notifyUserList'] != "") {
			foreach($users as $val){
				$userDetails	=	$this->newusers_model->getUserDetails($val);
				$emailtemplate = $this->emailtemplate_model->get_emailtempate_details(1);
				if(count($userDetails) > 0) {
					$host = $_SERVER['HTTP_HOST'];
					$subdomain = strstr(str_replace("www.", "", $host), ".", true);
					$AccessToken = mt_rand(100000, 999999);
					if ($emailtemplate) {
						$emailData = array();
						$emailData['toEmail'] = $userDetails[0]->UserEmail;
						
						$arrSearch = array('##FIRST_NAME##', '##EMAIL_ADDRESS##', '##ACCESS_TOKEN##', '##USER_TYPE##', "##SCHOOLNAME##", "##DEPARTMENTNAME##", "##MENTOR##", "##MENTEE##");
						$arrReplace = array($userDetails[0]->UserName, $userDetails[0]->UserName, $AccessToken, 'Mentor', $subdomain, $data['config'][0]->DepartmentName, $data['config'][0]->mentorName, $data['config'][0]->menteeName);
		
						$subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
						$emailData['subject'] = $subject;
		
						$body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
						$emailData['body'] = $body;
		
						send_mail($emailData);
						$updatePassword	=	$this->newusers_model->updatePassword($val,$AccessToken);
					}
				} else {
					if(count($users) > 0) {
						$this->session->set_flashdata('error', 'Selected user data is not found in appropriate table in backend');
						die('success');
					}
				}
			}
		}
		$this->session->set_flashdata('success', 'Notification to user sent Successfully');
		die('success');

		//redirect('admin/newusers');
	}
    //=========================================
    // user DELETE 
    //=========================================
    //public function delete($id) {
    //    $ds = $this->clientSource_model->delete_source($id);
    //    $this->session->set_flashdata('success', 'Client Source Delete Successfully');
    //    redirect('admin/clientSource');
    //}

}
