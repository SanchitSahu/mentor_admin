<?php
class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
		if(!loginCheck()){redirect('admin');}
		$this->load->model('admin/relationship_model');
		$this->load->model('admin/chart_model');
		$this->load->model('admin/settings_model');
		$res	=	loadDatabase();
		//print_r($res);exit;
		if($res != 0){
			$x	=	$this->load->database($res,TRUE);
			//print_r($x);exit;
			$this->db	=	$x;	
		}else{
			redirect("400.shtml");exit;
		}
    }

	// load dashboard
    public function dashboard(){ 
		$res['totalMeetings']	=	$this->relationship_model->totalMeetings();
		$res['totalMentors']	=	$this->relationship_model->totalMentors();
		$res['totalMentees']	=	$this->relationship_model->totalMentees();
		$res['avgMeetingLength']=	$this->relationship_model->avgMeetingLength();
		
		$res['topicMeetingChart'] = $this->chart_model->topicMeetingChart();
		$res['mentorMeetingChart'] = $this->chart_model->mentorMeetingChart();
		$res['avgMeetingLengthByTopic'] = $this->chart_model->avgMeetingLengthByTopic();
		
		$res['chart_data1'] = $this->chart_model->get_chart_data();/*mentor skill Data*/
		$res['menteeSkillchart'] = $this->chart_model->menteeSkillchart();
		
		$res['activeMentor'] = $this->chart_model->mentorActiveInactive();
		$res['inActiveMentor'] =  $this->chart_model->mentorINActiveInactive() - $res['activeMentor'];
		
		$getSkill	=	$this->chart_model->getSkill();
		//echo "<pre>";print_r($res['chart_data1']);print_r($res['menteeSkillchart']);
		//exit;
		$finalArr	=	array();
		foreach($res['chart_data1'] as $val){
			foreach($res['menteeSkillchart'] as $val2){
				if($val2->SkillID == $val->SkillID){
					$newArr	=	(object) array("SkillID"=>$val->SkillID,"SkillName"=>$val->SkillName,"mentor_ids"=>$val->mentor_ids,"mentor_names"=>$val->mentor_names,"mentee_ids"=>$val2->mentee_ids,"mentee_names"=>$val2->mentee_names);
					//print_r($newArr);exit;
					array_push($finalArr,$newArr);
					//echo $val->SkillID;
					break;
				}
			}
			
		}
		
		
		$diff = array();$diff2 = array();

		// Loop through all elements of the first array
		foreach($res['chart_data1'] as $value)
		{
		  // Loop through all elements of the second loop
		  // If any matches to the current element are found,
		  // they skip that element
		  foreach($res['menteeSkillchart'] as $value2)
		  {
			if($value->SkillID == $value2->SkillID)
			continue 2;
		  }
		  // If no matches were found, append it to $diff
		  $value->mentee_ids	=	0;
		  $value->mentee_names = "";
		  $diff[] = $value;
		}

		foreach($res['menteeSkillchart'] as $value)
		{
		  // Loop through all elements of the second loop
		  // If any matches to the current element are found,
		  // they skip that element
		  foreach($res['chart_data1'] as $value2)
		  {
			if($value->SkillID == $value2->SkillID)
			continue 2;
		  }
		  // If no matches were found, append it to $diff
		  $value->mentor_ids	=	0;
		  $value->mentor_names = "";
		  $diff2[] = $value;
		}

		//print_r(array_merge($diff,$diff2));
		$allArr	=	array_merge($diff,$diff2);
		
		
	
		//print '<pre>';
		/*print_r($allArr);
		print_r($finalArr);
		print '</pre>';exit;*/
		
		//print_r(array_merge($allArr,$finalArr));
		$chartArr	=	array_merge($allArr,$finalArr);
		//exit;
		
		$res['MentorMenteeSkillChart'] = $chartArr;

		$chart_data = $this->chart_model->mentorDetailchart();
		$newMentor	=	"";
		$menteelist = "";
		$arrayFinal	=	array();
		$mentorIdArr	=	array();
		//echo "<pre>";print_r($res['MentorMenteeSkillChart']);
		$res['MenteeList']	=	$this->chart_model->getMenteeList();
		foreach($chart_data as $val){
			$MentorID	=	$val->MentorID;
			
			//echo "<pre>";print_r($mentorIdArr);
			if(!in_array($MentorID,$mentorIdArr)){
				$key	=	$val->MentorID;
				$arrayFinal[$key][0]	=	array("MentorName"=>$val->MentorName,"MenteeName"=>$val->MenteeName,"total"=>$val->total,"totaltime"=>$val->totaltime,"MentorID"=>$val->MentorID);
				array_push($mentorIdArr,$MentorID);
			}else{
				$newArr	=	array("MentorName"=>$val->MentorName,"MenteeName"=>$val->MenteeName,"total"=>$val->total,"totaltime"=>$val->totaltime,"MentorID"=>$val->MentorID);
				$metorKey	=	$MentorID;
				$arrayFinal[$metorKey][] = $newArr;
				//array_push($newArr,$arrayFinal[$metorKey]);
			}
			$newMentor	=	$MentorID;
		}
		$newMentor	=	"";
		//echo "<pre>";print_r($arrayFinal);exit;
		$res['chart_data']	=	$arrayFinal;
		
		$res['config']	=	$this->settings_model->getConfig();
		$_SESSION['headerColor']	=	$res['config'][0]->headerColor;
        $this->load->view('includes/header_common');
        $this->load->view('admin/dashboard',$res);
        $this->load->view('includes/footer_common');
    }
	
	
}
