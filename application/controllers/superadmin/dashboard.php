<?php

class Dashboard extends Admin_Controller {

    function __construct() {
        parent::__construct();
        if (!loginCheck()) {
            redirect('dashboard');
        }
        $this->load->model('admin/settings_model');
        $this->load->model('superadmin/dashboard_model');
        $this->load->model('superadmin/subadmin_model');
        $res = loadDatabase();
        //print_r($res);exit;
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            //print_r($x);exit;
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
        $this->load->library('table');
    }

    // load dashboard
    public function index() {
        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('School Name', 'Admin Name', 'Email', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('superadmin/manageadmin/list');
        $this->template->publish_admin();

//		$res['config']	=	$this->settings_model->getConfig();
//		$_SESSION['headerColor']	=	$res['config'][0]->headerColor;
//        $this->load->view('includes/header_common');
//        $this->load->view('superadmin/dashboard',$res);
//        $this->load->view('includes/footer_common');
    }

    public function provisionSchool($id = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('subdomainName', 'Sub domain Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');

        if ($this->form_validation->run() === TRUE) {

            if ($id == 0) {
                $microtime = microtime();
                $verificationCode = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime);

                $subdomainName = strtolower($_REQUEST['subdomainName']);
                $addres = $this->dashboard_model->addSubDomain($subdomainName, $_REQUEST['email'], $verificationCode); //,$_REQUEST['programmeName']
                if ($addres > 0) {
                    $data['verificationCode'] = $verificationCode;
                    $data['userId'] = $addres;

                    $email_data = array(
                        'toEmail' => $_REQUEST['email'],
                        'fromEmail' => 'noreply@melstm.net',
                        'subject' => 'Confirm Registration',
                        'body' => $this->load->view('verifyRegistration', $data, TRUE)
                    );

                    if (send_mail($email_data)) {
                        $this->load->dbforge();
                        if ($this->db->query("CREATE DATABASE IF NOT EXISTS " . DB_TEMPLATE . "_" . $subdomainName . "")) {
                            $tables = $this->db->list_tables();
                            foreach ($tables as $table) {
                                $res = $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_TEMPLATE . "_" . $subdomainName . "." . $table . " LIKE " . DB_NAME . "." . $table . "");
                                if ($res == 0) {
                                    $this->dbforge->drop_database($subdomainName);
                                    $this->session->set_flashdata('error', 'Problem while copying data');
                                } else {
                                    if ($table == 'mentor_subdomains') {
                                        $res = $this->db->query("INSERT INTO " . DB_TEMPLATE . "_" . $subdomainName . "." . $table . " SELECT * FROM " . DB_NAME . "." . $table . " WHERE " . DB_NAME . "." . $table . ".subdomain_name='$subdomainName' AND " . DB_NAME . "." . $table . ".Status=1");
                                    } else if ($table == 'mentor_admindetails') {
                                        
                                    } else {
                                        $res = $this->db->query("INSERT INTO " . DB_TEMPLATE . "_" . $subdomainName . "." . $table . " SELECT * FROM " . DB_NAME . "." . $table . "");
                                    }
                                    $this->session->set_flashdata('success', 'Database Created successfully');
                                }
                            }
                        } else {
                            $this->session->set_flashdata('error', 'Error Creating database');
                        }
                        $this->session->set_flashdata('success', 'Please check your mail');
                    } else {
                        $this->session->set_flashdata('error', show_error($this->email->print_debugger()));
                    }
                } else {
                    $this->session->set_flashdata('error', 'Error Creating database');
                }
            } else {
                $subdomainName = strtolower($_REQUEST['subdomainName']);
                $addres = $this->dashboard_model->updateSubDomain($_REQUEST['email'], $id, $subdomainName); //,$_REQUEST['programmeName']
                $this->session->set_flashdata('success', 'School Updated Successfully');
            }



            redirect('superadmin/dashboard');
        }


        //$this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        //$this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        //$this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');
        //
        //$this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        //$this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        //$this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $fields = get_table_fields('subdomains');
        $subdomains = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $subdomains[$field] = '';
            }
        }
        if ($id) {
            $subdomains = $this->subadmin_model->edit_subadmin($id);
        }

        //print_r($subdomains);exit;
        $data['id'] = $id;
        $data['res'] = $subdomains;

        $this->template->content->view('superadmin/newSchool', $data);
        $this->template->publish_admin();
    }

    function verifyRegistration() {
        $data['verificationCode'] = $this->uri->segment(4);
        $data['subdomain_id'] = $this->uri->segment(5);
        $res = $this->dashboard_model->checkVerificationCode($data['verificationCode'], $data['subdomain_id']);
        if ($res == 1) {
            $this->load->view('superadmin/updatePassword', $data);
        } else
            echo "Error occurred while verifying the registration. Please try again";
    }

    public function updatePassword() {
        //print_r($_REQUEST);exit;
        $deleteRes = $this->dashboard_model->deleteVerificationCode($_REQUEST['verificationCode'], $_REQUEST['email'], $_REQUEST['password']);
        if ($deleteRes == 1) {
            echo "Registration Verified";
        } else {
            echo "Error occurred while verifying the registration. Please try again";
        }
    }

    function checkSubdomainName() {
        $subdomainName = $_REQUEST['subdomainName'];
        $subdomainID = $this->input->post('subdomainID');
        if ($subdomainID == "") {
            $res = $this->dashboard_model->checksubdomain($subdomainName);
        } else {
            $res = $this->dashboard_model->checksubdomain_edit($subdomainName, $subdomainID);
        }
        echo ($res > 0) ? "false" : "true";
    }

    function checkemail() {
        $email = $_REQUEST['email'];
        $subdomainID = $this->input->post('subdomainID');
        if ($subdomainID == "") {
            $res = $this->dashboard_model->checkemail($email);
        } else {
            $res = $this->dashboard_model->checkemail_edit($email, $subdomainID);
        }
        echo ($res > 0) ? "false" : "true";
    }

    public function resetpassword() {
        $this->load->model('admin/employee_model');
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $password = $this->input->post('pass');
            $password = fnEncrypt($password, $this->config->item('mentorKey'));

            //print_r($id);exit;
            if (empty($id)) {
                echo "Something went wrong try again... (__FILE__@__LINE__ in __FUNCTION__)";
            } else {
                $userDetails = $this->employee_model->get_employeeAdmin($id);
                $data = $this->dashboard_model->resetpassword($id, $password, $userDetails['subdomain_name']);
                //echo "here";print_r($userDetails);exit;

                $this->load->model('admin/emailtemplate_model');
                if (count($userDetails) > 0 && $data == true) {

                    $emailtemplate = $this->emailtemplate_model->get_emailtempate_details(12);
                    if ($emailtemplate) {
                        $emailData = array();
                        $emailData['toEmail'] = $userDetails['Email'];
                        $arrSearch = array('##FIRST_NAME##', "##EMAIL##", "##PASSWORD##");
                        if ($userDetails['FirstName'] == "" && $userDetails['LastName'] == "")
                            $userName = "User";
                        else
                            $userName = $userDetails['FirstName'] . ' ' . $userDetails['LastName'];
                        $arrReplace = array(ucwords($userName), $userDetails['Email'], $this->input->post('pass'));

                        $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                        $emailData['subject'] = $subject;

                        $body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                        $emailData['body'] = $body;

                        $emailData['fromEmail'] = 'noreply@melstm.net';

                        send_mail($emailData);
                        echo "mail-sent";
                        die;
                    }
                }else {
                    echo "There is some problem in sending email. Please try again later!";
                }
            }
        }
    }

    /* Function for creating new events */

    public function provisionEvent($id = '') {
        $config = $this->settings_model->getConfig();

        if (!empty($config)) {
            $label_for_mentor = 'Number Of ' . $config[0]->mentorName;
        } else {
            $label_for_mentor = 'Number Of Mentors';
        }
        if (!empty($config)) {
            $label_for_mentee = 'Number Of ' . $config[0]->menteeName;
        } else {
            $label_for_mentee = 'Number Of Mentees';
        }
        if (!empty($config)) {
            $maxMember = $config[0]->maxMember;
        } else {
            $maxMember = 0;
        }
        $data['label_for_mentor'] = $label_for_mentor;
        $data['label_for_mentee'] = $label_for_mentee;


        $this->load->library('form_validation');
        $this->form_validation->set_rules('subdomainName', 'Sub domain Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('number_of_mentors', $label_for_mentor, 'required|numeric');
        $this->form_validation->set_rules('number_of_mentees', $label_for_mentee, 'required|numeric');
        $this->form_validation->set_rules('number_of_teams', "Number OF Teams", 'required|numeric');
        if ($this->form_validation->run() === TRUE) {
            if ($id == 0) {
                $microtime = microtime();
                $verificationCode = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime);

                $subdomainName = strtolower($_REQUEST['subdomainName']);
                $addres = $this->dashboard_model->addSubDomain($subdomainName, $_REQUEST['email'], $verificationCode); //,$_REQUEST['programmeName']
                if ($addres > 0) {
                    $data['verificationCode'] = $verificationCode;
                    $data['userId'] = $addres;

                    $email_data = array(
                        'toEmail' => $_REQUEST['email'],
                        'fromEmail' => 'noreply@melstm.net',
                        'subject' => 'Confirm Registration',
                        'body' => $this->load->view('verifyRegistration', $data, TRUE)
                    );

                    if (send_mail($email_data)) {
                        $this->load->dbforge();
                        if ($this->db->query("CREATE DATABASE IF NOT EXISTS " . DB_TEMPLATE . "_" . $subdomainName . "")) {
                            $tables = $this->db->list_tables();

                            foreach ($tables as $table) {
                                $res = $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_TEMPLATE . "_" . $subdomainName . "." . $table . " LIKE " . DB_NAME . "." . $table . "");
                                if ($res == 0) {
                                    $this->dbforge->drop_database($subdomainName);
                                    $this->session->set_flashdata('error', 'Problem while copying data');
                                } else {
                                    if ($table == 'mentor_subdomains') {
                                        $res = $this->db->query("INSERT INTO " . DB_TEMPLATE . "_" . $subdomainName . "." . $table . " SELECT * FROM " . DB_NAME . "." . $table . " WHERE " . DB_NAME . "." . $table . ".subdomain_name='$subdomainName' AND " . DB_NAME . "." . $table . ".Status=1");
                                    } else if ($table == 'mentor_admindetails') {
                                        
                                    } else {
                                        $res = $this->db->query("INSERT INTO " . DB_TEMPLATE . "_" . $subdomainName . "." . $table . " SELECT * FROM " . DB_NAME . "." . $table . "");
                                    }
                                    $this->session->set_flashdata('success', 'Database Created successfully');
                                }
                            }
                            /* creating entries for new events only */
                            $this->dashboard_model->createEventsEntries(DB_TEMPLATE . "_" . $subdomainName, $_REQUEST['number_of_mentors'], $_REQUEST['number_of_mentees'], $_REQUEST['number_of_teams'], $maxMember);
                            /* Ends here */
                        } else {
                            $this->session->set_flashdata('error', 'Error Creating database');
                        }
                        $this->session->set_flashdata('success', 'Please check your mail');
                    } else {
                        $this->session->set_flashdata('error', show_error($this->email->print_debugger()));
                    }
                } else {
                    $this->session->set_flashdata('error', 'Error Creating database');
                }
            } else {
                $subdomainName = strtolower($_REQUEST['subdomainName']);
                $addres = $this->dashboard_model->updateSubDomain($_REQUEST['email'], $id, $subdomainName); //,$_REQUEST['programmeName']
                $this->session->set_flashdata('success', 'School Updated Successfully');
            }

            redirect('superadmin/dashboard');
        }

        $fields = get_table_fields('subdomains');
        $subdomains = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $subdomains[$field] = '';
            }
        }
        if ($id) {
            $subdomains = $this->subadmin_model->edit_subadmin($id);
        }

        $data['id'] = $id;
        $data['res'] = $subdomains;
        $this->template->content->view('superadmin/newEvent', $data);
        $this->template->publish_admin();
    }

    /* Above function Ends here */
}
