<?php

ob_start();

class Settings extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/settings_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
        $res = loadDatabase();
        //print_r($res);exit;
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            //print_r($x);exit;
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
    }

    //=========================================
    // Settings LISTING 
    //=========================================
    public function index() {


        $res['config'] = $this->settings_model->getConfig();


        $this->load->view('includes/header_common');
        $this->load->view('superadmin/settings/settings', $res);
        $this->load->view('includes/footer_common');
    }

    public function updateConfig() {
        //print_r($_REQUEST);
        $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/mentor/assets/admin/images/";

        if ($_FILES["logo"]["name"] != "") {
            $target_file = $target_dir . basename($_FILES["logo"]["name"]);
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);


            // Allow certain file formats
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["logo"]["tmp_name"], $target_file)) {
                    echo "The file " . basename($_FILES["logo"]["name"]) . " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
        }

        /*         * ********** code to upload icon *********** */

        if ($_FILES["icon"]["name"] != "") {

            $target_file_icon = $target_dir . basename($_FILES["icon"]["name"]);
            $uploadOk_icon = 1;
            $imageFileType_icon = pathinfo($target_file_icon, PATHINFO_EXTENSION);


            // Allow certain file formats
            if ($imageFileType_icon != "ico") {
                echo "Sorry, only ico files are allowed.";
                $uploadOk_icon = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk_icon == 0) {

                echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
            } else {

                if (move_uploaded_file($_FILES["icon"]["tmp_name"], $target_file_icon)) {
                    echo "The file " . basename($_FILES["icon"]["name"]) . " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
        }
        //exit;
        $config = $this->settings_model->getConfig();
        $this->session->unset_userdata('headerColor');
        $this->session->unset_userdata('leftBarColor');
        $this->session->unset_userdata('buttonColor');
        $this->session->set_userdata('headerColor', "#" . $_REQUEST['headerColor']);
        $this->session->set_userdata('leftBarColor', "#" . $_REQUEST['leftBarColor']);
        $this->session->set_userdata('buttonColor', "#" . $_REQUEST['buttonColor']);
        $this->session->set_userdata('fontColor', "#" . $_REQUEST['fontColor']);
        $this->session->set_userdata('backgroundColor', "#" . $_REQUEST['backgroundColor']);
        $this->session->set_userdata('highlightColor', "#" . $_REQUEST['highlightColor']);
        $this->session->set_userdata('leftMenuFont', "#" . $_REQUEST['leftMenuFont']);
        $this->session->set_userdata('tableFont', "#" . $_REQUEST['tableFont']);
        $this->session->set_userdata('tableBackground', "#" . $_REQUEST['tableBackground']);


        //echo $this->session->userdata('leftBarColor');;exit;

        if ($_FILES["logo"]["name"] != "")
            $this->session->set_userdata('logo', $_FILES["logo"]["name"]);
        else
            $this->session->set_userdata('logo', $config[0]->logo);

        $img = "";
        if ($_FILES["logo"]["name"] != "")
            $img = $_FILES["logo"]["name"];
        else
            $img = $config[0]->logo;
        
          /*         * *********code for icon *************** */

        if ($_FILES["icon"]["name"] != "")
            $this->session->set_userdata('icon', $_FILES["icon"]["name"]);
        else
            $this->session->set_userdata('icon', $config[0]->icon);

        $img_icon = "";
        if ($_FILES["icon"]["name"] != "")
            $img_icon = $_FILES["icon"]["name"];
        else
            $img_icon = $config[0]->icon;

        $res = $this->settings_model->updateConfig($_REQUEST, $img,$img_icon);
        ob_start();
        redirect("superadmin/settings/settings");
    }

}

?>