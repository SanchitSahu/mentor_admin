<?php

class Help extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('superadmin/help_model', 'help_model');
		$this->load->model('admin/settings_model');

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if(!loginCheck()){redirect('admin');}
		$res	=	loadDatabase();
		//print_r($res);exit;
		if($res != 0){
			$x	=	$this->load->database($res,TRUE);
			//print_r($x);exit;
			$this->db	=	$x;	
		}else{
			redirect("400.shtml");exit;
		}
    }

    //=========================================
    // SKILL LISTING 
    //=========================================
    public function index() {

        $tmpl = array ( 
            'table_open'  => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open'  => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">' 
        );
		
		$error['error']	=	"";
        $this->table->set_template($tmpl);
        $this->table->set_heading('Screen Type', 'Screen Name', 'Screen Identifier', 'User Type', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');
                
        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');
       
        $this->template->content->view('superadmin/help/list',$error);
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
		$config = $this->settings_model->getConfig();
        $this->datatables->select("Id, ScreenType, ScreenName, ScreenIdentifier, CASE WHEN UserType=0 THEN  '{$config[0]->mentorName}' WHEN UserType=1 THEN '{$config[0]->menteeName}' WHEN UserType=2 THEN 'Both Users' END AS UserType, Weight",FALSE)
        ->unset_column('Id')
        ->unset_column('Actions')
        ->add_column('Actions', get_Edit_Delete_Buttons('$1','superadmin/help'),'Id')
        ->from('help')
        ->where('Status','1');
               
        echo $this->datatables->generate();
    }


    //=========================================
    // SKILL ADD 
    //=========================================
    public function add($id = '') {
		$data = array();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('UserType', 'User Type', 'required');
		$this->form_validation->set_rules('ScreenName', 'Screen Name', 'required');
		$this->form_validation->set_rules('HelpDescription', 'Help Description', 'required');
		//$this->form_validation->set_rules('Weight', 'Weight', '');

        if ($this->form_validation->run() === TRUE) {
			// echo $this->input->post('UserType');exit;
            $data = array(
				'ScreenType'  => $this->input->post('ScreenType'),
                'UserType'  => $this->input->post('UserType'),
				'ScreenName'  => $this->input->post('ScreenName'),
				'ScreenIdentifier'  => $this->input->post('ScreenIdentifier'),
				'HelpDescription'  => $this->input->post('HelpDescription'),
                'Weight'  => $this->input->post('Weight'),
				'HeaderId' => ($this->input->post('ScreenHeader') == '') ? null : $this->input->post('ScreenHeader'),
				'FooterId' => ($this->input->post('ScreenFooter') == '') ? null : $this->input->post('ScreenFooter')
            );
			
            if ($id == 0) {
				//echo "<pre>";var_dump($id);print_r($_POST);echo "</pre>";exit;
                $result = $this->db->insert('help', $data);
                $id = $this->db->insert_id();
                $this->session->set_flashdata('success', 'Help Added Successfully');
            } else {
				//echo "<pre>";var_dump($id);print_r($_POST);echo "</pre>";exit;
                $this->db->where('Id', $id);
                $result = $this->db->update('help', $data);
                $this->session->set_flashdata('success', 'Help Updated Successfully');
            }
            redirect('superadmin/help');
        }
         
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datepicker/css/datepicker.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'bootstrap-datetimepicker/css/datetimepicker.css');
        
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/moment.min.js');
        $this->template->javascript->add($this->config->item('base_js') . 'bootstrap-daterangepicker/daterangepicker.js');
        
       
        $fields = get_table_fields('help');
        $help = array();
        if (isset($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $help[$field] = '';
            }
        }
        
        if ($id) {
             $help = $this->help_model->get_help($id);
        }
        $data['id'] = $id;
        $data['help'] = $help;
		$data['config'] = $this->settings_model->getConfig();
		$data['headerList'] = $this->help_model->getHeaderList();
		$data['footerList'] = $this->help_model->getFooterList();
		$data['screenList'] = $this->help_model->getScreenList();
		//echo "<pre>";print_r($data['screenList']);exit;
       
        $this->template->content->view('superadmin/help/add', $data);
        $this->template->publish_admin();

    }
    
    //=========================================
    // SKILL EDIT 
    //=========================================
    public function edit($id){ 
       $this->add($id);
    }
	
    //=========================================
    // SKILL DELETE 
    //=========================================
     public function delete($id){ 
        $result = $this->help_model->delete_help($id);
		if($result) {
			$this->session->set_flashdata('success', 'Help Deleted Successfully');
		} else {
			$this->session->set_flashdata('error', 'Error occured while Deleting Help Text. Please Try Again');
		}
		redirect('superadmin/help');
         
    }

}