<?php

class Managesubadmin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('superadmin/subadmin_model');
        $this->load->model('superadmin/dashboard_model');
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->helper('datatables');
        if (!loginCheck()) {
            redirect('admin');
        }
    }

    //=========================================
    // Subdomain Listing LISTING 
    //=========================================
    public function index() {

        $tmpl = array(
            'table_open' => '<table id="dynamic_table" class="display table table-bordered table-striped table-hover" >',
            'thead_open' => '<thead class="gridhead">',
            'heading_cell_start' => '<th class="sorting">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('School Name', 'Email', 'Display Order', 'Actions');

        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
        $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

        $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
        $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
        $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');

        $this->template->content->view('superadmin/manageadmin/list');
        $this->template->publish_admin();
    }

    //=========================================
    // DATATABLE CALLBACK FUNCTON 
    //=========================================
    function datatable() {
        $this->datatables->select("subdomain_id, subdomain_name,CONCAT(FirstName, ' ', LastName), Email,Weight", FALSE)//DepartmentName,
                ->unset_column('subdomain_id')
                ->edit_column('subdomain_name', '<a style="color:#0000ff" href="/mentor_staging/superadmin/managesubadmin/getSubdomain/$1">$2</a>', 'subdomain_id,subdomain_name')
                // ->add_column('Actions', get_Delete_Buttons('$1','superadmin/managesubadmin'),'subdomain_id')
                ->add_column('Actions', get_ResetPwd_Delete_Buttons('$1', 'superadmin/managesubadmin'), 'subdomain_id')
                ->from('subdomains')
                ->where('Status', '1');

        echo $this->datatables->generate();
    }

    //=========================================
    // Subdomain DELETE 
    //=========================================
    public function delete($id) {
        $this->subadmin_model->delete_subadmin($id);
        $this->session->set_flashdata('success', 'Sub Admin Delete Successfully');
        //redirect('superadmin/managesubadmin');
        redirect('superadmin/dashboard');
    }

    public function getSubdomain($id) {
        $superAdminData = $this->session->userdata('admin_data');
        $this->session->set_userdata('superadmin', $superAdminData);
        $getSubdomainName = $this->subadmin_model->getSubdomainName($id);
        $result = $this->subadmin_model->getLoginDetails($id, $getSubdomainName['subdomain_name']);
        $user['id'] = $result['subdomain_id'];
        $user['name'] = $result['FirstName'] . ' ' . $result['LastName'];
        $user['UserType'] = 'Admin';
        $user['profilePicture'] = $result['PictureURL'];
        $user['subdomain_name'] = $result['subdomain_name'];
        $this->session->set_userdata('admin_data', $user);
        $url = "http://" . $result['subdomain_name'] . ".melstm.net/mentor_staging/admin/dashboard";
        redirect($url);
    }

    /* public function edit($id){ 

      $this->load->library('form_validation');
      $this->form_validation->set_rules('subdomainName', 'Sub domain Name', 'required');
      $this->form_validation->set_rules('email', 'Email', 'required');

      $res['res']	=	$this->subadmin_model->edit_subadmin($id);



      $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_page.css');
      $this->template->stylesheet->add($this->config->item('base_js') . 'advanced-datatable/css/demo_table.css');
      $this->template->stylesheet->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.css');

      $this->template->javascript->add($this->config->item('base_js') . 'advanced-datatable/js/jquery.dataTables.js');
      $this->template->javascript->add($this->config->item('base_js') . 'data-tables/DT_bootstrap.js');
      $this->template->javascript->add($this->config->item('base_js') . 'custom-datatable-init.js');


      $this->template->content->view('superadmin/manageadmin/editSchool',$res);
      $this->template->publish_admin();



      } */
}
