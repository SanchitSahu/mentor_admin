<?php
class Relationship extends Admin_Controller {

    function __construct() {
        parent::__construct();
		if(!loginCheck()){redirect('admin');}
		$this->load->model('admin/relationship_model');
    }

	// load dashboard
    public function index(){ 
		
		$res['mentor']	=	$this->relationship_model->get_mentors();
		$res['mentee']	=	$this->relationship_model->get_mentees();
		$this->template->publish_admin();
		$this->load->view('admin/relationship/relationship',$res);
    }
	
	public function addRelation()
	{
		$mentor	=	$_REQUEST['mentor'];
		$mentee	=	$_REQUEST['mentee'];
		$res['mentee']	=	$this->relationship_model->addRelation($mentor,$mentee);
	}
}
