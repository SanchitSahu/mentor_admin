<?php

class Addtocalendar extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('addtocalendar_model');
    }

    public function index($school = 'noschool', $meeting_id = 0, $userType) {
        error_reporting(E_ALL);
        ini_set("display_errors","ON");
        $data = array();
        if ($meeting_id == 0 || $school == 'noschool') {
            redirect(base_url());
            exit;
        } else {
            $res = loadDatabaseFromSchoolName($school);
            if ($res != 0) {
                $x = $this->load->database($res, TRUE);
                $this->db = $x;
            } else {
                redirect(base_url());
                exit;
            }
        }
        
        $result = $this->addtocalendar_model->getMeetingInfo($meeting_id);
        $data['UserType'] = ($userType == 0) ? 'mentor' : 'mentee';
        $data['MeetingDetail'] = $result[0];
        //echo "<pre>";print_r($result);echo "</pre>";
        $this->load->view('addtocalendar', $data);
    }

}

?>