<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Addtocalendar_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function getMeetingInfo($meeting_id) {        
        $this->db->select('mi.InvitationID, mi.MentorID, mm.MentorName, mi.MenteeID, me.MenteeName, mi.MeetingDatetime, mi.MeetingTypeID, mmt.MeetingTypeName, mi.TopicID, mto.Topicdescription, mi.MeetingPlaceID, mmp.MeetingPlaceName', false);
        $this->db->from('mentor_invitation mi');
        $this->db->join('mentor_mentor mm', 'mm.MentorID=mi.MentorID', 'LEFT');
        $this->db->join('mentor_mentee me', 'me.MenteeID=mi.MenteeID', 'LEFT');
        $this->db->join('mentor_meetingtype mmt', 'mmt.MeetingTypeID=mi.MeetingTypeID', 'LEFT');
        $this->db->join('mentor_meetingplace mmp', 'mmp.MeetingPlaceID=mi.MeetingPlaceID', 'LEFT');
        $this->db->join('mentor_topic mto', 'mto.TopicID=mi.TopicID', 'LEFT');
        $this->db->where('mi.InvitationID', $meeting_id);
        $results = $this->db->get();
        return $results->result();
    }

}
