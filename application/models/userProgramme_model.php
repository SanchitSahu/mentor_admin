<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Userprogramme_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_programme($id) { 
        $query = $this->db->get_where('menteeprogrammes', array('programmeID' => $id));
        return $query->row_array();
    }
    
    
    public function delete_programme($id) { 
           
        $data=array('Status'=>'0');
        $this->db->where('programmeID', $id);
        $result = $this->db->update('menteeprogrammes', $data);
               
        /*$data=array('Status'=>'0');
        $this->db->where('userRoleID', $id);
        $result = $this->db->update('mentorskillset', $data);
        
         $data=array('Status'=>'0');
        $this->db->where('userRoleID', $id);
        $result = $this->db->update('menteeskill', $data);*/
      
    }
    

}