<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Userrole_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_roles($id) { 
        $query = $this->db->get_where('user_roles', array('userRoleID' => $id));
        return $query->row_array();
    }
    
    
    public function delete_roles($id) { 
           
        $data=array('Status'=>'0');
        $this->db->where('userRoleID', $id);
        $result = $this->db->update('user_roles', $data);
               
        /*$data=array('Status'=>'0');
        $this->db->where('userRoleID', $id);
        $result = $this->db->update('mentorskillset', $data);
        
         $data=array('Status'=>'0');
        $this->db->where('userRoleID', $id);
        $result = $this->db->update('menteeskill', $data);*/
      
    }
    

}