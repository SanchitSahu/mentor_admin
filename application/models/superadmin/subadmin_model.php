<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subadmin_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
    public function delete_subadmin($id) { 
    
        $data=array('Status'=>'0');
        $this->db->where('subdomain_id', $id);
        $result = $this->db->update('subdomains', $data);
		
		
		$query = $this->db->get_where('subdomains', array('subdomain_id' => $id));
        $res	=	$query->row_array();
		$subdomainName	=	$res['subdomain_name'];
		
		$res1	=	$this->db->query("update c10melstm_net_$subdomainName.mentor_subdomains set Status=0 where subdomain_id=$id");
		$newName	=	'c10melstm_net_'.$subdomainName.date('YmdHis');
		
		
		$tables = $this->db->list_tables();
		//echo "<pre>";print_r($tables);exit;
		$res2	=	$this->db->query("CREATE database $newName");
		foreach ($tables as $table) {
			$this->db->query("RENAME TABLE c10melstm_net_$subdomainName.$table TO $newName.$table");
			//echo $this->db->last_query();exit;
		}
		
		$this->db->query("DROP database c10melstm_net_$subdomainName");				
		
		//$this->load->dbforge();
		//$this->dbforge->drop_database($subdomainName);
    }
	
	public function getSubdomainName($id)
	{
		$query = $this->db->get_where('subdomains', array('subdomain_id' => $id));
		$res	=	$query->row_array();
		return $res;
	}
	
	public function getLoginDetails($id, $subdomain='')
	{
		if(empty($subdomain))
			$query = $this->db->get_where('subdomains', array('subdomain_id' => $id));
		else {
			$subdomainname	=	'c10melstm_net_'.$subdomain;
			$query = $this->db->get_where("$subdomainname.subdomains", array('subdomain_id' => $id));
		}	
        $res	=	$query->row_array();
		return $res;
	}
	
	
	function edit_subadmin($id){
		$sql = $this->db->select('*')
	    	->from('subdomains')
			->where("subdomain_id",$id)
			->where("Status",1)
	    	->get();
        return $sql->row_array();
	}

}