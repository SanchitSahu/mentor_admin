<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Emailtemplate_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    
    /**
     * This function is use for getting detail for state
     * @param type $stateId
     * @return type
     */
    function get_emailtempate_details($emailtemplateid) {
        try {
            $this->db->select('');
            $this->db->from('c10melstm_net_template.emailtemplate');
            $this->db->where('EmailTemplateId', $emailtemplateid);
            $email = $this->db->get();
            //echo $this->db->last_query();
            $data = $email->row_array();
            $email->free_result();
            return $data;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }
    
    function delete_emailtemplate($templateId){
        $data=array('Status'=>'0');
        $this->db->where('EmailTemplateId', $templateId);
        $result = $this->db->update('emailtemplate', $data);        
    }

}
