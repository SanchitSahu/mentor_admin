<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getsubdomain($subDomainName) {
        $sql = $this->db->select('*')
                ->from('subdomains')
                ->where("subdomain_name", $subDomainName)
                ->where("Status", 1)
                ->get();
        //$result = $sql->row_array();
        return $sql->num_rows();
    }

    public function addSubDomain($subdomainName, $email, $verificationCode) {//,$programmeName
        $data = array(
            'subdomain_name' => $subdomainName,
            'Email' => $email,
            'verificationCode' => $verificationCode,
            'Password' => fnEncrypt('admin123', $this->config->item('mentorKey')),
                //'DepartmentName' => $programmeName,
        );

        $result = $this->db->insert('subdomains', $data);
        $id = $this->db->insert_id();
        return $id;
    }

    public function checkemail($email) {
        $sql = $this->db->select('*')
                ->from('subdomains')
                ->where("Email", $email)
                ->where("Status", 1)
                ->get();
        //$result = $sql->row_array();
        return $sql->num_rows();
    }

    public function checkemail_edit($email, $subdomain_id) {
        $sql = $this->db->select('*')
                ->from('subdomains')
                ->where("Email", $email)
                ->where("subdomain_id !=", $subdomain_id)
                ->where("Status", 1)
                ->get();
        //$result = $sql->row_array();
        return $sql->num_rows();
    }

    public function checksubdomain($subdomain_name) {
        $sql = $this->db->select('*')
                ->from('subdomains')
                ->where("subdomain_name", $subdomain_name)
                ->where("Status", 1)
                ->get();
        //$result = $sql->row_array();
        return $sql->num_rows();
    }

    public function checksubdomain_edit($subdomain_name, $subdomain_id) {
        $sql = $this->db->select('*')
                ->from('subdomains')
                ->where("subdomain_name", $subdomain_name)
                ->where("subdomain_id !=", $subdomain_id)
                ->where("Status", 1)
                ->get();
        //$result = $sql->row_array();
        return $sql->num_rows();
    }

    function checkVerificationCode($verificationCode, $subdomain_id) {
        $this->db->where("subdomain_id", $subdomain_id);
        $this->db->where("verificationCode", $verificationCode);
        $query = $this->db->get("subdomains");

        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0)
            return 1;
        else
            return 0;
    }

    function deleteVerificationCode($verificationCode, $subdomain_id, $password) {
        $query = $this->db->get_where('subdomains', array('subdomain_id' => $subdomain_id));
        if ($query->num_rows() > 0) {
            $result = $query->result();
            $subdomain_name = $result[0]->subdomain_name;
        } else {
            return 0;
        }

        $data = array(
            'isConfirmed' => 1,
            'verificationCode' => "",
            'Password' => fnEncrypt($password, $this->config->item('mentorKey'))
        );
        $this->db->where("subdomain_id", $subdomain_id);
        $this->db->where("verificationCode", $verificationCode);
        $this->db->update("c10melstm_net_$subdomain_name.subdomains", $data);

        $this->db->where("subdomain_id", $subdomain_id);
        $this->db->where("verificationCode", $verificationCode);
        $this->db->update('subdomains', $data);
        return $this->db->affected_rows();
    }

    function resetpassword($id, $pass, $subdomain_name) {
        $data = array(
            'Password' => $pass
        );

        $this->db->where("subdomain_id", $id);
        $result = $this->db->update('subdomains', $data);
        $this->db->where("subdomain_id", $id);
        $result = $this->db->update("c10melstm_net_$subdomain_name.subdomains", $data);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function updateSubDomain($email, $id, $subdomainName) {//$programmeName,
        $data = array(
            'Email' => $email,
                //	'DepartmentName' => $programmeName,
        );

        $this->db->where("subdomain_id", $id);
        $result = $this->db->update('subdomains', $data);
        $idreturn = $this->db->affected_rows();

        //, DepartmentName=".$this->db->escape($programmeName)."
        $res1 = $this->db->query("update c10melstm_net_$subdomainName.mentor_subdomains set Email='$email' where subdomain_id=$id");
        //echo $this->db->last_query();exit;
        return $idreturn;
    }

    /* Function for creating new events entries */

    public function createEventsEntries($db_name, $number_of_mentors, $number_of_mentees, $number_of_teams, $maxMember) {
        $mentor_table_name = $db_name . '.mentor_mentor';
        $mentor_mentorcontactinfo_table_name = $db_name . '.mentor_mentorcontactinfo';
        $mentor_usertosource_table_name = $db_name . '.mentor_usertosource';
        $mentor_usertorole_table_name = $db_name . '.mentor_usertorole';

        for ($i = 1; $i <= $number_of_mentors; $i++) {
            $mentor_data = array(
                'MentorName' => '_mentor' . str_pad($i, 2, '0', STR_PAD_LEFT)
            );
            $this->db->insert($mentor_table_name, $mentor_data);
            $id = $this->db->insert_id();

            $mentor_mentorcontactinfo = array(
                'MentorID' => $id,
                'Password' => fnEncrypt(MENTOR_DEFAULT_PASSWORD, $this->config->item('mentorKey')),
            );
            $this->db->insert($mentor_mentorcontactinfo_table_name, $mentor_mentorcontactinfo);

            $mentor_usertosource = array(
                'MentorID' => $id,
                'MentorSourceID' => 6,
            );
            $this->db->insert($mentor_usertosource_table_name, $mentor_usertosource);

            $mentor_usertorole = array(
                'MentorID' => $id,
                'userRoleID' => 6,
            );
            $this->db->insert($mentor_usertorole_table_name, $mentor_usertorole);
        }

        $mentee_table_name = $db_name . '.mentor_mentee';
        $mentor_menteecontact_table_name = $db_name . '.mentor_menteecontact';
        $mentor_menteetoprogramme_table_name = $db_name . '.mentor_menteetoprogramme';
        $mentor_menteetosource_table_name = $db_name . '.mentor_menteetosource';
        $mentor_team_table_name = $db_name . '.mentor_team';

        for ($j = 1; $j <= $number_of_mentees; $j++) {
            $mentee_data = array(
                'MenteeName' => '_mentee' . str_pad($j, 2, '0', STR_PAD_LEFT)
            );
            $this->db->insert($mentee_table_name, $mentee_data);
            $mentee_id = $this->db->insert_id();

            $mentor_menteecontact = array(
                'MenteeId' => $mentee_id,
                'Password' => fnEncrypt(MENTEE_DEFAULT_PASSWORD, $this->config->item('mentorKey')),
            );
            $this->db->insert($mentor_menteecontact_table_name, $mentor_menteecontact);

            $mentor_menteetoprogramme = array(
                'MenteeID' => $mentee_id,
                'ProgrammeID' => 5,
            );
            $this->db->insert($mentor_menteetoprogramme_table_name, $mentor_menteetoprogramme);

            $mentor_menteetosource = array(
                'MenteeID' => $mentee_id,
                'MenteeSourceID' => 5,
            );
            $this->db->insert($mentor_menteetosource_table_name, $mentor_menteetosource);

            if ($j <= $number_of_teams) {
                $mentor_team = array(
                    'TeamName' => '_team' . str_pad($j, 2, '0', STR_PAD_LEFT),
                    'TeamOwnerUserID' => $mentee_id,
                    'MaxMembers' => $maxMember,
                    'TeamOwnerUserType' => '1',
                    'TeamSkills' => '9',
                    'CreatedDate' => date('Y-m-d H:i:s')
                );
                $this->db->insert($mentor_team_table_name, $mentor_team);
            }
        }
        return true;
    }

    /* Above function Ends here */
}
