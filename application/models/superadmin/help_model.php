<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Help_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_help($id) { 
        $query = $this->db->get_where('help', array('Id' => $id));
        return $query->row_array();
    }
    
    
    public function delete_help($id) {
        $data=array('Status'=>'0');
        $this->db->where('Id', $id);
        if ($this->db->update('help', $data)) {
            return true;
        } else {
            return false;
        }
        
    }
    
    public function getHeaderList() {
        $returnValue = array('' => '--Select Screen Header--');
        
        $this->db->select("Id, CONCAT(ScreenIdentifier, ' (' , ScreenName, ')') as Name", false);
        $this->db->where('ScreenType', 'header');
        $this->db->where('Status', 1);
        $result = $this->db->get('mentor_help');
        if($result->num_rows() > 0) {
            $resultSet = $result->result();
            foreach($result->result() as $eachHead) {
                $returnValue[$eachHead->Id] = $eachHead->Name;
            }
        }
        return $returnValue;
    }
    
    public function getFooterList() {
        $returnValue = array('' => '--Select Screen Footer--');
        
        $this->db->select("Id, CONCAT(ScreenIdentifier, ' (' , ScreenName, ')') as Name", false);
        $this->db->where('ScreenType', 'footer');
        $this->db->where('Status', 1);
        $result = $this->db->get('mentor_help');
        if($result->num_rows() > 0) {
            $resultSet = $result->result();
            foreach($result->result() as $eachHead) {
                $returnValue[$eachHead->Id] = $eachHead->Name;
            }
        }
        return $returnValue;
    }
    
    public function getScreenList() {
        $returnValue = array('' => '--Select Screen ID--');
        
        $this->db->select("ScreenName as Id, ScreenName as Name", false);
        //$this->db->where('ScreenType', 'screen_text');
        $this->db->where('Status', 1);
        $result = $this->db->get('mentor_help');
        if($result->num_rows() > 0) {
            $resultSet = $result->result();
            foreach($result->result() as $eachHead) {
                $returnValue[$eachHead->Id] = $eachHead->Name;
            }
        }
        return $returnValue;
    }
    

}