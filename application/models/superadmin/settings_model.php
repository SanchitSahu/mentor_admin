<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getConfig() {
        $query = $this->db->get("settings");
        //echo $this->db->last_query();exit;
        return $query->result();
    }

    public function updateConfig($data2, $target_file,$icon_target_file) {
        $data = array(
            'mentorName' => ucfirst($data2['mentorName']),
            'menteeName' => ucfirst($data2['menteeName']),
            'headerColor' => "#" . $data2['headerColor'],
            'leftBarColor' => "#" . $data2['leftBarColor'],
            'logo' => $target_file,
            'icon' => $icon_target_file,
            'buttonColor' => "#" . $data2['buttonColor'],
            'fontColor' => "#" . $data2['fontColor'],
            'backgroundColor' => "#" . $data2['backgroundColor'],
            'highlightColor' => "#" . $data2['highlightColor'],
            'leftMenuFont' => "#" . $data2['leftMenuFont'],
            'tableFont' => "#" . $data2['tableFont'],
            'tableBackground' => "#" . $data2['tableBackground']
        );
        $this->db->update('settings', $data);
        return $this->db->affected_rows();
    }

	public function errorLog() {
		$this->db->where("Status",1);
        $query = $this->db->get("errors");
        
        return $query->num_rows();
    }
}
