<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Relationship_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

	public function get_mentors() { 
    	$sql = $this->db->select('m.*,mci.*')
	    	->from('mentor m')
	    	->join('mentorcontactinfo mci', 'mci.MentorID = m.MentorID', 'left')
	    	->get();
        //$result = $sql->row_array();
        return $sql->result();
    }
    
	public function get_mentees() { 
    	$sql = $this->db->select('m.*,mci.*')
	    	->from('mentee m')
	    	->join('menteecontact mci', 'mci.MenteeId = m.MenteeId', 'left')
	    	->get();
       // $result = $sql->row_array();
        return $sql->result();
    }
	
	public function addRelation($mentor,$mentee)
	{
		$data = array(
		    'MentorID'	=>	$mentor,
			'MenteeID'	=>	$mentee
			);
			$this->db->insert('relationship', $data); 
			return $this->db->insert_id();
	}
    
}