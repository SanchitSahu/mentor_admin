<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Meetingplace_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_meetingplace($id) {
        $query = $this->db->get_where('meetingplace', array('MeetingPlaceID' => $id));
        return $query->row_array();
    }

    public function delete_meetingplace($id) {

        $data = array('Status' => '0');
        $this->db->where('MeetingPlaceID', $id);
        $result = $this->db->update('meetingplace', $data);

        $data = array('Status' => '0');
        $this->db->where('MeetingPlaceID', $id);
        $result = $this->db->update('meetinginfo', $data);
    }

    public function checkMeetingPlace($MeetingPlaceName) {
        try {
            $this->db->where('MeetingPlaceName', $MeetingPlaceName);
            $this->db->where('Status', 1);
            $this->db->from('meetingplace');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function checkMeetingPlaceEdit($MeetingPlaceName, $MeetingPlaceID) {
        try {
            $this->db->where('MeetingPlaceName', $MeetingPlaceName);
            $this->db->where('MeetingPlaceID !=', $MeetingPlaceID);
            $this->db->where('Status', 1);
            $this->db->from('meetingplace');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

}
