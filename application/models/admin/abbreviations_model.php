<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Abbreviations_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_abbreviation($id) {
        $query = $this->db->get_where('abbreviations', array('abbreviationId' => $id));
        return $query->row_array();
    }

    public function delete_abbreviation($id) {
        $data = array('Status' => '0');
        $this->db->where('abbreviationId', $id);
        $result = $this->db->update('abbreviations', $data);
    }

    public function checkAbbreviations($AbbreviationKey) {
        try {
            $this->db->where('AbbreviationKey', $AbbreviationKey);
            $this->db->where('Status', 1);
            $this->db->from('abbreviations');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function checkAbbreviationsEdit($AbbreviationKey, $AbbreviationId) {
        try {
            $this->db->where('AbbreviationKey', $AbbreviationKey);
            $this->db->where('AbbreviationId !=', $AbbreviationId);
            $this->db->where('Status', 1);
            $this->db->from('abbreviations');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

}
