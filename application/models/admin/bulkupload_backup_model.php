<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bulkupload_backup_model extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    /*
     * Check login process and create session
     * @return void
     */

    function getGenericHeaders($tbl_name) {
        $query = $this->db->query("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" . $tbl_name . "' AND TABLE_SCHEMA = '" . DB_NAME . "'");
        return $query->result();
    }

    function saveDataFromCSV($data, $tbl_name) {
        foreach ($data as $prod) {
//print("<pre>");var_dump(debug_backtrace());print("</pre>");
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: prod=<br/>");print_r($prod);print("</pre>\n");

            if ($this->db->insert($tbl_name, $prod)) {
                //echo "Inserted successfully<br>";	
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: Inserted successfully<br/></pre\n");	
            } else {
                $errNo = $this->db->_error_number();
                $errMess = $this->db->_error_message();
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: Insert error message=".$errMess."<br/></pre\n");	
                echo $errNo . "<br>" . $errMess . "<br>";
                exit;
            }
        }
    }

    function checkSubdomain($domainName) {
        $query = $this->db->query("SELECT * FROM mentor_subdomains WHERE subdomain_name = '" . $domainName . "'");

        //echo $this->db->last_query();exit;
        return $query->result();
        /* $i=0;
          foreach($query->result() as $header)
          {
          $columns[$i] = 	$header->COLUMN_NAME;
          $i++;
          }
          return $columns; */
    }

    function checkRoleExists($RoleCheck) {
        $query = $this->db->query("SELECT * FROM mentor_user_roles WHERE UserRoleName = '" . $RoleCheck . "'");

        //echo $this->db->last_query();exit;
        return $query->result();
    }

    function checkSourceExists($SourceCheck) {
        $query = $this->db->query("SELECT * FROM mentor_mentorsourcenames WHERE  MentorSourceName = '" . $SourceCheck . "'");

        //echo $this->db->last_query();exit;
        return $query->result();
    }

    function addMentor($mentorName, $mentorStatus = '', $mentorWeight = '') {
        $data = array(
            'MentorName' => $mentorName
        );
        if (!empty($mentorStatus)) {
            $data['Status'] = $mentorStatus;
        }
        if (!empty($mentorWeight)) {
            $data['Weight'] = $mentorWeight;
        }
        $result = $this->db->insert('mentor', $data);
        //echo $this->db->last_query();exit;
        return $this->db->insert_id();
    }

    function addContact($addMentor, $MentorPhone, $MentorEmail) {
        $data = array(
            'MentorID' => $addMentor,
            'MentorPhone' => $MentorPhone,
            'MentorEmail' => $MentorEmail
        );
        $result = $this->db->insert('mentorcontactinfo', $data);
        return $this->db->insert_id();
    }

    function addRole($addMentor, $roleId) {
        $data = array(
            'MentorID' => $addMentor,
            'userRoleID' => $roleId
        );
        $result = $this->db->insert('usertorole', $data);
        return $this->db->insert_id();
    }

    function addSource($addMentor, $sourceId) {
        $data = array(
            'MentorID' => $addMentor,
            'MentorSourceID' => $sourceId
        );
        $result = $this->db->insert('usertosource', $data);
        return $this->db->insert_id();
    }

    /* mentee */

    function checkRoleExistsMentee($RoleCheck) {
        $query = $this->db->query("SELECT * FROM mentor_menteeprogrammes WHERE programmeName  = '" . $RoleCheck . "'");

        //echo $this->db->last_query();exit;
        return $query->result();
    }

    function checkSourceExistsMentee($SourceCheck) {
        $query = $this->db->query("SELECT * FROM mentor_menteesourcenames WHERE MenteeSourceName = '" . $SourceCheck . "'");

        //echo $this->db->last_query()."<br>";
        return $query->result();
    }

    function addMentee($mentorName) {
        $data = array(
            'MenteeName' => $mentorName
        );
        $result = $this->db->insert('mentee', $data);
        //echo $this->db->last_query();exit;
        return $this->db->insert_id();
    }

    function addMenteeContact($addMentor, $MentorPhone, $MentorEmail) {
        $data = array(
            'MenteeId' => $addMentor,
            'MenteePhone' => $MentorPhone,
            'MenteeEmail' => $MentorEmail
        );
        $result = $this->db->insert('menteecontact', $data);
        return $this->db->insert_id();
    }

    function addMenteeRole($addMentor, $roleId) {
        $data = array(
            'MenteeID' => $addMentor,
            'programmeID' => $roleId
        );
        $result = $this->db->insert('menteertoprogramme', $data);
        return $this->db->insert_id();
    }

    function addMenteeSource($addMentor, $sourceId) {
        $data = array(
            'MenteeID' => $addMentor,
            'MenteeSourceID' => $sourceId
        );
        $result = $this->db->insert('clienttosource', $data);
        return $this->db->insert_id();
    }

    /* subtopic */

    function addSubTopic($subTopicName, $topicId) {
        $data = array(
            'TopicID' => $topicId,
            'SubTopicDescription' => $subTopicName
        );
        $result = $this->db->insert('mentor_subtopic', $data);
        return $this->db->insert_id();
    }

    function checkTopicExists($topic) {
        $query = $this->db->query("SELECT * FROM mentor_topic WHERE TopicDescription  = '" . $topic . "'");

        //echo $this->db->last_query();exit;
        return $query->result();
    }

}
