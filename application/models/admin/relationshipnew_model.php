<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Relationshipnew_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_mentors() {
        $sql = $this->db->select('m.*,mci.*')
                ->from('mentor m')
                ->join('mentorcontactinfo mci', 'mci.MentorID = m.MentorID', 'left')
                ->where('m.Status', 1)
                ->get();
        // return $result = $sql->row_array();
        return $sql->result();
    }

    public function get_mentees() {
        $sql = $this->db->select('m.*,mci.*')
                ->from('mentee m')
                ->join('menteecontact mci', 'mci.MenteeId = m.MenteeId', 'left')
                ->where('m.Status', 1)
                ->get();
        // $result = $sql->row_array();
        return $sql->result();
    }

    public function addRelation($mentor, $mentee) {
        $data = array(
            'MentorID' => $mentor,
            'MenteeID' => $mentee
        );
        $this->db->insert('relationship', $data);
        return $this->db->insert_id();
    }

    public function checkRelation($mentor, $mentee) {
        $sql = $this->db->select('*')
                ->from('relationship')
                ->where('MentorID', $mentor)
                ->where('MenteeID', $mentee)
                ->get();
        //echo $this->db->last_query();exit;	
        return $sql->num_rows();
    }

    public function get_skills() {
        $sql = $this->db->select('s.*')
                ->from('skill s')
                ->where('s.Status', 1)
                ->get();
        return $sql->result();
    }

    public function get_skills_mentee() {
        $sql = $this->db->select('s.*')
                ->from('menteeskill s')
                ->get();
        return $sql->result();
    }

    public function getMentorBySkill($skillID) {
        if ($skillID == 0) {
            $sql = $this->db->select('m.*,mci.*')
                    ->from('mentor m')
                    ->join('mentorcontactinfo mci', 'mci.MentorID = m.MentorID', 'left')
                    ->where('m.Status', 1)
                    ->get();
            //echo $this->db->last_query();exit;
        } else {
            $sql = $this->db->select('s.*,ms.*,mci.*,m.*')
                    ->from('skill s')
                    ->join('mentorskillset ms', 'ms.SkillID = s.SkillID', 'left')
                    ->join('mentorcontactinfo mci', 'mci.MentorID = ms.MentorID', 'left')
                    ->join('mentor m', 'mci.MentorID = m.MentorID', 'left')
                    ->where('s.SkillID', $skillID)
                    ->where('ms.Status', 1)
                    ->where('s.status', 1)
                    ->where('mci.status', 1)
                    ->get();
            /* 08-10-2015 */
            // $result = $sql->row_array(); 
            //echo $this->db->last_query();exit;		
        }
        return $sql->result();
    }

    public function getMenteeBySkill($skillID) {
        if ($skillID == 0) {
            $sql = $this->db->select('m.*,mci.*')
                    ->from('mentee m')
                    ->join('menteecontact mci', 'mci.MenteeId = m.MenteeId', 'left')
                    ->where('m.Status', 1)
                    ->get();
            //echo $this->db->last_query();exit;
        } else {
            $sql = $this->db->select('s.*,ms.*,mci.*,m.*')
                    ->from('skill s')
                    ->join('menteeskill ms', 'ms.SkillID = s.SkillID', 'left')
                    ->join('menteecontact mci', 'mci.MenteeID = ms.MenteeID', 'left')
                    ->join('mentee m', 'mci.MenteeID = m.MenteeID', 'left')
                    ->where('s.SkillID', $skillID)
                    ->where('ms.Status', 1)
                    ->get();
            // $result = $sql->row_array();   
        }
        return $sql->result();
    }

    public function get_mentors_skill($mentorID) {
        $sql = $this->db->select('m.*,s.*')
                ->from('mentorskillset m')
                ->join('skill s', 's.SkillID = m.SkillID', 'left')
                ->where('m.MentorID', $mentorID)
                ->where('m.Status', 1)
                ->get();
        //echo $this->db->last_query();exit;	
        return $sql->result();
    }

    public function get_mentees_skill($menteeID) {
        $sql = $this->db->select('m.*,s.*')
                ->from('menteeskill m')
                ->join('skill s', 's.SkillID = m.SkillID', 'left')
                ->where('m.MenteeID', $menteeID)
                ->where('m.Status', 1)
                ->get();
        //echo $this->db->last_query();exit;	
        return $sql->result();
    }

    public function getAssingedMentee($mentorID) {
        $sql = $this->db->select('m.MentorID, m.MentorName,r.*,mentee.*')
                ->from('mentor m')
                ->join('relationship r', 'r.MentorID = m.MentorID', 'left')
                ->join('mentee mentee', 'mentee.MenteeID = r.MenteeID', 'left')
                ->where('m.MentorID', $mentorID)
                ->get();
        //echo $this->db->last_query();exit;	
        return $sql->result();
    }

    public function getAssingedMentor($menteeID) {
        $sql = $this->db->select('m.MenteeID, m.MenteeName,r.*,mentor.*')
                ->from('mentee m')
                ->join('relationship r', 'r.MenteeID = m.MenteeID', 'left')
                ->join('mentor mentor', 'mentor.MentorID = r.MentorID', 'left')
                ->where('m.MenteeID', $menteeID)
                ->get();
        //echo $this->db->last_query();exit;	
        return $sql->result();
    }

    public function checkAssignedRelation($mentor, $mentee) {
        $this->db->select('*');
        $this->db->from('relationship');
        $this->db->where('MenteeID', $mentee);
        $this->db->where('MentorID', $mentor);
        $query = $this->db->count_all_results();
        return $query;
    }

    public function removeRelation($mentor, $mentee) {
        $this->db->where('MentorID', $mentor);
        $this->db->where('MenteeID', $mentee);
        $this->db->delete('relationship');
        return $this->db->affected_rows();
    }

    public function getMentorName($mentor) {
        $sql = $this->db->select('*')
                ->from('mentor')
                ->where('MentorID', $mentor)
                ->get();
        return $sql->result();
    }

    public function getMenteeName($mentee) {
        $sql = $this->db->select('*')
                ->from('mentee')
                ->where('MenteeID', $mentee)
                ->get();
        return $sql->result();
    }

    public function getAllMentor() {
        $sql = $this->db->select('*')
                ->from('mentor')
                ->where('Status', 1)
                ->get();
        return $sql->result();
    }

    public function totalMeetings() {
        $this->db->select('m.MentorName AS MentorName, me.MenteeName AS MenteeName, mi.MeetingStartDatetime AS MeetingStartDatetime, mi.MeetingEndDatetime AS MeetingEndDatetime, mi.MeetingElapsedTime AS MeetingElapsedTime, mi.MeetingFeedback, mc.MentorComment AS MentorComment, mc.SatisfactionIndex AS SatisfactionIndex', FALSE)
                ->from('meetinginfo mi')
                ->join('mentor m', 'm.MentorID = mi.MentorID')
                ->join('mentee me', 'me.MenteeID = mi.MenteeID')
                ->join('mentorcomment mc', 'mc.MeetingID = mi.MeetingID', 'left');
        //$query = $this->db->query('SELECT * FROM mentor_meetinginfo where status=1');
        return $this->db->count_all_results();
    }

    public function totalMentors() {
        $query = $this->db->query('SELECT * FROM mentor_mentor where status=1');
        return $query->num_rows();
    }

    public function totalMentees() {
        $query = $this->db->query('SELECT * FROM mentor_mentee where status=1');
        return $query->num_rows();
    }

    public function avgMeetingLength() {
        $query = $this->db->query('SELECT * FROM mentor_meetinginfo where status=1');
        $count = count($query->result());
        $sum = 0;
        foreach ($query->result() as $val) {

            $time = $val->MeetingElapsedTime;
            $sum = $sum + $time;
            //echo "<pre>";print_r($val);
        }
        if ($count == 0) {
            return $avg = 0;
        } else
            return $avg = ceil($sum / $count);
    }

}
