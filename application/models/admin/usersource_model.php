<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Usersource_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_source($id) {
        $query = $this->db->get_where('mentorsourcenames', array('MentorSourceID' => $id));
        return $query->row_array();
    }

    public function delete_source($id) {
        $data = array('Status' => '0');
        $this->db->where('MentorSourceID', $id);
        $result = $this->db->update('mentorsourcenames', $data);
    }

    public function checkUserSource($MentorSourceName) {
        try {
            $this->db->where('MentorSourceName', $MentorSourceName);
            $this->db->where('Status', 1);
            $this->db->from('mentorsourcenames');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function checkUserSourceEdit($MentorSourceName, $MentorSourceID) {
        try {
            $this->db->where('MentorSourceName', $MentorSourceName);
            $this->db->where('MentorSourceID !=', $MentorSourceID);
            $this->db->where('Status', 1);
            $this->db->from('mentorsourcenames');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

}
