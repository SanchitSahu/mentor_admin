<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Userprogramme_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_programme($id) {
        $query = $this->db->get_where('menteeprogrammes', array('ProgrammeID' => $id));
        return $query->row_array();
    }

    public function delete_programme($id) {
        $data = array('Status' => '0');
        $this->db->where('ProgrammeID', $id);
        $result = $this->db->update('menteeprogrammes', $data);
    }

    public function checkProgramme($ProgrammeName) {
        try {
            $this->db->where('ProgrammeName', $ProgrammeName);
            $this->db->where('Status', 1);
            $this->db->from('menteeprogrammes');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function checkProgrammeEdit($ProgrammeName, $ProgrammeID) {
        try {
            $this->db->where('ProgrammeName', $ProgrammeName);
            $this->db->where('ProgrammeID !=', $ProgrammeID);
            $this->db->where('Status', 1);
            $this->db->from('menteeprogrammes');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

}
