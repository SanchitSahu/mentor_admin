<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class State_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * This function is use for state listing
     * @return type
     */
    function stateList() {
        try {
            $this->db->select('s.iStateId,s.vStateName,s.iCountryId,c.iCountryId,c.vCountryName');
            $this->db->from('state s');
            $this->db->join('country c', 'c.iCountryId=s.iStateId');
            $this->db->order_by("vStateName", "asc");
            $query = $this->db->get();
            $resultArr = $query->result_array();
            if (!empty($resultArr)) {
                return $resultArr;
            }
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    /**
     * This function is use for add state
     * @param type $country
     */
    function addstate($state) {
        try {
            $this->db->insert('state', $state);
            return $this->db->insert_id();
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    /**
     * This function is use for getting detail for state
     * @param type $stateId
     * @return type
     */
    function get_state_details($stateId) {
        try {
            $this->db->select('');
            $this->db->from('state s');
            $this->db->where('iStateId', $stateId);
            $query = $this->db->get();
            return $query->row_array();
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    /**
     * This function is use for update state
     * @param type $country
     */
    function editstate($stateArr) {
        try {
            $this->db->update('state', $stateArr, array('iStateId' => $stateArr['iStateId']));
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }


    public function get_city_list($limit = 0, $offset = 10, $order_by = "iStateId", $sort_order = "asc", $search_data, $SearchType,$where = array()) {

        $data = $this->trim_serach_data($search_data, $SearchType);
        
        $this->db->select('s.*, con.vCountryName');        
        $this->db->from(STATE . ' as s');
        $this->db->join(COUNTRY . ' as con', 'con.iCountryId = s.iCountryId', 'left');
       
        if($SearchType == 'ORLIKE'){
            $likeStr = or_like_search($data);
        }
        if($SearchType == 'ANDLIKE'){
            $likeStr = and_like_search($data);
        }
        
//        $likeStr = or_like_search($data);
        if ($likeStr) {
            $this->db->where($likeStr, NULL, FALSE);
        }
        
        if(isset($where) && !empty($where)){
            $this->db->where($where);
        }

        //$this->db->where('p.Status <> 2');
        $this->db->order_by($order_by, $sort_order);
        $this->db->limit($limit, $offset);
        $query = $this->db->get();
      // echo $this->db->last_query();
        $dataResult = array();
        if ($query->num_rows() > 0) {
            $dataResult['result'] = $query->result_array();
            $dataResult['totalRecord'] = $this->get_state_count($search_data, $SearchType,$where);

            return $dataResult;
        }
        return false;
    }

    public function get_state_count($search_data, $SearchType, $where = array()) {

        $data = $this->trim_serach_data($search_data, $SearchType);

        $this->db->from(STATE . ' as s');
        $this->db->join(COUNTRY . ' as con', 'con.iCountryId = s.iCountryId', 'left');
        
        if($SearchType == 'ORLIKE'){
            $likeStr = or_like_search($data);
        }
        if($SearchType == 'ANDLIKE'){
            $likeStr = and_like_search($data);
        }
        
        //$likeStr = or_like_search($data);
        if(isset($where) && !empty($where)){
            $this->db->where($where);
        }
        if ($likeStr) {
            $this->db->where($likeStr, NULL, FALSE);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function trim_serach_data($search_data, $SearchType) {
        $QueryStr = array();
        if (!empty($search_data)) {
            if ($SearchType == 'ANDLIKE') {  // For Advance Search               
                $i = 0;
                foreach ($search_data as $key => $val){
                    $val = addslashes($val);
                    if($key == 'StateName' && !empty($val)){
                        $QueryStr[$i]['Field'] = 's.vStateName';
                        $QueryStr[$i]['Value'] = $val;
                        $QueryStr[$i]['Operator'] = 'LIKE';
                    }
                    if($key == 'CountryName' && !empty($val)){
                        $QueryStr[$i]['Field'] = 'con.vCountryName';
                        $QueryStr[$i]['Value'] = $val;
                        $QueryStr[$i]['Operator'] = 'LIKE';
                    }
                    if($key == 'Status' && !empty($val)){
                        $QueryStr[$i]['Field'] = 's.eStatus';
                        $QueryStr[$i]['Value'] = $val;
                        $QueryStr[$i]['Operator'] = '=';
                    }
                    $i++;
                }
            } else {
                !empty($search_data['StateName']) ? $QueryStr['s.vStateName'] = $search_data['StateName'] : "";
                !empty($search_data['CountryName']) ? $QueryStr['con.vCountryName'] = $search_data['CountryName'] : "";
                !empty($search_data['Status']) ? $QueryStr['s.eStatus'] = $search_data['Status'] : "";
            }
        }
        return $QueryStr;
    }
    
    public function get_states($countryId) {
        $this->db->order_by('vStateName ASC');
        $this->db->where('iCountryId', $countryId);
        $query = $this->db->get('state');        
        return $query->result_array();
    }    

}
