<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Groups_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function addMentorToGroup($GroupId, $UserId) {
        $data = array(
            'UserID' => $UserId,
            'GroupID' => $GroupId,
            'UserType' => 0,
            'CreatedDate' => date('Y-m-d H:i:s')
        );
        if ($this->db->insert('groupusers', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function addMenteeToGroup($GroupId, $UserId) {
        $data = array(
            'UserID' => $UserId,
            'GroupID' => $GroupId,
            'UserType' => 1,
            'CreatedDate' => date('Y-m-d H:i:s')
        );
        if ($this->db->insert('groupusers', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function getGroupDetails($id, $isGetUsers = '') {
        if ($id != '') {
            $this->db->where('GroupID', $id);
        }
        if ($isGetUsers == '') {
            $query = $this->db->get('groups');
        } else {
            $this->db->select('GroupID, UserID, UserType');
            $query = $this->db->get('groupusers');
        }
        if ($query->num_rows() == 0) {
            return array();
        } else {
            return $query->result();
        }
    }

    public function getMentorMenteeDetails($user) {
        if ($user->UserType == 1) {
            $this->db->select('MentorName');
            $this->db->where('MentorID', $user->UserID);
            $query = $this->db->get('mentor');
            if ($query->num_rows() > 0) {
                $result = $query->result();
                return $result[0]->MentorName;
            }
        } else if ($user->UserType == 0) {
            $this->db->select('MenteeName');
            $this->db->where('MenteeID', $user->UserID);
            $query = $this->db->get('mentee');
            if ($query->num_rows() > 0) {
                $result = $query->result();
                return $result[0]->MenteeName;
            }
        }
        return '';
    }

    public function getAllMentors() {
        $this->db->select('MentorID, MentorName');
        $this->db->where('Status !=', 0);
        $query = $this->db->get('mentor');

        $resultArr = $query->result_array();
        if (!empty($resultArr)) {
            foreach ($resultArr as $value) {
                $mentors[$value['MentorID']] = $value['MentorName'];
            }
            return $mentors;
        } else {
            return array("" => "");
        }
    }

    public function getAllMentees() {
        $this->db->select('MenteeID, MenteeName');
        $this->db->where('Status !=', 0);
        $query = $this->db->get('mentee');
        $resultArr = $query->result_array();

        if (!empty($resultArr)) {
            foreach ($resultArr as $value) {
                $mentees[$value['MenteeID']] = $value['MenteeName'];
            }
            return $mentees;
        } else {
            return array("" => "");
        }
    }

    public function delete_group($id) {
        $this->db->where('GroupId', $id);
        $this->db->delete('groups', $data);
        $this->db->where('GroupId', $id);
        $this->db->delete('groupusers', $data);
    }

}
