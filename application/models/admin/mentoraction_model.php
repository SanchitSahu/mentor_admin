<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MentorAction_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_mentordefinedactions($id) {
        $query = $this->db->get_where('mentordefinedactions', array('MentorActionID' => $id));
        return $query->row_array();
    }

    public function delete_mentordefinedactions($id) {

        $data = array('Status' => '0');
        $this->db->where('MentorActionID', $id);
        $result = $this->db->update('mentordefinedactions', $data);

        $data = array('Status' => '0');
        $this->db->where('MentorActionID', $id);
        $result = $this->db->update('mentoractionstaken', $data);
    }

    public function checkMentorAction($MentorActionName) {
        try {
            $this->db->where('MentorActionName', $MentorActionName);
            $this->db->where('Status', 1);
            $this->db->from('mentordefinedactions');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function checkMentorActionEdit($MentorActionName, $MentorActionID) {
        try {
            $this->db->where('MentorActionName', $MentorActionName);
            $this->db->where('MentorActionID !=', $MentorActionID);
            $this->db->where('Status', 1);
            $this->db->from('mentordefinedactions');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

}
