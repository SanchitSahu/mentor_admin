<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_model extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    /*
     * Check login process and create session
     * @return void
     */

    public function login() {
 
        $this->db->where('Email', $this->input->post('Email'));
        $this->db->where('Password', fnEncrypt($this->input->post('Password'), $this->config->item('mentorKey')));
        $this->db->where('Status', '1');
        $this->db->limit(1);
        $query = $this->db->get('admindetails');
		//echo $this->db->last_query();exit;

        if ($query->num_rows == 1) {
            $this->get_db_session_data($query->row());
            return true;
        }
        return false;
    }
	
	function checkDomain($subdomain){
		$this->db->where('subdomain_name', $subdomain);
		$this->db->where('Status', '1');
		$this->db->limit(1);
		$query = $this->db->get('subdomains');
		//echo $this->db->last_query();exit;

		if ($query->num_rows == 1) {
			return true; 
		}else
			return false;
	}
	
	
	public function subadminlogin($isCallFromApp = false) {
 
		$host = $_SERVER['HTTP_HOST'];
		$subdomain = strstr(str_replace("www.","",$host), ".",true);
        $this->db->where('Email', $this->input->post('Email'));
		if($isCallFromApp) {
			$this->db->where('Password', $_POST['Password']);
		} else {
			$this->db->where('Password', fnEncrypt($this->input->post('Password'), $this->config->item('mentorKey')));
		}
		$this->db->where('subdomain_name',strtolower($subdomain));
        //$this->db->where('Status', '1');
        $this->db->limit(1);
        $query = $this->db->get('subdomains');
		$getConfig = $this->db->get('settings');
		//echo $this->db->last_query();exit;
		$configRes = $getConfig->row();
		$config= $configRes->DepartmentName;

        if ($query->num_rows == 1) {
            $this->get_subadmin_db_session_data($query->row(),$config);
            return true;
        }
        return false;
    }

    public function vendorlogin() {

        $this->db->where('Email', $this->input->post('Email'));
        $this->db->where('Password', fnEncrypt($this->input->post('Password'), $this->config->item('mentorKey')));
        $this->db->limit(1);
        $query = $this->db->get('admindetails');

        if ($query->num_rows == 1) {
            $this->get_db_session_dataforvendor($query->row());
            return true;
        }
        return false;
    }

    /**
     * Serialize the session data stored in the database, 
     * store it in a new array and return it to the controller 
     * @return array
     */
    public function get_db_session_data($result) {
        /* put data in array using username as key */
		$this->db->select('');
        $this->db->from('admindetails');
        $this->db->where('AdminID', 1);
        $query = $this->db->get();
		$res	=	$query->result();
		
        $user['id'] = $result->AdminID;
        $user['name'] = $result->FirstName . ' ' . $result->LastName;
        $user['UserType'] = 'superadmin';
		$user['profilePicture'] =	$res[0]->PictureURL ;
		
        $this->session->set_userdata('admin_data', $user);
        return true;
    }
	
	public function get_subadmin_db_session_data($result,$config="") {
        /* put data in array using username as key */
//		$this->db->select('');
//        $this->db->from('subdomains');
//        $this->db->where('subdomain_id', $result['su']);
//        $query = $this->db->get();
//		$res	=	$query->result();
		//print_r($config);exit;
        $user['id'] = $result->subdomain_id;
        $user['name'] = $result->FirstName . ' ' . $result->LastName;
        $user['UserType'] = 'Admin';
		$user['profilePicture'] =	$result->PictureURL;
		$user['subdomain_name'] =	$result->subdomain_name;
        $this->session->set_userdata('admin_data', $user);
        return true;
    }

    public function get_db_session_dataforvendor($result) {
        /* put data in array using username as key */
        $user['id'] = $result->iVendorId;
        $user['name'] = $result->vFirstName . ' ' . $result->vLastName;
        $user['UserType'] = 'Vendor';
		$user['profilePicture'] =	$res[0]->PictureURL ;
        $this->session->set_userdata('admin_data', $user);
        return true;
    }

    function fnDecrypt($sValue, $sSecretKey) {
        return rtrim(
                mcrypt_decrypt(
                        MCRYPT_RIJNDAEL_256, $sSecretKey, base64_decode($sValue), MCRYPT_MODE_ECB, mcrypt_create_iv(
                                mcrypt_get_iv_size(
                                        MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB
                                ), MCRYPT_RAND
                        )
                ), "\0"
        );
    }


    function fnEncrypt($sValue, $sSecretKey) {
        return rtrim(
                base64_encode(
                        mcrypt_encrypt(
                                MCRYPT_RIJNDAEL_256, $sSecretKey, $sValue, MCRYPT_MODE_ECB, mcrypt_create_iv(
                                        mcrypt_get_iv_size(
                                                MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB
                                        ), MCRYPT_RAND)
                        )
                ), "\0"
        );
    }

}