<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Newusers_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

	function getUserDetails($id){
		
		$sql	=	$this->db->select('*')
        ->from('newregistrations')
		->where('RegistrationID', $id)
		->get();
		$result = $sql->result();
		
		//print_r($result);exit;
		
		//if($result[0]->UserType == 0 && $result[0]->DateUpdated	== "0000-00-00 00:00:00"){	
		if($result[0]->UserType == 0){
			$res	=	$this->db->select('m.MentorName as UserName,mci.MentorEmail as UserEmail',FALSE)
			->from('mentor m')
	    	->join('mentorcontactinfo mci', 'mci.MentorID = m.MentorID', 'left')
			->where('m.MentorID', $result[0]->UserID)
			->where('m.Status', 1)
			->get();
			return $res->result();
		//} else if ($result[0]->UserType == 1 && $result[0]->DateUpdated == "0000-00-00 00:00:00") {
		} else if ($result[0]->UserType == 1) {
			$res	=	$this->db->select('m.MenteeName as UserName, mci.MenteeEmail as UserEmail',FALSE)
			->from('mentee m')
	    	->join('menteecontact mci', 'mci.MenteeId = m.MenteeID', 'left')
			->where('m.MenteeID', $result[0]->UserID)
			->where('m.Status', 1)
			->get();
			return $res->result();
		}	
		return false;
	}
	
	function updatePassword($id,$AccessToken){
		$sql	=	$this->db->select('*')
        ->from('newregistrations')
		->where('RegistrationID', $id)
		->get();
		$result = $sql->result();
		
		$c_date = date('Y-m-d H:i:s');
		$data2['DateUpdated'] = $c_date;
		$this->db->where('RegistrationID', $id);			
		$this->db->update('newregistrations', $data2);
		//echo $this->db->last_query();exit;
		
		if($result[0]->UserType == 0){
			$data['Password'] = fnEncrypt($AccessToken, $this->config->item('mentorKey'));
			
			$this->db->where('MentorID', $result[0]->UserID);			
            $this->db->update('mentor_mentorcontactinfo', $data);
			return $this->db->affected_rows();
		}else{
			$data['Password'] = fnEncrypt($AccessToken, $this->config->item('mentorKey'));
			
			$this->db->where('MenteeID', $result[0]->UserID);						
            $this->db->update('mentor_menteecontact', $data);
			return $this->db->affected_rows();
		}
		return 0;
	}
}

