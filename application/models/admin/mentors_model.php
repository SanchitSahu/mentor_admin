<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mentors_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
	public function getMentorDetail($id){
		$query = $this->db->select('m.MentorID,m.MentorName, m.Status, m.Weight,mci.MentorPhone, mci.DisableSMS, mci.IsSubadmin, mci.MentorEmail, mci.MentorImage')
	    	->from('mentor m')
	    	->join('mentorcontactinfo mci', 'mci.MentorID = m.MentorID', 'left')
			->where('mci.MentorID',$id)
	    	->get();
        $result = $query->row_array();
        return $result;
	}

    public function get_mentors($id) {
		//m.*,mci.*,utr.userToRoleID,ur.*,us.userToSourceID,ms.*
		//$sql = $this->db->select('m.*,mci.*,ur.*,ms.*,utr.userToRoleID,us.userToSourceID')
		$sql = $this->db->select('m.MentorID,m.MentorName, m.Status, m.Weight,mci.MentorPhone, mci.DisableSMS, mci.IsSubadmin, mci.MentorEmail, mci.MentorImage,
				ur.UserRoleName,ur.userRoleID,ms.MentorSourceName,ms.MentorSourceID')
	    	->from('mentor m')
	    	->join('mentorcontactinfo mci', 'mci.MentorID = m.MentorID', 'left')
			->join('usertorole utr', 'utr.MentorID = mci.MentorID','left')
			->join('user_roles ur', 'utr.userRoleID = ur.userRoleID','left')
			->join('usertosource us', 'us.MentorID = mci.MentorID','left')
			->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID','left')	
	    	->where('mci.MentorID',$id)
	    	->get();
        $result = $sql->row_array();
        return $result;
    }
       
    
    public function delete_mentors($id) { 
       
        $data=array('Status'=>'0');
        $this->db->where('MentorID', $id);
        $result = $this->db->update('mentor', $data);
        
         $data=array('Status'=>'0');
        $this->db->where('MentorID', $id);
        $result = $this->db->update('mentorcontactinfo', $data);
        
         $data=array('Status'=>'0');
        $this->db->where('MentorID', $id);
        $result = $this->db->update('mentoractionstaken', $data);
        
         $data=array('Status'=>'0');
        $this->db->where('MentorID', $id);
        $result = $this->db->update('mentorcomment', $data);
        
         $data=array('Status'=>'0');
        $this->db->where('MentorID', $id);
        $result = $this->db->update('mentorskillset', $data);
             
    }
    
    
    //check email exist or not
    function checkuser_mail($email) {
        try {
            $this->db->select('MentorContactInfoID');
            $this->db->from('mentorcontactinfo');
            $this->db->where('MentorEmail', $email);
            $query = $this->db->count_all_results();
            if ($query == 0) {
            	$this->db->select('MenteeContactID');
            	$this->db->from('menteecontact');
            	$this->db->where('MenteeEmail', $email);
            	$query = $this->db->count_all_results();
            }
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }
	
	
	function checkMentorName($name) {
        try {
            $this->db->select('MentorID');
            $this->db->from('mentor');
            $this->db->where('MentorName', $name);
			$this->db->where('Status',1);
            $query = $this->db->count_all_results();
			if ($query == 0) {
            	$this->db->select('MenteeID');
            	$this->db->from('mentee');
            	$this->db->where('MenteeName', $name);
				$this->db->where('Status',1);
            	$query = $this->db->count_all_results();
            }
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }
    
    //check email exist or not
    function checkuser_mail_edit($id, $email) {
        try {
            $this->db->select('MentorContactInfoID');
            $this->db->from('mentorcontactinfo');
            $this->db->where('MentorEmail', $email);
            $this->db->where('MentorID !=', $id);
            $query = $this->db->count_all_results();
            if ($query == 0) {
            	$this->db->select('MenteeContactID');
				$this->db->from('menteecontact');
            	$this->db->where('MenteeEmail', $email);
            	$this->db->where('MenteeId !=', $id);
            	$query = $this->db->count_all_results();
            }
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }
	
	function checkMentorNameEdit($id, $name) {
		try {
            $this->db->select('MentorID');
            $this->db->from('mentor');
            $this->db->where('MentorName', $name);
            $this->db->where('MentorID !=', $id);
			$this->db->where('Status',1);
            $query = $this->db->count_all_results();
			if ($query == 0) {
            	$this->db->select('MenteeID');
            	$this->db->from('mentee');
            	$this->db->where('MenteeName', $name);
				$this->db->where('Status',1);
            	$query = $this->db->count_all_results();
            }
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
	}
	
	public function roleData(){
    	try {
            $this->db->select('userRoleID,UserRoleName');
            $this->db->from('user_roles');
			$this->db->where("Status", 1);
            $this->db->order_by("Weight", "asc");
			$this->db->order_by("UserRoleName", "asc");
			
            $query = $this->db->get();
            $resultArr = $query->result_array();
            $role = array("" => '--Select Role--');
            if (!empty($resultArr)) {
                foreach ($resultArr as $value) {
                    $role[$value['userRoleID']] =  $value['UserRoleName'];
                }
                return $role;
            }
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }
	
	public function sourceData(){
    	try {
            $this->db->select('MentorSourceID,MentorSourceName');
            $this->db->from('mentorsourcenames');
			$this->db->where("Status", 1);
            $this->db->order_by("Weight", "asc");
			$this->db->order_by("MentorSourceName", "asc");
            $query = $this->db->get();
            $resultArr = $query->result_array();
            $role = array("" => '--Select Source Name--');
            if (!empty($resultArr)) {
                foreach ($resultArr as $value) {
                    $role[$value['MentorSourceID']] =  $value['MentorSourceName'];
                }
                return $role;
            }
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }
	
	public function get_mentorsRole($mentorID)
	{
		$sql = $this->db->select('u.*')
	    	->from('usertorole u')
	    	->where('u.MentorID',$mentorID)
	    	->get();
        $result = $sql->row_array();
        return $result;
	}
	
	public function get_mentorsSource($mentorID)
	{
		$sql = $this->db->select('u.*')
	    	->from('usertosource u')
	    	->where('u.MentorID',$mentorID)
	    	->get();
        $result = $sql->row_array();
        return $result;
	}
	
	public function getDeparment(){
		$sql = $this->db->select('*')
	    	->from('settings')
	    	->get();
        $result = $sql->row_array();
        return $result;
	}
}