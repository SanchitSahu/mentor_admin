<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MenteeAction_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_menteedefinedactions($id) {
        $query = $this->db->get_where('menteedefinedactions', array('MenteeActionID' => $id));
        return $query->row_array();
    }

    public function delete_menteedefinedactions($id) {

        $data = array('Status' => '0');
        $this->db->where('MenteeActionID', $id);
        $result = $this->db->update('menteedefinedactions', $data);

        $data = array('Status' => '0');
        $this->db->where('MenteeActionID', $id);
        $result = $this->db->update('menteeactionstaken', $data);
    }

    public function checkMenteeAction($MenteeActionName) {
        try {
            $this->db->where('MenteeActionName', $MenteeActionName);
            $this->db->where('Status', 1);
            $this->db->from('menteedefinedactions');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function checkMenteeActionEdit($MenteeActionName, $MenteeActionID) {
        try {
            $this->db->where('MenteeActionName', $MenteeActionName);
            $this->db->where('MenteeActionID !=', $MenteeActionID);
            $this->db->where('Status', 1);
            $this->db->from('menteedefinedactions');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

}
