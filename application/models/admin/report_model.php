<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getSkillByMentorID($id) { 
    	$sql = $this->db->select('ms.*,s.*')
	    	->from('skill s')
	    	->join('mentorskillset ms', 's.SkillID = ms.SkillID', 'left')
	    	->where('ms.MentorID',$id)
	    	->get();
        $result = $sql->result();
		
		//echo "<pre>";print_r($result);exit;
        return $result;
    }
     
	public function getRole(){
		$sql = $this->db->select('*')
	    	->from('user_roles r')
			->where('Status',1)
			->order_by('Weight', 'ASC')
			->order_by('UserRoleName', 'ASC')
	    	->get();
        $result = $sql->result();
		
		//echo "<pre>";print_r($result);exit;
        return $result;
	}
	
	public function getProgramme(){
		$sql = $this->db->select('*')
	    	->from('mentor_menteeprogrammes mp')
			->where('Status',1)
			->order_by('Weight', 'ASC')
			->order_by('ProgrammeName', 'ASC')
	    	->get();
        $result = $sql->result();
		
		//echo "<pre>";print_r($result);exit;
        return $result;
	}
    
	public function getSource(){
		$sql = $this->db->select('*')
	    	->from('mentorsourcenames s')
			->where('Status',1)
			->order_by('Weight', 'ASC')
			->order_by('MentorSourceName', 'ASC')
	    	->get();
        $result = $sql->result();
		
		//echo "<pre>";print_r($result);exit;
        return $result;
	}
	
	public function getMentors(){
		$sql = $this->db->select('*')
	    	->from('mentor')
			->where('Status',1)
	    	->get();
        $result = $sql->result();
		
		//echo "<pre>";print_r($result);exit;
        return $result;
	}
	
	public function getMentees(){
		$sql = $this->db->select('*')
	    	->from('mentee')
			->where('Status',1)
	    	->get();
        $result = $sql->result();
		
		//echo "<pre>";print_r($result);exit;
        return $result;
	}
	
	public function getMenteeSource(){
		$sql = $this->db->select('*')
	    	->from('menteesourcenames s')
			->where('Status',1)
			->order_by('Weight', 'ASC')
			->order_by('MenteeSourceName', 'ASC')
	    	->get();
        $result = $sql->result();
		
		//echo "<pre>";print_r($result);exit;
        return $result;
	}
	
	
	function getSemester() {
		$query = $this->db->get("semester");
        //echo $this->db->last_query();exit;
        return $query->result();
	}
	
	
	function getSemesterDates($semester){
		$this->db->where('SemesterID',$semester);
		$query = $this->db->get("semester");
        //echo $this->db->last_query();exit;
        return $query->result();
	}
	
	function get_meetings() {
		$this->db->where('MenteeID >', 0);
        $query = $this->db->get("meetinginfo");
        return $query->result();
    }
	
	function getUserDetails($userId, $userType) {
        if ($userType == 'mentee') {
            $this->db->where('MenteeID', $userId);
            $result = $this->db->get('mentee');
            return $result->result();
        } else if ($userType == 'mentor') {
            $this->db->where('MentorID', $userId);
            $result = $this->db->get('mentor');
            return $result->result();
        }
    }
	
	function get_topic_details($topicId) {
        $this->db->where('TopicID', $topicId);
        $query = $this->db->get("topic");
        return $query->result();
    }

	function getSubmentorDetails($MentorID) {
        $sql = $this->db->select('m.*,mc.*')
	    	->from('mentor m')
	    	->join('mentorcontactinfo mc', 'm.MentorID = mc.MentorID', 'left')
	    	->where('mc.MentorID',$MentorID)
			->where('mc.IsSubmentor',1)
	    	->get();

		if($sql->num_rows() > 0) {
			return $sql->result();
		} else {
			return false;
		}
    }

	function getMentorName($MentorID){
		$sql = $this->db->select('m.MentorName')
	    	->from('mentor m')
	    	->where('m.MentorID',$MentorID)
	    	->get();


		if($sql->num_rows() > 0) {
			$result =  $sql->result();
			return $result[0]->MentorName;
		} else {
			return "";
		}
	}

	function getMenteeName($MenteeID){
		$sql = $this->db->select('m.MenteeName')
	    	->from('mentee m')
	    	->where('m.MenteeID',$MenteeID)
	    	->get();


		if($sql->num_rows() > 0) {
			$result =  $sql->result();
			return $result[0]->MenteeName;
		} else {
			return "";
		}
	}

	function getTeamName($TeamID){
		$sql = $this->db->select('t.TeamName')
	    	->from('team t')
	    	->where('t.TeamId',$TeamID)
			->where('t.TeamStatus','active')
	    	->get();


		if($sql->num_rows() > 0) {
			$result =  $sql->result();
			return $result[0]->TeamName;
		} else {
			return "";
		}
	}

}
