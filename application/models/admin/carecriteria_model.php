<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CareCriteria_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_careweighting($id) {
        $query = $this->db->get_where('careCriteria', array('CareCriteriaID' => $id));
        return $query->row_array();
    }

    public function delete_careweighting($id) {
        $data = array('Status' => '0');
        $this->db->where('CareCriteriaID', $id);
        $result = $this->db->update('careCriteria', $data);
    }

    public function checkCareCriteria($Name) {
        try {
            $this->db->where('Name', $Name);
            $this->db->where('Status', 1);
            $this->db->from('careCriteria');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function checkCareCriteriaEdit($Name, $CareCriteriaID) {
        try {
            $this->db->where('Name', $Name);
            $this->db->where('CareCriteriaID !=', $CareCriteriaID);
            $this->db->where('Status', 1);
            $this->db->from('careCriteria');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

}
