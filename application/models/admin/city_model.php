<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class City_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * This function is use for city listing
     * @return type
     */
    function cityList() {
        try {
            $this->db->select('ci.iCityId,ci.vCityName,ci.iCountryId,ci.iStateId,ci.eStatus,'
                    . 's.vStateName,c.vCountryName');
            $this->db->from('city ci');
            $this->db->join('state s', 's.iStateId=ci.iStateId','left');
            $this->db->join('country c', 'c.iCountryId=ci.iCountryId','left');
            $this->db->order_by("ci.vCityName", "asc");
            $query = $this->db->get();
            $resultArr = $query->result_array();
            if (!empty($resultArr)) {
                return $resultArr;
            }
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    /**
     * This function is use for add state
     * @param type $country
     */
    function addcity($city) {
        try {
            $this->db->insert('city', $city);
            return $this->db->insert_id();
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    /**
     * This function is use for getting detail for state
     * @param type $stateId
     * @return type
     */
    function get_city_details($cityId) {
        try {
            $this->db->select('');
            $this->db->from('city ci');
            $this->db->where('iCityId', $cityId);
            $query = $this->db->get();
            return $query->row_array();
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    /**
     * This function is use for update state
     * @param type $country
     */
    function editcity($cityArr) {
        try {
            $this->db->update('city', $cityArr, array('iCityId' => $cityArr['iCityId']));
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function get_city_list($limit = 0, $offset = 10, $order_by = "iCityId", $sort_order = "asc", $search_data, $SearchType,$where = array()) {

        $data = $this->trim_serach_data($search_data, $SearchType);
        
        $this->db->select('c.*,s.vStateName, con.vCountryName');
        $this->db->from(CITY . ' as c');
        $this->db->join(STATE . ' as s', 's.iStateId = c.iStateId', 'left');
        $this->db->join(COUNTRY . ' as con', 'con.iCountryId = c.iCountryId', 'left');
       
        if($SearchType == 'ORLIKE'){
            $likeStr = or_like_search($data);
        }
        if($SearchType == 'ANDLIKE'){
            $likeStr = and_like_search($data);
        }
        
//        $likeStr = or_like_search($data);
        if ($likeStr) {
            $this->db->where($likeStr, NULL, FALSE);
        }
        
        if(isset($where) && !empty($where)){
            $this->db->where($where);
        }

        //$this->db->where('p.Status <> 2');
        $this->db->order_by($order_by, $sort_order);
        $this->db->limit($limit, $offset);
        $query = $this->db->get();
      // echo $this->db->last_query();
        $dataResult = array();
        if ($query->num_rows() > 0) {
            $dataResult['result'] = $query->result_array();
            $dataResult['totalRecord'] = $this->get_city_count($search_data, $SearchType,$where);

            return $dataResult;
        }
        return false;
    }

    public function get_city_count($search_data, $SearchType, $where = array()) {

        $data = $this->trim_serach_data($search_data, $SearchType);

        $this->db->from(CITY . ' as c');
        $this->db->join(STATE . ' as s', 's.iStateId = c.iStateId', 'left');
        $this->db->join(COUNTRY . ' as con', 'con.iCountryId = c.iCountryId', 'left');
        
        if($SearchType == 'ORLIKE'){
            $likeStr = or_like_search($data);
        }
        if($SearchType == 'ANDLIKE'){
            $likeStr = and_like_search($data);
        }
        
        //$likeStr = or_like_search($data);
        if(isset($where) && !empty($where)){
            $this->db->where($where);
        }
        if ($likeStr) {
            $this->db->where($likeStr, NULL, FALSE);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function trim_serach_data($search_data, $SearchType) {
        $QueryStr = array();
        if (!empty($search_data)) {
            if ($SearchType == 'ANDLIKE') {  // For Advance Search               
                $i = 0;
                foreach ($search_data as $key => $val){
                    $val = addslashes($val);
                    if($key == 'CityName' && !empty($val)){
                        $QueryStr[$i]['Field'] = 'c.vCityName';
                        $QueryStr[$i]['Value'] = $val;
                        $QueryStr[$i]['Operator'] = 'LIKE';
                    }
                    if($key == 'StateName' && !empty($val)){
                        $QueryStr[$i]['Field'] = 's.vStateName';
                        $QueryStr[$i]['Value'] = $val;
                        $QueryStr[$i]['Operator'] = 'LIKE';
                    }
                    if($key == 'CountryName' && !empty($val)){
                        $QueryStr[$i]['Field'] = 'con.vCountryName';
                        $QueryStr[$i]['Value'] = $val;
                        $QueryStr[$i]['Operator'] = 'LIKE';
                    }
                    if($key == 'Status' && !empty($val)){
                        $QueryStr[$i]['Field'] = 'c.eStatus';
                        $QueryStr[$i]['Value'] = $val;
                        $QueryStr[$i]['Operator'] = '=';
                    }
                    $i++;
                }
            } else {
                !empty($search_data['CityName']) ? $QueryStr['c.vCityName'] = $search_data['CityName'] : "";
                !empty($search_data['StateName']) ? $QueryStr['s.vStateName'] = $search_data['StateName'] : "";
                !empty($search_data['CountryName']) ? $QueryStr['con.vCountryName'] = $search_data['CountryName'] : "";
                !empty($search_data['Status']) ? $QueryStr['c.eStatus'] = $search_data['Status'] : "";
            }
        }
        return $QueryStr;
    }

}
