<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getConfig() {
        $query = $this->db->get("settings");
        //echo $this->db->last_query();exit;
        return $query->result();
    }

    public function updateConfig($data2, $target_file,$icon_target_file) {
		
		//echo "<pre>";print_r($data2);exit;
		
		
        $data = array(
            'mentorName' => ucfirst($data2['mentorName']),
            'menteeName' => ucfirst($data2['menteeName']),
            'headerColor' => "#" . $data2['headerColor'],
            'leftBarColor' => "#" . $data2['leftBarColor'],
            'logo' => $target_file,
            'icon' => $icon_target_file,
            'buttonColor' => "#" . $data2['buttonColor'],
            'fontColor' => "#" . $data2['fontColor'],
            'backgroundColor' => "#" . $data2['backgroundColor'],
            'highlightColor' => "#" . $data2['highlightColor'],
            'leftMenuFont' => "#" . $data2['leftMenuFont'],
            'tableFont' => "#" . $data2['tableFont'],
            'tableBackground' => "#" . $data2['tableBackground'],
			'expiryTime' => $data2['expiryTime'],
			'missedMeeting' => $data2['missedMeeting'],
			'fiscalYear' => $data2['fiscalYear'],
			'noOfPeriod' => $data2['noOfPeriod'],
			'DepartmentName' => $data2['DepartmentName'],
			'maxMember' => $data2['maxMember'],
			'teamManager' => $data2['teamManager'],
			'Impromptu' => $data2['Impromptu']
        );
        
		$session = $this->session->userdata('admin_data');
		//print_r($session);exit;
		$user['id'] = $session['id'];
		$user['name'] = $session['name'];
		$user['UserType'] = 'Admin';
		$user['profilePicture'] = $session['profilePicture'];
	    $user['subdomain_name'] = $session['subdomain_name'];
		$this->session->set_userdata('admin_data', $user);
	
		$this->db->query('TRUNCATE TABLE mentor_semester');
		//echo $this->db->last_query();exit;
		$cnt = 0;
		//print_r($data2['semesterName']);
		foreach($data2['semesterName'] as $key => $value) {
			if (empty($value)){ $cnt++;}
			else{
				$data3	=	array(
							'SemesterName' => $data2['semesterName'][$cnt],
							'SemesterStartDate' => $data2['startDate'][$cnt],
							'SemesterEndDate' => $data2['endDate'][$cnt],
						);
				$this->db->insert('semester', $data3);
				$cnt++;
			}
		}

		//echo $cnt;exit;
		/*$count	=	$cnt;//count($data2['semesterName']);
		for($i=0;$i<$count;$i++){
			$data3	=	array(
							'SemesterName' => $data2['semesterName'][$i],
							'SemesterStartDate' => $data2['startDate'][$i],
							'SemesterEndDate' => $data2['endDate'][$i],
						);
			$this->db->insert('semester', $data3);
			
		}*/
		
        if($this->db->update('settings', $data)){
            return true;
        } else {
            return false;
        }
    }

	function getInvitation(){
		$query = $this->db->query("
				SELECT i.*,mentor.*,mentee.*,mmentor.MentorName,mmantee.MenteeName,t.TopicDescription
				FROM mentor_invitation i 
				LEFT JOIN mentor_menteecontact mentee ON i.MenteeID = mentee.MenteeId
				LEFT JOIN mentor_mentorcontactinfo mentor ON mentor.MentorID = i.MentorID
				LEFT JOIN mentor_mentee mmantee ON mmantee.MenteeID = i.MenteeID
				LEFT JOIN mentor_mentor mmentor ON mmentor.MentorID = i.MentorID
				LEFT JOIN mentor_topic t ON t.TopicID = i.TopicID
			");
        //echo $this->db->last_query();exit;
        return $query->result();
	}
	
	public function errorLog() {
		$this->db->where("Status",1);
        $query = $this->db->get("errors");
        
        return $query->num_rows();
    }
	
	function getSemester() {
		$this->db->order_by('SemesterStartDate', 'ASC');
		$query = $this->db->get("semester");
        //echo $this->db->last_query();exit;
        return $query->result();
	}
}
