<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vendors_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * This function is use for city listing
     * @return type
     */
    function vendorsList() {
        try {
            $this->db->select('vd.*,c.*,s.*');
            $this->db->from('vendordetails vd');
            $this->db->join('country c', 'c.iCountryId=vd.iCountryId', 'left');
            $this->db->join('state s', 's.iStateId=vd.iStateId', 'left');
            $this->db->order_by("iVendorId", "desc");
            $query = $this->db->get();
            $resultArr = $query->result_array();
            if (!empty($resultArr)) {
                return $resultArr;
            }
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    function setVendors($id = 0) {
        $data = array(
            'vFirstName' => $this->input->post('firstname'),
            'vLastName' => $this->input->post('lastname'),
            'vEmail' => $this->input->post('email'),
            'eGender' => $this->input->post('gender'),
            'dBirthDate' => $this->input->post('birthdate'),
            'dAnniversaryDate' => $this->input->post('anniversarydate'),
            'vCellPhone' => $this->input->post('mobile'),
            'vPhoneNumber1' => $this->input->post('phone'),
            'vAddress1' => $this->input->post('address1'),
            'vAddress2' => $this->input->post('address2'),
            'iCountryId' => $this->input->post('country'),
            'iStateId' => $this->input->post('state'),
            'vCity' => $this->input->post('city'),
            'vPostCode' => $this->input->post('postcode'),
        );

        if ($id == 0) {
            $result = $this->db->insert('vendordetails', $data);
            $id = $this->db->insert_id();
        } else {
            $this->db->where('iVendorId', $id);
            $result = $this->db->update('vendordetails', $data);
        }
        if ($result) {
            if ($_FILES['photo']['error'] == 0) {
                $pathMain = VENDOR_LOGO_PATH;
                $pathThumb = VENDOR_LOGO_THUMB_PATH;
                $imageNameTime = time();
                // $imageNamePrefix = "tmp_";
                //$orgFileName =  $imageNamePrefix.$imageNameTime; 
                $filename = $imageNameTime . "_" . $id;
                $result = do_upload("photo", $pathMain, $filename);
                if ($result['status'] == 1) {
                    $uploadedFileName = $result['upload_data']['file_name'];
                    $res = resize_image($pathMain . $uploadedFileName, $pathThumb . $uploadedFileName, VENDOR_LOGO_THUMB_WIDTH_ADMIN, VENDOR_LOGO_THUMB_HEIGHT_ADMIN);
                    $this->db->where('iVendorId', $id);
                    $this->db->update('vendordetails', array('iPictureURLId' => $pathThumb . $uploadedFileName));
                }
                @unlink($pathMain . $uploadedFileName);
            }
        }
    }

    /**
     * This function is use for add state
     * @param type $country
     */
    function addVendors($vendors) {
        try {
            $this->db->insert('vendordetails', $vendors);
            return $this->db->insert_id();
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    /**
     * This function is use for getting detail for state
     * @param type $stateId
     * @return type
     */
    function get_vender_details($vendorId) {
        try {
            $this->db->select('vd.*,c.*,s.*');
            $this->db->from('vendordetails vd');
            $this->db->join('country c', 'c.iCountryId=vd.iCountryId', 'left');
            $this->db->join('state s', 's.iStateId=vd.iStateId', 'left');
            $this->db->where('iVendorId', $vendorId);
            $query = $this->db->get();
            return $query->row_array();
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    /**
     * This function is use for update state
     * @param type $country
     */
    function editVendors($vendors) {
        try {
            $this->db->update('vendordetails', $vendors, array('iVendorId' => $vendors['iVendorId']));
            return $vendors['iVendorId'];
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    function getActiveVender($isforDropdown = false) {
        try {
        $userSession = $this->session->userdata('admin_data');
        if ($userSession['UserType'] == "Admin") { # FOR ADMIN LOGIN
            $this->db->select('iVendorId,vFirstName,vLastName,vFirmName,eStatus');
            $this->db->from('vendordetails');
            $this->db->where('eStatus', 1);
            $this->db->order_by("iVendorId", "desc");
            $query = $this->db->get();
        } else { # FOR VENDOR LOGIN
            $this->db->select('iVendorId,vFirstName,vLastName,vFirmName,eStatus');
            $this->db->from('vendordetails');
            $this->db->where('eStatus', 1);
            $this->db->where('iVendorId', $userSession['id']);
            $this->db->order_by("iVendorId", "desc");
            $query = $this->db->get();
        }
            $resultArr = $query->result_array();
            $vendors = array();
            if (!empty($resultArr)) {
                if ($isforDropdown == true) {
                    $vendors = array("" => 'Select Vendor');
                    foreach ($resultArr as $value) {
                        $vendors[$value['iVendorId']] = $value['vFirmName'];
                    }
                } else {
                    $vendors = $resultArr;
                }
            }
            return $vendors;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }
    
    function getActiveVenderForGallery($isforDropdown = false, $venderId) {
            $this->db->select('iVendorId,vFirstName,vLastName,vFirmName,eStatus');
            $this->db->from('vendordetails');
            $this->db->where('eStatus', 1);
            $this->db->where('iVendorId', $venderId);
            $this->db->order_by("iVendorId", "desc");
            $query = $this->db->get();

            $resultArr = $query->result_array();
            $vendors = array();
            if (!empty($resultArr)) {
                if ($isforDropdown == true) {
                    $vendors = array("" => 'Select Vendor');
                    foreach ($resultArr as $value) {
                        $vendors[$value['iVendorId']] = $value['vFirmName'];
                    }
                } else {
                    $vendors = $resultArr;
                }
            }
            return $vendors;    
    }

    //check email exist or not
    function checkuser_mail($email) {
        try {
            $this->db->select('iVendorId');
            $this->db->from('vendordetails');
            $this->db->where('vEmail', $email);
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    //check email exist or not
    function checkuser_mail_edit($id, $email) {
        try {
            $this->db->select('iVendorId');
            $this->db->from('vendordetails');
            $this->db->where('vEmail', $email);
            $this->db->where('iVendorId !=', $id);
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    function checkuser_uname($uname) {
        try {
            $this->db->select('iVendorId');
            $this->db->from('vendordetails');
            $this->db->where('vUserName', $uname);
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    function checkuser_uname_edit($id, $uname) {
        try {
            $this->db->select('iVendorId');
            $this->db->from('vendordetails');
            $this->db->where('vUserName', $uname);
            $this->db->where('iVendorId !=', $id);
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    //check email exist or not
    function checkuser_mail_Personal($email) {
        try {
            $this->db->select('iVendorId');
            $this->db->from('vendordetails');
            $this->db->where('vPersonalEmail', $email);
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    //check email exist or not
    function checkuser_mail_Personal_edit($id, $email) {
        try {
            $this->db->select('iVendorId');
            $this->db->from('vendordetails');
            $this->db->where('vPersonalEmail', $email);
            $this->db->where('iVendorId !=', $id);
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }
    
    public function updatepwd($pwd, $id) {
        $data = array(
            'Password' => fnEncrypt($pwd, $this->config->item('mentorKey'))
        );
        $this->db->where('AdminID', $id);
        $result = $this->db->update('admindetails', $data);
        return true;
    }

    public function get_vendor($id) {
        $query = $this->db->get_where('admindetails', array('AdminID' => $id));
        return $query->row_array();
    }
    
    public function get_vendorByemail($email) {
        $query = $this->db->get_where('vendordetails', array('vEmail' => $email));
        return $query->row_array();
    }
}