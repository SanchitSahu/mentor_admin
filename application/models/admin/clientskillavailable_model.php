<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clientskillavailable_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_clientSkills($id) { 
        $this->db->select("mss.MentorSkillSetID,mss.SkillID,ss.SkillName, mm.MentorName",FALSE);
        $this->db->from('mentorskillset mss');
        $this->db->join('skill ss','ss.SkillID = mss.SkillID','left');
		$this->db->join('mentor mm','mm.MentorID = mss.MentorID','left');
		$this->db->where('mss.Status','1');
		$this->db->where('mss.MentorID',$id);
		$query = $this->db->get();
        return $query->result_array();
    }
    
    
    public function delete_client_skill($id) { 
           
        $data=array('Status'=>'0');
        $this->db->where('MentorID', $id);
        $result = $this->db->update('mentorskillset', $data);
      
    }

	public function get_Skills(){
		$query =  $this->db->get("skill");
		return $query->result_array();
	}

	public function get_Mentor(){
		$query =  $this->db->get("mentor");
		return $query->result_array();
	}
    

}