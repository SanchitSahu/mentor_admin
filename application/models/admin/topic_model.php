<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Topic_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_topic($id) {
        $query = $this->db->get_where('topic', array('TopicID' => $id));
        return $query->row_array();
    }

    public function delete_topic($id) {

        $data = array('Status' => '0');
        $this->db->where('TopicID', $id);
        $result = $this->db->update('topic', $data);

        $data = array('Status' => '0');
        $this->db->where('TopicID', $id);
        $result = $this->db->update('subtopic', $data);
    }

    public function checkTopic($TopicDescription) {
        try {
            $this->db->where('TopicDescription', $TopicDescription);
            $this->db->where('Status', 1);
            $this->db->from('topic');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function checkTopicEdit($TopicDescription, $TopicID) {
        try {
            $this->db->where('TopicDescription', $TopicDescription);
            $this->db->where('TopicID !=', $TopicID);
            $this->db->where('Status', 1);
            $this->db->from('topic');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function topicList() {
        try {
            $this->db->select('TopicID,TopicDescription');
            $this->db->from('topic');
            $this->db->order_by("TopicDescription", "asc");
            $query = $this->db->get();
            $resultArr = $query->result_array();
            $topic = array("" => 'Select Topic');
            if (!empty($resultArr)) {
                foreach ($resultArr as $value) {
                    $topic[$value['TopicID']] = $value['TopicDescription'];
                }
                return $topic;
            }
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

}
