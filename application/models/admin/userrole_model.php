<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Userrole_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_roles($id) {
        $query = $this->db->get_where('user_roles', array('userRoleID' => $id));
        return $query->row_array();
    }

    public function delete_roles($id) {
        $data = array('Status' => '0');
        $this->db->where('userRoleID', $id);
        $result = $this->db->update('user_roles', $data);
    }

    public function checkUserRole($UserRoleName) {
        try {
            $this->db->where('UserRoleName', $UserRoleName);
            $this->db->where('Status', 1);
            $this->db->from('user_roles');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function checkUserRoleEdit($UserRoleName, $userRoleID) {
        try {
            $this->db->where('UserRoleName', $UserRoleName);
            $this->db->where('userRoleID !=', $userRoleID);
            $this->db->where('Status', 1);
            $this->db->from('user_roles');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

}
