<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Errors_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /*public function get_skill($id) { 
        $query = $this->db->get_where('skill', array('SkillID' => $id));
        return $query->row_array();
    }*/
    
    
    public function delete_errors($id) {
        //$this->db->where('ErrorLogID', $id);
        //$result = $this->db->delete('errors');
           
        $data=array('Status'=>'0');
        $this->db->where('ErrorLogID', $id);
        $result = $this->db->update('errors', $data);
    }
    

}