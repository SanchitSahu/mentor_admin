<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Meetingtype_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_meetingtype($id) {
        $query = $this->db->get_where('meetingtype', array('MeetingTypeID' => $id));
        return $query->row_array();
    }

    public function delete_meetingtype($id) {
        $data = array(
            'Status' => '0'
        );
        $this->db->where('MeetingTypeID', $id);
        $result = $this->db->update('meetingtype', $data);
    }

    public function checkMeetingType($MeetingTypeName) {
        try {
            $this->db->where('MeetingTypeName', $MeetingTypeName);
            $this->db->where('Status', 1);
            $this->db->from('meetingtype');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function checkMeetingTypeEdit($MeetingTypeName, $MeetingTypeID) {
        try {
            $this->db->where('MeetingTypeName', $MeetingTypeName);
            $this->db->where('MeetingTypeID !=', $MeetingTypeID);
            $this->db->where('Status', 1);
            $this->db->from('meetingtype');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

}
