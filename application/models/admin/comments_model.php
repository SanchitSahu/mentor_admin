<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Comments_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_acceptCommnets($id) {
        $query = $this->db->get_where('accept_responses', array('ResponseID' => $id));
        return $query->row_array();
    }

    public function delete_accept($id) {
        $data = array('Status' => '0');
        $this->db->where('ResponseID', $id);
        $result = $this->db->update('accept_responses', $data);
    }

    public function get_rejectCommnets($id) {
        $query = $this->db->get_where('decline_responses', array('ResponseID' => $id));
        return $query->row_array();
    }

    public function delete_reject($id) {
        $data = array('Status' => '0');
        $this->db->where('ResponseID', $id);
        $result = $this->db->update('decline_responses', $data);
    }

    public function checkMeetingType($Response, $ResponseName) {
        try {
            $this->db->where('ResponseName', $ResponseName);
            $this->db->where('Status', 1);
            $this->db->from("{$Response}_responses");
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function checkMeetingTypeEdit($Response, $ResponseName, $ResponseID) {
        try {
            $this->db->where('ResponseName', $ResponseName);
            $this->db->where('ResponseID !=', $ResponseID);
            $this->db->where('Status', 1);
            $this->db->from("{$Response}_responses");
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

}
