<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Chart_Model extends CI_Model {
	//private $performance = 'performance';
    function __construct() {
        parent::__construct();
    }
	
	function topicMeetingChart(){
		$query = $this->db->query("SELECT  count(mi.MeetingID ) MeetingID,t.TopicDescription
		 FROM   mentor_meetinginfo mi
			    inner JOIN mentor_topic t 
				ON mi.MeetingTopicID = t.TopicID  
		where mi.status=1
		GROUP  BY t.TopicID");
        return $query->result();
	}
	
	function mentorMeetingChart(){
		$query = $this->db->query("SELECT  count(mi.MeetingID ) MeetingID,m.MentorName
		 FROM   mentor_meetinginfo mi
			    inner JOIN mentor_mentor m 
				ON mi.MentorID = m.MentorID  
		where mi.status=1 and m.status=1
		GROUP  BY m.MentorID");
        return $query->result();
	}
	
	function avgMeetingLengthByTopic()
	{
		$query = $this->db->query("SELECT  ROUND(AVG(mi.MeetingID )) AS Average,t.TopicDescription
		 FROM   mentor_meetinginfo mi
				inner JOIN mentor_topic t
				ON mi.MeetingTopicID = t.TopicID  
		where mi.status=1 and t.status=1
		GROUP  BY t.TopicID");
        return $query->result();
	}


	function get_chart_data() {
        $query = $this->db->query("SELECT s.SkillID,s.SkillName, count(m.MentorID ) mentor_ids,    Group_concat(m.MentorName)     mentor_names 
		FROM   mentor_mentor m 
			   inner JOIN mentor_mentorskillset ms 
					   ON m.MentorID = ms.MentorID 
			   inner JOIN mentor_skill s 
					   ON ms.SkillID = s.skillID 
		where m.status=1			   
		GROUP  BY s.SkillID, 
				  s.SkillName");
        return $query->result();
    }
	
	function menteeSkillchart() {
        $query = $this->db->query("SELECT s.SkillID,s.SkillName, count(m.MenteeID ) mentee_ids,    Group_concat(m.MenteeName)     mentee_names 
		FROM   mentor_mentee m 
			   inner JOIN mentor_menteeskill ms 
					   ON m.MenteeID = ms.MenteeID 
			   inner JOIN mentor_skill s 
					   ON ms.SkillID = s.skillID 
		where m.status=1			   
		GROUP  BY s.SkillID, 
				  s.SkillName");
				  
				  
		/*SELECT
   s.SkillID,s.SkillName,count(m.MenteeID ) mentee_ids,    Group_concat(mentee_names)     mentee_names
FROM mentor_skill s

JOIN (SELECT MenteeID,SkillID
      FROM mentor_menteeskill) ms on s.SkillID = ms.SkillID

JOIN (SELECT MenteeID, Group_concat(MenteeName) AS mentee_names
      FROM mentor_mentee GROUP BY MenteeID) m on m.MenteeID = ms.MenteeID
	  
GROUP  BY s.SkillID, 
				  s.SkillName	  */		  
        return $query->result();
    }
	
	function mentorDetailchart(){
		$query = $this->db->query("SELECT m.MentorID,m.MentorName,me.MenteeName,count(me.MenteeID) as total,sum(mi.MeetingElapsedTime) as totaltime
		FROM mentor_meetinginfo as mi
		LEFT JOIN mentor_mentor AS m on mi.MentorID=m.MentorID
		LEFT JOIN mentor_mentee AS me on mi.MenteeID=me.MenteeID
		where m.status=1 and me.status=1
		GROUP BY me.MenteeID");
        return $query->result();
	}
	
	function getMenteeList()
	{
		$query = $this->db->query("SELECT * from mentor_mentee where status=1");
        return $query->result();
	}
	
	function getSkill(){
		$query = $this->db->query("SELECT * from mentor_skill where status=1");
        return $query->result();
	}
	
	function mentorActiveInactive(){
		$query = $this->db->query("SELECT count(m.MentorID)as totalMentors,m.MentorName
		FROM mentor_mentor as m
		INNER JOIN mentor_relationship AS r on r.MentorID=m.MentorID
		where m.status=1
		GROUP BY m.MentorID");
        return $query->num_rows();
	}
	
	function mentorINActiveInactive(){
		$query = $this->db->query("SELECT * from mentor_mentor where status=1");
        return $query->num_rows();
	}
}