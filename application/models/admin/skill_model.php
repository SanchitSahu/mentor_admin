<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Skill_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_skill($id) {
        $query = $this->db->get_where('skill', array('SkillID' => $id));
        return $query->row_array();
    }

    public function delete_skill($id) {

        $data = array('Status' => '0');
        $this->db->where('SkillID', $id);
        $result = $this->db->update('skill', $data);

        $data = array('Status' => '0');
        $this->db->where('SkillID', $id);
        $result = $this->db->update('mentorskillset', $data);

        $data = array('Status' => '0');
        $this->db->where('SkillID', $id);
        $result = $this->db->update('menteeskill', $data);
    }

    public function checkSkill($SkillName) {
        try {
            $this->db->where('SkillName', $SkillName);
            $this->db->where('Status', 1);
            $this->db->from('skill');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function checkSkillEdit($SkillName, $SkillID) {
        try {
            $this->db->where('SkillName', $SkillName);
            $this->db->where('SkillID !=', $SkillID);
            $this->db->where('Status', 1);
            $this->db->from('skill');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

}
