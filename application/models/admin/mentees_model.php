<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mentees_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_mentees($id) { 
    	//$sql = $this->db->select('m.*,mci.*,mp.MenteeToProgramme,p.*,us.menteeToSourceID,ms.*')
		$sql = $this->db->select('m.MenteeID, m.MenteeName, m.Status, m.Weight,mci.MenteePhone, mci.DisableSMS, mci.MenteeEmail, mci.MenteeImage,
p.ProgrammeName,p.ProgrammeID, ms.MenteeSourceID,ms.MenteeSourceName')
	    	->from('mentee m')
	    	->join('menteecontact mci', 'mci.MenteeId = m.MenteeId', 'left')
			->join('menteetoprogramme mp', 'mp.MenteeID = mci.MenteeID','left')
			->join('menteeprogrammes p', 'mp.ProgrammeID = p.ProgrammeID','left')
			->join('menteetosource us', 'us.MenteeID = mci.MenteeID','left')
			->join('menteesourcenames ms', 'us.MenteeSourceID = ms.MenteeSourceID','left')
	    	->where('mci.MenteeContactID',$id)
	    	->get();
        $result = $sql->row_array();
        return $result;
    }
    
    public function delete_mentees($id) { 
                  
        $data=array('Status'=>'0');
        $this->db->where('MenteeID', $id);
        $result = $this->db->update('mentee', $data);
        
        $data=array('Status'=>'0');
        $this->db->where('MenteeID', $id);
        $result = $this->db->update('menteeskill', $data);
              
        $data=array('Status'=>'0');
        $this->db->where('MenteeID', $id);
        $result = $this->db->update('menteecomment', $data);
        
        $data=array('Status'=>'0');
        $this->db->where('MenteeID', $id);
        $result = $this->db->update('menteecontact', $data);
           
        $data=array('Status'=>'0');
        $this->db->where('MenteeID', $id);
        $result = $this->db->update('menteeactionstaken', $data);
        
    }
    
	
	function checkMenteeName($name) {
        try {
            $this->db->select('MenteeID');
            $this->db->from('mentee');
            $this->db->where('MenteeName', $name);
			$this->db->where('Status',1);
            $query = $this->db->count_all_results();
			if ($query == 0) {
            	$this->db->select('MentorID');
            	$this->db->from('mentor');
            	$this->db->where('MentorName', $name);
				$this->db->where('Status',1);
            	$query = $this->db->count_all_results();
            }
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }
	
	function checkMenteeNameEdit($id, $name) {
		try {
            $this->db->select('MenteeID');
            $this->db->from('mentee');
            $this->db->where('MenteeName', $name);
            $this->db->where('MenteeID !=', $id);
			$this->db->where('Status',1);
            $query = $this->db->count_all_results();
			if ($query == 0) {
            	$this->db->select('MentorID');
            	$this->db->from('mentor');
            	$this->db->where('MentorName', $name);
				$this->db->where('Status',1);
            	$query = $this->db->count_all_results();
            }
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
	}
    
    //check email exist or not
    function checkuser_mail($email) {
        try {
            $this->db->select('MenteeContactID');
            $this->db->from('menteecontact');
            $this->db->where('MenteeEmail', $email);
			$this->db->where('Status',1);
            $query = $this->db->count_all_results();
            if ($query == 0) {
            	$this->db->select('MentorContactInfoID');
            	$this->db->from('mentorcontactinfo');
            	$this->db->where('MentorEmail', $email);
				$this->db->where('Status',1);
            	$query = $this->db->count_all_results();
            }
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }
    
    //check email exist or not
    function checkuser_mail_edit($id, $email) {
        try {
            $this->db->select('MenteeContactID');
            $this->db->from('menteecontact');
            $this->db->where('MenteeEmail', $email);
            $this->db->where('MenteeId !=', $id);
            $query = $this->db->count_all_results();
            if ($query == 0) {
		$this->db->select('MentorContactInfoID');
            	$this->db->from('mentorcontactinfo');
            	$this->db->where('MentorEmail', $email);
            	$this->db->where('MentorID !=', $id);
            	$query = $this->db->count_all_results();            
            }
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }
	
	public function get_menteeProgramme($menteeID)
	{
		$sql = $this->db->select('u.*')
	    	->from('menteetoprogramme u')
	    	->where('u.MenteeID',$menteeID)
	    	->get();
        $result = $sql->row_array();
        return $result;
	}
	
	public function programmeData(){
    	try {
            $this->db->select('ProgrammeID,ProgrammeName');
            $this->db->from('menteeprogrammes');
			$this->db->where("Status", 1);
			$this->db->order_by("Weight", "asc");
			$this->db->order_by("ProgrammeName", "asc");
            $query = $this->db->get();
            $resultArr = $query->result_array();
            $role = array("" => '--Select Programme--');
            if (!empty($resultArr)) {
                foreach ($resultArr as $value) {
                    $role[$value['ProgrammeID']] =  $value['ProgrammeName'];
                }
                return $role;
            } else {
				return $role;
			}
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }
	
	public function sourceData(){
    	try {
            $this->db->select('MenteeSourceID,MenteeSourceName');
            $this->db->from('menteesourcenames');
			$this->db->where("Status", 1);
            $this->db->order_by("Weight", "asc");
			$this->db->order_by("MenteeSourceName", "asc");
            $query = $this->db->get();
            $resultArr = $query->result_array();
            $role = array("" => '--Select Source Name--');
            if (!empty($resultArr)) {
                foreach ($resultArr as $value) {
                    $role[$value['MenteeSourceID']] =  $value['MenteeSourceName'];
                }
                return $role;
            } else {
				return $role;
			}
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }
	
	public function get_menteesSource($menteeID)
	{
		$sql = $this->db->select('u.*')
	    	->from('menteetosource u')
	    	->where('u.MenteeID',$menteeID)
	    	->get();
        $result = $sql->row_array();
        return $result;
	}
}
