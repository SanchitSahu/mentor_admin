<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Employee_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * This function is use for city listing
     * @return type
     */
    function bannerList() {
        try {
            $this->db->select('');
            $this->db->from('banners');
            $this->db->order_by("iSequence", "asc");
            $query = $this->db->get();
            $resultArr = $query->result_array();
            if (!empty($resultArr)) {
                return $resultArr;
            }
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    /**
     * This function is use for add state
     * @param type $country
     */
    function addBanner($banner) {
        try {
            $this->db->insert('banners', $banner);
            return $this->db->insert_id();
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    /**
     * This function is use for getting detail for state
     * @param type $stateId
     * @return type
     */
    function get_banner_details($bannerId) {
        try {
            $this->db->select('');
            $this->db->from('banners');
            $this->db->where('iBannerId', $bannerId);
            $query = $this->db->get();
            return $query->row_array();
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    /**
     * This function is use for update state
     * @param type $country
     */
    function editBanner($banner) {
        try {
            $this->db->update('banners', $banner, array('iBannerId' => $banner['iBannerId']));
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    function getActiveEmployee() {
        try {
            $this->db->select('');
            $this->db->from('employeedetails');
            $this->db->order_by("iEmployeeId", "desc");
            $query = $this->db->get();
            $resultArr = $query->result_array();
            if (!empty($resultArr)) {
                return $resultArr;
            }
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    /**
     * This function is use for holiday listing
     * @return type
     */
    function employeeList() {
        try {
            $this->db->select('e.iEmployeeId, e.vFirstName, e.vLastName, e.vUserName, e.vEmail, e.eGender, e.vCellPhone, e.vPhoneNumber, e.vAddress1, e.eStatus');
            $this->db->from('employeedetails e');

            $query = $this->db->get();
            $resultArr = $query->result_array();
            if (!empty($resultArr)) {
                return $resultArr;
            }
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    function setEmployee($id = 0,$UserType) {

		$this->db->select('');
        $this->db->from('admindetails');
        $this->db->where('AdminID', $id);
        $query = $this->db->get();
		$res	=	$query->result();
		//print_r($_REQUEST);exit;
		
		$username	=	explode(" ",$this->input->post('FirstName'));
		//print_r($sess);exit;
        $data = array(
            'FirstName' 	=>	$username[0],
            'LastName' 		=>	$username[1],
         // 'Gender' 		=>	$this->input->post('Gender'),
            'Address' 		=>	$this->input->post('Address'),
            'City' 			=>	$this->input->post('City'),
            'PostCode' 		=>	$this->input->post('PostCode'),
            'Cellphone' 	=>	$this->input->post('Cellphone'),
            'PhoneNumber' 	=>	$this->input->post('PhoneNumber'),
			'Email'			=>	$this->input->post('EmailAddress'),
			'Country'		=>	$this->input->post('Country'),
			'SMS'		=>	$this->input->post('sMSCell')
        );

		
		if(!empty($res)){
			$session = $this->session->userdata;
			//$sess = $session['admin_data'];
			$user['id'] = 1;
			$user['name'] = $this->input->post('FirstName') . ' ' . $this->input->post('LastName');
			$user['UserType'] = 'superadmin';
			$user['profilePicture'] =	$res[0]->PictureURL ;
			$this->session->set_userdata('admin_data', $user);
		}else{
			$this->db->select('');
			$this->db->from('subdomains');
			$this->db->where('subdomain_id', $id);
			$query2 = $this->db->get();
			$res2	=	$query2->result();
			if(!empty($res2)){
				$user['id'] = $res2[0]->subdomain_id;
				$user['name'] = "";
				$user['UserType'] = 'Admin';
				$user['profilePicture'] =	"" ;
				$this->session->set_userdata('admin_data', $user);
			}else{
				
			}
		}
        
		$filename	=	"";
		if($UserType	==	"Admin"){
			if ($id == 0) {
				$result = $this->db->insert('subdomains', $data);
				$id = $this->db->insert_id();
			} else {
				$this->db->where('subdomain_id', $id);
				$result = $this->db->update('subdomains', $data);
			}
			if ($result) {
				if ($_FILES['photo']['error'] == 0) {
					$pathMain = $this->config->item('base_images');
					$pathThumb = $this->config->item('base_images');
					$imageNameTime = time();
					// $imageNamePrefix = "tmp_";
					//$orgFileName =  $imageNamePrefix.$imageNameTime; 
					$path = $_SERVER['DOCUMENT_ROOT']."/mentor/assets/admin/images/admin/".$imageNameTime . "_" . $id.".jpg";
					$filename	=	$imageNameTime . "_" . $id.".jpg";
					if (move_uploaded_file($_FILES["photo"]["tmp_name"], $path)) {
						$status	=	1;
					} else {
						$status	=	0;
					}
					if ($status == 1) {
						$uploadedFileName = $result['upload_data']['file_name'];
						$arrEmployee = $this->get_employee($id);
						// UPDATE PHOTO FIELD INTO EMPLOYEE TABLE 
						$this->db->where('subdomain_id', $id);
						$this->db->update('subdomains', array('PictureURL' => $filename . $uploadedFileName));
					}
					@unlink($pathMain . $uploadedFileName);
				}
			}
			$user['id'] = $id;
			$user['name'] = $this->input->post('FirstName') . ' ' . $this->input->post('LastName');
			$user['UserType'] = 'Admin';
			$user['profilePicture'] =	$filename;
			$this->session->set_userdata('admin_data', $user);
		}else{
			if ($id == 0) {
				$result = $this->db->insert('admindetails', $data);
				$id = $this->db->insert_id();
			} else {
				$this->db->where('AdminID', $id);
				$result = $this->db->update('admindetails', $data);
			}
			if ($result) {
				if ($_FILES['photo']['error'] == 0) {
					$pathMain = $this->config->item('base_images');
					$pathThumb = $this->config->item('base_images');
					$imageNameTime = time();
					// $imageNamePrefix = "tmp_";
					//$orgFileName =  $imageNamePrefix.$imageNameTime; 
					$path = $_SERVER['DOCUMENT_ROOT']."/mentor/assets/admin/images/admin/".$imageNameTime . "_" . $id.".jpg";
					$filename	=	$imageNameTime . "_" . $id.".jpg";
					if (move_uploaded_file($_FILES["photo"]["tmp_name"], $path)) {
						$status	=	1;
					} else {
						$status	=	0;
					}
					if ($status == 1) {
						$uploadedFileName = $result['upload_data']['file_name'];
						$arrEmployee = $this->get_employee($id);
						// UPDATE PHOTO FIELD INTO EMPLOYEE TABLE 
						$this->db->where('AdminID', $id);
						$this->db->update('admindetails', array('PictureURL' => $filename . $uploadedFileName));
					}
					@unlink($pathMain . $uploadedFileName);
				}
			}
			$user['id'] = 1;
			$user['name'] = $this->input->post('FirstName') . ' ' . $this->input->post('LastName');
			$user['UserType'] = 'superadmin';
			$user['profilePicture'] =	$filename;
			$this->session->set_userdata('admin_data', $user);
		}

        
    }

    public function dellog() {

        $delArray = $this->input->post('dellog');

        for ($i = 0; $i < count($delArray); $i++) {
            $data = array(
                'eStatus' => 2
            );
            $this->db->where('iActivityLogId', $delArray[$i]);
            $result = $this->db->update('activitylog', $data);
        }
    }


    public function updatepwd($pwd, $id) {
        $data = array(
            'vPassword' => $pwd
        );
        $this->db->where('iEmployeeId', $id);
        $result = $this->db->update('employeedetails', $data);
        return true;
    }

    public function get_employee($id) {
        $query = $this->db->get_where('admindetails', array('AdminID' => $id));
        return $query->row_array();
    }

    public function get_employeeByemail($email) {
        $query = $this->db->get_where('employeedetails', array('vEmail' => $email));
        return $query->row_array();
    }

	public function get_employeeAdmin($id) {
        $query = $this->db->get_where('subdomains', array('subdomain_id' => $id));
		//echo $this->db->last_query();exit;
        return $query->row_array();
    }
	
	public function checkemail($email,$currentMail)
	{
		$sql = $this->db->select('*')
	    	->from('subdomains')
			->where("Email",$email)
			->where("Status",1)
			->where_not_in("Email",$currentMail)
	    	->get();
        //$result = $sql->row_array();
        return $sql->num_rows();
	}
}