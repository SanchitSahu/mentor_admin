<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clientsource_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_source($id) {
        $query = $this->db->get_where('menteesourcenames', array('MenteeSourceID' => $id));
        return $query->row_array();
    }

    public function delete_source($id) {
        $data = array('Status' => '0');
        $this->db->where('MenteeSourceID', $id);
        $result = $this->db->update('menteesourcenames', $data);
    }

    public function checkMenteeSource($MenteeSourceName) {
        try {
            $this->db->where('MenteeSourceName', $MenteeSourceName);
            $this->db->where('Status', 1);
            $this->db->from('menteesourcenames');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function checkMenteeSourceEdit($MenteeSourceName, $MenteeSourceID) {
        try {
            $this->db->where('MenteeSourceName', $MenteeSourceName);
            $this->db->where('MenteeSourceID !=', $MenteeSourceID);
            $this->db->where('Status', 1);
            $this->db->from('menteesourcenames');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

}
