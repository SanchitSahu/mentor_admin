<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Invitation_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function checkInvitation($days,$gap) { 
         			$query =$this->db->get("mentor_reminder_settings");
        			return $query->num_rows();
    			}
    
    public function getInvitationSetting()
    {
    	$query =$this->db->get("mentor_reminder_settings");
        			return $query->result();
    }
    
    public function getSubDomains(){
		$this->db->where('Status', 1);
		$query =$this->db->get("c10melstm_net_template.mentor_subdomains");
        return $query->result();
	}

	public function getExpiryTime($name){
		$query =$this->db->get("c10melstm_net_".$name.".mentor_settings");
        return $query->result();
	}
	
	public function getInvitationData($name){
		$this->db->where('Status', "Pending");
		$query =$this->db->get("c10melstm_net_".$name.".mentor_invitation");
        return $query->result();
	}
	
	public function deleteInvitation($name,$InvitationId){
		$data = array('Status' => 'TimedOut');
        $this->db->where('InvitationId', $InvitationId);
        $result = $this->db->update('c10melstm_net_'.$name.'.mentor_invitation', $data);
	}
}