<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bulkupload_model extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function saveDataFromCSV($data, $tbl_name) {
        foreach ($data as $single_data) {

            foreach ($single_data as $key => &$checkBlankValues) {
				$checkBlankValues	=	checkAbbreviations($checkBlankValues);
                if ($key == "Status" && $checkBlankValues == "") {
                    unset($single_data[$key]);
                } else if ($key == "Weight" && $checkBlankValues == "") {
                    unset($single_data[$key]);
                }
            }
            
            $count = 0;
            switch ($tbl_name) {
                case 'mentor_meetingtype': $fieldname = 'MeetingTypeName';
                    $count = $this->checkEntryAlreadyExist($tbl_name, $fieldname, $single_data['MeetingTypeName']);
                    break;
                case 'mentor_meetingplace': $fieldname = 'MeetingPlaceName';
                    $count = $this->checkEntryAlreadyExist($tbl_name, $fieldname, $single_data['MeetingPlaceName']);
                    break;
                case 'mentor_topic': $fieldname = 'TopicDescription';
                    $count = $this->checkEntryAlreadyExist($tbl_name, $fieldname, $single_data['TopicDescription']);
                    break;
                case 'mentor_mentordefinedactions': $fieldname = 'MentorActionName';
                    $count = $this->checkEntryAlreadyExist($tbl_name, $fieldname, $single_data['MentorActionName']);
                    break;
                case 'mentor_menteedefinedactions': $fieldname = 'MenteeActionName';
                    $count = $this->checkEntryAlreadyExist($tbl_name, $fieldname, $single_data['MenteeActionName']);
                    break;
                case 'mentor_skill': $fieldname = 'SkillName';
                    $count = $this->checkEntryAlreadyExist($tbl_name, $fieldname, $single_data['SkillName']);
                    break;
                case 'mentor_careCriteria': $fieldname = 'Name';
                    $count = $this->checkEntryAlreadyExist($tbl_name, $fieldname, $single_data['Name']);
                    break;
                case 'mentor_user_roles': $fieldname = 'UserRoleName';
                    $count = $this->checkEntryAlreadyExist($tbl_name, $fieldname, $single_data['UserRoleName']);
                    break;
                case 'mentor_mentorsourcenames': $fieldname = 'MentorSourceName';
                    $count = $this->checkEntryAlreadyExist($tbl_name, $fieldname, $single_data['MentorSourceName']);
                    break;
                case 'mentor_menteesourcenames': $fieldname = 'MenteeSourceName';
                    $count = $this->checkEntryAlreadyExist($tbl_name, $fieldname, $single_data['MenteeSourceName']);
                    break;
                case 'mentor_menteeprogrammes': $fieldname = 'ProgrammeName';
                    $count = $this->checkEntryAlreadyExist($tbl_name, $fieldname, $single_data['ProgrammeName']);
                    break;
                case 'mentor_accept_responses':
                case 'mentor_decline_responses': $fieldname = 'ResponseName';
                    $count = $this->checkEntryAlreadyExist($tbl_name, $fieldname, $single_data['ResponseName']);
                    break;
                case 'mentor_abbreviations': $fieldname = 'AbbreviationKey';
                    $count = $this->checkEntryAlreadyExist($tbl_name, $fieldname, $single_data['AbbreviationKey']);
                    break;
            }
            if ($count > 0) {
                continue;
            }

            if ($this->db->insert($tbl_name, $single_data)) {
                //echo "Inserted successfully<br>";	
            } else {
                $errNo = $this->db->_error_number();
                $errMess = $this->db->_error_message();
                echo $errNo . "<br>" . $errMess . "<br>";
                exit;
            }
        }
    }
    
    function checkEntryAlreadyExist($table, $fieldname, $value) {
        //echo $table . " " . $fieldname . " " . $value;

        $query = $this->db->query("SELECT $fieldname FROM $table WHERE `$fieldname`='$value' and Status !=0");
        //echo $this->db->last_query();
        return $query->num_rows();
    }

    function checkSubdomain($domainName) {
        $query = $this->db->query("SELECT * FROM mentor_subdomains WHERE subdomain_name = '" . $domainName . "'");

        return $query->result();
    }

    function checkRoleExists($RoleCheck) {
		$RoleCheck	=	checkAbbreviations($RoleCheck);
        $query = $this->db->query("SELECT * FROM mentor_user_roles WHERE UserRoleName = '" . $RoleCheck . "'");
		if($query->num_rows() == 0){
			$query = $this->db->query("SELECT * FROM mentor_user_roles WHERE UserRoleName = (SELECT mentorName FROM mentor_settings)");
			return $query->result();
		} else {
			return $query->result();
		}
    }

    function checkSourceExists($SourceCheck) {
		$SourceCheck	=	checkAbbreviations($SourceCheck);
        $query = $this->db->query("SELECT * FROM mentor_mentorsourcenames WHERE  MentorSourceName = '" . $SourceCheck . "'");

        return $query->result();
    }

    function addMentor($mentorName, $mentorStatus = '', $mentorWeight = '') {
		$mentorName	=	checkAbbreviations($mentorName);
		$query = $this->db->query("SELECT * FROM mentor_mentor WHERE `MentorName`='$mentorName' and Status !=0");
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			$data = array(
				'MentorName' => $mentorName
			);
			if (!empty($mentorStatus)) {
				$data['Status'] = $mentorStatus;
			}
			if (!empty($mentorWeight)) {
				$data['Weight'] = $mentorWeight;
			}
			$result = $this->db->insert('mentor', $data);
			
			$insertID = $this->db->insert_id();

			return $insertID;
		}
        
    }

    function addContact($addMentor, $MentorPhone, $MentorEmail,$MentorName, $IsSubadmin) {
        $data = array(
            'MentorID' => $addMentor,
            'MentorPhone' => $MentorPhone,
            'MentorEmail' => $MentorEmail,
			'IsSubadmin' => $IsSubadmin
        );
		
		$c_date = date('Y-m-d H:i:s');
		$newRegister = array(
			'UserID' => $addMentor,
			'UserType' => 0,
			'DateRegistered' => $c_date,
			//'DateUpdated' => $c_date,
			'UserName' => $MentorName,
			'UserEmail' => $MentorEmail
		);
		$this->db->insert('newregistrations',$newRegister);
		
        $result = $this->db->insert('mentorcontactinfo', $data);
        return $this->db->insert_id();
    }

    function addRole($addMentor, $roleId) {
        $data = array(
            'MentorID' => $addMentor,
            'userRoleID' => $roleId
        );
        $result = $this->db->insert('usertorole', $data);
        return $this->db->insert_id();
    }

    function addSource($addMentor, $sourceId) {
        $data = array(
            'MentorID' => $addMentor,
            'MentorSourceID' => $sourceId
        );
        $result = $this->db->insert('usertosource', $data);
        return $this->db->insert_id();
    }

    function checkRoleExistsMentee($RoleCheck) {
		$RoleCheck	=	checkAbbreviations($RoleCheck);
        $query = $this->db->query("SELECT * FROM mentor_menteeprogrammes WHERE programmeName  = '" . $RoleCheck . "'");

        return $query->result();
    }

    function checkSourceExistsMentee($SourceCheck) {
		$SourceCheck	=	checkAbbreviations($SourceCheck);
        $query = $this->db->query("SELECT * FROM mentor_menteesourcenames WHERE MenteeSourceName = '" . $SourceCheck . "'");

        return $query->result();
    }

    function addMentee($menteeName, $menteeStatus='', $menteeWeight='') {
		$menteeName	=	checkAbbreviations($menteeName);
		$query = $this->db->query("SELECT * FROM mentor_mentee WHERE `MenteeName`='$menteeName' and Status !=0");
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			$data = array(
				'MenteeName' => $menteeName
			);
			if (!empty($menteeStatus)) {
				$data['Status'] = $menteeStatus;
			}
			if (!empty($menteeWeight)) {
				$data['Weight'] = $menteeWeight;
			}
			$result = $this->db->insert('mentee', $data);

			$insertID = $this->db->insert_id();
			
			
			return $insertID;
		}
        
    }

    function addMenteeContact($addMentor, $MentorPhone, $MentorEmail,$menteeName) {
        $data = array(
            'MenteeId' => $addMentor,
            'MenteePhone' => $MentorPhone,
            'MenteeEmail' => $MentorEmail
        );
		
		$c_date = date('Y-m-d H:i:s');
		$newRegister = array(
			'UserID' => $addMentor,
			'UserType' => 1,
			'DateRegistered' => $c_date,
			//'DateUpdated' => $c_date,
			'UserName' => $menteeName,
			'UserEmail' => $MentorEmail
		);
		$this->db->insert('newregistrations',$newRegister);
		
        $result = $this->db->insert('menteecontact', $data);
        return $this->db->insert_id();
    }

    function addMenteeRole($addMentor, $roleId) {
        $data = array(
            'MenteeID' => $addMentor,
            'programmeID' => $roleId
        );
        $result = $this->db->insert('menteetoprogramme', $data);
        return $this->db->insert_id();
    }

    function addMenteeSource($addMentor, $sourceId) {
        $data = array(
            'MenteeID' => $addMentor,
            'MenteeSourceID' => $sourceId
        );
        $result = $this->db->insert('menteetosource', $data);
        return $this->db->insert_id();
    }

    function addSubTopic($subTopicName, $topicId, $status, $weight) {
		$subTopicName	=	checkAbbreviations($subTopicName);
		$query = $this->db->query("SELECT * FROM mentor_subtopic WHERE `SubTopicDescription`='$subTopicName' and TopicID='$topicId' and Status !=0");
		
		if($query->num_rows() > 0){
			return 1;
		}else{
			$insert_data = array(
				'TopicID' => $topicId,
				'SubTopicDescription' => $subTopicName
			);
			if ($status != '') {
				$insert_data['Status'] = $status;
			}
			if ($weight != '') {
				$insert_data['Weight'] = $weight;
			}
			$result = $this->db->insert('mentor_subtopic', $insert_data);
			return $this->db->insert_id();
		}
		
        
    }

    function checkTopicExists($topic) {
		$topic	=	checkAbbreviations($topic);
        $query = $this->db->query("SELECT * FROM mentor_topic WHERE TopicDescription  = '" . $topic . "' and Status !=0");

        return $query->result();
    }
    
    function checkSkillExists($skill) {
		$skill	=	checkAbbreviations($skill);
        $query = $this->db->query("SELECT * FROM mentor_skill WHERE SkillName = '$skill' and Status !=0");
        return $query->result();
    }

    function checkMentorExists($mentorName) {
		$mentorName	=	checkAbbreviations($mentorName);
        $query = $this->db->query("SELECT * FROM mentor_mentor WHERE MentorName = '$mentorName' and Status !=0");
        return $query->result();
    }

    function checkMenteeExists($menteeName) {
		$menteeName	=	checkAbbreviations($menteeName);
        $query = $this->db->query("SELECT * FROM mentor_mentee WHERE MenteeName = '$menteeName' and Status !=0");
        return $query->result();
    }

    function addSkillToUser($userId, $skillId, $userType, $status, $weight) {
        if (!empty($userId) && !empty($skillId) && !empty($userType)) {
            if ($userType == 'mentor') {
                $table = 'mentor_mentorskillset';
                $userField = 'MentorID';
            } else if ($userType == 'mentee') {
                $table = 'mentor_menteeskill';
                $userField = 'MenteeID';
            }

            $checkSkillExistWithUser = $this->db->query("SELECT * FROM $table WHERE $userField= '$userId' AND SkillID='$skillId' and Status !=0");
            if ($checkSkillExistWithUser->num_rows() > 0) {
                return FALSE;
            } else {
                $insert_data = array(
                    $userField => $userId,
                    'SkillID' => $skillId
                );
                if ($status != '') {
                    $insert_data['Status'] = $status;
                }
                if ($weight != '') {
                    $insert_data['Weight'] = $weight;
                }

                if($returnID = $this->db->insert($table, $insert_data)){
                    return $returnID;
                }
            }
        }
    }

	function updateContact($addMentor, $MentorPhone, $MentorEmail) {
        $data = array(
            'MentorPhone' => $MentorPhone,
            'MentorEmail' => $MentorEmail
        );
		$this->db->where('MentorID',$addMentor);
        $result = $this->db->update('mentorcontactinfo', $data);
        return $this->db->affected_rows();
    }

    function updateRole($addMentor, $roleId) {
        $data = array(
            'userRoleID' => $roleId
        );
		$this->db->where('MentorID',$addMentor);
        $result = $this->db->update('usertorole', $data);
        return $this->db->affected_rows();
    }

    function updateSource($addMentor, $sourceId) {
        $data = array(
            'MentorSourceID' => $sourceId
        );
		$this->db->where('MentorID',$addMentor);
        $result = $this->db->update('usertosource', $data);
        return $this->db->affected_rows();
    }
	
	function updateMenteeContact($addMentor, $MentorPhone, $MentorEmail) {
        $data = array(
            'MenteePhone' => $MentorPhone,
            'MenteeEmail' => $MentorEmail
        );
		$this->db->where('MenteeID',$addMentor);
        $result = $this->db->update('menteecontact', $data);
        return $this->db->affected_rows();
    }

    function updateMenteeRole($addMentor, $roleId) {
        $data = array(
            'programmeID' => $roleId
        );
		$this->db->where('MenteeID',$addMentor);
        $result = $this->db->update('menteetoprogramme', $data);
        return $this->db->affected_rows();
    }

    function updateMenteeSource($addMentor, $sourceId) {
        $data = array(
            'MenteeSourceID' => $sourceId
        );
		$this->db->where('MenteeID',$addMentor);
        $result = $this->db->update('menteetosource', $data);
        return $this->db->affected_rows();
    }
	
	function addError($error, $tableName,$lineNo,$extra_fields,$type) {
		$query	=	 $this->db->query("SELECT * FROM mentor_subdomains");
        $getDomain	=	$query->result();
		
		//print_r($extra_fields);exit;
		//$c_date = date('Y-m-d H:i:s');
		$data = array(
			'ValueInError' => $error,
			'FileName' => $tableName,
			'ErrorLine' => $lineNo,
			'NameOfField' =>$extra_fields,
			'UserID' => $getDomain[0]->subdomain_id,
			'ErrorType' => $type
		);		
        $result = $this->db->insert('errors', $data);
        return $this->db->insert_id();
	}
}
