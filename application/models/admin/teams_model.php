<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Teams_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function addMentorToTeam($TeamId, $UserId) {
        $data = array(
            'UserID' => $UserId,
            'TeamID' => $TeamId,
            'UserType' => 0,
            'CreatedDate' => date('Y-m-d H:i:s')
        );
        if ($this->db->insert('teammember', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function addMenteeToTeam($TeamId, $UserId) {
        $data = array(
            'UserID' => $UserId,
            'TeamId' => $TeamId,
            'UserType' => 1,
            'CreatedDate' => date('Y-m-d H:i:s')
        );
        if ($this->db->insert('teammember', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function getTeamDetails($id, $isGetUsers = '') {
        if ($id != '') {
            $this->db->where('TeamId', $id);
        }
        if ($isGetUsers == '') {
            $query = $this->db->get('team');
        } else {
            $this->db->select('TeamId, UserID, UserType');
            $query = $this->db->get('teammember');
        }
        if ($query->num_rows() == 0) {
            return array();
        } else {
            return $query->result();
        }
    }

    public function getMentorMenteeDetails($user) {
        if ($user->UserType == 1) {
            $this->db->select('MentorName');
            $this->db->where('MentorID', $user->UserID);
            $query = $this->db->get('mentor');
            if ($query->num_rows() > 0) {
                $result = $query->result();
                return $result[0]->MentorName;
            }
        } else if ($user->UserType == 0) {
            $this->db->select('MenteeName');
            $this->db->where('MenteeID', $user->UserID);
            $query = $this->db->get('mentee');
            if ($query->num_rows() > 0) {
                $result = $query->result();
                return $result[0]->MenteeName;
            }
        }
        return '';
    }

    public function getAllMentors() {
        /*$this->db->select('MentorID, MentorName');
        $this->db->where('Status !=', 0);
        $query = $this->db->get('mentor');*/
		$query = $this->db->select('distinct(m.MentorID),m.MentorName')
		->from('mentor m ')
		->join('teammember tm', 'tm.UserID != m.MentorID AND tm.UserType = 0', 'INNER')
        ->where('m.Status !=', 0)
		->where('tm.ApprovalStatus !=', 'team_deleted')
        ->get();

        $resultArr = $query->result_array();


		$query2 = $this->db->get("settings");
        $setting = $query2->result();	
		//print_r($setting);exit;
		$mentorName = '-- Select'.$setting[0]->mentorName.' --';
		$mentors = array("" => $mentorName);
        if (!empty($resultArr)) {
            foreach ($resultArr as $value) {
                $mentors[$value['MentorID']] = $value['MentorName'];
            }
            return $mentors;
        } else {
            return array("" => "");
        }
    }

    public function getAllMentees() {
        /*$this->db->select('MenteeID, MenteeName');
        $this->db->where('Status !=', 0);
        $query = $this->db->get('mentee');*/
		/*$query = $this->db->select('distinct(m.MenteeID),m.MenteeName')
		->from('mentee m ')
		->join('teammember tm', 'tm.UserID != m.MenteeID AND tm.UserType = 1', 'INNER')
        ->where('m.Status !=', 0)
		->where('tm.ApprovalStatus !=', 'team_deleted')
        ->get();*/

		$query = $this->db->select('distinct(m.MenteeID),m.MenteeName')
		->from('mentee m ')	
		->join('teammember tm', 'tm.UserID = m.MenteeID AND tm.UserType = 1', 'LEFT')
		->join('team t', 't.TeamOwnerUserID = m.MenteeID', 'LEFT')
		->where('m.Status !=', 0)
		//->where('tm.ApprovalStatus !=', 'team_deleted')
		->where('t.TeamOwnerUserID ', NULL)
		->get();

        $resultArr = $query->result_array();

		//echo $this->db->last_query();exit;

		$query2 = $this->db->get("settings");
        $setting = $query2->result();	
		//print_r($setting);exit;
		$menteeName = '-- Select'.$setting[0]->menteeName.' --';

		$mentees = array("" => $menteeName	);
        if (!empty($resultArr)) {
            foreach ($resultArr as $value) {
                $mentees[$value['MenteeID']] = $value['MenteeName'];
            }
            return $mentees;
        } else {
            return array("" => "");
        }
    }

	public function getAllMentorsDrp() {
        $query = $this->db->select('distinct(m.MentorID),m.MentorName')
		->from('mentor m ')
		->join('teammember tm', 'tm.UserID != m.MentorID AND tm.UserType = 0', 'INNER')
        ->where('m.Status !=', 0)	
		->where('tm.ApprovalStatus !=', 'team_deleted')
        ->get();

        $resultArr = $query->result_array();
        if (!empty($resultArr)) {
            foreach ($resultArr as $value) {
                $mentors[$value['MentorID']] = $value['MentorName'];
            }
            return $mentors;
        } else {
            return array("" => "");
        }
    }

    public function getAllMenteesDrp() {
		$query = $this->db->select('distinct(m.MenteeID),m.MenteeName')
		->from('mentee m ')
		->join('teammember tm', 'tm.UserID != m.MenteeID AND tm.UserType = 1', 'INNER')
        ->where('m.Status !=', 0)
		->where('tm.ApprovalStatus !=', 'team_deleted')
        ->get();
        /*$this->db->select('MenteeID, MenteeName');
        $this->db->where('Status !=', 0);
        $query = $this->db->get('mentee');*/
        $resultArr = $query->result_array();
        if (!empty($resultArr)) {
            foreach ($resultArr as $value) {
                $mentees[$value['MenteeID']] = $value['MenteeName'];
            }
            return $mentees;
        } else {
            return array("" => "");
        }
    }

    public function delete_team($id) {

		$data = array(
            'TeamStatus' => "deleted"
       		);
		$this->db->where('TeamId', $id);
        $this->db->update('team', $data);
	
		$this->db->select("*");
        $this->db->from('teammember t');
		$this->db->where('t.TeamId',$id);
		$query = $this->db->get();
		$result = $query->result_array();
		foreach($result as $val){
			$data = array(
            'ApprovalStatus' => "team_deleted"
       		);
			$this->db->where('TeamId', $id);
        	$this->db->update('teammember', $data); 
		}

        
        
        //$this->db->delete('teammember', $data);
    }

	public function get_teamSkills($id){
		$this->db->select("t.TeamSkills",FALSE);
        $this->db->from('team t');
		$this->db->where('t.TeamId',$id);
		$query = $this->db->get();

		//echo $this->db->last_query();exit;
        return $query->result_array();
	}

	public function getTeamMembersMentor(){
		$query = $this->db->select('distinct(m.MentorID),m.MentorName')
		->from('mentor m ')
		->join('teammember tm', 'tm.UserID != m.MentorID AND tm.UserType = 0', 'INNER')
        ->where('m.Status !=', 0)
        ->get();
		//echo $this->db->last_query();exit;
		$valArr = array();
        $resultArr = $query->result_array();
        if (!empty($resultArr)) {
            foreach ($resultArr as $value) {
                $mentors = $value['MentorID'];
				array_push($valArr,$mentors);
            }
            return $valArr;
        } else {
            return $valArr;
        }

	}

}
