<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clientskillneeded_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_clientSkills($id) { 
        $this->db->select("mss.MenteeSkillID,mss.SkillID,ss.SkillName, mm.MenteeName",FALSE);
        $this->db->from('menteeskill mss');
        $this->db->join('skill ss','ss.SkillID = mss.SkillID','left');
		$this->db->join('mentee mm','mm.MenteeID = mss.MenteeID','left');
		$this->db->where('mss.Status','1');
		$this->db->where('mss.MenteeID',$id);
		$query = $this->db->get();
        return $query->result_array();
    }
    
    
    public function delete_client_skill($id) { 
           
        $data=array('Status'=>'0');
        $this->db->where('MenteeID', $id);
        $result = $this->db->update('menteeskill', $data);
      
    }

	public function get_Skills(){
		$query =  $this->db->get("skill");
		return $query->result_array();
	}

	public function get_Mentee(){
		$query =  $this->db->get("mentee");
		return $query->result_array();
	}
    

}