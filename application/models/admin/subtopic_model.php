<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subtopic_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_subtopic($id) {
        $query = $this->db->get_where('subtopic', array('SubTopicID' => $id));
        return $query->row_array();
    }

    public function delete_subtopic($id) {
        $data = array('Status' => '0');
        $this->db->where('SubTopicID', $id);
        $result = $this->db->update('subtopic', $data);

        $data = array('Status' => '0');
        $this->db->where('SubTopicID', $id);
        $result = $this->db->update('meetingsubtopic', $data);
    }

    public function checkSubtopic($SubTopicDescription, $TopicID) {
        try {
            $this->db->where('SubTopicDescription', $SubTopicDescription);
            $this->db->where('TopicID', $TopicID);
            $this->db->where('Status', 1);
            $this->db->from('subtopic');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

    public function checkSubtopicEdit($SubTopicDescription, $TopicID, $SubTopicID) {
        try {
            $this->db->where('SubTopicDescription', $SubTopicDescription);
            $this->db->where('TopicID', $TopicID);
            $this->db->where('SubTopicID !=', $SubTopicID);
            $this->db->where('Status', 1);
            $this->db->from('subtopic');
            $query = $this->db->count_all_results();
            return $query;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

}
