<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

define("TRUNCATE_NAMES_ARRAY", 1); // when combining field names with field values and there are more names than values
define("TRUNCATE_VALUES_ARRAY", 2); // when combining field names with field values and there are more values then names

class CSVReader {

    var $fields;/** columns names retrieved after parsing */
    var $line_separator = "\n";/** separator used to explode each line */
    var $field_separator = ";";/** character used to separate field names and field values */
    var $enclosure = '"';/** quotes surrounding field names and values */
    var $max_row_size = 4096;/** maximum row size to be used for decoding */

    // issue_too_many_vars_error_msg - this message can be issued from more than one place
    //
    // Input: $max_values_values_expected - maximum number of values expected
    //        $actual_values_count_given - actual # of values received
    //
    // Returns: null
    //

    function issue_too_many_vars_error_msg($max_values_count_expected, $actual_values_count_given, $session, $file = "", $lineno = 0, $tablename = "current") {
        if ($tablename != "current")
            $tablename = $_POST['tableName'];
        $msg = "";
        if ($file) {
            $msg = "<p><span class='alertify-csv-error-label'>Warning</span> in " . $file . "#" . $lineno . ":<br/><br/></p>";
        }
        $msg .= "<p class='alertify-csv-error-text'>The format of the file you tried to upload is incompatible with the " . $tablename . " table. Too many values given.<br/><br/>" .
                "<span class='alertify-csv-error-label'>Expected: </span>A maximum of " . $max_values_count_expected .
                " values expected<br/>" .
                "<span class='alertify-csv-error-label'>But read:&nbsp;&nbsp;</span>" . $actual_values_count_given . " values.<br/></br>" .
                "Please see the Sample File for the correct CSV format.<br/><br/></p>";
        $session->set_flashdata('Warning', $msg);
        $return;
    }

    /* issue_missing_required_fields_error_msg
     *
     * Input: Two headers - one that is expected and one that was actually part of the input file
     *
     * Returns: null
     */

    function issue_missing_required_fields_error_msg($missing_field_names, $expected_count, $actual_count, $session, $file = "", $lineno = 0, $tablename = "current") {
        if ($tablename != "current")
            $tablename = $_POST['tableName'];
        $msg = "";
        if ($file) {
            $msg = "<p><span class='alertify-csv-error-label'>Warning</span> in " . $file . "#" . $lineno . "<br/><br/></p>";
        }
        $msg .= "<p class='alertify-csv-error-text'>The format of the record you tried to upload is incompatible with the " . $tablename . " table.<br/><br/>" .
                "<span class='alertify-csv-error-label'>Expected a minimum of </span>" . $expected_count . " values<br/>" .
                "<span class='alertify-csv-error-label'>But read only&nbsp;&nbsp;</span>" . $actual_count . " values<br/>" .
                "<span class='alertify-csv-error-label'>Missing values for fields: </span>" . $missing_field_names . "<br/><br/>" .
                "Please see the Sample File for the correct CSV format.<br/><br/></p>";
        $session->set_flashdata('Error', $msg);
        return;
    }

    /* parse_file_relate_keys_to_values - tries to do a 1-to-1 match between csv header field names and field values
     *
     * Returns an array with keys from the csv header and field values from the remaining rows in the file.
     *
     */

    function relate_field_names_to_field_values($field_names, $field_values, $session) {
        $ekeyed_array = array();
        $required_field_names_count = count($field_names['REQUIRED']);
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: required_field_names_count=");print($required_field_names_count);print("</pre>\n");
        $all_field_names_array = array_merge($field_names["REQUIRED"], $field_names["OPTIONAL"]);
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: all_field_names_array=<br/>");print_r($all_field_names_array);print("</pre>\n");
        $all_field_names_count = count($all_field_names_array);
        $value_rows_count = count($field_values);
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: all_field_names_count=" . $all_field_names_count . "<br/></pre>\n");
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: initial value_rows_count=" . $value_rows_count . "<br/></pre>\n");
        $truncated_field_names_array = array();
        $rekeyed_array = array();
        foreach ($field_values as $value_row) {
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: value_row=<br/>");print_r($value_row);print("</pre>\n");
            $field_values_count = count($value_row);
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: field_value_rows_count=" . $field_values_count . "<br/></pre>\n");
            // set up default values
            $truncated_field_names_array = $all_field_names_array;
            $truncated_field_values_array = $value_row;

            if ($all_field_names_count < $field_values_count) {  // more values than names
                $this->issue_too_many_vars_error_msg($all_field_names_count, $field_values_count, $session, __FILE__, __LINE__);
                $excess_values = array_slice($field_values, $all_field_names_count, $field_values_count);
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: ** Too many values. Only first ".$all_field_names_count." values used. Excess values are<br/>".print_r($excess_values)."</pre>\n");
                $truncated_field_values_array = array_slice($value_row, 0, $all_field_names_count, TRUE);
            } else if ($all_field_names_count == $field_values_count) { // same number of names and values
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: arrays same size<br/></pre>\n");
;       // do nothing since size of arrays the same
            } else {       // not enough values for required fields
                if ($required_field_names_count > $field_values_count) {
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: ** Missing required field_values **<br/></pre>\n");
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: field_names[REQUIRED]=<br/>");print_r($field_names['REQUIRED']);print("</pre>\n");
                    $missing_value_names = array_diff($field_names['REQUIRED'], array_slice($field_names['REQUIRED'], 0, $field_values_count));
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: Names of missing required fields=<br/>");print_r($missing_value_names);print("</pre>\n");
                    $this->issue_missing_required_fields_error_msg($missing_value_names, $required_field_names_count, $field_values_count, $session, __FILE__, __LINE__);
                    continue;     // ignore this row of values
                }
                $truncated_field_names_array = array_slice($all_field_names_array, 0, $field_values_count, TRUE);
            }
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: truncated_field_names_array=<br/>");print_r($truncated_field_names_array);print("</pre>\n");
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: truncated_field_values_array=<br/>");print_r($truncated_field_values_array);print("</pre>\n");
            $rekeyed_array[] = array_combine($truncated_field_names_array, $truncated_field_values_array);
//print("<pre>[" . __FILE__ . " @ " . __LINE__ . "]: rekeyed_array=<br/>");print_r($rekeyed_array);print("</pre>\n");
        }
        return array($truncated_field_names_array, $rekeyed_array);
    }

    /* analyze_header - analyze CSV header and break into required and optional fields
     *
     * Argument: $header_line
     * Returns:  An array containing required and optional fields.
     */

    function analyze_header($header_line) {
        $line = trim($header_line);
        $names_of_required_fields = array();
        $names_of_optional_fields = array();
        $space_pos = strpos($line, " ");
        if ($space_pos == FALSE) {
            $names_of_required_fields = str_getcsv($line, ";");
        } else {
            $names_of_required_fields = str_getcsv(substr($line, 0, $space_pos - 1), ";");
            $names_of_optional_fields = str_getcsv(substr($line, $space_pos + 1), ";");
        }
        $header_field_names = array(
            "REQUIRED" => $names_of_required_fields,
            "OPTIONAL" => $names_of_optional_fields
        );
        return $header_field_names;
    }

    /* readCSV - read CSV formatted file and delete all comment lines
     *
     * Argument: $csvfile - CSV formatted file
     *           $header_exists - TRUE if the file has a line of field names
     * Returns: NULL for empty file or an array(headers, content)
     */

    function readCSV($csvfile, $header_exists = TRUE) {
        if (!file_exists($csvfile) || !is_readable($csvfile))
            return FALSE;

        $raw_lines = file($csvfile);

        $header_found = FALSE;
        $header_field_names = array();
        $rows_of_field_values = array();
        $file_handle = fopen($csvfile, 'r');
        foreach ($raw_lines as $raw_line) {
            $line = trim($raw_line);
            if (substr($line, 0, 1) == "#") // ignore comment lines
                continue;
            if (!$header_exists || $header_found) {
                $field_values = str_getcsv($line, ";", '"');
                $rows_of_field_values[] = $field_values;
            } else { // csv header found
                $header_found = TRUE;
                $header_field_names = $this->analyze_header($line);
            }
        }
        return array(
            "KEYS" => $header_field_names,
            "VALUES" => $rows_of_field_values
        );
    }

    /*
     * parse_file - tokenize contents of csv file
     *
     * This functions, unlike the older version of this function does not do any semantic checking of input.
     * However, it does separate the csv header into keys and the rest of the file into an array of rows of 
     * arrays of values.
     *
     * Returns:
     */

    function parse_file($file, $session) {
//print("<pre>[".__FILE__." @ ".__LINE__."]: this->line_separator='".$this->line_separator."'<br/></pre>");
//print("<pre>[".__FILE__." @ ".__LINE__."]: this->field_separator='".$this->field_separator."'<br/></pre>");
        $data = array();
        $data['error'] = array();
        $content = array();
        $result = array();

        $rows = $this->readCSV($file);
        $rows_count = count($rows);
        $keys_array = $rows['KEYS'];
        $keys_count = count($keys_array);
        $raw_values_array = $rows["VALUES"];
        $values_array = array();

        foreach ($raw_values_array as $row) {
            $values_array[] = $this->escape_string($row);
        }
//print("<pre>[".__FILE__." @ ".__LINE__."]: values_array=<br/>");print_r($values_array);print("</pre>\n");
        list($csv_header, $rekeyed_values) = $this->relate_field_names_to_field_values($keys_array, $values_array, $session);

//print("<pre>[".__FILE__." @ ".__LINE__."]: data=<br/>");print_r($data);print("</pre>\n");
        return array($csv_header, $rekeyed_values);
    }

    function escape_string($data) {
        $result = array();
        foreach ($data as $row) {
            $result[] = str_replace('"', '', $row);
        }
        return $result;
    }

}
?>

