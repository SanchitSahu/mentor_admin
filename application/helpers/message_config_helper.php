<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 *  Custome Validation message
 */
define('INVALID_LOGIN', 'Please enter correct Email and Password');

define('INACTIVE_MESSAGE', 'Your account is inactive.');
define('DELETE_MESSAGE', 'Your account is suspended. Please contact site administrator');
define('BLOCKED_MESSAGE', 'Your account is suspended. Please contact site administrator');

define('USER_DATA_NOT_AVAILABLE', 'User data not available');

// get_message(MESSAGE, param);
define('SUCCESS_CREATED', 'You have successfully created %s.');
define('SUCCESS_UPDATED', 'You have successfully updated %s.');
define('SUCCESS_STATUS', 'You have successfully updated status %s.');
define('SUCCESS_DELETED', 'You have successfully deleted %s.');

define('SUCCESS_CREATED_WITH_EMAIL', 'You have successfully created user and an email has been sent to user.');
define('EMAIL_ERROR', 'You have successfully created user but due to some problem an email could not send to user.');

define('RECORD_NOT_AVAILABLE', 'Record not available');
define('ISSUE_WITH_DATA', 'Oops! there might be some issue with data. Please try again');

//define('SUCCESS_UPDATED', 'You have successfully updated %s.');

define('UNIQUE_EMAIL_MESSAGE', 'This email address is already registered.');

define('PASS_CHANGED_SUCC_MESSAGE', 'Your login password has been changed successfully!');
define('PASS_CHANGED_FAIL_MESSAGE', 'Your login password has been not changed successfully!');
define('NOT_AVAILABLE', 'Not Available');