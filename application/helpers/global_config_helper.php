<?php

/**
 * 
 * 
 * Print array
 * 
 * @access	public
 * @param	array
 * @param	boolean
 * @return	none
 */
if (!function_exists('get_dropdown_list')) {

    function get_dropdown_list($listArr, $key = 'Id', $value = 'Name', $first_option = '', $second_option = array()) {
        $resultDdl = array();
        if ($first_option) {
            $resultDdl[''] = $first_option;
        }
        if ($second_option) {
            foreach ($second_option as $k => $v) {
                $resultDdl[$k] = $v;
            }
        }
        if (is_array($listArr) && !empty($listArr)) {

            foreach ($listArr as $val) {
                if (is_array($val)) {
                    if ($key) {
                        $resultDdl[$val[$key]] = $val[$value];
                    } else {
                        $resultDdl[] = $val[$value];
                    }
                } else {
                    // if single dimension array
                    $resultDdl[$val] = $val;
                }
            }
            unset($listArr);
        }
        return $resultDdl;
    }

}
if (!function_exists('pr')) {

    function pr($array, $default = FALSE) {
        echo '<pre>';
        print_r($array);
        echo '</pre>';
        if ($default)
            exit;
    }

}
// ------------------------------------------------------------------------
/**
 * admin_logedin
 * 
 * check admin login or not
 * 
 * @access	public
 * 
 * @return	boolean 
 */
if (!function_exists('admin_logedin')) {

    function admin_logedin() {
        $ci = & get_instance();
        // We need to use $ci->session instead of $this->session
        $user = $ci->session->userdata('admin_data');
        if (empty($user)) {
            return false;
        } else {
            return true;
        }
     
    }

}

// ------------------------------------------------------------------------

/**
 * Admin URL
 * 
 * @access	public
 * @param       string
 * @return	string
 */
if (!function_exists('admin_url')) {

    function admin_url($uri = '') {
        $ci = & get_instance();
        return $ci->config->item('admin_url');
    }

}

// ------------------------------------------------------------------------

/**
 * Enterprise URL
 *
 * @access	public
 * @param       string
 * @return	string
 */
if (!function_exists('enterprise_url')) {

    function enterprise_url($uri = '') {
        $ci = & get_instance();
        return $ci->config->item('enterprise_url') . $uri;
    }

}

// ------------------------------------------------------------------------

/**
 * Staff URL
 *
 * @access	public
 * @param       string
 * @return	string
 */
if (!function_exists('staff_url')) {

    function staff_url($uri = '') {
        $ci = & get_instance();
        return $ci->config->item('staff_url') . $uri;
    }

}

// ------------------------------------------------------------------------

/**
 * Root path
 * 
 * @access	public
 * @param       string
 * @return	string
 */
if (!function_exists('root_path')) {

    function root_path($uri = '') {
        $ci = & get_instance();
        return $ci->config->item('root_path');
    }

}

// ------------------------------------------------------------------------

/**
 * get current date
 */
if (!function_exists('get_date')) {

    function get_date() {

        return get_curr_datetime();
        //return date('Y-m-d H:i:s');
        //$date = new DateTime('', new DateTimeZone(TIME_ZONE));
        //return $date->format('Y-m-d H:i:s');
    }

}

// ------------------------------------------------------------------------

if (!function_exists('get_display_date')) {

    function get_display_date($date = '', $format = DATE_FORMAT_DD_MM_YYYY) {
        return date($format, strtotime($date));
    }

}

// ------------------------------------------------------------------------

/**
 * return user status list
 * 
 * @access	public
 * @return	array
 */
if (!function_exists('get_user_status_list')) {

    function get_user_status_list() {
        return array(
            'UnVerified' => 'UnVerified',
            'Verified' => 'Verified',
            'Deleted' => 'Deleted',
            'Inactive' => 'Inactive',
            'Blocked' => 'Blocked'
        );
    }

}

// ------------------------------------------------------------------------

/**
 * return random string
 * 
 * @access	public
 * @param       integer $length length of string which will be generate
 * @return      string
 */
if (!function_exists('get_random_string')) {

    function get_random_string($length = 16) {
        $original_string = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        $original_string = implode("", $original_string);
        return substr(str_shuffle($original_string), 0, $length);
    }

}

// ------------------------------------------------------------------------

/**
 * return md5 encrypted string
 * 
 * @access	public
 * @param       string 
 * @return      string
 */
if (!function_exists('get_encyp_string')) {

    function get_encyp_string($str = '') {
        return md5($str);
    }

}

// ------------------------------------------------------------------------

/**
 * return base64_encode encrypted string
 * 
 * @access	public
 * @param       string 
 * @return      string
 */
if (!function_exists('encode_string')) {

    function encode_string($str = '') {
        return base64_encode($str);
    }

}

// ------------------------------------------------------------------------

/**
 * return base64_decode decrypted string
 * 
 * @access	public
 * @param       string 
 * @return      string
 */
if (!function_exists('decode_string')) {

    function decode_string($str = '') {
        return base64_decode($str);
    }

}

// ------------------------------------------------------------------------

/**
 * return urldecode string
 * 
 * @access	public
 * @param       string 
 * @return      string
 */
if (!function_exists('urldecode_string')) {

    function urldecode_string($str = '') {
        return urldecode($str);
    }

}

// ------------------------------------------------------------------------

/**
 * return urlencode string
 * 
 * @access	public
 * @param       string 
 * @return      string
 */
if (!function_exists('urlencode_string')) {

    function urlencode_string($str = '') {
        return urlencode($str);
    }

}

// ------------------------------------------------------------------------

/**
 * return message 
 * 
 * @access	public
 * @param       string          $message
 * @param       string / array  $param
 * @return      string          resulting string
 */
if (!function_exists('get_message')) {

    function get_message($message = '', $param = '') {
        return sprintf($message, $param);
    }

}


// ------------------------------------------------------------------------

/**
 * upload image
 * 
 * @access	public
 * 
 * @param string $htmlFieldName File field tag name
 * @param string $path Upload directory path
 * @param int $maxSize Maximum file size
 * 
 * @return array upload_data
 * @return boolean status 
 * @return boolean error (if exists)
 */
if (!function_exists('do_upload')) {

    function do_upload($htmlFieldName, $path, $filename, $maxSize = '1000', $maxWidth = '1920', $maxHeight = '1280') {
        $CI = & get_instance();


        $config['file_name'] = $filename;
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';

        $config['max_size'] = $maxSize;
        $config['max_width'] = $maxWidth;
        $config['max_height'] = $maxHeight;
        // $config['min_width'] = $minWidth;
        // $config['min_height'] = $minHeight;


        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);
        unset($config);
        if (!$CI->upload->do_upload($htmlFieldName)) {
            return array('error' => $CI->upload->display_errors(), 'status' => 0);
        } else {
            return array('status' => 1, 'upload_data' => $CI->upload->data());
        }
    }
    

}

/**
 * creates thumbnail image
 * 
 * @access	public
 * @param string $sourcePath Uploaded Image path
 * @param string $desPath Path for thumbnail image
 * @param int $width
 * @param int $height
 * 
 * @return  boolean
 */
if (!function_exists('resize_image')) {

    function resize_image($sourcePath, $desPath, $width = '100', $height = '100', $maintain_ratio = FALSE) {
        $CI = & get_instance();
        $CI->load->library('image_lib');
        $CI->image_lib->clear();
        $config['image_library'] = 'gd2';
        $config['source_image'] = $sourcePath;
        $config['new_image'] = $desPath;
        $config['quality'] = '100%';
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = $maintain_ratio;
        $config['thumb_marker'] = '';
        $config['width'] = $width;
        $config['height'] = $height;
        $config['overwrite'] = TRUE;
        $config['quality'] = '100';
        $CI->image_lib->initialize($config);

        if ($CI->image_lib->resize()) {
            return true;
        } else {
            echo $CI->image_lib->display_errors();
        }
        return false;
    }

}


// ------------------------------------------------------------------------

/**
 * return status
 * 
 * @access	public
 * @return	array
 */
if (!function_exists('get_status')) {

    function get_status() {
        return array(
            '1' => 'Active',
            '0' => 'Inactive'
        );
    }

}

// ------------------------------------------------------------------------

/**
 * Anchor void Link
 *
 * Creates an anchor based on the local URL.
 *
 * @access	public
 * @param	string	the link title
 * @param	mixed	any attributes
 * @return	string
 */
if (!function_exists('anchor_void')) {

    function anchor_void($title = '', $attributes = '') {
        $title = (string) $title;

        if ($title == '') {
            $title = $site_url;
        }

        if ($attributes != '') {
            $attributes = _parse_attributes($attributes);
        }

        return '<a href="javascript:void" ' . $attributes . '>' . $title . '</a>';
    }

}


// -----------------------------------------------------------------------

/**
 *  set email configuration
 * 
 * @access	public
 * @param	array 
 * @return	boolean
 */
if (!function_exists('send_mail')) {
	function send_mail($data) {
        if($data['toEmail'] == '') {
            return true;
        }
        $ci = & get_instance();

        #$ci->load->library('email', $ci->config->item('smtp_data'));
        $ci->load->library('email');
		$ci->email->set_mailtype("html");
		$ci->email->set_newline("\r\n");

        if (isset($data['fromEmail']) && !empty($data['fromEmail'])) {
            $ci->email->from($data['fromEmail'], 'MELStm');
        } else {
            //info.melsapp@gmail.com
            $ci->email->from('noreply@melstm.net', 'MELStm');
        }
        $ci->email->to($data['toEmail']);
		if(isset($data['cc']))
			$ci->email->cc($data['cc']);
		$ci->email->bcc('reg.charney+mels@entrebahn.com');
        //$ci->email->bcc('sanchit.sahu03@gmail.com');

        $ci->email->subject($data['subject']);
        //$ci->email->message($data['body']);
        $msg = $data['body'];
        
        //$img = "http://melstm.net/mentor/assets/admin/images/qrCode.png";
        //$msg .= "<img src=".$img." height='300px' width='300px'>";
        //$ci->email->attach($img);
        
        $ci->email->message($msg);

        if ($ci->email->send()) {
            return true;
        } else {
            return false;
        }
    }

}

// -----------------------------------------------------------------------
/**
 *  set email configuration
 * 
 * @access	public
 * @param	array 
 * @return	array
 */
if (!function_exists('send_activation_mail')) {

    function send_activation_mail($data) {
        $ci = & get_instance();

        $ci->load->model('site_emails_model');

        $activationLink = '<a href="' . base_url() . 'signup/user-activation/' . $data['ActivationCode'] . '">' . base_url() . 'signup/user-activation/' . $data['ActivationCode'] . '</a>';

        try {
            $emailTemplate = $ci->site_emails_model->get_email_by_key('REGISTRATION_ACTIVATION_EMAIL');

            $arrSearch = array('##FIRST_NAME##', '##ACTIVATION_LINK##');
            $arrReplace = array($data['FirstName'] . ' ' . $data['LastName'], $activationLink);
            $body = str_replace($arrSearch, $arrReplace, $emailTemplate['Body']);

            $emailData['toEmail'] = $data['Email'];
            $emailData['subject'] = $emailTemplate['Subject'];
            $emailData['body'] = $body;

            return send_mail($emailData);
        } catch (Exception $e) {
            
        }
    }

}
// ----------------------------------------------------------------------
/**
 * return encrypted string
 * 
 * @access	public
 * @param       string $string to be encrypted
 * @param       string $key used to encrypt string
 * @return      string
 */
if (!function_exists('encrypt')) {

    function encrypt($string, $key) {
        $result = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) + ord($keychar));
            $result.=$char;
        }
        return base64_encode($result);
    }

}

// ----------------------------------------------------------------------
/**
 * return decrypted string
 * 
 * @access	public
 * @param       string $string to be decrypted
 * @param       string $key used to decrypt string
 * @return      string
 */
if (!function_exists('decrypt')) {

    function decrypt($string, $key) {
        $result = '';
        $string = base64_decode($string);
        for ($i = 0; $i < strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) - ord($keychar));
            $result.=$char;
        }
        return $result;
    }

}

// ----------------------------------------------------------------------
/**
 * return get_custom_result
 * 
 * @access	public
 * @param       string 
 * @return      string
 */
if (!function_exists('get_custom_result')) {

    function get_custom_result($return = '', $field = '*', $table = '', $condition = array(), $limit = '') {
        $ci = & get_instance();
        $result = '';
        if ($table) {
            $ci->db->select($field)->from($table);

            if ($condition)
                $ci->db->where($condition);

            if ($limit)
                $ci->db->limit($limit);

            $query = $ci->db->get();

            if ($return == 'COUNT') {
                $result = $query->num_rows();
            } else if ($return == 'ARRAY') {
                $result = $query->result_array();
            }
        }

        return $result;
    }

}



// ----------------------------------------------------------------------

/**
 * return get_image
 * 
 * @access	public
 * @param       string $url
 * @param       string $type
 * @return      string
 */
if (!function_exists('get_image')) {

    function get_image($url = '', $type = '') {
        $ci = & get_instance();
        $image = '';

        switch ($type) {
            case 'user':
                if ($url && file_exists(root_path() . DIRECTORY_SEPARATOR . USER_UPLOAD_THUMB . '/' . $url)) {
                    $image = base_url() . USER_UPLOAD_THUMB . '/' . $url;
                } else {
                    $image = base_url() . FRONTEND_ASSETS . 'images/no-profile-image.jpg';
                }
                break;
            case 'category':
                if ($url && file_exists(root_path() . DIRECTORY_SEPARATOR . CAT_UPLOAD_THUMB . '/' . $url)) {
                    $image = base_url() . CAT_UPLOAD_THUMB . '/' . $url;
                } else {
                    $image = base_url() . FRONTEND_ASSETS . 'images/no-photo.png';
                }
                break;
            default :
        }
        return $image;
    }

}

// ------------------------------------------------------------------------

/**
 * country dropdown list
 *
 * @access	public
 * @return      country list
 */
if (!function_exists('country_dp')) {

    function country_dp() {
        $ci = & get_instance();

        $ci->db->select('Name');
        $ci->db->order_by('Name ASC');
        $query = $ci->db->get(COUNTRIES);

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $value) {
                $data[] = $value['Name'];
            }
        }
        return json_encode($data);
    }

}

// ------------------------------------------------------------------------

/**
 * state dropdown list
 *
 * @access	public
 * @return      country list
 */
if (!function_exists('state_dp')) {

    function state_dp() {
        $ci = & get_instance();

        $ci->db->select('Name');
        $ci->db->distinct();
        $ci->db->order_by('Name ASC');
        $query = $ci->db->get(STATES);

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $value) {
                $data[] = $value['Name'];
            }
        }
        return json_encode($data);
    }

}

/**
 * city dropdown list
 *
 * @access	public
 * @return      country list
 */
if (!function_exists('city_dp')) {

    function city_dp() {
        $ci = & get_instance();

        $ci->db->select('Name');
        $ci->db->distinct();
        $ci->db->order_by('Name ASC');
        $query = $ci->db->get(CITIES);

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $value) {
                $data[] = $value['Name'];
            }
        }
        return json_encode($data);
    }

}

// ----------------------------------------------------------------------

/**
 * return user total points
 * 
 * @access	public     
 * @return      points
 */
if (!function_exists('get_user_points')) {

    function get_user_points() {
        $ci = & get_instance();

        $ci->db->where('Id', $ci->session->userdata('user_id'));

        $query = $ci->db->get(USERS);

        $result = $query->row_array();
        return (isset($result['TotalPoints']) && $result['TotalPoints']) ? $result['TotalPoints'] : '0';
    }

}

/**
 * count of reviewd product
 *
 * @access		public
 * @param       integer $productId
 * @return      integer
 */
if (!function_exists('view_prod_review_count')) {

    function view_prod_review_count($productId) {
        $ci = & get_instance();

        $ci->db->where('ProductId', $productId);
        $query = $ci->db->get(PRODUCT_X_REVIEWS);

        return $query->num_rows();
    }

}


/**
 * Upload image from base64 string
 */
if (!function_exists('upload_base64_image')) {

    function upload_base64_image($base, $uploadPath, $filename) {
        $binary = base64_decode($base);
        $source_img = imagecreatefromstring($binary);
        imagesavealpha($source_img, true);
        imagepng($source_img, $uploadPath . $filename);
        //$imageSave = imagepng($rotated_img, $uploadPath.$filename, 10);
        if (imagedestroy($source_img)) {
            return true;
        }

        /* header('Content-Type: bitmap; charset=utf-8');
          $file = fopen($uploadPath.$filename, 'wb');
          fwrite($file, $binary);
          if(fclose($file)){
          return true;
          } */
        return false;
    }

}


// ----------------------------------------------------------------------


if (!function_exists('check_page_access')) {

    function check_page_access($user_type) {
        $ci = & get_instance();
        if ($ci->session->userdata('front_logged_in')) {
            if ($ci->session->userdata('user_type') != $user_type) {
                redirect(base_url("signin"));
            }
        } else {
            redirect(base_url("signin"));
        }
    }

}

if (!function_exists('add_quotes')) {

    function add_quotes($str) {
        return sprintf('"%s"', $str);
    }

}


if (!function_exists('check_access_allowed')) {

    function check_access_allowed($ConfigKey, $ConfigVal) {
        $ci = & get_instance();
        $ci->load->model('settings_model');

        if ($ConfigKey) {
            $result = $ci->settings_model->get_settings('first', array('s.ConfigKey' => $ConfigKey, 's.ConfigVal' => $ConfigVal));

            if (isset($result) && !empty($result)) {
                return true;
            }
        }
        return false;
    }

}


// ----------------------------------------------------------------------

/**
 * return existing field record id of the table
 * 
 * @access	public     
 * @param string $table table name
 * @param string $where condition
 * @param string $field field name which will be returned
 * @return (string OR integer)
 */
if (!function_exists('check_exists')) {

    function check_exists($table, $where = '', $field = '') {
        $ci = & get_instance();
        if ($table && $where) {
            $query = $ci->db->query("SELECT * FROM {$table} WHERE {$where}");

            if ($query->num_rows()) {
                $data = $query->row_array();
                if ($field) {
                    return $data[$field];
                } else {
                    return true;
                }
                exit;
            }
        }
        return false;
    }

}

if (!function_exists('fnEncrypt')) {

    function fnEncrypt($sValue, $sSecretKey) {
        return rtrim(
                base64_encode(
                        mcrypt_encrypt(
                                MCRYPT_RIJNDAEL_256, $sSecretKey, $sValue, MCRYPT_MODE_ECB, mcrypt_create_iv(
                                        mcrypt_get_iv_size(
                                                MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB
                                        ), MCRYPT_RAND)
                        )
                ), "\0"
        );
    }

}

if (!function_exists('fnDecrypt')) {

    function fnDecrypt($sValue, $sSecretKey) {
        return rtrim(
                mcrypt_decrypt(
                        MCRYPT_RIJNDAEL_256, $sSecretKey, base64_decode($sValue), MCRYPT_MODE_ECB, mcrypt_create_iv(
                                mcrypt_get_iv_size(
                                        MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB
                                ), MCRYPT_RAND
                        )
                ), "\0"
        );
    }

}

/*
  Resizes an image and converts it to PNG returning the PNG data as a string
 */
if (!function_exists('imageToPng')) {

    function imageToPng($srcFile, $destFile, $maxSize = 100) {
        list($width_orig, $height_orig, $type) = getimagesize($srcFile);

        // Get the aspect ratio
        $ratio_orig = $width_orig / $height_orig;

        $width = $maxSize;
        $height = $maxSize;

        // resize to height (orig is portrait) 
        if ($ratio_orig < 1) {
            $width = $height * $ratio_orig;
        }
        // resize to width (orig is landscape)
        else {
            $height = $width / $ratio_orig;
        }

        // Temporarily increase the memory limit to allow for larger images
        ini_set('memory_limit', '32M');

        switch ($type) {
            case IMAGETYPE_GIF:
                $image = imagecreatefromgif($srcFile);
                break;
            case IMAGETYPE_JPEG:
                $image = imagecreatefromjpeg($srcFile);
                break;
            case IMAGETYPE_PNG:
                $image = imagecreatefrompng($srcFile);
                break;
            default:
                throw new Exception('Unrecognized image type ' . $type);
        }

        // create a new blank image
        $newImage = imagecreatetruecolor($width, $height);

        // Copy the old image to the new image
        imagecopyresampled($newImage, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        // Output to a temp file
        // $destFile = tempnam();
        imagepng($newImage, $destFile);

        // Free memory                           
        imagedestroy($newImage);

        if (is_file($destFile)) {
            /*  $f = fopen($destFile, 'rb');   
              $data = fread($f);
              fclose($f);

              // Remove the tempfile
              unlink($destFile);
              return $data;
             * 
             */
            return true;
        }

        throw new Exception('Image conversion failed.');
    }

}

if (!function_exists('imagecrop')) {

    function imagecrop($img_name, $newname, $modwidth, $modheight) {
        $info = getimagesize($img_name);
        if (!empty($info)) {
            $width = $info[0];
            $height = $info[1];
            $type = $info['mime'];
        }

        // list($width, $height) = getimagesize($img_name) ; //get width & height in array list

        $tn = imagecreatetruecolor($modwidth, $modheight);
        if (!strcmp("image/png", $type)) {
            imagealphablending($tn, false); //For transparent BackGround
            imagesavealpha($tn, true);
        }

        if (!strcmp("image/jpg", $type) || !strcmp("image/jpeg", $type) || !strcmp("image/pjpeg", $type))
            $src_img = imagecreatefromjpeg($img_name);

        if (!strcmp("image/png", $type))
            $src_img = imagecreatefrompng($img_name);

        if (!strcmp("image/gif", $type))
            $src_img = imagecreatefromgif($img_name);

        imagecopyresampled($tn, $src_img, 0, 0, 0, 0, $modwidth, $modheight, $width, $height);

        if (!strcmp("image/png", $type)) {
            imagesavealpha($src_img, true);
            $ok = imagepng($tn, $newname);
        } else if (!strcmp("image/gif", $type)) {
            $ok = imagegif($tn, $newname);
        } else {
            $ok = imagejpeg($tn, $newname);
        }
        echo $ok;
        exit;

        if ($ok == 1) {
            return true;
        } else {
            return false;
        }
    }

}

if (!function_exists('resize_png')) {

    function resize_png($src, $dst, $dstw, $dsth) {
        list($width, $height, $type, $attr) = getimagesize($src);
        $im = imagecreatefrompng($src);
        $tim = imagecreatetruecolor($dstw, $dsth);
        imagecopyresampled($tim, $im, 0, 0, 0, 0, $dstw, $dsth, $width, $height);
        $tim = ImageTrueColorToPalette2($tim, false, 255);
        imagepng($tim, $dst);
    }

}


/**
 * Add pushnotification
 */
if (!function_exists('add_pushnotification')) {

    function add_pushnotification($type, $msg, $sendto = 0, $otherparam = array(), $device = 'Both') {
        $ci = & get_instance();

        // notification will not add for self activity
        if (isset($otherparam['user_id']) && $otherparam['user_id'] == $sendto) {
            return false;
            exit;
        }

        if ($type) {
            $data = array();
            $data['NotificationType'] = $type;
            $data['Message'] = $msg;
            $data['DeviceType'] = $device;
            $data['NotificationSend'] = 'No';
            $data['SendToUserId'] = $sendto;
            $data['OtherPararm'] = (isset($otherparam) && !empty($otherparam)) ? json_encode($otherparam) : '';
            $data['CreatedDateTime'] = get_date();


            $ci->db->select('Id');
            $ci->db->from(PUSH_NOTIFICATION);
            $ci->db->where(array('NotificationType' => $type, 'Message' => $msg, 'DeviceType' => $device, 'OtherPararm' => '', 'SendToUserId <= ' => 0));
            $query = $ci->db->get();

            if ($query->num_rows() > 0) {
                $PushData = $query->row_array();
                if (isset($PushData) && !empty($PushData)) {
                    if (grid_data_updates($data, PUSH_NOTIFICATION, 'Id', $PushData['Id'])) {
                        return true;
                    }
                } else {
                    if (grid_add_data($data, PUSH_NOTIFICATION)) {
                        return true;
                    }
                }
            } else {
                if (grid_add_data($data, PUSH_NOTIFICATION)) {
                    return true;
                }
            }
        }
        return false;
    }

}

/**
 * Dropdown list
 *
 * Creates dropdown list.
 *
 * @access	public
 * @param	string	the link title
 * @param	mixed	any attributes
 * @return	string
 */
if (!function_exists('get_dropdown_list')) {

    function get_dropdown_list($listArr, $key = 'Id', $value = 'Name', $first_option = '', $second_option = array()) {
        $resultDdl = array();
        if ($first_option) {
            $resultDdl[''] = $first_option;
        }
        if ($second_option) {
            foreach ($second_option as $k => $v) {
                $resultDdl[$k] = $v;
            }
        }
        if (is_array($listArr) && !empty($listArr)) {

            foreach ($listArr as $val) {
                if (is_array($val)) {
                    if ($key) {
                        $resultDdl[$val[$key]] = $val[$value];
                    } else {
                        $resultDdl[] = $val[$value];
                    }
                } else {
                    // if single dimension array
                    $resultDdl[$val] = $val;
                }
            }
            unset($listArr);
        }
        return $resultDdl;
    }

}

if (!function_exists('fnEncrypt')) {

    function fnEncrypt($sValue, $sSecretKey) {
        return rtrim(
                base64_encode(
                        mcrypt_encrypt(
                                MCRYPT_RIJNDAEL_256, $sSecretKey, $sValue, MCRYPT_MODE_ECB, mcrypt_create_iv(
                                        mcrypt_get_iv_size(
                                                MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB
                                        ), MCRYPT_RAND)
                        )
                ), "\0"
        );
    }

}

if (!function_exists('fnDecrypt')) {

    function fnDecrypt($sValue, $sSecretKey) {
        return rtrim(
                mcrypt_decrypt(
                        MCRYPT_RIJNDAEL_256, $sSecretKey, base64_decode($sValue), MCRYPT_MODE_ECB, mcrypt_create_iv(
                                mcrypt_get_iv_size(
                                        MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB
                                ), MCRYPT_RAND
                        )
                ), "\0"
        );
    }

}

if (!function_exists('sendPushNotificationToGCM')) {

    //generic php function to send GCM push notification
    function sendPushNotificationToGCM($registatoin_ids, $message) {
        //Google cloud messaging GCM-API url
        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
            'registration_ids' => array($registatoin_ids),
            'data' => array("message" => $message)
        );
        // Google Cloud Messaging GCM API Key
        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

}

if (!function_exists('getLeftMenuCount')) {
    function getLeftMenuCount(){
        $ci = & get_instance();

        $query = $ci->db->query('SELECT 
            (SELECT COUNT(*) FROM mentor_meetingtype WHERE `Status` !=0) AS meetingTypeCount,
            (SELECT COUNT(*) FROM mentor_meetingplace WHERE `Status` !=0) AS meetingPlaceCount,
            
            (SELECT COUNT(*) FROM mentor_topic WHERE `Status` !=0) AS topicCount,
            (SELECT COUNT(*) FROM mentor_subtopic WHERE `Status` !=0) AS subTopicCount,
            
            (SELECT COUNT(*) FROM mentor_mentordefinedactions WHERE `Status` !=0) AS mentorActionsCount,
            (SELECT COUNT(*) FROM mentor_menteedefinedactions WHERE `Status` !=0) AS menteeActionsCount,
            
            (SELECT COUNT(*) FROM mentor_skill WHERE `Status` !=0) AS skillCount,
            (SELECT COUNT(*) FROM mentor_careCriteria WHERE `Status` !=0) AS careCriteriaCount,
            
            (SELECT COUNT(*) FROM mentor_mentor WHERE `Status` !=0) AS mentorCount,
            (SELECT COUNT(*) FROM mentor_user_roles WHERE `Status` !=0) AS mentorRoleCount,
            (SELECT COUNT(*) FROM mentor_mentorsourcenames WHERE `Status` !=0) AS mentorSourceCount,
            (SELECT COUNT(DISTINCT MentorID) FROM `mentor_mentorskillset` WHERE Status!=0) AS mentorSkillsCount,
            
            (SELECT COUNT(*) FROM mentor_mentee WHERE `Status` !=0) AS menteeCount,
            (SELECT COUNT(*) FROM mentor_menteesourcenames WHERE `Status` !=0) AS menteeSourceCount,
            (SELECT COUNT(*) FROM mentor_menteeprogrammes WHERE `Status` !=0) AS menteeProgrammesCount,
            (SELECT COUNT(DISTINCT MenteeID) FROM mentor_menteeskill WHERE `Status` !=0) AS menteeSkillsCount,
			
			(SELECT COUNT(*) FROM mentor_accept_responses WHERE `Status` !=0) AS acceptCommentCount,
            (SELECT COUNT(*) FROM mentor_decline_responses WHERE `Status` !=0) AS rejectCommentCount,
            (SELECT COUNT(*) FROM mentor_abbreviations where `Status` !=0) AS abbreviationsCount,
            (SELECT COUNT(*) FROM mentor_errors where `Status` !=0) AS errorsCount
			
        ');
        return $query->result();
    }
}


if (!function_exists('checkSampleFileExists')) {

    function checkSampleFileExists($file) {
        $ci = & get_instance();
        $file_path = $_SERVER['DOCUMENT_ROOT'].'/mentor/assets/admin/csv/'.$file;
        if(file_exists($file_path)){
            return true;
        } else {
            return "Sample file not found";
        }
    }
}


if (!function_exists('loginCheck')) {

    function loginCheck() {
        $ci = & get_instance();
        // We need to use $ci->session instead of $this->session
        $user = $ci->session->userdata('admin_data');
        if (empty($user)) {
            return false;
        } else {
            return true;
        }
     
    }

}

if (!function_exists('randomString')) {
	function randomString($length = 6){
		//set the random id length 
		$random_id_length = $length; 
		
		//generate a random id encrypt it and store it in $rnd_id 
		$rnd_id = crypt(uniqid(rand(),1)); 
		
		//to remove any slashes that might have come 
		$rnd_id = strip_tags(stripslashes($rnd_id)); 
		
		//Removing any . or / and reversing the string 
		$rnd_id = str_replace(".","",$rnd_id); 
		$rnd_id = strrev(str_replace("/","",$rnd_id)); 
		
		//finally I take the first 10 characters from the $rnd_id 
		$rnd_id = substr($rnd_id,0,$random_id_length); 
		
		return $rnd_id;
	}
}

if (!function_exists('getDatabaseTable')) {
	function getDatabaseTable(){
		$arr = array(
                "mentor_careCriteria",
                "mentor_emailtemplate",
                "mentor_meetingtype",
                "mentor_meetingplace",
                "mentor_menteeprogrammes",
                "mentor_mentorsourcenames",
                "mentor_reminder_settings",
                "mentor_user_roles",
                "mentor_skill",
                "mentor_subdomains",
                "mentor_mentordefinedactions",
                "mentor_menteedefinedactions"
        );
		
		return $arr;
	}
}

if (!function_exists('checkDomain')) {
function checkDomain()
{
	$parts=explode('.', $_SERVER["SERVER_NAME"]);
	$subdomain	=	$parts[0];
	
	$CI =& get_instance();
	$CI->load->model('admin/login_model');
	$res	=	$CI->login_model->checkDomain($subdomain);
	if($res == true)return true;
	else return false;
	
}
}


if (!function_exists('loadDatabase')) {
function loadDatabase()
{
	$host = $_SERVER['HTTP_HOST'];
		
		$subdomain = strstr(str_replace("www.","",$host), ".",true);
		$CI =& get_instance();
		$CI->load->model('admin/bulkupload_model');
		$res = $CI->bulkupload_model->checkSubdomain($subdomain);
		//print_r($res);exit;
        //echo $subdomain;exit;
		if($subdomain == "melstm"){
			$dbName = 'c10melstm_net_template';
			$ci =& get_instance();
			$ci->load->helper('dynamicdb');
			$config_app = switch_db_dinamicomain($dbName);
			return $config_app;
		}else if(!empty($res)){
			$dbName = $subdomain;
			$ci =& get_instance();
			$ci->load->helper('dynamicdb');
			$config_app = switch_db_dinamico($dbName);
			//$ci->load->database($config_app,TRUE);
			//print_r($config_app);exit;
			//echo "df";exit;
			return $config_app;
			
		}else{
			return 0;// "No subdomain";exit;
		}
	
	
}
}

if (!function_exists('loadDatabaseFromSchoolName')) {

    function loadDatabaseFromSchoolName($schoolName) {
        $CI =& get_instance();
        $CI->load->model('admin/bulkupload_model');
        $res = $CI->bulkupload_model->checkSubdomain($schoolName);
        if (!empty($res)) {
            $dbName = $schoolName;
            $ci = & get_instance();
            $ci->load->helper('dynamicdb');
            $config_app = switch_db_dinamico($dbName);
            return $config_app;
        } else {
            return 0;
        }
    }

}

if (!function_exists('checkAbbreviations')) {
function checkAbbreviations($peram){
		$CI =& get_instance();
		$checkName	=	$CI->db->query("SELECT * FROM mentor_abbreviations WHERE `AbbreviationKey`='$peram'");
		$query	=	 $checkName->result();
		if($checkName->num_rows() > 0){
			$peram	=	$query[0]->AbbreviationFullform;
		}else{
			$peram	=	$peram;
		}
		return $peram;
	}
}
