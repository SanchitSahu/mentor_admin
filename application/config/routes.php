<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

$route['admin'] = "admin/index";
$route['admin/coupon'] = "admin/coupon/index";
$route['admin/logout'] = "admin/index/logout";
$route['admin/forgotpassword'] = "admin/index/forgotpassword";
$route['admin/logout_admin'] = "admin/index/logout_admin";

$route['admin/skill'] = "admin/skill/index";
$route['admin/skill/add'] = "admin/skill/add";
$route['admin/skill/edit/(:int)'] = "admin/skill/edit";

$route['admin/meetingtype'] = "admin/meetingtype/index";
$route['admin/meetingtype/add'] = "admin/meetingtype/add";
$route['admin/meetingtype/edit/(:int)'] = "admin/meetingtype/edit";

$route['admin/meetingplace'] = "admin/meetingplace/index";
$route['admin/meetingplace/add'] = "admin/meetingplace/add";
$route['admin/meetingplace/edit/(:int)'] = "admin/meetingplace/edit";

$route['admin/topic'] = "admin/topic/index";
$route['admin/topic/add'] = "admin/topic/add";
$route['admin/topic/edit/(:int)'] = "admin/topic/edit";

$route['admin/subtopic'] = "admin/subtopic/index";
$route['admin/subtopic/add'] = "admin/subtopic/add";
$route['admin/subtopic/edit/(:int)'] = "admin/subtopic/edit";

$route['admin/careweighting'] = "admin/careweighting/index";
$route['admin/careweighting/add'] = "admin/careweighting/add";
$route['admin/careweighting/edit/(:int)'] = "admin/careweighting/edit";

$route['admin/mentors'] = "admin/mentors/index";
$route['admin/mentors/add'] = "admin/mentors/add";
$route['admin/mentors/edit/(:int)'] = "admin/mentors/edit";

$route['admin/mentees'] = "admin/mentees/index";
$route['admin/mentees/add'] = "admin/mentees/add";
$route['admin/mentees/edit/(:int)'] = "admin/mentees/edit";

$route['admin/reports'] = "admin/reports/index";
//$route['admin/reports/mentor-report'] = "admin/reports/mentordetailreport";
//$route['admin/reports/mentordetailreport'] = "admin/reports/mentorreport";
//$route['admin/reports/mentee-report'] = "admin/reports/menteereport";
$route['admin/reports/global-report'] = "admin/reports/globalreport";

$route['addtocalendar/(:any)/(:num)'] = "addtocalendar/index/$1/$2";

$route['mentorService'] = "mentorService";
$route['admin/dashboard'] = "admin/admin/dashboard";

$route['default_controller'] = "admin/index";
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
