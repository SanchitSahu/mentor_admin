<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);	
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
define('DB_NAME',		'c10melstm_net_template');
define('DB_TEMPLATE',"c10melstm_net");

define('VERSION_NAME', 'Version 13.0 2017/05/08');

define("careCriteria", json_encode(array("REQUIRED" => array("Name"), "OPTIONAL" => array("CareValues", "Status", "Weight"))));
define("meetingplace", json_encode(array("REQUIRED" => array("MeetingPlaceName"), "OPTIONAL" => array("Status", "Weight"))));
define("meetingtype", json_encode(array("REQUIRED" => array("MeetingTypeName"), "OPTIONAL" => array("Status", "Weight"))));
define("menteeAction", json_encode(array("REQUIRED" => array("MenteeActionName"), "OPTIONAL" => array("Status", "Weight"))));
define("userProgramme", json_encode(array("REQUIRED" => array("ProgrammeName"), "OPTIONAL" => array("Status", "Weight"))));
define("menteeSource", json_encode(array("REQUIRED" => array("MenteeSourceName"), "OPTIONAL" => array("Status", "Weight"))));
define("mentorAction", json_encode(array("REQUIRED" => array("MentorActionName"), "OPTIONAL" => array("Status", "Weight"))));
define("userSource", json_encode(array("REQUIRED" => array("MentorSourceName"), "OPTIONAL" => array("Status", "Weight"))));
define("skill", json_encode(array("REQUIRED" => array("SkillName"), "OPTIONAL" => array("Status", "Weight"))));
define("topic", json_encode(array("REQUIRED" => array("TopicDescription"), "OPTIONAL" => array("Status", "Weight"))));
define("userRoles", json_encode(array("REQUIRED" => array("UserRoleName"), "OPTIONAL" => array("Status", "Weight"))));

define("acceptrejectComments", json_encode(array("REQUIRED" => array("ResponseName"), "OPTIONAL" => array("Status", "Weight"))));
define("abbreviations", json_encode(array("REQUIRED" => array("AbbreviationKey", "AbbreviationFullform"), "OPTIONAL" => array("Status", "Weight"))));


define("SubtopicHeader", json_encode(array("REQUIRED" => array("Topic", "SubTopicDescription"), "OPTIONAL" => array("Status", "Weight"))));

define("MentorHeader", json_encode(array("REQUIRED" => array("MentorName", "MentorEmail"), "OPTIONAL" => array("MentorRole", "MentorSource", "MentorPhone", "MentorImage", "MentorStatus", "Status", "Weight", "IsSubadmin"))));
define("MenteeHeader", json_encode(array("REQUIRED" => array("MenteeName", "MenteeEmail"), "OPTIONAL" => array("MenteeProgramme", "MenteeSource", "MenteePhone", "MenteeImage", "MenteeStatus", "Status", "Weight"))));

define("MenteeNeedsSkills", json_encode(array("REQUIRED" => array("MenteeName", "SkillName"), "OPTIONAL" => array("Status", "Weight"))));
define("MentorHavingSkills", json_encode(array("REQUIRED" => array("MentorName", "SkillName"), "OPTIONAL" => array("Status", "Weight"))));

define("MasterListText",'All sample .csv files start with a header line naming the fields to be uploaded followed by one or more data lines. A semicolon (;) separates field headings and field values on a given line. Double quotes (") surround headings and values containing special characters, such as semicolons.');

define("MENTOR_DEFAULT_PASSWORD",'mentor123');
define("MENTEE_DEFAULT_PASSWORD",'mentee123');
/* End of file constants.php */
/* Location: ./application/config/constants.php */


