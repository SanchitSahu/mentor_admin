<?php //echo "<pre>";print_r($res);exit;?>
<style>
.customTxt{width:68%;float:left;}
</style>
<section id="container" >
    <!--header start-->
    <?php //echo $this->load->view('admin/includes/header'); ?>
    <!--header end-->
    <?php $res['config']	=	$this->settings_model->getConfig();
	//echo $this->load->view('includes/left_menu',$res); ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong>
								<?php if(isset($res['subdomain_name']) && !empty($res['subdomain_name'])) { ?>
									Edit School Info
								<?php } else { ?>
									Provision New School
								<?php } ?>
							</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                                <!--<a href="javascript:;" class="fa fa-times"></a>-->
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = "";//($this->uri->segment(4) == "") ? 'superadmin/dashboard/provisionSchool' : 'superadmin/dashboard/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_school_form", 'method' => "post"));
                                ?>
								<div class="form-group col-lg-12">									
									<label for="subdomainName" class="control-label col-lg-3">School Name</label>
									<div class="col-lg-6">
										<div class="input-group" style="width: 100%;">
											<span class="input-group-addon">www.</span>
											<?php
												$formArr = array(
													'class' => "form-control customTxt",
													'placeholder' => "Enter School Name(as one word)",
													'name' => 'subdomainName',
													'id' => 'subdomainName',
													'value' =>$res['subdomain_name'],
													'maxlength'=>30
												);
												if($this->uri->segment(4) != "") { 
													$formArr['disabled'] = 'disabled';
												?>
													<input type="hidden" maxlength="10" name="subdomainName" value="<?php echo set_value('subdomainName', $res['subdomain_name']); ?>" />
												<?php }
											?>
											<!--<input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3">-->
											<?php echo form_input($formArr); ?>
											<input type="hidden" id="subdomainID" name="subdomainID" value="<?php echo set_value('subdomainID', $this->uri->segment(4)); ?>" />
											<span class="input-group-addon">.melstm.com</span>
										</div>
									</div>
									<?php echo form_error('subdomainName'); ?>
								</div>
								
                                <div class="form-group col-lg-12">
                                    <label for="Weight" class="control-label col-lg-3">Email Address</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Email Address of School Admin", 'name' => 'email','id' => 'email','value' =>$res['Email'])); ?>
                                        <?php echo form_error('email'); ?>
                                    </div>
                                </div>
								
								<?php /*<div class="form-group col-lg-12">
                                    <label for="Weight" class="control-label col-lg-3">Department/Programme Name</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Department/Programme Name", 'name' => 'programmeName','id' => 'programmeName','value' => $res['DepartmentName'])); ?>
                                        <?php echo form_error('programmeName'); ?>
                                    </div>
                                </div>*/ ?>
								
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
     function successalertify() {
        alertify.success("Please wait....");
     }
     function erroralertify() {
      //   alertify.error("There has been some Error occured.");
     }
     function logalertify() {
     	alertify.log("User has canceled event");
        setTimeout(function () {
	        window.location.href = "<?php echo $this->config->item('base_url').'superadmin/dashboard'?>";
        },1000);
     }


    $(document).ready(function() {
        // validate signup form on keyup and submit
        $("#add_school_form").validate({
			errorPlacement: function (error, element) {
				if($(element).attr('id') == 'subdomainName') {
					$(element).parent().after(error);
				} else {
					error.insertAfter(element);
				}
            },
            rules: {
				subdomainName:{
					required : true,
					noSpace: true,
					alphanumeric: true,
					remote: {
                        url: '<?php echo base_url();?>superadmin/dashboard/checkSubdomainName',
                        type: "post",
						data: {
                            subdomainID: function () {
                                return $("#subdomainID").val();
                            }
                        }
                    }
				},
				email:{
					required : true,
					email : true,
					remote: {
                        url: '<?php echo base_url();?>superadmin/dashboard/checkemail',
                        type: "post",
						data: {
                            subdomainID: function () {
                                return $("#subdomainID").val();
                            }
                        }
                    }
				}
            },
            messages: {
                subdomainName:{
					required : 'Please enter School Name',
					remote : "School with same name already exists"
				},
				email:{
					required : 'Please enter Email Address',
					email : 'Please enter a valid Email',
					remote : "Another School admin is using the same email."
				}
            }
        });
        
    });


	//$("input#subdomainName").on({
	//	keydown: function(e) {
	//		if (e.which === 32) {
	//			return false;
	//		} else if (e.which < 48 || (e.which > 57 && e.which < 65) || (e.which > 90 && e.which < 97) ||e.which > 122) {
	//			return false;
	//		}	
	//	},
	//	change: function() {
	//		this.value = this.value.replace(/\s/g, "");
	//		this.value = this.value.replace("'", '');
	//	}
	//});
</script>
