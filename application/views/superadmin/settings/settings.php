<link rel="stylesheet" href="<?php echo $this->config->item('base_css'); ?>pick-a-color-1.2.3.min.css">	  
<style>
    .custom-error{
        color: #b94a48;
        display: inline;
        font-weight: 400;
        margin: 5px 0;
    }	

    .divClass{
        margin-bottom:0px !important;
    }
    #uploadFile{
        padding-bottom: 3px;
        padding-top: 9px;
    }
</style>		

<!--header start-->
<?php echo $this->load->view('admin/includes/header'); ?>
<!--header end-->
<?php
$res['config'] = $this->settings_model->getConfig();
echo $this->load->view('includes/left_menu', $res);
?>


<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>There are various options in this section that you can change to customize the look and feel of MELS Admin Interface. These changes also affect the mobile app</strong>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="col-lg-12 col-sm-12 col-md-12">
                            <div class="setting-block" id="settingBlock">
                                <form method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>superadmin/settings/updateConfig">
                                    <div class="col-lg-12 col-sm-12 col-md-12">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <label>What do you call an Advisor (Advisor, Mentor, Coach, etc.)?</label>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <input style="width:225px;padding:7px 3 px;" type="text" value="<?php echo $config[0]->mentorName ?>" id="mentorName" name="mentorName">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-sm-12 col-md-12">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <label>What do you call a Client (Entrepreneur, Mentee, Helixer, etc.)?</label>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <input style="width:225px;padding:7px 3 px;" type="text" value="<?php echo $config[0]->menteeName ?>" id="menteeName" name="menteeName">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 col-md-12 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <label>Local file containing logo</label>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin">
                                            <input id="uploadFile" placeholder="Choose File" value="<?php echo $config[0]->logo ?>" disabled="disabled" />
                                            <div class="fileUpload btn btn-3d-success" style="background-color:<?php echo $config[0]->buttonColor; ?>">
                                                <span>Browse</span>
                                                <input type="file" id="logo" class="upload" name="logo">			
                                            </div>
                                            <div class="custom-error" id="fileErr" style="display:none;"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 col-md-12 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label>Local file containing icon</label> </div>
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input id="uploadFileicon" placeholder="Choose File" value="<?php echo $config[0]->icon ?>" disabled="disabled" />
                                            <div class="fileUpload btn btn-3d-success" style="background-color:<?php echo $config[0]->buttonColor; ?>">
                                                <span>Browse</span>
                                                <input type="file" id="icon" class="upload" name="icon">			
                                            </div>
                                            <div class="custom-error" id="iconfileErr" style="display:none;"></div>
                                        </div></div>


                                    <div class="col-lg-12 col-sm-12 col-md-12 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <label style="float: left;">Logo background color</label>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin">
                                            <input type="text" value="<?php echo $config[0]->headerColor ?>" id="headerColor" class="pick-a-color" name="headerColor">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 col-md-12 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <label style="float: left;">Left-panel font color</label>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin">
                                            <input type="text" value="<?php echo $config[0]->leftMenuFont ?>" id="leftMenuFont" class="pick-a-color" name="leftMenuFont">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 col-md-12 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <label  style="float: left;">Left-panel background color</label>
                                        </div> 
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin">
                                            <input type="text" value="<?php echo $config[0]->leftBarColor ?>" id="leftBarColor" class="pick-a-color" name="leftBarColor">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 col-md-12 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <label style="float: left;">Table header text font color</label>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin">
                                            <input type="text" value="<?php echo $config[0]->tableFont ?>" id="tableFont" class="pick-a-color" name="tableFont">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 col-md-12 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <label style="float: left;">Table header Background color</label>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin">
                                            <input type="text" value="<?php echo $config[0]->tableBackground ?>" id="tableBackground" class="pick-a-color" name="tableBackground">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 col-md-12 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <label  style="float: left;">Button background color</label>
                                        </div> 
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin">
                                            <input type="text" value="<?php echo $config[0]->buttonColor ?>" id="buttonColor" class="pick-a-color" name="buttonColor">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 col-md-12 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <label  style="float: left;">Default font color</label>
                                        </div> 
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin">
                                            <input type="text" value="<?php echo $config[0]->fontColor ?>" id="fontColor" class="pick-a-color" name="fontColor">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 col-md-12 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <label  style="float: left;">Background color</label>
                                        </div> 
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin">
                                            <input type="text" value="<?php echo $config[0]->backgroundColor ?>" id="backgroundColor" class="pick-a-color" name="backgroundColor">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 col-md-12 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <label  style="float: left;">Highlight background color</label>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin">
                                            <input type="text" value="<?php echo $config[0]->highlightColor ?>" id="highlightColor" class="pick-a-color" name="highlightColor">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-sm-12 col-md-12">
                                        <button class="btn btn-3d-success" style="background-color:<?php echo $config[0]->buttonColor; ?>" onclick="successalertify();" type="submit">Save</button>
                                    </div>
                                    <div class="col-lg-12 col-sm-12 col-md-6">
                                    <!--<input class="btn btn-3d-success" type="submit" value="Save">--></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<style>
    .fileUpload {
        position: relative;
        overflow: hidden;
    }
    .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
</style>
<script src="<?php echo $this->config->item('base_js'); ?>tinycolor-0.9.15.min.js"></script>
<script src="<?php echo $this->config->item('base_js'); ?>pick-a-color-1.2.3.min.js"></script>
<script type="text/javascript">
                                            $(document).ready(function () {
                                                $('#logo').change(function () {
                                                    var val = $(this).val().toLowerCase();
                                                    var regex = new RegExp("(.*?)\.(jpg|jpeg|svg|png|ico)$");

                                                    if (!(regex.test(val))) {
                                                        $(this).val('');
                                                        $("#fileErr").html("Unsupported file");
                                                        $("#fileErr").css("display", "block");
                                                    } else {
                                                        $("#fileErr").html("");
                                                        $("#fileErr").css("display", "none");
                                                        document.getElementById("uploadFile").value = val;

                                                        document.getElementById("logo").onchange = function () {
                                                            document.getElementById("uploadFile").value = this.value;
                                                        };
                                                    }
                                                });

                                                $('#icon').change(function () {
                                                    var val = $(this).val().toLowerCase();
                                                    var regex_icon = new RegExp("(.*?)\.(jpg|jpeg|svg|png|ico)$");
                                                    if (!(regex_icon.test(val))) {
                                                        $(this).val('');
                                                        $("#iconfileErr").html("Unsupported file");
                                                        $("#iconfileErr").css("display", "block");
                                                        //alert('Unsupported file');
                                                    } else {
                                                        //var val	=	"<?php echo $config[0]->logo ?>";
                                                        //alert(val);
                                                        $("#iconfileErr").html("");
                                                        $("#iconfileErr").css("display", "none");
                                                        document.getElementById("uploadFileicon").value = val;
                                                        document.getElementById("icon").onchange = function () {
                                                            document.getElementById("uploadFileicon").value = this.value;
                                                        };
                                                    }
                                                });

                                                $(".pick-a-color").pickAColor({
                                                    showSpectrum: true,
                                                    showSavedColors: true,
                                                    saveColorsPerElement: true,
                                                    fadeMenuToggle: true,
                                                    showAdvanced: true,
                                                    showBasicColors: true,
                                                    showHexInput: true,
                                                    allowBlank: true,
                                                    inlineDropdown: true
                                                });

                                            });
</script>