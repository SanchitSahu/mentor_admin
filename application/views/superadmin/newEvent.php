<?php //echo "<pre>";print_r($res);exit;                            ?>
<style>
    .customTxt{width:68%;float:left;}
</style>
<section id="container" >
    <!--header start-->
    <?php //echo $this->load->view('admin/includes/header'); ?>
    <!--header end-->

    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong>
                                <?php if (isset($res['subdomain_name']) && !empty($res['subdomain_name'])) { ?>
                                    Edit Event Info
                                <?php } else { ?>
                                    Provision New Event
                                <?php } ?>
                            </strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                                <!--<a href="javascript:;" class="fa fa-times"></a>-->
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ""; //($this->uri->segment(4) == "") ? 'superadmin/dashboard/provisionSchool' : 'superadmin/dashboard/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_event_form", 'method' => "post"));
                                ?>
                                <div class="form-group col-lg-12">									
                                    <label for="subdomainName" class="control-label col-lg-3">Event Name</label>
                                    <div class="col-lg-6">
                                        <div class="input-group" style="width: 100%;">
                                            <span class="input-group-addon">www.</span>
                                            <?php
                                            $formArr = array(
                                                'class' => "form-control customTxt",
                                                'placeholder' => "Enter Event Name(as one word)",
                                                'name' => 'subdomainName',
                                                'id' => 'subdomainName',
                                                'value' => $res['subdomain_name'],
                                                'maxlength' => 30
                                            );
                                            if ($this->uri->segment(4) != "") {
                                                $formArr['disabled'] = 'disabled';
                                                ?>
                                                <input type="hidden" maxlength="10" name="subdomainName" value="<?php echo set_value('subdomainName', $res['subdomain_name']); ?>" />
                                            <?php }
                                            ?>
<!--<input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3">-->
                                            <?php echo form_input($formArr); ?>
                                            <input type="hidden" id="subdomainID" name="subdomainID" value="<?php echo set_value('subdomainID', $this->uri->segment(4)); ?>" />
                                            <span class="input-group-addon">.melstm.com</span>
                                        </div>
                                    </div>
                                    <?php echo form_error('subdomainName'); ?>
                                </div>

                                <div class="form-group col-lg-12">
                                    <label for="email" class="control-label col-lg-3">Email Address</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Email Address of Event Admin", 'name' => 'email', 'id' => 'email', 'value' => $res['Email'])); ?>
                                        <?php echo form_error('email'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label for="number_of_mentors" class="control-label col-lg-3"><?php echo $label_for_mentor; ?></label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => $label_for_mentor, 'name' => 'number_of_mentors', 'id' => 'number_of_mentors', 'value' => '')); ?>
                                        <?php echo form_error('number_of_mentors'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label for="number_of_mentees" class="control-label col-lg-3"><?php echo $label_for_mentee; ?></label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => $label_for_mentee, 'name' => 'number_of_mentees', 'id' => 'number_of_mentees', 'value' => '')); ?>
                                        <?php echo form_error('number_of_mentees'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label for="number_of_teams" class="control-label col-lg-3"><?php echo 'Number Of Teams'; ?></label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => 'Number Of Teams', 'name' => 'number_of_teams', 'id' => 'number_of_teams', 'value' => '')); ?>
                                        <?php echo form_error('number_of_teams'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
    function successalertify() {
        alertify.success("Please wait....");
    }
    function erroralertify() {
        //   alertify.error("There has been some Error occured.");
    }
    function logalertify() {
        alertify.log("User has canceled event");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'superadmin/dashboard' ?>";
        }, 1000);
    }


    $(document).ready(function () {
        // validate signup form on keyup and submit
        $("#add_event_form").validate({
            errorPlacement: function (error, element) {
                if ($(element).attr('id') == 'subdomainName') {
                    $(element).parent().after(error);
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                subdomainName: {
                    required: true,
                    noSpace: true,
                    alphanumeric: true,
                    remote: {
                        url: '<?php echo base_url(); ?>superadmin/dashboard/checkSubdomainName',
                        type: "post",
                        data: {
                            subdomainID: function () {
                                return $("#subdomainID").val();
                            }
                        }
                    }
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: '<?php echo base_url(); ?>superadmin/dashboard/checkemail',
                        type: "post",
                        data: {
                            subdomainID: function () {
                                return $("#subdomainID").val();
                            }
                        }
                    }
                },
                number_of_mentors: {
                    required: true,
                    number: true
                },
                number_of_mentees: {
                    required: true,
                    number: true
                },
                number_of_teams: {
                    required: true,
                    number: true,
                    max: function () {
                        return ($("#number_of_mentees").val() * 1);
                    }
                }
            },
            messages: {
                subdomainName: {
                    required: 'Please enter Event Name',
                    remote: "Event with same name already exists"
                },
                email: {
                    required: 'Please enter Email Address',
                    email: 'Please enter a valid Email',
                    remote: "Another Event admin is using the same email."
                },
                number_of_mentors: {
                    required: 'Please enter <?php echo $label_for_mentor; ?>',
                    number: "Please enter number only."
                },
                number_of_mentees: {
                    required: 'Please enter <?php echo $label_for_mentee; ?>',
                    number: "Please enter number only."
                },
                number_of_teams: {
                    required: 'Please enter Number Of Teams',
                    number: "Please enter number only.",
                    max: 'Number Of Teams must be less than or equal to value of <?php echo $label_for_mentee; ?>',
                }
            }

        });
       // return false;
    });

</script>
