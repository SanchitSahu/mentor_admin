<?php //print_r($emailtemplate);exit;
if($emailtemplate['Weight'] == "")
	$emailtemplate['Weight']	=	1000;
else
	$emailtemplate['Weight']	=	$emailtemplate['Weight'];

?>

<section id="container" >
    <!--header start-->
    <?php //echo $this->load->view('admin/includes/header'); ?>
    <!--header end-->
    <?php $res['config']	=	$this->settings_model->getConfig();
	//echo $this->load->view('includes/left_menu',$res); ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong>Email Template</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                                <!--<a href="javascript:;" class="fa fa-times"></a>-->
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'superadmin/emailtemplate/add' : 'superadmin/emailtemplate/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_skill_form", 'method' => "post"));
                                ?>
                                <div class="form-group ">
                                    <label for="TemplateSubject" class="control-label col-lg-3">Subject</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Email Template Subject", 'name' => 'TemplateSubject', 'value' => $emailtemplate['EmailTemplateTitle'])); ?>
                                        <?php echo form_error('TemplateSubject'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="TemplateMessage " class="control-label col-lg-3">Message</label>
                                    <div class="col-lg-6">
                                        <?php echo form_textarea(array('class' => "form-control ckeditor", 'placeholder' => "Enter Email Template Message", 'name' => 'TemplateMessage', 'value' => $emailtemplate['Content'])); ?>
                                        <?php echo form_error('TemplateMessage'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-3">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight Name", 'name' => 'Weight', 'value' => $emailtemplate['Weight'])); ?>
                                        <?php echo form_error('Weight'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
     function successalertify() {
     //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
     }
     function erroralertify() {
      //   alertify.error("There has been some Error occured.");
     }
     function logalertify() {
     	alertify.log("User has cancelled event");
        setTimeout(function () {
	        window.location.href = "<?php echo $this->config->item('base_url').'superadmin/emailtemplate'?>";
        },1000);
     }


    $(document).ready(function() {
        // validate signup form on keyup and submit
        $("#add_skill_form").validate({
            ignore: [],
            rules: {
                TemplateMessage : {
                    required: function(){
                        CKEDITOR.instances.TemplateMessage.updateElement();
                    }
                },
                TemplateSubject:{
                    required: true
                },
                Weight: {
                    required: true
                }
            },
            messages: {
                TemplateMessage: {
                    required: "Please enter Template Message"
                },
                TemplateSubject:{
                    required: "Please enter Template Subject"
                },
                Weight: {
                    required: "Please enter Display Order"
                }
            }
        });
        
    });
</script>