<script language="JavaScript" type="text/javascript">
    function checkDelete(val) {
        var loc = $('.delete-' + val).attr('href');
        alertify.confirm("Confirm Deletion?", function (e) {
            if (e) {
                alertify.success("Record has been deleted successfully.");
                document.location.href = loc;
            } else {
                alertify.log("User has cancelled event");
            }
        });
        //prevent link from being followed immediately on click
        return false;
    }
</script>

<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>Email Templates</strong>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <!--<a href="javascript:;" class="fa fa-times"></a>-->
                        </span>
                    </header>
                    <div class="panel-body">
                        <a href="<?php echo $this->config->item('base_url') . 'superadmin/emailtemplate/add'; ?>" class="btn btn-round btn-success" style="float:left; margin-right:15px;"><i class="livicon" data-s="16" data-n="plus-alt" data-c="#fff" data-hc="0" id="I15" style="width: 16px; height: 16px;"></i> Add</a>
                        <!--<a href="" class="btn btn-round btn-danger" onclick="deleteconfirm();"><i class="livicon" data-s="16" data-n="trash" data-c="#fff" data-hc="0" id="I14" style="width: 16px; height: 16px;"></i> Delete</a>-->

                        <div class="adv-table">
                            <?php echo $this->table->generate(); ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!-- Init variables for DATATABLE.  Note: Please put Datatable js files at footer  -->
<script>
    var dattTableName = 'dynamic_table';
    var sAjaxSource = '<?php echo base_url(); ?>superadmin/emailtemplate/datatable';
    var baseUrl = '<?php echo base_url(); ?>';
</script>
<!-- Init variables for DATATABLE end -->