<style>
.customTxt{width:68%;float:left;}
</style>
<section id="container" >
    <!--header start-->
    <?php echo $this->load->view('admin/includes/header'); ?>
    <!--header end-->
    <?php $res['config']	=	$this->settings_model->getConfig();
	echo $this->load->view('includes/left_menu',$res); ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong>Provision New School</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                                <!--<a href="javascript:;" class="fa fa-times"></a>-->
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'superadmin/dashboard/provisionSchool' : 'superadmin/dashboard/editSchool/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_school_form", 'method' => "post"));
                                ?>
                                <div class="form-group col-lg-12">
                                    <label for="subdomainName" class="control-label col-lg-3">School Name</label>
                                    <div class="col-lg-8">
                                        <span style="float:left;margin-top:7px;">www.<?php echo $res[0]->subdomain_name?>.melstm.com</span>   
                                    </div>
									<?php echo form_error('subdomainName'); ?>
                                </div>
								
                                <div class="form-group col-lg-12">
                                    <label for="Weight" class="control-label col-lg-3">Email Address</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Email Address of School Admin", 'name' => 'email','id' => 'email','value' =>$res[0]->Email)); ?>
                                        <?php echo form_error('email'); ?>
                                    </div>
                                </div>
								
								<div class="form-group col-lg-12">
                                    <label for="Weight" class="control-label col-lg-3">Department/Programme Name</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Department/Programme Name", 'name' => 'programmeName','id' => 'programmeName','value' => $res[0]->DepartmentName)); ?>
                                        <?php echo form_error('programmeName'); ?>
                                    </div>
                                </div>
								
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
     function successalertify() {
     //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
     }
     function erroralertify() {
      //   alertify.error("There has been some Error occured.");
     }
     function logalertify() {
     	alertify.log("User has canceled event");
        setTimeout(function () {
	        window.location.href = "<?php echo $this->config->item('base_url').'superadmin/dashboard'?>";
        },1000);
     }


    $().ready(function() {
        // validate signup form on keyup and submit
        $("#add_topic_form").validate({
            rules: {
                TopicDescription: {
                    required: true,
                    minlength: 2
                }
            },
            messages: {
                TopicDescription: {
                    required: "Please enter topic description",
                    minlength: "Your topic description must consist of at least {0} characters"
                }
            }
        });
        
    });
	
$( "#subdomainName" ).change(function() {
	var subdomainName	=	$("#subdomainName").val();
	//var menteeDrp	=	$("#menteeDrp").val();

	if(subdomainName == ""){
		alert("Enter Subdomain name");
	}else{
		$.ajax({
			url: '<?php echo base_url();?>superadmin/dashboard/checkSubdomainName',
			data:"subdomainName="+subdomainName,
			success: function(result){ 
				if(result != ""){
					alertify.log(result);
					$("#subdomainName").val("");
				}
				
			}
		});	
	}	
});

$( "#email" ).keyup(function() {
	var email	=	$("#email").val();
	//var menteeDrp	=	$("#menteeDrp").val();

	if(email != ""){
		$.ajax({
			url: '<?php echo base_url();?>superadmin/dashboard/checkemail',
			data:"email="+email,
			success: function(result){ 
				if(result != ""){
					alertify.log(result);
					$("#email").val("");
				}
				
			}
		});	
	}	
});


$( "#email" ).change(function() {
	var email	=	$("#email").val();
	//var menteeDrp	=	$("#menteeDrp").val();

	if(email != ""){
		$.ajax({
			url: '<?php echo base_url();?>superadmin/dashboard/checkemail',
			data:"email="+email,
			success: function(result){ 
				if(result != ""){
					alertify.log(result);
					$("#email").val("");
				}
				
			}
		});	
	}	
});
</script>