<script language="JavaScript" type="text/javascript">
function checkDelete(val){
	var loc = $('.delete-'+val).attr('href');
	alertify.confirm("Confirm Deletion?", function (e) {
		if (e) {
			alertify.success("Record has been deleted successfully.");
			document.location.href = loc;
		} else {
			alertify.log("User have canceled the event.");		
		}
	});
	//prevent link from being followed immediately on click
	return false;
}
</script>

<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>Manage School Info</strong>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <!--<a href="javascript:;" class="fa fa-times"></a>-->
                        </span>
                    </header>
                    <div class="panel-body">
                        <a href="<?php echo $this->config->item('base_url') . 'superadmin/dashboard/provisionSchool'; ?>" class="btn btn-round btn-success" style="float:left; margin-right:15px;"><i class="livicon" data-s="16" data-n="plus-alt" data-c="#fff" data-hc="0" id="I15" style="width: 16px; height: 16px;"></i> Provision New School</a>
                        <a href="<?php echo $this->config->item('base_url') . 'superadmin/dashboard/provisionEvent'; ?>" class="btn btn-round btn-success" style="float:left; margin-right:15px;"><i class="livicon" data-s="16" data-n="plus-alt" data-c="#fff" data-hc="0" id="I16" style="width: 16px; height: 16px;"></i> Provision New Event</a>
                        <!--<a href="" class="btn btn-round btn-danger" onclick="deleteconfirm();"><i class="livicon" data-s="16" data-n="trash" data-c="#fff" data-hc="0" id="I14" style="width: 16px; height: 16px;"></i> Delete</a>-->
                        <div class="adv-table">
                            <?php echo $this->table->generate(); ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<div id="reset_pwd" style="width:400px;display: none;">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <strong>Reset Password</strong>
            </header>
            <div class="panel-body">
                <div class="form">
                    <form class="cmxform form-horizontal" id="resetpassword" name="resetpassword" method="post">
					<input type="hidden" name="school_id" id="school_id" />
                        <div class="form-group ">
                            <label for="vBannerTitle" class="control-label col-lg-3">Password</label>
                            <div class="col-lg-9">
                                <?php echo form_password(array('class' => "form-control", 'placeholder' => "Enter Password", 'name' => 'password', 'id' => 'password')); ?>
                            </div><br><br><br>
							<label for="vBannerTitle" class="control-label col-lg-3">Confirm Password</label>
                            <div class="col-lg-9">
                                <?php echo form_password(array('class' => "form-control", 'placeholder' => "Enter confirm Password", 'name' => 'confirm_password', 'id' => 'confirm_password')); ?>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
									<br/>
                                    <button class="btn btn-3d-success" type="submit">Set</button>
                                    <a href="javascript:void(0);" class="btn btn-3d-success close-box" type="button" >Cancel</a><!--onclick="logalertify('admin/country');"-->
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
 <!-- Init variables for DATATABLE.  Note: Please put Datatable js files at footer  -->
<script>
    var dattTableName ='dynamic_table';
    var sAjaxSource = '<?php echo base_url(); ?>superadmin/managesubadmin/datatable';
    var baseUrl  = '<?php echo base_url();?>';
</script>
<!-- Init variables for DATATABLE end -->
<script type="text/javascript">
		$(document).on("click",".resetpwd",function(){
			var id = $(this).attr("data-id");
			$("#school_id").val(id);
		});
$(document).ready(function () {
        $('#resetpassword').validate({
            rules: {
                "password": {
                    "required": true,
                },
				"confirm_password": {
                    "required": true,
					"equalTo" : "#password"
                }
            },
            messages: {
                "password": {
                    "required": "Please enter password.",
                },
				"confirm_password": {
                    "required": "Please enter confirm password.",
					"equalTo": "Confirm password not matched"
                }
            },
            submitHandler: function (form) {
                var id = $("#school_id").val();
				var pass = $("#password").val();
                if (id !== "") {
                    jQuery.ajax({
                        type: "POST",
                        url: "<?php echo $this->config->item('base_url'); ?>superadmin/dashboard/resetpassword",
                        data: {id:id, pass:pass},
                        success: function (output) {
                            if (output === "mail-sent") {
                                $.fancybox.close();
								alertify.success("Password reset successfully");
								$('#resetpassword')[0].reset();
                            } else {
								 $.fancybox.close();
								alertify.error(output);
                                return false;
                            }
                        }
                    });
                }
            }
        });
    });		
	</script>