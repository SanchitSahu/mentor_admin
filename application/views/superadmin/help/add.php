<?php
//echo "<pre>";print_r($help);exit;
if ($help['Weight'] == "")
    $help['Weight'] = 1000;
else
    $help['Weight'] = $help['Weight'];
?>

<section id="container" >
    <!--header start-->
    <?php //echo $this->load->view('admin/includes/header'); ?>
    <!--header end-->
    <?php
    $res['config'] = $this->settings_model->getConfig();
    //echo $this->load->view('includes/left_menu',$res); 
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong>Help Text</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                                <!--<a href="javascript:;" class="fa fa-times"></a>-->
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'superadmin/help/add' : 'superadmin/help/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_help_form", 'method' => "post"));
                                ?>
                                <div class="form-group">
                                    <label for="ScreenType" class="control-label col-lg-3">Screen Type</label>
                                    <div class="col-lg-6">
                                        <?php
                                        $options = array('' => '--Select Screen Type--', 'header' => 'Header', 'screen_text' => 'Screen Text', 'footer' => 'Footer');
                                        echo form_dropdown('ScreenType', $options, set_value('ScreenType', $help['ScreenType']), 'class="form-control" id="ScreenType"');
                                        echo form_error('ScreenType', '<label class="error">', '</label>');
                                        ?>
                                    </div>
                                </div>

                                <div class="form-group headerfooter" style="display:<?php echo ($help['ScreenType'] == 'screen_text') ? 'block' : 'none'; ?>">
                                    <label for="ScreenHeader" class="control-label col-lg-3">Screen Header</label>
                                    <div class="col-lg-6">
                                        <?php
                                        echo form_dropdown('ScreenHeader', $headerList, set_value('ScreenHeader', $help['HeaderId']), 'class="form-control" id="ScreenHeader"');
                                        echo form_error('ScreenHeader', '<label class="error">', '</label>');
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group headerfooter" style="display:<?php echo ($help['ScreenType'] == 'screen_text') ? 'block' : 'none'; ?>">
                                    <label for="ScreenFooter" class="control-label col-lg-3">Screen Footer</label>
                                    <div class="col-lg-6">
                                        <?php
                                        echo form_dropdown('ScreenFooter', $footerList, set_value('ScreenFooter', $help['FooterId']), 'class="form-control" id="ScreenFooter"');
                                        echo form_error('ScreenFooter', '<label class="error">', '</label>');
                                        ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="UserType" class="control-label col-lg-3">User Type</label>
                                    <div class="col-lg-6">
                                        <?php
                                        $options = array('' => '--Select User Type--', '0' => $config[0]->mentorName, '1' => $config[0]->menteeName, '2' => 'Both Users');
                                        echo form_dropdown('UserType', $options, set_value('UserType', $help['UserType']), 'class="form-control" id="UserType"');
                                        echo form_error('UserType', '<label class="error">', '</label>');
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="ScreenName" class="control-label col-lg-3">Screen ID</label>
                                    <div class="col-lg-6">
                                        <?php
                                        echo form_dropdown('ScreenName', $screenList, set_value('ScreenName', $help['ScreenName']), 'class="form-control" id="ScreenName"');
                                        echo form_error('ScreenName', '<label class="error">', '</label>');
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ScreenIdentifier" class="control-label col-lg-3">Screen Name</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Screen Name", 'name' => 'ScreenIdentifier', 'value' => $help['ScreenIdentifier'])); ?>
                                        <?php echo form_error('ScreenIdentifier'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="HelpDescription" class="control-label col-lg-3">Help Description</label>
                                    <div class="col-lg-6">
                                        <?php echo form_textarea(array('class' => "form-control ckeditor", 'placeholder' => "Enter Help Description", 'name' => 'HelpDescription', 'value' => $help['HelpDescription'])); ?>
                                        <?php echo form_error('HelpDescription'); ?>
                                        
                                        <label style="text-align: left; font-size: 13px;" class="control-label">Add <b>##IMPROMPTU_MEETING##</b> whereever you want to insert the Impromptu Meeting title set by School Admin</label>
                                        <label style="text-align: left; font-size: 13px;" class="control-label">Add <b>##CONFIGURED_MENTEE_NAME##</b> whereever you want to insert the Configured <?php echo $config[0]->menteeName; ?> Name</label>
                                        <label style="text-align: left; font-size: 13px;" class="control-label">Add <b>##CONFIGURED_MENTOR_NAME##</b> whereever you want to insert the Configured <?php echo $config[0]->mentorName; ?> Name</label>
                                        <label style="text-align: left; font-size: 13px;" class="control-label">Add <b>##MENTEE_USERNAME##</b> whereever you want to insert the <?php echo $config[0]->menteeName; ?> User Name</label>
                                        <label style="text-align: left; font-size: 13px;" class="control-label">Add <b>##MENTOR_USERNAME##</b> whereever you want to insert the <?php echo $config[0]->mentorName; ?> User Name</label>
                                        <label style="text-align: left; font-size: 13px;" class="control-label">Add <b>##CONFIGURED_TEAM_NAME##</b> whereever you want to insert the Configured Team Name</label>
                                        <label style="text-align: left; font-size: 13px;" class="control-label">Add <b>##USERNAME##</b> whereever you want to insert the Current Logged in <?php echo $config[0]->mentorName; ?>'s/<?php echo $config[0]->menteeName; ?>'s User Name</label>
                                        <label style="text-align: left; font-size: 13px;" class="control-label">Add <b>##MENTOR_HEADER##</b> or <b>##MENTEE_HEADER##</b> whereever you want to insert the Header for <?php echo $config[0]->mentorName; ?> / <?php echo $config[0]->menteeName; ?>, respectively</label>
                                        <label style="text-align: left; font-size: 13px;" class="control-label">Add <b>##MENTOR_FOOTER##</b> or <b>##MENTEE_FOOTER##</b> whereever you want to insert the Footer for <?php echo $config[0]->mentorName; ?> / <?php echo $config[0]->menteeName; ?>, respectively</label>
                                        <label style="text-align: left; font-size: 13px;" class="control-label">Add <b>##COMMON_HEADER##</b> or <b>##COMMON_FOOTER##</b> whereever you want to insert the Common Header / Common Footer, respectively</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Weight" class="control-label col-lg-3">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight Name", 'name' => 'Weight', 'value' => $help['Weight'])); ?>
                                        <?php echo form_error('Weight'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
    function successalertify() {
        //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
    }

    function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'superadmin/help' ?>";
        }, 1000);
    }


    $(document).ready(function () {
        $('#ScreenType').change(function () {
			if ($(this).val() == 'screen_text') {
                $('.headerfooter').show();
            } else {
				$('.headerfooter').hide();
			}
        });

        // validate signup form on keyup and submit
        $("#add_help_form").validate({
            ignore: [],
            rules: {
                ScreenType: {
                    required: true
                },
                UserType: {
                    required: true
                },
                ScreenName: {
                    required: true
                },
                //ScreenIdentifier: {
                //    required: true
                //},
                HelpDescription: {
                    required: function () {
                        CKEDITOR.instances.HelpDescription.updateElement();
                    }
                },
                Weight: {
                    required: true
                }
            },
            messages: {
                ScreenType: {
                    required: 'Please select Screen Type'
                },
                UserType: {
                    required: 'Please select User Type'
                },
                ScreenName: {
                    required: 'Please specify Screen Name'
                },
                //ScreenIdentifier: {
                //    required: 'Please specify Screen Common Name'
                //},
                HelpDescription: {
                    required: 'Please enter Help Text'
                },
                Weight: {
                    required: 'Please specify Display Order'
                }
            }
        });

    });
</script>