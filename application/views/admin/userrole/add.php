<?php
if ($userRoles['Weight'] == "")
    $userRoles['Weight'] = 1000;
else
    $userRoles['Weight'] = $userRoles['Weight'];
?>

<section id="container" >
    <?php $res['config'] = $this->settings_model->getConfig(); ?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong>Skills</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'admin/userRoles/add' : 'admin/userRoles/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_userrole_form", 'method' => "post"));
                                ?>
                                <div class="form-group ">
                                    <label for="UserRoleName" class="control-label col-lg-3">Role Name</label>
                                    <div class="col-lg-6">
                                        <input type="hidden" id="userRoleID" name="userRoleID" value="<?php echo set_value('userRoleID', $userRoles['userRoleID']); ?>" />
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Role Name", 'name' => 'UserRoleName', 'value' => $userRoles['UserRoleName'])); ?>
                                        <?php echo form_error('UserRoleName'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-3">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight Name", 'name' => 'Weight', 'value' => $userRoles['Weight'])); ?>
                                        <?php echo form_error('Weight'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
    function successalertify() {
        //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
    }
    function erroralertify() {
        //   alertify.error("There has been some Error occured.");
    }
    function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'admin/userRoles' ?>";
        }, 1000);
    }


    $().ready(function () {
        // validate signup form on keyup and submit
        $("#add_userrole_form").validate({
            rules: {
                UserRoleName: {
                    required: true,
                    remote: {
                        url: "<?php echo $this->config->item('base_url'); ?>admin/userRoles/checkUserRoleExist",
                        type: "post",
                        data: {
                            userRoleID: function () {
                                return $('#userRoleID').val();
                            }
                        }
                    }
                }
            },
            messages: {
                UserRoleName: {
                    required: "Please enter Role name",
                    remote: "Role with same name already exists"
                }
            }
        });

    });
</script>