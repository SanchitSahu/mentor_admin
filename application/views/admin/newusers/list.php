<link href="<?php echo $this->config->item('base_css'); ?>pickadate/default.css" rel="stylesheet" />
<link href="<?php echo $this->config->item('base_css'); ?>pickadate/default.date.css" rel="stylesheet" />
<link href="<?php echo $this->config->item('base_css'); ?>pickadate/default.time.css" rel="stylesheet" />
<style>
	.picker__weekday-display {
		background-color: #7A7AAE;
	}
	.picker__date-display {
		background-color: #b3b3ff;
	}
	.picker__close, .picker__today {
		color: #0044CC;
	}
	.picker__day.picker__day--today{
		color: #0044CC;
	}
</style>

<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>pickadate.js"></script>
<!--<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.1/js/dataTables.colReorder.min.js"></script>-->
<!--<link href="https://cdn.datatables.net/colreorder/1.3.1/css/colReorder.dataTables.min.css" rel="stylesheet" />-->
<script language="JavaScript" type="text/javascript">
    function checkDelete(val) {
        var loc = $('.delete-' + val).attr('href');
        alertify.confirm("Confirm Deletion?", function (e) {
            if (e) {
                alertify.success("Record has been deleted successfully.");
                document.location.href = loc;
            } else {
                alertify.log("User has cancelled event");
            }
        });
        //prevent link from being followed immediately on click
        return false;
    }
</script>

<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>User Notifications</strong>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <!--<a href="javascript:;" class="fa fa-times"></a>-->
                        </span>
                    </header>
					
					
					
                    <div class="panel-body">
						<div class="col-sm-12 paddingCls">
							<div class="col-sm-offset-2 col-sm-4">
								<label>From Registration Date</label>
								<div class='input-group date' id='datepicker'>
									<input type='text' id="from" class="form-control datepicker" />
									<span class="input-group-addon from_date">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
							
							<div class="col-sm-4">
								<label>To Registration Date (blank=today)</label>
								<div class='input-group date' id='datepicker'>
									<input type='text' id="to" class="form-control datepicker" />
									<span class="input-group-addon to_date">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								
							</div>
						</div>
						
						
						<div class="col-sm-12 paddingCls">
							<div class="col-sm-6" style="border-right: 1px solid #fff;">
								<div class="col-sm-4">
									<label for="NotifiedFilter">Notification Status</label>
								</div>
								<div class="col-sm-4" style="text-align: center;">
									<input type="checkbox" class="filled-in notified_checkbox" id="yesBox" name="yesBox" value='1'/>
									<label for="yesBox">Already Notified</label>	
								</div>	
								<div class="col-sm-4" style="text-align: center;">
									<input type="checkbox" class="filled-in notified_checkbox" id="noBox" name="noBox" value='1'/>
									<label for="noBox">Not Yet Notified</label>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="col-sm-3">
									<label for="NotifiedFilter">User Status</label>
								</div>
								<div class="col-sm-3" style="text-align: center;">
									<input type="radio" class="filled-in notified_radio" id="yesRadio" name="inActiveRadio" value='1' checked="checked"/>
									<label for="yesRadio">Active</label>	
								</div>
								<div class="col-sm-3" style="text-align: center;">
									<input type="radio" class="filled-in notified_radio" id="noRadio" name="inActiveRadio" value='0'/>
									<label for="noRadio">Inactive</label>
								</div>
								<div class="col-sm-3" style="text-align: center;">
									<input type="radio" class="filled-in notified_radio" id="allRadio" name="inActiveRadio" value='all'/>
									<label for="allRadio">All</label>	
								</div>
							</div>
						</div>
						<div class="col-sm-4" style="text-align: right;">
							<button class="btn btn-3d-success"  id="NotifyUser">Notify Selected Users</button>
						</div>
						<div class="col-sm-4" style="text-align: center;">
							<label></label>
							<button class="btn btn-3d-success"  id="listUsers">List Users</button>
						</div>
						<div class="col-sm-4" style="text-align: left;">
							<label></label>
							<button class="btn btn-3d-success"  id="resetSelection">Reset Selection</button>
						</div>
                        <div class="adv-table">
                            <?php echo $this->table->generate(); ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!-- Init variables for DATATABLE.  Note: Please put Datatable js files at footer  -->
<!--<script src="https://cdn.datatables.net/plug-ins/1.10.12/api/fnReloadAjax.js"></script>-->
<script>
    var dattTableName = 'dynamic_table';
    var sAjaxSource = '<?php echo base_url(); ?>admin/newusers/datatable';
    var baseUrl = '<?php echo base_url(); ?>';
</script>

<script type="text/javascript">
	$(document).ready(function () {
		//var s = document.createElement("script");
		//s.type = "text/javascript";
		//s.src = "https://cdn.datatables.net/plug-ins/1.10.12/api/fnReloadAjax.js";
		//$("head").append(s);
		//$("#noBox").attr("checked", true);
		
		$('#listUsers').click(function() {
			var from 	=	$("#from").val();
			var to 		=	$("#to").val();

			var showInactive = $('[name="inActiveRadio"]:checked').val();
			
			var yes_value = $('#yesBox').is(':checked');
			var no_value = $('#noBox').is(':checked');
			
			if( (new Date(from).getTime() > new Date(to).getTime()))
			{
				alertify.log('From date can not be greater then To date');
			} else {
				oTable.fnReloadAjax(sAjaxSource + "?from=" + from + "&to=" + to + "&yes_value=" + yes_value + "&no_value=" + no_value + "&showInactive=" + showInactive);
				
				//$.ajax({
				//	url: '<?php echo base_url();?>admin/newusers/getregisteredUser',
				//	data:"from=" + from + "&to=" + to + "&yes_value=" + yes_value + "&no_value=" + no_value + "&showInactive=" + showInactive,
				//	success: function(result){
				//		var obj = jQuery.parseJSON(result);
				//		$('#dynamic_table').dataTable({
				//			"bProcessing": true,
				//			"bDestroy":true,
				//			"aaData": obj.aaData,
				//		});
				//	}
				//});	
			}
		});
		//$('#listUsers').trigger('click');
		
		var today = new Date();
		var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
		var nextdates = new Date(new Date().setYear(new Date().getFullYear() + 10));
		$('.datepicker').pickadate({
			format:'yyyy-mm-dd',
			disable: [
				{ from: tomorrow, to: nextdates }
			]
		});
		
		$('#resetSelection').click(function(){
			$('[name="chkbox"]').attr('checked',false);
		});
		
		$('.from_date').click(function(event){
			event.stopPropagation();
			$('#from').pickadate("picker").open();
		});
		$('.to_date').click(function(event){
			event.stopPropagation();
			$('#to').pickadate("picker").open();
		});
		
		//$('[name="inActiveRadio"]').change(function() {
		//	$('#listUsers').trigger('click');
		//});
	
		$('.notified_checkbox').click(function() {
			$('#listUsers').trigger('click');
			//var yes_value = $('#yesBox').is(':checked');
			//var no_value = $('#noBox').is(':checked');
			//
			//var from 	=	$("#from").val();
			//var to 		=	$("#to").val();
			//
			//$.ajax({
			//	url: '<?php echo base_url();?>admin/newusers/getNotifiedUsers',
			//	type:'post',
			//	data:{yes_value:yes_value,no_value:no_value,from:from,to:to},
			//	success: function(result){
			//		var obj = jQuery.parseJSON(result);
			//		$('#dynamic_table').dataTable({
			//			"bProcessing": true,
			//			"bDestroy":true,
			//			"aaData": obj.aaData,
			//			"oColReorder": {
			//				"aiOrder": [ 4, 0, 1, 2, 3 ]
			//			}
			//		});
			//	}
			//});	
		});
	
		$('#NotifyUser').click(function() {
			var myarray = []; 
			$('input:checkbox[name=chkbox]').each(function() {    
				if($(this).is(':checked')){
					myarray.push($(this).val());
				}  
			});

			$.ajax({
				url: '<?php echo base_url();?>admin/newusers/NotifyUser',
				data:"notifyUserList=" + myarray,
				success: function(result){
					//var obj = jQuery.parseJSON(result);
					window.location.reload();
					$('#dynamic_table').dataTable({
						"bProcessing": true,
						"bDestroy":true
					});
				}
			});	
		});
		
		//var table = $('#dynamic_table').DataTable({
		//	colReorder: true
		//});
		//table.colReorder.order( [ 4, 0, 1, 2, 3] );
	});
</script>
<!-- Init variables for DATATABLE end -->
