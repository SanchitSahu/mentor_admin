<?php if(isset($callFromApp) && $callFromApp == true) {
	$config[0]->mentorName = 'Sub' . $config[0]->mentorName;
	?>
	
    <style>
        .header.fixed-top.clearfix{
            display: none;
        }
        body {
            margin-top: 0px !important;
        }
        footer{
			display:none;
		}
		#main-content{
			margin-left: 0px;
		}
		aside{
			display: none;
		}
		.wrapper{
			margin-top: 10px;
		}
    </style>
<?php } ?>

<?php
if ($mentors['Weight'] == "")
    $mentors['Weight'] = 1000;
else
    $mentors['Weight'] = $mentors['Weight'];
?>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong><?php echo $config[0]->mentorName ?> Management</strong>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="cmxform form-horizontal " id="holidayForm" method="post" action=""  enctype="multipart/form-data" >
                                <div class="form-group ">
                                    <label for="MentorName" class="control-label col-lg-3"><?php echo $config[0]->mentorName ?> Name</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter " . $config[0]->mentorName . " Name", 'name' => 'MentorName', 'value' => $mentors['MentorName'], 'id' => 'MentorName')); ?>
										
										<?php if(isset($callFromApp) && $callFromApp == true) { ?>
                                            <input type="hidden" id="callFromAppHidden" name="callFromAppHidden" value="true" />
											<input type="hidden" id="IsSubmentor" name="IsSubmentor" value="1" />
                                        <?php } ?>
                                        <input type="hidden" id="MentorID" name="MentorID" value="<?php echo set_value('MentorID', $mentors['MentorID']); ?>" />
                                        <div class="custom-error" id="mentorNameErr" style="display:none;">
                                            <label class="error"><?php echo $config[0]->mentorName ?> Name already exist</label>
                                        </div>
                                    </div>									
                                </div>  
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Photo</label>
                                    <div class="col-md-9">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 200px;">
                                                <?php
                                                if ($mentors['MentorImage'] == "") {
                                                    $imageURL = $this->config->item('base_images') . "default.png";
                                                } else {
                                                    $imageURL = "http://melstm.net/mentor/assets/admin/images/admin/" . $mentors['MentorImage'];
                                                }
                                                ?>
                                                <img id="previewImg" src="<?php echo $imageURL; ?>" alt="" />
                                                <?php if(isset($mentors['MentorImage'])) $imgVal = $mentors['MentorImage'];else $imgVal = "";?>
                                                <input type="hidden" name="imgVal" value="<?php echo $imgVal?>">
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-white btn-file">
                                                    <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                    <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                    <input type="file" id="photo" class="default" name="photo" />
                                                </span>
                                            </div>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                                        </div>                                    
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="MentorPhone" class="control-label col-lg-3 control-label"><?php echo $config[0]->mentorName ?> Phone</label>
                                    <div class="col-lg-6">
                                        <input class=" form-control" id="MentorPhone" name="MentorPhone" type="text" placeholder="Enter <?php echo $config[0]->mentorName ?> Phone" maxlength="100" value="<?php echo set_value('MentorPhone', $mentors['MentorPhone']); ?>" />
                                    </div>
                                </div>
                                
                                <div class="form-group mentorPhoneDiv" style="display: <?php echo ($mentors['MentorPhone'] == '' ) ? 'none' : ''; ?>;">
                                    <label for="MentorPhoneSMS" class="control-label col-lg-3 control-label">Do you want to receive text messages on your phone/tablet?</label>
                                    <div class="col-lg-6">
                                        <input class="change_radio with-gap" name="MentorPhoneSMS" type="radio" id="MentorPhoneSMS1" value="0" <?php echo ($mentors['DisableSMS'] == 0 || $mentors['DisableSMS'] == '') ? 'checked' : '' ; ?>/>
                                        <label for="MentorPhoneSMS1">Yes</label>
                                        <input class="change_radio with-gap" name="MentorPhoneSMS" type="radio" id="MentorPhoneSMS2" value="1" <?php echo ($mentors['DisableSMS'] == 1) ? 'checked' : '' ; ?>/>
                                        <label for="MentorPhoneSMS2" style="margin-left: 40px;">No</label>
                                    </div>
                                </div>
                                
								<div class="form-group" style="display: <?php echo (isset($callFromApp) && $callFromApp == true) ? 'none' : ''; ?>">
									<label for="MentorIsSubadmin" class="control-label col-lg-3 control-label">Is this <?php echo $config[0]->mentorName ?> a Subadmin?</label>
									<div class="col-lg-6">
										<input class="change_radio with-gap" name="MentorIsSubadmin" type="radio" id="MentorIsSubadmin1" value="1" <?php echo ($mentors['IsSubadmin'] == 1) ? 'checked' : '' ; ?>/>
										<label for="MentorIsSubadmin1">Yes</label>
										<input class="change_radio with-gap" name="MentorIsSubadmin" type="radio" id="MentorIsSubadmin2" value="0" <?php echo ($mentors['IsSubadmin'] == 0 || $mentors['IsSubadmin'] == '') ? 'checked' : '' ; ?>/>
										<label for="MentorIsSubadmin2" style="margin-left: 40px;">No</label>
									</div>
								</div>
                                
                                <div class="form-group">
                                    <label for="MentorEmail" class="control-label col-lg-3 control-label"><?php echo $config[0]->mentorName ?> Email</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter " . $config[0]->mentorName . " Email", 'name' => 'MentorEmail', 'value' => $mentors['MentorEmail'], 'id' => 'MentorEmail')); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="userRoleID" class="control-label col-lg-3">Role</label>
                                    <div class="col-lg-6">
                                        <?php echo form_dropdown('userRoleID', $roleData, set_value('userRoleID', $mentors['userRoleID']), 'class="form-control" id="userRoleID"'); ?>
                                        <?php echo form_error('userRoleID', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="MentorSourceID" class="control-label col-lg-3">Source</label>
                                    <div class="col-lg-6">
                                        <?php echo form_dropdown('MentorSourceID', $sourceData, set_value('MentorSourceID', $mentors['MentorSourceID']), 'class="form-control" id="MentorSourceID"'); ?>
                                        <?php echo form_error('MentorSourceID', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-3">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight", 'name' => 'Weight', 'value' => $mentors['Weight'])); ?>
                                        <?php echo form_error('Weight'); ?>
                                    </div>
                                </div>                                    
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();" >Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<script type="text/javascript">
	var formValidation = '';
    $(document).ready(function () {
		$('#MentorPhone').keyup(function() {
			if($(this).val() == '') {
				$('.mentorPhoneDiv').hide();
			} else if($(this).val() != '') {
				$('.mentorPhoneDiv').show();
			}
		});
		
		$("[name=MentorIsSubadmin]").change(function() {
			if($(this).val() !== 0 && !$('#userRoleID').valid()) {
				$('input[name=MentorIsSubadmin]').not(':checked').prop("checked", true);
			}
		});
		
		$('#userRoleID').change(function(){
			if(!$('#userRoleID').valid()) {
				$('input[name=MentorIsSubadmin][value=0]').prop("checked", true);
			}
		});
		
        $('#photo').change(function () {
            //alert("here");
            var val = $(this).val().toLowerCase();
            var regex = new RegExp("(.*?)\.(jpg|jpeg|png|gif)$");

            if (!(regex.test(val))) {
                $(this).val('');
                $(".imgErr").html("Unsupported file");
                $(".imgErr").css("display", "block");
            } else {
                var _URL = window.URL || window.webkitURL;
                var file, img;
                if ((file = this.files[0])) {
                    img = new Image();

                    img.src = _URL.createObjectURL(file);
                    return false;
                }
            }
        });
    
        formValidation = $("#holidayForm").validate({
            rules: {
                MentorName: {
                    required: true,
                    minlength: 2,
                    remote: {
                        url: "<?php echo $this->config->item('base_url'); ?>admin/mentors/checkMentorName",
                        type: "post",
                        data: {
                            MentorID: function () {
                                return $("#MentorID").val();
                            }
                        }
                    }
                },
				userRoleID: {
					required: function (){
						return $('input[name=MentorIsSubadmin][value=1]').is(":checked");
					}
				},
                MentorEmail: {
                    required: true,
                    email: true,
                    remote: {
                        url: "<?php echo $this->config->item('base_url'); ?>admin/mentors/checkEmailExist",
                        type: "post",
                        data: {
                            MentorID: function () {
                                return $("#MentorID").val();
                            }
                        }
                    }
                }
            },
            messages: {
                MentorName: {
                    required: "Please enter <?php echo $config[0]->mentorName ?> Name",
                    minlength: "<?php echo $config[0]->mentorName ?> name must consist of at least {0} characters",
                    remote: "User with same name is already registered."
                },
				userRoleID: {
					required: 'Please select <?php echo $config[0]->mentorName ?> Role'
				},
                MentorEmail: {
                    required: "Please enter <?php echo $config[0]->mentorName ?> email",
                    email: "Please enter valid email",
                    remote: "Email is already registered"
                }
            }
        });
    });
	
	function successalertify() {
        //  alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
    }
    
    function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
			<?php if(isset($callFromApp) && $callFromApp == true) { ?>
				window.location.href = "<?php echo $this->config->item('base_url') . 'admin/mentors?callFromApp=true&loggedInMentor='.$mentor_details['MentorID']; ?>";
			<?php } else { ?>
				window.location.href = "<?php echo $this->config->item('base_url') . 'admin/mentors' ?>";
			<?php } ?>
        }, 1000);
    }
</script>