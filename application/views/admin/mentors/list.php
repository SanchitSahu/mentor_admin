<?php if(isset($callFromApp) && $callFromApp == true) {
	$config[0]->mentorName = 'Sub' . $config[0]->mentorName;
	?>
	
    <style>
        .header.fixed-top.clearfix{
            display: none;
        }
		a.btn.btn-round.btn-success:not(:first-of-type), form:first-of-type, label.download_label{
			display: none;
		}
        body {
            margin-top: 0px !important;
        }
        footer{
			display:none;
		}
		#main-content{
			margin-left: 0px;
		}
		aside{
			display: none;
		}
		.wrapper{
			margin-top: 10px;
		}
    </style>
<?php } ?>
<script language="JavaScript" type="text/javascript">
function checkDelete(val){
	var loc = $('.delete-'+val).attr('href');
	alertify.confirm("Confirm Deletion?", function (e) {
		if (e) {
			alertify.success("Record has been deleted successfully.");
			document.location.href = loc;
		} else {
			alertify.log("User has cancelled event");		
		}
	});
	//prevent link from being followed immediately on click
	return false;
}
</script>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
						<?php if(isset($callFromApp) && $callFromApp == true) { ?>
							<strong><?php echo ucfirst($mentor_details['MentorName']); ?>'s Submentors</strong>
						<?php } else { ?>
							<strong><?php echo $config[0]->mentorName?> Management</strong>
						<?php } ?>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <!--<a href="javascript:;" class="fa fa-times"></a>-->
                        </span>
                    </header>
                    <div class="panel-body">
						<?php if(isset($callFromApp) && $callFromApp == true) { ?>
							<a href="<?php echo base_url(); ?>admin/mentors/add?callFromApp=true&loggedInMentor=<?php echo $mentor_details['MentorID'];?>" class="btn btn-round btn-success" style="float:left; margin-right:15px;"><i class="livicon" data-s="16" data-n="plus-alt" data-c="#fff" data-hc="0" id="I15" style="width: 16px; height: 16px;"></i> Add</a>
						<?php } else { ?>
							<a href="<?php echo base_url(); ?>admin/mentors/add" class="btn btn-round btn-success" style="float:left; margin-right:15px;"><i class="livicon" data-s="16" data-n="plus-alt" data-c="#fff" data-hc="0" id="I15" style="width: 16px; height: 16px;"></i> Add</a>
						<?php } ?>
						
						<?php echo form_open_multipart('admin/bulkupload/genericUploader');?>
						<div class="input-field col s12">
						 
							<div class="input-field col s12">
								<div class="file-field input-field" style="float: left; background:#88a755; padding: 0px 20px; border-radius: 20px;">
									<span>Bulk Provision</span>
									<div class="btn" style="margin:0px 15px; background:none; border:none; box-shadow:none;">
										<input type="file" name="userfile" id="userfile" />
									</div>
									<button value="upload" class="waves-effect waves-light btn-large center" type="submit" id="submit">Submit</button>
								</div>
								<div class="input-field col s12">
									<span class="errCalss col s12" id="imgErr"><?php echo $error;?></span>
								</div>
								
							</div>
							<input type="hidden" name="tableName" value="mentor_mentor" id="tsbleName" />
							<input type="hidden" name="urlPath" value="mentors" />
							
						</div>
						<?php echo form_close(); ?>
						
						<?php
						$filename = "Mentor.csv";
						$chkFileExist = checkSampleFileExists($filename);
						if($chkFileExist === true) { ?>
							<a href="../../../mentor/assets/admin/csv/<?php echo $filename; ?>" target="_blank"  class="btn btn-round btn-success" style="float:right;"><i class="mdi-content-add prefix"></i>Download Sample File</a>
						<?php } else { ?>
							<a href="#" class="btn btn-round btn-success" onclick='alertify.error("<?php echo $chkFileExist; ?>");' style="float:right;"><i class="mdi-content-add prefix"></i>Download Sample File</a>
						<?php } ?>
						
                        <div class="adv-table">
							<label class='download_label'><?php echo MasterListText; ?></label>
                            <?php echo $this->table->generate(); ?>    
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!-- Init variables for DATATABLE.  Note: Please put Datatable js files at footer  -->
<script>
    var dattTableName = 'dynamic_table';
	<?php if(isset($callFromApp) && $callFromApp == true) { ?>
		var extraURL = '?callFromApp=true&userId=<?php echo $mentor_details['MentorID'];?>';
	<?php } else { ?>
		var extraURL = '';
	<?php } ?>
    var sAjaxSource = '<?php echo base_url(); ?>admin/mentors/datatable' + extraURL;
    var baseUrl  = '<?php echo base_url();?>';
</script>
<!-- Init variables for DATATABLE end -->