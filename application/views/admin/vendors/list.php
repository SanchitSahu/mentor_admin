<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>Vendor</strong>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <?php $userSession = $this->session->userdata('admin_data');
                              if ($userSession['UserType'] == "Admin") { # FOR ADMIN LOGIN ?>
                              <a href="<?php echo $this->config->item('base_url') . 'admin/vendors/add'; ?>" class="btn btn-round btn-success"><i class="livicon" data-s="16" data-n="plus-alt" data-c="#fff" data-hc="0" id="I15" style="width: 16px; height: 16px;"></i> Add</a>
                        <?php } ?>
                        <div class="adv-table">
                            <?php echo $this->table->generate(); ?>
                            <?php /* 
                            <table  class="display table table-bordered table-striped table-hover" id="dynamic-table">
                                <thead class="gridhead">
                                    <tr>
                                        <th></th>
                                        <th>Vendor Name</th>
                                        <th>Firm Name</th>
                                        <th>Username</th>
                                        <th>Email</th>                        
                                        <th>Gender</th>
                                        <th>Cellphone</th>
                                        <th>Phone No.</th>
                                        <th>Address</th>
                                        <th align="center">Status</th>
                                        <th align="center">Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($vendorsData)) {
                                        foreach ($vendorsData as $vendors) {
                                            ?>
                                            <tr class="gradeX">
                                                <td align="center"><input type="checkbox" /></td>
                                                <td><?php echo ucwords($vendors['vFirstName'].' '.$vendors['vLastName']); ?></td>
                                                <td><?php echo ucwords($vendors['vFirmName']); ?></td>
                                                <td><?php echo $vendors['vUserName']; ?></td>
                                                <td><?php echo $vendors['vEmail']; ?></td>
                                                <td><?php echo $vendors['eGender']; ?></td>
                                                <td><?php echo $vendors['vCellPhone']; ?></td>
                                                <td><?php echo $vendors['vPhoneNumber1']; ?></td>
                                                <td><?php echo ucwords($vendors['vAddress1'].', '.$vendors['vAddress2'].', '.$vendors['vCity'].' '.$vendors['vStateName'].' '.$vendors['vCountryName'].' -'.$vendors['vPostCode']); ?></td>
                                                <td align="center"><a href="" style="cursor:pointer;"><i class="livicon" data-s="18" data-n="check-circle" data-c="green" data-hc="0" id="<?php echo "vs".$vendors['iVendorId']; ?>" style="width: 16px; height: 16px;"></i></a></td>
                                                <td align="center"><a href="<?php echo $this->config->item('base_url'); ?>admin/vendors/edit/<?php echo $vendors['iVendorId']; ?>" style="cursor:pointer;"><i class="livicon" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="<?php echo "ve".$vendors['iVendorId']; ?>" style="width: 16px; height: 16px;"></i></a></td>                        
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table> 
                            */ ?> 
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!-- Init variables for DATATABLE.  Note: Please put Datatable js files at footer  -->
<script>
    var dattTableName ='dynamic_table';
    var sAjaxSource = '<?php echo base_url(); ?>admin/vendors/datatable';
    var baseUrl  = '<?php echo base_url();?>';
</script>
<!-- Init variables for DATATABLE end -->
