<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>Vendor</strong>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <?php
                            $action = ($this->uri->segment(4) == "") ? 'admin/vendors/add' : 'admin/vendors/edit/' . $this->uri->segment(4);
                            echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_vendors_form", 'method' => "post", "enctype" => 'multipart/form-data'));
                            ?>
                            <div class="form-group ">
                                <label for="vFirstName" class="control-label col-lg-3">First Name</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter First Name", 'name' => 'vFirstName', 'id' => 'vFirstName', 'value' => $vendors['vFirstName'])); ?>
                                    <?php echo form_error('vFirstName', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="vLastName" class="control-label col-lg-3">Last Name</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Last Name", 'name' => 'vLastName', 'value' => $vendors['vLastName'])); ?>
                                    <?php echo form_error('vLastName', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="vOwnerName" class="control-label col-lg-3">Owner Name</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Owner Name", 'name' => 'vOwnerName', 'value' => $vendors['vOwnerName'])); ?>
                                    <?php echo form_error('vOwnerName', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="vFirmName" class="control-label col-lg-3">Firm Name</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Owner Name", 'name' => 'vFirmName', 'value' => $vendors['vFirmName'])); ?>
                                    <?php echo form_error('vFirmName', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3">Photo</label>
                                <div class="col-md-9">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 200px;">
                                            <?php
                                            if (isset($vendors['iPictureURLId']) && $vendors['iPictureURLId'] != "") {
                                                $fileExistClass = 'fileupload-new';
                                                ?> 
                                                <img src="<?php echo $this->config->item('base_uploads') . "vendor/thumb/" . $vendors['iPictureURLId']; ?>" alt="" />
                                                <?php
                                            } else {
                                                $fileExistClass = 'fileupload-exists';
                                                ?>
                                                <img src="<?php echo $this->config->item('base_images') . "noimage.gif"; ?>" alt="" />
                                            <?php } ?>
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 200px; line-height: 20px;"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                <span class="<?php echo $fileExistClass ?>"><i class="fa fa-undo"></i> Change</span>
                                                <input type="file" class="default" name="iPictureURLId" id="iPictureURLId" />
                                            </span>
                                            <a href="#" class="btn btn-danger <?php echo $fileExistClass ?>" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                        </div>
                                        <?php echo form_error('vPictureURL', '<label class="error">', '</label>'); ?>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3">Logo</label>
                                <div class="col-md-9">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 200px;">
                                            <?php
                                            if (isset($vendors['iLogoURLID']) && $vendors['iLogoURLID'] != "") {
                                                $fileExistClass = 'fileupload-new';
                                                ?> 
                                                <img src="<?php echo $this->config->item('base_uploads') . "vendor/thumb/" . $vendors['iLogoURLID']; ?>" alt="" />
                                                <?php
                                            } else {
                                                $fileExistClass = 'fileupload-exists';
                                                ?>
                                                <img src="<?php echo $this->config->item('base_images') . "noimage.gif"; ?>" alt="" />
                                            <?php } ?>
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 200px; line-height: 20px;"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                <span class="<?php echo $fileExistClass ?>"><i class="fa fa-undo"></i> Change</span>
                                                <input type="file" class="default" name="iLogoURLID" id="iLogoURLID" />
                                            </span>
                                            <a href="#" class="btn btn-danger <?php echo $fileExistClass ?>" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                        </div>
                                        <?php echo form_error('vLogoURL', '<label class="error">', '</label>'); ?>
                                    </div>                                    
                                </div>
                            </div>
                            <?php /*
                              <div class="form-group ">
                              <label for="vUserName" class="control-label col-lg-3">User Name</label>
                              <div class="col-lg-6">
                              <?php $jsValFunUname = (isset($vendors['iVendorId']) && $vendors['iVendorId'] != "") ? 'javascript:UsernameEditCheck();' : 'javascript:UsernameCheck();'; ?>
                              <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter User Name", 'value' => $vendors['vUserName'], 'name' => 'vUserName', 'id' => 'vUserName', 'onblur' => $jsValFunUname)); ?>
                              <?php echo form_error('vUserName', '<label class="error">', '</label>'); ?>
                              <div class="custom-error" id="uname_check">
                              <label class="error">Username already exist</label>
                              </div>
                              </div>
                              </div> */ ?> 
                            <div class="form-group ">
                                <label for="vPassword" class="control-label col-lg-3">Password</label>
                                <div class="col-lg-6">
                                    <?php echo form_password(array('class' => "form-control", 'placeholder' => "Enter Password", 'name' => 'vPassword', 'id' => 'vPassword')); ?>
                                    <?php echo form_error('vPassword', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="vcPassword" class="control-label col-lg-3">Confirm Password</label>
                                <div class="col-lg-6">
                                    <?php echo form_password(array('class' => "form-control", 'placeholder' => "Enter Confirm Password", 'name' => 'vcPassword', 'id' => 'vcPassword')); ?>
                                    <?php echo form_error('vcPassword', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="vEmail" class="control-label col-lg-3">Email</label>
                                <div class="col-lg-6">
                                    <?php $jsValFun = (isset($vendors['iVendorId']) && $vendors['iVendorId'] != "") ? 'javascript:EmailEditCheck();' : 'javascript:EmailCheck();'; ?>
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Email", 'name' => 'vEmail', 'value' => $vendors['vEmail'], 'id' => 'vEmail', 'onblur' => $jsValFun)); ?>
                                    <?php echo form_error('vEmail', '<label class="error">', '</label>'); ?>
                                    <div class="custom-error" id="email_check">
                                        <label class="error">Email address already exist</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="vPersonalEmail" class="control-label col-lg-3">Personal Email</label>
                                <div class="col-lg-6">
                                    <?php $jsValFunPersonal = (isset($vendors['iVendorId']) && $vendors['iVendorId'] != "") ? 'javascript:PersonalEmailEditCheck();' : 'javascript:PersonalEmailCheck();'; ?>
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Personal Email", 'name' => 'vPersonalEmail', 'value' => $vendors['vPersonalEmail'], 'id' => 'vPersonalEmail', 'onblur' => $jsValFunPersonal)); ?>
                                    <?php echo form_error('vPersonalEmail', '<label class="error">', '</label>'); ?>
                                    <div class="custom-error" id="Personal_email_check">
                                        <label class="error">Email address already exist</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="eGender" class="control-label col-lg-3">Gender</label>
                                <div class="col-lg-6">
                                    <label class="radio-inline">
                                        <input type="radio" class="uniform" name="eGender" value="Male" <?php echo set_radio('Gender', 'Male', $vendors['eGender'] == 'Male'); ?> />Male
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" class="uniform" name="eGender" value="Female" <?php echo set_radio('Gender', 'Female', $vendors['eGender'] == 'Female'); ?> />Female
                                    </label>
                                    <?php echo form_error('eGender', '<label class="error">', '</label>'); ?>
                                    <br>
                                    <label class="error" for="eGender"></label>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="dBirthDate" class="control-label col-lg-3">Birth Date</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control form-control-inline input-medium default-date-picker", 'value' => $vendors['dBirthDate'], 'placeholder' => "Enter Birth Date", 'name' => 'dBirthDate')); ?>
                                    <?php echo form_error('dBirthDate', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="dAnniversaryDate" class="control-label col-lg-3">Anniversary Date</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control form-control-inline input-medium default-date-picker", 'value' => $vendors['dAnniversaryDate'], 'placeholder' => "Enter Anniversary Date", 'name' => 'dAnniversaryDate')); ?>
                                    <?php echo form_error('dAnniversaryDate', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="vCellPhone" class="control-label col-lg-3">Mobile No.</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Mobile No.", 'name' => 'vCellPhone', 'value' => $vendors['vCellPhone'])); ?>
                                    <?php echo form_error('vCellPhone', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="vPhoneNumber1" class="control-label col-lg-3">Phone No 1</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Phone No 1", 'name' => 'vPhoneNumber1', 'value' => $vendors['vPhoneNumber1'])); ?>
                                    <?php echo form_error('vPhoneNumber1', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="vPhoneNumber2" class="control-label col-lg-3">Phone No 2</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Phone No 2", 'name' => 'vPhoneNumber2', 'value' => $vendors['vPhoneNumber2'])); ?>
                                    <?php echo form_error('vPhoneNumber2', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <?php
                            if ($this->uri->segment(4) != "") {
                                $addressData = explode(',', $vendors['vAddress1']);
                            }
                            ?>
                            <div class="form-group ">
                                <label for="vBlock" class="control-label col-lg-3">Block No.</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Block", 'name' => 'vBlock', 'value' => (isset($addressData[0]) && $addressData[0] != "") ? trim($addressData[0]) : "")); ?>
                                    <?php echo form_error('vBlock', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="vAddress1" class="control-label col-lg-3">Address1</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Address1", 'name' => 'vAddress1', 'id' => 'vAddress1', 'value' => (isset($addressData[1]) && $addressData[1] != "") ? trim($addressData[1]) : "")); ?>
                                    <?php echo form_error('vAddress1', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="vAddress2" class="control-label col-lg-3">Address2</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Address2", 'name' => 'vAddress2', 'id' => 'vAddress2', 'value' => $vendors['vAddress2'])); ?>
                                    <?php echo form_error('vAddress2', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="countryname" class="control-label col-lg-3">Country</label>
                                <div class="col-lg-6">
                                    <select class="form-control" name="iCountryId" id="iCountryId" onchange="selectState(this.options[this.selectedIndex].value)">
                                        <option value="">Select Country</option>
                                        <?php foreach ($countryData as $country) { ?>
                                            <option value="<?php echo $country['iCountryId']; ?>"><?php echo ucwords($country['vCountryName']); ?></option>
                                        <?php } ?>
                                    </select>
                                    <?php echo form_error('iCountryId', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="statename" class="control-label col-lg-3">State</label>
                                <div class="col-lg-6">
                                    <select class="form-control" name="iStateId" id="iStateId">
                                        <option value="">Select State</option>
                                        <?php /* foreach ($stateData as $state) { ?>
                                          <option value="<?php echo $state['iStateId']; ?>"><?php echo ucwords($state['vStateName']); ?></option>
                                          <?php } */ ?>
                                    </select>
                                    <?php echo form_error('iStateId', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="vCity" class="control-label col-lg-3">City</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter City", 'name' => 'vCity', 'id' => 'vCity', 'value' => $vendors['vCity'])); ?>
                                    <?php echo form_error('vCity', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="vPostCode" class="control-label col-lg-3">Postcode</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Postcode", 'name' => 'vPostCode', 'id' => 'vPostCode', 'value' => $vendors['vPostCode'])); ?>
                                    <?php echo form_error('vPostCode', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="vTimeOpration" class="control-label col-lg-3">Time Opration</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Time Opration", 'name' => 'vTimeOpration', 'id' => 'vTimeOpration', 'value' => $vendors['vTimeOpration'])); ?>
                                    <?php echo form_error('vTimeOpration', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="vWebsite" class="control-label col-lg-3">Website</label>
                                <div class="col-lg-6">
                                    <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Website", 'name' => 'vWebsite', 'id' => 'vWebsite', 'value' => $vendors['vWebsite'])); ?>
                                    <?php echo form_error('vWebsite', '<label class="error">', '</label>'); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="eStatus" class="control-label col-lg-3">Status</label>
                                <div class="col-lg-6">
                                    <?php echo form_checkbox('eStatus', 'Active', TRUE); ?>
                                </div>
                            </div>
                            <!--                            <div class="form-group ">
                                                            <label for="eStatus" class="control-label col-lg-3">Location</label>
                                                            <div class="col-lg-6">
                                                                <input type="text" name="location" class="form-control" placeholder="Location" id="location"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label for="eStatus" class="control-label col-lg-3">Map</label>
                                                            <div class="col-lg-6">
                                                                <div id="us3" style="width: 550px; height: 400px;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="p-r-small col-sm-1 control-label">Lat.:</label>
                                                            <div class="col-sm-3"><input type="text" class="form-control" style="width: 110px" id="us3-lat"/></div>
                                                            <label class="p-r-small col-sm-2 control-label">Long.:</label>
                                                            <div class="col-sm-3"><input type="text" class="form-control" style="width: 110px" id="us3-lon"/></div>
                                                        </div>-->
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <button class="btn btn-3d-success" type="submit" onclick="successalertify();" >Save</button>
                                    <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                </div>
                            </div>
                            <input type="hidden" name="vendorid" id="vendorid" value="<?php echo $id; ?>" />   
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<script>
//    $('#us3').locationpicker({
//        location: {
//            latitude: 46.15242437752303,
//            longitude: 2.7470703125
//        },
//        radius: 0,
//        inputBinding: {
//            latitudeInput: $('#us3-lat'),
//            longitudeInput: $('#us3-lon'),
//            //radiusInput: $('#us3-radius'),
//            locationNameInput: $('#location')
//        },
//        enableAutocomplete: true,
//        onchanged: function (currentLocation, radius, isMarkerDropped) {
//            // Uncomment line below to show alert on each Location Changed event
//            //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
//        }
//    });
//    $(document).ready(function () {
//        $('.default-date-picker').datepicker({
//            format: 'yyyy-mm-dd'
//        });
//        $('#vAddress1').on('blur', function () {
//            $("#location").val($("#vAddress1").val());
//        });
//        $('#vAddress2').on('blur', function () {
//            var Add2 = $("#vAddress1").val() + ' ' + $("#vAddress2").val();
//            $("#location").val(Add2);
//        });
//        $('#iCountryId').on('change', function () {
//            var Country = $("#vAddress1").val() + ' ' + $("#vAddress2").val() + ' ' + $("#iCountryId option:selected").text();
//            $("#location").val(Country);
//        });
//        $('#iStateId').on('blur', function () {
//            var State = $("#vAddress1").val() + ' ' + $("#vAddress2").val() + ' ' + $("#iCountryId option:selected").text() + ' ' + $("#iStateId option:selected").text();
//            $("#location").val(State);
//        });
//        $('#vCity').on('blur', function () {
//            var City = $("#vAddress1").val() + ' ' + $("#vAddress2").val() + ' ' + $("#vCity").val() + ' ' + $("#iCountryId option:selected").text() + ' ' + $("#iStateId option:selected").text();
//            $("#location").val(City);
//        });
//        $('#vPostCode').on('blur', function () {
//            var PostCode = $("#vAddress1").val() + ' ' + $("#vAddress2").val() + ' ' + $("#vCity").val() + ' ' + $("#iStateId option:selected").text() + ' ' + $("#vPostCode").val() + ' ' + $("#iCountryId option:selected").text();
//            $("#location").val(PostCode);
//            $('body').focus();
//        });
//    });
</script>
<script type="text/javascript">
    
    function EmailCheck() {
        var email = $("#vEmail").val();
        if (email !== "") {
            jQuery.ajax({
                type: "POST",
                url: "<?php echo $this->config->item('base_url'); ?>admin/vendors/checkEmailExist",
                data: {email_address: email},
                success: function (output) {
                    if (output === "false") {
                        $("#email_check").show();
                        $("#vEmail").focus();
                        return false;
                    } else {
                        $("#email_check").hide();
                    }
                }
            });
        }
    }
    function EmailEditCheck() {
        var email = $("#vEmail").val();
        if (email !== "") {
            jQuery.ajax({
                type: "POST",
                url: "<?php echo $this->config->item('base_url'); ?>admin/vendors/checkEmailEditExist",
                data: {email_address: email, id: "<?php echo $vendors['iVendorId']; ?>"},
                success: function (output) {
                    if (output === "false") {
                        $("#email_check").show();
                        $("#vEmail").focus();
                        return false;
                    } else {
                        $("#email_check").hide();
                    }
                }
            });
        }
    }

    function PersonalEmailCheck() {
        var Personalemail = $("#vPersonalEmail").val();
        if (Personalemail !== "") {
            jQuery.ajax({
                type: "POST",
                url: "<?php echo $this->config->item('base_url'); ?>admin/vendors/checkPersonalEmailExist",
                data: {email_address: Personalemail},
                success: function (output) {
                    if (output === "false") {
                        $("#Personal_email_check").show();
                        $("#vPersonalEmail").focus();
                        return false;
                    } else {
                        $("#Personal_email_check").hide();
                    }
                }
            });
        }
    }
    function PersonalEmailEditCheck() {
        var Personalemail = $("#vPersonalEmail").val();
        if (Personalemail !== "") {
            jQuery.ajax({
                type: "POST",
                url: "<?php echo $this->config->item('base_url'); ?>admin/vendors/checkPersonalEmailEditExist",
                data: {email_address: Personalemail, id: "<?php echo $vendors['iVendorId']; ?>"},
                success: function (output) {
                    if (output === "false") {
                        $("#Personal_email_check").show();
                        $("#vPersonalEmail").focus();
                        return false;
                    } else {
                        $("#Personal_email_check").hide();
                    }
                }
            });
        }
    }

    function UsernameCheck() {
        var email = $("#vUserName").val();
        if (email !== "") {
            jQuery.ajax({
                type: "POST",
                url: "<?php echo $this->config->item('base_url'); ?>admin/vendors/checkUnameExist",
                data: {uname: email},
                success: function (output) {
                    if (output === "false") {
                        $("#uname_check").show();
                        $("#signup_uname").css({"background-color": "#fdcaca", "border-color": "#ffc3c3", "border-radius": "3px"});
                        $("#signup_uname").focus();
                        return false;
                    } else {
                        $("#signup_uname").css({"background-color": "", "border-color": "", "border-radius": ""});
                        $("#uname_check").hide();
                    }
                }
            });
        }
    }
    function UsernameEditCheck() {
        var email = $("#vUserName").val();
        if (email !== "") {
            jQuery.ajax({
                type: "POST",
                url: "<?php echo $this->config->item('base_url'); ?>admin/vendors/checkUnameEditExist",
                data: {uname: email, id: "<?php echo $vendors['iVendorId']; ?>"},
                success: function (output) {
                    if (output === "false") {
                        $("#uname_check").show();
                        $("#signup_uname").css({"background-color": "#fdcaca", "border-color": "#ffc3c3", "border-radius": "3px"});
                        $("#signup_uname").focus();
                        return false;
                    } else {
                        $("#signup_uname").css({"background-color": "", "border-color": "", "border-radius": ""});
                        $("#uname_check").hide();
                    }
                }
            });
        }
    }
    $(document).ready(function () {
        var countryId = null;
<?php if ($vendors['iCountryId'] != "") { ?>
            var countryId = <?php echo $vendors['iCountryId']; ?>;
<?php } ?>
        $("#iCountryId").val(countryId);

        var stateId = null;
<?php if ($vendors['iStateId'] != "") { ?>
            var stateId = <?php echo $vendors['iStateId']; ?>;
<?php } ?>
        $("#iStateId").val(stateId);

        var value = '<?php echo $vendors['eGender']; ?>';
        if (value !== "") {
            $("input[name=eGender][value=" + value + "]").attr('checked', 'checked');
        }
    });
</script>
<?php 
$flag = "true";
if($this->uri->segment(4) != ""){
    $flag = "false";
}
?>
<script type="text/javascript">
    function successalertify() {
       //  alertify.success("Record has been saved successfully.");
       alertify.success("Please wait....");
     }
     function erroralertify() {
        // alertify.error("There has been some Error occured.");
     }
     function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
	        window.location.href = "<?php echo $this->config->item('base_url').'admin/vendors'?>";
        },1000);
     }

    function selectState(country_id) {
        if (country_id !== "0") {
            loadState('state', country_id);
            $("#city_dropdown").html("<option value='0'>Select State</option>");
        } else {
            $("#state_dropdown").html("<option value='0'>Select State</option>");
        }
    }

    function loadState(loadType, loadId) {
        var dataString = 'loadType=' + loadType + '&loadId=' + loadId;
        $("#" + loadType + "_loader").show();
        $("#" + loadType + "_loader").fadeIn(400).html('Please wait... ');
        $.ajax({
            type: "POST",
            url: "<?php echo $this->config->item("base_url"); ?>vendors/loadState",
            data: dataString,
            cache: false,
            success: function (result) {
                //$("#signup_state").hide();
                $("#iStateId").html("<option value=''>Select " + loadType + "</option>");
                $("#iStateId").append(result);
            }
        });
    }
//    ,function (element) {
//                        if ($('#vendorid').val() !== "") {
//                            return false;
//                        } else {
//                            return true;
//                        }
//                    }


//function (element) {
//                        if ($('input[name="eGender"]:checked').length == 0) {
//                            return true;
//                        }
//                    }
    selectState('<?php echo $vendors['iCountryId'] ?>');
    
    $(document).ready(function () {
	$('.default-date-picker').datetimepicker({
		format: 'yyyy-mm-dd'
	});
    });
    $().ready(function () {
        // validate signup form on keyup and submit
        $("#add_vendors_form").validate({
            rules: {
                vFirstName: {
                    required: false,
                    minlength: 2
                },
                vLastName: {required: false},
                vOwnerName: {
                    required: true,
                    minlength: 2
                },
                vFirmName: {
                    required: false,
                    minlength: 2
                },
                vPassword: {
                    required: false
                },
                vcPassword: {
                    required: false,
                    equalTo: "#vPassword"
                },
                vEmail: {
                    required: false
                },
                vPersonalEmail: {
                    required: false
                },
                eGender: {
                    required: false
                },
                vCellPhone: {
                    required: false
                },
                vAddress1: "required",
                vAddress2: {
                    required: false
                },
                iCountryId: "required",
                iStateId: "required",
                vCity: "required",
                vPostCode: {
                    required: false
                }
            },
            messages: {
                vFirstName: {
                    required: "Please enter first name",
                    minlength: "Your first name must consist of at least 2 characters"
                },
                vLastName: "Please enter last name",
                vOwnerName: {
                    required: "Please enter owner name",
                    minlength: "Your owner name must consist of at least 2 characters"
                },
                vFirmName: {
                    required: "Please enter firm name",
                    minlength: "Your firm name must consist of at least 2 characters"
                },
                vPassword: "Please enter password ",
                vcPassword: {
                    required: "Please enter confirm password",
                    equalTo: "Password and confirm password should match"
                },
                vEmail: "Please enter email",
                vPersonalEmail: "Please select personal email",
                eGender: "Please select gender",
                vCellPhone: "Please enter cell phone",
                vAddress1: "Please enter address",
                iCountryId: "Please select country",
                vPostCode: "Please enter post code",
                iStateId: "Please select state",
                vCity: "Please enter city",
            }
        });

    });
    
</script>