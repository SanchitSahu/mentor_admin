<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>My Profile </strong>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form enctype="multipart/form-data" class="cmxform form-horizontal" class="cmxform form-horizontal" id="empForm" method="post">
                                <div class="form-group ">
                                    <label for="firstname" class="control-label col-lg-3">First Name</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('id' => "firstname", 'name' => 'firstname', 'class' => 'form-control', 'value' => set_value('firstname', $product['vFirstName']), 'placeholder' => 'Enter First Name', 'autofocus')) ?>
                                        <?php echo form_error('firstname', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="lastname" class="control-label col-lg-3">Last Name</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('id' => "lastname", 'name' => 'lastname', 'class' => 'form-control', 'value' => set_value('lastname', $product['vLastName']), 'placeholder' => 'Enter Last Name', 'autofocus')) ?>
                                        <?php echo form_error('lastname', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Photo</label>
                                    <div class="col-md-9">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 200px;">
                                                <?php if ($product['iPictureURLId'] != "") { ?>
                                                    <img src="<?php echo $this->config->item('base_url') . $product['iPictureURLId'] ?>" alt="" />
                                                <?php } else { ?>
                                                    <img src="http://www.placehold.it/200x200/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                <?php } ?>
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 200px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-white btn-file">
                                                    <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                    <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                    <input type="file" class="default" name="photo" />
                                                </span>
                                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                            </div>
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="radios">
                                    <label for="eGender" class="control-label col-lg-3">Gender</label>
                                    <label class="label_radio col-lg-3" for="radio-01">
                                        <input name="gender" id="radio-01" value="1" type="radio" checked  /> Male
                                    </label>
                                    <label class="label_radio col-lg-9 col-sm-6" for="radio-02">
                                        <input name="gender" id="radio-02" value="1" type="radio" /> Female
                                    </label>
                                </div>

                                <div class="form-group ">
                                    <label for="dBirthDate" class="control-label col-lg-3">Birth Date</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control form-control-inline input-medium default-date-picker", 'value' => $product['dBirthDate'], 'placeholder' => "Enter Birth Date", 'name' => 'dBirthDate')); ?>
                                        <?php echo form_error('dBirthDate'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="dAnniversaryDate" class="control-label col-lg-3">Anniversary Date</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control form-control-inline input-medium default-date-picker", 'value' => $product['dAnniversaryDate'], 'placeholder' => "Enter Anniversary Date", 'name' => 'dAnniversaryDate')); ?>
                                        <?php echo form_error('dAnniversaryDate'); ?>
                                    </div>
                                </div>                                

                                <div class="form-group">
                                    <label class="control-label col-lg-3">Mobile No.</label>
                                    <div class="col-lg-6">
                                        <input name="mobile" value="<?php echo $product['vCellPhone']; ?>" type="text" placeholder="" data-mask="(999) 999-9999" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Phone No.</label>
                                    <div class="col-lg-6">
                                        <input name="phone" value="<?php echo $product['vPhoneNumber1']; ?>" type="text" placeholder="" data-mask="(999) 999-9999" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="address1" class="control-label col-lg-3">Address1</label>
                                    <div class="col-lg-6">
                                        <input class=" form-control" id="Text1" name="address1" type="text" value="<?php echo $product['vAddress1']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="address2" class="control-label col-lg-3">Address2</label>
                                    <div class="col-lg-6">
                                        <input class=" form-control" id="address2" name="address2" type="text"  value="<?php echo $product['vAddress2']; ?>" />
                                    </div>
                                </div>                                    
                                <div class="form-group ">
                                    <label class="control-label col-lg-3 control-label">Country</label>
                                    <div class="col-lg-6">
                                        <!--<select class="form-control"  id="country" name="country">
                                            <option value="">Select Country</option>
                                            <option value="1">California</option>
                                            <option value="2">Nevada</option>
                                            <option value="3">Oregon</option>
                                            <option value="4">Washington</option>
                                        </select>-->
                                        <?php if ($id): ?>
                                            <?php echo form_dropdown('country', $country, set_value('country', $emp['iCountryId']), 'class="form-control" id="country"'); ?>
                                        <?php else: ?>
                                            <?php echo form_dropdown('country', $country, '', 'class="form-control" id="country"'); ?>
                                        <?php endif; ?>
                                        <?php echo form_error('country', '<label class="error">', '</label>'); ?>                                    
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-3 control-label">State</label>
                                    <div class="col-lg-6">
                                        <?php if ($id): ?>
                                            <?php echo form_dropdown('state', $state, set_value('state', $emp['iStateId']), 'class="form-control" id="state"'); ?>
                                        <?php else: ?>
                                            <?php echo form_dropdown('state', $state, '', 'class="form-control" id="state"'); ?>
                                        <?php endif; ?>
                                        <?php echo form_error('state', '<label class="error">', '</label>'); ?>  
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-3 control-label">City</label>
                                    <div class="col-lg-6">
                                        <input class=" form-control" id="city" name="city" type="text"  value="<?php echo $emp['vCity']; ?>" />
                                        <?php echo form_error('city', '<label class="error">', '</label>'); ?>  
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="postcode" class="control-label col-lg-3">Postcode</label>
                                    <div class="col-lg-6">
                                        <!--<input class=" form-control" id="postcode" name="postcode" type="text" />-->
                                        <?php echo form_input(array('id' => "postcode", 'value' => set_value('postcode', $product['vPostCode']), 'name' => 'postcode', 'class' => 'form-control', 'placeholder' => 'Enter Post Code')) ?>
                                        <?php echo form_error('postcode', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <input class=" form-control" id="email" name="email" type="hidden"  value="<?php echo $emp['vEmail']; ?>" />
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();" >Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>


<script type="text/javascript">
    // function successalertify() {
    //     alertify.success("Record has been saved successfully.");
    // }
    // function erroralertify() {
    //     alertify.error("There has been some Error occured.");
    // }
    // function logalertify() {
    //     alertify.log("User have canceled the event.");
    // }

    $().ready(function () {
        // validate signup form on keyup and submit
        $("#empForm").validate({
            rules: {
                firstname: "required",
                lastname: "required",
                empcode: "required",
                username: "required",
                password: "required",
                confirmpassword: "required",
                address1: "required",
                country: "required",
                state: "required",
                city: "required",
                postcode: "required",
                mobile: "required"
            },
            messages: {
                firstname: "Please enter first code",
                lastname: "Please enter last name",
                empcode: "Please provide a empcode",
                username: "Please select username",
                password: "Please enter password",
                confirmpassword: "Please enter confirmpassword",
                address1: "Please enter address",
                country: "Please enter country",
                state: "Please enter state",
                city: "Please enter city",
                postcode: "Please enter postcode",
                mobile: "Please enter mobile"
            }
        });
    });
</script>