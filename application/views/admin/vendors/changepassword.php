
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>Change Password</strong>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="form">                            
                            <form id="change_pass" enctype="multipart/form-data" class="cmxform form-horizontal" class="cmxform form-horizontal" id="empForm" method="post">
                                <div class="form-group ">
                                    <label for="firstname" class="control-label col-lg-3">Old Password</label>
                                    <div class="col-lg-6">
                                            <?php echo form_input(array('class'=>'form-contrl','type' => 'password', 'name' => 'OldPassword', 'class' => 'textbg', 'placeholder' => 'Old Password*')); ?>
                                            <?php echo form_error('OldPassword', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="lastname" class="control-label col-lg-3">New Password</label>
                                    <div class="col-lg-6">
                                            <?php echo form_input(array('class'=>'form-contrl','type' => 'password', 'name' => 'NewPassword', 'maxlength' => "25", 'id' => 'newPassword', 'class' => 'textbg', 'placeholder' => 'New Password*')); ?>
                                            <?php echo form_error('NewPassword', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                
                                <div class="form-group ">
                                    <label for="lastname" class="control-label col-lg-3">Confirm Password</label>
                                    <div class="col-lg-6">
                                            <?php echo form_input(array('class'=>'form-contrl','type' => 'password', 'name' => 'ConfirmPassword', 'maxlength' => "25", 'class' => 'textbg', 'placeholder' => 'Confirm Password*')) ?>
                                            <?php echo form_error('ConfirmPassword', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();" >Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>



<script>
   

    
    $(document).ready(function() {
        $('#change_pass').validate({
            rules: {
                "OldPassword": {
                    "required": true
                },
                "NewPassword": {
                    "required": true,
                    "minlength": 6
                },
                "ConfirmPassword": {
                    "required": true,
                    "equalTo": "#newPassword"
                }
            },
            messages: {
                "OldPassword": {
                    "required": "Please enter old password."
                },
                "NewPassword": {
                    "required": "Please enter new password.",
                    "minlength": "Please enter at least {0} characters."
                }, 
                "ConfirmPassword": {
                    "required": "Please enter confirm password.",
                    "equalTo": "Password does not match the confirm password."
                }
            }
        });
    });
</script>