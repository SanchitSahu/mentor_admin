<?php
if ($abbreviations['Weight'] == "")
    $abbreviations['Weight'] = 1000;
else
    $abbreviations['Weight'] = $abbreviations['Weight'];
?>

<section id="container" >
    <?php $res['config'] = $this->settings_model->getConfig(); ?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong><?php echo ($this->uri->segment(4) == "") ? 'Add' : 'Edit'; ?> Abbreviations</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'admin/abbreviations/add' : 'admin/abbreviations/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_abbreviation_form", 'method' => "post"));
                                ?>
                                <div class="form-group ">
                                    <label for="AbbreviationKey" class="control-label col-lg-3">Abbreviation Key</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Abbreviation Key", 'name' => 'AbbreviationKey', 'value' => $abbreviations['AbbreviationKey'])); ?>
                                        <?php echo form_error('AbbreviationKey'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="AbbreviationFullform" class="control-label col-lg-3">Abbreviation Fullform</label>
                                    <div class="col-lg-6">
                                        <input type="hidden" id="AbbreviationId" name="AbbreviationId" value="<?php echo set_value('AbbreviationId', $abbreviations['AbbreviationId']); ?>" />
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Abbreviation Fullform", 'name' => 'AbbreviationFullform', 'value' => $abbreviations['AbbreviationFullform'])); ?>
                                        <?php echo form_error('AbbreviationFullform'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-3">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight Name", 'name' => 'Weight', 'value' => $abbreviations['Weight'])); ?>
                                        <?php echo form_error('Weight'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
    function successalertify() {
        //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
    }
    function erroralertify() {
        //   alertify.error("There has been some Error occured.");
    }
    function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'admin/abbreviations' ?>";
        }, 1000);
    }


    $().ready(function () {
        // validate signup form on keyup and submit
        $("#add_abbreviation_form").validate({
            rules: {
                AbbreviationKey: {
                    required: true,
                    remote: {
                        url: "<?php echo $this->config->item('base_url'); ?>admin/abbreviations/checkAbbreviationsExist",
                        type: "post",
                        data: {
                            AbbreviationId: function () {
                                return $('#AbbreviationId').val();
                            }
                        }
                    }
                },
                AbbreviationFullform: {
                    required: true
                },
                Weight: {
                    required: true
                }
            },
            messages: {
                AbbreviationKey: {
                    required: "Please enter Abbreviation Key",
                    remote: "Abbreviation Key already Exists"
                },
                AbbreviationFullform: {
                    required: "Please enter Abbreviation Fullform"
                },
                Weight: {
                    required: "Please enter Weight"
                }
            }
        });

    });
</script>