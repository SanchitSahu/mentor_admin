<?php //echo "<pre>";print_r($mentor);exit;?>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
<div class=" container-fluid">
  <div class="row">
    <div style="overflow:hidden;">
    <div class="col-lg-12 res-mange">
      <h2><strong>Relationship Manager</strong></h2>
      <div class="row">
        <div class="col-lg-5 col-sm-12 col-md-6">
          <h3>Mentor Panel</h3>
         <div class="top-col">
           <div class="col-lg-12 select-mentor">
			<div class="col-lg-4"><strong>Skills Needed</strong></div>
			<div class="col-lg-8">
				<select name="mentorSkillsDrp" id="mentorSkillsDrp" class="form-control">
				  <option value="0">Any or All</option>
					<?php foreach($skill as $val){?>
					<option value="<?php echo $val->SkillID; ?>"><?php echo $val->SkillName;?></option>
					<?php } ?>
				</select>
			</div>	
          </div>
          <!--<div class="col-lg-6 select-option">
            <select class="form-control">
              <option disabled="disabled">Expirence</option>
              <option>1 year</option>
              <option>2 year</option>
              <option>3 year</option>
              <option>4 year</option>
            </select>
          </div>-->
          <div class="col-lg-12 select-mentor">
            <div class="col-lg-4"><strong>Select Mentor</strong></div>
            <div class="col-lg-8">
				<select name="mentorDrp" id="mentorDrp" class="form-control">
                <option value="0">Select Mentor</option>
				<?PHP foreach($mentor as $val){	?>
					<option value="<?php echo $val->MentorID;?>"><?php echo $val->MentorName;?></option>
                <?php } ?>
				</select>
            </div>
          </div>
         </div>
          <div class="mentor-block" id="mentorBlock">
            <div class="user-slider">
              <div>
                <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
                  <!-- Indicators --> 
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
					
					<?php $j=0;
					foreach($mentor as $val){
					
					$assignedMentee	=	explode(",",$val->assignedMentee); 	
					if($assignedMentee[0]	!=	"")
						$cnt			=	count($assignedMentee);	
					else
						$cnt	=0;
						?>
                    <div class="item <?php if($j == 0) echo "active"?>" id="<?php echo $val->MentorID?>">
                      <div class="user-img"><img src="<?php echo base_url()?>/assets/admin/images/<?php echo $val->MentorID.".jpg";?>" class="img-circle"></div>
                      <div class="user-name"><?php echo $val->MentorName ;?><span><a href="#">(<?php echo $cnt;?>)
                        <div class="connect-friend">
                          <ul>
							<?php if($val->assignedMentee == ""){ ?>
									<li>No Mentee Assigned</li>
							<?php }else {
								
								foreach($assignedMentee as $s) { ?>
									<li><?php echo $s;?></li>
							<?php } }?>
                          </ul>
                        </div>
                        </a> </span> </div>
                      <form>
                        <?php $skillArr	=	explode(",",$val->skill); ?>
                        <textarea readonly name="skill" cols="" rows="" placeholder="No skills specified yet"><?php foreach($skillArr as $s)echo $s."\n" ?></textarea>
                      </form>
                    </div>
                    <?php $j++; } ?>
                  </div>
                  
                  <!-- Left and right controls --> 
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <span class="left-errow" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <span class="right-errow" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
              </div>
            </div>
          </div>
        </div>
		
		
        <div class="col-lg-2 col-sm-12 col-md-6 button-block">
          <a type="button" data-toggle="tooltip" title="Form a relationship between the current Mentor and Mentee" class="helper green-button"><img src="<?php echo base_url()?>/assets/admin/images/btn1.png"></a>
          <a type="button" data-toggle="tooltip" title="Remove the relationship between the current Mentor and Mentee" class="helper red-button"><img src="<?php echo base_url()?>/assets/admin/images/btn2.png"></a>
        </div>
        <div class="col-lg-5 col-sm-12 col-md-6">
          <h3>Mentee Panel</h3>
       <div class="top-col">
          <div class="col-lg-12 select-mentor">
			<div class="col-lg-4"><strong>Skills Available</strong></div>
			<div class="col-lg-8">
            <select name="menteeSkillsDrp" id="menteeSkillsDrp" class="form-control">
              <option value="0">Not Specified</option>
				<?php foreach($skill as $val){?>
				<option value="<?php echo $val->SkillID; ?>"><?php echo $val->SkillName;?></option>
				<?php } ?>
            </select>
			</div>
			
          </div>
          <div class="col-lg-6 select-option">
            <!--<select class="form-control">
              <option disabled="disabled">Expirence</option>
              <option>1 year</option>
              <option>2 year</option>
              <option>3 year</option>
              <option>4 year</option>
            </select>-->
          </div>
          <div class="col-lg-12 select-mentor">
            <div class="col-lg-4"><strong>Select Mentee</strong></div>
            <div class="col-lg-8">
              <select name="menteeDrp" id="menteeDrp" class="form-control">
                <option value="0">Select Mentee</option>
                <?PHP foreach($mentee as $val){	?>
					<option value="<?php echo $val->MenteeID;?>"><?php echo $val->MenteeName;?></option>
                <?php } ?>
              </select>
            </div>
          </div>
         </div>
          <div class="mentor-block" id="menteeBlock">
            <div class="user-slider">
              <div>
                <div id="myCarousel1" class="carousel slide" data-ride="carousel"> 
                  <!-- Indicators --> 
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
					<?php $k = 0;
					foreach($mentee as $val){
						$assignedMentor	=	explode(",",$val->assignedMentor); 
						if($assignedMentor[0]	!=	"")
							$cnt	=	count($assignedMentor);
						else
							$cnt	=0;
						?>
                    <div class="item <?php if($k == 0) echo "active"?>" id="<?php echo $val->MenteeID?>">
                      <div class="user-img"><img src="<?php echo base_url()?>/assets/admin/images/<?php echo $val->MenteeID.".jpg";?>" class="img-circle"></div>
                      <div class="user-name"><?php echo $val->MenteeName ;?><span><a href="#">(<?php echo $cnt;?>)
                        <div class="connect-friend">
                          <ul>
                            <?php if($val->assignedMentor == ""){ ?>
									<li>No Mentor Assigned</li>
							<?php }else {
								$assignedMentor	=	explode(",",$val->assignedMentor); 
								foreach($assignedMentor as $s) { ?>
									<li><?php echo $s;?></li>
							<?php } }?>
                          </ul>
                        </div>
                        </a> </span> </div>
                      <form>
						<?php $skillArr	=	explode(",",$val->skill); ?>
                        <textarea readonly name="skill" cols="" rows="" placeholder="No skills specified yet"><?php foreach($skillArr as $s)echo $s."\n" ?></textarea>
                        
                      </form>
                    </div>
                    <?php $k++; } ?>
                  </div>
                  
                  <!-- Left and right controls --> 
                  <a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev"> <span class="left-errow" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next"> <span class="right-errow" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div></div>
  </div>
</div>
</section>
</section>

<script>
$(document).ready(function(){
    //$('[data-toggle="tooltip"]').tooltip();   
	$('.helper').tooltip({'placement': 'bottom'})
$('.helper').on('click', function(){console.log(this)});

$('.carousel').carousel({
    pause: true,
    interval: false
});
});
</script>

<script type="text/javascript">
$('.carousel').carousel({
    pause: true,
    interval: false
});
</script>
<script>

$( document ).ready(function() {
	
	var drpMentor	=	$("#mentorDrp").val();
	var menteeDrp	=	$("#menteeDrp").val();
	//alert(drpMentor);alert(menteeDrp);
	
	if(drpMentor == 0 || menteeDrp == 0)
	{
		$.ajax({
			url: '<?php echo base_url();?>admin/relationship/getDummyProfile',
			success: function(result){
				//alert(result);
				var data	= result.split("|");
				$("#myCarousel").find('div.active').removeClass('active');
				$("#myCarousel1").find('div.active').removeClass('active');
				$("#myCarousel .carousel-inner").append(data[0]);
				$("#myCarousel1 .carousel-inner").append(data[1]);
				$(".green-button").attr("disabled","disabled");
				$(".green-button").css("opacity","0.3");
				
				$(".red-button").attr("disabled","disabled");
				$(".red-button").css("opacity","0.3");
				
				/*$("#myCarousel .left-errow").parent( ".carousel-control" ).removeAttr("href");
				$("#myCarousel1 .left-errow").parent( ".carousel-control" ).removeAttr("href");
				
				$("#myCarousel .right-errow").parent( ".carousel-control" ).removeAttr("href");
				$("#myCarousel1 .right-errow").parent( ".carousel-control" ).removeAttr("href");*/
				
			}
		});	
	}else{
		var mentor	=	$("#myCarousel").find('div.active').attr('id');
		var mentee	=	$("#myCarousel1").find('div.active').attr('id');
		//alert(mentor);alert(mentee);
		
		$.ajax({
			url: '<?php echo base_url();?>admin/relationship/checkRelation',
			data:"mentor="+mentor+"&mentee="+mentee,
			success: function(result){
				if(result > 0){
					//alert(result);
					$(".green-button").attr("disabled","disabled");
					$(".green-button").css("opacity","0.3");
					
					$(".red-button").removeAttr("disabled");
					$(".red-button").css("opacity","1");
					
				}else{
					
					$(".green-button").removeAttr("disabled");
					$(".green-button").css("opacity","1");
					
					$(".red-button").attr("disabled","disabled");
					$(".red-button").css("opacity","0.3");
					
				}	
			}
		});	
	}	
});

$( ".green-button" ).click(function() {
	var mentor	=	$("#myCarousel").find('div.active').attr('id');
	var mentee	=	$("#myCarousel1").find('div.active').attr('id');
	//alert(mentor);alert(mentee);
	$.ajax({
		url: '<?php echo base_url();?>admin/relationship/addRelation',
		data:"mentor="+mentor+"&mentee="+mentee,
		success: function(result){
			alertify.log(result);
			window.location.reload();
		}
	});	
});


$( ".red-button" ).click(function() {
	var mentor	=	$("#myCarousel").find('div.active').attr('id');
	var mentee	=	$("#myCarousel1").find('div.active').attr('id');
	//alert(mentor);alert(mentee);
	$.ajax({
		url: '<?php echo base_url();?>admin/relationship/removeRelation',
		data:"mentor="+mentor+"&mentee="+mentee,
		success: function(result){
			alertify.log(result);
			window.location.reload();
		}
	});	
});


$("#mentorDrp").change(function(){
	var mentorID	=	this.value;
	var mentee	=	$("#myCarousel1").find('div.active').attr('id');
	//alert(mentee);alert(mentorID);
	
	if(mentorID	==	0){
		$.ajax({
			url: '<?php echo base_url();?>admin/relationship/getDummyProfile',
			success: function(result){
				//alert(result);
				var data	= result.split("|");
				$("#myCarousel").find('div.active').removeClass('active');
				$("#myCarousel1").find('div.active').removeClass('active');
				$("#myCarousel .carousel-inner").append(data[0]);
				$("#myCarousel1 .carousel-inner").append(data[1]);
				$(".green-button").attr("disabled","disabled");
				$(".green-button").css("opacity","0.3");
				
				$(".red-button").attr("disabled","disabled");
				$(".red-button").css("opacity","0.3");
				
				/*$("#myCarousel .left-errow").parent( ".carousel-control" ).removeAttr("href");
				$("#myCarousel1 .left-errow").parent( ".carousel-control" ).removeAttr("href");
				
				$("#myCarousel .right-errow").parent( ".carousel-control" ).removeAttr("href");
				$("#myCarousel1 .right-errow").parent( ".carousel-control" ).removeAttr("href");*/
				
			}
		});	
	}else{
		$("#myCarousel").find('div.active').removeClass('active');
		$("#myCarousel").find("[id="+mentorID+"]").addClass('active');
		//var t=	$("[id="+mentorID+"]").addClass('active');
		
		if(mentee	==	"noMentee"){
			$(".green-button").attr("disabled");
			$(".green-button").css("opacity","0.3");
			
			$(".red-button").attr("disabled","disabled");
			$(".red-button").css("opacity","0.3");
		}else{
			
			if(mentorID != 0 && mentee != 0 && mentorID	!= "noMentor" && mentee != "noMentee"){
				$.ajax({
				url: '<?php echo base_url();?>admin/relationship/checkRelation',
				data:"mentor="+mentorID+"&mentee="+mentee,
				success: function(result){
					if(result > 0){
						//alert(result);
						$(".green-button").attr("disabled","disabled");
						$(".green-button").css("opacity","0.3");
						
						$(".red-button").removeAttr("disabled");
						$(".red-button").css("opacity","1");
						
					}else{
						
						$(".green-button").removeAttr("disabled");
						$(".green-button").css("opacity","1");
						
						$(".red-button").attr("disabled","disabled");
						$(".red-button").css("opacity","0.3");
						
					}	
				}
				});	
			}else{
				$(".green-button").attr("disabled","disabled");
				$(".green-button").css("opacity","0.3");
				
				$(".red-button").attr("disabled","disabled");
				$(".red-button").css("opacity","0.3");
			}
		}
		
	}
	
	
	$('#noMentor').remove();
	
});


$("#menteeDrp").change(function(){
	var menteeID	=	this.value;
	var mentor	=	$("#myCarousel").find('div.active').attr('id');
	//alert(mentor);
	
	if(menteeID	==	0){
		$.ajax({
			url: '<?php echo base_url();?>admin/relationship/getDummyProfile',
			success: function(result){
				//alert(result);
				var data	= result.split("|");
				$("#myCarousel").find('div.active').removeClass('active');
				$("#myCarousel1").find('div.active').removeClass('active');
				$("#myCarousel .carousel-inner").append(data[0]);
				$("#myCarousel1 .carousel-inner").append(data[1]);
				$(".green-button").attr("disabled","disabled");
				$(".green-button").css("opacity","0.3");
				
				$(".red-button").attr("disabled","disabled");
				$(".red-button").css("opacity","0.3");
				
				/*$("#myCarousel .left-errow").parent( ".carousel-control" ).removeAttr("href");
				$("#myCarousel1 .left-errow").parent( ".carousel-control" ).removeAttr("href");
				
				$("#myCarousel .right-errow").parent( ".carousel-control" ).removeAttr("href");
				$("#myCarousel1 .right-errow").parent( ".carousel-control" ).removeAttr("href");*/
				
			}
		});	
	}else{
		$("#myCarousel1").find('div.active').removeClass('active');
		$("#myCarousel1").find("[id="+menteeID+"]").addClass('active');
		//var t=	$("[id="+mentorID+"]").addClass('active');
		//alert(mentor);
		if(mentor	==	"noMentor"){
			$(".green-button").attr("disabled","disabled");
			$(".green-button").css("opacity","0.3");
			
			$(".red-button").attr("disabled","disabled");
			$(".red-button").css("opacity","0.3");
		}else{
			if(mentor != 0 && menteeID != 0 && mentor	!= "noMentor" && menteeID != "noMentee"){
				
				$.ajax({
				url: '<?php echo base_url();?>admin/relationship/checkRelation',
				data:"mentor="+mentor+"&mentee="+menteeID,
				success: function(result){
					if(result > 0){
						//alert(result);
						$(".green-button").attr("disabled","disabled");
						$(".green-button").css("opacity","0.3");
						
						$(".red-button").removeAttr("disabled");
						$(".red-button").css("opacity","1");
						
					}else{
						
						$(".green-button").removeAttr("disabled");
						$(".green-button").css("opacity","1");
						
						$(".red-button").attr("disabled","disabled");
						$(".red-button").css("opacity","0.3");
						
					}	
				}
				});	
			}else{
				$(".green-button").attr("disabled","disabled");
				$(".green-button").css("opacity","0.3");
				
				$(".red-button").attr("disabled","disabled");
				$(".red-button").css("opacity","0.3");
			}
		}
			
		$('#noMentee').remove();
	}	
	
});

$("#mentorSkillsDrp").change(function (){
	var skillID	=	this.value;
	//alert(skillID);
	$.ajax({
		url: '<?php echo base_url();?>admin/relationship/getMentorBySkill',
		data:"skillID="+skillID,
		success: function(result){
			//alert(result);
			var data	= result.split("|");
			$("#mentorBlock").html(data[0]);
			$("#mentorDrp").empty();
			$("#mentorDrp").append(data[1]);
			
			var mentor	=	$("#myCarousel").find('div.active').attr('id');
			var mentee	=	$("#myCarousel1").find('div.active').attr('id');
			//alert(mentor);alert(mentee);
			
			if(mentor != 0 && mentee != 0 && mentor	!= "noMentor" && mentee != "noMentee"){
				
				$.ajax({
					url: '<?php echo base_url();?>admin/relationship/checkRelation',
					data:"mentor="+mentor+"&mentee="+mentee,
					success: function(result){
						if(result > 0){
							//alert(result);
							$(".green-button").attr("disabled","disabled");
							$(".green-button").css("opacity","0.3");
							
							$(".red-button").removeAttr("disabled");
							$(".red-button").css("opacity","1");
							
						}else{
							
							$(".green-button").removeAttr("disabled");
							$(".green-button").css("opacity","1");
							
							$(".red-button").attr("disabled","disabled");
							$(".red-button").css("opacity","0.3");
							
						}	
					}
				});	
			
			}else{
				$(".green-button").attr("disabled","disabled");
				$(".green-button").css("opacity","0.3");
				
				$(".red-button").attr("disabled","disabled");
				$(".red-button").css("opacity","0.3");
			}
		}
	});	
	
});


$("#menteeSkillsDrp").change(function (){
	var skillID	=	this.value;
	//alert(skillID);
	$.ajax({
		url: '<?php echo base_url();?>admin/relationship/getMenteeBySkill',
		data:"skillID="+skillID,
		success: function(result){
			//alert(result);
			var data	= result.split("|");
			$("#menteeBlock").html(data[0]);
			$("#menteeDrp").empty();
			$("#menteeDrp").append(data[1]);
			
			var mentor	=	$("#myCarousel").find('div.active').attr('id');
			var mentee	=	$("#myCarousel1").find('div.active').attr('id');
			//alert(mentor);alert(mentee);
			
			if(mentor != 0 && mentee != 0 && mentor	!= "noMentor" && mentee != "noMentee"){
				
				$.ajax({
					url: '<?php echo base_url();?>admin/relationship/checkRelation',
					data:"mentor="+mentor+"&mentee="+mentee,
					success: function(result){
						if(result > 0){
							//alert(result);
							$(".green-button").attr("disabled","disabled");
							$(".green-button").css("opacity","0.3");
							
							$(".red-button").removeAttr("disabled");
							$(".red-button").css("opacity","1");
							
						}else{
							
							$(".green-button").removeAttr("disabled");
							$(".green-button").css("opacity","1");
							
							$(".red-button").attr("disabled","disabled");
							$(".red-button").css("opacity","0.3");
							
						}	
					}
				});	
			
			}else{
				$(".green-button").attr("disabled","disabled");
				$(".green-button").css("opacity","0.3");
				
				$(".red-button").attr("disabled","disabled");
				$(".red-button").css("opacity","0.3");
			}	
		}
	});	
	
});


$( "#myCarousel .right-errow" ).click(function() {
	
	var currentMentor	=	$("#myCarousel").find('div.active').attr('id');
	//alert(currentMentor);
	var mentee	=	$("#myCarousel1").find('div.active').attr('id');
	
	if(currentMentor	==	"noMentor"){
		return false;
	}else{
		
		if(mentee	==	"noMentee"){
			$(".green-button").attr("disabled");
			$(".green-button").css("opacity","0.3");
			
			$(".red-button").attr("disabled","disabled");
			$(".red-button").css("opacity","0.3");
		}else{
			var mentor	=	$("#myCarousel").find('div.active').next().attr('id');
			var last	=	$("#myCarousel").find('div.active').next().length;
			//alert(mentor);alert(last);
			if(last == 0){
				//last child
				var mentor	=	$(".carousel-inner div:first-child").attr("id");
			}
			
			//alert(mentor);alert(mentee);
			$.ajax({
				url: '<?php echo base_url();?>admin/relationship/checkRelation',
				data:"mentor="+mentor+"&mentee="+mentee,
				success: function(result){
					if(result > 0){
						//alert(result);
						$(".green-button").attr("disabled","disabled");
						$(".green-button").css("opacity","0.3");
						
						$(".red-button").removeAttr("disabled");
						$(".red-button").css("opacity","1");
						
					}else{
						
						$(".green-button").removeAttr("disabled");
						$(".green-button").css("opacity","1");
						
						$(".red-button").attr("disabled","disabled");
						$(".red-button").css("opacity","0.3");
						
					}	
				}
			});	
		}
		
	}
	
	
	
	
	
});


$( "#myCarousel .left-errow" ).click(function() {
	
	var currentMentor	=	$("#myCarousel").find('div.active').attr('id');
	var mentee	=	$("#myCarousel1").find('div.active').attr('id');
	if(currentMentor	==	"noMentor"){
		return false;
	}else{
		if(mentee	==	"noMentee"){
			$(".green-button").attr("disabled");
			$(".green-button").css("opacity","0.3");
			
			$(".red-button").attr("disabled","disabled");
			$(".red-button").css("opacity","0.3");
		}else{
			var mentor	=	$("#myCarousel").find('div.active').prev().attr('id');
			var last	=	$("#myCarousel").find('div.active').prev().length;
			if(last == 0){
				//first child
				var mentor	=	$('#myCarousel').children('div').children('div').last().attr("id");
				//alert(mentor);
			}
			
			//alert(mentor);alert(mentee);
			$.ajax({
				url: '<?php echo base_url();?>admin/relationship/checkRelation',
				data:"mentor="+mentor+"&mentee="+mentee,
				success: function(result){
					if(result > 0){
						//alert(result);
						$(".green-button").attr("disabled","disabled");
						$(".green-button").css("opacity","0.3");
						
						$(".red-button").removeAttr("disabled");
						$(".red-button").css("opacity","1");
						
					}else{
						
						$(".green-button").removeAttr("disabled");
						$(".green-button").css("opacity","1");
						
						$(".red-button").attr("disabled","disabled");
						$(".red-button").css("opacity","0.3");
						
					}	
				}
			});	
		}
		
	}
	
	
	
});




$( "#myCarousel1 .right-errow" ).click(function() {
	
	var currentMentee	=	$("#myCarousel1").find('div.active').attr('id');
	var mentor	=	$("#myCarousel").find('div.active').attr('id');
	if(currentMentee	==	"noMentee"){
		return false;
	}else{
		if(mentor	==	"noMentor"){
			$(".green-button").attr("disabled");
			$(".green-button").css("opacity","0.3");
			
			$(".red-button").attr("disabled","disabled");
			$(".red-button").css("opacity","0.3");
		}else{
			var last	=	$("#myCarousel1").find('div.active').next().length;
			var mentee	=	$("#myCarousel1").find('div.active').next().attr('id');
			//alert(last);
			if(last == 0){
				//last child
				var mentee	=	$("#myCarousel1 .carousel-inner div:first-child").attr("id");
			}
			
			//alert(mentor);alert(mentee);
			$.ajax({
				url: '<?php echo base_url();?>admin/relationship/checkRelation',
				data:"mentor="+mentor+"&mentee="+mentee,
				success: function(result){
					if(result > 0){
						//alert(result);
						$(".green-button").attr("disabled","disabled");
						$(".green-button").css("opacity","0.3");
						
						$(".red-button").removeAttr("disabled");
						$(".red-button").css("opacity","1");
						
					}else{
						
						$(".green-button").removeAttr("disabled");
						$(".green-button").css("opacity","1");
						
						$(".red-button").attr("disabled","disabled");
						$(".red-button").css("opacity","0.3");
						
					}	
				}
			});	
		}
		
		
		
	}	
	
	
});


$( "#myCarousel1 .left-errow" ).click(function() {
	
	var currentMentee	=	$("#myCarousel1").find('div.active').attr('id');
	var mentor	=	$("#myCarousel").find('div.active').attr('id');
	if(currentMentee	==	"noMentee"){
		return false;
	}else{
		if(mentor	==	"noMentor"){
			$(".green-button").attr("disabled");
			$(".green-button").css("opacity","0.3");
			
			$(".red-button").attr("disabled","disabled");
			$(".red-button").css("opacity","0.3");
		}else{
			var mentee	=	$("#myCarousel1").find('div.active').prev().attr('id');
			var last	=	$("#myCarousel1").find('div.active').prev().length;
			if(last == 0){
				//first child
				var mentee	=	$('#myCarousel1').children('div').children('div').last().attr("id");
				
				//alert(mentee);
			}
			
			//alert(mentor);alert(mentee);
			$.ajax({
				url: '<?php echo base_url();?>admin/relationship/checkRelation',
				data:"mentor="+mentor+"&mentee="+mentee,
				success: function(result){
					if(result > 0){
						//alert(result);
						$(".green-button").attr("disabled","disabled");
						$(".green-button").css("opacity","0.3");
						
						$(".red-button").removeAttr("disabled");
						$(".red-button").css("opacity","1");
						
					}else{
						
						$(".green-button").removeAttr("disabled");
						$(".green-button").css("opacity","1");
						
						$(".red-button").attr("disabled","disabled");
						$(".red-button").css("opacity","0.3");
						
					}	
				}
			});
		}	
		
		
	}	
	
	
});
</script>