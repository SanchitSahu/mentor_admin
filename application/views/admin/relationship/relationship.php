<?php if(isset($callFromApp) && $callFromApp == true) { ?>
    <style>
        .header.fixed-top.clearfix{
            display: none;
        }
        body {
            margin-top: 0px !important;
        }
		footer{
			display:none;
		}
		#main-content{
			margin-left: 0px;
		}
		aside{
			display: none;
		}
		.wrapper{
			margin-top: 10px;
		}
    </style>
<?php } ?>


<style type="text/css">
	@media (max-width: 992px) {
		.relationship-btns a.green-button,.relationship-btns a.red-button {
			margin: 0;
			width: 46%;
			margin: 10px;
		}
	}
	@media (max-width: 768px) {
		.subadminName {
			margin-left: 0;
		}
		.subadminName h1 {
		    float: left;
		    margin: 15px 0 0 20px;
		}
		.subadminName ul {
		    float: right;
		    margin: 20px 20px 0 0;
		    position: static;
		}
	}
	@media (max-width: 640px) {
		.relationship-btns a.green-button,.relationship-btns a.red-button {
			width: 100%;
			width: 42%;
			margin: 10px;
		}
	}
</style>



<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.highlighttextarea.js"></script>
<link href="<?php echo $this->config->item('base_css'); ?>jquery.highlighttextarea.css" rel="stylesheet" />
<?php //echo "<pre>";print_r($mentor);exit;       ?>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class=" container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong><?php echo $config[0]->mentorName ?> /  <?php echo $config[0]->menteeName ?> Relationships</strong>
                            <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <!--<a href="javascript:;" class="fa fa-times"></a>-->
                            </span>
                        </header>
                        <div style="overflow:hidden; width: 100%;">
                            <div class="col-lg-12 res-mange">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-12 col-md-6 button-block" style="float: none; margin: 0px auto;">
                                        <div class="select-mentor">
                                            <div class="col-lg-12"><h3>Skills needed</h3></div>
                                            <div class="col-lg-12">
                                                <select name="mentorSkillsDrp" id="mentorSkillsDrp" class="form-control">
                                                    <option value="">--Any Skill--</option>
                                                    <?php foreach ($skill as $val) { ?>
                                                        <option value="<?php echo $val->SkillID; ?>"><?php echo $val->SkillName; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>	
                                        </div>
                                    </div>
									<div class="col-lg-4 col-sm-12 col-md-6 button-block" style="float: none; margin: 0px auto;">
                                        <div class="select-mentor">
                                            <div class="col-lg-12">
                                                <div style="float: left;">
													<input class="showNotCommon" id="showNotCommon" type="checkbox" name="showNotCommon" style="height: 20px;width: 20px;">
												</div>
												<div style="padding-top: 5px;margin-left: 30px;">
													Show pairs with no skills in common
												</div>
                                            </div>	
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12 col-md-6">
                                        <h3><?php echo $config[0]->mentorName ?> Selection</h3>
                                        <div class="top-col">

                                            <!--<div class="col-lg-6 select-option">
                                              <select class="form-control">
                                                <option disabled="disabled">Expirence</option>
                                                <option>1 year</option>
                                                <option>2 year</option>
                                                <option>3 year</option>
                                                <option>4 year</option>
                                              </select>
                                            </div>-->
                                            <div class="col-lg-12 select-mentor">
                                                <div class="col-lg-4"><strong><?php echo $config[0]->mentorName ?> who have these skills</strong></div>
                                                <div class="col-lg-8">
                                                    <select name="mentorDrp" id="mentorDrp" class="form-control">
                                                        <option value="0">--Select <?php echo $config[0]->mentorName ?>--</option>
                                                        <?PHP foreach ($mentor as $val) { ?>
                                                            <option value="<?php echo $val->MentorID; ?>"><?php echo $val->MentorName; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-12 col-md-6">
                                        <h3><?php echo $config[0]->menteeName ?> Selection</h3>
                                        <div class="top-col">
                                            <!-- <div class="col-lg-12 select-mentor">
                                                           <div class="col-lg-4"><strong>3: Skills needed</strong></div>
                                                           <div class="col-lg-8">
                                               <select name="menteeSkillsDrp" id="menteeSkillsDrp" class="form-control">
                                                 <option value="0">Any Skill</option>
                                            <?php //foreach($skill as $val){?>
                                                                   <option value="<?php //echo $val->SkillID;        ?>"><?php //echo $val->SkillName;       ?></option>
                                            <?php //} ?>
                                               </select>
                                                           </div>
                                                           
                                             </div>-->
                                            <div class="col-lg-6 select-option">
                                              <!--<select class="form-control">
                                                <option disabled="disabled">Expirence</option>
                                                <option>1 year</option>
                                                <option>2 year</option>
                                                <option>3 year</option>
                                                <option>4 year</option>
                                              </select>-->
                                            </div>
                                            <div class="col-lg-12 select-mentor">
                                                <div class="col-lg-4"><strong><?php echo $config[0]->menteeName ?> who need these skills</strong></div>
                                                <div class="col-lg-8">
                                                    <select name="menteeDrp" id="menteeDrp" class="form-control">
                                                        <option value="0">--Select <?php echo $config[0]->menteeName ?>--</option>
                                                        <?PHP foreach ($mentee as $val) { ?>
                                                            <option value="<?php echo $val->MenteeID; ?>"><?php echo $val->MenteeName; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-5 col-sm-12 col-md-5">

                                        <div class="mentor-block" id="mentorBlock">
                                            <div class="user-slider">
                                                <div>
                                                    <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false"> 
                                                        <!-- Indicators --> 
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner" role="listbox">

                                                            <?php
                                                            $j = 0;
                                                            foreach ($mentor as $val) {

                                                                $assignedMentee = explode(",", $val->assignedMentee);
                                                                if ($assignedMentee[0] != "")
                                                                    $cnt = count($assignedMentee);
                                                                else
                                                                    $cnt = 0;
                                                                ?>
																<?php
																	$image_path = base_url() . "assets/admin/images/admin/";
																	$file_headers = @get_headers($image_path.$val->MentorImage);
																	if($file_headers[0] == 'HTTP/1.1 200 OK') {
																		$image = $image_path.$val->MentorImage;
																	} else {
																		$image = $image_path.'avatar.png';
																	}
																?>
                                                                <div class="item <?php if ($j == 0) echo "active" ?>" id="<?php echo $val->MentorID ?>">
                                                                    <div class="user-img">
																		<img src="<?php echo $image; ?>" class="img-circle">
																	</div>
                                                                    <div class="user-name"><?php echo $val->MentorName; ?>
																		<span>
																			<a href="#">(<?php echo $cnt; ?>)
                                                                                <div class="connect-friend">
                                                                                    <span>Relationships with:</span>
                                                                                    <ul>
                                                                                        <?php if ($val->assignedMentee == "") { ?>
                                                                                            <li>No <?php echo $config[0]->menteeName ?> Assigned</li>
                                                                                            <?php
                                                                                        } else {
                                                                                            foreach ($assignedMentee as $s) {
                                                                                                ?>
                                                                                                <li><?php echo $s; ?></li>
                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                        ?>
                                                                                    </ul>
                                                                                </div>
                                                                            </a>
																		</span>
																	</div>
                                                                    <form>
                                                                        <?php $skillArr = explode(",", $val->skill); ?>
                                                                        <?php
																			$ss = '';
																			foreach ($skillArr as $s)
																				$ss .= "$s\n";
																			$ss = trim($ss,"\n");
																		?>
																		<textarea readonly name="skill" cols="" rows="" placeholder="Other"><?php echo $ss;?></textarea>
                                                                    </form>
                                                                </div>
                                                                <?php
                                                                $j++;
                                                            }
                                                            ?>
                                                        </div>

                                                        <!-- Left and right controls --> 
                                                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <span class="left-errow" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <span class="right-errow" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-sm-12 col-md-2 relationship-btns">
                                        <a type="button" data-toggle="tooltip" title="Form a relationship between the current <?php echo $config[0]->mentorName; ?> and <?php echo $config[0]->menteeName;?>" class="helper green-button"><img src="<?php echo base_url() ?>/assets/admin/images/btn1.png"></a>
                                        <a type="button" data-toggle="tooltip" title="Remove the relationship between the current <?php echo $config[0]->mentorName; ?> and <?php echo $config[0]->menteeName; ?>" class="helper red-button"><img src="<?php echo base_url() ?>/assets/admin/images/btn2.png"></a>
                                    </div>

                                    <div class="col-lg-5 col-sm-12 col-md-5">






                                        <div class="mentor-block" id="menteeBlock">
                                            <div class="user-slider">
                                                <div>
                                                    <div id="myCarousel1" class="carousel slide" data-ride="carousel" data-interval="false"> 
                                                        <!-- Indicators --> 
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner" role="listbox">
                                                            <?php
                                                            $k = 0;
                                                            foreach ($mentee as $val) {
                                                                $assignedMentor = explode(",", $val->assignedMentor);
                                                                if ($assignedMentor[0] != "")
                                                                    $cnt = count($assignedMentor);
                                                                else
                                                                    $cnt = 0;
                                                                ?>
                                                                <div class="item <?php if ($k == 0) echo "active" ?>" id="<?php echo $val->MenteeID ?>">
																	
																	<?php
																	$image_path = base_url() . "assets/admin/images/admin/";
																	$file_headers = @get_headers($image_path.$val->MenteeImage);
																	if($file_headers[0] == 'HTTP/1.1 200 OK') {
																		$image = $image_path.$val->MenteeImage;
																	} else {
																		$image = $image_path.'avatar.png';
																	}
																	?>
                                                                    <div class="user-img"><img src="<?php echo $image; ?>" class="img-circle"></div>
                                                                    <div class="user-name"><?php echo $val->MenteeName; ?><span><a href="#">(<?php echo $cnt; ?>)
                                                                                <div class="connect-friend">
                                                                                    <span>Relationships with:</span>
                                                                                    <ul>
                                                                                        <?php if ($val->assignedMentor == "") { ?>
                                                                                            <li>No <?php echo $config[0]->mentorName ?> Assigned</li>
                                                                                            <?php
                                                                                        } else {
                                                                                            $assignedMentor = explode(",", $val->assignedMentor);
                                                                                            foreach ($assignedMentor as $s) {
                                                                                                ?>
                                                                                                <li><?php echo $s; ?></li>
                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                        ?>
                                                                                    </ul>
                                                                                </div>
                                                                            </a> </span> </div>
                                                                    <form>
                                                                        <?php $skillArr = explode(",", $val->skill); ?>
																		<?php
																			$ss = '';
																			foreach ($skillArr as $s)
																				$ss .= "$s\n";
																			$ss = trim($ss,"\n");
																		?>
                                                                        <textarea readonly name="skill" cols="" rows="" placeholder="Other"><?php echo $ss; ?></textarea>

                                                                    </form>
                                                                </div>
                                                                <?php
                                                                $k++;
                                                            }
                                                            ?>
                                                        </div>

                                                        <!-- Left and right controls --> 
                                                        <a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev"> <span class="left-errow" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next"> <span class="right-errow" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div></div>
                        </div></div>
            </div>
        </div>
    </section>
</section>

<script>
    $(document).ready(function () {
        //$('[data-toggle="tooltip"]').tooltip();   
        $('.helper').tooltip({'placement': 'bottom'})
        $('.helper').on('click', function () {
            console.log(this)
        });

        //$('.carousel').carousel({
        //    pause: true,
        //    interval: false
        //});
    });
</script>

<script type="text/javascript">
    //$('.carousel').carousel({
    //    pause: true,
    //    interval: false
    //});
</script>
<script>

    $(document).ready(function () {

        var drpMentor = $("#mentorDrp").val();
        var menteeDrp = $("#menteeDrp").val();
        //alert(drpMentor);alert(menteeDrp);

		var skillID = $("#mentorSkillsDrp").val();
		if(skillID == ''){
			$('#showNotCommon').attr('disabled', true);
		}else
			$('#showNotCommon').attr('disabled', false);
		
        if (drpMentor == 0 || menteeDrp == 0)
        {
            $.ajax({
                url: '<?php echo base_url(); ?>admin/relationship/getDummyProfile',
                success: function (result) {
                    //alert(result);
                    var data = result.split("|");
                    $("#myCarousel").find('div.active').removeClass('active');
                    $("#myCarousel1").find('div.active').removeClass('active');
                    $("#myCarousel .carousel-inner").append(data[0]);
                    $("#myCarousel1 .carousel-inner").append(data[1]);
                    $(".green-button").attr("disabled", "disabled");
                    $(".green-button").css("opacity", "0.3");

                    $(".red-button").attr("disabled", "disabled");
                    $(".red-button").css("opacity", "0.3");

                    /*$("#myCarousel .left-errow").parent( ".carousel-control" ).removeAttr("href");
                     $("#myCarousel1 .left-errow").parent( ".carousel-control" ).removeAttr("href");
                     
                     $("#myCarousel .right-errow").parent( ".carousel-control" ).removeAttr("href");
                     $("#myCarousel1 .right-errow").parent( ".carousel-control" ).removeAttr("href");*/

                }
            });
        } else {
            var mentor = $("#myCarousel").find('div.active').attr('id');
            var mentee = $("#myCarousel1").find('div.active').attr('id');
            //alert(mentor);alert(mentee);

            $.ajax({
                url: '<?php echo base_url(); ?>admin/relationship/checkRelation',
                data: "mentor=" + mentor + "&mentee=" + mentee,
                success: function (result) {
                    if (result > 0) {
                        //alert(result);
                        $(".green-button").attr("disabled", "disabled");
                        $(".green-button").css("opacity", "0.3");

                        $(".red-button").removeAttr("disabled");
                        $(".red-button").css("opacity", "1");

                    } else {

                        $(".green-button").removeAttr("disabled");
                        $(".green-button").css("opacity", "1");

                        $(".red-button").attr("disabled", "disabled");
                        $(".red-button").css("opacity", "0.3");

                    }
					highlightCommonSkills();
                }
            });
        }
    });

    $(".green-button").click(function () {
        var mentor = $("#myCarousel").find('div.active').attr('id');
        var mentee = $("#myCarousel1").find('div.active').attr('id');
        //alert(mentor);alert(mentee);
        $.ajax({
            url: '<?php echo base_url(); ?>admin/relationship/addRelation',
            data: "mentor=" + mentor + "&mentee=" + mentee,
            success: function (result) {
                alertify.log(result);
                window.location.reload();
            }
        });
    });


    $(".red-button").click(function () {
        var mentor = $("#myCarousel").find('div.active').attr('id');
        var mentee = $("#myCarousel1").find('div.active').attr('id');
        //alert(mentor);alert(mentee);
        $.ajax({
            url: '<?php echo base_url(); ?>admin/relationship/removeRelation',
            data: "mentor=" + mentor + "&mentee=" + mentee,
            success: function (result) {
                alertify.log(result);
                window.location.reload();
            }
        });
    });


    $("#mentorDrp").change(function () {
        var mentorID = this.value;
        var mentee = $("#myCarousel1").find('div.active').attr('id');
        //alert(mentee);alert(mentorID);

		var skillID = $("#mentorSkillsDrp").val();
		if(skillID == ''){
			$('#showNotCommon').attr('disabled', true);
		}else
			$('#showNotCommon').attr('disabled', false);
		
        if (mentorID == 0) {
            $.ajax({
                url: '<?php echo base_url(); ?>admin/relationship/getDummyProfile',
                success: function (result) {
                    //alert(result);
                    var data = result.split("|");
                    $("#myCarousel").find('div.active').removeClass('active');
                    $("#myCarousel1").find('div.active').removeClass('active');
                    $("#myCarousel .carousel-inner").append(data[0]);
                    $("#myCarousel1 .carousel-inner").append(data[1]);
                    $(".green-button").attr("disabled", "disabled");
                    $(".green-button").css("opacity", "0.3");

                    $(".red-button").attr("disabled", "disabled");
                    $(".red-button").css("opacity", "0.3");

                    /*$("#myCarousel .left-errow").parent( ".carousel-control" ).removeAttr("href");
                     $("#myCarousel1 .left-errow").parent( ".carousel-control" ).removeAttr("href");
                     
                     $("#myCarousel .right-errow").parent( ".carousel-control" ).removeAttr("href");
                     $("#myCarousel1 .right-errow").parent( ".carousel-control" ).removeAttr("href");*/

                }
            });
        } else {
            $("#myCarousel").find('div.active').removeClass('active');
            $("#myCarousel").find("[id=" + mentorID + "]").addClass('active');
            //var t=	$("[id="+mentorID+"]").addClass('active');

            if (mentee == "noMentee") {
                $(".green-button").attr("disabled");
                $(".green-button").css("opacity", "0.3");

                $(".red-button").attr("disabled", "disabled");
                $(".red-button").css("opacity", "0.3");
            } else {

                if (mentorID != 0 && mentee != 0 && mentorID != "noMentor" && mentee != "noMentee") {
                    $.ajax({
                        url: '<?php echo base_url(); ?>admin/relationship/checkRelation',
                        data: "mentor=" + mentorID + "&mentee=" + mentee,
                        success: function (result) {
                            if (result > 0) {
                                $(".green-button").attr("disabled", "disabled");
                                $(".green-button").css("opacity", "0.3");

                                $(".red-button").removeAttr("disabled");
                                $(".red-button").css("opacity", "1");
                            } else {
                                $(".green-button").removeAttr("disabled");
                                $(".green-button").css("opacity", "1");

                                $(".red-button").attr("disabled", "disabled");
                                $(".red-button").css("opacity", "0.3");
                            }
							highlightCommonSkills();
                        }
                    });
                } else {
                    $(".green-button").attr("disabled", "disabled");
                    $(".green-button").css("opacity", "0.3");

                    $(".red-button").attr("disabled", "disabled");
                    $(".red-button").css("opacity", "0.3");
                }
            }

        }


        $('#noMentor').remove();

    });


    $("#menteeDrp").change(function () {
        var menteeID = this.value;
        var mentor = $("#myCarousel").find('div.active').attr('id');
        //alert(mentor);

		var skillID = $("#mentorSkillsDrp").val();
		if(skillID == ''){
			$('#showNotCommon').attr('disabled', true);
		}else
			$('#showNotCommon').attr('disabled', false);
		
        if (menteeID == 0) {
            $.ajax({
                url: '<?php echo base_url(); ?>admin/relationship/getDummyProfile',
                success: function (result) {
                    //alert(result);
                    var data = result.split("|");
                    $("#myCarousel").find('div.active').removeClass('active');
                    $("#myCarousel1").find('div.active').removeClass('active');
                    $("#myCarousel .carousel-inner").append(data[0]);
                    $("#myCarousel1 .carousel-inner").append(data[1]);
                    $(".green-button").attr("disabled", "disabled");
                    $(".green-button").css("opacity", "0.3");

                    $(".red-button").attr("disabled", "disabled");
                    $(".red-button").css("opacity", "0.3");

                    /*$("#myCarousel .left-errow").parent( ".carousel-control" ).removeAttr("href");
                     $("#myCarousel1 .left-errow").parent( ".carousel-control" ).removeAttr("href");
                     
                     $("#myCarousel .right-errow").parent( ".carousel-control" ).removeAttr("href");
                     $("#myCarousel1 .right-errow").parent( ".carousel-control" ).removeAttr("href");*/

                }
            });
        } else {
            $("#myCarousel1").find('div.active').removeClass('active');
            $("#myCarousel1").find("[id=" + menteeID + "]").addClass('active');
            //var t=	$("[id="+mentorID+"]").addClass('active');
            //alert(mentor);
            if (mentor == "noMentor") {
                $(".green-button").attr("disabled", "disabled");
                $(".green-button").css("opacity", "0.3");

                $(".red-button").attr("disabled", "disabled");
                $(".red-button").css("opacity", "0.3");
            } else {
                if (mentor != 0 && menteeID != 0 && mentor != "noMentor" && menteeID != "noMentee") {

                    $.ajax({
                        url: '<?php echo base_url(); ?>admin/relationship/checkRelation',
                        data: "mentor=" + mentor + "&mentee=" + menteeID,
                        success: function (result) {
                            if (result > 0) {
                                //alert(result);
                                $(".green-button").attr("disabled", "disabled");
                                $(".green-button").css("opacity", "0.3");

                                $(".red-button").removeAttr("disabled");
                                $(".red-button").css("opacity", "1");

                            } else {

                                $(".green-button").removeAttr("disabled");
                                $(".green-button").css("opacity", "1");

                                $(".red-button").attr("disabled", "disabled");
                                $(".red-button").css("opacity", "0.3");

                            }
							highlightCommonSkills();
                        }
                    });
                } else {
                    $(".green-button").attr("disabled", "disabled");
                    $(".green-button").css("opacity", "0.3");

                    $(".red-button").attr("disabled", "disabled");
                    $(".red-button").css("opacity", "0.3");
                }
            }

            $('#noMentee').remove();
        }

    });

    $("#mentorSkillsDrp").change(function () {
        var skillID = this.value;
		if(skillID == ''){
			$('#showNotCommon').attr('disabled', true);
		}else
			$('#showNotCommon').attr('disabled', false);
        //alert(skillID);
        $.ajax({
            url: '<?php echo base_url(); ?>admin/relationship/getMentorBySkill',
            data: "skillID=" + skillID,
            success: function (result) {
                //alert(result);
                var data = result.split("|");
                $("#mentorBlock").html(data[0]);
                $("#mentorDrp").empty();
                $("#mentorDrp").append(data[1]);

                var mentor = $("#myCarousel").find('div.active').attr('id');
                var mentee = $("#myCarousel1").find('div.active').attr('id');
                //alert(mentor);alert(mentee);

                if (mentor != 0 && mentee != 0 && mentor != "noMentor" && mentee != "noMentee") {

                    $.ajax({
                        url: '<?php echo base_url(); ?>admin/relationship/checkRelation',
                        data: "mentor=" + mentor + "&mentee=" + mentee,
                        success: function (result) {
                            if (result > 0) {
                                //alert(result);
                                $(".green-button").attr("disabled", "disabled");
                                $(".green-button").css("opacity", "0.3");

                                $(".red-button").removeAttr("disabled");
                                $(".red-button").css("opacity", "1");

                            } else {

                                $(".green-button").removeAttr("disabled");
                                $(".green-button").css("opacity", "1");

                                $(".red-button").attr("disabled", "disabled");
                                $(".red-button").css("opacity", "0.3");

                            }
							highlightCommonSkills();
                        }
                    });

                } else {
                    $(".green-button").attr("disabled", "disabled");
                    $(".green-button").css("opacity", "0.3");

                    $(".red-button").attr("disabled", "disabled");
                    $(".red-button").css("opacity", "0.3");
                }
            }
        });


        $.ajax({
            url: '<?php echo base_url(); ?>admin/relationship/getMenteeBySkill',
            data: "skillID=" + skillID,
            success: function (result) {
                //alert(result);
                var data = result.split("|");
                $("#menteeBlock").html(data[0]);
                $("#menteeDrp").empty();
                $("#menteeDrp").append(data[1]);

                var mentor = $("#myCarousel").find('div.active').attr('id');
                var mentee = $("#myCarousel1").find('div.active').attr('id');
                //alert(mentor);alert(mentee);

                if (mentor != 0 && mentee != 0 && mentor != "noMentor" && mentee != "noMentee") {

                    $.ajax({
                        url: '<?php echo base_url(); ?>admin/relationship/checkRelation',
                        data: "mentor=" + mentor + "&mentee=" + mentee,
                        success: function (result) {
                            if (result > 0) {
                                //alert(result);
                                $(".green-button").attr("disabled", "disabled");
                                $(".green-button").css("opacity", "0.3");

                                $(".red-button").removeAttr("disabled");
                                $(".red-button").css("opacity", "1");

                            } else {

                                $(".green-button").removeAttr("disabled");
                                $(".green-button").css("opacity", "1");

                                $(".red-button").attr("disabled", "disabled");
                                $(".red-button").css("opacity", "0.3");

                            }
							highlightCommonSkills();
                        }
                    });

                } else {
                    $(".green-button").attr("disabled", "disabled");
                    $(".green-button").css("opacity", "0.3");

                    $(".red-button").attr("disabled", "disabled");
                    $(".red-button").css("opacity", "0.3");
                }
            }
        });

    });
	
	
	
	$("#showNotCommon").change(function () {
		
        var skillID = (this.checked) ? '' : 0;
        //alert(skillID);
        $.ajax({
            url: '<?php echo base_url(); ?>admin/relationship/getMentorBySkill',
            data: "skillID=" + skillID,
            success: function (result) {
                //alert(result);
                var data = result.split("|");
                $("#mentorBlock").html(data[0]);
                $("#mentorDrp").empty();
                $("#mentorDrp").append(data[1]);

                var mentor = $("#myCarousel").find('div.active').attr('id');
                var mentee = $("#myCarousel1").find('div.active').attr('id');
                //alert(mentor);alert(mentee);

                if (mentor != 0 && mentee != 0 && mentor != "noMentor" && mentee != "noMentee") {

                    $.ajax({
                        url: '<?php echo base_url(); ?>admin/relationship/checkRelation',
                        data: "mentor=" + mentor + "&mentee=" + mentee,
                        success: function (result) {
                            if (result > 0) {
                                //alert(result);
                                $(".green-button").attr("disabled", "disabled");
                                $(".green-button").css("opacity", "0.3");

                                $(".red-button").removeAttr("disabled");
                                $(".red-button").css("opacity", "1");

                            } else {

                                $(".green-button").removeAttr("disabled");
                                $(".green-button").css("opacity", "1");

                                $(".red-button").attr("disabled", "disabled");
                                $(".red-button").css("opacity", "0.3");

                            }
                        }
                    });

                } else {
                    $(".green-button").attr("disabled", "disabled");
                    $(".green-button").css("opacity", "0.3");

                    $(".red-button").attr("disabled", "disabled");
                    $(".red-button").css("opacity", "0.3");
                }
            }
        });


        $.ajax({
            url: '<?php echo base_url(); ?>admin/relationship/getMenteeBySkill',
            data: "skillID=" + skillID,
            success: function (result) {
                //alert(result);
                var data = result.split("|");
                $("#menteeBlock").html(data[0]);
                $("#menteeDrp").empty();
                $("#menteeDrp").append(data[1]);

                var mentor = $("#myCarousel").find('div.active').attr('id');
                var mentee = $("#myCarousel1").find('div.active').attr('id');
                //alert(mentor);alert(mentee);

                if (mentor != 0 && mentee != 0 && mentor != "noMentor" && mentee != "noMentee") {

                    $.ajax({
                        url: '<?php echo base_url(); ?>admin/relationship/checkRelation',
                        data: "mentor=" + mentor + "&mentee=" + mentee,
                        success: function (result) {
                            if (result > 0) {
                                //alert(result);
                                $(".green-button").attr("disabled", "disabled");
                                $(".green-button").css("opacity", "0.3");

                                $(".red-button").removeAttr("disabled");
                                $(".red-button").css("opacity", "1");

                            } else {

                                $(".green-button").removeAttr("disabled");
                                $(".green-button").css("opacity", "1");

                                $(".red-button").attr("disabled", "disabled");
                                $(".red-button").css("opacity", "0.3");

                            }
							highlightCommonSkills();
                        }
                    });

                } else {
                    $(".green-button").attr("disabled", "disabled");
                    $(".green-button").css("opacity", "0.3");

                    $(".red-button").attr("disabled", "disabled");
                    $(".red-button").css("opacity", "0.3");
                }
            }
        });

    });
	
	


    /*$("#menteeSkillsDrp").change(function (){
     var skillID	=	this.value;
     //alert(skillID);
     $.ajax({
     url: '<?php echo base_url(); ?>admin/relationship/getMenteeBySkill',
     data:"skillID="+skillID,
     success: function(result){
     //alert(result);
     var data	= result.split("|");
     $("#menteeBlock").html(data[0]);
     $("#menteeDrp").empty();
     $("#menteeDrp").append(data[1]);
     
     var mentor	=	$("#myCarousel").find('div.active').attr('id');
     var mentee	=	$("#myCarousel1").find('div.active').attr('id');
     //alert(mentor);alert(mentee);
     
     if(mentor != 0 && mentee != 0 && mentor	!= "noMentor" && mentee != "noMentee"){
     
     $.ajax({
     url: '<?php echo base_url(); ?>admin/relationship/checkRelation',
     data:"mentor="+mentor+"&mentee="+mentee,
     success: function(result){
     if(result > 0){
     //alert(result);
     $(".green-button").attr("disabled","disabled");
     $(".green-button").css("opacity","0.3");
     
     $(".red-button").removeAttr("disabled");
     $(".red-button").css("opacity","1");
     
     }else{
     
     $(".green-button").removeAttr("disabled");
     $(".green-button").css("opacity","1");
     
     $(".red-button").attr("disabled","disabled");
     $(".red-button").css("opacity","0.3");
     
     }	
     }
     });	
     
     }else{
     $(".green-button").attr("disabled","disabled");
     $(".green-button").css("opacity","0.3");
     
     $(".red-button").attr("disabled","disabled");
     $(".red-button").css("opacity","0.3");
     }	
     }
     });	
     
     });*/


    $("#myCarousel .right-errow").click(function () {

        var currentMentor = $("#myCarousel").find('div.active').attr('id');
        //alert(currentMentor);
        var mentee = $("#myCarousel1").find('div.active').attr('id');
        if (currentMentor == "noMentor") {
            return false;
        } else {

            if (mentee == "noMentee") {
                $(".green-button").attr("disabled");
                $(".green-button").css("opacity", "0.3");

                $(".red-button").attr("disabled", "disabled");
                $(".red-button").css("opacity", "0.3");
            } else {
                var mentor = $("#myCarousel").find('div.active').next().attr('id');
                var last = $("#myCarousel").find('div.active').next().length;
                //alert(mentor);alert(last);
                if (last == 0) {
                    //last child
                    var mentor = $(".carousel-inner div:first-child").attr("id");
                }

                //alert(mentor);alert(mentee);
                $.ajax({
                    url: '<?php echo base_url(); ?>admin/relationship/checkRelation',
                    data: "mentor=" + mentor + "&mentee=" + mentee,
                    success: function (result) {
                        if (result > 0) {
                            //alert(result);
                            $(".green-button").attr("disabled", "disabled");
                            $(".green-button").css("opacity", "0.3");

                            $(".red-button").removeAttr("disabled");
                            $(".red-button").css("opacity", "1");

                        } else {

                            $(".green-button").removeAttr("disabled");
                            $(".green-button").css("opacity", "1");

                            $(".red-button").attr("disabled", "disabled");
                            $(".red-button").css("opacity", "0.3");

                        }
						highlightCommonSkills();
                    }
                });
            }

        }





    });


    $("#myCarousel .left-errow").click(function () {

        var currentMentor = $("#myCarousel").find('div.active').attr('id');
        var mentee = $("#myCarousel1").find('div.active').attr('id');
        if (currentMentor == "noMentor") {
            return false;
        } else {
            if (mentee == "noMentee") {
                $(".green-button").attr("disabled");
                $(".green-button").css("opacity", "0.3");

                $(".red-button").attr("disabled", "disabled");
                $(".red-button").css("opacity", "0.3");
            } else {
                var mentor = $("#myCarousel").find('div.active').prev().attr('id');
                var last = $("#myCarousel").find('div.active').prev().length;
                if (last == 0) {
                    //first child
                    var mentor = $('#myCarousel').children('div').children('div').last().attr("id");
                    //alert(mentor);
                }

                //alert(mentor);alert(mentee);
                $.ajax({
                    url: '<?php echo base_url(); ?>admin/relationship/checkRelation',
                    data: "mentor=" + mentor + "&mentee=" + mentee,
                    success: function (result) {
                        if (result > 0) {
                            //alert(result);
                            $(".green-button").attr("disabled", "disabled");
                            $(".green-button").css("opacity", "0.3");

                            $(".red-button").removeAttr("disabled");
                            $(".red-button").css("opacity", "1");

                        } else {

                            $(".green-button").removeAttr("disabled");
                            $(".green-button").css("opacity", "1");

                            $(".red-button").attr("disabled", "disabled");
                            $(".red-button").css("opacity", "0.3");

                        }
						highlightCommonSkills();
                    }
                });
            }

        }



    });




    $("#myCarousel1 .right-errow").click(function () {

        var currentMentee = $("#myCarousel1").find('div.active').attr('id');
        var mentor = $("#myCarousel").find('div.active').attr('id');
        if (currentMentee == "noMentee") {
            return false;
        } else {
            if (mentor == "noMentor") {
                $(".green-button").attr("disabled");
                $(".green-button").css("opacity", "0.3");

                $(".red-button").attr("disabled", "disabled");
                $(".red-button").css("opacity", "0.3");
            } else {
                var last = $("#myCarousel1").find('div.active').next().length;
                var mentee = $("#myCarousel1").find('div.active').next().attr('id');
                //alert(last);
                if (last == 0) {
                    //last child
                    var mentee = $("#myCarousel1 .carousel-inner div:first-child").attr("id");
                }

                //alert(mentor);alert(mentee);
                $.ajax({
                    url: '<?php echo base_url(); ?>admin/relationship/checkRelation',
                    data: "mentor=" + mentor + "&mentee=" + mentee,
                    success: function (result) {
                        if (result > 0) {
                            //alert(result);
                            $(".green-button").attr("disabled", "disabled");
                            $(".green-button").css("opacity", "0.3");

                            $(".red-button").removeAttr("disabled");
                            $(".red-button").css("opacity", "1");

                        } else {

                            $(".green-button").removeAttr("disabled");
                            $(".green-button").css("opacity", "1");

                            $(".red-button").attr("disabled", "disabled");
                            $(".red-button").css("opacity", "0.3");

                        }
						highlightCommonSkills();
                    }
                });
            }



        }


    });


    $("#myCarousel1 .left-errow").click(function () {

        var currentMentee = $("#myCarousel1").find('div.active').attr('id');
        var mentor = $("#myCarousel").find('div.active').attr('id');
        if (currentMentee == "noMentee") {
            return false;
        } else {
            if (mentor == "noMentor") {
                $(".green-button").attr("disabled");
                $(".green-button").css("opacity", "0.3");

                $(".red-button").attr("disabled", "disabled");
                $(".red-button").css("opacity", "0.3");
            } else {
                var mentee = $("#myCarousel1").find('div.active').prev().attr('id');
                var last = $("#myCarousel1").find('div.active').prev().length;
                if (last == 0) {
                    //first child
                    var mentee = $('#myCarousel1').children('div').children('div').last().attr("id");

                    //alert(mentee);
                }

                //alert(mentor);alert(mentee);
                $.ajax({
                    url: '<?php echo base_url(); ?>admin/relationship/checkRelation',
                    data: "mentor=" + mentor + "&mentee=" + mentee,
                    success: function (result) {
                        if (result > 0) {
                            //alert(result);
                            $(".green-button").attr("disabled", "disabled");
                            $(".green-button").css("opacity", "0.3");

                            $(".red-button").removeAttr("disabled");
                            $(".red-button").css("opacity", "1");

                        } else {

                            $(".green-button").removeAttr("disabled");
                            $(".green-button").css("opacity", "1");

                            $(".red-button").attr("disabled", "disabled");
                            $(".red-button").css("opacity", "0.3");

                        }
						highlightCommonSkills();
                    }
                });
            }


        }


    });
	
	function highlightCommonSkills(){
		
		setTimeout(function(){
			var mentorSkill = $('#myCarousel .item.active textarea').text().split("\n");
			var menteeSkill = $('#myCarousel1 .item.active textarea').text().split("\n");
			$('textarea').highlightTextarea('destroy');
			//$('#myCarousel1 .item.active textarea').highlightTextarea('destroy');
			commonSkill = [];
			for (i = 0; i < mentorSkill.length; i++) {
				var mSkill = mentorSkill[i];
				for (j = 0; j < menteeSkill.length; j++) {
					if (mSkill == menteeSkill[j]) {
						commonSkill.push(mSkill);
					}
				}
			}
			console.log(commonSkill);
			if(commonSkill.length > 0) {
				$('#myCarousel .item.active textarea').highlightTextarea({
					words: commonSkill,
				});
				$('#myCarousel1 .item.active textarea').highlightTextarea({
					words: commonSkill,
				});
			}
		},500);
	}
</script>