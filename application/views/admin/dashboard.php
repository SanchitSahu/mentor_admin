<?php //echo "<pre>";print_r($avgMeetingLengthByTopic);exit;

$MenteeListArr	=	array();
foreach($MenteeList as $val){
	//print_r($val);
	array_push($MenteeListArr,$val->MenteeName);
}
//print_r($MenteeListArr);


//TJ - 18.02


$tooltip = new stdClass();
$tooltip->role = 'tooltip';



$chart_data = array_values($chart_data);
//echo '<pre>';print_r($chart_data);
//exit;


//$chart_data = $marks;
//echo '<pre>';print_r($chart_data);
//exit;
$mentee_count = count($MenteeListArr);
$data_count = count($chart_data);
 
$a=-1;$b=0;
for($i=0;$i<$mentee_count;$i++)
{
	$header[0] = 'Mentor';
	$a=$a+2;$b=$b+2;
	$header[$a] = $MenteeListArr[$i];
	$header[$b] = $tooltip;
	
}

//echo '<pre>';print_r($chart_data);
//exit;

if($mentee_count == 0){
	$header[0] = 'Mentor';
	$a=$a+2;$b=$b+2;
	$header[$a] = array();
	$header[$b] = $tooltip;
}
$head[] = $header;
//echo '<pre>';print_r($header);
//$head = json_encode($head);
//print_r($head);
$stack = array();

for($j=0;$j<$data_count;$j++)
{
	
	for($i=0;$i<$mentee_count*2+1;$i++)
	{
		$stack[$j][]=0;
	}
	$meeting_count = count($chart_data[$j]);
	for($k=0;$k<$meeting_count;$k++)
	{
		$l=0;
		$p=-1; 
		for($i=0;$i<$mentee_count;$i++)
		{
		
		//echo $MenteeListArr[$i];
		//echo $chart_data[$j][$k]['MenteeName'].'<br>';
		 $l=$l+2;
		  $p=$p+2;
		if($MenteeListArr[$i] == $chart_data[$j][$k]['MenteeName'])
		{
			$stack[$j][0] = $chart_data[$j][$k]['MentorName'];
			//echo 'Matched.<br>';
			//echo $chart_data[$j][$k]['MentorName'].'<br>';
			if(@$stack[$j][$p] == 0 )
			{
				$stack[$j][$p] = (int)$chart_data[$j][$k]['totaltime'];
				$stack[$j][$l] = $chart_data[$j][$k]['MentorName'] ."\n ".$chart_data[$j][$k]['MenteeName']. " \n " .$chart_data[$j][$k]['total']." Meetings \n ".$chart_data[$j][$k]['totaltime']." Minutes";
			}
			else
			{
					
			}
			//break;
		}
		else
		{
			$stack[$j][0] = $chart_data[$j][$k]['MentorName'];
			//echo 'Not Matched.<br>';
			//echo $chart_data[$j][$k]['MentorName'].'<br>';
			//echo $stack[$j][$i];
			if(@$stack[$j][$p] != 0 || !empty($stack))
			{
				//echo 'not if';
			}
			else
			{
			//	echo 'not else';
				$stack[$j][$p] = 0;	
				$stack[$j][$l] = "";
			}
			//break;
		}
		
		}
	}
}


//echo '<pre>';print_r( $stack);


$final = array_merge($head,$stack);

//echo '<pre>';print_r($stack);
//echo '<pre>';print_r($final);

$json_final = json_encode($final);


?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">

	google.load("visualization", "1.0", {packages: ["corechart"]});
	function topicMeetingChartChange(val,id)
	{
		drawChart(val,id);	
	}
	google.setOnLoadCallback(drawChart2);
	
	function drawChart2() {
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'SkillName');
		data.addColumn('number', 'No of <?php echo $config[0]->mentorName?>');
		 // A column for custom tooltip content
        data.addColumn({type: 'string', role: 'tooltip'});
		
		data.addRows([
		<?php
			foreach ($topicMeetingChart as $data) {
				$tooltip	=	"No of Meetings: ".$data->MeetingID;
				echo '["' . $data->TopicDescription . '",' . $data->MeetingID . ',"' . $tooltip . '"],';
			}
		?>
		]);
		var options = {
			title: 'Topic Meetings Chart'/*,
			vAxis: {maxValue: 4, format: '0'}*/
		};
		var chart = new google.visualization.PieChart(document.getElementById('topicMeetingChart'));
		chart.draw(data, options);
		
		
		/**/
		
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'MentorName');
		data.addColumn('number', 'No of Meetings');
		 // A column for custom tooltip content
        data.addColumn({type: 'string', role: 'tooltip'});
		
		data.addRows([
		<?php
			foreach ($mentorMeetingChart as $data) {
				$tooltip	=	"No of Meetings: ".$data->MeetingID;
				echo '["' . $data->MentorName . '",' . $data->MeetingID . ',"' . $tooltip . '"],';
			}
		?>
		]);
		var options = {
			title: '<?php echo $config[0]->mentorName?> Meetings Chart'/*,
			vAxis: {maxValue: 4, format: '0'}*/
		};

		var chart = new google.visualization.PieChart(document.getElementById('mentorMeetingChart'));
		chart.draw(data, options);
		
		/**/

		var data = new google.visualization.DataTable();
		data.addColumn('string', 'MentorName');
		data.addColumn('number', 'No of Meetings');
		 // A column for custom tooltip content
        data.addColumn({type: 'string', role: 'tooltip'});
		
		data.addRows([
		<?php
			foreach ($avgMeetingLengthByTopic as $data) {
				$tooltip	=	"Meetings Avg: ".$data->Average;
				echo '["' . $data->TopicDescription . '",' . $data->Average . ',"' . $tooltip . '"],';
			}
		?>
		]);
		var options = {
			title: '<?php echo $config[0]->mentorName?>/<?php echo $config[0]->menteeName?> Avg Meeting Chart',
			 height: 200,
			width:475,
			legend: { position: 'top', maxLines: 3 },
			bar: { groupWidth: '75%' },
		};

		var chart = new google.visualization.ColumnChart(document.getElementById('avgMeetingLengthByTopic'));
		chart.draw(data, options);
		
		/**/
		
		var data = new google.visualization.DataTable();
		
		var options = {
		title: '<?php echo $config[0]->mentorName?>/<?php echo $config[0]->menteeName?> Skill Chart',  
        height: 350,
		width:960,
        legend: { position: 'top', maxLines: 15 },
        bar: { groupWidth: '75%' },
		
		
      };
	  
      data.addColumn('string', 'Skill');
      data.addColumn('number', 'No Of <?php echo $config[0]->mentorName?>s');
      data.addColumn('number', 'No Of <?php echo $config[0]->menteeName?>s');

      data.addRows([
		
		<?php
			foreach ($MentorMenteeSkillChart as $data) {
			echo '[{f:"' . $data->SkillName . '"},' . $data->mentor_ids . ',' . $data->mentee_ids . '],';
			}
		?>
		
        
      ]);

      var chart = new google.visualization.BarChart(document.getElementById('columnchart_material'));
      chart.draw(data, options);
		
		/**/
		
		/*var data = new google.visualization.DataTable();
		data.addColumn('string', 'SkillName');
		data.addColumn('number', 'No of <?php echo $config[0]->menteeName?>');
		 // A column for custom tooltip content
        data.addColumn({type: 'string', role: 'tooltip'});
		
		data.addRows([
		<?php
			foreach ($menteeSkillchart as $data) {
				echo '["' . $data->SkillName . '",' . $data->mentee_ids . ',"' . $data->mentee_names . '"],';
			}
		?>
		]);
		var options = {
			title: '<?php echo $config[0]->menteeName?> Skills Chart'
		};

		var chart = new google.visualization.ColumnChart(document.getElementById('menteeSkillchart'));
		chart.draw(data, options);
		*/
		
		
		
		/**/
		
		
		var data = google.visualization.arrayToDataTable(<?php print_r($json_final) ?>);
	  
	 

      var options = {
		title: '<?php echo $config[0]->mentorName?>/<?php echo $config[0]->menteeName?> Meetings',  
        height: 200,
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: true
		
      };

      var view = new google.visualization.DataView(data);
     

     
      var chart = new google.visualization.BarChart(document.getElementById("barchart_values"));
      chart.draw(view, options);
	  
	  
	  
	  
	  var data = new google.visualization.DataTable();
		data.addColumn('string', 'SkillName');
		data.addColumn('number', 'No of <?php echo $config[0]->mentorName?>');
		 // A column for custom tooltip content
        data.addColumn({type: 'string', role: 'tooltip'});
		
		data.addRows([
		<?php
				$tooltip	=	"No of Active ".$config[0]->mentorName.": ".$activeMentor;
				$tooltip2	=	"No of Inactive ".$config[0]->mentorName.": ".$inActiveMentor;
				echo '["Active '.$config[0]->mentorName.'s",' . $activeMentor . ',"' . $tooltip . '"],';
				echo '["Inactive ' . $config[0]->mentorName . 's",' . $inActiveMentor . ',"' . $tooltip2 . '"],';
			
		?>
		]);
		var options = {
			title: 'Active/Inactive <?php echo $config[0]->mentorName?>'
		};
		var chart = new google.visualization.PieChart(document.getElementById('activeMetors'));
		chart.draw(data, options);
	}

	
	
	function drawChart(chartType,selectId) {
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'SkillName');
		data.addColumn('number', 'No of <?php echo $config[0]->mentorName?>');
		 // A column for custom tooltip content
        data.addColumn({type: 'string', role: 'tooltip'});
		
		data.addRows([
		<?php
			foreach ($topicMeetingChart as $data) {
				$tooltip	=	"No of Meetings: ".$data->MeetingID;
				echo '["' . $data->TopicDescription . '",' . $data->MeetingID . ',"' . $tooltip . '"],';
			}
		?>
		]);
		var options = {
			title: 'Topic Meetings Chart'/*,
			vAxis: {maxValue: 4, format: '0'}*/
		};

		var chart;
		//alert(chartType);
		if(chartType== 'Line' && selectId == 'topicMeetingChartSelect'){
			var chart = new google.visualization.LineChart(document.getElementById('topicMeetingChart'));
			chart.draw(data, options);
		}else if(chartType== 'Area' && selectId == 'topicMeetingChartSelect'){
			var chart = new google.visualization.AreaChart(document.getElementById('topicMeetingChart'));
			chart.draw(data, options);
		}else if(chartType== 'Bar' && selectId == 'topicMeetingChartSelect'){
			var chart = new google.visualization.BarChart(document.getElementById('topicMeetingChart'));
			chart.draw(data, options);
		}else if(selectId	==	'topicMeetingChartSelect'){
			//alert(selectId);
			var chart = new google.visualization.PieChart(document.getElementById('topicMeetingChart'));
			chart.draw(data, options);
		}

		

		//chart.draw(data, options);
		
		
		
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'MentorName');
		data.addColumn('number', 'No of Meetings');
		 // A column for custom tooltip content
        data.addColumn({type: 'string', role: 'tooltip'});
		
		data.addRows([
		<?php
			foreach ($mentorMeetingChart as $data) {
				$tooltip	=	"No of Meetings: ".$data->MeetingID;
				echo '["' . $data->MentorName . '",' . $data->MeetingID . ',"' . $tooltip . '"],';
			}
		?>
		]);
		var options = {
			title: 'Mentor Meetings Chart'/*,
			vAxis: {maxValue: 4, format: '0'}*/
		};

		

		var chart;
		//alert(selectId);
		if(chartType== 'Line' && selectId	==	'mentorMeetingChartSelect'){
			var chart = new google.visualization.LineChart(document.getElementById('mentorMeetingChart'));
			chart.draw(data, options);
		}else if(chartType== 'Area' && selectId	==	'mentorMeetingChartSelect'){
			var chart = new google.visualization.AreaChart(document.getElementById('mentorMeetingChart'));
			chart.draw(data, options);
		}else if(chartType== 'Bar' && selectId	==	'mentorMeetingChartSelect'){
			var chart = new google.visualization.BarChart(document.getElementById('mentorMeetingChart'));
			chart.draw(data, options);
		}else if(selectId	==	'mentorMeetingChartSelect'){
			//alert(selectId);
			var chart = new google.visualization.PieChart(document.getElementById('mentorMeetingChart'));
			chart.draw(data, options);
		}
		
		
		

		var data = new google.visualization.DataTable();
		data.addColumn('string', 'MentorName');
		data.addColumn('number', 'No of Meetings');
		 // A column for custom tooltip content
        data.addColumn({type: 'string', role: 'tooltip'});
		
		data.addRows([
		<?php
			foreach ($avgMeetingLengthByTopic as $data) {
				$tooltip	=	"Meetings Avg: ".$data->Average;
				echo '["' . $data->TopicDescription . '",' . $data->Average . ',"' . $tooltip . '"],';
			}
		?>
		]);
		var options = {
			title: 'Avg Length of Meeting By Topic Chart'/*,
			vAxis: {maxValue: 4, format: '0'}*/
		};

		
		var chart;
		//alert(selectId);
		if(chartType== 'Line' && selectId	==	'avgMeetingLengthByTopicSelect'){
			var chart = new google.visualization.LineChart(document.getElementById('avgMeetingLengthByTopic'));
			chart.draw(data, options);
		}else if(chartType== 'Area' && selectId	==	'avgMeetingLengthByTopicSelect'){
			var chart = new google.visualization.AreaChart(document.getElementById('avgMeetingLengthByTopic'));
			chart.draw(data, options);
		}else if(chartType== 'Bar' && selectId	==	'avgMeetingLengthByTopicSelect'){
			var chart = new google.visualization.BarChart(document.getElementById('avgMeetingLengthByTopic'));
			chart.draw(data, options);
		}else if(selectId	==	'avgMeetingLengthByTopicSelect'){
			//alert(selectId);
			var chart = new google.visualization.ColumnChart(document.getElementById('avgMeetingLengthByTopic'));
			chart.draw(data, options);
		}
		//chart.draw(data, options);
		
		
		
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'SkillName');
		data.addColumn('number', 'No of <?php echo $config[0]->mentorName?>');
		// A column for custom tooltip content
        data.addColumn({type: 'string', role: 'tooltip'});
		
		data.addRows([
		<?php
			foreach ($chart_data1 as $data) {
				echo '["' . $data->SkillName . '",' . $data->mentor_ids . ',"' . $data->mentor_names . '"],';
			}
		?>
		]);
		var options = {
			title: 'Mentor Skills Chart'/*,
			vAxis: {maxValue: 4, format: '0'}*/
		};

		var chart;
		//alert(selectId);
		if(chartType== 'Line' && selectId	==	'columnchart_materialSelect'){
			var chart = new google.visualization.LineChart(document.getElementById('columnchart_material'));
			chart.draw(data, options);
		}else if(chartType== 'Area' && selectId	==	'columnchart_materialSelect'){
			var chart = new google.visualization.AreaChart(document.getElementById('columnchart_material'));
			chart.draw(data, options);
		}else if(chartType== 'Bar' && selectId	==	'columnchart_materialSelect'){
			var chart = new google.visualization.BarChart(document.getElementById('columnchart_material'));
			chart.draw(data, options);
		}else if(selectId	==	'columnchart_materialSelect'){
			//alert(selectId);
			var chart = new google.visualization.ColumnChart(document.getElementById('columnchart_material'));
			chart.draw(data, options);
		}
		
		
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'SkillName');
		data.addColumn('number', 'No of <?php echo $config[0]->menteeName?>');
		 // A column for custom tooltip content
        data.addColumn({type: 'string', role: 'tooltip'});
		
		data.addRows([
		<?php
			foreach ($menteeSkillchart as $data) {
				echo '["' . $data->SkillName . '",' . $data->mentee_ids . ',"' . $data->mentee_names . '"],';
			}
		?>
		]);
		var options = {
			title: '<?php echo $config[0]->menteeName?> Skills Chart'
		};

		
		var chart;
		//alert(selectId);
		if(chartType== 'Line' && selectId	==	'menteeSkillchartSelect'){
			var chart = new google.visualization.LineChart(document.getElementById('menteeSkillchart'));
			chart.draw(data, options);
		}else if(chartType== 'Area' && selectId	==	'menteeSkillchartSelect'){
			var chart = new google.visualization.AreaChart(document.getElementById('menteeSkillchart'));
			chart.draw(data, options);
		}else if(chartType== 'Bar' && selectId	==	'menteeSkillchartSelect'){
			var chart = new google.visualization.BarChart(document.getElementById('menteeSkillchart'));
			chart.draw(data, options);
		}else if(selectId	==	'menteeSkillchartSelect'){
			//alert(selectId);
			var chart = new google.visualization.ColumnChart(document.getElementById('menteeSkillchart'));
			chart.draw(data, options);
		}
		//chart.draw(data, options);
		
		
		
	
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'SkillName');
		data.addColumn('number', 'No of <?php echo $config[0]->mentorName?>');
		 // A column for custom tooltip content
        data.addColumn({type: 'string', role: 'tooltip'});
		
		data.addRows([
		<?php
				$tooltip	=	"No of Active ".$config[0]->mentorName.": ".$activeMentor;
				$tooltip2	=	"No of Inactive ".$config[0]->mentorName.": ".$inActiveMentor;
				echo '["Active '.$config[0]->mentorName.'",' . $activeMentor . ',"' . $tooltip . '"],';
				echo '["Inactive ' . $config[0]->mentorName . '",' . $inActiveMentor . ',"' . $tooltip2 . '"],';
			
		?>
		]);
		var options = {
			title: 'Active/Inactive <?php echo $config[0]->mentorName?>'
		};
		var chart = new google.visualization.PieChart(document.getElementById('activeMetors'));
		var chart;
		//alert(selectId);
		if(chartType== 'Pie' && selectId	==	'activeMetorsSelect'){
			var chart = new google.visualization.PieChart(document.getElementById('activeMetors'));
			chart.draw(data, options);
		}else if(chartType== 'Area' && selectId	==	'activeMetorsSelect'){
			var chart = new google.visualization.AreaChart(document.getElementById('activeMetors'));
			chart.draw(data, options);
		}else if(chartType== 'Bar' && selectId	==	'activeMetorsSelect'){
			var chart = new google.visualization.BarChart(document.getElementById('activeMetors'));
			chart.draw(data, options);
		}else if(chartType== 'Line' && selectId	==	'activeMetorsSelect'){
			var chart = new google.visualization.LineChart(document.getElementById('activeMetors'));
			chart.draw(data, options);
		}else if(selectId	==	'activeMetorsSelect'){
			//alert(selectId);
			var chart = new google.visualization.PieChart(document.getElementById('activeMetors'));
			chart.draw(data, options);
		}
		
		
		
		
		var data = google.visualization.arrayToDataTable(<?php print_r($json_final) ?>);
	  
	 

      var options = {
        height: 200,
		width:960,
		title: '<?php echo $config[0]->mentorName?>/<?php echo $config[0]->menteeName?> Meetings',
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: true
		
      };

      var view = new google.visualization.DataView(data);
     

     
      var chart = new google.visualization.BarChart(document.getElementById("barchart_values"));
      chart.draw(view, options);
	}

</script>
<style>
.col-lg-6.dashboard > label {
  margin-bottom: 20px;
}

.col-lg-6.dashboard > select {
  border: 1px solid #ddd;
  padding: 5px;
  width: 100%;
}

.col-lg-6.dashboard {
  margin-top: 15px;
}

#barchart_values { width:100% !important;}

</style>
<section id="container">
    <!--header start-->
    <?php echo $this->load->view('admin/includes/header'); ?>
    <!--header end-->
    <?php $res['config']	=	$this->settings_model->getConfig();
	echo $this->load->view('includes/left_menu',$res); ?>
    
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
           <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>DASHBOARD</strong>
                    </header>
                </section>
            </div>
            </div>

            
            <div class="row">
                <div class="col-md-3">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon orange"><i class="livicon" data-s="32" data-n="user" data-c="#fff" data-hc="0" id="I11" style="padding-bottom:55px;"></i></span>
                        <div class="mini-stat-info">
                            <span><?php echo $totalMeetings;?></span>
                            Total Meetings
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon tar"><i class="livicon" data-s="32" data-n="users" data-c="#fff" data-hc="0" id="I12" style="padding-bottom:55px;"></i></span>
                        <div class="mini-stat-info">
                            <span><?php echo $totalMentors;?></span>
                            Total <?php echo ($config[0]->mentorName == 1)? $config[0]->mentorName : $config[0]->mentorName."s"?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon pink"><i class="livicon" data-s="32" data-n="users" data-c="#fff" data-hc="0" id="I13" style="padding-bottom:55px;"></i></span>
                        <div class="mini-stat-info">
                            <span><?php echo $totalMentees;?></span>
                            Total <?php echo ($config[0]->menteeName == 1)? $config[0]->menteeName : $config[0]->menteeName."s"?>
                        </div>
                    </div>
                </div>
				
				<div class="col-md-3">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon orange"><i class="livicon" data-s="32" data-n="info" data-c="#fff" data-hc="0" id="I14" style="padding-bottom:55px;"></i></span>
                        <div class="mini-stat-info">
                            <span><?php echo $avgMeetingLength;?></span>
                            Average Meeting Length
                        </div>
                    </div>
                </div>
                
            </div>
            <!--mini statistics end-->
			<?php if(empty($topicMeetingChart) || empty($mentorMeetingChart) || empty($avgMeetingLengthByTopic) || empty($chart_data1)  || empty($menteeSkillchart)){ ?>
				<section class="panel">
					<header class="panel-heading">
						No Information Available
					</header>
				</section>
			<?php }else{ ?>
				
			<div class="col-lg-6 dashboard">
				<label>Chart Type </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select style="width:200px;" id="topicMeetingChartSelect"  onchange="return topicMeetingChartChange(this.value,'topicMeetingChartSelect')">
					<option value="0">Pie Chart</option>
					<option value="Bar">Bar Chart</option>
					<option value="Area">Area Chart</option>
					<option value="Line">Line Chart</option>
				</select>
				<div id="topicMeetingChart"></div>
            </div>
          
			
			<div class="col-lg-6 dashboard">
				<label>Chart Type </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select style="width:200px;" id="mentorMeetingChartSelect"  onchange="return topicMeetingChartChange(this.value,'mentorMeetingChartSelect')" value="0">
					<option value="0">Pie Chart</option>
					<option value="Bar">Bar Chart</option>
				</select>
				<div id="mentorMeetingChart"></div>
			</div>
            
            <div style="border-bottom: 2px solid rgb(221, 221, 221); float: left; display: inline-block; width: 100%; margin-top: 20px;" id="lineSepreator"></div>
			<div class="col-lg-6 dashboard">
				<label>Chart Type </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select style="width:200px;" id="avgMeetingLengthByTopicSelect"  onchange="return topicMeetingChartChange(this.value,'avgMeetingLengthByTopicSelect')" >
					<option value="0">Column Chart</option>
					<option value="Bar">Bar Chart</option>
					<option value="Area">Area Chart</option>
					<option value="Line">Line Chart</option>
				</select>
				<div id="avgMeetingLengthByTopic"></div>
			</div>
            
            
            
			
			
			
			<?php /*<div class="col-lg-6 dashboard"><label>Chart Type </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<select style="width:200px;" id="menteeSkillchartSelect"  onchange="return topicMeetingChartChange(this.value,'menteeSkillchartSelect')" >
					<option value="0">Column Chart</option>
					<option value="Bar">Bar Chart</option>
					<option value="Line">Line Chart</option>
				</select>
				<div id="menteeSkillchart"></div>
			</div>*/ ?>
			 
			<div class="col-lg-6 dashboard">
				<!--<strong style="display: inline-block;margin-bottom: 10px;">Mentor/Mentee Meetings</strong>-->
				<label>Chart Type </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select style="width:200px;" id="activeMetorsSelect"  onchange="return topicMeetingChartChange(this.value,'activeMetorsSelect')">
					<option value="0">Pie Chart</option>
					<option value="Bar">Bar Chart</option>
					<option value="Area">Area Chart</option>
					<option value="Line">Line Chart</option>
				</select>
				<div id="activeMetors"></div>
			</div> 
			<?php }?>
           <div style="border-bottom: 2px solid rgb(221, 221, 221); float: left; display: inline-block; width: 100%; margin-top: 20px;" id="lineSepreator"></div>
			<div class="col-lg-12 dashboard" style="margin-top:61px;">
				<!--<strong style="display: inline-block;margin-bottom: 10px;">Mentor/Mentee Meetings</strong>-->
				<div id="barchart_values"></div>
			</div>
			 <div style="border-bottom: 2px solid rgb(221, 221, 221); float: left; display: inline-block; width: 100%; margin-top: 20px;" id="lineSepreator"></div>

			<div class="col-lg-12 dashboard" style="margin-top:61px;"><label><?php /*Chart Type </label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select style="width:200px;" id="columnchart_materialSelect"  onchange="return topicMeetingChartChange(this.value,'columnchart_materialSelect')" >
				<option value="0">Column Chart</option>
				<option value="Bar">Bar Chart</option>
				<option value="Line">Line Chart</option>
			</select>*/?>
			<div id="columnchart_material"></div></div>
            
           


        </section>
    </section>
    <!--main content end-->
    <!--right sidebar start-->

    <!--right sidebar end-->
</section>
