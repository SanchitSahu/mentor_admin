<link rel="stylesheet" href="<?php echo $this->config->item('base_css'); ?>pick-a-color-1.2.3.min.css">
<link href="<?php echo $this->config->item('base_css'); ?>pickadate/default.css" rel="stylesheet" />
<link href="<?php echo $this->config->item('base_css'); ?>pickadate/default.date.css" rel="stylesheet" />
<link href="<?php echo $this->config->item('base_css'); ?>pickadate/default.time.css" rel="stylesheet" />

<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>pickadate.js"></script>
<style>
    .custom-error{
        color: #b94a48;
        display: inline;
        font-weight: 400;
        margin: 5px 0;
    }	
	.no-ft td {
		float: left !important;
	}
    .divClass{margin-bottom:0px !important;}
    #uploadFile{padding-bottom: 3px;
                padding-top: 9px;}
    </style>		


    <?php //echo "<pre>";print_r($mentor);exit;?>
    <!--header start-->
    <?php echo $this->load->view('admin/includes/header'); ?>
    <!--header end-->
    <?php
    $res['config'] = $this->settings_model->getConfig();
	
    echo $this->load->view('includes/left_menu', $res);
    ?>


    <section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>There are various options in this section that you can change to customize the look and feel of MELS Admin Interface. These changes also affect the mobile app</strong>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <!--<a href="javascript:;" class="fa fa-times"></a>-->
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="col-lg-12 col-sm-12 col-md-12">
                            <div class="setting-block" id="settingBlock">
                                <form method="post" id="config_form" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/settings/updateConfig">
                                    <div class="col-lg-12 col-sm-12 col-md-6">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <label>What do you call an Advisor (Advisor, Mentor, etc.)?</label>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <input style="width:225px;padding:7px 3 px;" type="text" value="<?php echo $config[0]->mentorName ?>" id="mentorName" name="mentorName">
                                        </div>
										<div class="mentorNameError"></div>
                                    </div>
                                    <div class="col-lg-12 col-sm-12 col-md-6">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label>What do you call an Entrepreneur (Mentee, Client, Helixer, etc.)?</label></div>
                                        <div class="col-lg-6 col-sm-12 col-md-6"><input style="width:225px;padding:7px 3 px;" type="text" value="<?php echo $config[0]->menteeName ?>" id="menteeName" name="menteeName"></div>
										<div class="menteeNameError"></div>
									</div>
									 <div class="col-lg-12 col-sm-12 col-md-6">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label>Department Name (simple HTML styles allowed)</label></div>
                                        <div class="col-lg-6 col-sm-12 col-md-6"><input style="width:225px;padding:7px 3 px;" type="text" value='<?php echo $config[0]->DepartmentName ?>' id="DepartmentName" name="DepartmentName"></div>
										<div class="DepartmentNameErr"></div>
									</div>
									 
									<div class="col-lg-12 col-sm-12 col-md-6">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <label>Team Manager</label>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <input style="width:225px;padding:7px 3 px;" type="text" value="<?php echo $config[0]->teamManager ?>" id="teamManager" name="teamManager">
                                        </div>
										<div class="teamManagerError"></div>
                                    </div>
									
									<div class="col-lg-12 col-sm-12 col-md-6">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
											<label  style="float: left;">Impromptu</label>
										</div> 
                                        <div class="col-lg-6 col-sm-12 col-md-6">
											<input style="width:225px;padding:7px 3 px;" type="text" value="<?php echo $config[0]->Impromptu ?>" id="Impromptu" name="Impromptu">
										</div>
										<div class="ImpromptuError"></div>
									</div>
	
                                    <div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label>Local file containing logo</label> </div>
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input id="uploadFile" placeholder="Choose File" value="<?php echo $config[0]->logo ?>" disabled="disabled" />
                                            <div class="fileUpload btn btn-3d-success" style="background-color:<?php echo $config[0]->buttonColor; ?>">
                                                <span>Browse</span>
                                                <input type="file" id="logo" class="upload" name="logo">			
                                            </div>
                                            <div class="custom-error" id="fileErr" style="display:none;"></div>
                                        </div></div>

                                    <div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label>Local file containing icon</label> </div>
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input id="uploadFileicon" placeholder="Choose File" value="<?php echo $config[0]->icon ?>" disabled="disabled" />
                                            <div class="fileUpload btn btn-3d-success" style="background-color:<?php echo $config[0]->buttonColor; ?>">
                                                <span>Browse</span>
                                                <input type="file" id="icon" class="upload" name="icon">			
                                            </div>
                                            <div class="custom-error" id="iconfileErr" style="display:none;"></div>
                                        </div></div>


                                    <div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label style="float: left;">Logo background color</label></div>
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input type="text" value="<?php echo $config[0]->headerColor ?>" id="headerColor" class="pick-a-color" name="headerColor"></div></div>

                                    <div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label style="float: left;">Left-panel Menu font color</label></div>
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input type="text" value="<?php echo $config[0]->leftMenuFont ?>" id="leftMenuFont" class="pick-a-color" name="leftMenuFont"></div></div>

                                    <div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label  style="float: left;">Left-panel background color</label></div> 
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input type="text" value="<?php echo $config[0]->leftBarColor ?>" id="leftBarColor" class="pick-a-color" name="leftBarColor"></div></div>

                                    <div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label style="float: left;">Table header text font color</label></div>
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input type="text" value="<?php echo $config[0]->tableFont ?>" id="tableFont" class="pick-a-color" name="tableFont"></div></div>

                                    <div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label style="float: left;">Table header Background color</label></div>
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input type="text" value="<?php echo $config[0]->tableBackground ?>" id="tableBackground" class="pick-a-color" name="tableBackground"></div></div>


                                    <div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label  style="float: left;">Button background color</label></div> 
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input type="text" value="<?php echo $config[0]->buttonColor ?>" id="buttonColor" class="pick-a-color" name="buttonColor"></div></div>

                                    <div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label  style="float: left;">Default font color</label></div> 
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input type="text" value="<?php echo $config[0]->fontColor ?>" id="fontColor" class="pick-a-color" name="fontColor"></div></div>

                                    <div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label  style="float: left;">Background color</label></div> 
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input type="text" value="<?php echo $config[0]->backgroundColor ?>" id="backgroundColor" class="pick-a-color" name="backgroundColor"></div></div>

                                    <div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label  style="float: left;">Highlight background color</label></div> 
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input type="text" value="<?php echo $config[0]->highlightColor ?>" id="highlightColor" class="pick-a-color" name="highlightColor"></div></div>
									
									<div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label  style="float: left;">Max Member in a Team allowed (not more than 12)</label></div> 
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input style="width:50px;padding:7px 3 px;" type="number" maxlength="4" max="7" value="<?php echo $config[0]->maxMember ?>" id="maxMember" name="maxMember"></div>
										<div class="maxMemberError"></div>
									</div>
									
									<div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label  style="float: left;">Invitation expires if not answered in [N] hours</label></div> 
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input style="width:50px;padding:7px 3 px;" type="text" maxlength="4" value="<?php echo $config[0]->expiryTime ?>" id="expiryTime" name="expiryTime"></div>
										<div class="expiryTimeError"></div>
									</div>
										
									<div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label  style="float: left;">Missed meeting canceled after [M] hours:</label></div> 
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input style="width:50px;padding:7px 3 px;" type="text" maxlength="2" value="<?php echo $config[0]->missedMeeting ?>" id="missedMeeting" name="missedMeeting"></div>
										<div class="missedMeetingError"></div>
									</div>	
									
									<div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label  style="float: left;">Fiscal Year:</label></div> 
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><div class="input-group date" id="datepicker"><input type="text" id="fiscalYear" name="fiscalYear" value="<?php echo $config[0]->fiscalYear ?>" class="form-control datepicker" /><span class="input-group-addon start"><span class="glyphicon glyphicon-calendar"></span></span></div></div>
										<div class="fiscalYearError"></div>
									</div>
									
									<div class="col-lg-12 col-sm-12 col-md-6 divClass">
                                        <div class="col-lg-6 col-sm-12 col-md-6"><label  style="float: left;">No of fiscal periods</label></div> 
                                        <div class="col-lg-6 col-sm-12 col-md-6 noMargin"><input style="width:50px;padding:7px 3 px;" type="text" maxlength="1" value="<?php echo $config[0]->noOfPeriod ?>" id="noOfPeriod" name="noOfPeriod"></div>
										<div class="noOfPeriodError"></div>
										
										</div>
									
								</label>	
									<div class="col-lg-12 col-sm-12 col-md-12">
										<table class="semesterTable col-lg-12 col-sm-12 col-md-12">
											<tr class="col-lg-12 col-sm-12 col-md-12 no-ft">
												<td class="col-lg-4 col-sm-12 col-md-4"><label>Semester Name</label></td>
												<td class="col-lg-3 col-sm-12 col-md-3"><label>Semester Start Date</label></td>
												<td class="col-lg-3 col-sm-12 col-md-3"><label>Semester End Date</label></td>
												<td class="col-lg-2 col-sm-12 col-md-2"><label>Add/Remove</label></td>
											</tr>
											<!--<tr class="col-lg-12 col-sm-12 col-md-12">
												<td class="col-lg-4 col-sm-12 col-md-4"><input type="text" value="<?php if(!empty($semester[0]->SemesterName)) echo $semester[0]->SemesterName;echo ""; ?>" name="semesterName[]"></td>
												<td class="col-lg-3 col-sm-12 col-md-3">
													<div class='input-group date' id='datepicker'>
														<input type='text' id="start4" name="startDate[]" value="<?php if(!empty($semester[0]->SemesterName)) echo $semester[0]->SemesterStartDate;echo ""; ?>" class="form-control datepicker" />
														<span class="input-group-addon start">
															<span class="glyphicon glyphicon-calendar"></span>
														</span>
													</div>
												</td>
												<td class="col-lg-3 col-sm-12 col-md-3">
													<div class='input-group date' id='datepicker'>
														<input type='text' id="end4" name="endDate[]" value="<?php if(!empty($semester[0]->SemesterName)) echo $semester[0]->SemesterEndDate;echo ""; ?>" class="form-control datepicker" />
														<span class="input-group-addon end">
															<span class="glyphicon glyphicon-calendar"></span>
														</span>
													</div>
												</td>
												<td class="col-lg-2 col-sm-12 col-md-2"><button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button></td>
											</tr>-->
											
											
									
									<?php
									if(isset($semester) && count($semester) > 0) {
										$cnt = 0;
										foreach($semester as $data){
											$cnt++;
											addSemesterDiv($data, 'remove',$cnt);
										}
										//addSemesterDiv('', 'add');
									} else {
										addSemesterDiv("", 'add',1);
									}
									
									function addSemesterDiv($singleSemester="", $showButton,$count){
										$html = '<tr class="col-lg-12 col-sm-12 col-md-12">';
										$html .= '<td class="col-lg-4 col-sm-12 col-md-4"><input type="text" value="' . (!empty($singleSemester->SemesterName) ? $singleSemester->SemesterName : "") . '" name="semesterName[]"></td>';
										$html .= '<td class="col-lg-3 col-sm-12 col-md-3">';
										$html .= '<div class="input-group date" id="datepicker">';
										$html .= '<input type="text" id="start4" name="startDate[]" value="' . ((!empty($singleSemester->SemesterName)) ? $singleSemester->SemesterStartDate:"") . '" class="form-control datepicker" />';
										$html .= '<span class="input-group-addon start">';
										$html .= '<span class="glyphicon glyphicon-calendar"></span>';
										$html .= '</span>';
										$html .= '</div>';
										$html .= '</td>';
										$html .= '<td class="col-lg-3 col-sm-12 col-md-3">';
										$html .= '<div class="input-group date" id="datepicker">';
										$html .= '<input type="text" id="end4" name="endDate[]" value="' . (!empty($singleSemester->SemesterName) ? $singleSemester->SemesterEndDate : "") . '" class="form-control datepicker" />';
										$html .= '<span class="input-group-addon end">';
										$html .= '<span class="glyphicon glyphicon-calendar"></span>';
										$html .= '</span>';
										$html .= '</div>';
										$html .= '</td>';
										$html .= '<td class="col-lg-2 col-sm-12 col-md-2">';
										//if($showButton == 'add') {
										if($count == 1) {
											$html .= '<button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>';
										} else if($showButton == 'remove') {
											$html .= '<button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>';
										}
										$html .='</td>';
										$html .= '</tr>';
										echo $html;
									}
									?>
									
										</table>
									</div>


                                    <div class="col-lg-6 col-sm-12 col-md-6">
                                        <button class="btn btn-3d-success" style="background-color:<?php echo $config[0]->buttonColor; ?>" onclick="successalertify();" type="submit">Save</button>

                                    </div>
                                    <div class="col-lg-12 col-sm-12 col-md-6">
                                    <!--<input class="btn btn-3d-success" type="submit" value="Save">--></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<style>
    .fileUpload {
        position: relative;
        overflow: hidden;
    }
    .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
</style>
<script src="<?php echo $this->config->item('base_js'); ?>tinycolor-0.9.15.min.js"></script>
<script src="<?php echo $this->config->item('base_js'); ?>pick-a-color-1.2.3.min.js"></script>
<script>
    function successalertify() {
     //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
     }
     //function erroralertify() {
      //   alertify.error("Some Error has occured.");
     ///}
     //function logalertify() {
//     	alertify.log("User has cancelled event");
//        setTimeout(function () {
//	        window.location.href = "<?php echo $this->config->item('base_url').'admin/dashboard'?>";
//        },1000);
     //}

    $(document).ready(function () {
		
		
		
		var $table = $('table.semesterTable');
        var MaxOption	= 4;
		var currRows = $('.semesterTable tr').length;
		var insertRows = MaxOption - (currRows-1);
		//alert(MaxOption);alert(currRows);
		
		// Add button click handler
        $("form").on('click', '.addButton', function() {
			if(insertRows !== 0){
				//$(".newRow").removeClass('hide');
				
				var row	=	'<tr class="col-lg-12 col-sm-12 col-md-12"><td class="col-lg-4 col-sm-12 col-md-4"><input type="text" name="semesterName[]"></td><td class="col-lg-3 col-sm-12 col-md-3"><div class="input-group date" id="datepicker"><input type="text" id="start'+insertRows+'" name="startDate[]" class="form-control datepicker" /><span class="input-group-addon start"><span class="glyphicon glyphicon-calendar"></span></span></div></td><td class="col-lg-3 col-sm-12 col-md-3"><div class="input-group date" id="datepicker"><input type="text" id="end'+insertRows+'" name="endDate[]" class="form-control datepicker" /><span class="input-group-addon end"><span class="glyphicon glyphicon-calendar"></span></span></div></td><td class="col-lg-2 col-sm-12 col-md-2"><button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button></td></tr>';
				
				$table.append(row);
				insertRows--;
			}
				
        });

		$table.on('click', '.removeButton', function() {
			$(this).closest('tr').remove();
			insertRows++;
		});
		
		$(document.body).on("click",".datepicker", function(){
			$('.datepicker').pickadate({
				format:'yyyy-mm-dd',
			//formatSubmit: 'yyyy-mm-dd',
			});
		});
		
		$(document.body).on("click",".start", function(event){
		//$('.start').click(function(event){
			//alert("fff");
			event.stopPropagation();
			//$('#start'+MaxOption).pickadate("picker").open();
			$(this).prev().prev().pickadate("picker").open();
		});
		
		$(document.body).on("click",".end", function(event){
		//$('.end').click(function(event){
			event.stopPropagation();
			//$('#end'+MaxOption).pickadate("picker").open();
			$(this).prev().prev().pickadate("picker").open();
		});
		
		
        $('#logo').change(function () {
            var val = $(this).val().toLowerCase();
            var regex = new RegExp("(.*?)\.(jpg|jpeg|svg|png|ico)$");

            if (!(regex.test(val))) {
                $(this).val('');
                $("#fileErr").html("Unsupported file");
                $("#fileErr").css("display", "block");
            } else {
                document.getElementById("uploadFile").value = val.split("\\").pop();
                document.getElementById("logo").onchange = function () {
                    document.getElementById("uploadFile").value = this.value;
                };
            }

        });
        $('#icon').change(function () {
            var val = $(this).val().toLowerCase();
            var regex_icon = new RegExp("(.*?)\.(jpg|jpeg|svg|png|ico)$");
            if (!(regex_icon.test(val))) {
                $(this).val('');
                $("#iconfileErr").html("Unsupported file");
                $("#iconfileErr").css("display", "block");
            } else {
                $("#iconfileErr").html("");
                $("#iconfileErr").css("display", "none");
                document.getElementById("uploadFileicon").value = val.split("\\").pop();
                document.getElementById("icon").onchange = function () {
                    document.getElementById("uploadFileicon").value = this.value;
                };
            }
        });

        $(".pick-a-color").pickAColor({
            showSpectrum: true,
            showSavedColors: true,
            saveColorsPerElement: true,
            fadeMenuToggle: true,
            showAdvanced: true,
            showBasicColors: true,
            showHexInput: true,
            allowBlank: true,
            inlineDropdown: true
        });

		$("#expiryTime").keypress(function (event) {
			 //if the letter is not digit then display error and don't type anything
			  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
				event.preventDefault();
			  }
			/* if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				//display error message
				$("#expiryTimeError").html("Digits Only").show().fadeOut("slow");
					   return false;
			}*/
		});
		
		$("#missedMeeting").keypress(function (e) {
			 //if the letter is not digit then display error and don't type anything
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				//display error message
				$("#missedMeetingError").html("Digits Only").show().fadeOut("slow");
					   return false;
			}
		});
    
	
	$("#config_form").validate({
            rules: {
                mentorName: {
					required: true
				},
                menteeName: {
                    required: true
                },
                expiryTime: {
                    required: true
                },
				missedMeeting: {
                    required: true
                }
            },
            messages: {
                mentorName: {
                    required: "Please enter <?php echo $config[0]->mentorName ?> name"
                },
                menteeName: {
                    required: "Please enter <?php echo $config[0]->menteeName ?> phone"
                },
                expiryTime: {
                    required: "Please enter expiry time"
                },
                missedMeeting: {
                    required: "Please enter missed meeting"
                }
            }
        });
	});
	
	$("form").submit(function(){
		var n = $("input[name^='startDate']").length;
		var array = $("input[name^='startDate']");
		//alert(n);
		var endN = $("input[name^='endDate']").length;
		var endArr = $("input[name^='endDate']");
		//alert(endN);
		
		for(i=0; i < n; i++) {
			startVal = array.eq(i).val();
			//alert(startVal);
			for(j=0; j < endN; j++) {
				endVal = endArr.eq(i).val();
				//alert(endVal);
				if( (new Date(startVal).getTime() > new Date(endVal).getTime()))
				{
					
					//$(this).css({"border-color": "#FF0000", "border-width":"1px", "border-style":"solid"});
					alertify.log('From date can not be greater then To date');
					return false;
				}
			}
		}

	});
</script>
