
<?php //print_r($invitation);exit;?>

<section id="container" >
    <!--header start-->
    <?php //echo $this->load->view('admin/includes/header'); ?>
    <!--header end-->
    <?php $res['config']	=	$this->settings_model->getConfig();
	//echo $this->load->view('includes/left_menu',$res); ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong>Reminder Settings</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                                <!--<a href="javascript:;" class="fa fa-times"></a>-->
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = 'admin/ReminderSettings/add';
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_skill_form", 'method' => "post"));
                                ?>
                                <div class="form-group ">
                                    <label for="skillname" class="control-label col-lg-3">Number of times to send reminder after invitation</label>
                                    <div class="col-lg-6">
                                    	<?php $days	=	"";if(isset($invitation[0]->SendReminderDay)) $days	=	 $invitation[0]->SendReminderDay;else $days='';?>
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Skill Name", 'name' => 'days', 'value' => $days)); ?>
                                        <?php echo form_error('days'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-3">Number of days between reminders</label>
                                    <div class="col-lg-6">
                                    	 <?php $gap	=	"";if(isset($invitation[0]->SendReminderGap)) $gap	=	 $invitation[0]->SendReminderGap;else $gap='';?>
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Gap", 'name' => 'gap', 'value' => $gap)); ?>
                                        <?php echo form_error('gap'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
     function successalertify() {
     //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
     }
     function erroralertify() {
      //   alertify.error("There has been some Error occured.");
     }
     function logalertify() {
     	alertify.log("User has cancelled event");
        setTimeout(function () {
	        window.location.href = "<?php echo $this->config->item('base_url').'admin/skill'?>";
        },1000);
     }


    $().ready(function() {
        // validate signup form on keyup and submit
        $("#add_skill_form").validate({
            rules: {
                SkillName: {
                    required: true,
                    minlength: 2
                }
            },
            messages: {
                SkillName: {
                    required: "Please enter skill name",
                    minlength: "Your skill name must consist of at least 2 characters"
                }
            }
        });
        
    });
</script>