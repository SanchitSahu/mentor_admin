<?php
if ($clientSource['Weight'] == "")
    $clientSource['Weight'] = 1000;
else
    $clientSource['Weight'] = $clientSource['Weight'];
?>

<section id="container" >
    <?php $res['config'] = $this->settings_model->getConfig(); ?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong>Source</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'admin/clientSource/add' : 'admin/clientSource/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_MenteeSource_form", 'method' => "post"));
                                ?>
                                <div class="form-group ">
                                    <label for="MenteeSourceName" class="control-label col-lg-3">Source Name</label>
                                    <div class="col-lg-6">
                                        <input type="hidden" id="MenteeSourceID" name="MenteeSourceID" value="<?php echo set_value('MenteeSourceID', $clientSource['MenteeSourceID']); ?>" />
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Source Name", 'name' => 'MenteeSourceName', 'value' => $clientSource['MenteeSourceName'])); ?>
                                        <?php echo form_error('MenteeSourceName'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-3">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight Name", 'name' => 'Weight', 'value' => $clientSource['Weight'])); ?>
                                        <?php echo form_error('Weight'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
    function successalertify() {
        //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
    }
    function erroralertify() {
        //   alertify.error("There has been some Error occured.");
    }
    function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'admin/clientSource' ?>";
        }, 1000);
    }


    $().ready(function () {
        // validate signup form on keyup and submit
        $("#add_MenteeSource_form").validate({
            rules: {
                MenteeSourceName: {
                    required: true,
                    remote: {
                        url: "<?php echo $this->config->item('base_url'); ?>admin/clientSource/checkMenteeSourceExist",
                        type: "post",
                        data: {
                            MenteeSourceID: function () {
                                return $('#MenteeSourceID').val();
                            }
                        }
                    }
                }
            },
            messages: {
                MenteeSourceName: {
                    required: "Please enter source name",
                    remote: "Source with same name already exists"
                }
            }
        });

    });
</script>