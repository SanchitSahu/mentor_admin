<style>
	.panel-body-top {
		width: 100%;
		float: left;
		margin-bottom: 1px;
	}
	.panel-body-top input {
		margin-right: 5px;
	}
	.panel-body-top > div:not(.addStyle) {
		border-right: 1px solid #fff;
		float: left;
		padding: 5px 10px;
		min-height: 30px;
	}
	.picker__weekday-display {
		background-color: #7A7AAE;
	}
	.picker__date-display {
		background-color: #b3b3ff;
	}
	.picker__close, .picker__today {
		color: #0044CC;
	}
	.picker__day.picker__day--today{
		color: #0044CC;
	}
	label.dateRange_label{
		float: left;
		margin-top: 8px;
	}
	div.addStyle input.datepicker, div.addStyle span.input-group-addon span{
		margin-left: 30px;
	}

	#dateFrom, #dateTo{background-color: white;
      cursor: pointer;}
</style>

<div class="panel-body-top">
	<div class="filterDiv col-lg-6 col-sm-6 col-md-6">
		<label class="dateRange_label">Date From: </label>
		<div class='addStyle input-group date'>
			<input type='text' id="dateFrom" class="form-control datepicker" />
			<span class="input-group-addon from_date">
				<span class="glyphicon glyphicon-calendar"></span>
			</span>
		</div>
	</div>
	
	<div class="filterDiv col-lg-6 col-sm-6 col-md-6">
		<label class="dateRange_label">Date To: </label>
		<div class='addStyle input-group date'>
			<input type='text' id="dateTo" class="form-control datepicker" />
			<span class="input-group-addon to_date">
				<span class="glyphicon glyphicon-calendar"></span>
			</span>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		var today = new Date();
		var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
		var nextdates = new Date(new Date().setYear(new Date().getFullYear() + 10));
		$('.datepicker').pickadate({
			format:'yyyy-mm-dd',
			disable: [
				{
					from: tomorrow,
					to: nextdates
				}
			],
			onSet: function() {
				var from 	=	$("#dateFrom").val();
				var to 		=	$("#dateTo").val();
				if( (new Date(from).getTime() > new Date(to).getTime()))
				{
					alertify.log('From date can not be greater then To date');
				}
			}
		});
		$('.from_date').click(function(event){
			event.stopPropagation();
			$('#dateFrom').pickadate("picker").open();
		});
		$('.to_date').click(function(event){
			event.stopPropagation();
			$('#dateTo').pickadate("picker").open();
		});
	});
</script>
<!--<div class="panel-body-top">
	<div class="col-lg-12 col-sm-12 col-md-12 widthDiv">This</div>
	<div class="col-lg-12 col-sm-12 col-md-1 widthDiv"><input type="radio" value="Today" name="filter">Today</div>
	<div class="col-lg-12 col-sm-12 col-md-1 widthDiv"><input type="radio" value="Week" name="filter">Week</div>
	<div class="col-lg-12 col-sm-12 col-md-1 widthDiv"><input type="radio" value="Month" name="filter">Month</div>
	<div class="col-lg-12 col-sm-12 col-md-1 widthDiv"><input type="radio" value="Quarter" name="filter">Quarter</div>
	<div class="col-lg-12 col-sm-12 col-md-1 widthDiv"><input type="radio" value="Year" name="filter">Year</div>
	<div class="col-lg-12 col-sm-12 col-md-1 widthDiv"><input type="radio" value="YTD" name="filter">YTD</div>
	<div class="col-lg-12 col-sm-12 col-md-3 widthDiv"><input type="radio" value="FP" name="filter">Fiscal Year</div>
</div>
<div class="panel-body-top">
	<div class="col-lg-12 col-sm-12 col-md-12 widthDiv">Last&nbsp;&nbsp;<select id="lastN" name="lastN">
			<option>Select</option>
		</select></div>
	
	<div class="col-lg-12 col-sm-12 col-md-12 widthDiv"><input type="radio" value="Days" name="filterN">Days</div>
	<div class="col-lg-12 col-sm-12 col-md-12 widthDiv"><input type="radio" value="Week" name="filterN">Weeks</div>
	<div class="col-lg-12 col-sm-12 col-md-12 widthDiv"><input type="radio" value="Month" name="filterN">Months</div>
	<div class="col-lg-12 col-sm-12 col-md-12 widthDiv"><input type="radio" value="Quarter" name="filterN">Quarters</div>
	<div class="col-lg-12 col-sm-12 col-md-12 widthDiv"><input type="radio" value="Year" name="filterN">Years</div>
	<div class="col-lg-12 col-sm-12 col-md-12 widthDiv"><input type="radio" value="FYE" name="filterN">FYE</div>
	<div class="col-lg-12 col-sm-12 col-md-12 widthDiv"><input type="radio" value="FP" name="filterN">Fiscal Periods</div>
</div>-->
