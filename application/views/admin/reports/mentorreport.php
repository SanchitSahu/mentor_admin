<?php //echo "<pre>";print_r($config);exit;?>

<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong><?php echo $config[0]->mentorName?>s' Report</strong>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <!--<a href="javascript:;" class="fa fa-times"></a>-->
                        </span>
						
						<br><div style="text-transform: none;">You could select reporting period or <?php echo $config[0]->mentorName;?> role or <?php echo $config[0]->mentorName;?> source or combination of these filters to generate custom report.</div>
                    </header>
                    <div class="panel-body">
						<?php $this->load->view("admin/reports/mentorFilter"); ?>
						
						<div class="panel-body-top">
							<div class="filterDiv">
								<label>Filter By <?php echo $config[0]->mentorName?> Role</label>
								<select name="roleName" id="roleName">
									<option value="0">-- Select <?php echo $config[0]->mentorName?> Role --</option>
									<?php foreach($role as $val){ ?>
										<option value="<?php echo $val->userRoleID; ?>"><?php echo $val->UserRoleName; ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="filterDiv">
								<label>Filter By <?php echo $config[0]->mentorName?> Source</label>
								<select name="sourceName" id="sourceName">
									<option value="0">-- Select <?php echo $config[0]->mentorName?> Source --</option>
									<?php foreach($source as $val){ ?>
										<option value="<?php echo $val->MentorSourceID; ?>"><?php echo $val->MentorSourceName; ?></option>
									<?php } ?>
								</select>
							</div>
							
							<div class="filterDiv">
								<label>Filter By <?php echo $config[0]->menteeName?> Programme</label>
								<select name="programmeName" id="programmeName">
									<option value="0">-- Select <?php echo $config[0]->menteeName?> Programme --</option>
									<?php foreach($programme as $val){ ?>
										<option value="<?php echo $val->ProgrammeID; ?>"><?php echo $val->ProgrammeName; ?></option>
									<?php } ?>
								</select>
							</div>
							
							<div class="filterDiv">
								<label>Filter By <?php echo $config[0]->menteeName?> Source</label>
								<select name="menteeSourceName" id="menteeSourceName">
									<option value="0">-- Select <?php echo $config[0]->menteeName?> Source --</option>
									<?php foreach($menteesource as $val){ ?>
										<option value="<?php echo $val->MenteeSourceID; ?>"><?php echo $val->MenteeSourceName; ?></option>
									<?php } ?>
								</select>
							</div>
							
							
							<!--<div class="filterDiv">
								<label>Filter By Semester</label>
								<select name="semesterName" id="semesterName">
									<option value="0">--Select--</option>
									<?php foreach($semester as $val){ ?>
										<option value="<?php echo $val->SemesterID; ?>"><?php echo $val->SemesterName; ?></option>
									<?php } ?>
								</select>
							</div>-->
							
						</div>
						<div class="panel-body-top">
							<div class="filterDiv1"><button class="btn btn-3d-success"  id="reportGenerate">Generate Report</button></div>
						</div>
						
                        <div class="adv-table">
                            <?php echo $this->table->generate(); ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
		<div class="row">
			<div class="col-sm-5 center"></div>
            <div class="col-sm-4 center">
				<form action="<?php echo base_url(); ?>admin/reports/exporttoexcel" class="exconvert"  method="post">
				<input type="hidden" id="expo1" name="expo1">
				<input type="submit" class="btn btn-3d-success" value="Download report data in Excel" />
			  </form>
			</div>
		</div>	

		
        <!-- page end-->
    </section>
</section>
<!-- Init variables for DATATABLE.  Note: Please put Datatable js files at footer  -->
<script>
    var dattTableName ='dynamic_table2';
    var sAjaxSource = '<?php echo base_url(); ?>admin/reports/mentordatatable';
    var baseUrl  = '<?php echo base_url();?>';
</script>
<!-- Init variables for DATATABLE end -->

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>          
<script type="text/javascript">
$(document).ready(function(){
    $(document).on("submit", '.exconvert', function(event) { 
        $("#expo1").val( $("<div>").append( $("#dynamic_table2").eq(0).clone() ).html() )
     });          
});



$('input:radio[name="filter"]').change(function() {
	$('input:radio[name="filterN"]').removeAttr('checked');
	/*var value = $(this).val();
	var page  = "mentordatatable";
	//alert(value);
	$.ajax({
		url: '<?php echo base_url();?>admin/filter/filterDataByType',
		data:"filterType="+value+"&page="+page,
		success: function(result){
			var obj = jQuery.parseJSON(result);
			//alert(obj.toSource());
			$('#dynamic_table').dataTable({
				"bProcessing": true,
				"bDestroy":true,
				"aaData": obj.aaData,// <-- your array of objects
			});
		}
	});	*/
});


$('input:radio[name="filterN"]').change(function() {
	$('input:radio[name="filter"]').removeAttr('checked');
	var value = $(this).val();
	$.ajax({
		url: '<?php echo base_url();?>admin/filter/getDrpDwn',
		data:"filterType="+value,
		success: function(result){
			$("#lastN").empty();
			$("#lastN").append(result);
		}
	});	
});

/*$('#lastN').change(function() {
	var value = $(this).val();
	var page  = "mentordatatable";
	//alert(value);
	var filterType	=	$("input[name=filterN]:checked").val();
	//alert(filterType);
	$.ajax({
		url: '<?php echo base_url();?>admin/filter/filterNDataByType',
		data:"filterType="+filterType+"&number="+value+"&page="+page,
		success: function(result){
			var obj = jQuery.parseJSON(result);
			//alert(obj.toSource());
			 $('#dynamic_table').dataTable({
				"bProcessing": true,
				"bDestroy":true,
				"aaData": obj.aaData,// <-- your array of objects
			  });
			
		}
	});	
});*/

$('#reportGenerate').click(function() {
	//var filterTypeF	=	$("input[name=filter]:checked").val();
	//var filterTypeN	=	$("input[name=filterN]:checked").val();
	//var number 		=	$("#lastN").val();
	var page  		=	"mentordatatable";
	var role 		=	$("#roleName").val();
	var source 		=	$("#sourceName").val();
	var programme 		=	$("#programmeName").val();
	var menteesource 		=	$("#menteeSourceName").val();
	//var semester 	=	$("#semesterName").val();
	
	
	//alert(filterTypeF);alert(filterTypeN);
	/*if(filterTypeF != undefined){
		
		$.ajax({
			url: '<?php echo base_url();?>admin/filter/filterDataByType',
			data:"filterType="+filterTypeF+"&number="+number+"&page="+page+"&role="+role+"&source="+source+"&semester="+semester,
			success: function(result){
				var obj = jQuery.parseJSON(result);
				//alert(obj.toSource());
				$('#dynamic_table2').dataTable({
					"bProcessing": true,
					"bDestroy":true,
					"aaData": obj.aaData,// <-- your array of objects
				});
			}
		});	
	} else if(filterTypeN != undefined){
		if(number == 0){
			alert("select Last"+filterTypeN);
		}else{
			$.ajax({
				url: '<?php echo base_url();?>admin/filter/filterNDataByType',
				data:"filterType="+filterTypeN+"&number="+number+"&page="+page+"&role="+role+"&source="+source+"&semester="+semester,
				success: function(result){
					var obj = jQuery.parseJSON(result);
					//alert(obj.toSource());
					 $('#dynamic_table2').dataTable({
						"bProcessing": true,
						"bDestroy":true,
						"aaData": obj.aaData,// <-- your array of objects
					  });	
				}
			});	
		}
	} else {*/
		var startDate 	=	$("#dateFrom").val();
        var endDate 		=	$("#dateTo").val();
        var from = new Date(startDate).getTime();
        var to = new Date(endDate).getTime();
        
        if((!isNaN(from) && isNaN(to)) || (!isNaN(from) && isNaN(to))) {
            alertify.log('Please select both Date from and Date to');
            return false;
        } else if(!isNaN(from) && !isNaN(to)) {
            
        } else {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
    
            var yyyy = today.getFullYear();
            if(dd<10){
                dd = '0' + dd;
            } 
            if(mm < 10){
                mm = '0' + mm;
            } 
            today = yyyy + '-' + mm + '-' + dd;
            //alert(today);
            startDate = '1970-01-01';
            endDate = today;
        }
		
		$.ajax({
            url: '<?php echo base_url();?>admin/filter/page',
            //data:'page=' + page +'&startDate=' + startDate + '&endDate=' + endDate + '&role=' + role + '&source=' + source + '&semester=' + semester,
			data:'page=' + page +'&startDate=' + startDate + '&endDate=' + endDate + '&role=' + role + '&source=' + source + '&programme=' + programme + '&menteesource=' + menteesource,
            success: function(result){
                var obj = jQuery.parseJSON(result);
                //alert(obj.toSource());
                $('#dynamic_table2').dataTable({
                    "bProcessing": true,
                    "bDestroy":true,
                    "aaData": obj.aaData,// <-- your array of objects
                });
            }
        });
	//}
});
</script>