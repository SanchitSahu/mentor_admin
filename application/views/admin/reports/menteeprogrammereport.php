<style>
	.panel-body-top {
		width: 100%;
		float: left;
		margin-bottom: 1px;
	}
	.panel-body-top input {
		margin-right: 5px;
	}
	.panel-body-top > div:not(.addStyle) {
		border-right: 1px solid #fff;
		float: left;
		padding: 5px 10px;
		min-height: 30px;
	}
</style><section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong><?php echo $config[0]->menteeName?> Programmes Reports</strong>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                        </span>
                    </header>
                    <div class="panel-body">
						<div class="panel-body-top">
							<div class="filterDiv">
								<label>Filter By <?php echo $config[0]->menteeName?> Programme</label>
								<select name="programmeName" id="programmeName">
									<option value="0">-- Select <?php echo $config[0]->menteeName?> Programme --</option>
									<?php foreach($programme as $val){ ?>
										<option value="<?php echo $val->ProgrammeID; ?>"><?php echo $val->ProgrammeName; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="filterDiv1"><button class="btn btn-3d-success"  id="reportGenerate">Generate Report</button></div>
                        <div class="adv-table">
                            <?php echo $this->table->generate(); ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
		<div class="row">
			<div class="col-sm-5 center"></div>
            <div class="col-sm-4 center">
				<form action="<?php echo base_url(); ?>admin/reports/exporttoexcel" class="exconvert"  method="post">
				<input type="hidden" id="expo1" name="expo1">
				<input type="submit" class="btn btn-3d-success" value="Download report data in Excel" />
			  </form>
			</div>
		</div>	

        <!-- page end-->
    </section>
</section>
<!-- Init variables for DATATABLE.  Note: Please put Datatable js files at footer  -->
<script>
    var dattTableName ='dynamic_table';
    var sAjaxSource = '<?php echo base_url(); ?>admin/reports/menteeprogrammedatatable';
    var baseUrl  = '<?php echo base_url();?>';
</script>
<!-- Init variables for DATATABLE end -->

  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>          
<script type="text/javascript">
$(document).ready(function(){
    $(document).on("submit", '.exconvert', function(event) { 
        $("#expo1").val( $("<div>").append( $("#dynamic_table").eq(0).clone() ).html() )
     });          
});

$('#reportGenerate').click(function() {
	var page  		=	"menteeprogrammedatatable";
	var programme 		=	$("#programmeName").val();
	//alert(programme);
		$.ajax({
			url: '<?php echo base_url();?>admin/filter/menteeprogrammedatatable',
			data:"programme="+programme,
			success: function(result){
				var obj = jQuery.parseJSON(result);
				//alert(obj.toSource());
				$('#dynamic_table').dataTable({
					"bProcessing": true,
					"bDestroy":true,
					"aaData": obj.aaData,// <-- your array of objects
				});
			}
		});	
});
</script>