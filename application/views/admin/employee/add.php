<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>Employee</strong>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form enctype="multipart/form-data" class="cmxform form-horizontal" class="cmxform form-horizontal " id="empForm" method="post">
                                <div class="form-group ">
                                    <label for="firstname" class="control-label col-lg-3">First Name</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('id' => "firstname", 'name' => 'firstname', 'class' => 'form-control', 'value' => set_value('firstname', $product['vFirstName']), 'placeholder' => 'Enter First Name', 'autofocus')) ?>
                                        <?php echo form_error('firstname', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="lastname" class="control-label col-lg-3">Last Name</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('id' => "lastname", 'name' => 'lastname', 'class' => 'form-control', 'value' => set_value('lastname', $product['vLastName']), 'placeholder' => 'Enter Last Name', 'autofocus')) ?>
                                        <?php echo form_error('lastname', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Photo</label>
                                    <div class="col-md-9">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-white btn-file">
                                                    <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                    <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                    <input type="file" class="default" name="photo" />
                                                </span>
                                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                            </div>
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="empcode" class="control-label col-lg-3">Employee Code</label>
                                    <div class="col-lg-6">
                                        <!-- 'value' => set_value('empcode', $product['vEmployeeCode'])-->
                                        <?php echo form_input(array('id' => "empcode", 'name' => 'empcode', 'class' => 'form-control', 'value' => set_value('empcode', $product['vEmployeeCode']), 'placeholder' => 'Enter Employee Code', 'autofocus')) ?>
                                        <?php echo form_error('empcode', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="username" class="control-label col-lg-3">User Name</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('id' => "username", 'name' => 'username', 'class' => 'form-control', 'value' => set_value('username', $product['vEmployeeCode']), 'placeholder' => 'Enter User name')) ?>
                                        <?php echo form_error('username', '<label class="error">', '</label>'); ?>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="password" class="control-label col-lg-3">Password</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('id' => "password", 'name' => 'password', 'class' => 'form-control', 'type' =>'password', 'placeholder' => 'Password')) ?>
                                        <?php echo form_error('password', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="confirmpassword" class="control-label col-lg-3">Confirm Password</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('id' => "confirmpassword", 'name' => 'confirmpassword', 'class' => 'form-control', 'type' =>'password', 'placeholder' => 'Confirm Password')) ?>
                                        <?php echo form_error('confirmpassword', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <!--
                                <div class="form-group ">
                                    <label class="control-label col-lg-3 control-label">Role</label>
                                    <div class="col-lg-6">
                                        <select class="form-control"  id="roles" name="role">
                                            <option value="">Select Role</option>
                                            <option value="0">Sub Admin</option>
                                            <option value="1">Accountant</option>
                                            <option value="2">HR</option>
                                            <option value="3">Data Entry Operator</option>
                                        </select>
                                    </div>
                                </div>
                                -->
                                <div class="form-group ">
                                    <label for="username" class="control-label col-lg-3">Email</label>
                                    <div class="col-lg-6">                                        
                                        <?php echo form_input(array('id' => "email", 'name' => 'email', 'value' => set_value('email', $product['vEmail']), 'class' => 'form-control', 'placeholder' => 'Enter Email')) ?>
                                        <?php echo form_error('email', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group radios">
                                    <label for="gender" class="control-label col-lg-3">Gender</label>
                                    <label class="label_radio col-lg-1" for="radio-01">
                                        <input name="gender" id="radio-01" value="1" type="radio" checked  /> Male
                                    </label>
                                    <label class="label_radio col-lg-1 col-sm-6" for="radio-02">
                                        <input name="gender" id="radio-02" value="1" type="radio" /> Female
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3 control-label">Birth Date</label>
                                    <div class="col-lg-6">
                                        <input name="birthdate" class="form-control form-control-inline input-medium default-date-picker"  size="16" type="text" value="" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3 control-label">Anniversary Date</label>
                                    <div class="col-lg-6">
                                        <input name="anniversarydate" class="form-control form-control-inline input-medium default-date-picker"  size="16" type="text" value="" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Mobile No.</label>
                                    <div class="col-lg-6">
                                        <input name="mobile" value="<?php echo $product['vCellPhone']; ?>" type="text" placeholder="" data-mask="(999) 999-9999" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Phone No.</label>
                                    <div class="col-lg-6">
                                        <input name="phone" value="<?php echo $product['vPhoneNumber']; ?>" type="text" placeholder="" data-mask="(999) 999-9999" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="address1" class="control-label col-lg-3">Address1</label>
                                    <div class="col-lg-6">
                                        <input class=" form-control" id="Text1" name="address1" type="text" value="<?php echo $product['vAddress1']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="address2" class="control-label col-lg-3">Address2</label>
                                    <div class="col-lg-6">
                                        <input class=" form-control" id="address2" name="address2" type="text"  value="<?php echo $product['vAddress2']; ?>" />
                                    </div>
                                </div>                                    
                                <div class="form-group ">
                                    <label class="control-label col-lg-3 control-label">Country</label>
                                    <div class="col-lg-6">
                                        <select class="form-control" name="country" id="country" onchange="selectState(this.options[this.selectedIndex].value)">
                                             <option value="">Select Country</option>
                                             <?php foreach ($countryData as $country) { ?>
                                                  <option value="<?php echo $country['iCountryId']; ?>"><?php echo ucwords($country['vCountryName']); ?></option>
                                             <?php } ?>
                                        </select>
                                        <?php echo form_error('country', '<label class="error">', '</label>'); ?>
                                    </div>

                                    <!--<div class="col-lg-6">
                                        <?php if ($id): ?>
                                            <?php echo form_dropdown('country', $country, set_value('country', $product['iCountryId']), 'class="form-control" id="country"'); ?>
                                        <?php else: ?>
                                            <?php echo form_dropdown('country', $country, '', 'class="form-control" id="country"'); ?>
                                        <?php endif; ?>
                                        <?php echo form_error('country', '<label class="error">', '</label>'); ?>                                    
                                    </div>-->

                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-3 control-label">State</label>
                                <div class="col-lg-6">
                                    <select class="form-control" name="state" id="state">
                                        <option value="">Select State</option>
                                    </select>
                                    <?php echo form_error('state', '<label class="error">', '</label>'); ?>
                                </div>
                                    <!--<div class="col-lg-6">
                                        <?php if ($id): ?>
                                            <?php echo form_dropdown('state', $state, set_value('state', $product['iStateId']), 'class="form-control" id="country"'); ?>
                                        <?php else: ?>
                                            <?php echo form_dropdown('state', $state, '', 'class="form-control" id="state"'); ?>
                                        <?php endif; ?>
                                        <?php echo form_error('state', '<label class="error">', '</label>'); ?>  
                                    </div>-->
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-3 control-label">City</label>
                                    <div class="col-lg-6">
                                        <input class=" form-control" id="city" name="city" type="text"  value="<?php echo $product['vCity']; ?>" />
                                        <?php /*if ($id): ?>
                                            <?php echo form_dropdown('city', $city, set_value('city', $product['vCityId']), 'class="form-control" id="country"'); ?>
                                        <?php else: ?>
                                            <?php echo form_dropdown('city', $city, '', 'class="form-control" id="city"'); ?>
                                        <?php endif;*/ ?>
                                        <?php echo form_error('city', '<label class="error">', '</label>'); ?>  
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="postcode" class="control-label col-lg-3">Postcode</label>
                                    <div class="col-lg-6">
                                        <!--<input class=" form-control" id="postcode" name="postcode" type="text" />-->
                                        <?php echo form_input(array('id' => "postcode", 'value' => set_value('postcode', $product['vPostCode']), 'name' => 'postcode', 'class' => 'form-control', 'placeholder' => 'Enter Post Code')) ?>
                                        <?php echo form_error('postcode', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();" >Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>


<script type="text/javascript">
    function successalertify() {
       //  alertify.success("Record has been saved successfully.");
       alertify.success("Please wait....");
     }
     function erroralertify() {
        // alertify.error("There has been some Error occured.");
     }
     function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
	        window.location.href = "<?php echo $this->config->item('base_url').'admin/employee'?>";
        },1000);
     }

    function selectState(country_id) {
        if (country_id !== "0") {
            loadState('state', country_id);
            $("#city_dropdown").html("<option value='0'>Select State</option>");
        } else {
            $("#state_dropdown").html("<option value='0'>Select State</option>");
        }
    }

    function loadState(loadType, loadId) {
        var dataString = 'loadType=' + loadType + '&loadId=' + loadId;
        $("#" + loadType + "_loader").show();
        $("#" + loadType + "_loader").fadeIn(400).html('Please wait... ');
        $.ajax({
            type: "POST",
            url: "<?php echo $this->config->item("base_url"); ?>vendors/loadState",
            data: dataString,
            cache: false,
            success: function (result) {
                //$("#signup_state").hide();
                $("#state").html("<option value=''>Select " + loadType + "</option>");
                $("#state").append(result);
            }
        });
    }

    $(document).ready(function () {
	$('.default-date-picker').datetimepicker({
		format: 'yyyy-mm-dd'
	});
    });
    $().ready(function () {
        // validate signup form on keyup and submit
        $("#empForm").validate({
            rules: {
                firstname: "required",
                lastname: "required",
                empcode: "required",
                username: "required",
                password: "required",
                confirmpassword: "required",
                address1: "required",
                country: "required",
                state: "required",
                city: "required",
                postcode: "required",
                mobile: "required"
            },
            messages: {
                firstname: "Please enter first code",
                lastname: "Please enter last name",
                empcode: "Please provide a description",
                username: "Please select username",
                password: "Please enter password",
                confirmpassword: "Please enter confirmpassword",
                address1: "Please enter address",
                country: "Please enter country",
                state: "Please enter state",
                city: "Please enter city",
                postcode: "Please enter postcode",
                mobile: "Please enter mobile"
            }
        });
    });
</script>