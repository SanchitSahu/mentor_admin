
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>Forgot Password</strong>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="form">                            
                            <form id="forgot_pass" enctype="multipart/form-data" class="cmxform form-horizontal" class="cmxform form-horizontal" id="empForm" method="post">
                                <div class="form-group ">
                                    <label for="firstname" class="control-label col-lg-3">Email Address</label>
                                    <div class="col-lg-6">
                                            <?php echo form_input(array('type' => 'email', 'name' => 'Email', 'class' => 'textbg', 'placeholder' => 'Email*', 'value' => set_value('Email', ''))); ?>
                                            <?php echo form_error('Email', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();" >Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<script>
    $(document).ready(function() {
        $('#forgot_pass').validate({
            rules: {
                "Email": {
                    "required": true,
                    "email": true
                }
            },
            messages: {
                "Email": {
                    required: "Please enter email.",
                    email: "Please enter valid email."
                }
            }
        });
    });
</script>