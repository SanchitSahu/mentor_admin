<style>
    .custom-error{
        color: #b94a48;
        display: inline;
        font-weight: 400;
        margin: 5px 0;
    }	
</style>

<?php 
$session = $this->session->userdata;
$sess = $session['admin_data'];         
?>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>My Profile</strong>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form enctype="multipart/form-data" class="cmxform form-horizontal" class="cmxform form-horizontal" id="empForm" method="post">
                                <div class="form-group ">
                                    <label for="FirstName" class="control-label col-lg-3">Username</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('id' => "FirstName", 'name' => 'FirstName', 'class' => 'form-control', 'value' => set_value('FirstName', $product['FirstName'] . " " . $product['LastName']), 'placeholder' => 'Enter User Name', 'autofocus')) ?>
                                        <?php echo form_error('FirstName', '<label id="fErr" class="error">', '</label>'); ?>
                                    </div>
                                </div>
								<?php  if($sess['UserType']	==	"Admin") { ?>				
									<div class="form-group ">
										<label for="FirstName" class="control-label col-lg-3">Givenname</label>
										<div class="col-lg-6">
											<?php echo form_input(array('id' => "GivenName", 'name' => 'GivenName', 'class' => 'form-control', 'value' => set_value('GivenName', $product['GivenName']), 'placeholder' => 'Enter Given Name', 'autofocus')) ?>
											<?php echo form_error('GivenName', '<label id="fErr" class="error">', '</label>'); ?>
										</div>
									</div>
	
					
									<?php /*<div class="form-group ">
										<label for="Department_name" class="control-label col-lg-3">Department Name</label>
										<div class="col-lg-6">
											<?php echo form_input(array('id' => "Department_name", 'name' => 'Department_name', 'class' => 'form-control', 'value' => set_value('Department_name', $product['DepartmentName'] . " " . $product['LastName']), 'placeholder' => 'Enter Department Name', 'autofocus')) ?>
											<?php echo form_error('Department_name', '<label id="fErr" class="error">', '</label>'); ?>
											<span id="fErr" class="custom-error"></span>
										</div>
									</div>*/?>
								<?php } ?>	
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Photo</label>
                                    <div class="col-md-9">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 200px;">
                                                <?php
                                                if ($product['PictureURL'] == "") {
                                                    $imageURL = $this->config->item('base_images') . "default.png";
                                                } else {
                                                    $imageURL = $this->config->item('base_images') . "/admin/" . $product['PictureURL'];
                                                }
                                                ?>
                                                <img id="previewImg" src="<?php echo $imageURL; ?>" alt="" />
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-white btn-file">
                                                    <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                    <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                    <input type="file" id="photo" class="default" name="photo" />

                                                </span>
                                                                                                <!--<span class="imgErr custom-error" style="display:none;"></span>-->
                                            </div>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                                        </div>                                    
                                    </div>
                                </div>
                                <?php /* <div class="form-group">
                                  <div class="form-group radios">
                                  <label for="Gender" class="control-label col-lg-3">Gender</label>
                                  <label class="label_radio col-lg-1" for="radio-01">
                                  <input name="Gender" id="radio-01" value="Male" type="radio" checked  /> Male
                                  </label>
                                  <label class="label_radio col-lg-2" for="radio-02">
                                  <input name="Gender" id="radio-02" value="Female" type="radio" /> Female
                                  </label>
                                  </div>
                                  </div> */ ?>

                                <div class="form-group ">
                                    <label for="EmailAddress" class="control-label col-lg-3">Email Address</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('id' => "EmailAddress", 'name' => 'EmailAddress', 'class' => 'form-control', 'value' => set_value('EmailAddress', $product['Email']), 'placeholder' => 'Enter Email Address', 'autofocus')) ?>
                                        <?php echo form_error('EmailAddress', '<label id="eErr" class="error">', '</label>'); ?>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="Address" class="control-label col-lg-3">Address</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('id' => "Address", 'name' => 'Address', 'class' => 'form-control', 'value' => set_value('Address', $product['Address']), 'placeholder' => 'Enter Address', 'autofocus')) ?>
                                        <?php echo form_error('Address', '<label id="aErr" class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="City" class="control-label col-lg-3">City</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('id' => "City", 'name' => 'City', 'class' => 'form-control', 'value' => set_value('City', $product['City']), 'placeholder' => 'Enter City', 'autofocus')) ?>
                                        <?php echo form_error('City', '<label id="cErr" class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="PostCode" class="control-label col-lg-3" maxlength="13">Postal/Zip Code</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('id' => "PostCode", 'name' => 'PostCode', 'class' => 'form-control', 'value' => set_value('PostCode', $product['PostCode']), 'placeholder' => 'Enter Post Code', 'autofocus')) ?>
                                        <?php echo form_error('PostCode', '<label id="pErr" class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Cellphone" class="control-label col-lg-3" maxlength="13">Cell Phone</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('id' => "Cellphone", 'name' => 'Cellphone', 'class' => 'form-control', 'value' => set_value('Cellphone', $product['Cellphone']), 'placeholder' => 'Enter Cell Phone', 'autofocus')) ?>
                                        <?php echo form_error('Cellphone', '<label id="cellErr" class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="PhoneNumber" class="control-label col-lg-3" maxlength="13">Work Phone</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('id' => "PhoneNumber", 'name' => 'PhoneNumber', 'class' => 'form-control', 'value' => set_value('PhoneNumber', $product['PhoneNumber']), 'placeholder' => 'Enter Work Phone', 'autofocus')) ?>
                                        <?php echo form_error('PhoneNumber', '<label id="phoneErr" class="error">', '</label>'); ?>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="Country" class="control-label col-lg-3" maxlength="13">Country</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('id' => "Country", 'name' => 'Country', 'class' => 'form-control', 'value' => set_value('Country', $product['Country']), 'placeholder' => 'Enter Country Name', 'autofocus')) ?>
                                        <?php echo form_error('Country', '<label id="countryErr" class="error">', '</label>'); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-group radios">
                                        <label for="sMSCell" class="control-label col-lg-3">Where do you want to receive text messages?</label>
										<div class="col-lg-6">
											<input type="checkbox" class="filled-in" name="sMSCell[]" id="checkbox-01" value="cell" <?php echo ($product['SMS'] == 'cell' || $product['SMS'] == 'both') ? 'checked' : '' ; ?> <?php echo ($product['SMS'] == 'cell' || $product['SMS'] == 'both') ? '' : 'disabled'; ?>;">
											<label for="checkbox-01">CellPhone</label>
											<input style="margin-left: 40px;" type="checkbox" class="filled-in" name="sMSCell[]" id="checkbox-02" value="work" <?php echo ($product['SMS'] == 'work' || $product['SMS'] == 'both') ? 'checked' : '' ; ?> <?php echo ($product['SMS'] == 'work' || $product['SMS'] == 'both') ? '' : 'disabled'; ?>;">
											<label for="checkbox-02">WorkPhone</label>
										</div>
                                    </div>
                                </div>

                                <input class=" form-control" id="email" name="email" type="hidden"  value="<?php echo $product['Email']; ?>" />
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="return successalertify();" >Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>


<script type="text/javascript">
    $(document).ready(function () {
		if($('#Cellphone').val() == '') {
			$('#checkbox-01').attr('checked',false);
			$('#checkbox-01').attr('disabled',true);
		}
		
		if($('#PhoneNumber').val() == '') {
			$('#checkbox-02').attr('checked',false);
			$('#checkbox-02').attr('disabled',true);
		}
			
		$('#Cellphone').keyup(function() {
			if($(this).val() == '') {
				$('#checkbox-01').attr('checked',false);
				$('#checkbox-01').attr('disabled',true);
			} else if($(this).val() != '') {
				$('#checkbox-01').attr('disabled',false);
			}
		});
		
		$('#PhoneNumber').keyup(function() {
			if($(this).val() == '') {
				$('#checkbox-02').attr('checked',false);
				$('#checkbox-02').attr('disabled',true);
			} else if($(this).val() != '') {
				$('#checkbox-02').attr('disabled',false);
			}
		});
		
		
		$("#EmailAddress").keyup(function () {
			var email = $("#EmailAddress").val();
			var currentMail = $("#email").val();
	
			if (email != "") {
				$.ajax({
					url: '<?php echo base_url(); ?>admin/employee/checkemail',
					data: "email=" + email + "&currentMail=" + currentMail,
					success: function (result) {
						if (result != "") {
							alertify.log(result);
							$("#EmailAddress").val(currentMail);
						}
	
					}
				});
			}
		});
		
        $('#photo').change(function () {
            //alert("here");
            var val = $(this).val().toLowerCase();
            var regex = new RegExp("(.*?)\.(jpg|jpeg|txt|png|docx|gif|doc|pdf|xml|bmp|ppt|xls)$");

            if (!(regex.test(val))) {
                $(this).val('');
                $(".imgErr").html("Unsupported file");
                $(".imgErr").css("display", "block");
                //alert('Unsupported file');
            } else {
                var _URL = window.URL || window.webkitURL;
                var file, img;
                if ((file = this.files[0])) {
                    img = new Image();
                    img.onload = function () {
                        var width = this.width;
                        var height = this.height;
                        if (height != width) {
                            setTimeout(function () {
                                $('.close.fileupload-exists').trigger('click');
                            }, 10);
                            alertify.log("Upload only square image");
                            return false;
                        }
                    };
                    img.src = _URL.createObjectURL(file);
                    return false;
                }
            }
        });
		
		
		$(document).ready(function () {
        // validate signup form on keyup and submit
			$("#empForm").validate({
				rules: {
					FirstName: {
						required : true
					},
					GivenName: {
						required: true
					},
					EmailAddress: {
						required: true
					}
				},
				messages: {
					FirstName: {
						required : "Please enter UserName"
					},
					GivenName: {
						required : "Please enter GivenName"
					},
					EmailAddress: {
						required : "Please enter Email Address"
					}
				}
			});
		});
    });
	
    function successalertify() {
        //  alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
    }
    
	function erroralertify() {
        // alertify.error("There has been some Error occured.");
    }
    
	function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'admin/dashboard' ?>";
        }, 1000);
    }
</script>
