<?php //print_r($topic);exit;
if($topic['Weight'] == "")
	$topic['Weight']	=	1000;
else
	$topic['Weight']	=	$topic['Weight'];

?>
<section id="container" >
    <!--header start-->
    <?php //echo $this->load->view('admin/includes/header'); ?>
    <!--header end-->
    <?php $res['config']	=	$this->settings_model->getConfig();
	//echo $this->load->view('includes/left_menu',$res); ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong>Manage Sub Admin</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                                <!--<a href="javascript:;" class="fa fa-times"></a>-->
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'superadmin/managesubadmin/add' : 'superadmin/managesubadmin/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_topic_form", 'method' => "post"));
                                ?>
                                <div class="form-group ">
                                    <label for="TopicDescription" class="control-label col-lg-3">Sub Admin</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Topic Description", 'name' => 'subadmin', 'value' => $topic['TopicDescription'])); ?>
                                        <?php echo form_error('TopicDescription'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-3">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight", 'name' => 'Weight', 'value' => $topic['Weight'])); ?>
                                        <?php echo form_error('Weight'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
     function successalertify() {
     //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
     }
     function erroralertify() {
      //   alertify.error("There has been some Error occured.");
     }
     function logalertify() {
     	alertify.log("User has cancelled event");
        setTimeout(function () {
	        window.location.href = "<?php echo $this->config->item('base_url').'admin/topic'?>";
        },1000);
     }


    $().ready(function() {
        // validate signup form on keyup and submit
        $("#add_topic_form").validate({
            rules: {
                TopicDescription: {
                    required: true,
                    minlength: 2
                }
            },
            messages: {
                TopicDescription: {
                    required: "Please enter topic description",
                    minlength: "Your topic description must consist of at least {0} characters"
                }
            }
        });
        
    });
</script>