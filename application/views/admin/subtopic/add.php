<?php
if ($subtopic['Weight'] == "")
    $subtopic['Weight'] = 1000;
else
    $subtopic['Weight'] = $subtopic['Weight'];
?>
<section id="container" >
    <?php $res['config'] = $this->settings_model->getConfig(); ?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong>Sub Topics</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'admin/subtopic/add' : 'admin/subtopic/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_subtopic_form", 'method' => "post"));
                                ?>
                                <div class="form-group ">
                                    <label for="TopicID" class="control-label col-lg-3">Topic</label>
                                    <div class="col-lg-6">
                                        <?php echo form_dropdown('TopicID', $topicData, set_value('TopicID', $subtopic['TopicID']), 'class="form-control" id="TopicID"'); ?>
                                        <?php echo form_error('TopicID', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="SubTopicDescription" class="control-label col-lg-3">Subtopic Description</label>
                                    <div class="col-lg-6">
                                        <input type="hidden" id="SubTopicID" name="SubTopicID" value="<?php echo set_value('SubTopicID', $subtopic['SubTopicID']); ?>" />
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Subtopic Description", 'name' => 'SubTopicDescription', 'value' => $subtopic['SubTopicDescription'])); ?>
                                        <?php echo form_error('SubTopicDescription'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-3">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight", 'name' => 'Weight', 'value' => $subtopic['Weight'])); ?>
                                        <?php echo form_error('Weight'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
    function successalertify() {
        //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
    }
    function erroralertify() {
        //   alertify.error("There has been some Error occured.");
    }
    function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'admin/subtopic' ?>";
        }, 1000);
    }


    $().ready(function () {
        // validate signup form on keyup and submit
        $("#add_subtopic_form").validate({
            rules: {
                TopicID: {
                    required: true
                },
                SubTopicDescription: {
                    required: true,
                    remote: {
                        url: "<?php echo $this->config->item('base_url'); ?>admin/subtopic/checkSubtopicExist",
                        type: "post",
                        data: {
                            SubTopicID: function () {
                                return $('#SubTopicID').val();
                            },
                            TopicID: function () {
                                return $('#TopicID').val();
                            }
                        }
                    }
                }
            },
            messages: {
                TopicID: {
                    required: "Please select topic"
                },
                SubTopicDescription: {
                    required: "Please enter subtopic description",
                    remote: "Subtopic with same desciption already exists under selected Topic"
                }
            }
        });

    });
</script>