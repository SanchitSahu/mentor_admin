<?php
if ($meetingtype['Weight'] == "")
    $meetingtype['Weight'] = 1000;
?>
<section id="container" >
    <?php $res['config'] = $this->settings_model->getConfig(); ?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong>Meeting Types</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'admin/meetingtype/add' : 'admin/meetingtype/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_meetingtype_form", 'method' => "post"));
                                ?>
                                <div class="form-group ">
                                    <label for="MeetingTypeName" class="control-label col-lg-3">Meeting Type Name</label>
                                    <div class="col-lg-6">
                                        <input type="hidden" id="MeetingTypeID" name="MeetingTypeID" value="<?php echo set_value('MeetingTypeID', $meetingtype['MeetingTypeID']); ?>" />
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Meeting Type Name", 'name' => 'MeetingTypeName', 'value' => $meetingtype['MeetingTypeName'])); ?>
                                        <?php echo form_error('MeetingTypeName'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-3">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight", 'name' => 'Weight', 'value' => $meetingtype['Weight'])); ?>
                                        <?php echo form_error('Weight'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
    function successalertify() {
        //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
    }
    function erroralertify() {
        //   alertify.error("Some Error has occured.");
    }
    function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'admin/meetingtype' ?>";
        }, 1000);
    }


    $().ready(function () {
        // validate signup form on keyup and submit
        $("#add_meetingtype_form").validate({
            rules: {
                MeetingTypeName: {
                    required: true,
                    remote: {
                        url: "<?php echo $this->config->item('base_url'); ?>admin/meetingtype/checkMeetingTypeExist",
                        type: "post",
                        data: {
                            MeetingTypeID: function () {
                                return $('#MeetingTypeID').val();
                            }
                        }
                    }
                }
            },
            messages: {
                MeetingTypeName: {
                    required: "Please enter meeting type name",
                    remote: "Meeting Type with same name already exists"
                }
            }
        });

    });
</script>
