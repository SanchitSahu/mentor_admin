<?php
if ($comments['Weight'] == "")
    $comments['Weight'] = 1000;
else
    $comments['Weight'] = $comments['Weight'];
?>

<section id="container" >
    <?php $res['config'] = $this->settings_model->getConfig(); ?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong><?php echo ($this->uri->segment(4) == "") ? "Add " : "Edit "; ?>Standard Response When Declining Invitations</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'admin/comments/addReject' : 'admin/comments/editReject/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_decline_form", 'method' => "post"));
                                ?>
                                <div class="form-group ">
                                    <label for="ResponseName" class="control-label col-lg-3">Response Name</label>
                                    <div class="col-lg-6">
                                        <input type="hidden" id="ResponseID" name="ResponseID" value="<?php echo set_value('ResponseID', $comments['ResponseID']); ?>" />
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Response Text", 'name' => 'ResponseName', 'value' => $comments['ResponseName'])); ?>
                                        <?php echo form_error('ResponseName'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-3">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight Name", 'name' => 'Weight', 'value' => $comments['Weight'])); ?>
                                        <?php echo form_error('Weight'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
    function successalertify() {
        //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
    }
    function erroralertify() {
        //   alertify.error("There has been some Error occured.");
    }
    function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'admin/comments/listAccept' ?>";
        }, 1000);
    }


    $().ready(function () {
        // validate signup form on keyup and submit
        $("#add_decline_form").validate({
            rules: {
                ResponseName: {
                    required: true,
                    remote: {
                        url: "<?php echo $this->config->item('base_url'); ?>admin/comments/checkResponseExists",
                        type: "post",
                        data: {
                            Response: 'decline',
                            ResponseID: function () {
                                return $('#ResponseID').val();
                            }
                        }
                    }
                },
            },
            messages: {
                ResponseName: {
                    required: "Please enter Response text",
                    remote: "Decline Response already exists"
                }
            }
        });

    });
</script>