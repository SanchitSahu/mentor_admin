<?php
if ($groups['Weight'] == "")
    $groups['Weight'] = 1000;
else
    $groups['Weight'] = $groups['Weight'];
?>

<link href="<?php echo $this->config->item('base_css'); ?>select2.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>select2.full.js"></script>

<section id="container" >
    <?php $res['config'] = $this->settings_model->getConfig(); ?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong><?php echo ($this->uri->segment(4) == "") ? 'Add' : 'Edit'; ?> Group</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'admin/groups/add' : 'admin/groups/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_group_form", 'method' => "post"));
                                ?>
                                <input type="hidden" id="GroupID" name="GroupID" value="<?php echo set_value('GroupID', $groups['GroupID']); ?>" />
                                <div class="form-group ">
                                    <label for="GroupName" class="control-label col-lg-4">Group Name</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Group Name", 'name' => 'GroupName', 'value' => $groups['GroupName'])); ?>
                                        <?php echo form_error('GroupName'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="GroupMentors" class="control-label col-lg-4">Click here to select <?php echo $config[0]->mentorName; ?>s from this dropdown list</label>
                                    <div class="col-lg-6">
                                        <?php echo form_dropdown('GroupMentors[]', $groups['MentorsList'], set_value('GroupMentors', $groups['GroupMentorsID']), 'class="form-control group-mentors-multiple" id="GroupMentors" multiple="multiple" style="width: 100%"'); ?>
                                        <?php echo form_error('GroupMentors', '<label class="error">', '</label>'); ?>

                                        <?php //echo form_input(array('class' => "form-control", 'placeholder' => "Select Group Mentors", 'name' => 'GroupMentors', 'value' => implode(', ', $groups['GroupMentors']))); ?>
                                        <?php //echo form_error('GroupMentors'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="GroupMentees" class="control-label col-lg-4">Click here to select <?php echo $config[0]->menteeName; ?>s from this dropdown list</label>
                                    <div class="col-lg-6">
                                        <?php echo form_dropdown('GroupMentees[]', $groups['MenteesList'], set_value('GroupMentees', $groups['GroupMenteesID']), 'class="form-control group-mentees-multiple" id="GroupMentees" multiple="multiple" style="width: 100%"'); ?>
                                        <?php echo form_error('GroupMentees', '<label class="error">', '</label>'); ?>

                                        <?php //echo form_input(array('class' => "form-control", 'placeholder' => "Select Group Mentees", 'name' => 'GroupMentees', 'value' => implode(', ', $groups['GroupMentees']))); ?>
                                        <?php //echo form_error('GroupMentees'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-4">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight Name", 'name' => 'Weight', 'value' => $groups['Weight'])); ?>
                                        <?php echo form_error('Weight'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
    function successalertify() {
        //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
    }

    function erroralertify() {
        //   alertify.error("There has been some Error occured.");
    }
    function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'admin/groups' ?>";
        }, 1000);
    }

    $(document).ready(function () {
        $(".group-mentors-multiple").select2({
            placeholder: "Select <?php echo $config[0]->mentorName; ?>s to Add in Group",
            allowClear: true
        });

        $(".group-mentees-multiple").select2({
            placeholder: "Select <?php echo $config[0]->menteeName; ?>s to Add in Group",
            allowClear: true
        });
    });
</script>