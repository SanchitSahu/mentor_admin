<script language="JavaScript" type="text/javascript">
    function checkDelete(val) {
        var loc = $('.delete-' + val).attr('href');
        alertify.confirm("Confirm Deletion?", function (e) {
            if (e) {
                alertify.success("Record has been deleted successfully.");
                document.location.href = loc;
            } else {
                alertify.log("User has cancelled event");
            }
        });
        //prevent link from being followed immediately on click
        return false;
    }
</script>

<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>Groups</strong>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <!--<a href="javascript:;" class="fa fa-times"></a>-->
                        </span>
                    </header>
                    <div class="panel-body">
                        <a href="<?php echo $this->config->item('base_url') . 'admin/groups/add'; ?>" class="btn btn-round btn-success" style="float:left; margin-right:15px;"><i class="livicon" data-s="16" data-n="plus-alt" data-c="#fff" data-hc="0" id="I15" style="width: 16px; height: 16px;"></i> Add</a>
                        <!--<a href="" class="btn btn-round btn-danger" onclick="deleteconfirm();"><i class="livicon" data-s="16" data-n="trash" data-c="#fff" data-hc="0" id="I14" style="width: 16px; height: 16px;"></i> Delete</a>-->

						<?php /*
                        <?php echo form_open_multipart('admin/bulkupload/genericUploader'); ?>
                        <div class="input-field col s12">

                            <div class="input-field col s12">
                                <div class="file-field input-field" style="float: left; background:#88a755; padding: 0px 20px; border-radius: 20px;">
                                    <span>Bulk Provision</span>
                                    <div class="btn" style="margin:0px 15px; background:none; border:none; box-shadow:none;">
                                        <input type="file" name="userfile" id="userfile" />
                                    </div>
                                    <button value="upload" class="waves-effect waves-light btn-large center" type="submit">Submit</button>
                                </div>
                                <div class="input-field col s12">
                                    <span class="errCalss col s12" id="imgErr"><?php echo $error; ?></span>
                                </div>

                            </div>
                            <input type="hidden" name="tableName" value="mentor_groups" id="tsbleName" />
                            <input type="hidden" name="urlPath" value="groups" />

                        </div>
                        <?php echo form_close(); ?>
                        
                        <?php
						$filename = "Abbreviations.csv";
						$chkFileExist = checkSampleFileExists($filename);
						if($chkFileExist === true) { ?>
							<a href="../../../mentor/assets/admin/csv/<?php echo $filename; ?>" target="_blank"  class="btn btn-round btn-success" style="float:right;"><i class="mdi-content-add prefix"></i>Download Sample File</a>
						<?php } else { ?>
							<a href="#" class="btn btn-round btn-success" onclick='alertify.error("<?php echo $chkFileExist; ?>");' style="float:right;"><i class="mdi-content-add prefix"></i>Download Sample File</a>
						<?php } ?>
						*/ ?>

                        <div class="adv-table">
                            <!--<label class='download_label'>All sample .csv files start with a header line naming the fields to be uploaded followed by one or more data lines. A semicolon (;) separates field headings and field values on a given line. Double quotes (") surround headings and values containing special characters, such as semicolons. If the line starts with a semicolon, it means that the row starts with an auto-increment field.</label>-->
                            <?php echo $this->table->generate(); ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!-- Init variables for DATATABLE.  Note: Please put Datatable js files at footer  -->
<script>
    var dattTableName = 'dynamic_table';
    var sAjaxSource = '<?php echo base_url(); ?>admin/groups/datatable';
    var baseUrl = '<?php echo base_url(); ?>';
</script>
<!-- Init variables for DATATABLE end -->