<?php
//print_r($clientSource);exit;
if (!isset($clientProgramme['Weight'])) {
    $clientProgramme['Weight'] = 1000;
} else {
    if ($clientProgramme['Weight'] == "")
        $clientProgramme['Weight'] = 1000;
    else
        $clientProgramme['Weight'] = $clientProgramme['Weight'];
}
?>

<section id="container" >
    <!--header start-->
    <?php //echo $this->load->view('admin/includes/header'); ?>
    <!--header end-->
    <?php $res['config'] = $this->settings_model->getConfig();
    //echo $this->load->view('includes/left_menu', $res);
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong>Skills Needed</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                                <!--<a href="javascript:;" class="fa fa-times"></a>-->
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'admin/menteeSkillNeeded/add' : 'admin/menteeSkillNeeded/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_skill_form", 'method' => "post"));
                                ?>
                                <div class="form-group ">
                                    <label for="ProgrammeName" class="control-label col-lg-3"><?php echo $res['config'][0]->menteeName; ?> Name</label>
                                    <div class="col-lg-6">
                                        <?php if (isset($clientskillneeded[0]['MenteeName'])) {
                                            if ($clientskillneeded[0]['MenteeName'] != "") {
                                                ?>	
                                                <label for="ProgrammeName" class="control-label col-lg-3"><?php echo $clientskillneeded[0]['MenteeName']; ?></label>  
												<input type="hidden" name="mentee" value="<?php echo $clientskillneeded[0]['MenteeName']; ?>" >			
                                                <?php }
                                            } else { ?>	
                                            <select name="mentee" id="mentee">
											<option value="0">Select <?php echo $res['config'][0]->menteeName;?></option>
                                            <?php foreach ($mentee as $men) { ?>
                                                    <option value="<?php echo $men['MenteeID']; ?>"><?php echo $men['MenteeName']; ?></option>
											<?php } ?>
                                            </select>
										<?php } ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-3">Skill Sets</label>
                                    <div class="col-lg-6" id="list">
                                        <?php if (isset($clientskillneeded[0]['MenteeName'])) {
                                            if ($clientskillneeded[0]['MenteeName'] != "") {
                                                ?>
                                                <?php $userskills = array(); ?>				
                                                <?php
                                                foreach ($clientskillneeded as $csn) {
                                                    $userskills[] = $csn['SkillID'];
                                                }
                                                ?>
                                                <?php foreach ($skills as $skill) { ?>
                                                    <?php if (in_array($skill['SkillID'], $userskills)) { ?>		
                                                        <input type="checkbox" checked class="control-form" name="skills[]" value="<?php echo $skill['SkillID']; ?>" /><?php echo $skill['SkillName']; ?><br><br>
													<?php } else { ?>
                                                        <input type="checkbox" class="control-form" name="skills[]" value="<?php echo $skill['SkillID']; ?>" /><?php echo $skill['SkillName']; ?><br><br>
												<?php }
											} ?>
										<?php }
										} else { ?>											
											<?php foreach ($skills as $skill) { ?>                                                                                                      											
                                                    <input type="checkbox" class="control-form" name="skills[]" value="<?php echo $skill['SkillID']; ?>" /><?php echo $skill['SkillName']; ?><br><br>
											<?php } ?>												
										<?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
								<?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
    function successalertify() {
		if($('select').val() == 0){
			alertify.error("Select <?php echo $res['config'][0]->menteeName;?>");
			window.location.href = "<?php echo $this->config->item('base_url') . 'admin/menteeSkillNeeded/add' ?>";
		} else if($('input[type="checkbox"]:checked').length> 0){
			alertify.success("Please wait....");
		} else if($('input[type="checkbox"]:checked').length == 0) {
			alertify.error("Select atleast one skill");
			window.location.href = "<?php echo $this->config->item('base_url') . 'admin/menteeSkillNeeded/add' ?>";
		}
	}
	
    function erroralertify() {
        //   alertify.error("There has been some Error occured.");
    }
    function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'admin/menteeSkillNeeded' ?>";
        }, 1000);
    }

	$("#mentee").on("change",function(){
		varMentorID = $(this).val();
		
		 jQuery.ajax({
			type: "POST",
			url: "<?php echo $this->config->item('base_url'); ?>admin/menteeSkillNeeded/ajaxGetMenteeSkills",
			data: {id:varMentorID},
			success: function (output) {
				if (output !== "") {
					$('input[type="checkbox"]').prop("checked", false);
					var values = output.split(',');
					$("#list").find('[value=' + values.join('], [value=') + ']').prop("checked", true);
				}else{
					$('input[type="checkbox"]').prop("checked", false);
				} 
			}
		});
		
	})
</script>