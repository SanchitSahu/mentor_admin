<?php
$userSession = $this->session->userdata('admin_data');
$superuserSession = $this->session->userdata('superadmin');
$this->load->model('admin/settings_model');
$config	=	$this->settings_model->getConfig();
?>
<header class="header fixed-top clearfix">
    <!--logo start-->
    <div class="brand" style="height: inherit; background:<?php echo $this->session->userdata('headerColor') ?>">

        <a href="<?php echo base_url(); ?>admin/dashboard" title="Home" class="logo"i style="text-align: center; margin: 0; width: 240px;">
            <img src="<?php echo $this->config->item('base_images'); ?><?php echo $this->session->userdata('logo') ?>" alt="" style="margin: 5px 20px; height: 64px;" />
        </a>
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars" style="color: <?php echo $this->session->userdata('headerColor') ?>;"></div>
        </div>
    </div>
    <!--logo end-->
    <div class="top-nav clearfix subadminName">
        <?php if ($userSession['UserType'] == "Admin") { ?>
            <h1 style="text-align: center;"><?php echo ($config[0]->DepartmentName != "") ? $config[0]->DepartmentName : ucwords($userSession['subdomain_name']); ?></h1>
        <?php } ?>
        <!--search & user info start-->
        <ul class="nav pull-right top-menu">       
            <!-- user login dropdown start-->
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <?php if ($userSession['profilePicture'] != "") { ?>
                        <img alt="" src="<?php echo $this->config->item('base_images'); ?>admin/<?php echo $userSession['profilePicture']; ?>" />
                    <?php } else { ?>
                        <img alt="" src="<?php echo $this->config->item('base_images'); ?>default.jpg" />
                    <?php } ?>
                    <span class="username">
                        <?php
                        //$userSession = $this->session->userdata('admin_data');
                        if (isset($userSession) && !empty($userSession)) {
                            echo ucwords($userSession['name']);
                        } else {
                            echo "Anonymous User";
                        }
                        ?>
                    </span>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu extended logout"> 
                    <?php if ($userSession['UserType'] == "Vendor") { ?>
                        <li><a href="<?php echo $this->config->item('base_url') . 'admin/vendors/profile' ?>"><i class=" fa fa-suitcase"></i>Profile</a></li>
                    <?php } else { ?>
                        <li><a href="<?php echo $this->config->item('base_url') . 'admin/employee/profile' ?>"><i class=" fa fa-suitcase"></i>Profile</a></li>
                    <?php } ?>

                    <?php //if(empty($superuserSession)) {  ?>
                    <li><a href="<?php echo $this->config->item('base_url') . 'admin/employee/changepassword' ?>"><i class="fa fa-cog"></i> Password Change</a></li>
                    <?php //}  ?>
                    <?php if (!empty($superuserSession)) { ?>
                        <li><a href="<?php echo $this->config->item('base_url') . 'admin/logout_admin' ?>"><i class="fa fa-key"></i> Go back to Superadmin</a></li>
                    <?php } else { ?>
                        <li><a href="<?php echo $this->config->item('base_url') . 'admin/logout' ?>"><i class="fa fa-key"></i> Log Out</a></li>
                        <?php } ?>
                </ul>
            </li>
            <!-- user login dropdown end -->

        </ul>
        <!--search & user info end-->
    </div>
</header>
