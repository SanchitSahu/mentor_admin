<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />
        <meta name="author" content="ThemeBucket" />
        <?php echo $this->template->meta; ?>
        <?php echo $this->template->stylesheet; ?>
        <title><?php echo $this->template->title->default("MELS"); ?></title>
        <!--Core CSS -->
        <link href="<?php echo $this->config->item('base_assets'); ?>bs3/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo $this->config->item('base_css'); ?>bootstrap-reset.css" rel="stylesheet" />
        <link href="<?php echo $this->config->item('base_fonts'); ?>css/font-awesome.css" rel="stylesheet" />
		<link href="<?php echo $this->config->item('base_css'); ?>jquery.fancybox.css" rel="Stylesheet" />
		<?php 
		//	$this->load->model('admin/settings_model');
			$res['config']	=	$this->settings_model->getConfig();
			session_start();
			
			$errorLog	=	$this->settings_model->errorLog();
	
			$this->session->set_userdata('headerColor', $res['config'][0]->headerColor);
			$this->session->set_userdata('leftBarColor', $res['config'][0]->leftBarColor);
			$this->session->set_userdata('logo', $res['config'][0]->logo);
			$this->session->set_userdata('buttonColor', $res['config'][0]->buttonColor);
			$this->session->set_userdata('fontColor', $res['config'][0]->fontColor);
			$this->session->set_userdata('backgroundColor', $res['config'][0]->backgroundColor);
			$this->session->set_userdata('highlightColor', $res['config'][0]->highlightColor);
			$this->session->set_userdata('leftMenuFont', $res['config'][0]->leftMenuFont);
			$this->session->set_userdata('tableFont', $res['config'][0]->tableFont);
			$this->session->set_userdata('tableBackground', $res['config'][0]->tableBackground);
			$this->session->set_userdata('icon', $res['config'][0]->icon);
				
			$_SESSION['buttonColor']	=	$res['config'][0]->buttonColor;
			$_SESSION['headerColor']	=	$res['config'][0]->headerColor;
			$_SESSION['leftBarColor']	=	$res['config'][0]->leftBarColor;
			$_SESSION['logo']	=	$res['config'][0]->logo;
			$_SESSION['fontColor']	=	$res['config'][0]->fontColor;
			$_SESSION['backgroundColor']	=	$res['config'][0]->backgroundColor;
			$_SESSION['highlightColor']	=	$res['config'][0]->highlightColor;
			$_SESSION['leftMenuFont']	=	$res['config'][0]->leftMenuFont;
			$_SESSION['tableFont']	=	$res['config'][0]->tableFont;
			$_SESSION['tableBackground']	=	$res['config'][0]->tableBackground;
			$_SESSION['icon']	=	$res['config'][0]->icon;
			//print_r($res['config']);exit;
		?>

        <!-- Custom styles for this template -->
        <?php /*<link href="<?php echo $this->config->item('base_css'); ?>style.php" type="text/css" rel="stylesheet" />*/?>
		<link rel="shortcut icon harsh" type="image/jpg" href="<?php echo $this->config->item('base_images'); ?><?php echo $res['config'][0]->icon;?>"; />
		
		<style>
		<?php include_once $_SERVER['DOCUMENT_ROOT']."/mentor/assets/admin/css/style.php"; ?>
		</style>
		
        <link href="<?php echo $this->config->item('base_css'); ?>style-responsive.css" rel="stylesheet" />
        <link href="<?php echo $this->config->item('base_css'); ?>livicons-help.css" rel="Stylesheet" />
        <link href="<?php echo $this->config->item('base_css'); ?>prettify.css" rel="Stylesheet" />
        <?php /*<link href="<?php echo $this->config->item('base_css'); ?>3d_buttons.php" rel="Stylesheet" />*/?>
		<style>
		<?php include_once $_SERVER['DOCUMENT_ROOT']."/mentor/assets/admin/css/3d_buttons.php"; ?>
		</style>
        <link href="<?php echo $this->config->item('base_css'); ?>alertify.core.css" rel="Stylesheet" />
        <link href="<?php echo $this->config->item('base_css'); ?>alertify.default.css" rel="Stylesheet" />  
		
		 <script type="text/javascript">    
			window.history.forward();
			function noBack() { 
			  window.history.forward(); 
			}
        </script>
        
        <link href="<?php echo $this->config->item('base_js'); ?>bootstrap-datepicker/css/datepicker.css" rel="Stylesheet" />
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.js"></script>
		<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery-migrate-1.2.1.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.validate.min.js"></script>
		<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>additional-methods.js"></script>
       <!-- <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>-->
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>locationpicker.jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]>
        <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
		
		
		<script>
			$(document).ready(function(){
				setTimeout(function(){
					$('.dcjq-icon').each(function(){
						$(this).addClass('fa').addClass('fa-chevron-right').removeClass('dcjq-icon');
					});
					$('.sidebar-menu li a').click(function(){
						//if($(this).children('a.dcjq-parent:first').length > 0){
						//}
						if($(this).parent().parent().hasClass('sub') == false) {
							$('.sidebar-menu li a span').addClass('fa').addClass('fa-chevron-right').removeClass('fa-chevron-down');
						} else {
							$('.sidebar-menu .sub li a span').addClass('fa').addClass('fa-chevron-right').removeClass('fa-chevron-down');
						}
						$(this).find('span').removeClass('fa').removeClass('dcjq-icon').removeClass('fa-chevron-right').removeClass('fa-chevron-down');
						if($(this).hasClass('active') == true){
							$(this).find('span').addClass('fa').addClass('fa-chevron-down');
						} else {
							$(this).find('span').addClass('fa').addClass('fa-chevron-right');
						}
					});
				},1000)
			})
		</script>
    </head>
	<?php $userSession = $this->session->userdata('admin_data');?>
    <body <?php if(isset($userSession['returnFromAdmin']) && !empty($userSession['returnFromAdmin'])) echo 'onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload=""';?>>
        <!-- Placed js at the end of the document so the pages load faster -->
        <section id="container" >
            <!--header start-->
                <?php echo $this->load->view('admin/includes/header',$res); ?>
            <!--header end--> 
            <!--sidebar start-->
                <?php echo $this->load->view('admin/includes/left',$res); ?>
            <!--sidebar end-->
            <!--main content start-->
                <?php echo $this->template->content; ?>
            <!--main content end-->
            <!--right sidebar start-->
                <?php //echo $this->load->view('admin/includes/right'); ?>
            <!--right sidebar end-->
			
				
        </section>
		
			<?php $this->load->view('admin/includes/footer'); ?>
        <!--Core js-->
        <?php echo $this->template->javascript; ?>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_assets'); ?>bs3/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.dcjqaccordion.2.7.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.scrollTo.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.nicescroll.js"></script>
        
        <!--common script init for all pages-->
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>/scripts.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>/morris-chart/raphael-min.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>/prettify.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>/raphael-min.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>/alertify.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>/livicons-1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.fancybox.pack.js"></script>
		<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>ckeditor/ckeditor.js"></script>
        <script type="text/javascript">
            
            <?php 
			$showDetail	=	"Show Details (".$errorLog.")";
			
			if($this->session->flashdata('error'))  { ?>
						if (window.location.href.split('/').pop() == 'newusers') {
							alertify.confirm("<?php echo $this->session->flashdata('error'); ?>", function (e) {
								
							});
						} else {
							alertify.set({labels: {
									ok: "OK",
									cancel: "<?php echo $showDetail; ?>"
								}});
							var href = "<?php echo base_url(); ?>admin/errors";
							alertify.confirm("<?php echo $this->session->flashdata('error'); ?>", function (e) {
								console.log(e);
								if (!e) {
									window.location.href = href;
								}
							});
						}
	
						//alertify.confirm("<?php echo $this->session->flashdata('error');?>");
						//alertify.alert("<?php echo $this->session->flashdata('error');?>");
				<?php } ?>
				
                
            <?php if($this->session->flashdata('success')) { ?>
                alertify.success("<?php echo $this->session->flashdata('success');?>");
            <?php } ?>            
            
            function deleteconfirm() {
                alertify.confirm("Do you want to Delete this Record(s)?", function(e) {
					console.log(e);return false;
                    if (e) {
                        alertify.success("Record has been deleted successfully.");
                    } else {
                        alertify.log("User has canceled the event.");
                    }
                });
            }
			 $(document).ready(function () {
                $('.fancybox').fancybox({
                    autoSize: true,
                    width: "auto",
                    height: "80%"
                });
                
                $(".close-box").click(function (){
                    $.fancybox.close();
                });
                
            });
        </script>
    </body>
</html>
