<?php
if ($userProgramme['Weight'] == "")
    $userProgramme['Weight'] = 1000;
else
    $userProgramme['Weight'] = $userProgramme['Weight'];
?>

<section id="container" >
    <?php $res['config'] = $this->settings_model->getConfig(); ?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong>Programme</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'admin/userProgramme/add' : 'admin/userProgramme/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_programme_form", 'method' => "post"));
                                ?>
                                <div class="form-group ">
                                    <label for="ProgrammeName" class="control-label col-lg-3">Programme Name</label>
                                    <div class="col-lg-6">
                                        <input type="hidden" id="ProgrammeID" name="ProgrammeID" value="<?php echo set_value('ProgrammeID', $userProgramme['ProgrammeID']); ?>" />
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Programme Name", 'name' => 'ProgrammeName', 'value' => $userProgramme['ProgrammeName'])); ?>
                                        <?php echo form_error('ProgrammeName'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-3">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight Name", 'name' => 'Weight', 'value' => $userProgramme['Weight'])); ?>
                                        <?php echo form_error('Weight'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
    function successalertify() {
        //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
    }
    function erroralertify() {
        //   alertify.error("There has been some Error occured.");
    }
    function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'admin/userProgramme' ?>";
        }, 1000);
    }


    $().ready(function () {
        // validate signup form on keyup and submit
        $("#add_programme_form").validate({
            rules: {
                ProgrammeName: {
                    required: true,
                    remote: {
                        url: "<?php echo $this->config->item('base_url'); ?>admin/userProgramme/checkProgrammeExist",
                        type: "post",
                        data: {
                            ProgrammeID: function () {
                                return $('#ProgrammeID').val();
                            }
                        }
                    }
                }
            },
            messages: {
                ProgrammeName: {
                    required: "Please enter Programme name",
                    remote: "Programme with same name already exists"
                }
            }
        });

    });
</script>