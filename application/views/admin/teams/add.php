<?php
if ($teams['Weight'] == "")
    $teams['Weight'] = 1000;
else
    $teams['Weight'] = $teams['Weight'];
?>

<link href="<?php echo $this->config->item('base_css'); ?>select2.css" rel="stylesheet" />
<link href="<?php echo $this->config->item('base_js'); ?>bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>select2.full.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>bootstrap-fileupload/bootstrap-fileupload.js"></script>

<section id="container" >
    <?php $res['config'] = $this->settings_model->getConfig(); ?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong><?php echo ($this->uri->segment(4) == "") ? 'Add' : 'Edit'; ?> Team</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'admin/teams/add' : 'admin/teams/edit/' . $this->uri->segment(4);
                                echo form_open_multipart($action, array('class' => 'cmxform form-horizontal', 'id' => "add_team_form", 'method' => "post", "onsubmit"=>"return submitForm()"));
                                ?>
                                <input type="hidden" id="TeamID" name="TeamID" value="<?php echo set_value('TeamID', $teams['TeamID']); ?>" />
                                <div class="form-group ">
                                    <label for="TeamName" class="control-label col-lg-4">Team Name</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter ".$config[0]->teamManager." Name", 'name' => 'TeamName','id'=> 'TeamName', 'value' => $teams['TeamName'])); ?>
                                        <?php echo form_error('TeamName', '<label class="error">', '</label>'); ?>
										<label id="TeamNameErr" class="error"></label>
                                    </div>
                                </div>
								<!--<div class="form-group ">
                                    <label for="OwnerType" class="control-label col-lg-4">Owner Type</label>
                                    <div class="col-lg-6">
										<?php if($teams['TeamOwnerUserType'] != ""){ ?>
                                        <input type="radio" class="control-form team-mentor-owner" name="ownerType" id="ownerType1" value="0"<?php echo($teams['TeamOwnerUserType']==0)? "checked='checked'" :""?> /><?php echo $config[0]->mentorName; ?>
										<input type="radio" class="control-form team-mentee-owner" name="ownerType" id="ownerType2" value="1"<?php echo($teams['TeamOwnerUserType']==1)? "checked='checked'" :""?> /><?php echo $config[0]->menteeName; ?>
										<?php }else {?>
										<input type="radio" class="control-form team-mentor-owner" name="ownerType" id="ownerType1" value="0" /><?php echo $config[0]->mentorName; ?>
										<input type="radio" class="control-form team-mentee-owner" name="ownerType" id="ownerType2" value="1" /><?php echo $config[0]->menteeName; ?>
										<?php }?>
										<br>
										<?php echo form_error('ownerType', '<label class="error">', '</label>'); ?>
										<label id="ownerTypeErr" class="error"></label>
                                    </div>
                                </div>-->
								<input type="hidden" name="ownerType" id="ownerType2" value="1" />
								<!--<?php if($teams['TeamOwnerUserType'] != "" && $teams['TeamOwnerUserType'] == 0) $style ="dispaly:block;";else $style= "display:none;"?>
								<div class="form-group mentorType" style="<?php echo $style ?>">
                                    <label for="Mentors" class="control-label col-lg-4"><?php echo $config[0]->mentorName; ?> Name</label>
                                    <div class="col-lg-6">
                                        <?php echo form_dropdown('Mentors', $teams['MentorsList'], set_value('Mentors', $teams['TeamOwnerUserID']), 'class="form-control mentor-owner-type" id="TeamOwnerUserType1"'); ?>
                                        <?php echo form_error('Mentors', '<label class="error">', '</label>'); ?>
										<label id="MentorsErr" class="error"></label>
                                    </div>
                                </div>-->
								<?php if($teams['TeamOwnerUserType'] != "" && $teams['TeamOwnerUserType'] == 1) $style ="dispaly:block;";else $style= "display:block;"?>
								<div class="form-group menteeType" style="<?php echo $style ?>">
                                    <label for="Mentees" class="control-label col-lg-4"><?php echo $config[0]->menteeName; ?> Name</label>
                                    <div class="col-lg-6">
                                        <?php echo form_dropdown('Mentees', $teams['MenteesList'], set_value('Mentees', $teams['TeamOwnerUserID']), 'class="form-control mentee-owner-type" id="TeamOwnerUserType2"'); ?>
                                        <?php echo form_error('Mentees', '<label class="error">', '</label>'); ?>
										<label id="MenteesErr" class="error"></label>	
                                    </div>
                                </div>

								
								<div class="form-group ">
                                    <label for="TeamName" class="control-label col-lg-4">Team Description</label>
                                    <div class="col-lg-6">
                                        <?php echo form_textarea(array('class' => "form-control", 'placeholder' => "Enter Team Description", 'name' => 'TeamDescription','id'=>'TeamDescription', 'value' => $teams['TeamDescription'],'rows'=> '5','cols'=> '10')); ?>
                                        <?php echo form_error('TeamDescription', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
								
								<div class="form-group">,
                                    <label class="control-label col-lg-4">Team Image</label>
                                    <div class="col-md-6">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 200px;">
                                                <?php
                                                if ($teams['TeamImage'] == "") {
                                                    $imageURL = "http://melstm.net/mentor/assets/admin/images/admin/group_avatar.png";
                                                } else {
                                                    $imageURL = "http://melstm.net/mentor/assets/admin/images/admin/" . $teams['TeamImage'];
                                                }
                                                ?>
                                                <img id="previewImg" src="<?php echo $imageURL; ?>" alt="" />
                                                <?php if(isset($teams['TeamImage'])) $imgVal = $teams['TeamImage'];else $imgVal = "";?>
                                                <input type="hidden" name="imgVal" value="<?php echo $imgVal?>">
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-white btn-file">
                                                    <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                    <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                    <input type="file" id="photo" class="default" name="photo" />
                                                </span>
                                            </div>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                                        </div>                                    
                                    </div>
                                </div>
								<div class="form-group ">
                                    <label for="MaxMembers" class="control-label col-lg-4">Max Members</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('type'=>'number','class' => "form-control", 'placeholder' => "Enter Max Members", 'name' => 'MaxMembers','id'=>'MaxMembers','min'=>0,'max'=>$config[0]->maxMember, 'value' => $teams['MaxMembers'])); ?>
                                        <?php echo form_error('MaxMembers', '<label class="error">', '</label>'); ?>
										<label id="MaxMembersErr" class="error"></label>
                                    </div>
                                </div>
								<div class="form-group" id="team_skillsset">
                                    <label for="Weight" class="control-label col-lg-4">Skill Sets</label>
                                    <div class="col-lg-6">
                                        		
                                                <?php foreach ($skills as $skill) { ?>
                                                    <?php if (in_array($skill['SkillID'], $teamSkill)) { 
?>		
                                                        <input type="checkbox" checked class="control-form" name="skills[]" value="<?php echo $skill['SkillID']; ?>" /><?php echo $skill['SkillName']; ?><br><br>
													<?php } else { ?>
                                                        <input type="checkbox" class="control-form" name="skills[]" value="<?php echo $skill['SkillID']; ?>" /><?php echo $skill['SkillName']; ?><br><br>
												<?php }
											} ?>
										<?php echo form_error('skills[]', '<label class="error">', '</label>'); ?>
										<label id="skillsErr" class="error"></label>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="TeamMentors" class="control-label col-lg-4">Click here to select <?php echo $config[0]->mentorName; ?>s from this dropdown list</label>
                                    <div class="col-lg-6">
                                        <?php echo form_dropdown('TeamMentors[]', $teams['MentorsListDrp'], set_value('TeamMentors', $teams['TeamMentorsID']), 'class="form-control team-mentors-multiple" id="TeamMentors" multiple="multiple" style="width: 100%"'); ?>
                                        <?php echo form_error('TeamMentors', '<label class="error">', '</label>'); ?>

                                        <?php //echo form_input(array('class' => "form-control", 'placeholder' => "Select Team Mentors", 'name' => 'TeamMentors', 'value' => implode(', ', $teams['TeamMentors']))); ?>
                                        <?php //echo form_error('TeamMentors'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="TeamMentees" class="control-label col-lg-4">Click here to select <?php echo $config[0]->menteeName; ?>s from this dropdown list</label>
                                    <div class="col-lg-6">
                                        <?php echo form_dropdown('TeamMentees[]', $teams['MenteesListDrp'], set_value('TeamMentees', $teams['TeamMenteesID']), 'class="form-control team-mentees-multiple" id="TeamMentees" multiple="multiple" style="width: 100%"'); ?>
                                        <?php echo form_error('TeamMentees', '<label class="error">', '</label>'); ?>

                                        <?php //echo form_input(array('class' => "form-control", 'placeholder' => "Select Team Mentees", 'name' => 'TeamMentees', 'value' => implode(', ', $teams['TeamMentees']))); ?>
                                        <?php //echo form_error('TeamMentees'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-4">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight Name", 'name' => 'Weight', 'value' => $teams['Weight'])); ?>
                                        <?php echo form_error('Weight', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
	function submitForm(){
		//var mentor = <?php echo $config[0]->mentorName; ?>;
		//var mentee = <?php echo $config[0]->menteeName; ?>;
		//var team1   = <?php echo $config[0]->teamManager; ?>;
		var TeamName = $("#TeamName").val();
		//var ownerType = $("input[name='ownerType']:checked").val();
		var ownerType = $("input[name='ownerType']").val();//alert(ownerType);
		
		var MaxMembers = $("#MaxMembers").val();
		var skills = $("input[name='skills[]']:checked").val();//alert("skill"+skills);
		var err= 0;	
		
		if(TeamName == ""){
			$("#TeamNameErr").text("<?php echo $config[0]->teamManager; ?> Name is required");
			err++;
		}else{
			$("#TeamNameErr").text("");
		}

		if(ownerType =="undefined" || ownerType == undefined){
			$("#ownerTypeErr").text("Owner Type is required");
			err++;
		}else{
			$("#ownerTypeErr").text("");
			if(ownerType == 0){		
				var TeamOwnerUserType1 = $("#TeamOwnerUserType1").val();	
				if(TeamOwnerUserType1 == ""){
					$("#MentorsErr").text("<?php echo $config[0]->mentorName; ?> Name is required");
					err++;
				}else{
					$("#MentorsErr").text("");
				}
			}else if(ownerType == 1){
				var TeamOwnerUserType2 = $("#TeamOwnerUserType2").val();
				if(TeamOwnerUserType2 == ""){
					$("#MenteesErr").text("<?php echo $config[0]->menteeName; ?> Name is required");
					err++;
				}else{
					$("#MenteesErr").text("");
				}
			}
		}
		if(MaxMembers == ""){
			$("#MaxMembersErr").text("Max Members is required");
			err++;
		}else{
			$("#MaxMembersErr").text("");
		}

		if(skills =="undefined" || skills == undefined){
			$("#skillsErr").text("Skills is required");
			err++;
		}else{
			$("#skillsErr").text("");
		}	
		//alert(err);
		if(err == 0){ 
			return true;
		}return false;
	}
    function successalertify() {
        //    alertify.success("Record has been saved successfully.");
		alertify.success("Please wait....");   
    }

    function erroralertify() {
        //   alertify.error("There has been some Error occured.");
    }
    function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'admin/teams' ?>";
        }, 1000);
    }

    $(document).ready(function () {
        $(".team-mentors-multiple").select2({
            placeholder: "Select <?php echo $config[0]->mentorName; ?>s to Add in Team",
            allowClear: true
        });

        $(".team-mentees-multiple").select2({
            placeholder: "Select <?php echo $config[0]->menteeName; ?>s to Add in Team",
            allowClear: true
        });
    });

	$(document.body).on("click", ".team-mentor-owner", function (e) {
		$(".mentorType").css("display","block");
		$(".menteeType").css("display","none");
	})

	$(document.body).on("click", ".team-mentee-owner", function (e) {
		$(".mentorType").css("display","none");
		$(".menteeType").css("display","block");
	})

/*
	function mentorDropDown(){
		$.ajax({
            type: "POST",
            dataType: "json",
            url: "teams/getMentors/",
            success: function (data) {
                alert(data)
				$('#OwnerName').empty();
				$.each(data, function(key, value) {
				        $('#OwnerName').append('<option value="' + value.id + '">' + value.name + '</option>');
				    });
				}
            ,
        });

            
        			
	}*/
</script>
