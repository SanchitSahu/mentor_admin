<?php //echo "<pre>";print_r($chart_data);
$recCountArray = array();
foreach ($chart_data as $device) {
   $recCountArray[] = $device->mentor_ids;
}

$maxCount = max($recCountArray);

?>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
	google.load("visualization", "1.0", {packages: ["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'SkillName');
		data.addColumn('number', 'No of Mentors');
		 // A column for custom tooltip content
        data.addColumn({type: 'string', role: 'tooltip'});
		
		data.addRows([
		<?php
			foreach ($chart_data as $data) {
				echo '["' . $data->SkillName . '",' . $data->mentor_ids . ',"' . $data->mentor_names . '"],';
			}
		?>
		]);
		var options = {
			title: 'Mentor Skills Chart'/*,
			vAxis: {maxValue: 4, format: '0'}*/
		};

		var chart = new google.visualization.ColumnChart(document.getElementById('columnchart_material'));

		chart.draw(data, options);
	}
</script>

<section id="main-content">
	<section class="wrapper">
			<!-- page start-->
	<div class=" container-fluid">
		<div class="row">
			<div style="overflow:hidden;">
				<div class="col-lg-12 res-mange">
				  <h2><strong>Chart</strong></h2>
					<div class="row">
						<div id="columnchart_material" style="width: 900px; height: 500px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>
</section>
