<div class="lock-wrapper">
    <div id="time"></div>
    <form role="form" class="form-inline" method="post">
        <div class="lock-box text-center">
            <div>
                <label class="error" style="color:#fff;">
                    <?php
                    //echo validation_errors(); 
                    if (isset($error_message)) {
                        echo $error_message;
                    }
                    ?>
                </label>
            </div>        
            <div class="lock-name">
                <!--<input type="email" placeholder="Email" id="exampleInputPassword2" class="form-control lock-input">-->
                <?php echo form_input(array('type' => 'email', 'name' => 'Email', 'class' => 'form-control lock-input', 'id' => 'exampleInputEmail1', 'placeholder' => 'Email', 'required' => 'required')); ?>
            </div>
            <img src="<?php echo $this->config->item('base_images') ?>lock_thumb.jpg" alt="lock avatar"/>
            <div class="lock-pwd">            
                <div class="form-group">
                    <?php echo form_input(array('type' => 'password', 'name' => 'Password', 'class' => 'form-control lock-input', 'id' => 'exampleInputPassword1', 'placeholder' => 'Password')); ?>
                    <button class="btn btn-lock" type="submit"><i class="fa fa-arrow-right"></i></button>
                </div>            
            </div>

        </div>
        <a href="#inline1" class="fancybox" style="float:right;" title="Forgot Password?">Forgot Password ?</a>
    </form>
</div>
<div id="inline1" style="width:400px;display: none;">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <strong>Forgot Password?</strong>
            </header>
            <div class="panel-body">
                <div class="form">
                    <form class="cmxform form-horizontal" id="forgotpassword" name="forgotpassword" method="post">
                        <div class="form-group ">
                            <label for="vBannerTitle" class="control-label col-lg-3">Email</label>
                            <div class="col-lg-9">
                                <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter email", 'name' => 'forgot_email', 'id' => 'forgot_email')); ?>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
									<br/>
                                    <button class="btn btn-3d-success" type="submit">Send</button>
                                    <a href="javascript:void(0);" class="btn btn-3d-success close-box" type="button" >Cancel</a><!--onclick="logalertify('admin/country');"-->
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#forgotpassword').validate({
            rules: {
                "forgot_email": {
                    "required": true,
                    "maxlength": 50
                }
            },
            messages: {
                "forgot_email": {
                    "required": "Please enter email address.",
                    "maxlength": "Please enter less than {0} characters."
                }
            },
            submitHandler: function (form) {
                var email = $("#forgot_email").val();
                if (email !== "") {
                    jQuery.ajax({
                        type: "POST",
                        url: "<?php echo $this->config->item('base_url'); ?>admin/forgotpassword",
                        data: {email: email},
                        success: function (output) {
                            if (output === "mail-sent") {
                                $.fancybox.close();
								$(".error").html("The mail has been successfully sent to the email address!");
                            } else {
								 $.fancybox.close();
								$(".error").html(output);
                                return false;
                            }
                        }
                    });
                }
            }
        });
    });
    function startTime()
    {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        // add a zero in front of numbers<10
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
        t = setTimeout(function () {
            startTime()
        }, 500);
    }

    function checkTime(i)
    {
        if (i < 10)
        {
            i = "0" + i;
        }
        return i;
    }
</script>
