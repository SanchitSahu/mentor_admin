<?php
if ($menteedefinedactions['Weight'] == "")
    $menteedefinedactions['Weight'] = 1000;
else
    $menteedefinedactions['Weight'] = $menteedefinedactions['Weight'];
?>

<section id="container" >
    <?php $res['config'] = $this->settings_model->getConfig(); ?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong><?php echo $config[0]->menteeName ?> Actions</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'admin/menteeAction/add' : 'admin/menteeAction/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_menteeAction_form", 'method' => "post"));
                                ?>
                                <div class="form-group ">
                                    <label for="MenteeActionName" class="control-label col-lg-3"><?php echo $config[0]->menteeName ?> Action Name</label>
                                    <div class="col-lg-6">
                                        <input type="hidden" id="MenteeActionID" name="MenteeActionID" value="<?php echo set_value('MenteeActionID', $menteedefinedactions['MenteeActionID']); ?>" />
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter " . $config[0]->menteeName . " Action Name", 'name' => 'MenteeActionName', 'value' => $menteedefinedactions['MenteeActionName'])); ?>
                                        <?php echo form_error('MenteeActionName'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-3">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight Name", 'name' => 'Weight', 'value' => $menteedefinedactions['Weight'])); ?>
                                        <?php echo form_error('Weight'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
    function successalertify() {
        //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
    }
    function erroralertify() {
        //   alertify.error("There has been some Error occured.");
    }
    function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'admin/menteeAction' ?>";
        }, 1000);
    }


    $().ready(function () {
        // validate signup form on keyup and submit
        $("#add_menteeAction_form").validate({
            rules: {
                MenteeActionName: {
                    required: true,
                    remote: {
                        url: "<?php echo $this->config->item('base_url'); ?>admin/menteeAction/checkMenteeActionExist",
                        type: "post",
                        data: {
                            MenteeActionID: function () {
                                return $('#MenteeActionID').val();
                            }
                        }
                    }
                }
            },
            messages: {
                MenteeActionName: {
                    required: "Please enter <?php echo $config[0]->menteeName ?> Action name",
                    remote: "Your <?php echo $config[0]->menteeName ?> Action with same name already exists"
                }
            }
        });

    });
</script>