<?php if(isset($callFromApp) && $callFromApp == true) { ?>
    <style>
        .header.fixed-top.clearfix{
            display: none;
        }
        body {
            margin-top: 0px !important;
        }
        footer{
			display:none;
		}
		#main-content{
			margin-left: 0px;
		}
		aside{
			display: none;
		}
		.wrapper{
			margin-top: 10px;
		}
    </style>
<?php } ?>

<?php
if ($mentees['Weight'] == "")
    $mentees['Weight'] = 1000;
else
    $mentees['Weight'] = $mentees['Weight'];
?>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <strong><?php echo $config[0]->menteeName ?> Management</strong>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="cmxform form-horizontal " id="holidayForm" method="post" action=""  enctype="multipart/form-data" >
                                <div class="form-group ">
                                    <label for="MenteeName" class="control-label col-lg-3"><?php echo $config[0]->menteeName ?> Name</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter " . $config[0]->menteeName . " Name", 'name' => 'MenteeName', 'value' => $mentees['MenteeName'], 'id' => 'MenteeName')); ?>
                                        
                                        <?php if(isset($callFromApp) && $callFromApp == true) { ?>
                                            <input type="hidden" id="callFromAppHidden" name="callFromAppHidden" value="true" />
                                        <?php } ?>
                                        <input type="hidden" id="MenteeID" name="MenteeID" value="<?php echo set_value('MenteeID', $mentees['MenteeID']); ?>" />
                                        <div class="custom-error" id="menteeNameErr" style="display:none;">
                                            <label class="error"><?php echo $config[0]->menteeName ?> Name already exist</label>
                                        </div>
                                    </div>
                                </div>    

                                <div class="form-group">
                                    <label class="control-label col-lg-3">Photo</label>
                                    <div class="col-md-9">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 200px;">
                                                <?php
                                                if ($mentees['MenteeImage'] == "") {
                                                    $imageURL = $this->config->item('base_images') . "default.png";
                                                } else {
                                                    $imageURL = "http://melstm.net/mentor/assets/admin/images/admin/" . $mentees['MenteeImage'];
                                                }
                                                ?>
                                                <img id="previewImg" src="<?php echo $imageURL; ?>" alt="" />
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-white btn-file">
                                                    <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                    <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                    <input type="file" id="photo" class="default" name="photo" />
                                                </span>
                                            </div>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                                        </div>                                    
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="MenteePhone" class="control-label col-lg-3 control-label"><?php echo $config[0]->menteeName ?> Phone</label>
                                    <div class="col-lg-6">
                                        <input class=" form-control" id="MenteePhone" name="MenteePhone" type="text" placeholder="Enter <?php echo $config[0]->menteeName ?> Phone" maxlength="100" value="<?php echo set_value('MenteePhone', $mentees['MenteePhone']); ?>" />
                                    </div>
                                </div>
                                    
								<div class="form-group menteePhoneDiv" style="display: <?php echo ($mentees['MenteePhone'] == '' ) ? 'none' : ''; ?>;">
                                    <label for="MenteePhoneSMS" class="control-label col-lg-3 control-label">Do you want to receive text messages on your phone/tablet?</label>
                                    <div class="col-lg-6">
                                        <!--<input class=" form-control" id="MenteePhoneSMS" name="MenteePhoneSMS" type="text" placeholder="Enter <?php echo $config[0]->menteeName ?> Phone" maxlength="100" value="<?php echo set_value('MenteePhoneSMS', $mentees['DisableSMS']); ?>" />-->
                                        <input class="change_radio with-gap" name="MenteePhoneSMS" type="radio" id="MenteePhoneSMS1" value="0" <?php echo ($mentees['DisableSMS'] == 0 || $mentees['DisableSMS'] == '') ? 'checked' : '' ; ?>/>
                                        <label for="MenteePhoneSMS1">Yes</label>
                                        <input class="change_radio with-gap" name="MenteePhoneSMS" type="radio" id="MenteePhoneSMS2" value="1" <?php echo ($mentees['DisableSMS'] == 1) ? 'checked' : '' ; ?>/>
                                        <label for="MenteePhoneSMS2" style="margin-left: 40px;">No</label>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="MenteeEmail" class="control-label col-lg-3 control-label"><?php echo $config[0]->menteeName ?> Email</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter " . $config[0]->menteeName . "Email", 'name' => 'MenteeEmail', 'value' => $mentees['MenteeEmail'], 'id' => 'MenteeEmail')); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="programmeID" class="control-label col-lg-3">Programme</label>
                                    <div class="col-lg-6">
                                        <?php echo form_dropdown('programmeID', $programmeData, set_value('programmeID', $mentees['ProgrammeID']), 'class="form-control" id="programmeID"'); ?>
                                        <?php echo form_error('programmeID', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="MenteeSourceID" class="control-label col-lg-3">Source</label>
                                    <div class="col-lg-6">
                                        <?php echo form_dropdown('MenteeSourceID', $sourceData, set_value('MenteeSourceID', $mentees['MenteeSourceID']), 'class="form-control" id="MenteeSourceID"'); ?>
                                        <?php echo form_error('MenteeSourceID', '<label class="error">', '</label>'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-3">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight", 'name' => 'Weight', 'value' => $mentees['Weight'])); ?>
                                        <?php echo form_error('Weight'); ?>
                                    </div>
                                </div>                                    
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();" >Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<script type="text/javascript">

    $(document).ready(function () {
		$('#MenteePhone').keyup(function() {
			if($(this).val() == '') {
				$('.menteePhoneDiv').hide();
			} else if($(this).val() != '') {
				$('.menteePhoneDiv').show();
			}
		});
		
        $('#photo').change(function () {
            var val = $(this).val().toLowerCase();
            var regex = new RegExp("(.*?)\.(jpg|jpeg|png|gif)$");

            if (!(regex.test(val))) {
                $(this).val('');
                $(".imgErr").html("Unsupported file");
                $(".imgErr").css("display", "block");
            } else {
                var _URL = window.URL || window.webkitURL;
                var file, img;
                if ((file = this.files[0])) {
                    img = new Image();

                    img.src = _URL.createObjectURL(file);
                    return false;
                }
            }
        });
    });
    function successalertify() {
        alertify.success("Please wait....");
    }
    function erroralertify() {
        // alertify.error("There has been some Error occured.");
    }
    function logalertify() {
        alertify.log("Update canceled");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'admin/mentees' ?>";
        }, 1000);
    }

    $(document).ready(function () {
        // validate signup form on keyup and submit
        $("#holidayForm").validate({
            rules: {
                MenteeName: {
                    required: true,
                    minlength: 2,
                    remote: {
                        url: "<?php echo $this->config->item('base_url'); ?>admin/mentees/checkMenteeName",
                        type: "post",
                        data: {
                            MenteeID: function () {
                                return $("#MenteeID").val();
                            }
                        }
                    }
                },
                //MenteePhone: {
                //    required: true
                //},
                MenteeEmail: {
                    required: true,
                    email: true,
                    remote: {
                        url: "<?php echo $this->config->item('base_url'); ?>admin/mentees/checkEmailExist",
                        type: "post",
                        data: {
                            MenteeID: function () {
                                return $("#MenteeID").val();
                            }
                        }
                    }
                }
            },
            messages: {
                MenteeName: {
                    required: "Please enter <?php echo $config[0]->menteeName ?> name",
                    minlength: "<?php echo $config[0]->menteeName ?> name must consist of at least {0} characters",
                    remote: "User with same name is already registered."
                },
                //MenteePhone: {
                //    required: "Please enter <?php echo $config[0]->menteeName ?> phone"
                //},
                MenteeEmail: {
                    required: "Please enter <?php echo $config[0]->menteeName ?> email",
                    email: "Please enter valid email",
                    remote: "Email is already registered"
                }
            }
        });
    });
</script>