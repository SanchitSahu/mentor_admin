<?php
//print_r($clientSource);exit;
if (!isset($clientProgramme['Weight'])) {
    $clientProgramme['Weight'] = 1000;
} else {
    if ($clientProgramme['Weight'] == "")
        $clientProgramme['Weight'] = 1000;
    else
        $clientProgramme['Weight'] = $clientProgramme['Weight'];
}
?>

<section id="container" >
    <!--header start-->
    <?php //echo $this->load->view('admin/includes/header'); ?>
    <!--header end-->
    <?php $res['config'] = $this->settings_model->getConfig();
    //echo $this->load->view('includes/left_menu', $res);
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong>Skills Avaliable</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                                <!--<a href="javascript:;" class="fa fa-times"></a>-->
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'admin/mentorSkillAvailable/add' : 'admin/mentorSkillAvailable/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_skill_form", 'method' => "post"));
                                ?>
                                <div class="form-group ">
                                    <label for="ProgrammeName" class="control-label col-lg-3"><?php echo $res['config'][0]->mentorName?> Name</label>
                                    <div class="col-lg-6">
                                        <?php if (isset($clientskillavailable[0]['MentorName'])) {
                                            if ($clientskillavailable[0]['MentorName'] != "") {
                                                ?>	
                                                <label for="ProgrammeName" class="control-label col-lg-3"><?php echo $clientskillavailable[0]['MentorName']; ?></label>  
												<input type="hidden" name="mentor" value="<?php echo $clientskillavailable[0]['MentorName']; ?>" >			
                                                <?php }
                                            } else { ?>	
                                            <select name="mentor" id="mentor">
											<option value="0">Select <?php echo $res['config'][0]->mentorName;?></option>
                                            <?php foreach ($mentor as $men) { ?>
                                                    <option value="<?php echo $men['MentorID']; ?>"><?php echo $men['MentorName']; ?></option>
											<?php } ?>
                                            </select>
										<?php } ?>
                                    </div>
                                </div>
                                <div class="form-group" id="mentor_skillsset">
                                    <label for="Weight" class="control-label col-lg-3">Skill Sets</label>
                                    <div class="col-lg-6">
                                        <?php if (isset($clientskillavailable[0]['MentorName'])) {
                                            if ($clientskillavailable[0]['MentorName'] != "") {
                                                ?>
                                                <?php $userskills = array(); ?>				
                                                <?php
                                                foreach ($clientskillavailable as $csa) {
                                                    $userskills[] = $csa['SkillID'];
                                                }
                                                ?>
                                                <?php foreach ($skills as $skill) { ?>
                                                    <?php if (in_array($skill['SkillID'], $userskills)) { ?>		
                                                        <input type="checkbox" checked class="control-form" name="skills[]" value="<?php echo $skill['SkillID']; ?>" /><?php echo $skill['SkillName']; ?><br><br>
													<?php } else { ?>
                                                        <input type="checkbox" class="control-form" name="skills[]" value="<?php echo $skill['SkillID']; ?>" /><?php echo $skill['SkillName']; ?><br><br>
												<?php }
											} ?>
										<?php }
										} else { ?>										
											<?php foreach ($skills as $skill) { ?>                                                                                                      											
                                                    <input type="checkbox" class="control-form" name="skills[]" value="<?php echo $skill['SkillID']; ?>" /><?php echo $skill['SkillName']; ?><br><br>
											<?php } ?>												
										<?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
<?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
    function successalertify() {
		if($('select').val() != 0){
			if($('input[type="checkbox"]:checked').length> 0){
				alertify.success("Please wait....");			
			} else {
				alertify.error("Select atleast one skill");
				window.location.href = "<?php echo $this->config->item('base_url') . 'admin/mentorSkillAvailable/add' ?>";
			}
		}else{
			alertify.error("Select <?php echo $res['config'][0]->mentorName;?>");
			window.location.href = "<?php echo $this->config->item('base_url') . 'admin/mentorSkillAvailable/add' ?>";
		}
    }
    function erroralertify() {
        //   alertify.error("There has been some Error occured.");
    }
    function logalertify() {
        alertify.log("User has cancelled event");
        setTimeout(function () {
            window.location.href = "<?php echo $this->config->item('base_url') . 'admin/mentorSkillAvailable' ?>";
        }, 1000);
    }

	$("#mentor").on("change",function(){
		var mentorId = $(this).val();
		
		 jQuery.ajax({
			type: "POST",
			url: "<?php echo $this->config->item('base_url'); ?>admin/mentorSkillAvailable/ajaxGetMentorSkills",
			data: {id:mentorId},
			success: function (output) {
				if (output !== "") {
					$('#mentor_skillsset input[type="checkbox"]').prop("checked", false);
					var values = output.split(',');
					$("#mentor_skillsset").find('input[type="checkbox"][value=' + values.join('], input[type="checkbox"][value=') + ']').prop("checked", true);
				} else {
					$("#mentor_skillsset input[type='checkbox']").prop("checked", false);
				} 
			}
		});
		
	})
</script>