<?php //print_r($topic);exit;
if($careweighting['Weight'] == "")
	$careweighting['Weight']	=	1000;
else
	$careweighting['Weight']	=	$careweighting['Weight'];

?>
<section id="container" >
    <!--header start-->
    <?php //echo $this->load->view('admin/includes/header'); ?>
    <!--header end-->
    <?php $res['config']	=	$this->settings_model->getConfig();
	//echo $this->load->view('includes/left_menu',$res); ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <strong>CARE Criteria</strong>
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                                <!--<a href="javascript:;" class="fa fa-times"></a>-->
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <?php
                                $action = ($this->uri->segment(4) == "") ? 'admin/careweighting/add' : 'admin/careweighting/edit/' . $this->uri->segment(4);
                                echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_careweighting_form", 'method' => "post"));
                                ?>
                                <div class="form-group ">
                                    <label for="Name" class="control-label col-lg-3">Name</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Name", 'name' => 'Name', 'value' => $careweighting['Name'])); ?>
                                        <?php echo form_error('Name'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="CareValues" class="control-label col-lg-3">Care Values</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Care Values", 'name' => 'CareValues', 'value' => $careweighting['CareValues'])); ?>
                                        <?php echo form_error('Name'); ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="Weight" class="control-label col-lg-3">Display Order</label>
                                    <div class="col-lg-6">
                                        <?php echo form_input(array('class' => "form-control", 'placeholder' => "Enter Weight", 'name' => 'Weight', 'value' => $careweighting['Weight'])); ?>
                                        <?php echo form_error('Weight'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-3d-success" type="submit" onclick="successalertify();">Save</button>
                                        <button class="btn btn-3d-inverse" type="button" onclick="logalertify();">Cancel</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<script type="text/javascript">
     function successalertify() {
     //    alertify.success("Record has been saved successfully.");
        alertify.success("Please wait....");
     }
     function erroralertify() {
      //   alertify.error("There has been some Error occured.");
     }
     function logalertify() {
     	alertify.log("User has cancelled event");
        setTimeout(function () {
	        window.location.href = "<?php echo $this->config->item('base_url').'admin/careweighting'?>";
        },1000);
     }


    $().ready(function() {
        // validate signup form on keyup and submit
        $("#add_careweighting_form").validate({
            rules: {
                Name: {
                    required: true,
                    minlength: 2
                }
            },
            messages: {
                Name: {
                    required: "Please enter careweighting name",
                    minlength: "Your careweighting name must consist of at least {0} characters"
                }
            }
        });
        
    });
</script>