<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Choose Password</title>
	<style type="text/css">

@media screen and (max-width: 600px) {
    table[class="container"] {
        width: 95% !important;
    }
}
	#tableCss
	{
		background: #B3B3FF none repeat scroll 0 0;
		border-radius: 10px;
		margin-top: 20px;
		width: 650px;
		color:#fff;
		height:300px;
	}
	
	.form-control {
		background-color: #fff;
		background-image: none;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
		color: #555;
		display: block;
		font-size: 14px;
		line-height: 1.42857;
		padding: 6px 12px;
		transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
		width: 80%;
	}
	#outlook a {padding:0;}
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
		.ExternalClass {width:100%;}
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
		a img {border:none;}
		.image_fix {display:block;}
		p {margin: 1em 0;}
		h1, h2, h3, h4, h5, h6 {color: black !important;}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
			color: red !important; 
		 }

		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
			color: purple !important; 
		}

		table td {border-collapse: collapse;}

		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }

		a {color: #000;}

		@media only screen and (max-device-width: 480px) {

			a[href^="tel"], a[href^="sms"] {
						text-decoration: none;
						color: black; /* or whatever your want */
						pointer-events: none;
						cursor: default;
					}

			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
						text-decoration: default;
						color: orange !important; /* or whatever your want */
						pointer-events: auto;
						cursor: default;
					}
		}


		@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
			a[href^="tel"], a[href^="sms"] {
						text-decoration: none;
						color: blue; /* or whatever your want */
						pointer-events: none;
						cursor: default;
					}

			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
						text-decoration: default;
						color: orange !important;
						pointer-events: auto;
						cursor: default;
					}
		}

		@media only screen and (-webkit-min-device-pixel-ratio: 2) {
			/* Put your iPhone 4g styles in here */
		}

		@media only screen and (-webkit-device-pixel-ratio:.75){
			/* Put CSS for low density (ldpi) Android layouts in here */
		}
		@media only screen and (-webkit-device-pixel-ratio:1){
			/* Put CSS for medium density (mdpi) Android layouts in here */
		}
		@media only screen and (-webkit-device-pixel-ratio:1.5){
			/* Put CSS for high density (hdpi) Android layouts in here */
		}
		/* end Android targeting */
		h2{
			color:#181818;
			font-family:Helvetica, Arial, sans-serif;
			font-size:22px;
			line-height: 22px;
			font-weight: normal;
		}
		a.link1{

		}
		a.link2{
			color:#fff;
			text-decoration:none;
			font-family:Helvetica, Arial, sans-serif;
			font-size:16px;
			color:#fff;border-radius:4px;
		}
		p{
			color:#555;
			font-family:Helvetica, Arial, sans-serif;
			font-size:16px;
			line-height:160%;
		}
		
.btn-success {
    background-color: #fff;
    background-image: none;
    background-repeat: repeat-x;
    border: 1px solid #fff;
    box-shadow: 1px 1px #fff inset, -1px -1px #fff inset, 0 1px #fff, 0 2px #fff, 1px 1px 3px rgba(22, 44, 22, 0.6);
    color: white;
    margin-bottom: 2px;
    margin-top: 0;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	border-radius: 30px;
	color:#B3B3FF;
	font-size:15px;
	width:100px;
	padding:10px 0px;
}
	</style>

<script type="colorScheme" class="swatch active">
  {
    "name":"Default",
    "bgBody":"ffffff",
    "link":"fff",
    "color":"555555",
    "bgItem":"ffffff",
    "title":"181818"
  }
</script>

</head>
<body>

<?php //echo $content.'dfgdfgdf'; ?>
	<!-- Wrapper/Container Table: Use a wrapper table to control the width and the background color consistently of your email. Use this approach instead of setting attributes on the body tag. -->
	<table cellpadding="0" width="100%" cellspacing="0" border="0" id="backgroundTable" class='bgBody'>
	<tr>
		<td>
	<table cellpadding="0" width="620" class="container" align="center" cellspacing="0" border="0">
	<tr>
		<td>
		<!-- Tables are the most common way to format your email consistently. Set your table widths inside cells and in most cases reset cellpadding, cellspacing, and border to zero. Use nested tables as a way to space effectively in your message. -->
		

		<table id="tableCss" cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
			<tr>
				<td class='movableContentContainer bgItem'>

					<div class='movableContent'>
						<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
							<tr height="40">
								<td width="200">&nbsp;</td>
								<td width="200">&nbsp;</td>
								<td width="200">&nbsp;</td>
							</tr>
							<tr>
								<td width="200" valign="top">&nbsp;</td>
								<td width="200" valign="top" align="center">
									<div class="contentEditableContainer contentImageEditable">
					                	<div class="contentEditable" align='center' >
					                  		<img src="<?php echo base_url().'assets/admin/images/logo-64x64.png'; ?>"alt='Logo'  data-default="placeholder" />
					                	</div>
					              	</div>
								</td>
								<td width="200" valign="top">&nbsp;</td>
							</tr>
							<tr height="25">
								<td width="200">&nbsp;</td>
								<td width="200">&nbsp;</td>
								<td width="200">&nbsp;</td>
							</tr>
						</table>
					</div>

					<div class='movableContent'>
						<form action="<?php echo base_url()?>admin/index/updateMentorpassword" method="post">
						<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
							<tr>
								<td>Password</td>
								<td align="center" style="padding-top:25px;">
									<input class="form-control" type="password" name="password" id="password">
									<span id="pwdErr"></span>
								</td>
							</tr>
							<tr>
								<td>Confirm Password</td>
								<td align="center" style="padding-top:25px;">
									<input class="form-control" type="password" name="cpassword" id="cpassword">
									<span id="cpwdErr"></span>
								</td>
							</tr>
							<tr>
								<td width="240" valign="top"> </td>
								<td colspan="2" style="padding-top:25px;padding-bottom:25px">
								<input type="hidden" name="mainId" id="mainId" value="<?php echo $id; ?>">
								<input type="submit" class="btn-success" name="submit" value="Set" onclick="return checkPassword();"></td>
							</tr>
						</table>
                        </form>
					</div>
				</td>
			</tr>
		</table>

		
		

	</td></tr></table>
	
		</td>
	</tr>
	</table>
	<!-- End of wrapper table -->

<!--Default Zone End-->

</body>
</html>
<script>

function checkPassword()
{
	var	 pwd	=	document.getElementById("password").value;
	var  len	=	document.getElementById("password").value.length;	
	
	var	 cpwd	=	document.getElementById("cpassword").value;
	var  clen	=	document.getElementById("cpassword").value.length;
	if(pwd	==	""){
		document.getElementById("pwdErr").textContent="Please Enter Password";
		return false;
	}else if(len <= 6){
		document.getElementById("pwdErr").textContent="Length must be more then 6 character";
		return false;
	}else{
		if(cpwd	==	""){
			document.getElementById("cpwdErr").textContent="Please Enter Confirm Password";
			return false;
		}else if(len <= 6){
			document.getElementById("cpwdErr").textContent="Length must be more then 6 character";
			return false;
		}else if(pwd != cpwd){
			document.getElementById("cpwdErr").textContent="Password does not match";
			return false;
		}else{
			document.getElementById("cpwdErr").textContent="";
			return true;
		}		
	}

	
}
</script>
