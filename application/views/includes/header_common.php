<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />
        <meta name="author" content="ThemeBucket" />
        <title>MELS</title>
        <!--Core CSS -->
        <link href="<?php echo $this->config->item('base_assets'); ?>bs3/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo $this->config->item('base_css'); ?>bootstrap-reset.css" rel="stylesheet" />
        <link href="<?php echo $this->config->item('base_fonts'); ?>css/font-awesome.css" rel="stylesheet" />
      
		<?php 
				
		//	$this->load->model('admin/settings_model');
			$res['config']	=	$this->settings_model->getConfig();
			//echo "<pre>";print_r($res['config']);exit;
			session_start();
			
			$errorLog	=	$this->settings_model->errorLog();
			
			
			$this->session->set_userdata('headerColor', $res['config'][0]->headerColor);
			$this->session->set_userdata('leftBarColor', $res['config'][0]->leftBarColor);
			$this->session->set_userdata('logo', $res['config'][0]->logo);
			$this->session->set_userdata('buttonColor', $res['config'][0]->buttonColor);
			$this->session->set_userdata('fontColor', $res['config'][0]->fontColor);
			$this->session->set_userdata('backgroundColor', $res['config'][0]->backgroundColor);
			$this->session->set_userdata('highlightColor', $res['config'][0]->highlightColor);
			$this->session->set_userdata('leftMenuFont', $res['config'][0]->leftMenuFont);
			$this->session->set_userdata('tableFont', $res['config'][0]->tableFont);
			$this->session->set_userdata('tableBackground', $res['config'][0]->tableBackground);
			$this->session->set_userdata('icon', $res['config'][0]->icon);
			
			
				
			$_SESSION['buttonColor']	=	$res['config'][0]->buttonColor;
			$_SESSION['headerColor']	=	$res['config'][0]->headerColor;
			$_SESSION['leftBarColor']	=	$res['config'][0]->leftBarColor;
			$_SESSION['logo']	=	$res['config'][0]->logo;
			$_SESSION['fontColor']	=	$res['config'][0]->fontColor;
			$_SESSION['backgroundColor']	=	$res['config'][0]->backgroundColor;
			$_SESSION['highlightColor']	=	$res['config'][0]->highlightColor;
			$_SESSION['leftMenuFont']	=	$res['config'][0]->leftMenuFont;
			$_SESSION['tableFont']	=	$res['config'][0]->tableFont;
			$_SESSION['tableBackground']	=	$res['config'][0]->tableBackground;
			$_SESSION['icon']	=	$res['config'][0]->icon;
			
			
				/*print_r($_SESSION);
				session_commit();*/
		?>
        <link rel="shortcut icon" href="<?php echo $this->config->item('base_images'); ?><?php echo $res['config'][0]->icon;?>" />
		<link rel="shortcut icon" type="image/jpg" href="<?php echo $this->config->item('base_images'); ?><?php echo $res['config'][0]->icon;?>"/>
        <!--dynamic data table -->
          <link href="<?php echo $this->config->item('base_js'); ?>advanced-datatable/css/demo_page.css" rel="stylesheet" />
          <link href="<?php echo $this->config->item('base_js'); ?>advanced-datatable/css/demo_table.css" rel="stylesheet" />
          <link rel="stylesheet" href="<?php echo $this->config->item('base_js'); ?>data-tables/DT_bootstrap.css" />
       <!-- end of dynamic data table--> 
	   
	   
        <style>
		<?php include_once $_SERVER['DOCUMENT_ROOT']."/mentor/assets/admin/css/style.php"; ?>
		</style>
       

	   <!-- Custom styles for this template -->
		<!--<link href='<?php echo $this->config->item('base_css'); ?>style.php' type="text/css" rel="stylesheet" />-->
        <link href="<?php echo $this->config->item('base_css'); ?>style-responsive.css" rel="stylesheet" />
        <link href="<?php echo $this->config->item('base_css'); ?>livicons-help.css" rel="Stylesheet" />
        <link href="<?php echo $this->config->item('base_css'); ?>prettify.css" rel="Stylesheet" />
        <link href="<?php echo $this->config->item('base_css'); ?>3d_buttons.php" rel="Stylesheet" />
        <link href="<?php echo $this->config->item('base_css'); ?>alertify.core.css" rel="Stylesheet" />
        <link href="<?php echo $this->config->item('base_css'); ?>alertify.default.css" rel="Stylesheet" />  
        <script type="text/javascript">    
			window.history.forward();
			function noBack() { 
			  window.history.forward(); 
			}
        </script>
		
        <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_js'); ?>bootstrap-datepicker/css/datepicker.css" />
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.js"></script>
		<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery-migrate-1.2.1.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.validate.min.js"></script>
      <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]>
        <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
		<script>
			$(document).ready(function(){
				setTimeout(function(){
					$('.dcjq-icon').each(function(){
						$(this).addClass('fa').addClass('fa-chevron-right').removeClass('dcjq-icon');
					});
					//$('.sidebar-menu li').click(function(){
					//	if($(this).children('a.dcjq-parent:first').length > 0){
					//		$('.sidebar-menu li a span').addClass('fa').addClass('fa-chevron-right').removeClass('fa-chevron-down');
					//		$(this).children('a.dcjq-parent:first').find('span').removeClass('fa').removeClass('dcjq-icon').removeClass('fa-chevron-right').removeClass('fa-chevron-down');
					//		if($(this).children('a.dcjq-parent').hasClass('active') == true){
					//			$(this).children('a.dcjq-parent:first').find('span').addClass('fa').addClass('fa-chevron-down');
					//		} else {
					//			$(this).children('a.dcjq-parent:first').find('span').addClass('fa').addClass('fa-chevron-right');
					//		}
					//	}
					//});
					$('.sidebar-menu li a').click(function(){
						//if($(this).children('a.dcjq-parent:first').length > 0){
						//}
						if($(this).parent().parent().hasClass('sub') == false) {
							$('.sidebar-menu li a span').addClass('fa').addClass('fa-chevron-right').removeClass('fa-chevron-down');
						} else {
							$('.sidebar-menu .sub li a span').addClass('fa').addClass('fa-chevron-right').removeClass('fa-chevron-down');
						}
						$(this).find('span').removeClass('fa').removeClass('dcjq-icon').removeClass('fa-chevron-right').removeClass('fa-chevron-down');
						if($(this).hasClass('active') == true){
							$(this).find('span').addClass('fa').addClass('fa-chevron-down');
						} else {
							$(this).find('span').addClass('fa').addClass('fa-chevron-right');
						}
					});
				},1000);
				
				<?php 
				$showDetail	=	"Show Details (".$errorLog.")";
				if($this->session->flashdata('error'))  { ?>
						alertify.set({ labels: {
							ok     : "OK",
							cancel : "<?php echo $showDetail;?>"
						} });
						var href = "<?php echo base_url();?>admin/errors";
						alertify.confirm("<?php echo $this->session->flashdata('error');?>", function (e) {
							console.log(e);
							if (!e) {
								window.location.href = href;
							}
						});
	
						//alertify.confirm("<?php echo $this->session->flashdata('error');?>");
						//alertify.alert("<?php echo $this->session->flashdata('error');?>");
				<?php } ?>
				
				
				<?php if($this->session->flashdata('success')) { ?>
						alertify.success("<?php echo $this->session->flashdata('success');?>");
				<?php } ?>
				
				
			});
		</script>
		
    </head>
	<?php $userSession = $this->session->userdata('admin_data');?>
    <body <?php if(isset($userSession['returnFromAdmin']) && !empty($userSession['returnFromAdmin'])) echo 'onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload=""';?>>
        
