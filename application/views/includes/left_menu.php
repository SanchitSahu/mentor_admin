<?php
$userSession = $this->session->userdata('admin_data');
$getLeftMenuCount = getLeftMenuCount();
?>
<!--sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
			<?php if ($userSession['UserType'] == "superadmin") { ?>
				<ul class="sidebar-menu" id="nav-accordion">
					<li>
						<a href="<?php echo $this->config->item('base_url') . 'superadmin/dashboard' ?>">
							<i class="livicon" data-s="18" data-n="home" data-c="<?php echo $config[0]->leftMenuFont; ?>" data-hc="0" id="I1" style="width: 16px; height: 16px;"></i> Dashboard
						</a>
					</li>
					<li>
                        <a href="<?php echo $this->config->item('base_url') . 'superadmin/emailtemplate' ?>">
                            <i class="livicon" data-s="18" data-n="home" data-c="<?php echo $config[0]->leftMenuFont; ?>" data-hc="0" id="I2" style="width: 16px; height: 16px;"></i> Manage Email Templates
                        </a>
                    </li>
					<li>
						<a href="<?php echo $this->config->item('base_url') . 'superadmin/settings' ?>">
							<i class="livicon" data-s="18" data-n="settings" data-c="<?php echo $config[0]->leftMenuFont; ?>" data-hc="0" id="I10" style="width: 16px; height: 16px;"></i> Configuration Settings
						</a>
					</li>
					<li>
						<a href="<?php echo $this->config->item('base_url') . 'superadmin/help' ?>">
							<i class="livicon" data-s="18" data-n="help" data-c="<?php echo $config[0]->leftMenuFont;?>" data-hc="0" id="I11" style="width: 16px; height: 16px;"></i> Help Text
						</a>
					</li>
				</ul>	
			<?php } else { ?>
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a href="<?php echo $this->config->item('base_url') . 'admin/dashboard' ?>">
                        <i class="livicon" data-s="18" data-n="home" data-c="<?php echo $config[0]->leftMenuFont;?>" data-hc="0" id="I1" style="width: 16px; height: 16px;"></i> Dashboard
                    </a>
                </li>
				<li>
                    <a href="<?php echo $this->config->item('base_url') . 'admin/settings' ?>">
                        <i class="livicon" data-s="18" data-n="settings" data-c="<?php echo $config[0]->leftMenuFont;?>" data-hc="0" id="I10" style="width: 16px; height: 16px;"></i> Configuration Settings
                    </a>
                </li>
				
                <?php if ($userSession['UserType'] == "Admin") { ?>
                    <li class="sub-menu">
                        <a href="javascript:;">
                            <i class="livicon" data-s="18" data-n="key" data-c="<?php echo $config[0]->leftMenuFont;?>" data-hc="0" id="I2" style="width: 16px; height: 16px;"></i> Master Lists
                        </a>
                        <ul class="sub">
							<li class="sub-menu">
								<a href="javascript:;">Meetings</a>
								<ul class="sub">
										<li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/meetingtype/' ?>">Meeting Types (<?php echo $getLeftMenuCount[0]->meetingTypeCount; ?>)</a></li>
										<li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/meetingplace/' ?>">Meeting Places (<?php echo $getLeftMenuCount[0]->meetingPlaceCount; ?>)</a></li>
								</ul>
							</li>

							<li class="sub-menu">
								<a href="javascript:;">Topics</a>
								<ul class="sub">
									<li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/topic/' ?>">Main Topics (<?php echo $getLeftMenuCount[0]->topicCount; ?>)</a></li>
									<li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/subtopic/' ?>">Sub Topics (<?php echo $getLeftMenuCount[0]->subTopicCount; ?>)</a></li>
								</ul>
							</li>
							<li class="sub-menu">
								<a href="javascript:;">Actions</a>
								<ul class="sub">
									<li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/mentorAction/' ?>"><?php echo $config[0]->mentorName?> Actions (<?php echo $getLeftMenuCount[0]->mentorActionsCount; ?>)</a></li>
									<li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/menteeAction/' ?>"><?php echo $config[0]->menteeName?> Actions (<?php echo $getLeftMenuCount[0]->menteeActionsCount; ?>)</a></li>
								</ul>
							</li>
                            <li><a id="mentor_skills" href="<?php echo $this->config->item('base_url') . 'admin/skill/' ?>">Skills (<?php echo $getLeftMenuCount[0]->skillCount; ?>)</a></li>
                            <li><a href="<?php echo $this->config->item('base_url') . 'admin/careCriteria/' ?>">CARE Criteria (<?php echo $getLeftMenuCount[0]->careCriteriaCount; ?>)</a></li>
							<!--<li><a href="<?php echo $this->config->item('base_url') . 'admin/userProgramme/' ?>"><?php echo $config[0]->menteeName?> Programmes (<?php echo $getLeftMenuCount[0]->menteeProgrammesCount; ?>)</a></li>-->
							
							<li class="sub-menu">
								<a href="javascript:;">Standard <?php echo $config[0]->mentorName?> Invitation Responses</a>
								<ul class="sub">
										<li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/comments/listAccept' ?>">Standard Acceptance Responses (<?php echo $getLeftMenuCount[0]->acceptCommentCount; ?>)</a></li>
										<li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/comments/listReject' ?>">Standard Decline Responses (<?php echo $getLeftMenuCount[0]->rejectCommentCount; ?>)</a></li>
								</ul>
							</li>
							
							<li><a href="<?php echo $this->config->item('base_url') . 'admin/abbreviations' ?>">Abbreviations (<?php echo $getLeftMenuCount[0]->abbreviationsCount; ?>)</a></li>
                        </ul>
                    </li>
					
					<li class="sub-menu">
                        <a href="javascript:;">
                            <i class="livicon" data-s="18" data-n="key" data-c="<?php echo $config[0]->leftMenuFont;?>" data-hc="0" id="I22" style="width: 16px; height: 16px;"></i> User Management
                        </a>
                        <ul class="sub">
							<li class="sub-menu">
								<a href="javascript:;"><?php echo $config[0]->mentorName?></a>
								<ul class="sub">									
									<li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/userRoles/' ?>"><?php echo $config[0]->mentorName?> Roles (<?php echo $getLeftMenuCount[0]->mentorRoleCount; ?>)</a></li>
									<li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/userSource/' ?>"><?php echo $config[0]->mentorName?> Source (<?php echo $getLeftMenuCount[0]->mentorSourceCount; ?>)</a></li>
									<li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/invitationSettings/' ?>"><?php echo $config[0]->mentorName?> Reminder Settings</a></li>
                                    <li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/mentors/' ?>"><?php echo $config[0]->mentorName?> Management (<?php echo $getLeftMenuCount[0]->mentorCount; ?>)</a></li>
									<li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/mentorSkillAvailable' ?>"><?php echo $config[0]->mentorName?> Skills Available (<?php echo $getLeftMenuCount[0]->mentorSkillsCount; ?>)</a></li>
								</ul>
							</li>	
							<li class="sub-menu">
								<a href="javascript:;"><?php echo $config[0]->menteeName?></a>
								<ul class="sub">									 
									<li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/clientSource/' ?>"><?php echo $config[0]->menteeName?> Source (<?php echo $getLeftMenuCount[0]->menteeSourceCount; ?>)</a></li>
									<li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/userProgramme' ?>"><?php echo $config[0]->menteeName?> Programme (<?php echo $getLeftMenuCount[0]->menteeProgrammesCount; ?>)</a></li>
                                    <li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/mentees/' ?>"><?php echo $config[0]->menteeName?> Management (<?php echo $getLeftMenuCount[0]->menteeCount; ?>)</a></li>
									<li><a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/menteeSkillNeeded' ?>"><?php echo $config[0]->menteeName?> Skills Needed (<?php echo $getLeftMenuCount[0]->menteeSkillsCount; ?>)</a></li>
								</ul>
							</li>
							<li>
								<a href="<?php echo $this->config->item('base_url') . 'admin/relationship' ?>">
									Relationships
								</a>
							</li>	
							<li>
								<a href="<?php echo $this->config->item('base_url') . 'admin/newusers' ?>">
									User Notifications
								</a>
							</li>		
                        </ul>
						
                    </li>
                <?php } ?>
				
				<!--<li>
					<a href="<?php echo $this->config->item('base_url') . 'admin/groups' ?>">
						<i class="livicon" data-s="18" data-n="users" data-c="<?php echo $config[0]->leftMenuFont;?>" data-hc="0" id="GM20" style="width: 16px; height: 16px;"></i> Group Management
					</a>
				</li>-->

				<li>
					<a href="<?php echo $this->config->item('base_url') . 'admin/teams' ?>">
						<i class="livicon" data-s="18" data-n="users" data-c="<?php echo $config[0]->leftMenuFont;?>" data-hc="0" id="TM20" style="width: 16px; height: 16px;"></i> Team Management
					</a>
				</li>
				
				<li class="sub-menu">
                        <a href="javascript:;">
                            <i class="livicon" data-s="18" data-n="linechart" data-c="<?php echo $config[0]->leftMenuFont;?>" data-hc="0" id="GLI80" style="width: 16px; height: 16px;"></i> Reports
                        </a>
                        <ul class="sub">
							<li class="sub-menu">
								<a href="javascript:;"><?php echo $config[0]->mentorName?></a>
								<ul class="sub">
									<li>
										<a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/reports/mentorreport' ?>">
											<?php echo $config[0]->mentorName?> Reports
										</a>
									</li>
									<li>
										<a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/reports/mentordetailreport' ?>">
											<?php echo $config[0]->mentorName?> Detail Reports
										</a>
									</li>
									<li>
										<a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/reports/mentorrolereport' ?>">
											<?php echo $config[0]->mentorName?> Role Reports
										</a>
									</li>
									<li>
										<a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/reports/mentorskillreport' ?>">
											Skills <?php echo $config[0]->mentorName?> Has Report
										</a>
									</li>
									<li>
										<a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/reports/mentorsourcereport' ?>">
											<?php echo $config[0]->mentorName?> Source Reports
										</a>
									</li>
								</ul>
							</li>
							<li class="sub-menu">
								<a href="javascript:;"><?php echo $config[0]->menteeName?></a>
								<ul class="sub">
									<li>
										<a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/reports/menteereport' ?>">
											<?php echo $config[0]->menteeName?> Reports
										</a>
									</li>
									<li>
										<a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/reports/menteedetailreport' ?>">
											<?php echo $config[0]->menteeName?> Detail Reports
										</a>
									</li>
									<li>
										<a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/reports/mentorprogrammereport' ?>">
											<?php echo $config[0]->menteeName?> Programmes Reports
										</a>
									</li>
									<li>
										<a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/reports/menteeskillreport' ?>">
											<?php echo $config[0]->menteeName?>s&apos; Skills Needed Report
										</a>
									</li>
									<li>
										<a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/reports/menteesourcereport' ?>">
											<?php echo $config[0]->menteeName?> Source Reports
										</a>
									</li>	
								</ul>
							</li>	
							<li class="sub-menu">
								<a href="javascript:;">Group Reports</a>
								<ul class="sub">
									<li>
										<a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/reports/subadminreport' ?>">
											Subadmin Report
										</a>
									</li>
									<li>
										<a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/reports/teamreport' ?>">
											Team Report
										</a>
									</li>
								</ul>
							</li>	
							<li>
								<a href="<?php echo $this->config->item('base_url') . 'admin/reports/mentorMeetingReport' ?>">
								Meetings Report
								</a>
							</li>
							<li>
								<a href="<?php echo $this->config->item('base_url') . 'admin/reports/followUpReport' ?>">
								Follow Up Meeting Report
								</a>
							</li>
							<li>
								<a href="<?php echo $this->config->item('base_url') . 'admin/reports/followUpDetailsReport' ?>">
								Follow Up Meeting Detail Report
								</a>
							</li>
							<?php /* <li class="sub-menu">
								<a href="javascript:;">Meetings Report</a>
								<ul class="sub">
									<li>
										<a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/reports/mentorMeetingReport' ?>">
											<?php echo $config[0]->mentorName?> Meeting Report
										</a>
									</li>
									<li>
										<a class="subClass" href="<?php echo $this->config->item('base_url') . 'admin/reports/menteeMeetingReport' ?>">
											<?php echo $config[0]->menteeName?> Meeting Report
										</a>
									</li>
								</ul>
							</li> */?>
                        </ul>
					</li>
					
					<li>
						<a href="<?php echo $this->config->item('base_url') . 'admin/errors' ?>">
							<i class="livicon" data-s="18" data-n="warning-alt" data-c="<?php echo $config[0]->leftMenuFont;?>" data-hc="0" id="I20" style="width: 16px; height: 16px;"></i> Error Logs (<?php echo $getLeftMenuCount[0]->errorsCount; ?>)
						</a>
					</li>
				</ul>
			 <?php } ?>
        </div>        
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->
<script>
$(document).ready(function(){
    var str=location.href.toLowerCase();
	$(".sidebar-menu li a").each(function() {
		if (str.indexOf(this.href.toLowerCase()) > -1) {
			$("li.highlight").removeClass("highlight");
			$(this).parent().addClass("highlight");
		}
	});
	setTimeout(function(){
		$("li.highlight").parent().parent().addClass("main-highlight").parent().parent().addClass("main-highlight");
	},50);
	if(str.indexOf('dashboard'.toLowerCase()) > -1){
		
	}
	
	
});
</script>
