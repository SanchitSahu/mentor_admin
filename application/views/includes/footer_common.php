<!-- Placed js at the end of the document so the pages load faster -->
<footer style="font-size: 15px;padding-right: 20px; bottom: 0px; text-align: right; width: 100%;"><span><?php echo VERSION_NAME; ?></span></footer>
<!--Core js-->
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_assets'); ?>bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.dcjqaccordion.2.7.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.nicescroll.js"></script>
<!--Easy Pie Chart-->
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>sparkline/jquery.sparkline.js"></script>

<!--jQuery Flot Chart-->
<!--<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>flot-chart/jquery.flot.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>flot-chart/jquery.flot.tooltip.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>flot-chart/jquery.flot.resize.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>flot-chart/jquery.flot.pie.resize.js"></script>-->

<!--dynamic table-->
<script type="text/javascript" language="javascript" src="<?php echo $this->config->item('base_js'); ?>advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>data-tables/DT_bootstrap.js"></script>
<!--common script init for all pages-->
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>scripts.js"></script>
<!--dynamic table initialization -->
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>dynamic_table_init.js"></script>

<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>morris-chart/raphael-min.js"></script>

<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>morris-chart/raphael-min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>prettify.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>raphael-min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>livicons-1.3.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>alertify.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.default-date-picker').datepicker({
            format: 'yyyy-mm-dd'
        });
    });
    function deleteconfirm() {
        alertify.confirm("Do you want to Delete this Record(s)?", function (e) {
            if (e) {
                alertify.success("Record has been deleted successfully.");
            } else {
                alertify.log("User has canceled the event.");
            }
        });
    }
</script>
</body>
</html>
