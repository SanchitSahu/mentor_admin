<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<link rel="shortcut icon" type="image/jpg" href="<?php echo $this->config->item('base_images');?>/logo.png"/>

        <title>MELS Admin Sign In</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo $this->config->item('base_assets'); ?>bs3/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $this->config->item('base_css'); ?>bootstrap-reset.css" rel="stylesheet">
        <!--external css-->
        <link href="<?php echo $this->config->item('base_fonts'); ?>css/font-awesome.css" rel="stylesheet" />
        <!-- Custom styles for this template -->
        <link href="<?php echo $this->config->item('base_css'); ?>style.css" rel="stylesheet">
        <link href="<?php echo $this->config->item('base_css'); ?>style-responsive.css" rel="stylesheet" />
        <link href="<?php echo $this->config->item('base_css'); ?>jquery.fancybox.css" rel="Stylesheet" />
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.mousewheel.pack.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.fancybox.pack.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.validate.min.js"></script>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            $(document).ready(function () {
                $('.fancybox').fancybox({
                    autoSize: true,
                    width: "auto",
                    height: "80%"
                });
                
                $(".close-box").click(function (){
                    $.fancybox.close();
                });
                
            });
        </script>
    </head>

    <body class="lock-screen" onload="startTime()">
