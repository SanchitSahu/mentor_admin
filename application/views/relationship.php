<?php //echo "<pre>";print_r($mentor);exit;?>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
<div class=" container-fluid">
  <div class="row">
    
    <div class="col-lg-12 res-mange">
      <h2><strong>Relationship Manager</strong></h2>
      <div class="row">
        <div class="col-lg-5 col-sm-12 col-md-6">
          <h3>Mentor Panel</h3>
         <div class="top-col">
          <div class="col-lg-6 col-sm-12 col-md-6 select-option">
            <select class="form-control">
              <option disabled="disabled">Skills</option>
              <option>Skills 1</option>
              <option>Skills 2</option>
              <option>Skills 3</option>
              <option>Skills 4</option>
            </select>
          </div>
          <div class="col-lg-6 select-option">
            <select class="form-control">
              <option disabled="disabled">Expirence</option>
              <option>1 year</option>
              <option>2 year</option>
              <option>3 year</option>
              <option>4 year</option>
            </select>
          </div>
          <div class="col-lg-12 select-mentor">
            <div class="col-lg-4"><strong>Select Mentor</strong></div>
            <div class="col-lg-8">
              <select class="form-control">
                <option disabled="disabled">Mentor 1</option>
                <option>Mentor 2</option>
                <option>Mentor 3</option>
                <option>Mentor 4</option>
                <option>Mentor 5</option>
              </select>
            </div>
          </div>
         </div>
          <div class="mentor-block">
            <div class="user-slider">
              <div>
                <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
                  <!-- Indicators --> 
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
					
					<?php $j=0;
					foreach($mentor as $val){?>
                    <div class="item <?php if($j == 0) echo "active"?>" id="<?php echo $val->MentorID?>">
                      <div class="user-img"><img src="<?php echo base_url()?>/assets/admin/images/<?php echo $val->MentorID.".jpg";?>" class="img-circle"></div>
                      <div class="user-name"><?php $val->MentorName ;?><span><a href="#">(25)
                        <div class="connect-friend">
                          <ul>
                            <li>James</li>
                            <li>Alexander</li>
                            <li>Ethan</li>
                            <li>William</li>
                          </ul>
                        </div>
                        </a> </span> </div>
                      <form>
                        <textarea name="skill" placeholder="Skill Set"></textarea>
                        <textarea name="skill" rows="" placeholder="Expirence"></textarea>
                      </form>
                    </div>
                    <?php $j++; } ?>
                  </div>
                  
                  <!-- Left and right controls --> 
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <span class="left-errow" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <span class="right-errow" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
              </div>
            </div>
          </div>
        </div>
		
		
        <div class="col-lg-2 col-sm-12 col-md-6 button-block">
          <button type="button" class="green-button"><img src="<?php echo base_url()?>/assets/admin/images/btn1.png"></button>
          <button type="button" class="red-button"><img src="<?php echo base_url()?>/assets/admin/images/btn2.png"></button>
        </div>
        <div class="col-lg-5 col-sm-12 col-md-6">
          <h3>Mentee Panel</h3>
       <div class="top-col">
          <div class="col-lg-6 col-sm-12 col-md-6 select-option">
            <select class="form-control">
              <option disabled="disabled">Skills</option>
              <option>Skills 1</option>
              <option>Skills 2</option>
              <option>Skills 3</option>
              <option>Skills 4</option>
            </select>
          </div>
          <div class="col-lg-6 select-option">
            <select class="form-control">
              <option disabled="disabled">Expirence</option>
              <option>1 year</option>
              <option>2 year</option>
              <option>3 year</option>
              <option>4 year</option>
            </select>
          </div>
          <div class="col-lg-12 select-mentor">
            <div class="col-lg-4"><strong>Select Mentee</strong></div>
            <div class="col-lg-8">
              <select class="form-control">
                <option disabled="disabled">Mentee</option>
                <option>Mentee 2</option>
                <option>Mentee 3</option>
                <option>Mentee 4</option>
                <option>Mentee 5</option>
              </select>
            </div>
          </div>
         </div>
          <div class="mentor-block">
            <div class="user-slider">
              <div>
                <div id="myCarousel1" class="carousel slide" data-ride="carousel"> 
                  <!-- Indicators --> 
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
					<?php $k = 0;
					foreach($mentee as $val){?>
                    <div class="item <?php if($k == 0) echo "active"?>" id="<?php echo $val->MenteeID?>">
                      <div class="user-img"><img src="<?php echo base_url()?>/assets/admin/images/<?php echo $val->MenteeID.".jpg";?>" class="img-circle"></div>
                      <div class="user-name"><?php $val->MenteeName ;?><span><a href="#">(25)
                        <div class="connect-friend">
                          <ul>
                            <li>James</li>
                            <li>Alexander</li>
                            <li>Ethan</li>
                            <li>William</li>
                          </ul>
                        </div>
                        </a> </span> </div>
                      <form>
                        <textarea name="skill" cols="" rows="" placeholder="Skill Set"></textarea>
                        <textarea name="skill" cols="" rows="" placeholder="Expirence"></textarea>
                      </form>
                    </div>
                    <?php $k++; } ?>
                  </div>
                  
                  <!-- Left and right controls --> 
                  <a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev"> <span class="left-errow" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next"> <span class="right-errow" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
</section>

<script type="text/javascript">
$('.carousel').carousel({
    pause: true,
    interval: false
});
</script>
<script>
$( ".green-button" ).click(function() {
	var mentor	=	$("#myCarousel").find('div.active').attr('id');
	var mentee	=	$("#myCarousel1").find('div.active').attr('id');
	//alert(mentor);alert(mentee);
	$.ajax({
		url: '<?php echo base_url();?>admin/relationship/addRelation',
		data:"mentor="+mentor+"&mentee="+mentee,
		success: function(result){
			//alert(result);
		}
	});	
});
</script>