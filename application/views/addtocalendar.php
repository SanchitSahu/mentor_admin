<?php

//echo "<pre>";print_r($MeetingDetail);echo "</pre>";
?>

<html>
    <head>

    </head>
    <body>
        <style type="text/css">
            .thankyou_page .fa{
                font-size: 40px;
            }
            .thankyou_page .panel label, .thankyou_page .panel span{
                padding: 10px 0px;
            }
        </style>

        <link href="<?php echo base_url('assets/admin/css/bootstrap-3.0.0.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/admin/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" type="text/css">
        <link href="http://addtocalendar.com/atc/1.5/atc-style-blue.css" rel="stylesheet" type="text/css">
        <section id="main-content">
            <section class="wrapper">
                <!-- page start-->
                <div class=" container-fluid">
                    <div class="wrapper lg-pad">
                        <div class="container thankyou_page">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <img src="<?php echo base_url('assets/admin/images/default-logo.png'); ?>"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <h1 style="text-align: center;">Just 1 step to get ready for the meeting...</h1>
                                    <h3 style="text-align: center;">Add this meeting to your calendar by clicking the "Add to Calendar" button on the right.</h3>
                                </div>
                                <div class="col-md-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Add it to your Calendar!</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <label>Date:</label>
                                                    <span><?php echo date('jS F Y', strtotime($MeetingDetail->MeetingDatetime)); ?></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <label>Start Time:</label>
                                                    <span><?php echo date('H:i', strtotime($MeetingDetail->MeetingDatetime)); ?></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <label>Meeting With:</label>
                                                    <?php if($UserType == 'mentor') { ?>
                                                        <span><?php echo ucfirst($MeetingDetail->MenteeName); ?></span>
                                                    <?php } else { ?>
                                                        <span><?php echo ucfirst($MeetingDetail->MentorName); ?></span>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <label>Meeting Topic:</label>
                                                    <span><?php echo $MeetingDetail->Topicdescription; ?></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12" style="text-align: center;">
                                                    <span class="addtocalendar atc-style-blue">
                                                        <var class="atc_event">
                                                            <var class="atc_date_start"><?php echo date('Y-m-d H:i:s', strtotime($MeetingDetail->MeetingDatetime)); ?></var>
                                                            <var class="atc_date_end"><?php echo date('Y-m-d H:i:s', strtotime($MeetingDetail->MeetingDatetime . "+30 minutes")); ?></var>
                                                            <!--<var class="atc_timezone">America/New_York</var>-->
                                                            <var class="atc_timezone">UTC</var>
                                                            <?php if($UserType == 'mentor') { ?>
                                                                <var class="atc_title">Meeting with <?php echo ucfirst($MeetingDetail->MenteeName); ?> to discuss <?php echo $MeetingDetail->Topicdescription; ?></var>
                                                            <?php } else { ?>
                                                                <var class="atc_title">Meeting with <?php echo ucfirst($MeetingDetail->MentorName); ?> to discuss <?php echo $MeetingDetail->Topicdescription; ?></var>
                                                            <?php } ?>
                                                            <var class="atc_location"><?php echo $MeetingDetail->MeetingTypeName . ' at ' . $MeetingDetail->MeetingPlaceName; ?></var>
                                                        </var>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <script type="text/javascript">(function () {
                if (window.addtocalendar)
                    if (typeof window.addtocalendar.start == "function")
                        return;
                if (window.ifaddtocalendar == undefined) {
                    window.ifaddtocalendar = 1;
                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                    s.type = 'text/javascript';
                    s.charset = 'UTF-8';
                    s.async = true;
                    s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://addtocalendar.com/atc/1.5/atc.min.js';
                    var h = d[g]('body')[0];
                    h.appendChild(s);
                }
            })();
        </script>
    </body>
</html>