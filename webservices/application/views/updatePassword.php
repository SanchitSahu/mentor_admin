<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
		<link rel="shortcut icon" type="image/jpg" href="<?php echo $this->config->item('base_images');?>/logo.png"/>

        <title>MELS Admin Sign In</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo $this->config->item('base_assets'); ?>bs3/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $this->config->item('base_css'); ?>bootstrap-reset.css" rel="stylesheet">
        <!--external css-->
        <link href="<?php echo $this->config->item('base_fonts'); ?>css/font-awesome.css" rel="stylesheet" />
        <!-- Custom styles for this template -->
        <link href="<?php echo $this->config->item('base_css'); ?>style.css" rel="stylesheet">
        <link href="<?php echo $this->config->item('base_css'); ?>style-responsive.css" rel="stylesheet" />
        <link href="<?php echo $this->config->item('base_css'); ?>jquery.fancybox.css" rel="Stylesheet" />
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.mousewheel.pack.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.fancybox.pack.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('base_js'); ?>jquery.validate.min.js"></script>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            $(document).ready(function () {
                $('.fancybox').fancybox({
                    autoSize: true,
                    width: "auto",
                    height: "80%"
                });
                
                $(".close-box").click(function (){
                    $.fancybox.close();
                });
                
            });
        </script>
		<style>
.page_center {
    margin: 120px auto 50px;
    width: 80%;
}
.page_center h2 {
    color: #fff;
    font-size: 46px;
    margin: 0 0 20px;
}
a.link_btn img{
	margin: 5px 5px 5px 5px;
}
</style>
    </head>
	
<?php
	if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on"){$ssl_set = "s";} else{$ssl_set = "";} 
	$base_url = 'http'.$ssl_set.'://'.$_SERVER['HTTP_HOST']."/mentor/";
?>
	
				
<body class="lock-screen">


<?php if(isset($id)){ ?>
	<!-- Wrapper/Container Table: Use a wrapper table to control the width and the background color consistently of your email. Use this approach instead of setting attributes on the body tag. -->
	<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" class='bgBody'>
	<tr>
		<td>
	<table cellpadding="0" class="container" align="center" cellspacing="0" border="0">
	<tr>
		<td>
		<!-- Tables are the most common way to format your email consistently. Set your table widths inside cells and in most cases reset cellpadding, cellspacing, and border to zero. Use nested tables as a way to space effectively in your message. -->
		

		<div class="panel-body">
			<div class="contentEditableContainer contentImageEditable" style="margin-bottom:20px;">
				<div class="contentEditable" align='center' >
					<img src="<?php echo $base_url.'assets/admin/images/logo-64x64.png'; ?>"alt='Logo'  data-default="placeholder" />
				</div>
			</div>
			<div class="form">
				<?php
				$action = base_url()."index.php/mentorService/updateMentorpassword";
				echo form_open($action, array('class' => 'cmxform form-horizontal', 'id' => "add_school_form", 'method' => "post"));
				?>
				<div class="form-group col-lg-12">
					<label for="subdomainName" class="control-label col-lg-6">Password</label>
					<div class="col-lg-6">
						<input type="password" name="password" id="password">
						<span id="pwdErr"></span>
					</div>
					<?php echo form_error('subdomainName'); ?>
				</div>
				
				<div class="form-group col-lg-12">
					<label for="Weight" class="control-label col-lg-6">Confirm Password</label>
					<div class="col-lg-6">
						<input type="password" name="cpassword" id="cpassword">
						<span id="cpwdErr"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-offset-6 col-lg-12">
						<input type="hidden" name="mainId" id="mainId" value="<?php echo $id; ?>">
						<input type="hidden" name="UserType" id="UserType" value="<?php echo $UserType; ?>">
						<input class="btn btn-3d-success" type="submit" name="submit" value="Set" onclick="return checkPassword();"></td>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
		<?php /*				
		<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
			<tr>
				<td class='movableContentContainer bgItem'>

					<div class='movableContent'>
						<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
							<tr height="40">
								<td width="200">&nbsp;</td>
								<td width="200">&nbsp;</td>
								<td width="200">&nbsp;</td>
							</tr>
							<tr>
								<td width="200" valign="top">&nbsp;</td>
								<td width="200" valign="top" align="center">
									<div class="contentEditableContainer contentImageEditable">
					                	<div class="contentEditable" align='center' >
					                  		<img src="<?php echo base_url().'assets/admin/images/logo-64x64.png'; ?>"alt='Logo'  data-default="placeholder" />
					                	</div>
					              	</div>
								</td>
								<td width="200" valign="top">&nbsp;</td>
							</tr>
							<tr height="25">
								<td width="200">&nbsp;</td>
								<td width="200">&nbsp;</td>
								<td width="200">&nbsp;</td>
							</tr>
						</table>
					</div>

					<div class='movableContent'>
					
						<form action="<?php echo $base_url?>webservices/index.php/mentorService/updateMentorpassword" method="post">
						<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
							<tr>
								<td width="200">Password</td>
								<td width="200" align="center" style="padding-top:25px;">
									<input type="password" name="password" id="password">
									<span id="pwdErr"></span>
								</td>
							</tr>
							<tr>
								<td width="200">Confirm Password</td>
								<td width="200" align="center" style="padding-top:25px;">
									<input type="password" name="cpassword" id="cpassword">
									<span id="cpwdErr"></span>
								</td>
							</tr>
							<tr>
								<td colspan="2">
								<input type="hidden" name="mainId" id="mainId" value="<?php echo $id; ?>">
								<input type="hidden" name="UserType" id="UserType" value="<?php echo $UserType; ?>">
								<input type="submit" name="submit" value="Set" onclick="return checkPassword();"></td>
							</tr>
						</table>
                        </form>
					</div>
				</td>
			</tr>
		</table>

		*/ ?>
		

	</td></tr></table>
	
		</td>
	</tr>
	</table>
	<!-- End of wrapper table -->

<!--Default Zone End-->
<?php }else{ ?>
	<div class="contentEditableContainer contentImageEditable" style="margin-bottom:20px;">
		<div class="page_center contentEditable" align='center' >
			<h2>Password changed successfully</h2>
				<a href="https://play.google.com/store/apps/details?id=com.melstm" class="link_btn">
					<img src="http://melstm.net/mentor/assets/admin/images/playstore_btn.png">
				</a>
				<a href="https://itunes.apple.com/us/app/melstm/id1070132202?ls=1&mt=8" class="link_btn">
					<img src="http://melstm.net/mentor/assets/admin/images/appstore_btn.png">
				</a> 
		</div>
	</div>
<?php } ?>
<script>
	$(document).ready(function(){
		<?php if(!isset($id)) { ?>
			var metaTag=$('meta')[0];
			$(metaTag).after('<meta name="apple-itunes-app" content="app-id=1070132202">');
			var isMobileDevice = isMobile();
			if(isMobileDevice == true) {
				setTimeout(function(){
					var MobileDevice = getMobileOperatingSystem();
					if(MobileDevice == "Android"){
						window.location.href = 'http://melstm.net/mentor/webservices/index.php/mentorService/launchApp/' + MobileDevice;
					}
				},100);
			} else {
				//alert(isMobileDevice);
			}
		<?php } ?>
	});
	
	function isMobile() {
	 try {
		if(/Android|webOS|iPhone|iPad|iPod|pocket|psp|kindle|avantgo|blazer|midori|Tablet|Palm|maemo|plucker|phone|BlackBerry|symbian|IEMobile|mobile|ZuneWP7|Windows Phone|Opera Mini/i.test(navigator.userAgent)) {
			//alert(true);
		 return true;
		};
		return false;
	 } catch(e){ console.log("Error in isMobile"); return false; }
	}
	
	
	function getMobileOperatingSystem() {
		var userAgent = navigator.userAgent || navigator.vendor || window.opera;
		if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) ) {
			return 'iOS';
		} else if(userAgent.match(/Android/i)) {
			return 'Android';
		} else {
			return 'unknown';
		}
	}
function checkPassword()
{
	var	 pwd	=	document.getElementById("password").value;
	var  len	=	document.getElementById("password").value.length;	
	
	var	 cpwd	=	document.getElementById("cpassword").value;
	var  clen	=	document.getElementById("cpassword").value.length;
	if(pwd	==	""){
		document.getElementById("pwdErr").textContent="Please Enter Password";
		return false;
	}else if(len <= 6){
		document.getElementById("pwdErr").textContent="Length must be more then 6 character";
		return false;
	}else{
		if(cpwd	==	""){
			document.getElementById("cpwdErr").textContent="Please Enter Confirm Password";
			return false;
		}else if(len <= 6){
			document.getElementById("cpwdErr").textContent="Length must be more then 6 character";
			return false;
		}else if(pwd != cpwd){
			document.getElementById("cpwdErr").textContent="Password does not match";
			return false;
		}else{
			document.getElementById("cpwdErr").textContent="";
			return true;
		}		
	}

	
}
</script>
</body>
</html>