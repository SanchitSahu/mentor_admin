<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mentor_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getSchoolList() {
        //$this->db->select("subdomain_id, subdomain_name, concat(UCASE(LEFT(subdomain_name, '1')),SUBSTRING(subdomain_name, '2'))");
        //$query = $this->db->get('mentor_subdomains');
        $query = $this->db->query("
			SELECT subdomain_id, subdomain_name, concat(UCASE(LEFT(subdomain_name, 1)),SUBSTRING(subdomain_name, 2)) as school_name
			FROM (mentor_subdomains)
		");
        //echo $this->db->last_query();
        return $query->result();
    }

    function checkSubdomain($domainName) {
        $query = $this->db->query("SELECT * FROM mentor_subdomains WHERE subdomain_name = '" . $domainName . "'");

        return $query->result();
    }

    public function getNewMeetingDetails($mentorId) {
        $query = $this->db->query("SELECT mi.MenteeId,mt.TopicDescription,mi.ManteeComments FROM mentor_invitation mi
            LEFT JOIN mentor_topic mt ON mi.TopicID=mt.TopicID
            WHERE mi.MentorID=$mentorId");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getMentorActionTakenDetail() {
        //$sql = "call  usp_GetMentorActionTaken()";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $query = $this->db->query('SELECT t.MentorActionID, t.MentorActionName FROM mentor_mentordefinedactions t');
        return $query->result();
    }

    public function registerUserDetail($Email, $AccessToken, $UserType) {
        $sql = "call  usp_RegisterUser('" . $Email . "','" . $AccessToken . "','" . $UserType . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }

    public function checkLoginDetail($username, $accesstoken) {
        //$sql = "call usp_CheckLogin('" . $username . "','" . $accesstoken . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $userType = '';
        if ($this->checkMentorExist('', $username, '', FALSE)) {
            $userType = 'Mentor';
        } else if ($this->checkMenteeExist('', $username, '', FALSE)) {
            $userType = 'Mentee';
        }

        if (empty($userType)) {
            $return_result = array((object) array('Message' => "Username and/or password and/or school do not match. Please try again."));
        } else {
            if ($userType == 'Mentor') {
                $result = $this->db->query("
                    SELECT m.MentorID AS Id, m.MentorName, mci.MentorPhone, mci.MentorEmail, mci.MentorStatus, 0 AS UserType,
                    IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_mentorskillset ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MentorID = m.MentorID),'') AS Skills,
                    mci.Password,mci.DisableSMS,CONCAT('http://melstm.net/mentor/assets/admin/images/admin/', mci.MentorImage) AS MentorImage
                    FROM mentor_mentor m 
                    LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorID = m.MentorID
                    WHERE m.MentorName = '$username' AND mci.Password = '$accesstoken' AND mci.MentorStatus = 'Active'
                ")->result();
                if (count($result) > 0) {
                    $return_result = $result;
                } else {
                    $return_result = array((object) array('Message' => "Username and/or password and/or school do not match. Please try again."));
                }
            } else if ($userType == 'Mentee') {
                $result = $this->db->query("
                    SELECT m.MenteeID AS Id, m.MenteeName, mc.MenteePhone, mc.MenteeEmail, mc.MenteeStatus, 1 AS UserType,
                    IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_menteeskill ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MenteeID = m.MenteeID),'') AS Skills,
                    mc.Password,mc.DisableSMS,CONCAT('http://melstm.net/mentor/assets/admin/images/admin/', mc.MenteeImage) AS MenteeImage
                    FROM mentor_mentee m
                    LEFT JOIN mentor_menteecontact mc ON mc.MenteeId = m.MenteeID
                    WHERE m.MenteeName = '$username' AND mc.Password = '$accesstoken' AND mc.MenteeStatus = 'Active';
                ")->result();

                if (count($result) > 0) {
                    $return_result = $result;
                } else {
                    $return_result = array((object) array('Message' => "Username and/or password and/or school do not match. Please try again."));
                }
            }
        }
        return $return_result;
    }

    function forgotPasswordDetail($Email) {
        //$sql = "call usp_ForgotPassword('" . $Email . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $userType = '';
        $return_result = '';
        if ($this->checkMentorExist('', '', $Email, FALSE)) {
            $userType = 'Mentor';
        } else if ($this->checkMenteeExist('', '', $Email, FALSE)) {
            $userType = 'Mentee';
        }

        if ($userType != '') {
            if ($userType == 'Mentor') {
                $query = $this->db->query("SELECT mci.MentorContactInfoID FROM mentor_mentorcontactinfo mci WHERE mci.MentorEmail = '$Email' AND mci.MentorStatus = 'Active'");
                if ($query->num_rows() > 0) {
                    $return_result = $this->db->query("
                        SELECT m.MentorID AS Id, m.MentorName, mci.MentorPhone, mci.Password, mci.MentorEmail, mci.MentorStatus, 0 AS UserType
                        FROM mentor_mentor m 
                        LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorID = m.MentorID
                        WHERE mci.MentorEmail = '$Email'
                    ")->result();
                } else {
                    $return_result = array((object) array('Message' => "Email is not active please use the access token provided earlier to register first."));
                }
            } else {
                $query = $this->db->query("SELECT mc.MenteeContactID FROM mentor_menteecontact mc WHERE mc.MenteeEmail = '$Email'");
                if ($query->num_rows() > 0) {
                    $return_result = $this->db->query("
                        SELECT m.MenteeID AS Id, m.MenteeName, mc.MenteePhone, mc.Password, mc.MenteeEmail, mc.MenteeStatus, 1 AS UserType
                        FROM mentor_mentee m 
                        LEFT JOIN mentor_menteecontact mc ON mc.MenteeId = m.MenteeID
                        WHERE mc.MenteeEmail = '$Email'
                    ")->result();
                } else {
                    $return_result = array((object) array('Message' => "Email is not active please use the access token provided earlier to register first."));
                }
            }
        } else {
            $return_result = array((object) array('Message' => "Email not found."));
        }
        return $return_result;
    }

    /* 21-8-2015 */

    function checkInvitationDetail($MentorID, $MenteeID) {
        //$sql = "call usp_checkInvitationDetail('" . $MentorID . "','" . $MenteeID . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $query = $this->db->query("
            SELECT mentor.MentorEmail,m.MeetingID,mmentor.MentorName,mmantee.MenteeName,mantee.MenteeEmail,mt.TopicDescription FROM mentor_meetinginfo m
            LEFT JOIN mentor_menteecontact mantee ON m.MentorID = mantee.MenteeId
            LEFT JOIN mentor_mentorcontactinfo mentor ON mentor.MentorID = m.MentorID
            LEFT JOIN mentor_mentee mmantee ON mmantee.MenteeID = m.MenteeID
            LEFT JOIN mentor_mentor mmentor ON mmentor.MentorID = m.MentorID
            LEFT JOIN mentor_topic mt ON mt.TopicID = m.MeetingTopicID
            WHERE m.MentorID = '$MentorID' AND m.MenteeID = '$MenteeID'
        ");
        return $query->result();
    }

    /**/

    function changePasswordDetail($Email, $OldPassword, $Password, $usertype) {
        $oldPwd = (int) $OldPassword;
        $sql = "call usp_ChangePassword('" . $Email . "','" . $oldPwd . "','" . $Password . "','" . $usertype . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        //print_r($query->result());exit;
        return $query->result();
    }

    function submitMeetingInfoDetail($MeetingID, $MentorID, $MenteeID, $MeetingStartDatetime, $MeetingEndDatetime, $MeetingTypeID, $MeetingTopicID, $MeetingSubTopicID, $MeetingPlaceID, $MenteeActionIDs, $MeetingElapsedTime, $MeetingFeedback, $MentorActionItemDone, $MenteeActionItemDone) {
//        $sql = "call usp_SubmitMeetingInfo('" . $MeetingID . "','" . $MentorID . "','" . $MenteeID . "','" . $MeetingStartDatetime . "','" . $MeetingEndDatetime . "','" . $MeetingTypeID . "','" . $MeetingTopicID . "','" . $MeetingSubTopicID . "','" . $MeetingPlaceID . "','" . $MenteeActionIDs . "','" . $MeetingElapsedTime . "','" . $MeetingFeedback . "','" . $MentorActionItemDone . "','" . $MenteeActionItemDone . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();


        $data = array(
            'MentorID' => $MentorID,
            'MenteeID' => $MenteeID,
            'MeetingStartDatetime' => $MeetingStartDatetime,
            'MeetingEndDatetime' => $MeetingEndDatetime,
            'MeetingTypeID' => $MeetingTypeID,
            'MeetingTopicID' => $MeetingTopicID,
            'MeetingSubTopicID' => $MeetingSubTopicID,
            'MeetingPlaceID' => $MeetingPlaceID,
            'MeetingElapsedTime' => $MeetingElapsedTime,
            'MeetingFeedback' => $MeetingFeedback
        );

        if ($MeetingID == 0) {
            if ($this->db->insert('mentor_meetinginfo', $data)) {
                return array((object) array('InsertedId' => $this->db->insert_id()));
            } else {
                return array((object) array('Message' => 'There is issue in inserting meeting'));
            }
        } else {
            $data['MentorActionItemDone'] = $MentorActionItemDone;
            $data['MenteeActionItemDone'] = $MenteeActionItemDone;
            $data['Meeting_status'] = '3';
            $this->db->where('MeetingID', $MeetingID);
            if ($this->db->update('mentor_meetinginfo', $data)) {
                return array((object) array('InsertedId' => $MeetingID));
            } else {
                return array((object) array('Message' => 'There is issue in updating meeting'));
            }
        }
    }

    function addMenteeactionstakenDetail($MenteeID, $MeetingID, $ActionId) {
        //$sql = "call usp_AddMenteeactionstaken('" . $MenteeID . "','" . $MeetingID . "','" . $ActionId . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $data = array(
            'MenteeID' => $MenteeID,
            'MeetingID' => $MeetingID,
            'MenteeActionID' => $ActionId
        );
        $this->db->insert('mentor_menteeactionstaken', $data);
        $insert_id = $this->db->insert_id();
    }

    function addMentoractionstakenDetail($MentorID, $MeetingID, $ActionId) {
        //$sql = "call usp_AddMentoractionstaken('" . $MentorID . "','" . $MeetingID . "','" . $ActionId . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $data = array(
            'MentorID' => $MentorID,
            'MeetingID' => $MeetingID,
            'MentorActionID' => $ActionId
        );
        $this->db->insert('mentor_mentoractionstaken', $data);
        return $this->db->insert_id();
    }

    /* added by khushbu 18-08-2015 */

    function addMentorsubtopicsIDs($MentorID, $MeetingID, $ActionId) {
        //$sql = "call usp_AddMentorsubtopicsIDs('" . $MentorID . "','" . $MeetingID . "','" . $ActionId . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $data = array(
            'MentorID' => $MentorID,
            'MeetingID' => $MeetingID,
            'MentorSubTopicsID' => $ActionId
        );
        $this->db->insert('mentor_subTopicsIDs', $data);
        return $this->db->insert_id();
    }

    function getMentorsubtopicsIDs($MeetingID) {
        $this->db->select('*');
        $this->db->from('mentor_subTopicsIDs');
        $this->db->where('MeetingID', $MeetingID);
        mysqli_next_result($this->db->conn_id);
        $mentorData = $this->db->get();

        return $mentorData->result();
    }

    function updateMentorsubtopicsIDs($mentorSubIdResultsIds, $MeetingID) {
//        $sql = "call usp_updateMentorsubtopicsIDs('" . $mentorSubIdResultsIds . "', " . $MeetingID . ")";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();

        $data = array(
            'MeetingSubTopicID' => $mentorSubIdResultsIds
        );
        $this->db->where('MeetingID', $MeetingID);
        $this->db->update('mentor_meetinginfo', $data);
        return true;
    }

    /* khushbu */

    public function getMentorDetail($MenteeID) {
//        $sql = "call usp_GetMentor('" . $MenteeID . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        return $query->result();

        $query = $this->db->query("
            SELECT m.MenteeID,m.MenteeName, r.*, mentor.* 
            FROM (mentor_mentee m) 
            LEFT JOIN mentor_relationship r ON r.MenteeID= m.MenteeID 
            LEFT JOIN mentor_mentor mentor ON mentor.MentorID = r.MentorID 
            WHERE m.MenteeID =$MenteeID AND m.Status = 1
            ORDER BY mentor.MentorName ASC
        ");
        return $query->result();
    }

    function mentorCommentDetail($MentorID, $MenteeID, $MeetingID, $MentorComment, $SatisfactionIndex) {
        //$sql = "call usp_AddMentorComment('" . $MentorID . "','" . $MenteeID . "','" . $MeetingID . "','" . $MentorComment . "','" . $SatisfactionIndex . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $query = $this->db->query("(SELECT MentorCommentID FROM mentor_mentorcomment  WHERE MentorID = $MentorID AND MeetingID = $MeetingID)");
        if ($query->num_rows() > 0) {
            $data = array(
                'MentorID' => $MentorID,
                'MenteeID' => $MenteeID,
                'MeetingID' => $MeetingID,
                'MentorComment' => $MentorComment,
                'SatisfactionIndex' => $SatisfactionIndex
            );
            $this->db->where('MentorID', $MentorID);
            $this->db->where('MeetingID', $MeetingID);
            $this->db->update('mentor_mentorcomment', $data);
            $response = array((object) array(
                    'InsertedId' => 1
                )
            );
        } else {
            $data = array(
                'MentorID' => $MentorID,
                'MenteeID' => $MenteeID,
                'MeetingID' => $MeetingID,
                'MentorComment' => $MentorComment,
                'SatisfactionIndex' => $SatisfactionIndex
            );
            $insert_id = $this->db->insert('mentor_mentorcomment', $data);
            $response = array((object) array(
                    'InsertedId' => $insert_id
                )
            );
        }
        return $response;
    }

    /* khushbu 19-8 */

    function mentorInvitationDetail($MentorID, $MenteeID, $TopicID, $MenteeComments, $MenteeEmail, $MenteePhone, $SmsCapable) {
//        $sql = "call usp_MentorInvitation('" . $MentorID . "','" . $MenteeID . "'," . $TopicID . ",'" . $MenteeComments . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();

        $query = $this->db->query("SELECT i.InvitationId FROM mentor_invitation i WHERE i.MentorID = $MentorID AND i.MenteeID = $MenteeID AND i.Status!='Rejected'");
        if ($query->num_rows() > 0) {
            $response = array((object) array(
                    'Message' => 'Invitation already sent.'
                )
            );
        } else {
            $data = array(
                'MentorID' => $MentorID,
                'MenteeID' => $MenteeID,
                'TopicID' => $TopicID,
                'ManteeComments' => $MenteeComments,
                'inviteTime' => date('Y-m-d H:i:s'),
                'MenteeEmail' => $MenteeEmail,
                'MenteePhone' => $MenteePhone,
                'SmsCapable' => $SmsCapable
            );
            $insert_id = $this->db->insert('mentor_invitation', $data);
            $response = array((object) array(
                    'InsertedId' => $insert_id
                )
            );
        }
        return $response;
    }

    function getMentorForInvitation() {
        $this->db->select('*');
        $this->db->from('mentor_mentor');
        mysqli_next_result($this->db->conn_id);
        $mentorData = $this->db->get();

        return $mentorData->result();
    }

    function getMentorForInvitationStatus($MentorID, $MenteeID) {
        //$sql = "call usp_getMentorForInvitation('" . $MentorID . "','" . $MenteeID . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();


        $query = $this->db->query("SELECT i.InvitationId FROM mentor_invitation i WHERE i.MentorID = $MentorID AND i.MenteeID = $MenteeID AND `Status` != 'Rejected'");
        if ($query->num_rows() > 0) {
//            $query1 = $this->db->query("SELECT m.MentorID, m.MentorName,'TRUE' as rStatus FROM mentor_mentor m WHERE m.MentorID = $MentorID");
            return FALSE;
        } else {
            $query1 = $this->db->query("SELECT m.MentorID, m.MentorName,'FALSE' as rStatus FROM mentor_mentor m WHERE m.MentorID = $MentorID");
        }
        return $query1->result();
    }

    function startEndMeetingSession($MenteeID, $MentorID, $DateTime) {
        $this->db->select('*');
        $this->db->from('mentor_meetinginfo');
        $this->db->where('MenteeID', $MenteeID);
        $this->db->where('MentorID', $MentorID);
        mysqli_next_result($this->db->conn_id);
        $mentorData = $this->db->get();

        return $mentorData->num_rows();
    }

    function addStartMeetingSession($MenteeID, $MentorID, $DateTime, $MettingStatus, $MeetingTypeID, $MeetingPlaceID, $MainTopicID) {
        //$sql = "call usp_addStartEndMeetingSession('" . $MentorID . "','" . $MenteeID . "','" . $DateTime . "','" . $MettingStatus . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $query = $this->db->query("SELECT ManteeComments FROM `mentor_invitation` WHERE MentorID=$MentorID AND MenteeID=$MenteeID  AND STATUS='Accepted'");
        if ($query->num_rows() > 0) {
            $result = $query->result();
            $data = array(
                'MentorID' => $MentorID,
                'MenteeID' => $MenteeID,
                'MeetingStartDatetime' => $DateTime,
                'Meeting_status' => $MettingStatus,
                'RawStartDatetime' => $DateTime,
                'MenteeInvitationComment' => $result[0]->ManteeComments,
                'MeetingTypeID' => $MeetingTypeID,
                'MeetingPlaceID' => $MeetingPlaceID,
                'MeetingTopicID' => $MainTopicID
            );
            $this->db->insert('mentor_meetinginfo', $data);
            $insert_id = $this->db->insert_id();

            $this->db->where('MentorID', $MentorID);
            $this->db->where('MenteeID', $MenteeID);
            $this->db->delete('mentor_invitation');
            $response = array((object) array('LastId' => "$insert_id"));
        } else {
            $response = array((object) array('LastId' => '0'));
        }
        return $response;
    }

    /* khushbu 19-8 */
    /* khushbu 20-8 */

    function updateStartMeetingSession($DateTime, $MettingStatus, $MentorID, $MenteeID, $MeetingTypeID, $MeetingPlaceID, $MainTopicID) {
//        $sql = "call usp_updateStartEndMeetingSession('" . $DateTime . "','" . $MettingStatus . "','" . $MentorID . "','" . $MenteeID . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();


        $data = array(
            'MeetingStartDatetime' => $DateTime,
            'Meeting_status' => $MettingStatus,
            'MeetingTypeID' => $MeetingTypeID,
            'MeetingPlaceID' => $MeetingPlaceID,
            'MeetingTopicID' => $MainTopicID
        );
        $this->db->where('MenteeID', $MenteeID);
        $this->db->where('MentorID', $MentorID);
        $this->db->where('Meeting_status !=', '3');
        $this->db->update('mentor_meetinginfo', $data);
        return true;
    }

    function updateEndMeetingSession($DateTime, $MettingStatus, $MentorID, $MenteeID, $ElapsedTime) {
//        $sql = "call usp_updateEndMeetingSession('" . $DateTime . "','" . $MettingStatus . "','" . $MentorID . "','" . $MenteeID . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();

        $data = array(
            'MeetingEndDatetime' => $DateTime,
            'Meeting_status' => $MettingStatus,
            'RawEndDatetime' => $DateTime,
            'MeetingElapsedTime' => $ElapsedTime
        );
        $this->db->where('MenteeID', $MenteeID);
        $this->db->where('MentorID', $MentorID);
        $this->db->where('Meeting_status !=', '3');
        $this->db->update('mentor_meetinginfo', $data);
        return true;
    }

    function getMeetingSessionIDforSubmit($MenteeID, $MentorID, $MeetingID) {

        if ($MeetingID == 0) {
            $this->db->select('*');
            $this->db->from('mentor_meetinginfo');
            $this->db->where('MenteeID', $MenteeID);
            $this->db->where('MentorID', $MentorID);
            $this->db->where('Meeting_status !=', '3');
        } else {
            $this->db->select('*');
            $this->db->from('mentor_meetinginfo');
            $this->db->where('MeetingID', $MeetingID);
        }

        mysqli_next_result($this->db->conn_id);
        $mentorData = $this->db->get();


        //echo $this->db->last_query();exit;    
        return $mentorData->result();
    }

    function getMeetingSessionID($MenteeID, $MentorID) {
        //echo $MenteeID;echo $MentorID;exit;
        $this->db->select('*');
        $this->db->from('mentor_meetinginfo');
        $this->db->where('MenteeID', $MenteeID);
        $this->db->where('MentorID', $MentorID);
        $this->db->where('Meeting_status !=', '3');
        mysqli_next_result($this->db->conn_id);
        $mentorData = $this->db->get();

        //echo $this->db->last_query();exit;    
        return $mentorData->result();
    }

    function getMentorWaitScreen($MenteeID) {
        $this->db->select('*');
        $this->db->from('mentor_invitation');
        $this->db->where('MenteeID', $MenteeID);
        $this->db->order_by("inviteTime", "desc");
        mysqli_next_result($this->db->conn_id);
        $mentorData = $this->db->get();


        //echo $this->db->last_query();exit;    
        return $mentorData->result();
    }

    function getMentorWaitScreenDetails($MentorID, $MenteeID, $InvitationId) {
//        $sql = "call usp_getMentorWaitScreen('" . $MentorID . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->row_array();

        $query = $this->db->query("
            SELECT i.InvitationId, i.Status, i.inviteTime, i.MentorComments, t.TopicDescription, CONCAT('http://melstm.net/mentor/assets/admin/images/admin/', mci.MentorImage) AS MentorImage,
            IFNULL(i.MenteeID, '') AS MenteeID, IFNULL(i.MentorID, '') AS MentorID, IFNULL(m.MentorName, '') AS MentorName,
            i.MenteeEmail, i.MenteePhone, i.SmsCapable, i.ManteeComments AS MenteeComment,mt.MeetingTypeName,mp.MeetingPlaceName,i.MeetingDateTime
            FROM mentor_invitation i
            LEFT JOIN mentor_mentor m ON m.MentorID = i.MentorID
            LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorId = i.MentorId
            LEFT JOIN mentor_topic t ON t.TopicID = i.TopicID
            LEFT JOIN mentor_meetingplace mp ON mp.MeetingPlaceID = i.MeetingPlaceID
            LEFT JOIN mentor_meetingtype mt ON mt.MeetingTypeID = i.MeetingTypeID
            WHERE i.MentorID = $MentorID AND i.MenteeID = $MenteeID AND i.InvitationId = $InvitationId
        ");
        return $query->row_array();
    }

    function getMettingWaitScreenInfo($MentorID, $MenteeID) {
//        $sql = "call usp_getMettingWaitScreenInfo('" . $MentorID . "','" . $MenteeID . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();


        $query = $this->db->query("
            SELECT i.Meeting_status ,m.Status
            FROM mentor_meetinginfo i
            LEFT JOIN mentor_invitation m ON m.MentorID = i.MentorID AND m.MenteeID = i.MenteeID
            WHERE i.MentorID =  $MentorID AND i.MenteeID = $MenteeID
        ");
        return $query->result();
    }

    /* 20-8 */

    function getMentorPendingRequestDetail($MentorID) {
        //$sql = "call usp_GetMentorPendingRequest('" . $MentorID . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $query = $this->db->query("SELECT i.InvitationId FROM mentor_invitation i WHERE i.MentorID = $MentorID AND i.Status = 'Pending'");
        if ($query->num_rows()) {
            $query = $this->db->query("
                SELECT i.InvitationId,i.ManteeComments,mt.TopicDescription, IFNULL(i.MenteeID,'') AS MenteeID, IFNULL(me.MenteeName,'') AS MenteeName,
                (CONCAT('http://melstm.net/mentor/assets/admin/images/admin/',mci.MenteeImage)) AS MenteeImage, i.inviteTime,i.MenteeEmail,i.MenteePhone,i.SmsCapable
                FROM mentor_invitation i 
                LEFT JOIN mentor_mentee me ON me.MenteeID = i.MenteeID
                LEFT JOIN mentor_menteecontact mci ON mci.MenteeID = me.MenteeID
                LEFT JOIN mentor_topic mt ON mt.TopicID	= i.TopicID
                WHERE i.MentorID = $MentorID AND i.Status = 'Pending'
            ");
            $response = $query->result();
        } else {
            $response = array((object) array(
                    'Message' => 'No invitation found.'
                )
            );
        }
        return $response;
    }

    function getMentorPendingMeetingDetail($MentorID) {

        $query = $this->db->query("SELECT i.InvitationId FROM mentor_invitation i WHERE i.MentorID = $MentorID AND i.Status = 'Accepted'");
        if ($query->num_rows()) {
            $query = $this->db->query("
                SELECT i.InvitationId,i.ManteeComments,i.TopicID,mt.TopicDescription, IFNULL(i.MenteeID,'') AS MenteeID, IFNULL(me.MenteeName,'') AS MenteeName,
                (CONCAT('http://melstm.net/mentor/assets/admin/images/admin/',mci.MenteeImage)) AS MenteeImage, i.inviteTime,i.MenteeEmail,i.MenteePhone,i.SmsCapable,i.MentorComments,i.MeetingPlaceID,IFNULL(mp.MeetingPlaceName,'') as MeetingPlaceName,i.MeetingDateTime,i.MeetingTypeID,IFNULL(mtype.MeetingTypeName,'') as MeetingTypeName
                FROM mentor_invitation i 
                LEFT JOIN mentor_mentee me ON me.MenteeID = i.MenteeID
                LEFT JOIN mentor_menteecontact mci ON mci.MenteeID = me.MenteeID
                LEFT JOIN mentor_topic mt ON mt.TopicID	= i.TopicID
				LEFT JOIN mentor_meetingplace mp ON mp.MeetingPlaceID = i.MeetingPlaceID
				LEFT JOIN mentor_meetingtype mtype ON mtype.MeetingTypeID = i.MeetingTypeID
                WHERE i.MentorID = $MentorID AND i.Status = 'Accepted'
            ");
            $response = $query->result();
        } else {
            $response = array((object) array(
                    'Message' => 'No Pending Meetings found'
                )
            );
        }
        return $response;
    }

    function mentorResponseDetail($details) {
//        $sql = "call usp_MentorResponse('" . $InvitationId . "','" . $Status . "','" . $Comments . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();

        $query = $this->db->query("SELECT COUNT(i.InvitationId) FROM mentor_invitation i WHERE i.InvitationId = {$details->InvitationId}");
        if ($query->num_rows() > 0) {
            $data = array(
                'Status' => $details->Status,
                'AcceptOrRejectTime' => date('Y-m-d H:i:s'),
                'MentorComments' => $details->Comments
            );
            if (isset($details->MeetingPlaceID))
                $data['MeetingPlaceID'] = $details->MeetingPlaceID;
            if (isset($details->MeetingDateTime))
                $data['MeetingDateTime'] = $details->MeetingDateTime;
			if (isset($details->MeetingTypeID))
                $data['MeetingTypeID'] = $details->MeetingTypeID;

            $this->db->where('InvitationId', $details->InvitationId);
            $this->db->update('mentor_invitation', $data);

            $invitation = $this->db->query("SELECT MentorID,MenteeID FROM mentor_invitation WHERE InvitationId = {$details->InvitationId}")->result();

            $relationship = $this->db->query("SELECT mr.RelationshipID FROM mentor_relationship mr WHERE mr.MentorID={$invitation[0]->MentorID} AND mr.MenteeID={$invitation[0]->MenteeID}");
            if ($relationship->num_rows() > 0) {
                $response = array((object) array(
                        'InsertedId' => '1'
                    )
                );
            } else {
                $data = array(
                    'MentorID' => $invitation[0]->MentorID,
                    'MenteeID' => $invitation[0]->MenteeID
                );
                $this->db->insert('mentor_relationship', $data);
                $response = array((object) array(
                        'InsertedId' => '1'
                    )
                );
            }
        } else {
            $response = array((object) array(
                    'Message' => 'No invitation found.'
                )
            );
        }
        return $response;
    }

    /* ,$ImageData */

    function checkMentorExist($userId, $userName = '', $email = '', $checkUserId = true) {
        $userCdn = ($checkUserId) ? "m.MentorID != '$userId' AND" : "";
        if (!empty($userName)) {
            $UserExistCheck = $this->db->query("SELECT COUNT(m.MentorID) as UserCheck FROM mentor_mentor m WHERE $userCdn m.MentorName = '$userName'")->result();
        } else if (!empty($email)) {
            $UserExistCheck = $this->db->query("SELECT COUNT(m.MentorContactInfoID) as UserCheck FROM mentor_mentorcontactinfo m WHERE $userCdn m.MentorEmail = '$email'")->result();
        } else {
            $UserExistCheck = $this->db->query("SELECT COUNT(m.MentorContactInfoID) as UserCheck FROM mentor_mentorcontactinfo m WHERE m.MentorID = '$userId'")->result();
        }
        if ($UserExistCheck[0]->UserCheck > 0) {
            return true;
        } else {
            return false;
        }
    }

    function checkMenteeExist($userId, $userName = '', $email = '', $checkUserId = true) {
        $userCdn = ($checkUserId) ? "m.MenteeID != '$userId' AND" : "";
        if (!empty($userName)) {
            $UserExistCheck = $this->db->query("SELECT COUNT(m.MenteeID) as UserCheck FROM mentor_mentee m WHERE $userCdn m.MenteeName = '$userName'")->result();
        } else if (!empty($email)) {
            $UserExistCheck = $this->db->query("SELECT COUNT(m.MenteeContactID) as UserCheck FROM mentor_menteecontact m WHERE $userCdn m.MenteeEmail = '$email'")->result();
        } else {
            $UserExistCheck = $this->db->query("SELECT COUNT(m.MenteeContactID) as UserCheck FROM mentor_menteecontact m WHERE m.MenteeID != '$userId'")->result();
        }

        if ($UserExistCheck[0]->UserCheck > 0) {
            return true;
        } else {
            return false;
        }
    }

    function editProfileImage($UserId, $UserType, $Path) {
//        $sql = "call usp_EditProfile('" . $UserId . "','" . $UserType . "','" . $Path . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();

        if ($UserType == 0) { //For Mentor
            if ($this->checkMentorExist($UserId)) {
                $this->db->where('MentorID', $UserId);
                $this->db->update('mentor_mentorcontactinfo', array('MentorImage' => $Path));

//                $this->db->where('MentorID', $UserId);
//                $data = array(
//                    'MentorPhone' => $PhoneNumber,
//                    'Password' => $Pass,
//                    'MentorEmail' => $Email
//                );
//                $this->db->update('mentor_mentorcontactinfo', $data);

                $query = $this->db->query("SELECT m.MentorID AS Id, m.MentorName, mci.MentorPhone, mci.MentorEmail, mci.MentorStatus, 0 AS UserType,
                            IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_mentorskillset ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MentorID = '$UserId'), '') AS Skills,
                            mci.Password, CONCAT ( '{$image_path}admin/', mci.MentorImage ) AS MentorImage,mci.DisableSMS
                            FROM mentor_mentor m
                            LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorID = m.MentorID
                            WHERE m.MentorID = '$UserId'");
                $response = $query->result();
            } else {
                $response = array((object) array(
                        'Message' => 'Profile do not match.'
                    )
                );
            }
        } else if ($UserType == 1) { //For Mentee
            if ($this->checkMenteeExist($UserId)) {
                $this->db->where('MenteeID', $UserId);
                $this->db->update('mentor_menteecontact', array('MenteeImage' => $Path));

//                $this->db->where('MenteeID', $UserId);
//                $data = array(
//                    'MenteePhone' => $PhoneNumber,
//                    'Password' => $Pass,
//                    'MenteeEmail' => $Email
//                );
//                $this->db->update('mentor_menteecontact', $data);

                $query = $this->db->query("SELECT m.MenteeID AS Id, m.MenteeName, mc.MenteePhone, mc.MenteeEmail, mc.MenteeStatus, 1 AS UserType,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_menteeskill ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MenteeID = '$UserId'), '') AS Skills,
                        mc.Password, CONCAT ( '{$image_path}admin/', mc.MenteeImage ) AS MenteeImage,mc.DisableSMS
                        FROM mentor_mentee m
                        LEFT JOIN mentor_menteecontact mc ON mc.MenteeId = m.MenteeID
                        WHERE m.MenteeID = '$UserId'");
                $response = $query->result();
            } else {
                $response = array((object) array(
                        'Message' => 'Profile do not match.'
                    )
                );
            }
        }

        return $response;
    }

    function editProfileDetail($UserId, $UserType, $UserName, $PhoneNumber, $Pass, $Email, $DisableSMS) {
        if ($this->checkMentorExist($UserId, $UserName) || $this->checkMenteeExist($UserId, $UserName)) {
            $response = array((object) array(
                    'Message' => 'UserName already Exists'
                )
            );
        } else if ($this->checkMentorExist($UserId, '', $Email) || $this->checkMenteeExist($UserId, '', $Email)) {
            $response = array((object) array(
                    'Message' => 'Email already Exists'
                )
            );
        } else {
            $image_path = 'http://melstm.net/mentor/assets/admin/images/';
            if ($UserType == 0) { //For Mentor
                if ($this->checkMentorExist($UserId)) {
                    $this->db->where('MentorID', $UserId);
                    $this->db->update('mentor', array('MentorName' => $UserName));

                    $this->db->where('MentorID', $UserId);
                    $data = array(
                        'MentorPhone' => $PhoneNumber,
                        'Password' => $Pass,
                        'MentorEmail' => $Email,
                        'DisableSMS' => $DisableSMS
                    );
                    $this->db->update('mentor_mentorcontactinfo', $data);

                    $query = $this->db->query("SELECT m.MentorID AS Id, m.MentorName, mci.MentorPhone, mci.MentorEmail, mci.MentorStatus, 0 AS UserType,
                            IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_mentorskillset ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MentorID = '$UserId'), '') AS Skills,
							mci.Password, CONCAT ( '{$image_path}admin/', mci.MentorImage ) AS MentorImage, mci.DisableSMS
                            FROM mentor_mentor m
                            LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorID = m.MentorID
                            WHERE m.MentorID = '$UserId'");
                    $response = $query->result();
                } else {
                    $response = array((object) array(
                            'Message' => 'Profile do not match. Please try again.'
                        )
                    );
                }
            } else if ($UserType == 1) { //For Mentee
                if ($this->checkMenteeExist($UserId)) {
                    $this->db->where('MenteeID', $UserId);
                    $this->db->update('mentee', array('MenteeName' => $UserName));

                    $this->db->where('MenteeID', $UserId);
                    $data = array(
                        'MenteePhone' => $PhoneNumber,
                        'Password' => $Pass,
                        'MenteeEmail' => $Email,
                        'DisableSMS' => $DisableSMS
                    );
                    $this->db->update('mentor_menteecontact', $data);

                    $query = $this->db->query("SELECT m.MenteeID AS Id, m.MenteeName, mc.MenteePhone, mc.MenteeEmail, mc.MenteeStatus, 1 AS UserType,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_menteeskill ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MenteeID = '$UserId'), '') AS Skills,
						mc.Password, CONCAT ( '{$image_path}admin/', mc.MenteeImage ) AS MenteeImage,mc.DisableSMS
                        FROM mentor_mentee m
                        LEFT JOIN mentor_menteecontact mc ON mc.MenteeId = m.MenteeID
                        WHERE m.MenteeID = '$UserId'");
                    $response = $query->result();
                } else {
                    $response = array((object) array(
                            'Message' => 'Profile do not match. Please try again.'
                        )
                    );
                }
            }
        }
        return $response;
    }

    function getProfile($UserId, $UserType) {
//		$sql = "call usp_GetProfile('" . $UserId . "','" . $UserType . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();

        $image_path = 'http://melstm.net/mentor/assets/admin/images/';
        if ($UserType == 0) { //For Mentor
            if ($this->checkMentorExist($UserId)) {
                $query = $this->db->query("SELECT m.MentorID AS Id, m.MentorName, mci.MentorPhone, mci.MentorEmail, mci.MentorStatus, 0 AS UserType,
                    IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_mentorskillset ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MentorID = '$UserId'), '') AS Skills,
                    mci.Password, CONCAT ( '{$image_path}admin/', mci.MentorImage ) AS MentorImage,mci.DisableSMS
                    FROM mentor_mentor m
                    LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorID = m.MentorID
                    WHERE m.MentorID = '$UserId'");
                $response = $query->result();
            } else {
                $response = array((object) array(
                        'Message' => 'Profile do not match'
                    )
                );
            }
        } else if ($UserType == 1) { //For Mentee
            if ($this->checkMenteeExist($UserId)) {

                $query = $this->db->query("SELECT m.MenteeID AS Id, m.MenteeName, mc.MenteePhone, mc.MenteeEmail, mc.MenteeStatus, 1 AS UserType,
                    IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_menteeskill ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MenteeID = '$UserId'), '') AS Skills,
                    mc.Password, CONCAT ( '{$image_path}admin/', mc.MenteeImage ) AS MenteeImage,mc.DisableSMS
                    FROM mentor_mentee m
                    LEFT JOIN mentor_menteecontact mc ON mc.MenteeId = m.MenteeID
                    WHERE m.MenteeID = '$UserId'");
                $response = $query->result();
            } else {
                $response = array((object) array(
                        'Message' => 'Profile do not match'
                    )
                );
            }
        }
        return $response;
    }

    function getProfileDetail($UserId, $UserType) {
        $arrReturn = array();
        if ($UserType == "0") {
            $this->db->select('m.*,me.*');
            $this->db->from('mentor m');
            $this->db->join('mentorcontactinfo me', 'me.MentorID = m.MentorID', "left");
            $this->db->where('m.MentorID', $UserId);
            mysqli_next_result($this->db->conn_id);
            $userData = $this->db->get()->row_array();
            if (!empty($userData)) {
                $arrReturn['Name'] = $userData['MentorName'];
                $arrReturn['Email'] = $userData['MentorEmail'];
                $arrReturn['AccessToken'] = $userData['Password'];
                $arrReturn['MentorImage'] = $userData['MentorImage'];
                $arrReturn['UserType'] = 'Mentor';
            }
            return $arrReturn;
        } else {
            $this->db->select('m.*,me.*');
            $this->db->from('mentee m');
            $this->db->join('menteecontact me', 'me.MenteeId = m.MenteeId', "left");
            $this->db->where('m.MenteeId', $UserId);
            mysqli_next_result($this->db->conn_id);
            $userData = $this->db->get()->row_array();
            if (!empty($userData)) {
                $arrReturn['Name'] = $userData['MenteeName'];
                $arrReturn['Email'] = $userData['MenteeEmail'];
                $arrReturn['AccessToken'] = $userData['Password'];
                $arrReturn['MentorImage'] = $userData['MenteeImage'];
                $arrReturn['UserType'] = 'Mentee';
            }
            return $arrReturn;
        }
    }

    function updateMenteeactionsItemsWithDoneDetail($MenteeID, $MeetingID, $ActionId) {
        //$sql = "call usp_UpdateMenteeactionsItemsWithDone('" . $MenteeID . "','" . $MeetingID . "','" . $ActionId . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $data = array(
            "MenteeID" => $MenteeID,
            "MeetingID" => $MeetingID,
            "MenteeActionID" => $ActionId
        );
        $insert_id = $this->db->insert('mentor_menteeactionstaken', $data);
        return $insert_id;
    }

    function updateMeetingMenteeactionsItemsWithDoneDetail($MeetingID, $MenteeActionItemDone) {
        //$sql = "call usp_UpdateMeetingMenteeactionsItemsWithDone('" . $MeetingID . "','" . $MenteeActionItemDone . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $data = array(
            "MenteeActionItemDone" => $MenteeActionItemDone
        );
        $this->db->where('MeetingID', $MeetingID);
        if ($this->db->update('mentor_meetinginfo', $data)) {
            $result = array((object) array(
                    'Id' => 1
            ));
        } else {
            $result = array((object) array(
                    'Message' => 'Failed to update meeting'
            ));
        }
        return $result;
    }

    function addinviteList($MentorID, $MenteeID) {
        $sql = "call  usp_addinviteList('" . $MentorID . "','" . $MenteeID . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }

    function getConfig() {
//        $sql = "call  usp_getConfig()";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();

        $query = $this->db->query("SELECT settingID, mentorName, menteeName, headerColor, leftBarColor, icon, buttonColor, fontColor, backgroundColor, highlightColor, leftMenuFont, tableFont, tableBackground,concat('http://melstm.net/mentor/assets/admin/images/',logo) AS logo FROM mentor_settings");
        return $query->result();
    }

    function setMentorIamge($MentorID, $MentorIamge) {
        $sql = "call usp_setMentorIamge('" . $MentorID . "','" . $MentorIamge . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }

}