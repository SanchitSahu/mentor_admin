<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Emailtemplate_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    
    /**
     * This function is use for getting detail for state
     * @param type $stateId
     * @return type
     */
    function get_emailtempate_details($emailtemplateid) {
        try {
            $this->db->select('');
            $this->db->from('c10melstm_net_template.emailtemplate');
            $this->db->where('EmailTemplateId', $emailtemplateid);
            mysqli_next_result($this->db->conn_id);
            $query = $this->db->get();
            $data = $query->row_array();
            $query->free_result();
            return $data;
        } catch (Exception $ex) {
            show_error($ex->getMessage());
        }
    }

}