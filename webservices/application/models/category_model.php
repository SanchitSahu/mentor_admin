<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getCategory() {
        $sql = "call usp_GetCategory()";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        return $query->result();
    }
}
