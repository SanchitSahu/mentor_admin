<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mentee_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    public function getMenteeDetail($MentorID) {
        //$sql = "call  usp_GetMentee('" . $MentorID . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $query = $this->db->query("
            SELECT m.MentorID, m.MentorName, mentee.*,mi.TopicID,mi.ManteeComments AS MenteeComments
            FROM (mentor_invitation mi) 
            LEFT JOIN mentor_mentor m ON mi.MentorID=m.MentorID
            #LEFT JOIN mentor_relationship r ON r.MentorID = m.MentorID AND mi.MenteeID=r.MenteeID
            LEFT JOIN mentor_mentee mentee ON mentee.MenteeID = mi.MenteeID 
            WHERE mi.MentorID = $MentorID 
			AND (mi.Status='Accepted' OR mi.Status='Rescheduled') OR mi.InitiatedBy = 0
            ORDER BY mentee.Weight ASC, mentee.MenteeName ASC;
        ");
        return $query->result();
    }
    
    public function getMenteeActionTakenDetail() {
        //$sql = "call  usp_GetMenteeActionTaken()";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $query = $this->db->query("SELECT t.MenteeActionID, t.MenteeActionName FROM mentor_menteedefinedactions t WHERE t.Status != 0");
        return $query->result();
    }
    
    public function menteeCommentDetail($MentorID, $MenteeID, $MeetingID, $MenteeComment, $CareC, $CareA, $CareR, $CareE) {
        //$sql = "call usp_AddMenteeComment('" . $MentorID . "','" . $MenteeID . "','" . $MeetingID . "','" . $MenteeComment . "','" . $CareC . "','" . $CareA . "','" . $CareR . "','" . $CareE . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $query = $this->db->query("SELECT COUNT(MenteeCommentID) as commentCount FROM mentor_menteecomment WHERE MenteeID = '$MenteeID' AND MeetingID = '$MeetingID'");
        $commentCount = $query->result();

        $data = array(
            'MentorID' => $MentorID,
            'MenteeID' => $MenteeID,
            'MeetingID' => $MeetingID,
            'MenteeComment' => $MenteeComment,
            'CareC' => $CareC,
            'CareA' => $CareA,
            'CareR' => $CareR,
            'CareE' => $CareE
        );

        if ($commentCount[0]->commentCount > 0) {
            $this->db->where('MenteeID', $MenteeID);
            $this->db->where('MeetingID', $MeetingID);
            $result = $this->db->update('mentor_menteecomment', $data);
        } else {
            $result = $this->db->insert('mentor_menteecomment', $data);
        }

        return array((object) array('InsertedId' => $result));
    }
}