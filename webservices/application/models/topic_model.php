<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Topic_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getTopicDetail() {
        //$sql = "call  usp_GetTopic()";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //return $query->result();

        $query = $this->db->query("SELECT t.TopicID, t.TopicDescription FROM mentor_topic t where t.Status = 1 ORDER BY t.Weight ASC, t.TopicDescription ASC");
        return $query->result();
    }

    public function getSubTopicDetail() {
        //$sql = "call  usp_GetSubTopic()";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //return $query->result();

        $query = $this->db->query("
            SELECT t.SubTopicID, t.TopicID, t.SubTopicDescription
            FROM mentor_subtopic t
            WHERE t.Status = 1
            ORDER BY t.Weight ASC");
        return $query->result();
    }

}
