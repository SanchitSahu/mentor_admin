<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Meetingtype_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    public function getMeetingTypeDetail() {
//        $sql = "call  usp_GetMeetingType()";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();

        $query = $this->db->query("SELECT t.MeetingTypeID, t.MeetingTypeName,t.Weight FROM mentor_meetingtype t WHERE t.Status = 1 ORDER BY t.Weight ASC");
        return $query->result();
    }
    
    public function getMeetingHistoryDetail($UserId, $UserType, $CurrentPage, $PageSize) {
//        $sql = "call usp_GetMeetingHistory('" . $UserId . "','" . $UserType . "','" . $CurrentPage . "','" . $PageSize . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();
        $result = "";
        if ($UserType == 0) { //For mentor
            $count = $this->checkMeetingExistsForUser($UserId);
            if ($count > 0) {
                $query = $this->db->query("
                    (
                        SELECT 
                        mee.MeetingID, mee.MentorID,CONCAT('http://melstm.net/mentor/assets/admin/images/admin/',mci.MenteeImage) AS MenteeImage, mee.MenteeID, DATE_FORMAT(mee.MeetingStartDatetime,'%Y-%m-%d %H:%i') AS MeetingStartDatetime, 
                        DATE_FORMAT(mee.MeetingEndDatetime,'%Y-%m-%d %H:%i') AS MeetingEndDatetime,mee.RawStartDatetime, mee.RawEndDatetime, mee.MeetingTypeID, mee.MeetingTopicID,
                        #mee.MeetingSubTopicID, 
                        mee.MeetingPlaceID, mee.MeetingElapsedTime, mee.MeetingFeedback, mee.Status,mee.Meeting_status as MeetingStatus, mee.MentorActionItemDone, mee.MenteeActionItemDone,
                        m.MentorName,me.MenteeName,mt.MeetingTypeName,mp.MeetingPlaceName,t.TopicDescription,mee.MenteeInvitationComment,
    
                        #IFNULL(FN_GetMeetingSubTopicIds(mee.MeetingSubTopicID),'') AS MeetingSubTopicID,
                        IFNULL((SELECT GROUP_CONCAT(MentorSubTopicsID) AS MeetingSubTopicIds
                        FROM mentor_subTopicsIDs mst
                        WHERE FIND_IN_SET(mst.MentorSubTopicsIDsId , mee.MeetingSubTopicID)),'') AS MeetingSubTopicID,
                        
                        IFNULL((SELECT NoteID FROM mentor_notes mn
                        WHERE mee.MeetingID = mn.MeetingID AND NoteUserType='$UserType'),'') AS NoteID,
                        
                        #IFNULL(FN_GetSubTopicDetails(mee.MeetingID),'') AS SubTopicDescription,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(ss.SubTopicID,'~',ss.SubTopicDescription)) AS SubTopicDetails
                        FROM mentor_meetinginfo m
                        INNER JOIN mentor_subTopicsIDs s ON s.MeetingID = m.MeetingID 
                        INNER JOIN mentor_subtopic ss ON ss.SubTopicID = s.MentorSubTopicsID
                        WHERE s.MeetingID = mee.MeetingID),'') AS SubTopicDescription,
    
                        #IFNULL(FN_GetSubTopicNames(mee.MeetingID),'') AS SubTopicName,
                        IFNULL((SELECT GROUP_CONCAT(ss.SubTopicDescription) AS SubTopicDetails
                        FROM mentor_meetinginfo m
                        INNER JOIN mentor_subTopicsIDs s ON s.MeetingID = m.MeetingID 
                        INNER JOIN mentor_subtopic ss ON ss.SubTopicID = s.MentorSubTopicsID
                        WHERE s.MeetingID = mee.MeetingID),'') AS SubTopicName,
                        
                        #IFNULL(FN_GetActionDetails(mee.MeetingID,'0'),'') AS MentorActionDetail,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mdd.MentorActionID,'~',mdd.MentorActionName)) AS ActionDetails
                        FROM mentor_mentoractionstaken AS mat
                        LEFT JOIN mentor_mentordefinedactions AS mdd ON mdd.MentorActionID = mat.MentorActionID
                        WHERE mat.MeetingID = mee.MeetingID),'') AS MentorActionDetail,
    
                        #IFNULL(FN_GetActionDetails(mee.MeetingID,'1'),'') AS MenteeActionDetail,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mdd.MenteeActionID,'~',mdd.MenteeActionName)) AS ActionDetails
                        FROM mentor_menteeactionstaken AS mat
                        LEFT JOIN mentor_menteedefinedactions AS mdd ON mdd.MenteeActionID = mat.MenteeActionID
                        WHERE mat.MeetingID = mee.MeetingID),'') AS MenteeActionDetail,
                        
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mdd.MentorActionID,'~',mdd.MentorActionName)) AS ActionDetails
                        FROM mentor_mentordefinedactions mdd
                        WHERE FIND_IN_SET(mdd.MentorActionID, mee.MentorActionItemDone)),'') AS MentorActionDoneDetail,
                        
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mdd.MenteeActionID,'~',mdd.MenteeActionName)) AS ActionDetails
                        FROM mentor_menteedefinedactions mdd
                        WHERE FIND_IN_SET(mdd.MenteeActionID, mee.MenteeActionItemDone)),'') AS MenteeActionDoneDetail,
    
                        #IFNULL(FN_GetCommentsDetails(mee.MeetingID,'0'),'') AS MentorComment,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mc.MentorComment)) AS CommentsDetails
                        FROM mentor_mentorcomment mc WHERE mc.MeetingID = mee.MeetingID),'') AS MentorComment,
    
                        #IFNULL(FN_GetCommentsDetails(mee.MeetingID,'1'),'') AS MenteeComment,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mc.MenteeComment)) AS CommentsDetails
                        FROM mentor_menteecomment mc WHERE mc.MeetingID = mee.MeetingID),'') AS MenteeComment,
    
                        #IFNULL(FN_GetSatFacDetails(mee.MeetingID),'') AS SatisfactionIndex
                        IFNULL((SELECT mc.SatisfactionIndex AS SatFacDetails FROM mentor_mentorcomment mc WHERE mc.MeetingID = mee.MeetingID),'') AS average,
                        
                        (SELECT IFNULL((SELECT ROUND(SUM(CareC+CareA+CareR+CareE)/4) FROM `mentor_menteecomment` WHERE MeetingID=mee.MeetingID),0)) AS SatisfactionIndex,
                        0 AS isCancelled, mee.MeetingStartDatetime AS sort_date_time, 0 AS IsTeamMeeting,
                        
                        CONCAT(DATE_FORMAT(mee.MeetingStartDateTime, '%d-%m-%y %h:%i%p'), ' - ', t.TopicDescription) MeetingDetail,
                        
                        mee.FollowUpMeetingFor as FollowUpMeetingFor
                        
                        FROM mentor_mentor m
                        LEFT JOIN mentor_meetinginfo mee ON mee.MentorID = m.MentorID
                        LEFT JOIN mentor_mentee me ON me.MenteeID = mee.MenteeID
                        LEFT JOIN mentor_menteecontact mci ON mci.MenteeID = mee.MenteeID
                        LEFT JOIN mentor_meetingplace mp ON mp.MeetingPlaceID = mee.MeetingPlaceID
                        LEFT JOIN mentor_meetingtype mt ON mt.MeetingTypeID = mee.MeetingTypeID
                        LEFT JOIN mentor_topic t ON t.TopicID = mee.MeetingTopicID
                        WHERE m.MentorID = '$UserId' AND mee.MenteeID != 0 #AND mee.Meeting_status ='3'
                    )
                    UNION
                    (
                        SELECT 0 MeetingID, mi.MentorID MentorID, CONCAT('http://melstm.net/mentor/assets/admin/images/admin/',mc.MenteeImage) MenteeImage, 
                        mi.MenteeID MenteeID, 0 MeetingStartDatetime,0 MeetingEndDatetime, '' RawStartDatetime, '' RawEndDatetime,
                        mi.MeetingTypeID MeetingTypeID, mi.topicID MeetingTopicID, mi.MeetingPlaceID MeetingPlaceID, 0 MeetingElapsedTime,
                        0 MeetingFeedback, mi.Status STATUS, 0 MeetingStatus, 0 MentorActionItemDone, 0 MenteeActionItemDone,
                        m.MentorName MentorName, me.MenteeName MenteeName, mt.MeetingTypeName MeetingTypeName, mp.MeetingPlaceName MeetingPlaceName,
                        t.TopicDescription TopicDescription, 0 MenteeInvitationComment, 0 MeetingSubTopicID, '' NoteID, 0 SubTopicDescription,
                        '' SubTopicName, '' MentorActionDetail, '' MenteeActionDetail, '' MentorActionDoneDetail, '' MenteeActionDoneDetail,
                        mi.MentorComments MentorComment, mi.ManteeComments MenteeComment, 0 SatisfactionIndex, 0 average, 1 AS isCancelled,
                        mi.AcceptOrRejectTime AS sort_date_time, 0 AS IsTeamMeeting,
                        0 AS MeetingDetail, 0 as FollowUpMeetingFor
                        
                        FROM mentor_invitation mi 
                        LEFT JOIN mentor_mentor m ON mi.MentorID=m.MentorID
                        LEFT JOIN mentor_mentorcontactinfo mci ON mi.MentorID=mci.MentorID
                        
                        LEFT JOIN mentor_mentee me ON mi.MenteeID=me.MenteeID
                        LEFT JOIN mentor_menteecontact mc ON mi.MenteeID=mc.MenteeID
                        
                        LEFT JOIN mentor_meetingplace mp ON mp.MeetingPlaceID = mi.MeetingPlaceID
                        LEFT JOIN mentor_meetingtype mt ON mt.MeetingTypeID = mi.MeetingTypeID
                        LEFT JOIN mentor_topic t ON t.TopicID = mi.TopicID
                        
                        WHERE (mi.Status='Cancelled' OR mi.Status='Rejected')
                        AND mi.MentorID = '$UserId'
                    )
                    UNION
                    (
                        SELECT 
                    
                        mee.MeetingID, mee.MentorID,CONCAT('http://melstm.net/mentor/assets/admin/images/admin/',mtt.TeamImage) AS MenteeImage, mtt.TeamId AS MenteeID, DATE_FORMAT(mee.MeetingStartDatetime,'%Y-%m-%d %H:%i') AS MeetingStartDatetime, 
                        DATE_FORMAT(mee.MeetingEndDatetime,'%Y-%m-%d %H:%i') AS MeetingEndDatetime,mee.RawStartDatetime, mee.RawEndDatetime, mee.MeetingTypeID, mee.MeetingTopicID,
                        #mee.MeetingSubTopicID, 
                        mee.MeetingPlaceID, mee.MeetingElapsedTime, mee.MeetingFeedback, mee.Status,mee.Meeting_status AS MeetingStatus, mee.MentorActionItemDone, mee.MenteeActionItemDone,
                        m.MentorName,mtt.TeamName AS MenteeName,mt.MeetingTypeName,mp.MeetingPlaceName,t.TopicDescription,mee.MenteeInvitationComment,
                    
                        #IFNULL(FN_GetMeetingSubTopicIds(mee.MeetingSubTopicID),'') AS MeetingSubTopicID,
                        IFNULL((SELECT GROUP_CONCAT(MentorSubTopicsID) AS MeetingSubTopicIds
                        FROM mentor_subTopicsIDs mst
                        WHERE FIND_IN_SET(mst.MentorSubTopicsIDsId , mee.MeetingSubTopicID)),'') AS MeetingSubTopicID,
                    
                        IFNULL((SELECT NoteID FROM mentor_notes mn
                        WHERE mee.MeetingID = mn.MeetingID AND NoteUserType='$UserType'),'') AS NoteID,
                    
                        #IFNULL(FN_GetSubTopicDetails(mee.MeetingID),'') AS SubTopicDescription,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(ss.SubTopicID,'~',ss.SubTopicDescription)) AS SubTopicDetails
                        FROM mentor_meetinginfo m
                        INNER JOIN mentor_subTopicsIDs s ON s.MeetingID = m.MeetingID 
                        INNER JOIN mentor_subtopic ss ON ss.SubTopicID = s.MentorSubTopicsID
                        WHERE s.MeetingID = mee.MeetingID),'') AS SubTopicDescription,
                    
                        #IFNULL(FN_GetSubTopicNames(mee.MeetingID),'') AS SubTopicName,
                        IFNULL((SELECT GROUP_CONCAT(ss.SubTopicDescription) AS SubTopicDetails
                        FROM mentor_meetinginfo m
                        INNER JOIN mentor_subTopicsIDs s ON s.MeetingID = m.MeetingID 
                        INNER JOIN mentor_subtopic ss ON ss.SubTopicID = s.MentorSubTopicsID
                        WHERE s.MeetingID = mee.MeetingID),'') AS SubTopicName,
                    
                        #IFNULL(FN_GetActionDetails(mee.MeetingID,'0'),'') AS MentorActionDetail,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mdd.MentorActionID,'~',mdd.MentorActionName)) AS ActionDetails
                        FROM mentor_mentoractionstaken AS mat
                        LEFT JOIN mentor_mentordefinedactions AS mdd ON mdd.MentorActionID = mat.MentorActionID
                        WHERE mat.MeetingID = mee.MeetingID),'') AS MentorActionDetail,
                    
                        #IFNULL(FN_GetActionDetails(mee.MeetingID,'1'),'') AS MenteeActionDetail,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mdd.MenteeActionID,'~',mdd.MenteeActionName)) AS ActionDetails
                        FROM mentor_teamactionstaken AS tat
                        LEFT JOIN mentor_menteedefinedactions AS mdd ON mdd.MenteeActionID = tat.TeamActionID
                        WHERE tat.MeetingID = mee.MeetingID),'') AS MenteeActionDetail,  
                    
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mdd.MentorActionID,'~',mdd.MentorActionName)) AS ActionDetails
                        FROM mentor_mentordefinedactions mdd
                        WHERE FIND_IN_SET(mdd.MentorActionID, mee.MentorActionItemDone)),'') AS MentorActionDoneDetail,
                    
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mdd.MenteeActionID,'~',mdd.MenteeActionName)) AS ActionDetails
                        FROM mentor_menteedefinedactions mdd
                        WHERE FIND_IN_SET(mdd.MenteeActionID, mee.MenteeActionItemDone)),'') AS MenteeActionDoneDetail,
                    
                        #IFNULL(FN_GetCommentsDetails(mee.MeetingID,'0'),'') AS MentorComment,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mc.MentorComment)) AS CommentsDetails
                        FROM mentor_mentorcomment mc WHERE mc.MeetingID = mee.MeetingID),'') AS MentorComment,
                    
                        #IFNULL(FN_GetCommentsDetails(mee.MeetingID,'1'),'') AS MenteeComment,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mc.MenteeComment)) AS CommentsDetails
                        FROM mentor_menteecomment mc WHERE mc.MeetingID = mee.MeetingID),'') AS MenteeComment,
                    
                        #IFNULL(FN_GetSatFacDetails(mee.MeetingID),'') AS SatisfactionIndex
                        IFNULL((SELECT mc.SatisfactionIndex AS SatFacDetails FROM mentor_mentorcomment mc WHERE mc.MeetingID = mee.MeetingID),'') AS SatisfactionIndex,
                    
                        (SELECT IFNULL((SELECT ROUND(SUM(CareC+CareA+CareR+CareE)/4) FROM `mentor_menteecomment` WHERE MeetingID=mee.MeetingID),0)) AS average,
                        0 AS isCancelled, mee.MeetingStartDatetime AS sort_date_time, 1 AS IsTeamMeeting,
                        0 AS MeetingDetail, 0 as FollowUpMeetingFor
                    
                        FROM mentor_mentor m
                    
                        LEFT JOIN mentor_meetinginfo mee ON mee.MentorID = m.MentorID
                    
                        LEFT JOIN mentor_team mtt ON mee.TeamID = mtt.TeamId
                    
                        #LEFT JOIN mentor_menteecontact mci ON mci.MenteeID = mee.MenteeID
                    
                        LEFT JOIN mentor_meetingplace mp ON mp.MeetingPlaceID = mee.MeetingPlaceID
                    
                        LEFT JOIN mentor_meetingtype mt ON mt.MeetingTypeID = mee.MeetingTypeID
                    
                        LEFT JOIN mentor_topic t ON t.TopicID = mee.MeetingTopicID
                    
                        WHERE m.MentorID = '$UserId' AND mee.MenteeID = 0
                    )  
                    ORDER BY sort_date_time DESC
                ");
                $result = $query->result();
            }
        } else if ($UserType == 1) { //For Mentee
            $count = $this->checkMeetingExistsForUser('', $UserId);
            if ($count > 0) {
                $query = $this->db->query("
                    (
                        SELECT 
                        mee.MeetingID, mee.MentorID,CONCAT('http://melstm.net/mentor/assets/admin/images/admin/',mci.MentorImage) AS MentorImage, mee.MenteeID, DATE_FORMAT(mee.MeetingStartDatetime,'%Y-%m-%d %H:%i') AS MeetingStartDatetime, 
                        DATE_FORMAT(mee.MeetingEndDatetime,'%Y-%m-%d %H:%i') AS MeetingEndDatetime,mee.RawStartDatetime, mee.RawEndDatetime, mee.MeetingTypeID, mee.MeetingTopicID,
                        #mee.MeetingSubTopicID, 
                        mee.MeetingPlaceID, mee.MeetingElapsedTime, mee.MeetingFeedback, mee.Status,mee.Meeting_status as MeetingStatus, mee.MentorActionItemDone, mee.MenteeActionItemDone,
                        m.MentorName,me.MenteeName,mt.MeetingTypeName,mp.MeetingPlaceName,t.TopicDescription,
    
                        #IFNULL(FN_GetMeetingSubTopicIds(mee.MeetingSubTopicID),'') AS MeetingSubTopicID,
                        IFNULL((SELECT GROUP_CONCAT(MentorSubTopicsID) AS MeetingSubTopicIds
                        FROM mentor_subTopicsIDs mst
                        WHERE FIND_IN_SET(mst.MentorSubTopicsIDsId , mee.MeetingSubTopicID)),'') AS MeetingSubTopicID,
                        
                        IFNULL((SELECT NoteID FROM mentor_notes mn
                        WHERE mee.MeetingID = mn.MeetingID AND NoteUserType='$UserType'),'') AS NoteID,
                        
                        #IFNULL(FN_GetSubTopicNames(mee.MeetingID),'') AS SubTopicName,
                        IFNULL((SELECT GROUP_CONCAT(ss.SubTopicDescription) AS SubTopicDetails
                        FROM mentor_meetinginfo m
                        INNER JOIN mentor_subTopicsIDs s ON s.MeetingID = m.MeetingID 
                        INNER JOIN mentor_subtopic ss ON ss.SubTopicID = s.MentorSubTopicsID
                        WHERE s.MeetingID = mee.MeetingID),'') AS SubTopicName,
    
                        #IFNULL(FN_GetSubTopicDetails(mee.MeetingID),'') AS SubTopicDescription,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(ss.SubTopicID,'~',ss.SubTopicDescription)) AS SubTopicDetails
                        FROM mentor_meetinginfo m
                        INNER JOIN mentor_subTopicsIDs s ON s.MeetingID = m.MeetingID 
                        INNER JOIN mentor_subtopic ss ON ss.SubTopicID = s.MentorSubTopicsID
                        WHERE s.MeetingID = mee.MeetingID),'') AS SubTopicDescription,
    
                        #IFNULL(FN_GetActionDetails(mee.MeetingID,'0'),'') AS MentorActionDetail,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mdd.MentorActionID,'~',mdd.MentorActionName)) AS ActionDetails
                        FROM mentor_mentoractionstaken AS mat
                        LEFT JOIN mentor_mentordefinedactions AS mdd ON mdd.MentorActionID = mat.MentorActionID
                        WHERE mat.MeetingID = mee.MeetingID),'') AS MentorActionDetail,
    
                        #IFNULL(FN_GetActionDetails(mee.MeetingID,'1'),'') AS MenteeActionDetail,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mdd.MenteeActionID,'~',mdd.MenteeActionName)) AS ActionDetails
                        FROM mentor_menteeactionstaken AS mat
                        LEFT JOIN mentor_menteedefinedactions AS mdd ON mdd.MenteeActionID = mat.MenteeActionID
                        WHERE mat.MeetingID = mee.MeetingID),'') AS MenteeActionDetail,
                        
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mdd.MentorActionID,'~',mdd.MentorActionName)) AS ActionDetails
                        FROM mentor_mentordefinedactions mdd
                        WHERE FIND_IN_SET( mdd.MentorActionID, mee.MentorActionItemDone )),'') AS MentorActionDoneDetail,
                        
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mdd.MenteeActionID,'~',mdd.MenteeActionName)) AS ActionDetails
                        FROM mentor_menteedefinedactions mdd
                        WHERE FIND_IN_SET(mdd.MenteeActionID, mee.MenteeActionItemDone )),'')AS MenteeActionDoneDetail,
    
                        #IFNULL(FN_GetCommentsDetails(mee.MeetingID,'0'),'') AS MentorComment,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mc.MentorComment)) AS CommentsDetails
                        FROM mentor_mentorcomment mc WHERE mc.MeetingID = mee.MeetingID),'') AS MentorComment,
    
                        #IFNULL(FN_GetCommentsDetails(mee.MeetingID,'1'),'') AS MenteeComment,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mc.MenteeComment)) AS CommentsDetails
                        FROM mentor_menteecomment mc WHERE mc.MeetingID = mee.MeetingID),'') AS MenteeComment,
    
                        #IFNULL(FN_GetCareCDetails(mee.MenteeID, mee.MeetingID),'') AS CareC,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mc.CareC)) AS CareCDetails 
                        FROM mentor_menteecomment mc WHERE mc.MenteeID = mee.MenteeID AND mc.MeetingID = mee.MeetingID),'') AS CareC,
    
                        #IFNULL(FN_GetCareADetails(mee.MenteeID, mee.MeetingID),'') AS CareA,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mc.CareA)) AS CareADetails 
                        FROM mentor_menteecomment mc WHERE mc.MenteeID = mee.MenteeID AND mc.MeetingID = mee.MeetingID),'') AS CareA,
    
                        #IFNULL(FN_GetCareRDetails(mee.MenteeID, mee.MeetingID),'') AS CareR,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mc.CareR)) AS CareRDetails 
                        FROM mentor_menteecomment mc WHERE mc.MenteeID = mee.MenteeID AND mc.MeetingID = mee.MeetingID),'') AS CareR,
    
                        #IFNULL(FN_GetCareEDetails(mee.MenteeID, mee.MeetingID),'') AS CareE
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(mc.CareE)) AS CareEDetails 
                        FROM mentor_menteecomment mc WHERE mc.MenteeID = mee.MenteeID AND mc.MeetingID = mee.MeetingID),'') AS CareE,
                        
                        (SELECT IFNULL((SELECT ROUND(SUM(CareC+CareA+CareR+CareE)/4) FROM `mentor_menteecomment` WHERE MeetingID=mee.MeetingID),0)) AS average,
                        #IFNULL(FN_GetSatFacDetails(mee.MeetingID),'') AS SatisfactionIndex
                        IFNULL((SELECT mc.SatisfactionIndex AS SatFacDetails FROM mentor_mentorcomment mc WHERE mc.MeetingID = mee.MeetingID),'') AS SatisfactionIndex,
                        
                        0 AS isCancelled, mee.MeetingStartDatetime AS sort_date_time,
                        
                        CONCAT(DATE_FORMAT(mee.MeetingStartDateTime, '%d %b %y %h:%i%p'), ' - ', t.TopicDescription) MeetingDetail
    
                        FROM mentor_mentee me
                        LEFT JOIN mentor_meetinginfo mee ON mee.MenteeID = me.MenteeID
                        LEFT JOIN mentor_mentor m ON m.MentorID = mee.MentorID
                        LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorID = mee.MentorID
                        LEFT JOIN mentor_meetingplace mp ON mp.MeetingPlaceID = mee.MeetingPlaceID
                        LEFT JOIN mentor_meetingtype mt ON mt.MeetingTypeID = mee.MeetingTypeID
                        LEFT JOIN mentor_topic t ON t.TopicID = mee.MeetingTopicID
    
                        WHERE me.MenteeID = '$UserId'
                        #AND mee.Meeting_status ='3'
                        #ORDER BY MeetingStartDatetime DESC
                    )
                    
                    UNION
                    
                    (
                        SELECT 
                        0 AS MeetingID, mi.MentorID AS MentorID, CONCAT('http://melstm.net/mentor/assets/admin/images/admin/',mci.MentorImage) AS MentorImage, 
                        mi.MenteeID AS MenteeID, 0 AS MeetingStartDatetime, 0 AS MeetingEndDatetime, 0 AS RawStartDatetime, 0 AS RawEndDatetime, 
                        mi.MeetingTypeID AS MeetingTypeID, mi.topicID AS MeetingTopicID, mi.MeetingPlaceID AS MeetingPlaceID, '' AS MeetingElapsedTime, 
                        0 AS MeetingFeedback, mi.Status AS Status, 0 AS MeetingStatus, 0 AS MentorActionItemDone, 0 AS MenteeActionItemDone, 
                        m.MentorName AS MentorName, me.MenteeName AS MenteeName, mt.MeetingTypeName AS MeetingTypeName, mp.MeetingPlaceName AS MeetingPlaceName, 
                        t.TopicDescription AS TopicDescription, 0 AS MeetingSubTopicID, '' AS NoteID, '' AS SubTopicName, 0 AS SubTopicDescription, 
                        0 AS MentorActionDetail, 0 AS MenteeActionDetail, 0 AS MentorActionDoneDetail, 0 AS MenteeActionDoneDetail, 
                        
                        mi.MentorComments AS MentorComment, mi.ManteeComments AS MenteeComment, 
                        
                        0 AS CareC, 0 AS CareA, 0 AS CareR, 0 AS CareE, 0 AS average, 0 AS SatisfactionIndex, 1 AS isCancelled,
                        mi.AcceptOrRejectTime AS sort_date_time,
                        
                        0 AS MeetingDetail
                        
                        FROM mentor_invitation mi 
                        LEFT JOIN mentor_mentor m ON mi.MentorID=m.MentorID
                        LEFT JOIN mentor_mentorcontactinfo mci ON mi.MentorID=mci.MentorID
                        
                        LEFT JOIN mentor_mentee me ON mi.MenteeID=me.MenteeID
                        LEFT JOIN mentor_menteecontact mc ON mi.MenteeID=mc.MenteeID
                        
                        LEFT JOIN mentor_meetingplace mp ON mp.MeetingPlaceID = mi.MeetingPlaceID
                        LEFT JOIN mentor_meetingtype mt ON mt.MeetingTypeID = mi.MeetingTypeID
                        LEFT JOIN mentor_topic t ON t.TopicID = mi.TopicID
                        
                        WHERE (mi.Status='Cancelled' OR mi.Status='Rejected')
                        AND mi.MenteeID = '$UserId'
                    )
                    ORDER BY sort_date_time DESC
                ");
                $result = $query->result();
            }
        }
        return $result;
    }

    public function getPendingDetail($mentorID) {
//        $sql = "call usp_GetPendingDetail('" . $mentorID . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();
//or Status = 'Rescheduled'

        /*$query = $this->db->query("
            SELECT COUNT(InvitationId) AS sum1
            FROM mentor_invitation
            WHERE MentorID = '$mentorID' AND (Status = 'Pending'  OR Status = 'Rescheduled' AND rescheduledby=1)  OR (InitiatedBy = 1 OR rescheduledby=1)
        ");*/
        $query = $this->db->query("
            SELECT COUNT(InvitationId) AS sum1
            FROM mentor_invitation i
            WHERE (i.MentorID = $mentorID AND i.rescheduledby=1 AND i.Status = 'Rescheduled')
            OR 
            (i.MentorID = $mentorID AND (i.Status = 'Pending' AND i.InitiatedBy != 0))
        ");
        return $query->result();
    }
    
    function getMentorPendingMeetingCount($MentorID) {

        //$query = $this->db->query("SELECT i.InvitationId FROM mentor_invitation i WHERE i.MentorID = $MentorID AND (i.Status = 'Accepted'  OR i.Status = 'Rescheduled' AND i.rescheduledby=0) OR (InitiatedBy = 0 AND i.Status != 'Rejected' AND i.rescheduledby=0)");
        
        $query = $this->db->query("
            SELECT i.InvitationId FROM mentor_invitation i
            WHERE (i.MentorID = $MentorID AND i.rescheduledby=0 AND i.Status = 'Rescheduled')
            OR 
            (i.MentorID = $MentorID AND i.Status = 'Accepted')            
            OR
            (i.MentorID = $MentorID AND i.Status = 'Pending' AND i.InitiatedBy=0)
        ");
        return $query->num_rows();
    }
	
	public function getMeetingPlaceDetail() {
//        $sql = "call  usp_GetMeetingPlace()";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();
        $query = $this->db->query("SELECT p.MeetingPlaceID, p.MeetingPlaceName,p.Weight FROM mentor_meetingplace p WHERE p.Status = 1 ORDER BY p.Weight ASC");
        return $query->result();
    }
	
	function checkMeetingExistsForUser($mentorId = '', $menteeId = '') {
        if (empty($menteeId) && empty($mentorId)) {
            return false;
        } else if (!empty($mentorId)) {
            $this->db->where('MentorID', $mentorId);
        } else if (!empty($menteeId)) {
            $this->db->where('MenteeID', $menteeId);
        }
        $query = $this->db->get('meetinginfo');
        return $query->num_rows();
    }
}
