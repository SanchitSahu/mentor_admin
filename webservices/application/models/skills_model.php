<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Skills_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    public function getSkillsDetail() {
        //$sql = "call  usp_GetSkills()";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $query = $this->db->query("SELECT SkillID, SkillName FROM mentor_skill WHERE Status = 1 ORDER BY SkillName ASC");
        return $query->result();
    }
    
    public function addSkillDetail($UserID, $UserType, $SkillID) {
//        $sql = "call usp_AddSkill('" . $UserID . "','" . $UserType . "','" . $SkillID . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        $result = $query->result();
//        $query->next_result();
//        $query->free_result();

        $result = "";
        $data = array('SkillID' => $SkillID);
        if ($UserType == 0) { // For mentor
            $data['MentorID'] = $UserID;
            if ($this->db->insert('mentorskillset', $data)) {
                $query = $this->db->query("SELECT m.MentorID AS Id, m.MentorName, mci.MentorPhone, mci.MentorEmail, mci.MentorStatus, $UserType AS UserType,
                    IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails
                    FROM mentor_mentorskillset ms
                    LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID
                    WHERE ms.MentorID = '$UserID'), '') AS Skills, mci.Password
                    FROM mentor_mentor m
                    LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorID = m.MentorID
                    WHERE m.MentorID = '$UserID'
                ");
                $result = $query->result();
            }
        } else { // For mentee
            $data['MenteeID'] = $UserID;
            if ($this->db->insert('menteeskill', $data)) {
                $query = $this->db->query("
                    SELECT m.MenteeID AS Id, m.MenteeName, mc.MenteePhone, mc.MenteeEmail, mc.MenteeStatus, $UserType AS UserType,
                    IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails
                    FROM mentor_menteeskill ms
                    LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID
                    WHERE ms.MenteeID = '$UserID'), '') AS Skills, mc.Password
                    FROM mentor_mentee m
                    LEFT JOIN mentor_menteecontact mc ON mc.MenteeId = m.MenteeID
                    WHERE m.MenteeID = '$UserID'
                ");
                $result = $query->result();
            }
        }
        return $result;
    }
}