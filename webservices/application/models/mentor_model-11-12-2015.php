<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mentor_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    public function getMentorActionTakenDetail(){
        $sql = "call  usp_GetMentorActionTaken()";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }
    
    
    public function registerUserDetail($Email, $AccessToken, $UserType) {
        $sql = "call  usp_RegisterUser('" . $Email . "','" . $AccessToken . "','" . $UserType . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }
    
    public function checkLoginDetail($username, $accesstoken) {
        $sql = "call usp_CheckLogin('" . $username . "','" . $accesstoken . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }
    
    function forgotPasswordDetail($Email) {
        $sql = "call usp_ForgotPassword('" . $Email . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }
	
	/*21-8-2015*/
	function checkInvitationDetail($MentorID, $MenteeID) {
        $sql = "call usp_checkInvitationDetail('" . $MentorID . "','". $MenteeID ."')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }
	/**/
    
    function changePasswordDetail($Email, $OldPassword, $Password, $usertype) {
		$oldPwd	=	(int)$OldPassword;
        $sql = "call usp_ChangePassword('" . $Email . "','" . $oldPwd . "','" . $Password . "','" . $usertype . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
		//print_r($query->result());exit;
        return $query->result();
    }
    
    function submitMeetingInfoDetail($MeetingID, $MentorID, $MenteeID, $MeetingStartDatetime, $MeetingEndDatetime, $MeetingTypeID, $MeetingTopicID, $MeetingSubTopicID, $MeetingPlaceID, $MenteeActionIDs, $MeetingElapsedTime, $MeetingFeedback, $MentorActionItemDone, $MenteeActionItemDone)      {       
        $sql = "call usp_SubmitMeetingInfo('" . $MeetingID . "','" . $MentorID . "','" . $MenteeID . "','" . $MeetingStartDatetime . "','" . $MeetingEndDatetime . "','" . $MeetingTypeID . "','" . $MeetingTopicID . "','" . $MeetingSubTopicID . "','" . $MeetingPlaceID . "','" . $MenteeActionIDs . "','" . $MeetingElapsedTime . "','" . $MeetingFeedback . "','" . $MentorActionItemDone . "','" . $MenteeActionItemDone . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }
    
    function addMenteeactionstakenDetail($MenteeID, $MeetingID, $ActionId) {
        $sql = "call usp_AddMenteeactionstaken('" . $MenteeID . "','" . $MeetingID . "','" . $ActionId . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }
    
    function addMentoractionstakenDetail($MentorID, $MeetingID, $ActionId) {
        $sql = "call usp_AddMentoractionstaken('" . $MentorID . "','" . $MeetingID . "','" . $ActionId . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }
    
    
    
    /* added by khushbu 18-08-2015*/
    function addMentorsubtopicsIDs($MentorID, $MeetingID, $ActionId) {
        $sql = "call usp_AddMentorsubtopicsIDs('" . $MentorID . "','" . $MeetingID . "','" . $ActionId . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }
    
    function getMentorsubtopicsIDs($MeetingID){
			$this->db->select('*');
	      $this->db->from('mentor_subTopicsIDs');
	      $this->db->where('MeetingID', $MeetingID);
         mysqli_next_result($this->db->conn_id);
	        $mentorData = $this->db->get();
	        
	        return $mentorData->result();    
    }
    
    function updateMentorsubtopicsIDs($mentorSubIdResultsIds,$MeetingID){
    	
			/*$data = array(
               'MeetingSubTopicID' => $mentorSubIdResultsIds
            );

			$this->db->where('MeetingID', $MeetingID);
			$query	=	$this->db->update('mentor_meetinginfo', $data);
			
			
	      return $this->db->affected_rows();  
    		*/
    	 // echo $mentorSubIdResultsIds;exit;
		  $sql = "call usp_updateMentorsubtopicsIDs('" . $mentorSubIdResultsIds . "', ".$MeetingID.")";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();     
    }
    /* khushbu */
    
    
    public function getMentorDetail($MenteeID){
        $sql = "call usp_GetMentor('".$MenteeID."')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        return $query->result();
    }
    
    function mentorCommentDetail($MentorID, $MenteeID, $MeetingID, $MentorComment, $SatisfactionIndex) {
        $sql = "call usp_AddMentorComment('" . $MentorID . "','" . $MenteeID . "','" . $MeetingID . "','" . $MentorComment . "','" . $SatisfactionIndex . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();    
    }
    
    /*khushbu 19-8*/
    function mentorInvitationDetail($MentorID, $MenteeID,$TopicID,$MenteeComments) {
        $sql = "call usp_MentorInvitation('" . $MentorID . "','" . $MenteeID . "',".$TopicID.",'".$MenteeComments."')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();    
    }
    
    function getMentorForInvitation(){
   	 $this->db->select('*');
	    $this->db->from('mentor_mentor');
        mysqli_next_result($this->db->conn_id);
	    $mentorData = $this->db->get();
	        
	    return $mentorData->result();  
	   
    }
    
    function getMentorForInvitationStatus($MentorID,$MenteeID){
    	  $sql = "call usp_getMentorForInvitation('".$MentorID."','" . $MenteeID . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();    
			    
    }
    
    function startEndMeetingSession($MenteeID,$MentorID,$DateTime)
    {
		 $this->db->select('*');
	    $this->db->from('mentor_meetinginfo');
	     $this->db->where('MenteeID', $MenteeID);
	      $this->db->where('MentorID', $MentorID);
        mysqli_next_result($this->db->conn_id);
	    $mentorData = $this->db->get();
	        
	    return $mentorData->num_rows();   
    }
    
    function addStartMeetingSession($MenteeID,$MentorID,$DateTime,$MettingStatus){
     	  $sql = "call usp_addStartEndMeetingSession('".$MentorID."','" . $MenteeID . "','".$DateTime."','".$MettingStatus."')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();  
		    
    }
    /* khushbu 19-8 */
    /*khushbu 20-8*/
    function updateStartMeetingSession($DateTime,$MettingStatus,$MentorID,$MenteeID){
     	  $sql = "call usp_updateStartEndMeetingSession('".$DateTime."','".$MettingStatus."','".$MentorID."','" . $MenteeID . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();  
		    
    }
    
     function updateEndMeetingSession($DateTime,$MettingStatus,$MentorID,$MenteeID){
     	
    		/*$query	=	$this->db->query("UPDATE mentor_meetinginfo SET MeetingEndDatetime = '".$DateTime."', Meeting_status = '".$MettingStatus."' 
	WHERE MenteeID = ".$MenteeID." AND MentorID = ".$MentorID." and Meeting_status != '3'");*/
     	  $sql = "call usp_updateEndMeetingSession('".$DateTime."','".$MettingStatus."','".$MentorID."','" . $MenteeID . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();  
		    
    }
    
	function getMeetingSessionIDforSubmit($MenteeID,$MentorID,$MeetingID){
		
		if($MeetingID == 0){
			$this->db->select('*');
			$this->db->from('mentor_meetinginfo');
			$this->db->where('MenteeID', $MenteeID);
			$this->db->where('MentorID', $MentorID);
			$this->db->where('Meeting_status !=', '3');
		}else{
			$this->db->select('*');
			$this->db->from('mentor_meetinginfo');
			$this->db->where('MeetingID', $MeetingID);
		}
			
		mysqli_next_result($this->db->conn_id);
		$mentorData = $this->db->get();
	        
	        
	    //echo $this->db->last_query();exit;    
	    return $mentorData->result(); 
		
	}
	
    
    function getMeetingSessionID($MenteeID,$MentorID){
    	//echo $MenteeID;echo $MentorID;exit;
		$this->db->select('*');
		$this->db->from('mentor_meetinginfo');
		$this->db->where('MenteeID', $MenteeID);
		$this->db->where('MentorID', $MentorID);
		$this->db->where('Meeting_status !=', '3');
		mysqli_next_result($this->db->conn_id);
		$mentorData = $this->db->get();
     
	    //echo $this->db->last_query();exit;    
	    return $mentorData->result(); 
    }
    
    function getMentorWaitScreen($MenteeID){
		$this->db->select('*');
	  	$this->db->from('mentor_invitation');
	  	$this->db->where('MenteeID', $MenteeID);
      mysqli_next_result($this->db->conn_id);
	   $mentorData = $this->db->get();
	        
	        
	    //echo $this->db->last_query();exit;    
	    return $mentorData->result();   
    }
    
    function getMentorWaitScreenDetails($MentorID){
		  $sql = "call usp_getMentorWaitScreen('" . $MentorID . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->row_array();      
    }
    
    
    function getMettingWaitScreenInfo($MentorID,$MenteeID){
    	 $sql = "call usp_getMettingWaitScreenInfo('" . $MentorID . "','".$MenteeID."')";
       $parameters = array();
       $query = $this->db->query($sql, $parameters);
       mysqli_next_result($this->db->conn_id);
       return $query->result(); 
    }
    
    /*20-8*/
    
    function getMentorPendingRequestDetail($MentorID) {
        $sql = "call usp_GetMentorPendingRequest('" . $MentorID . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();    
    }
    
    function mentorResponseDetail($InvitationId, $Status, $Comments) {
        $sql = "call usp_MentorResponse('" . $InvitationId . "','" . $Status . "','" . $Comments . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();    
    }
    /*,$ImageData*/
    function editProfileDetail($UserId, $UserType, $UserName, $PhoneNumber, $Pass,$Email) {
		
		/*$data = $ImageData;
		$data = base64_decode($data);
		$img  = "";
		$im = imagecreatefromstring($data);
		if ($im !== false) {
			//header('Content-Type: image/png');
			$imgname = microtime().'.jpg';
			$imgname = str_replace(' ','',$imgname);
			 
			imagejpeg($im,'../UserImages/'.$imgname);
			//print_r($img);
			//echo $img;
			//imagepng($im);
			imagedestroy($im);
			$img = $imgname;
		}else {
			echo 'An error occurred.';
		}
		
		//echo $img;exit;
		if($img != "" || $img != NULL){*/
			$sql = "call usp_EditProfile('" . $UserId . "','" . $UserType . "','" . $UserName . "','" . $PhoneNumber . "','" . $Pass . "','".$Email."')";/*,'".$img."'*/
			$parameters = array();
			$query = $this->db->query($sql, $parameters);
			mysqli_next_result($this->db->conn_id);
			//print_r($query->result());exit;
			return $query->result();    
		/*}else{
			echo "An error occurred.";
		}	*/
    }
    
    function getProfileDetail($UserId, $UserType) {
    	$arrReturn = array();
    	if ($UserType == "0"){
 		$this->db->select('m.*,me.*');
	        $this->db->from('mentor m');
                $this->db->join('mentorcontactinfo me', 'me.MentorID = m.MentorID', "left");
              	$this->db->where('m.MentorID', $UserId);
           	mysqli_next_result($this->db->conn_id);
	        $userData = $this->db->get()->row_array();
	        if (!empty($userData)){
	        	$arrReturn['Name'] 	  = $userData['MentorName'];
	        	$arrReturn['Email'] 	  = $userData['MentorEmail'];
	        	$arrReturn['AccessToken'] = $userData['Password'];
	        	$arrReturn['UserType']    = 'Mentor';
	        }
	        return $arrReturn;
    	}else{
    		$this->db->select('m.*,me.*');
	        $this->db->from('mentee m');
                $this->db->join('menteecontact me', 'me.MenteeId = m.MenteeId', "left");
              	$this->db->where('m.MenteeId', $UserId);
            	mysqli_next_result($this->db->conn_id);
	        $userData = $this->db->get()->row_array();
	        if (!empty($userData)){
	        	$arrReturn['Name'] 	  = $userData['MenteeName'];
	        	$arrReturn['Email'] 	  = $userData['MenteeEmail'];
	        	$arrReturn['AccessToken'] = $userData['Password'];
	        	$arrReturn['UserType']    = 'Mentee';
	        }
	        return $arrReturn;
    	}
    }
    
    function updateMenteeactionsItemsWithDoneDetail($MenteeID, $MeetingID, $ActionId) {
        $sql = "call usp_UpdateMenteeactionsItemsWithDone('" . $MenteeID . "','" . $MeetingID . "','" . $ActionId . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();    
    }
    
    function updateMeetingMenteeactionsItemsWithDoneDetail($MeetingID, $MenteeActionItemDone) {
        $sql = "call usp_UpdateMeetingMenteeactionsItemsWithDone('" . $MeetingID . "','" . $MenteeActionItemDone . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();        
    }
	
	function addinviteList($MentorID, $MenteeID){
		$sql = "call  usp_addinviteList('" . $MentorID . "','" . $MenteeID . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
	}
	
	function getConfig(){
		$sql = "call  usp_getConfig()";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
	}
}
