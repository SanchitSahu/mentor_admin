<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mentor_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
	function removeCancelledMeetingEntry_model($invitationId) {
		$data = array('isRemovedByMentee' => 1);
		$this->db->where('InvitationId', $invitationId);
		if($this->db->update('mentor_invitation', $data)) {
			return true;
		} else {
			return false;
		}
		
	}
	
	function updateLastLoginTime($UserId, $UserType) {
		if($UserType == 0) {
			$this->db->where('MentorID', $UserId);
			$this->db->update('mentorcontactinfo', array('LastLogin' => date('Y-m-d H:i:s')));
		} else if($UserType == 1) {
			$this->db->where('MenteeID', $UserId);
			$this->db->update('menteecontact', array('LastLogin' => date('Y-m-d H:i:s')));
		}
	}
	
	function getActionItemComments(){
		$this->db->where('Status',1);
		$resultMentees = $this->db->get('menteeactionitemcomments');
		
		$this->db->where('Status',1);
		$resultMentors = $this->db->get('mentoractionitemcomments');
		return $finalResult = array(
			'mentorActionComments' => $resultMentors->result(),
			'menteeActionComments' => $resultMentees->result()
		);
	}
	
	function addActionItemComment($data) {
		$insertData = array(
            'Comment' => $data->CommentDescription,
            'MeetingID' => $data->MeetingID
        );
        if($data->ActionItemType == 0){ //Mentor Action Item
            $insertData['MentorActionItemID'] = $data->ActionItemID;
            $table = 'mentoractionitemcomments';
        } else if($data->ActionItemType == 1){ //Mentee Action Item
            $insertData['MenteeActionItemID'] = $data->ActionItemID;
			$table = 'menteeactionitemcomments';
        }
		
		if($data->CommentID == 0 || $data->CommentID == '') {
			if ($this->db->insert($table, $insertData)) {
				return array((object) array('CommentID' => $this->db->insert_id()));
			} else {
				return false;
			}
		} else {
			if($data->ActionItemType == 0){ //Mentor Action Item
				$this->db->where('MentorActionItemCommentID', $data->CommentID);
			} else if($data->ActionItemType == 1){ //Mentee Action Item
				$this->db->where('MenteeActionItemCommentID', $data->CommentID);
			}
		
			if ($this->db->update($table, $insertData)){
				return array((object) array('CommentID' => 0));
			} else {
				return false;
			}
		}
	}

    function getSchoolList() {
        //$this->db->select("subdomain_id, subdomain_name, concat(UCASE(LEFT(subdomain_name, '1')),SUBSTRING(subdomain_name, '2'))");
        //$query = $this->db->get('mentor_subdomains');
        $query = $this->db->query("
			SELECT subdomain_id, subdomain_name, concat(UCASE(LEFT(subdomain_name, 1)),SUBSTRING(subdomain_name, 2)) as school_name
			FROM (mentor_subdomains) WHERE Status=1 AND subdomain_name != 'template'
			ORDER BY subdomain_name ASC
		");
        //echo $this->db->last_query();
        return $query->result();
    }
	
	function getSchoolName() {
        //$this->db->select("subdomain_id, subdomain_name, concat(UCASE(LEFT(subdomain_name, '1')),SUBSTRING(subdomain_name, '2'))");
        //$query = $this->db->get('mentor_subdomains');
        $query = $this->db->query("
			SELECT subdomain_name
			FROM mentor_subdomains WHERE Status=1
		");
        //echo $this->db->last_query();
        return $query->result();
    }
	
	function addUserNote($data) {
		$config = $this->getConfig();
		
		$title = explode("\n", $data->NoteDescription);
		
		$insertData = array(
			'MeetingID' => $data->NoteMeetingID,
			'NoteUserType' => $data->UserType,
			'NoteAuthor' => $data->UserID,
			//'NoteTitle' => substr($title[0], 0, $config[0]->noteTitleLength),
			'NoteDescription' => $data->NoteDescription,
			'Status' => 1,
			'Weight' => 1000,
			'UpdatedDate' => date('Y-m-d H:i:d')
		);
		
		if($data->NoteID == 0){
			$insertData['CreatedDate'] = date('Y-m-d H:i:d');
			if ($this->db->insert('notes', $insertData)){
				return array((object) array('NoteID' => $this->db->insert_id()));
			} else {
				return false;
			}
		} else {
			$this->db->where('NoteID', $data->NoteID);
			if ($this->db->update('notes', $insertData)){
				return array((object) array('NoteID' => 0));
			} else {
				return false;
			}
		}
	}
	
	function deleteUserNote($NoteID) {
		if($this->db->delete('notes', array('NoteID' => $NoteID))) {
			return true;
		} else {
			return false;
		}
	}
	
	function shareUserNote($Share, $NoteID) {
		$data = array(
			'Access' => ($Share == 'private') ? 0 : (($Share == 'public') ? 1 : 0)
		);
		$this->db->where('NoteID', $NoteID);
		
		if($this->db->update('notes', $data)) {
			return true;
		} else {
			return false;
		}
	}
	
	function getUserNotes($UserID, $UserType) {
		//$this->db->where('NoteUserType', $UserType);
		//$this->db->where('NoteAuthor', $UserID);
		//$this->db->or_where('Access', 1); 
		//$this->db->order_by("UpdatedDate", "desc"); 
		//$query = $this->db->get('mentor_notes');
		
		if($UserType == 0) {
			$query = $this->db->query("
				SELECT * FROM mentor_notes
				WHERE (NoteUserType =  $UserType AND NoteAuthor =  $UserID)
				OR Access = 1
				ORDER BY UpdatedDate desc
			");
		} else if($UserType == 1) {
			$query = $this->db->query("
				SELECT * FROM mentor_notes
				WHERE (NoteUserType =  $UserType AND NoteAuthor =  $UserID)
				ORDER BY UpdatedDate desc
			");
		}
		
        return $query->result();
	}
	
	function acceptRescheduledMeetingByMentor($invitationID){
		$details = array(
			'Status' => 'Accepted'
			);
		$this->db->where('InvitationId', $invitationID);
		if($this->db->update('invitation', $details)){
			return true;
		} else {
			return false;
		}
	}
	
	function getSourceDetail($usertype){
		if($usertype == '0') {
			$query = $this->db->query("
				SELECT MentorSourceID as SourceID, MentorSourceName as SourceName
				FROM (mentor_mentorsourcenames)
			");
		} else if($usertype == '1'){
			$query = $this->db->query("
				SELECT MenteeSourceID as SourceID, MenteeSourceName as SourceName
				FROM (mentor_menteesourcenames)
			");
		}
        //echo $this->db->last_query();
        return $query->result();
	}

    function checkSubdomain($domainName) {
        $query = $this->db->query("SELECT * FROM mentor_subdomains WHERE subdomain_name = '" . $domainName . "'");

        return $query->result();
    }

    public function getNewMeetingDetails($mentorId) {
        $query = $this->db->query("SELECT mi.MenteeId,mt.TopicDescription,mi.ManteeComments FROM mentor_invitation mi
            LEFT JOIN mentor_topic mt ON mi.TopicID=mt.TopicID
            WHERE mi.MentorID=$mentorId");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getMentorActionTakenDetail() {
        //$sql = "call  usp_GetMentorActionTaken()";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $query = $this->db->query('SELECT t.MentorActionID, t.MentorActionName FROM mentor_mentordefinedactions t WHERE t.Status != 0');
        return $query->result();
    }

    public function registerUserDetail($Email, $AccessToken, $UserType) {
        $sql = "call  usp_RegisterUser('" . $Email . "','" . $AccessToken . "','" . $UserType . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }

    public function checkLoginDetail($username, $accesstoken) {
        //$sql = "call usp_CheckLogin('" . $username . "','" . $accesstoken . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $userType = '';
        if ($this->checkMentorExist('', $username, '', FALSE)) {
            $userType = 'Mentor';
        } else if ($this->checkMenteeExist('', $username, '', FALSE)) {
            $userType = 'Mentee';
        }

        if (empty($userType)) {
            $return_result = array((object) array('Message' => "Username and/or password and/or school do not match. Please try again."));
        } else {
			$password	=	fnEncrypt($accesstoken, $this->config->item('mentorKey'));
            if ($userType == 'Mentor') {
                $result = $this->db->query("
                    SELECT m.MentorID AS Id, mci.IsSubadmin AS IsSubadmin, m.MentorName, mci.MentorPhone, mci.MentorEmail, mci.MentorStatus, 0 AS UserType,
                    IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_mentorskillset ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MentorID = m.MentorID),'') AS Skills,
                    mci.Password,mci.DisableSMS,CONCAT('http://melstm.net/mentor/assets/admin/images/admin/', mci.MentorImage) AS MentorImage,
					msn.MentorSourceID,msn.MentorSourceName
                    FROM mentor_mentor m 
                    LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorID = m.MentorID
					LEFT JOIN mentor_usertosource us ON us.MentorID = mci.MentorID
					LEFT JOIN mentor_mentorsourcenames msn ON us.MentorSourceID = msn.MentorSourceID
                    WHERE m.MentorName = '$username' AND mci.Password = '$password' AND mci.MentorStatus = 'Active'
                ")->result();
				//echo $password;
				//echo $this->db->last_query();exit;
                if (count($result) > 0) {
                    $return_result = $result;
                } else {
                    $return_result = array((object) array('Message' => "Username and/or password and/or school do not match. Please try again."));
                }
            } else if ($userType == 'Mentee') {
                $result = $this->db->query("
                    SELECT m.MenteeID AS Id, m.MenteeName, mc.MenteePhone, mc.MenteeEmail, mc.MenteeStatus, 1 AS UserType,
                    IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_menteeskill ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MenteeID = m.MenteeID),'') AS Skills,
                    mc.Password,mc.DisableSMS,CONCAT('http://melstm.net/mentor/assets/admin/images/admin/', mc.MenteeImage) AS MenteeImage,
					msn.MenteeSourceID,msn.MenteeSourceName
                    FROM mentor_mentee m
                    LEFT JOIN mentor_menteecontact mc ON mc.MenteeId = m.MenteeID
					LEFT JOIN mentor_menteetosource us ON us.MenteeID = mc.MenteeID
					LEFT JOIN mentor_menteesourcenames msn ON us.MenteeSourceID = msn.MenteeSourceID
                    WHERE m.MenteeName = '$username' AND mc.Password = '$password' AND mc.MenteeStatus = 'Active';
                ")->result();

                if (count($result) > 0) {
                    $return_result = $result;
                } else {
                    $return_result = array((object) array('Message' => "Username and/or password and/or school do not match. Please try again."));
                }
            }
        }
        return $return_result;
    }

    function forgotPasswordDetail($Email) {
        //$sql = "call usp_ForgotPassword('" . $Email . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $userType = '';
        $return_result = '';
        if ($this->checkMentorExist('', '', $Email, FALSE)) {
            $userType = 'Mentor';
        } else if ($this->checkMenteeExist('', '', $Email, FALSE)) {
            $userType = 'Mentee';
        }

        if ($userType != '') {
            if ($userType == 'Mentor') {
                $query = $this->db->query("SELECT mci.MentorContactInfoID FROM mentor_mentorcontactinfo mci WHERE mci.MentorEmail = '$Email' AND mci.MentorStatus = 'Active'");
                if ($query->num_rows() > 0) {
                    $return_result = $this->db->query("
                        SELECT m.MentorID AS Id, m.MentorName, mci.MentorPhone, mci.Password, mci.MentorEmail, mci.MentorStatus, 0 AS UserType,
						(SELECT subdomain_name FROM mentor_subdomains WHERE `status`=1 LIMIT 1) AS subdomainName
                        FROM mentor_mentor m 
                        LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorID = m.MentorID
                        WHERE mci.MentorEmail = '$Email'
                    ")->result();
                } else {
                    $return_result = array((object) array('Message' => "Email is not active please use the access token provided earlier to register first."));
                }
            } else {
                $query = $this->db->query("SELECT mc.MenteeContactID FROM mentor_menteecontact mc WHERE mc.MenteeEmail = '$Email'");
                if ($query->num_rows() > 0) {
                    $return_result = $this->db->query("
                        SELECT m.MenteeID AS Id, m.MenteeName, mc.MenteePhone, mc.Password, mc.MenteeEmail, mc.MenteeStatus, 1 AS UserType,
						(SELECT subdomain_name FROM mentor_subdomains WHERE `status`=1 LIMIT 1) AS subdomainName
                        FROM mentor_mentee m 
                        LEFT JOIN mentor_menteecontact mc ON mc.MenteeId = m.MenteeID
                        WHERE mc.MenteeEmail = '$Email'
                    ")->result();
                } else {
                    $return_result = array((object) array('Message' => "Email is not active please use the access token provided earlier to register first."));
                }
            }
        } else {
            $return_result = array((object) array('Message' => "Email not found."));
        }
        return $return_result;
    }

    /* 21-8-2015 */

    function checkInvitationDetail($MentorID, $MenteeID) {
        //$sql = "call usp_checkInvitationDetail('" . $MentorID . "','" . $MenteeID . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $query = $this->db->query("
            SELECT mentor.MentorEmail,mi.InvitationID,mmentor.MentorName,mmantee.MenteeName,mantee.MenteeEmail,mt.TopicDescription,mantee.MenteePhone,mentor.MentorPhone FROM mentor_invitation mi
            LEFT JOIN mentor_menteecontact mantee ON mi.MenteeID = mantee.MenteeId
            LEFT JOIN mentor_mentorcontactinfo mentor ON mentor.MentorID = mi.MentorID
            LEFT JOIN mentor_mentee mmantee ON mmantee.MenteeID = mi.MenteeID
            LEFT JOIN mentor_mentor mmentor ON mmentor.MentorID = mi.MentorID
            LEFT JOIN mentor_topic mt ON mt.TopicID = mi.TopicID
            WHERE mi.MentorID = '$MentorID' AND mi.MenteeID = '$MenteeID' and mi.Status ='Pending'
        ");
        return $query->result();
    }

    /**/

    function changePasswordDetail($Email, $OldPassword, $Password, $usertype) {
        $oldPwd = (int) $OldPassword;
        $sql = "call usp_ChangePassword('" . $Email . "','" . $oldPwd . "','" . $Password . "','" . $usertype . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        //print_r($query->result());exit;
        return $query->result();
    }

    function submitMeetingInfoDetail($MeetingID, $MentorID, $MenteeID, $MeetingStartDatetime, $MeetingEndDatetime, $MeetingTypeID, $MeetingTopicID, $MeetingSubTopicID, $MeetingPlaceID, $MenteeActionIDs, $MeetingElapsedTime, $MeetingFeedback, $MentorActionItemDone, $MenteeActionItemDone) {
//        $sql = "call usp_SubmitMeetingInfo('" . $MeetingID . "','" . $MentorID . "','" . $MenteeID . "','" . $MeetingStartDatetime . "','" . $MeetingEndDatetime . "','" . $MeetingTypeID . "','" . $MeetingTopicID . "','" . $MeetingSubTopicID . "','" . $MeetingPlaceID . "','" . $MenteeActionIDs . "','" . $MeetingElapsedTime . "','" . $MeetingFeedback . "','" . $MentorActionItemDone . "','" . $MenteeActionItemDone . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();


        $data = array(
            'MentorID' => $MentorID,
            'MenteeID' => $MenteeID,
            'MeetingStartDatetime' => $MeetingStartDatetime,
            'MeetingEndDatetime' => $MeetingEndDatetime,
            'MeetingTypeID' => $MeetingTypeID,
            'MeetingTopicID' => $MeetingTopicID,
            'MeetingSubTopicID' => $MeetingSubTopicID,
            'MeetingPlaceID' => $MeetingPlaceID,
            'MeetingElapsedTime' => $MeetingElapsedTime,
            'MeetingFeedback' => $MeetingFeedback
        );

        if ($MeetingID == 0) {
            if ($this->db->insert('mentor_meetinginfo', $data)) {
                return array((object) array('InsertedId' => $this->db->insert_id()));
            } else {
                return array((object) array('Message' => 'There is issue in inserting meeting'));
            }
        } else {
            $data['MentorActionItemDone'] = $MentorActionItemDone;
            $data['MenteeActionItemDone'] = $MenteeActionItemDone;
            $data['Meeting_status'] = '3';
            $this->db->where('MeetingID', $MeetingID);
            if ($this->db->update('mentor_meetinginfo', $data)) {
                return array((object) array('InsertedId' => $MeetingID));
            } else {
                return array((object) array('Message' => 'There is issue in updating meeting'));
            }
        }
    }

    function addMenteeactionstakenDetail($MenteeID, $MeetingID, $ActionId) {
        //$sql = "call usp_AddMenteeactionstaken('" . $MenteeID . "','" . $MeetingID . "','" . $ActionId . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $data = array(
            'MenteeID' => $MenteeID,
            'MeetingID' => $MeetingID,
            'MenteeActionID' => $ActionId
        );
        $this->db->insert('mentor_menteeactionstaken', $data);
        $insert_id = $this->db->insert_id();
    }

    function addMentoractionstakenDetail($MentorID, $MeetingID, $ActionId) {
        //$sql = "call usp_AddMentoractionstaken('" . $MentorID . "','" . $MeetingID . "','" . $ActionId . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $data = array(
            'MentorID' => $MentorID,
            'MeetingID' => $MeetingID,
            'MentorActionID' => $ActionId
        );
        $this->db->insert('mentor_mentoractionstaken', $data);
        return $this->db->insert_id();
    }

    /* added by khushbu 18-08-2015 */

    function addMentorsubtopicsIDs($MentorID, $MeetingID, $ActionId) {
        //$sql = "call usp_AddMentorsubtopicsIDs('" . $MentorID . "','" . $MeetingID . "','" . $ActionId . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $data = array(
            'MentorID' => $MentorID,
            'MeetingID' => $MeetingID,
            'MentorSubTopicsID' => $ActionId
        );
        $this->db->insert('mentor_subTopicsIDs', $data);
        return $this->db->insert_id();
    }

    function getMentorsubtopicsIDs($MeetingID) {
        $this->db->select('*');
        $this->db->from('mentor_subTopicsIDs');
        $this->db->where('MeetingID', $MeetingID);
        mysqli_next_result($this->db->conn_id);
        $mentorData = $this->db->get();

        return $mentorData->result();
    }

    function updateMentorsubtopicsIDs($mentorSubIdResultsIds, $MeetingID) {
//        $sql = "call usp_updateMentorsubtopicsIDs('" . $mentorSubIdResultsIds . "', " . $MeetingID . ")";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();

        $data = array(
            'MeetingSubTopicID' => $mentorSubIdResultsIds
        );
        $this->db->where('MeetingID', $MeetingID);
        $this->db->update('mentor_meetinginfo', $data);
        return true;
    }

    /* khushbu */

    public function getMentorDetail($MenteeID) {
//        $sql = "call usp_GetMentor('" . $MenteeID . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        return $query->result();

        $query = $this->db->query("
            SELECT m.MenteeID,m.MenteeName, r.*, mentor.* 
            FROM (mentor_mentee m) 
            LEFT JOIN mentor_relationship r ON r.MenteeID= m.MenteeID 
            LEFT JOIN mentor_mentor mentor ON mentor.MentorID = r.MentorID 
            WHERE m.MenteeID =$MenteeID AND m.Status = 1
            ORDER BY mentor.MentorName ASC
        ");
        return $query->result();
    }
	
	function updateMentorCommentDetail($data) {
        $query = $this->db->query("(SELECT MentorCommentID FROM mentor_mentorcomment  WHERE MentorID = {$data->MentorID} AND MeetingID = {$data->MeetingID} AND MenteeID= {$data->MenteeID})");
        if ($query->num_rows() > 0) {
			
			$meeting_data = array(
				'MeetingTypeID' => $data->MeetingTypeID,
				'MeetingPlaceID' => $data->MeetingPlaceID,
				'MeetingTopicID' => $data->MeetingTopicID,
				'MeetingStartDatetime' => $data->MeetingStartTime,
				'MeetingEndDatetime' => $data->MeetingEndTime,
				'MeetingElapsedTime' => $data->MeetingElapsedTime,
				'MentorActionItemDone' => $data->MentorActionItemIDDone,
				'Meeting_status' => '3'
			);
			$this->db->where('MentorID', $data->MentorID);
            $this->db->where('MeetingID', $data->MeetingID);
			$this->db->where('MeetingID', $data->MeetingID);
			$this->db->update('mentor_meetinginfo', $meeting_data);
            $details = array(
                'MentorID' => $data->MentorID,
                'MenteeID' => $data->MenteeID,
                'MeetingID' => $data->MeetingID,
                'MentorComment' => $data->MentorComment,
                'SatisfactionIndex' => $data->SatisfactionIndex
            );

            $this->db->where('MentorID', $data->MentorID);
            $this->db->where('MeetingID', $data->MeetingID);
            $this->db->update('mentor_mentorcomment', $details);

            $response = array((object) array(
                    'InsertedId' => $data->MeetingID
                )
            );
        } else {
			$meeting_data = array(
				'MeetingTypeID' => $data->MeetingTypeID,
				'MeetingPlaceID' => $data->MeetingPlaceID,
				'MeetingTopicID' => $data->MeetingTopicID,
				'MeetingStartDatetime' => $data->MeetingStartTime,
				'MeetingEndDatetime' => $data->MeetingEndTime,
				'MeetingElapsedTime' => $data->MeetingElapsedTime,
				'MentorActionItemDone' => $data->MentorActionItemIDDone,
				'Meeting_status' => '3'
			);
			$this->db->where('MentorID', $data->MentorID);
            $this->db->where('MeetingID', $data->MeetingID);
			$this->db->where('MeetingID', $data->MeetingID);
			$this->db->update('mentor_meetinginfo', $meeting_data);
			
            $details = array(
                'MentorID' => $data->MentorID,
                'MenteeID' => $data->MenteeID,
                'MeetingID' => $data->MeetingID,
                'MentorComment' => $data->MentorComment,
                'SatisfactionIndex' => $data->SatisfactionIndex
            );
            $insert_id = $this->db->insert('mentor_mentorcomment', $details);
            $response = array((object) array(
                    'InsertedId' => $data->MeetingID
                )
            );
        }
        return $response;
    }

    function mentorCommentDetail($MentorID, $MenteeID, $MeetingID, $MentorComment, $SatisfactionIndex) {
        //$sql = "call usp_AddMentorComment('" . $MentorID . "','" . $MenteeID . "','" . $MeetingID . "','" . $MentorComment . "','" . $SatisfactionIndex . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $query = $this->db->query("(SELECT MentorCommentID FROM mentor_mentorcomment  WHERE MentorID = $MentorID AND MeetingID = $MeetingID)");
        if ($query->num_rows() > 0) {
            $data = array(
                'MentorID' => $MentorID,
                'MenteeID' => $MenteeID,
                'MeetingID' => $MeetingID,
                'MentorComment' => $MentorComment,
                'SatisfactionIndex' => $SatisfactionIndex
            );
            $this->db->where('MentorID', $MentorID);
            $this->db->where('MeetingID', $MeetingID);
            $this->db->update('mentor_mentorcomment', $data);
            $response = array((object) array(
                    'InsertedId' => 1
                )
            );
        } else {
            $data = array(
                'MentorID' => $MentorID,
                'MenteeID' => $MenteeID,
                'MeetingID' => $MeetingID,
                'MentorComment' => $MentorComment,
                'SatisfactionIndex' => $SatisfactionIndex
            );
            $insert_id = $this->db->insert('mentor_mentorcomment', $data);
            $response = array((object) array(
                    'InsertedId' => $insert_id
                )
            );
        }
        return $response;
    }

    function mentorInvitationDetail($MentorID, $MenteeID, $TopicID, $MenteeComments, $MenteeEmail, $MenteePhone, $SmsCapable, $FollowUpMeetingFor) {

        $query = $this->db->query("SELECT i.InvitationId FROM mentor_invitation i WHERE i.MentorID = $MentorID AND i.MenteeID = $MenteeID AND i.Status!='Rejected' AND i.Status!='Cancelled' AND i.Status!='TimedOut'");
        if ($query->num_rows() > 0) {
            $response = array((object) array(
                    'Message' => 'Invitation already sent.'
                )
            );
        } else {
            $data = array(
                'MentorID' => $MentorID,
                'MenteeID' => $MenteeID,
                'TopicID' => $TopicID,
                'ManteeComments' => $MenteeComments,
                'inviteTime' => date('Y-m-d H:i:s'),
                'InviteeEmail' => $MenteeEmail,
                'InviteePhone' => $MenteePhone,
                'SmsCapable' => $SmsCapable,
				'InitiatedBy' => 1,
				'FollowUpMeetingFor' => $FollowUpMeetingFor
				
            );
            $insert_id = $this->db->insert('mentor_invitation', $data);
            $response = array((object) array(
                    'InsertedId' => $insert_id
                )
            );
        }
        return $response;
    }
	
	function menteeInvitationDetail($MentorID, $MenteeID, $TopicID, $MentorComments, $MentorEmail, $MentorPhone, $SmsCapable, $MeetingDateTime, $MeetingType, $MeetingPlace, $MentorAcceptResponsesName) {

        $query = $this->db->query("SELECT i.InvitationId FROM mentor_invitation i WHERE i.MentorID = $MentorID AND i.MenteeID = $MenteeID AND i.Status!='Rejected' AND i.Status!='Cancelled' AND i.Status!='TimedOut'");
        if ($query->num_rows() > 0) {
            $response = array((object) array(
                    'Message' => 'Invitation already sent.'
                )
            );
        } else {
            $data = array(
                'MentorID' => $MentorID,
                'MenteeID' => $MenteeID,
                'TopicID' => $TopicID,
                'MentorComments' => $MentorComments,
                'inviteTime' => date('Y-m-d H:i:s'),
                'InviteeEmail' => $MentorEmail,
                'InviteePhone' => $MentorPhone,
                'SmsCapable' => $SmsCapable,
				'MeetingDateTime' => $MeetingDateTime,
				'InitiatedBy' => 0,
				'MeetingTypeID' => $MeetingType,
				'MeetingPlaceID' => $MeetingPlace,
				'AcceptResponsesName' => $MentorAcceptResponsesName
            );
            $insert_id = $this->db->insert('mentor_invitation', $data);
            $response = array((object) array(
                    'InsertedId' => $insert_id
                )
            );
        }
        return $response;
    }

    function getMentorForInvitation() {
        $this->db->select('*');
		$this->db->where('Status', 1);
        $this->db->from('mentor_mentor');
		$this->db->order_by("Weight", "asc");
		$this->db->order_by("MentorName", "asc");
        mysqli_next_result($this->db->conn_id);
        $mentorData = $this->db->get();

        return $mentorData->result();
    }
	
	function getMenteeForInvitation() {
        $this->db->select('*');
		$this->db->where('Status', 1);
        $this->db->from('mentor_mentee');
		//$this->db->order_by("Weight", "asc");
		$this->db->order_by("MenteeName", "asc");
        mysqli_next_result($this->db->conn_id);
        $mentorData = $this->db->get();

        return $mentorData->result();
    }

    function getMentorForInvitationStatus($MentorID, $MenteeID) {
        //$sql = "call usp_getMentorForInvitation('" . $MentorID . "','" . $MenteeID . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();


        $query = $this->db->query("SELECT i.InvitationId FROM mentor_invitation i WHERE i.MentorID = $MentorID AND i.MenteeID = $MenteeID AND `Status` != 'Rejected' AND `Status` != 'Cancelled' AND `Status` != 'TimedOut'");
        if ($query->num_rows() > 0) {
//            $query1 = $this->db->query("SELECT m.MentorID, m.MentorName,'TRUE' as rStatus FROM mentor_mentor m WHERE m.MentorID = $MentorID");
            return FALSE;
        } else {
            $query1 = $this->db->query("SELECT m.MentorID, m.MentorName,'FALSE' as rStatus FROM mentor_mentor m WHERE m.MentorID = $MentorID");
        }
        return $query1->result();
    }
	
	function getMenteeForInvitationStatus($MenteeID, $MentorID) {
        $query = $this->db->query("SELECT i.InvitationId FROM mentor_invitation i WHERE i.MentorID = $MentorID AND i.MenteeID = $MenteeID AND `Status` != 'Rejected' AND `Status` != 'Cancelled' AND `Status` != 'TimedOut'");
        if ($query->num_rows() > 0) {
//            $query1 = $this->db->query("SELECT m.MentorID, m.MentorName,'TRUE' as rStatus FROM mentor_mentor m WHERE m.MentorID = $MentorID");
            return FALSE;
        } else {
            $query1 = $this->db->query("SELECT m.MenteeID, m.MenteeName,'FALSE' as rStatus FROM mentor_mentee m WHERE m.MenteeID = $MenteeID");
        }
        return $query1->result();
    }

    function startEndMeetingSession($MenteeID, $MentorID, $DateTime) {
        $this->db->select('*');
        $this->db->from('mentor_meetinginfo');
        $this->db->where('MenteeID', $MenteeID);
        $this->db->where('MentorID', $MentorID);
        mysqli_next_result($this->db->conn_id);
        $mentorData = $this->db->get();

        return $mentorData->num_rows();
    }

    function addStartMeetingSession($MenteeID, $MentorID, $DateTime, $MettingStatus, $MeetingTypeID, $MeetingPlaceID, $MainTopicID, $FollowUpMeetingFor) {
        //$sql = "call usp_addStartEndMeetingSession('" . $MentorID . "','" . $MenteeID . "','" . $DateTime . "','" . $MettingStatus . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $query = $this->db->query("SELECT ManteeComments FROM `mentor_invitation` WHERE MentorID=$MentorID AND MenteeID=$MenteeID  AND (Status='Accepted' OR Status='Rescheduled') ");
        if ($query->num_rows() > 0) {
            $result = $query->result();
            $data = array(
                'MentorID' => $MentorID,
                'MenteeID' => $MenteeID,
                'MeetingStartDatetime' => $DateTime,
                'Meeting_status' => $MettingStatus,
                'RawStartDatetime' => $DateTime,
                'MenteeInvitationComment' => $result[0]->ManteeComments,
                'MeetingTypeID' => $MeetingTypeID,
                'MeetingPlaceID' => $MeetingPlaceID,
                'MeetingTopicID' => $MainTopicID,
				'FollowUpMeetingFor' => $FollowUpMeetingFor
            );
            $this->db->insert('mentor_meetinginfo', $data);
            $insert_id = $this->db->insert_id();

            $this->db->where('MentorID', $MentorID);
            $this->db->where('MenteeID', $MenteeID);
            $this->db->delete('mentor_invitation');
            $response = array((object) array('LastId' => "$insert_id"));
        } else {
            $response = array((object) array('LastId' => '0'));
        }
        return $response;
    }

    /* khushbu 19-8 */
    /* khushbu 20-8 */

    function updateStartMeetingSession($DateTime, $MettingStatus, $MentorID, $MenteeID, $MeetingTypeID, $MeetingPlaceID, $MainTopicID) {
//        $sql = "call usp_updateStartEndMeetingSession('" . $DateTime . "','" . $MettingStatus . "','" . $MentorID . "','" . $MenteeID . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();


        $data = array(
            'MeetingStartDatetime' => $DateTime,
            'Meeting_status' => $MettingStatus,
            'MeetingTypeID' => $MeetingTypeID,
            'MeetingPlaceID' => $MeetingPlaceID,
            'MeetingTopicID' => $MainTopicID
        );
        $this->db->where('MenteeID', $MenteeID);
        $this->db->where('MentorID', $MentorID);
        $this->db->where('Meeting_status !=', '3');
        $this->db->update('mentor_meetinginfo', $data);
        return true;
    }

    function updateEndMeetingSession($DateTime, $MettingStatus, $MentorID, $MenteeID, $ElapsedTime) {
//        $sql = "call usp_updateEndMeetingSession('" . $DateTime . "','" . $MettingStatus . "','" . $MentorID . "','" . $MenteeID . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();

        $data = array(
            'MeetingEndDatetime' => $DateTime,
            'Meeting_status' => $MettingStatus,
            'RawEndDatetime' => $DateTime,
            'MeetingElapsedTime' => $ElapsedTime
        );
        $this->db->where('MenteeID', $MenteeID);
        $this->db->where('MentorID', $MentorID);
        $this->db->where('Meeting_status !=', '3');
        $this->db->update('mentor_meetinginfo', $data);
        return true;
    }

    function getMeetingSessionIDforSubmit($MenteeID, $MentorID, $MeetingID) {

        if ($MeetingID == 0) {
            $this->db->select('*');
            $this->db->from('mentor_meetinginfo');
            $this->db->where('MenteeID', $MenteeID);
            $this->db->where('MentorID', $MentorID);
            $this->db->where('Meeting_status !=', '3');
        } else {
            $this->db->select('*');
            $this->db->from('mentor_meetinginfo');
            $this->db->where('MeetingID', $MeetingID);
        }

        mysqli_next_result($this->db->conn_id);
        $mentorData = $this->db->get();


        //echo $this->db->last_query();exit;    
        return $mentorData->result();
    }

    function getMeetingSessionID($MenteeID, $MentorID, $MainTopicID) {
        //echo $MenteeID;echo $MentorID;exit;
        $this->db->select('*');
        $this->db->from('mentor_meetinginfo');
        $this->db->where('MenteeID', $MenteeID);
        $this->db->where('MentorID', $MentorID);
		$this->db->where('MeetingTopicID', $MainTopicID);
        $this->db->where('Meeting_status !=', '3');
        mysqli_next_result($this->db->conn_id);
        $mentorData = $this->db->get();

        //echo $this->db->last_query();exit;    
        return $mentorData->result();
    }

    function getMentorWaitScreen($MenteeID) {
        $this->db->select('*');
        $this->db->from('mentor_invitation');
        $this->db->where('MenteeID', $MenteeID);
        $this->db->order_by("inviteTime", "desc");
        mysqli_next_result($this->db->conn_id);
        $mentorData = $this->db->get();


        //echo $this->db->last_query();exit;    
        return $mentorData->result();
    }

    function getMentorWaitScreenDetails($MentorID, $MenteeID, $InvitationId) {
//        $sql = "call usp_getMentorWaitScreen('" . $MentorID . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->row_array();

        $query = $this->db->query("
            SELECT i.InvitationId, i.InitiatedBy, i.Status, i.inviteTime, i.MentorComments, t.TopicDescription, CONCAT('http://melstm.net/mentor/assets/admin/images/admin/', mci.MentorImage) AS MentorImage,
            IFNULL(i.MenteeID, '') AS MenteeID, IFNULL(i.MentorID, '') AS MentorID, IFNULL(m.MentorName, '') AS MentorName,
            i.InviteeEmail, i.InviteePhone, i.SmsCapable, i.ManteeComments AS MenteeComment,mt.MeetingTypeName,mp.MeetingPlaceName,i.MeetingDateTime,mci.MentorEmail,mci.DisableSMS As MentorSmsCapable,
			mci.MentorPhone,i.cancellationReason,i.rescheduledBy,i.cancelledBy,i.RescheduleComments,i.AcceptOrRejectTime,i.MeetingPlaceID,i.MeetingTypeID,i.AcceptResponsesName,i.DeclineResponsesName
            FROM mentor_invitation i
            LEFT JOIN mentor_mentor m ON m.MentorID = i.MentorID
            LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorId = i.MentorId
            LEFT JOIN mentor_topic t ON t.TopicID = i.TopicID
            LEFT JOIN mentor_meetingplace mp ON mp.MeetingPlaceID = i.MeetingPlaceID
            LEFT JOIN mentor_meetingtype mt ON mt.MeetingTypeID = i.MeetingTypeID
            WHERE i.MentorID = $MentorID AND i.MenteeID = $MenteeID AND i.InvitationId = $InvitationId
        ");
        return $query->row_array();
    }

    function getMettingWaitScreenInfo($MentorID, $MenteeID) {
//        $sql = "call usp_getMettingWaitScreenInfo('" . $MentorID . "','" . $MenteeID . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();


        $query = $this->db->query("
            SELECT i.Meeting_status ,m.Status
            FROM mentor_meetinginfo i
            LEFT JOIN mentor_invitation m ON m.MentorID = i.MentorID AND m.MenteeID = i.MenteeID
            WHERE i.MentorID =  $MentorID AND i.MenteeID = $MenteeID
        ");
        return $query->result();
    }

    /* 20-8 */

    function getMentorPendingRequestDetail($MentorID) {
        //$sql = "call usp_GetMentorPendingRequest('" . $MentorID . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();
		//(i.Status = 'Pending' or i.Status = 'Rescheduled')
        $query = $this->db->query("SELECT i.InvitationId FROM mentor_invitation i WHERE
				(i.MentorID = $MentorID AND i.rescheduledby=1 AND i.Status = 'Rescheduled')
				OR 
				(i.MentorID = $MentorID AND (i.Status = 'Pending' AND i.InitiatedBy != 0))

				#(i.MentorID = $MentorID AND (i.InitiatedBy = 0 OR i.rescheduledby=0) AND (i.Status = 'Rescheduled')) OR 
				#(i.MentorID = $MentorID AND (i.InitiatedBy = 1 OR i.rescheduledby=1) AND (i.Status = 'Pending'  OR i.Status = 'Rescheduled'))"
			);
        if ($query->num_rows()) {
            /*$query = $this->db->query("
                SELECT i.InvitationId,i.ManteeComments,mt.TopicDescription, IFNULL(i.MenteeID,'') AS MenteeID, IFNULL(me.MenteeName,'') AS MenteeName,
                (CONCAT('http://melstm.net/mentor/assets/admin/images/admin/',mci.MenteeImage)) AS MenteeImage, i.inviteTime,i.InviteeEmail,i.InviteePhone,i.SmsCapable,i.rescheduledBy,i.RescheduleComments,i.MeetingDateTime,i.Status
                FROM mentor_invitation i 
                LEFT JOIN mentor_mentee me ON me.MenteeID = i.MenteeID
                LEFT JOIN mentor_menteecontact mci ON mci.MenteeID = me.MenteeID
                LEFT JOIN mentor_topic mt ON mt.TopicID	= i.TopicID
                WHERE i.MentorID = $MentorID  AND (i.Status = 'Pending'  OR i.Status = 'Rescheduled' AND i.rescheduledby=1) OR (i.InitiatedBy = 1 OR i.rescheduledby=1)
				ORDER BY i.inviteTime asc
            ");*/
			
			$query = $this->db->query("
				SELECT IFNULL(i.MentorID,'') AS MentorID,IFNULL(i.MenteeID,'') AS MenteeID, i.InvitationId,i.ManteeComments,mt.TopicDescription, IFNULL(me.MenteeName,'') AS MenteeName,
				(CONCAT('http://melstm.net/mentor/assets/admin/images/admin/',mci.MenteeImage)) AS MenteeImage, i.inviteTime,i.InviteeEmail,i.InviteePhone,i.SmsCapable,i.rescheduledBy,i.RescheduleComments,i.MeetingDateTime,i.Status
				FROM mentor_invitation i 
				LEFT JOIN mentor_mentee me ON me.MenteeID = i.MenteeID
				LEFT JOIN mentor_menteecontact mci ON mci.MenteeID = me.MenteeID
				LEFT JOIN mentor_topic mt ON mt.TopicID	= i.TopicID
				WHERE
				(i.MentorID = $MentorID AND i.rescheduledby=1 AND i.Status = 'Rescheduled')
				OR 
				(i.MentorID = $MentorID AND (i.Status = 'Pending' AND i.InitiatedBy != 0))
				#(i.MentorID = $MentorID AND (i.InitiatedBy = 0 OR i.rescheduledby=0) AND (i.Status = 'Rescheduled')) OR 
				#(i.MentorID = $MentorID AND (i.InitiatedBy = 1 OR i.rescheduledby=1) AND (i.Status = 'Pending'  OR i.Status = 'Rescheduled'))
				ORDER BY i.inviteTime ASC
			");
			
            $response = $query->result();
        } else {
            $response = array((object) array(
                    'Message' => 'No invitation found.'
                )
            );
        }
        return $response;
    }

    function getMentorPendingMeetingDetail($MentorID) {

		//OR (i.Status = 'Rescheduled' AND i.rescheduledby=0)
        $query = $this->db->query("SELECT i.InvitationId FROM mentor_invitation i WHERE
			(i.MentorID = $MentorID AND i.rescheduledby=0 AND i.Status = 'Rescheduled')
			OR 
			(i.MentorID = $MentorID AND i.Status = 'Accepted')
			OR
			(i.MentorID = $MentorID AND i.Status = 'Pending' AND i.InitiatedBy=0)

			#(i.MentorID = $MentorID AND (i.InitiatedBy = 0) AND (i.Status != 'Rejected' AND i.Status != 'Cancelled')) OR 
			#(i.MentorID = $MentorID AND (i.rescheduledby=0) AND ((i.Status = 'Accepted'  OR i.Status = 'Rescheduled') AND i.Status != 'Cancelled'))

			#(i.MentorID = $MentorID AND (i.InitiatedBy = 1 OR i.rescheduledby=0) AND ((i.Status = 'Accepted'  OR i.Status = 'Rescheduled') AND i.Status != 'Cancelled'))
		");
        if ($query->num_rows()) {
            /*$query = $this->db->query("
                SELECT i.InvitationId,i.InitiatedBy,i.ManteeComments,i.TopicID,mt.TopicDescription, IFNULL(i.MenteeID,'') AS MenteeID, IFNULL(me.MenteeName,'') AS MenteeName,
                (CONCAT('http://melstm.net/mentor/assets/admin/images/admin/',mci.MenteeImage)) AS MenteeImage, i.inviteTime,i.InviteeEmail,i.InviteePhone,i.SmsCapable,i.MentorComments,i.MeetingPlaceID,IFNULL(mp.MeetingPlaceName,'') as MeetingPlaceName,i.MeetingDateTime,i.MeetingTypeID,IFNULL(mtype.MeetingTypeName,'') as MeetingTypeName,i.rescheduledBy,i.cancelledBy,i.RescheduleComments,i.cancellationReason,i.Status
                FROM mentor_invitation i 
                LEFT JOIN mentor_mentee me ON me.MenteeID = i.MenteeID
                LEFT JOIN mentor_menteecontact mci ON mci.MenteeID = me.MenteeID
                LEFT JOIN mentor_topic mt ON mt.TopicID	= i.TopicID
				LEFT JOIN mentor_meetingplace mp ON mp.MeetingPlaceID = i.MeetingPlaceID
				LEFT JOIN mentor_meetingtype mtype ON mtype.MeetingTypeID = i.MeetingTypeID
                WHERE i.MentorID = $MentorID AND (i.Status = 'Accepted'  OR i.Status = 'Rescheduled' AND i.rescheduledby=0) OR (InitiatedBy = 0 AND i.Status != 'Rejected' AND i.rescheduledby=0)
				ORDER BY i.MeetingDateTime asc
            ");*/
			
			$query = $this->db->query("
				SELECT i.FollowUpMeetingFor,i.InvitationId,i.InitiatedBy,i.ManteeComments,i.TopicID,mt.TopicDescription, IFNULL(i.MenteeID,'') AS MenteeID, IFNULL(me.MenteeName,'') AS MenteeName,
				(CONCAT('http://melstm.net/mentor/assets/admin/images/admin/',mci.MenteeImage)) AS MenteeImage, i.inviteTime,i.InviteeEmail,i.InviteePhone,i.SmsCapable,i.MentorComments,i.MeetingPlaceID,IFNULL(mp.MeetingPlaceName,'') AS MeetingPlaceName,i.MeetingDateTime,i.MeetingTypeID,IFNULL(mtype.MeetingTypeName,'') AS MeetingTypeName,i.rescheduledBy,i.cancelledBy,i.RescheduleComments,i.cancellationReason,i.Status
				FROM mentor_invitation i 
				LEFT JOIN mentor_mentee me ON me.MenteeID = i.MenteeID
				LEFT JOIN mentor_menteecontact mci ON mci.MenteeID = me.MenteeID
				LEFT JOIN mentor_topic mt ON mt.TopicID	= i.TopicID
				LEFT JOIN mentor_meetingplace mp ON mp.MeetingPlaceID = i.MeetingPlaceID
				LEFT JOIN mentor_meetingtype mtype ON mtype.MeetingTypeID = i.MeetingTypeID
				WHERE 
				(i.MentorID = $MentorID AND i.rescheduledby=0 AND i.Status = 'Rescheduled')
			OR 
			(i.MentorID = $MentorID AND i.Status = 'Accepted')
			OR
			(i.MentorID = $MentorID AND i.Status = 'Pending' AND i.InitiatedBy=0)
				#(i.MentorID = $MentorID AND (i.InitiatedBy = 0) AND (i.Status != 'Rejected')) OR 
				#(i.MentorID = $MentorID AND (i.InitiatedBy = 1 OR i.rescheduledby=0) AND (i.Status = 'Accepted'  OR i.Status = 'Rescheduled'))
				ORDER BY i.MeetingDateTime ASC
			");
            $response = $query->result();
        } else {
            $response = array((object) array(
                    'Message' => 'No Pending Meetings found'
                )
            );
        }
        return $response;
    }

    function mentorResponseDetail($details) {
//        $sql = "call usp_MentorResponse('" . $InvitationId . "','" . $Status . "','" . $Comments . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();

        $query = $this->db->query("SELECT COUNT(i.InvitationId) FROM mentor_invitation i WHERE i.InvitationId = {$details->InvitationId}");
        if ($query->num_rows() > 0) {
            $data = array(
                'Status' => $details->Status,
                'AcceptOrRejectTime' => date('Y-m-d H:i:s'),
                'MentorComments' => $details->Comments
            );
            if (isset($details->MeetingPlaceID))
                $data['MeetingPlaceID'] = $details->MeetingPlaceID;
            if (isset($details->MeetingDateTime))
                $data['MeetingDateTime'] = $details->MeetingDateTime;
			if (isset($details->MeetingTypeID))
                $data['MeetingTypeID'] = $details->MeetingTypeID;
			if (isset($details->MentorAcceptResponsesName)){
				$data['AcceptResponsesName'] = $details->MentorAcceptResponsesName;
				$data['DeclineResponsesName'] = "";
			}
			if (isset($details->MentorDeclineResponsesName)) {
				$data['AcceptResponsesName'] = "";
                $data['DeclineResponsesName'] = $details->MentorDeclineResponsesName;
			}

            $this->db->where('InvitationId', $details->InvitationId);
            $this->db->update('mentor_invitation', $data);

            $invitation = $this->db->query("SELECT MentorID,MenteeID FROM mentor_invitation WHERE InvitationId = {$details->InvitationId}")->result();

            $relationship = $this->db->query("SELECT mr.RelationshipID FROM mentor_relationship mr WHERE mr.MentorID={$invitation[0]->MentorID} AND mr.MenteeID={$invitation[0]->MenteeID}");
            if ($relationship->num_rows() > 0) {
                $response = array((object) array(
                        'InsertedId' => '1'
                    )
                );
            } else {
                $data = array(
                    'MentorID' => $invitation[0]->MentorID,
                    'MenteeID' => $invitation[0]->MenteeID
                );
                $this->db->insert('mentor_relationship', $data);
                $response = array((object) array(
                        'InsertedId' => '1'
                    )
                );
            }
        } else {
            $response = array((object) array(
                    'Message' => 'No invitation found.'
                )
            );
        }
        return $response;
    }
	
	function menteeResponseDetail($details) {
        $query = $this->db->query("SELECT COUNT(i.InvitationId) FROM mentor_invitation i WHERE i.InvitationId = {$details->InvitationId}");
        if ($query->num_rows() > 0) {
            $data = array(
                'Status' => $details->Status,
                'AcceptOrRejectTime' => date('Y-m-d H:i:s'),
                'ManteeComments' => $details->Comments
            );
            if (isset($details->MeetingPlaceID))
                $data['MeetingPlaceID'] = $details->MeetingPlaceID;
            if (isset($details->MeetingDateTime))
                $data['MeetingDateTime'] = $details->MeetingDateTime;
			if (isset($details->MeetingTypeID))
                $data['MeetingTypeID'] = $details->MeetingTypeID;
			if (isset($details->MenteeAcceptResponsesName)){
				$data['AcceptResponsesName'] = $details->MenteeAcceptResponsesName;
				$data['DeclineResponsesName'] = "";
			}
			if (isset($details->MenteeDeclineResponsesName)) {
				$data['AcceptResponsesName'] = "";
                $data['DeclineResponsesName'] = $details->MenteeDeclineResponsesName;
			}

            $this->db->where('InvitationId', $details->InvitationId);
            $this->db->update('mentor_invitation', $data);

            $invitation = $this->db->query("SELECT MentorID,MenteeID FROM mentor_invitation WHERE InvitationId = {$details->InvitationId}")->result();

            $relationship = $this->db->query("SELECT mr.RelationshipID FROM mentor_relationship mr WHERE mr.MentorID={$invitation[0]->MentorID} AND mr.MenteeID={$invitation[0]->MenteeID}");
            if ($relationship->num_rows() > 0) {
                $response = array((object) array(
                        'InsertedId' => '1'
                    )
                );
            } else {
                $data = array(
                    'MentorID' => $invitation[0]->MentorID,
                    'MenteeID' => $invitation[0]->MenteeID
                );
                $this->db->insert('mentor_relationship', $data);
                $response = array((object) array(
                        'InsertedId' => '1'
                    )
                );
            }
        } else {
            $response = array((object) array(
                    'Message' => 'No invitation found.'
                )
            );
        }
        return $response;
    }

    function updateTeamPicture($path, $teamId) {
		$this->db->where('TeamId', $teamId);
		if($this->db->update('mentor_team', array('TeamImage', $path))) {
			return true;
		} else {
			return false;
		}
	}

    function checkMentorExist($userId, $userName = '', $email = '', $checkUserId = true) {
        $userCdn = ($checkUserId) ? "m.MentorID != '$userId' AND" : "";
        if (!empty($userName)) {
            $UserExistCheck = $this->db->query("SELECT COUNT(m.MentorID) as UserCheck FROM mentor_mentor m WHERE $userCdn m.MentorName = '$userName'")->result();
        } else if (!empty($email)) {
            $UserExistCheck = $this->db->query("SELECT COUNT(m.MentorContactInfoID) as UserCheck FROM mentor_mentorcontactinfo m WHERE $userCdn m.MentorEmail = '$email'")->result();
        } else {
            $UserExistCheck = $this->db->query("SELECT COUNT(m.MentorContactInfoID) as UserCheck FROM mentor_mentorcontactinfo m WHERE m.MentorID = '$userId'")->result();
        }
        if ($UserExistCheck[0]->UserCheck > 0) {
            return true;
        } else {
            return false;
        }
    }

    function checkMenteeExist($userId, $userName = '', $email = '', $checkUserId = true) {
        $userCdn = ($checkUserId) ? "m.MenteeID != '$userId' AND" : "";
        if (!empty($userName)) {
            $UserExistCheck = $this->db->query("SELECT COUNT(m.MenteeID) as UserCheck FROM mentor_mentee m WHERE $userCdn m.MenteeName = '$userName'")->result();
        } else if (!empty($email)) {
            $UserExistCheck = $this->db->query("SELECT COUNT(m.MenteeContactID) as UserCheck FROM mentor_menteecontact m WHERE $userCdn m.MenteeEmail = '$email'")->result();
        } else {
            $UserExistCheck = $this->db->query("SELECT COUNT(m.MenteeContactID) as UserCheck FROM mentor_menteecontact m WHERE m.MenteeID = '$userId'")->result();
        }

        if ($UserExistCheck[0]->UserCheck > 0) {
            return true;
        } else {
            return false;
        }
    }

    function editProfileImage($UserId, $UserType, $Path) {
//        $sql = "call usp_EditProfile('" . $UserId . "','" . $UserType . "','" . $Path . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();
		$image_path = 'http://melstm.net/mentor/assets/admin/images/';
        if ($UserType == 0) { //For Mentor
            if ($this->checkMentorExist($UserId)) {
                $this->db->where('MentorID', $UserId);
                $this->db->update('mentor_mentorcontactinfo', array('MentorImage' => $Path));

//                $this->db->where('MentorID', $UserId);
//                $data = array(
//                    'MentorPhone' => $PhoneNumber,
//                    'Password' => $Pass,
//                    'MentorEmail' => $Email
//                );
//                $this->db->update('mentor_mentorcontactinfo', $data);

                $query = $this->db->query("SELECT m.MentorID AS Id, mci.IsSubadmin AS IsSubadmin, m.MentorName, mci.MentorPhone, mci.MentorEmail, mci.MentorStatus, 0 AS UserType,
                            IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_mentorskillset ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MentorID = '$UserId'), '') AS Skills,
                            mci.Password, CONCAT ( '{$image_path}admin/', mci.MentorImage ) AS MentorImage,mci.DisableSMS,msn.MentorSourceID,msn.MentorSourceName
                            FROM mentor_mentor m
                            LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorID = m.MentorID
							LEFT JOIN mentor_usertosource us ON us.MentorID = mci.MentorID
							LEFT JOIN mentor_mentorsourcenames msn ON us.MentorSourceID = msn.MentorSourceID
                            WHERE m.MentorID = '$UserId'");
                $response = $query->result();
            } else {
                $response = array((object) array(
                        'Message' => 'Profile do not match.'
                    )
                );
            }
        } else if ($UserType == 1) { //For Mentee
            if ($this->checkMenteeExist($UserId)) {
                $this->db->where('MenteeID', $UserId);
                $this->db->update('mentor_menteecontact', array('MenteeImage' => $Path));

//                $this->db->where('MenteeID', $UserId);
//                $data = array(
//                    'MenteePhone' => $PhoneNumber,
//                    'Password' => $Pass,
//                    'MenteeEmail' => $Email
//                );
//                $this->db->update('mentor_menteecontact', $data);

                $query = $this->db->query("SELECT m.MenteeID AS Id, m.MenteeName, mc.MenteePhone, mc.MenteeEmail, mc.MenteeStatus, 1 AS UserType,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_menteeskill ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MenteeID = '$UserId'), '') AS Skills,
                        mc.Password, CONCAT ( '{$image_path}admin/', mc.MenteeImage ) AS MenteeImage,mc.DisableSMS,msn.MenteeSourceID,msn.MenteeSourceName
                        FROM mentor_mentee m
                        LEFT JOIN mentor_menteecontact mc ON mc.MenteeId = m.MenteeID
						LEFT JOIN mentor_menteetosource us ON us.MenteeID = mc.MenteeID
						LEFT JOIN mentor_menteesourcenames msn ON us.MenteeSourceID = msn.MenteeSourceID
                        WHERE m.MenteeID = '$UserId'");
                $response = $query->result();
            } else {
                $response = array((object) array(
                        'Message' => 'Profile do not match.'
                    )
                );
            }
        }

        return $response;
    }

    function editProfileDetail($UserId, $UserType, $UserName, $PhoneNumber, $Pass, $Email, $DisableSMS,$sourceID) {
        if ($this->checkMentorExist($UserId, $UserName) || $this->checkMenteeExist($UserId, $UserName)) {
            $response = array((object) array(
                    'Message' => 'UserName already Exists'
                )
            );
        } else if ($this->checkMentorExist($UserId, '', $Email) || $this->checkMenteeExist($UserId, '', $Email)) {
            $response = array((object) array(
                    'Message' => 'Email already Exists'
                )
            );
        } else {
            $image_path = 'http://melstm.net/mentor/assets/admin/images/';
            if ($UserType == 0) { //For Mentor
                if ($this->checkMentorExist($UserId)) {
                    $this->db->where('MentorID', $UserId);
                    $this->db->update('mentor', array('MentorName' => $UserName));

                    $this->db->where('MentorID', $UserId);
					
                    $data = array(
                        'MentorPhone' => $PhoneNumber,
                        'MentorEmail' => $Email,
                        'DisableSMS' => $DisableSMS
                    );
					if($Pass!="")
						$data['Password'] = fnEncrypt($Pass, $this->config->item('mentorKey'));
					
                    $this->db->update('mentor_mentorcontactinfo', $data);
					
					//update Mentor source
					$this->db->where('MentorID',$UserId);
					$getsource = $this->db->get('mentor_usertosource');
					if($getsource->num_rows() > 0){
						$sourceData = array(
							'MentorID' => $UserId,
							'MentorSourceID' => $sourceID
						);
						$this->db->update('mentor_usertosource',$sourceData);
					} else {
						$sourceData = array(
							'MentorID' => $UserId,
							'MentorSourceID' => $sourceID
						);
						$this->db->insert('mentor_usertosource',$sourceData);
					}

                    $query = $this->db->query("SELECT m.MentorID AS Id, mci.IsSubadmin AS IsSubadmin, m.MentorName, mci.MentorPhone, mci.MentorEmail, mci.MentorStatus, 0 AS UserType,
                            IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_mentorskillset ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MentorID = '$UserId'), '') AS Skills,
							mci.Password, CONCAT ( '{$image_path}admin/', mci.MentorImage ) AS MentorImage, mci.DisableSMS,msn.MentorSourceID,msn.MentorSourceName
                            FROM mentor_mentor m
                            LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorID = m.MentorID
							LEFT JOIN mentor_usertosource us ON us.MentorID = mci.MentorID
							LEFT JOIN mentor_mentorsourcenames msn ON us.MentorSourceID = msn.MentorSourceID
                            WHERE m.MentorID = '$UserId'");
                    $response = $query->result();
                } else {
                    $response = array((object) array(
                            'Message' => 'Profile do not match. Please try again.'
                        )
                    );
                }
            } else if ($UserType == 1) { //For Mentee
                if ($this->checkMenteeExist($UserId)) {
                    $this->db->where('MenteeID', $UserId);
                    $this->db->update('mentee', array('MenteeName' => $UserName));

                    $this->db->where('MenteeID', $UserId);
                    $data = array(
                        'MenteePhone' => $PhoneNumber,
                        'MenteeEmail' => $Email,
                        'DisableSMS' => $DisableSMS
                    );
					
					if($Pass!="")
						$data['Password'] = fnEncrypt($Pass, $this->config->item('mentorKey'));
					
                    $this->db->update('mentor_menteecontact', $data);
					
					//update Mentee source
					$this->db->where('MenteeID',$UserId);
					$getsource = $this->db->get('mentor_menteetosource');
					if($getsource->num_rows() > 0){
						$sourceData = array(
							'MenteeID' => $UserId,
							'MenteeSourceID' => $sourceID
						);
						$this->db->update('mentor_menteetosource',$sourceData);
					} else {
						$sourceData = array(
							'MenteeID' => $UserId,
							'MenteeSourceID' => $sourceID
						);
						$this->db->insert('mentor_menteetosource',$sourceData);
					}

                    $query = $this->db->query("SELECT m.MenteeID AS Id, m.MenteeName, mc.MenteePhone, mc.MenteeEmail, mc.MenteeStatus, 1 AS UserType,
                        IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_menteeskill ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MenteeID = '$UserId'), '') AS Skills,
						mc.Password, CONCAT ( '{$image_path}admin/', mc.MenteeImage ) AS MenteeImage,mc.DisableSMS,msn.MenteeSourceID,msn.MenteeSourceName
                        FROM mentor_mentee m
                        LEFT JOIN mentor_menteecontact mc ON mc.MenteeId = m.MenteeID
						LEFT JOIN mentor_menteetosource us ON us.MenteeID = mc.MenteeID
						LEFT JOIN mentor_menteesourcenames msn ON us.MenteeSourceID = msn.MenteeSourceID
                        WHERE m.MenteeID = '$UserId'");
                    $response = $query->result();
                } else {
                    $response = array((object) array(
                            'Message' => 'Profile do not match. Please try again.'
                        )
                    );
                }
            }
        }
        return $response;
    }

    function getProfile($UserId, $UserType) {
//		$sql = "call usp_GetProfile('" . $UserId . "','" . $UserType . "')";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();


        $image_path = 'http://melstm.net/mentor/assets/admin/images/';
        if ($UserType == 0) { //For Mentor
            if ($this->checkMentorExist($UserId)) {
                $query = $this->db->query("SELECT m.MentorID AS Id, mci.IsSubadmin AS IsSubadmin, m.MentorName, mci.MentorPhone, mci.MentorEmail, mci.MentorStatus, 0 AS UserType,
                    IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_mentorskillset ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MentorID = '$UserId'), '') AS Skills,
                    mci.Password, CONCAT ( '{$image_path}admin/', mci.MentorImage ) AS MentorImage,mci.DisableSMS,msn.MentorSourceID,msn.MentorSourceName
                    FROM mentor_mentor m
                    LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorID = m.MentorID
					LEFT JOIN mentor_usertosource us ON us.MentorID = mci.MentorID
					LEFT JOIN mentor_mentorsourcenames msn ON us.MentorSourceID = msn.MentorSourceID
                    WHERE m.MentorID = '$UserId'");
                $response = $query->result();
            } else {
                $response = array((object) array(
                        'Message' => 'Profile do not match'
                    )
                );
            }
        } else if ($UserType == 1) { //For Mentee
            if ($this->checkMenteeExist($UserId)) {

                $query = $this->db->query("SELECT m.MenteeID AS Id, m.MenteeName, mc.MenteePhone, mc.MenteeEmail, mc.MenteeStatus, 1 AS UserType,
                    IFNULL((SELECT GROUP_CONCAT(CONCAT(ms.SkillID,'~',s.SkillName)) AS SkillDetails FROM mentor_menteeskill ms LEFT JOIN mentor_skill s ON s.SkillID = ms.SkillID WHERE ms.MenteeID = '$UserId'), '') AS Skills,
                    mc.Password, CONCAT ( '{$image_path}admin/', mc.MenteeImage ) AS MenteeImage,mc.DisableSMS,msn.MenteeSourceID,msn.MenteeSourceName
                    FROM mentor_mentee m
                    LEFT JOIN mentor_menteecontact mc ON mc.MenteeId = m.MenteeID
					LEFT JOIN mentor_menteetosource us ON us.MenteeID = mc.MenteeID
					LEFT JOIN mentor_menteesourcenames msn ON us.MenteeSourceID = msn.MenteeSourceID
					
                    WHERE m.MenteeID = '$UserId'");
                $response = $query->result();
            } else {
                $response = array((object) array(
                        'Message' => 'Profile do not match'
                    )
                );
            }
        }
        return $response;
    }

    function getProfileDetail($UserId, $UserType) {
        $arrReturn = array();
        if ($UserType == "0") {
            $this->db->select('m.*,me.*,ms.*');
            $this->db->from('mentor m');
            $this->db->join('mentorcontactinfo me', 'me.MentorID = m.MentorID', "left");
			$this->db->join('usertosource us', 'us.MentorID = me.MentorID','left');
			$this->db->join('mentorsourcenames ms', 'us.MentorSourceID = ms.MentorSourceID','left');
            $this->db->where('m.MentorID', $UserId);
            mysqli_next_result($this->db->conn_id);
            $userData = $this->db->get()->row_array();
            if (!empty($userData)) {
                $arrReturn['Name'] = $userData['MentorName'];
                $arrReturn['Email'] = $userData['MentorEmail'];
                $arrReturn['AccessToken'] = fnDecrypt($userData['Password'], $this->config->item('mentorKey'));
                $arrReturn['MentorImage'] = $userData['MentorImage'];
                $arrReturn['UserType'] = 'Mentor';
				$arrReturn['MentorSource'] = $userData['MentorSourceName'];
            }
            return $arrReturn;
        } else {
            $this->db->select('m.*,me.*,ms.*');
            $this->db->from('mentee m');
            $this->db->join('menteecontact me', 'me.MenteeId = m.MenteeId', "left");
			$this->db->join('menteetosource us', 'us.MenteeID = me.MenteeID','left');
			$this->db->join('menteesourcenames ms', 'us.MenteeSourceID = ms.MenteeSourceID','left');
            $this->db->where('m.MenteeId', $UserId);
            mysqli_next_result($this->db->conn_id);
            $userData = $this->db->get()->row_array();
            if (!empty($userData)) {
                $arrReturn['Name'] = $userData['MenteeName'];
                $arrReturn['Email'] = $userData['MenteeEmail'];
                $arrReturn['AccessToken'] = fnDecrypt($userData['Password'], $this->config->item('mentorKey'));
                $arrReturn['MentorImage'] = $userData['MenteeImage'];
                $arrReturn['UserType'] = 'Mentee';
				$arrReturn['MenteeSource'] = $userData['MenteeSourceName'];
            }
            return $arrReturn;
        }
    }

    function updateMenteeactionsItemsWithDoneDetail($MenteeID, $MeetingID, $ActionId) {
        //$sql = "call usp_UpdateMenteeactionsItemsWithDone('" . $MenteeID . "','" . $MeetingID . "','" . $ActionId . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $data = array(
            "MenteeID" => $MenteeID,
            "MeetingID" => $MeetingID,
            "MenteeActionID" => $ActionId
        );
        $insert_id = $this->db->insert('mentor_menteeactionstaken', $data);
        return $insert_id;
    }

    function updateMeetingMenteeactionsItemsWithDoneDetail($MeetingID, $MenteeActionItemDone) {
        //$sql = "call usp_UpdateMeetingMenteeactionsItemsWithDone('" . $MeetingID . "','" . $MenteeActionItemDone . "')";
        //$parameters = array();
        //$query = $this->db->query($sql, $parameters);
        //mysqli_next_result($this->db->conn_id);
        //return $query->result();

        $data = array(
            "MenteeActionItemDone" => $MenteeActionItemDone
        );
        $this->db->where('MeetingID', $MeetingID);
        if ($this->db->update('mentor_meetinginfo', $data)) {
            $result = array((object) array(
                    'Id' => 1
            ));
        } else {
            $result = array((object) array(
                    'Message' => 'Failed to update meeting'
            ));
        }
        return $result;
    }

    function addinviteList($MentorID, $MenteeID) {
        $sql = "call  usp_addinviteList('" . $MentorID . "','" . $MenteeID . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }

    function getConfig() {
//        $sql = "call  usp_getConfig()";
//        $parameters = array();
//        $query = $this->db->query($sql, $parameters);
//        mysqli_next_result($this->db->conn_id);
//        return $query->result();

        $query = $this->db->query("SELECT maxMember, noteTitleLength,settingID, mentorName, menteeName, headerColor, leftBarColor, icon, buttonColor, fontColor, backgroundColor, highlightColor, leftMenuFont, tableFont, tableBackground, Impromptu impromptu, teamManager, concat('http://melstm.net/mentor/assets/admin/images/',logo) AS logo,expiryTime, (SELECT Email FROM  mentor_subdomains LIMIT 1) as AdminEmail, (SELECT Password FROM  mentor_subdomains LIMIT 1) as AdminPassword FROM mentor_settings");
        return $query->result();
    }

    function setMentorIamge($MentorID, $MentorIamge) {
        $sql = "call usp_setMentorIamge('" . $MentorID . "','" . $MentorIamge . "')";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }
	
	function rescheduleMeeting($details){
		$query = $this->db->query("SELECT * FROM mentor_invitation i WHERE i.InvitationId = {$details->InvitationId}");
        if ($query->num_rows() > 0) {
			$invitation_data = $query->result();
            
			$data = array();
			
			if($details->UserType == 0){
				 $data = array(
                    'MeetingDateTime' => $details->MeetingDateTime,
                    'MeetingTypeID' => $details->MeetingTypeID,
					'MeetingPlaceID' => $details->MeetingPlaceID,
					'RescheduleComments' => $details->RescheduleComments,
					'rescheduledBy' => $details->UserType,
					'Status' => 'Rescheduled',
					'AcceptResponsesName' => '',
					'DeclineResponsesName' => ''
                );
			}else{
				 $data = array(
				//	'MeetingTypeID' => $details->MeetingTypeID,
				//	'MeetingPlaceID' => $details->MeetingPlaceID,
					'RescheduleComments' => $details->RescheduleComments,
					'rescheduledBy' => $details->UserType,
					'Status' => 'Rescheduled',
					'AcceptResponsesName' => '',
					'DeclineResponsesName' => ''
                );
			}


            $this->db->where('InvitationId', $details->InvitationId);	
			if($this->db->update('mentor_invitation', $data)){
				
				$invitation = $this->db->query("SELECT mi.MentorID,mi.MenteeID,mc.MenteeEmail,m.MenteeName,mm.MentorName,mt.TopicDescription,mci.MentorEmail,mci.MentorPhone,mc.MenteePhone,mmp.MeetingPlaceName,mmt.MeetingTypeName
				FROM mentor_invitation mi
				Left join mentor_menteecontact mc on mc.MenteeId=mi.MenteeID
				left join mentor_mentee m on m.MenteeID=mi.MenteeID
				left join mentor_mentor mm on mi.MentorID=mm.MentorID
				left join mentor_topic mt on mt.TopicID= mi.TopicID
				left join mentor_mentorcontactinfo mci on mci.MentorID=mi.MentorID
				left join mentor_meetingplace mmp on mmp.MeetingPlaceID=mi.MeetingPlaceID
				left join mentor_meetingtype mmt on mmt.MeetingTypeID=mi.MeetingTypeID
				WHERE mi.InvitationId = {$details->InvitationId}");
				
				$data	=	$invitation->result();
				$response = array((object) array(
                        'InsertedId' => '1',
						'MenteeEmail' => $data[0]->MenteeEmail,
						'MenteeName' => $data[0]->MenteeName,
						'MentorName' => $data[0]->MentorName,
						'TopicDescription' => $data[0]->TopicDescription,
						'oldDate'	=> $invitation_data[0]->MeetingDateTime,
						'MentorEmail' => $data[0]->MentorEmail,
						'MentorPhone' => $data[0]->MentorPhone,
						'MenteePhone' => $data[0]->MenteePhone,
						'MeetingPlaceName' => $data[0]->MeetingPlaceName,
						'MeetingTypeName' => $data[0]->MeetingTypeName						
                    )
                );
			}
        } else {
            $response = array((object) array(
                    'Message' => 'No invitation found.'
                )
            );
        }
        return $response;
	}
	
	function cancelMeeting($details){
		$query = $this->db->query("SELECT * FROM mentor_invitation i WHERE i.InvitationId = {$details->InvitationId}");
        if ($query->num_rows() > 0) {
			$invitation_data = $query->result();
            $data = array(
                'cancellationReason' => $details->cancellationReason,
				'Status' => 'Cancelled',
				'cancelledBy' => $details->UserType,
				'AcceptOrRejectTime' => date('Y-m-d H:i:s')
            );
            
            $this->db->where('InvitationId', $details->InvitationId);
            

            if ($this->db->update('invitation', $data)) {
                $invitation = $this->db->query("
				
					SELECT mi.MentorID, mi.MenteeID, mc.MenteeEmail, m.MenteeName, mm.MentorName, mci.MentorEmail, mt.TopicDescription, mi.MeetingDateTime,mci.MentorPhone,mc.MenteePhone
					FROM mentor_invitation mi
					LEFT JOIN mentor_mentee m ON mi.MenteeID=m.MenteeID
					LEFT JOIN mentor_mentor mm ON mm.MentorID=mm.MentorID
					LEFT JOIN mentor_menteecontact mc ON mc.MenteeID=mi.MenteeID
					LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorID=mi.MentorID
					LEFT JOIN mentor_topic mt ON mt.TopicID=mi.TopicID
					WHERE mi.InvitationId ={$details->InvitationId} AND mc.MenteeId = mi.MenteeID AND mc.MenteeId = m.MenteeID AND mi.MentorID = mm.MentorID AND mt.TopicID = mi.TopicID");
					
					
					/*SELECT mi.MentorID,mi.MenteeID,mc.MenteeEmail,m.MenteeName,mm.MentorName,mci.MentorEmail,mt.TopicDescription,mi.MeetingDateTime
					FROM mentor_invitation mi,mentor_menteecontact mc, mentor_mentee m,mentor_mentor mm,mentor_mentorcontactinfo mci,mentor_topic mt 
					WHERE mi.InvitationId = {$details->InvitationId} and mc.MenteeId=mi.MenteeID and mc.MenteeId=m.MenteeID and mi.MentorID=mm.MentorID and mt.TopicID= mi.TopicID*/
				
				$data	=	$invitation->result();
				$response = array((object) array(
                        'InsertedId' => '1',
						'MenteeEmail' => $data[0]->MenteeEmail,
						'MentorEmail' => $data[0]->MentorEmail,
						'MenteeName' => $data[0]->MenteeName,
						'MentorName' => $data[0]->MentorName,
						'TopicDescription' => $data[0]->TopicDescription,
						'oldDate'	=> $data[0]->MeetingDateTime,
						'Status'	=> $invitation_data[0]->Status,
						'MentorPhone' => $data[0]->MentorPhone,
						'MenteePhone' => $data[0]->MenteePhone,
                    )
                );
            } else {
                $response = array((object) array(
                        'InsertedId' => '0'
                    )
                );
            }
        } else {
            $response = array((object) array(
                    'Message' => 'No invitation found.'
                )
            );
        }
        return $response;
	}

	function getHelpText($reqData){
		$config = $this->getConfig();
		//print_r($config);
		//print_r(array($config[0]->menteeName, $config[0]->mentorName, $config[0]->teamManager, $data->CurrentUserName));
		$this->db->select("
			h.ScreenName, h.HelpDescription, h.HeaderId, h.FooterId, 
			(SELECT HelpDescription FROM c10melstm_net_template.mentor_help WHERE ScreenName = 'header_mentor') AS Mentor_header, 
			(SELECT HelpDescription FROM c10melstm_net_template.mentor_help WHERE ScreenName = 'header_mentee') AS Mentee_header, 
			(SELECT HelpDescription FROM c10melstm_net_template.mentor_help WHERE ScreenName = 'footer_mentor') AS Mentor_footer, 
			(SELECT HelpDescription FROM c10melstm_net_template.mentor_help WHERE ScreenName = 'footer_mentee') AS Mentee_footer, 
			(SELECT HelpDescription FROM c10melstm_net_template.mentor_help WHERE ScreenName = 'header_common') AS Common_header, 
			(SELECT HelpDescription FROM c10melstm_net_template.mentor_help WHERE ScreenName = 'footer_common') AS Common_footer
		");
        $this->db->from('c10melstm_net_template.mentor_help h');
		$this->db->where('h.UserType', 2);
		if($reqData->UserType == 1 || $reqData->UserType == 0){
			$this->db->or_where('h.UserType', $reqData->UserType);
		}
        mysqli_next_result($this->db->conn_id);
        $mentorData = $this->db->get();

        $data	=	$mentorData->result();
		$tempArr	=	array();
		foreach($data as $key=>$val) {
			$HelpDescription = '';
			//$HelpDescription = $val->HeaderText . " " . $val->HelpDescription . " " . $val->FooterText;
			$HelpDescription = $val->HelpDescription;
			
			$searchHeaderFooter = array('##MENTOR_HEADER##', '##MENTEE_HEADER##', '##COMMON_HEADER##', '##MENTOR_FOOTER##', '##MENTEE_FOOTER##', '##COMMON_FOOTER##');
			$replaceHeaderFooter = array($val->Mentor_header, $val->Mentee_header, $val->Common_header, $val->Mentor_footer, $val->Mentee_footer, $val->Common_footer);
			$HelpDescription = str_replace($searchHeaderFooter, $replaceHeaderFooter, $HelpDescription);
			
			
			$search_array = array('##CONFIGURED_MENTEE_NAME##', '##CONFIGURED_MENTOR_NAME##', '##CONFIGURED_TEAM_NAME##', '##USERNAME##', '##IMPROMPTU_MEETING##');
			$replace_array = array($config[0]->menteeName, $config[0]->mentorName, $config[0]->teamManager, $reqData->CurrentUserName, $config[0]->impromptu);
			$HelpDescription = str_replace($search_array, $replace_array, $HelpDescription);
			$tempArr[$val->ScreenName]	=	$HelpDescription;//$val->HelpDescription;
		}
		return $tempArr;
	}
	
	 function getMentorWaitScreenStatus($MenteeID,$where) {
        $this->db->select('*');
        $this->db->from('mentor_invitation');
		
		if($where == 'Rejected'){
			
			$whereCdn = "MenteeID = '$MenteeID' AND (Status = '$where' OR Status = 'Cancelled') AND isRemovedByMentee=0";
			$this->db->order_by("inviteTime", "desc");
			
		} else if($where == 'Accepted'){
			
			$whereCdn = "MenteeID = '$MenteeID' AND (Status = '$where' OR (Status = 'Rescheduled' AND rescheduledby=0) OR (Status='Pending' AND InitiatedBy = 0))";
			$this->db->order_by("MeetingDateTime", "desc");
			
		} else if($where == 'Pending'){
			
			$whereCdn = "MenteeID = '$MenteeID' AND (Status = '$where' AND InitiatedBy != 0 OR (Status = 'Rescheduled' AND rescheduledby=1) OR (Status='Pending' AND InitiatedBy = 1))";
			$this->db->order_by("inviteTime", "asc");
			
		} else {
			
			$whereCdn = "MenteeID = '$MenteeID' AND (Status = '$where')";
			$this->db->order_by("inviteTime", "desc");
			
		}
		$this->db->where($whereCdn);
		
        mysqli_next_result($this->db->conn_id);
        $mentorData = $this->db->get();


        //echo $this->db->last_query();exit;    
        return $mentorData->result();
    }
	
	function getMentorId($usertype,$Email){
		if($usertype == '0') {
			$query = $this->db->query("SELECT * FROM (mentor_mentorcontactinfo) where MentorEmail='$Email'");
		} else if($usertype == '1'){
			$query = $this->db->query("SELECT * FROM (mentor_menteecontact) where MenteeEmail='$Email'");
		}
       // echo $this->db->last_query();
        return $query->result();
	}
	
	function getAcceptResponses(){
		$this->db->select('ResponseID,ResponseName');
		$query = $this->db->get_where("mentor_accept_responses",array('Status !=' => 0));
        return $query->result();
	}
	
	function getDeclineResponses(){
		$this->db->select('ResponseID,ResponseName');
		$query = $this->db->get_where("mentor_decline_responses",array('Status !=' => 0));
        return $query->result();
	}
	
	function resetpassword($id,$pass,$UserType){
		$data = array(
			'Password' => $pass
        );
		if($UserType == 0){
			$this->db->where("MentorID",$id);
			$result = $this->db->update('mentor_mentorcontactinfo', $data);
		}else{
			$this->db->where("MenteeId",$id);
			$result = $this->db->update("mentor_menteecontact", $data);
		}
		//echo $this->db->last_query();
		if($result){
			return true;
		}else{
			return false;
		}
        
	}
	
	
	function mentorResponseEmail($details){
		$query = $this->db->query("SELECT * FROM mentor_invitation i WHERE i.InvitationId = {$details->InvitationId}");
        if ($query->num_rows() > 0) {
				$invitation = $this->db->query("SELECT mi.MentorID,mi.MenteeID, mi.MeetingDateTime,mc.MenteeEmail,m.MenteeName,mm.MentorName,mt.TopicDescription,mci.MentorEmail,mci.MentorPhone,mc.MenteePhone
				FROM mentor_invitation mi
				Left join mentor_menteecontact mc on mc.MenteeId=mi.MenteeID
				left join mentor_mentee m on m.MenteeID=mi.MenteeID
				left join mentor_mentor mm on mi.MentorID=mm.MentorID
				left join mentor_topic mt on mt.TopicID= mi.TopicID
				left join mentor_mentorcontactinfo mci on mci.MentorID=mi.MentorID
				WHERE mi.InvitationId = {$details->InvitationId}");
				
				$data	=	$invitation->result();
				$response = array((object) array(
                        'InsertedId' => '1',
						'MenteeEmail' => $data[0]->MenteeEmail,
						'MentorEmail' => $data[0]->MentorEmail,
						'MenteeName' => $data[0]->MenteeName,
						'MentorName' => $data[0]->MentorName,
						'TopicDescription' => $data[0]->TopicDescription,			
						'MentorPhone' => $data[0]->MentorPhone,
						'MenteePhone' => $data[0]->MenteePhone,
						'MeetingDateTime' => $data[0]->MeetingDateTime
                    )
                );
        } else {
            $response = array((object) array(
                    'Message' => 'No invitation found.'
                )
            );
        }
        return $response;
	}
}