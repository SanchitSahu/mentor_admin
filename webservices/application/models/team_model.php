<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Team_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function saveInstantMeeting_model($data) {
        $insertData = array(
            'MentorID' => $data->MentorID,
            'MeetingStartDatetime' => $data->MeetingStartTime,
            'MeetingEndDatetime' => $data->MeetingEndTime,
            'RawStartDatetime' => $data->MeetingStartTime,
            'RawEndDatetime' => $data->MeetingEndTime,
            'MeetingTypeID' => $data->MeetingType,
            'MeetingTopicID' => $data->MainTopic,
            'MeetingSubTopicID' => '',
            'MeetingPlaceID' => $data->MeetingPlace,
            'MeetingElapsedTime' => $data->MeetingElapsedTime,
            'MeetingFeedback' => '',
            'MentorActionItemDone' => $data->MentorTeamActionItemDoneID,
            'MenteeActionItemDone' => $data->MenteeTeamActionItemDoneID,
            'MenteeInvitationComment' => '',
            'Meeting_status' => 3
        );
        
        if($data->MeetingWith == 'mentee') {
            $insertData['MenteeID'] = $data->MeetingWithID;
            $insertData['TeamID'] = 0;
        } else if($data->MeetingWith == 'team') {
            $insertData['MenteeID'] = 0;
            $insertData['TeamID'] = $data->MeetingWithID;
        }
        
        if($this->db->insert('mentor_meetinginfo', $insertData)) {
            $MeetingID =  $this->db->insert_id();
            
            $MentorTeamActionItemID = explode(',', $data->MentorTeamActionItemID);
            foreach($MentorTeamActionItemID as $values) {
                $insertMentorActionItemArr = array(
                    'MentorID' => $data->MentorID,
                    'MeetingID' => $MeetingID,
                    'MentorActionID' => $values
                    
                );
                $this->db->insert('mentor_mentoractionstaken', $insertMentorActionItemArr);
            }
            
            $MenteeTeamActionItemID = explode(',', $data->MenteeTeamActionItemID);
            foreach($MenteeTeamActionItemID as $values) {
                $insertMenteeTeamActionItemArr = array(
                    'MeetingID' => $MeetingID
                );
                if($data->MeetingWith == 'mentee') {
                    $insertMenteeTeamActionItemArr['MenteeID'] = $data->MeetingWithID;
                    $insertMenteeTeamActionItemArr['MenteeActionID'] = $values;
                    $this->db->insert('mentor_menteeactionstaken', $insertMenteeTeamActionItemArr);
                } else if($data->MeetingWith == 'team') {
                    $insertMenteeTeamActionItemArr['TeamID'] = $data->MeetingWithID;
                    $insertMenteeTeamActionItemArr['TeamActionID'] = $values;
                    $this->db->insert('mentor_teamactionstaken', $insertMenteeTeamActionItemArr);
                }
            }
            return $MeetingID;
        } else {
            return false;
        }
    }
    
    function getAllMentorsForTeam_model($data) {
        $finalMentorData = array();
        
        $result = $this->db->query("
            SELECT m.MentorID, m.MentorName
            FROM mentor_mentor m
            LEFT JOIN mentor_mentorcontactinfo mci ON m.MentorID=mci.MentorID
            WHERE m.Status != 0 AND mci.Status!=0  
        ");
        
        if($result->num_rows() > 0) {
            $mentorData = $result->result();
            foreach($mentorData as $data) {
                $sub1 = $this->db->query("SELECT COUNT(*) as sub1  FROM mentor_team WHERE TeamOwnerUserId={$data->MentorID} AND TeamOwnerUserType=0")->result();
                $sub2 = $this->db->query("SELECT COUNT(*) as sub2 FROM mentor_teammember WHERE UserID = {$data->MentorID} AND UserType=0 AND ApprovalStatus='joined'")->result();
                if($sub1[0]->sub1 == 0 && $sub2[0]->sub2 == 0) {
                    $finalMentorData[] = $data;
                }
            }
        }
        if(count($finalMentorData) > 0){
            return $finalMentorData;
        } else {
            return array();
        }
    }
    
    function getAllMenteesForTeam_model($data) {
        $finalMenteeData = array();
        
        $result = $this->db->query("
            SELECT m.MenteeID, m.MenteeName
            FROM mentor_mentee m
            LEFT JOIN mentor_menteecontact mci ON m.MenteeID=mci.MenteeID
            WHERE m.Status != 0 AND mci.Status!=0  
        ");
        
        if($result->num_rows() > 0) {
            $mentorData = $result->result();
            foreach($mentorData as $data) {
                $sub1 = $this->db->query("SELECT COUNT(*) as sub1  FROM mentor_team WHERE TeamOwnerUserId={$data->MenteeID} AND TeamOwnerUserType=1 and TeamStatus = 'active'")->result();
                $sub2 = $this->db->query("SELECT COUNT(*) as sub2 FROM mentor_teammember WHERE UserID = {$data->MenteeID} AND UserType=1 AND ApprovalStatus='joined'")->result();
                if($sub1[0]->sub1 == 0 && $sub2[0]->sub2 == 0) {
                    $finalMenteeData[] = $data;
                }
            }
        }
        if(count($finalMenteeData) > 0){
            return $finalMenteeData;
        } else {
            return array();
        }
    }
    
    function addTeam_model($data){
        $insertData = array(
            'TeamName' => $data->TeamName,
            'TeamDescription' => $data->TeamDescription,
            'TeamOwnerUserID' => $data->UserId,
            'TeamOwnerUserType' => $data->UserType,
            'MaxMembers' => $data->MaxMemberCount,
            'TeamSkills' => $data->SkillID,
            'TeamImage' => $data->TeamImage,
            'UpdatedDate' => date('Y-m-d H:i:s')
        );
        if($data->TeamId == '') {
            $insertData['CreatedDate'] = date('Y-m-d H:i:s');
            if($this->db->insert('team', $insertData)) {
                return $this->db->insert_id();
            } else {
                return false;
            }
        } else {
            $this->db->where('TeamId', $data->TeamId);
            if($this->db->update('team', $insertData)) {
                return $data->TeamId;
            } else {
                return false;
            }
        }
    }
    
    function deleteTeam_model($data) {
        $this->db->where(array(
            'TeamOwnerUserID' => $data->UserId,
            'TeamOwnerUserType' => $data->UserType,
            'TeamId' => $data->TeamId
        ));
        $update_arr = array(
            'TeamStatus' => 'deleted'
        );
        if($this->db->update('mentor_team', $update_arr)) {
            $this->db->where(array(
                'TeamID' => $data->TeamId,
                'ApprovalStatus' => 'joined'
            ));
            $update_arr = array(
                'ApprovalStatus' => 'team_deleted'
            );
            $this->db->update('mentor_teammember', $update_arr);
            return true;
        } else {
            return false;
        }
    }
    
    function joinTeam_model($data) {
        $insertArray = array(
            'UserID' => $data->UserID,
            'UserType'=> $data->UserType,
            'TeamID' => $data->TeamId,
            'ApprovalStatus' => 'joined',
            'CreatedDate' => date('Y-m-d H:i:s'),
            'UpdatedDate' => date('Y-m-d H:i:s')
        );
        if($this->db->insert('mentor_teammember', $insertArray)) {
            $team_data = $this->getTeamName_model($data->TeamId);
            return $team_data[0]->TeamName;
        } else {
            return false;
        }
    }
    
    function leaveTeam_model($data) {
        $this->db->where(array(
            'UserID' => $data->UserID,
            'UserType'=> $data->UserType,
            'TeamID' => $data->TeamId
        ));
        $updateArray = array('ApprovalStatus' => 'leaved');
        if($this->db->update('mentor_teammember', $updateArray)) {
            return true;
        } else {
            return false;
        }
    }
    
    function getOwnerTeamDetail_model($data) {
        $image_path = 'http://melstm.net/mentor/assets/admin/images/';
        $result = $this->db->query("
            SELECT mt.*,
            CONCAT ( '{$image_path}admin/', mt.TeamImage ) AS TeamFullImage,
            IF(mt.TeamOwnerUserType=0,mm.MentorName, mme.MenteeName) AS UserName,
            IFNULL((SELECT GROUP_CONCAT(CONCAT(s.SkillID,'~',s.SkillName)) FROM mentor_skill s WHERE FIND_IN_SET(s.SkillID,mt.TeamSkills)), '') AS Skills,
            1 as isOwner
            FROM mentor_team mt
            LEFT JOIN mentor_mentor mm ON mt.TeamOwnerUserId = mm.MentorID
            LEFT JOIN mentor_mentorcontactinfo mmi ON mt.TeamOwnerUserId = mmi.MentorID
            LEFT JOIN mentor_mentee mme ON mt.TeamOwnerUserId = mme.MenteeID
            LEFT JOIN mentor_menteecontact mmei ON mt.TeamOwnerUserId = mmei.MenteeID
            WHERE mt.TeamStatus = 'active'
            AND mt.TeamOwnerUserID = {$data->UserId}
            AND mt.TeamOwnerUserType = {$data->UserType}
        ");
        
        if($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;
        }
        
    }
    
    function getAllTeamMembers_model($data) {
        $image_path = 'http://melstm.net/mentor/assets/admin/images/';
        $query = $this->db->query("
            (
                SELECT 
                IF(mtm.TeamOwnerUserType =0, mm.MentorName, mme.MenteeName) UserName,
                mtm.Teamid TeamID,
                CONCAT ( '{$image_path}admin/', IF(mtm.TeamOwnerUserType =0, mmi.MentorImage, mmei.MenteeImage) ) UserImage,mtm.TeamOwnerUserType UserType, mtm.TeamOwnerUserID UserID, 1 as isOwner
                
                FROM mentor_team mtm
                
                LEFT JOIN mentor_mentor mm ON mtm.TeamOwnerUserID = mm.MentorID
                LEFT JOIN mentor_mentorcontactinfo mmi ON mtm.TeamOwnerUserID = mmi.MentorID
                LEFT JOIN mentor_mentee mme ON mtm.TeamOwnerUserID = mme.MenteeID
                LEFT JOIN mentor_menteecontact mmei ON mtm.TeamOwnerUserID= mmei.MenteeID
                
                WHERE mtm.TeamId = {$data->TeamId} AND 
                mtm.TeamStatus = 'active'  
            )
            UNION
            (
                SELECT 
                IF(mtm.UserType =0, mm.MentorName, mme.MenteeName) UserName, mt.TeamId TeamID,
                CONCAT ( '{$image_path}admin/', IF(mtm.UserType =0, mmi.MentorImage, mmei.MenteeImage) ) UserImage,mtm.UserType UserType, mtm.UserID UserID, 0 as isOwner
                FROM mentor_teammember mtm
                LEFT JOIN mentor_team mt ON mt.TeamId = mtm.TeamID
                LEFT JOIN mentor_mentor mm ON mtm.UserID = mm.MentorID
                LEFT JOIN mentor_mentorcontactinfo mmi ON mtm.UserID = mmi.MentorID
                LEFT JOIN mentor_mentee mme ON mtm.UserID = mme.MenteeID
                LEFT JOIN mentor_menteecontact mmei ON mtm.UserID= mmei.MenteeID
                WHERE mt.TeamStatus = 'active'
                AND mtm.TeamID={$data->TeamId} AND mtm.ApprovalStatus = 'joined'
                AND ( mt.TeamOwnerUserID != mtm.UserID OR (mt.TeamOwnerUserID = mtm.UserID AND mt.TeamOwnerUserType != mtm.UserType))
            )
        ");
        return $query->result();
    }
    
    function getMemberTeamDetail_model($data, $status) {
        $image_path = 'http://melstm.net/mentor/assets/admin/images/';
        $result = $this->db->query("
            SELECT IF(mt.TeamOwnerUserType=0,mm.MentorName, mme.MenteeName) AS TeamOwnerName, mt.*,
            CONCAT ( '{$image_path}admin/', mt.TeamImage ) AS TeamFullImage,
            IFNULL((SELECT GROUP_CONCAT(CONCAT(s.SkillID,'~',s.SkillName)) FROM mentor_skill s WHERE FIND_IN_SET(s.SkillID,mt.TeamSkills)), '') AS Skills,
            0 as isOwner
            
            FROM mentor_teammember mtm
            LEFT JOIN mentor_team mt ON mt.TeamID = mtm.TeamID
            LEFT JOIN mentor_mentor mm ON mt.TeamOwnerUserID = mm.MentorID
            LEFT JOIN mentor_mentorcontactinfo mmi ON mt.TeamOwnerUserID = mmi.MentorID
            LEFT JOIN mentor_mentee mme ON mt.TeamOwnerUserID = mme.MenteeID
            LEFT JOIN mentor_menteecontact mmei ON mt.TeamOwnerUserID = mmei.MenteeID
            
            WHERE mtm.ApprovalStatus = '$status'
            AND mtm.UserID = {$data->UserId}
            AND mtm.UserType = {$data->UserType}
        ");
        
        if($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;
        }
        
    }
    
    function getAllTeamDetail_model($data) {
        $image_path = 'http://melstm.net/mentor/assets/admin/images/';
        $result = $this->db->query("
            SELECT mt.*, IF(mt.TeamOwnerUserType=0, mm.MentorName, mme.MenteeName) AS OwnerName,
            IF(mt.TeamOwnerUserType=0, CONCAT ( '{$image_path}admin/', mmi.MentorImage ), CONCAT ( '{$image_path}admin/', mmei.MenteeImage )) AS OwnerFullImage,
            CONCAT ( '{$image_path}admin/', mt.TeamImage ) AS TeamFullImage,
            IFNULL((SELECT GROUP_CONCAT(CONCAT(s.SkillID,'~',s.SkillName)) FROM mentor_skill s WHERE FIND_IN_SET(s.SkillID,mt.TeamSkills)), '') AS Skills
            
            FROM mentor_team mt
            
            LEFT JOIN mentor_mentor mm ON mt.TeamOwnerUserId = mm.MentorID
            LEFT JOIN mentor_mentorcontactinfo mmi ON mt.TeamOwnerUserId = mmi.MentorID
            LEFT JOIN mentor_mentee mme ON mt.TeamOwnerUserId = mme.MenteeID
            LEFT JOIN mentor_menteecontact mmei ON mt.TeamOwnerUserId = mmei.MenteeID
            WHERE mt.TeamStatus='active'
            AND ( mt.TeamOwnerUserID != {$data->UserId} OR (mt.TeamOwnerUserID = {$data->UserId} AND mt.TeamOwnerUserType != {$data->UserType}))
        ");
        
        if($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;
        }
    }

    function removeTeamMember_model($data) {
        $whereCdn = array(
            'TeamID' => $data->TeamID,
            'UserID' => $data->UserID,
            'UserType' => $data->UserType,
            'ApprovalStatus' => 'joined',
        );
        $updateData = array(
            'ApprovalStatus' => 'removed',
            'Comment' => $data->Comment
        );
        $this->db->where($whereCdn);
        if($this->db->update('mentor_teammember', $updateData)) {
            return true;
        } else {
            return false;
        }        
    }
    
    function addTeamMember_model($data) {
        $mentorId = explode(',', $data->MentorID);
        $menteeId = explode(',', $data->MenteeID);
        
        $insertData = array();
        
        if($data->MentorID != '') {
            foreach($mentorId as $id) {
                $arr = array(
                    'ApprovalStatus' => 'joined',
                    'TeamID' => $data->TeamID,
                    'UserID' => $id,
                    'UserType' => 0
                );
                $insertData[] = $arr;
            }
        }
        
        if($data->MenteeID != '') {
            foreach($menteeId as $id) {
                $arr = array(
                    'ApprovalStatus' => 'joined',
                    'TeamID' => $data->TeamID,
                    'UserID' => $id,
                    'UserType' => 1
                );
                $insertData[] = $arr;
            }
        }
        
        if($this->db->insert_batch('mentor_teammember', $insertData)) {
            return true;
        } else {
            return false;
        }
    }
    
    function blockTeamMember_model($data) {
        $whereCdn = array(
            'TeamID' => $data->TeamID,
            'UserID' => $data->UserID,
            'UserType' => $data->UserType,
            'ApprovalStatus' => 'joined'
        );
        $updateData = array(
            'ApprovalStatus' => 'blocked',
            'Comment' => $data->Comment
        );
        $this->db->where($whereCdn);
        if($this->db->update('mentor_teammember', $updateData)) {
            return true;
        } else {
            return false;
        }
    }

	function getUserInfo_model($ID,$UserType){
		if($UserType == 0){
			$result = $this->db->query("
		        SELECT m.*,mci.*,m.MentorName as UserName,mci.MentorPhone as UserPhone,mci.MentorEmail as UserEmail
		        FROM mentor_mentor m
		        LEFT JOIN mentor_mentorcontactinfo mci ON m.MentorID=mci.MentorID
		        WHERE m.Status != 0 AND mci.Status!=0 and m.MentorID = $ID
		    ");
		}else{
			$result = $this->db->query("
		        SELECT m.*,mci.*,m.MenteeName as UserName,mci.MenteePhone as UserPhone,mci.MenteeEmail as UserEmail
		        FROM mentor_mentee m
		        LEFT JOIN mentor_menteecontact mci ON m.MenteeID=mci.MenteeID
		        WHERE m.Status != 0 AND mci.Status!=0  and m.MenteeID = $ID
		    ");
		}
		if($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;
        } 
	}

	function getTeamName_model($TeamId){
		$sql = $this->db->select('*')
	    	->from('team')
			->where('TeamId',$TeamId)
		//	->where('TeamStatus', 'active')
	    	->get();
        $result = $sql->result();

		//echo $this->db->last_query();exit;
		if($sql->num_rows() > 0) {
            return $sql->result();
        } else {
            return "";
        } 
	}

	function getTeamOwner_model($TeamId,$UserType){
		//echo $UserType;
		$result = "";
		if($UserType ==0){
			$result = $this->db->query("
		        SELECT m.MentorName as OwnerName
		        FROM mentor_mentor m
		        LEFT JOIN mentor_mentorcontactinfo mci ON m.MentorID=mci.MentorID
				LEFT JOIN mentor_team mt ON mt.TeamOwnerUserID=mci.MentorID
		        WHERE m.Status != 0 AND mci.Status!=0 and mt.TeamId = $TeamId
		    ");
		}else{
			$result = $this->db->query("
		        SELECT m.MenteeName as OwnerName
		        FROM mentor_mentee m
		        LEFT JOIN mentor_menteecontact mci ON m.MenteeID=mci.MenteeID
				LEFT JOIN mentor_team mt ON mt.TeamOwnerUserID=mci.MenteeID
		        WHERE m.Status != 0 AND mci.Status!=0  and mt.TeamId = $TeamId
		    ");
		}
		//echo $this->db->last_query();exit;
		if($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;
        } 
	}

	function getExistingMembers_model($TeamId,$UserID,$UserType){
		$sql = $this->db->query("
		        SELECT t.*,tm.*
		        FROM mentor_team t
		        LEFT JOIN mentor_teammember tm ON tm.TeamID = t.TeamId
		        WHERE t.TeamId = $TeamId AND t.TeamStatus = 'active' and tm.ApprovalStatus='joined' and (tm.UserID !=$UserID or (tm.UserID = $UserID AND tm.UserType != $UserType) )
		    ");


		//echo $this->db->last_query();exit;
        $result = $sql->result();
		if($sql->num_rows() > 0) {
            return $sql->result();
        } else {
            return "";
        } 
	}

	function getTeamOwnerData_model($TeamId,$UserID,$UserType){
		$sql = $this->db->query("
		        SELECT t.* 
            FROM mentor_team t
            WHERE t.TeamStatus='active'
            AND ( t.TeamOwnerUserID != $UserID OR (t.TeamOwnerUserID = $UserID AND t.TeamOwnerUserType != $UserType))
            AND t.TeamId = $TeamId
		    ");


		//echo $this->db->last_query();exit;
        $result = $sql->result();
		if($sql->num_rows() > 0) {
            return $sql->result();
        } else {
            return "";
        } 
	}
}
