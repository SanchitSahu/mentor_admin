<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Front_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->library('uri');

    }

    public static function pr($data, $flag = false) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        ($flag) ? exit : null;
    }

}

class Admin_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('text');
        $this->load->library('uri');
        $this->load->library('upload');
        
    }

    public static function pr($data, $flag = false) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        ($flag) ? exit : null;
    }
    
}