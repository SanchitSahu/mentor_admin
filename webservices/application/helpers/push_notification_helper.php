<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('get_user_device_id')) {
	function get_user_device_id($customer_id = '',$where = array()) {
		$ci =& get_instance();
		$ci->db->select('di.iDeviceId, di.iCustomerId, di.vDeviceTokenId, di.DeviceType');                
		$ci->db->from(DEVICEINFO.' as di');
        $ci->db->where('di.eDeviceStatus', 1);
		
		if($customer_id){
			$ci->db->where('di.iCustomerId', $customer_id);
		}
		if($where){
			$ci->db->where($where);
		}
		
		$query = $ci->db->get();
		
		if($query->num_rows() > 0) {
			return $query->result_array();
		}
		
		return false;
	}
}

if (!function_exists('ios_push_notification')) {
	function ios_push_notification($deviceId,$NotificationType,$msg,$customeParam = array()) {
		// Put your private key's passphrase here:
		$passphrase = 'skinsecret';
		
		// Put your device token here (without spaces):
		$deviceToken = $deviceId;
		
		// Put your alert message here:
		$message = $msg;
		
		if(IOS_PEM_STAGE == 'dev'){
			$pemfile = IOS_PEM_FILE_DEV;
			$apns_url = IOS_DEV_APNS_URL;
		}else if(IOS_PEM_STAGE == 'prod'){
			$pemfile = IOS_PEM_FILE_PROD;
			$apns_url = IOS_PROD_APNS_URL;
		}else{
			$pemfile = '';
			$apns_url = '';
		}
		
		
		////////////////////////////////////////////////////////////////////////////////
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'libraries/'.$pemfile);
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		//stream_context_set_option($ctx, 'ssl', 'cafile', $path.'entrust_2048_ca.cer');
		
		
		// Open a connection to the APNS server
		$fp = stream_socket_client(
				$apns_url, $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		
		if (!$fp){
			exit("Failed to connect: $err $errstr" . PHP_EOL);
			return false;
		}else{
			//echo 'Connected to APNS' . PHP_EOL;				
			// Create the payload body
			$body['aps'] = array(
					'alert' => $message,
					'sound' => 'default'
			);
			$body['case'] = $NotificationType;
			if(isset($customeParam) && !empty($customeParam)){
				foreach ($customeParam as $key => $vall){
					$body[$key] = $vall;
				}
			}
				
			// Encode the payload as JSON
			$payload = json_encode($body);
			
			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
				
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
				
			if (!$result){
				echo 'iOS:===>Message not delivered' . PHP_EOL."<br/>";
				return false;
			}else{
				echo 'iOS:===>Message successfully delivered' . PHP_EOL."<br/>";
				return true;
			}
			// Close the connection to the server
			fclose($fp);
		}
		return false;		
	}
}


if (!function_exists('android_push_notification')) {
	function android_push_notification($deviceId,$NotificationType,$msg,$customeParam = array()) {
	    
		// Set POST variables
		$url = 'https://android.googleapis.com/gcm/send';
		
		$message = array
		(
				'message' 	=> $msg,
				'title'		=> 'Skin Secret',
				'msgtype'	=> $NotificationType				
		);
		
		if(isset($customeParam) && !empty($customeParam)){
			foreach ($customeParam as $key => $val){
				$message[$key] = $val;
			}			
		}
		
		$fields = array(
				'registration_ids' => array($deviceId),
				'data' => $message,
		);
		
		$headers = array(
				'Authorization: key='.GOOGLE_API_KEY, //GOOGLE_API_KEY
				'Content-Type: application/json'
		);
		// Open connection
		$ch = curl_init();
		//echo "<pre>";print_r($headers);die;
		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		// Disabling SSL Certificate support temporarly
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		
		// Execute post
		$result = curl_exec($ch);
		if ($result === FALSE) {
			//die('Curl failed: ' . curl_error($ch));
			echo 'Android:===>Curl failed: ' . curl_error($ch)."<br/>";
			return false;
		}
		
		// Close connection
		curl_close($ch);
		
		if($result){
			$resData = (array) json_decode($result);
			
			if(is_array($resData)){
				if(isset($resData['success']) && !empty($resData['success'])){
					if($resData['success'] == 1){
						echo 'Android:===>Message successfully delivered';
						return true;
					}
				}
			}
			echo "Android:===>Error: ".$result."<br/>";			
		}
            
            return false;
	}
}

if (!function_exists('send_push_notification')) {
	function send_push_notification($NotificationType, $msg,$DeviceType, $userId = '',$customeParam = array()) {
		$where = array();
		if($DeviceType == 'ios'){
			$where = array('di.eDeviceType' => 'ios');
		}
		if($DeviceType == 'android'){
			$where = array('di.eDeviceType' => 'android');
		}
		
		$DeviceData = get_user_device_id($userId,$where);
		if(isset($DeviceData) && !empty($DeviceData)){
			foreach ($DeviceData as $val){
				$deviceId = $val['DeviceId'];
				if($deviceId != ''){
					if($val['DeviceType'] == 'ios'){
						ios_push_notification($deviceId,$NotificationType,$msg,$customeParam);
					}
					if($val['DeviceType'] == 'android'){
                    	android_push_notification($deviceId,$NotificationType,$msg,$customeParam);
					}
				}
			}
			return true;
		}
		
		return false;
	}
}
/* End of file general_function_helper.php */
/* Location: ./application/helpers/push_notification_helper.php */

