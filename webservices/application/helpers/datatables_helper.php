<?php

/*
 * function that generate the action buttons edit, delete
 * This is just showing the idea you can use it in different view or whatever fits your needs
 */

function get_buttons($id, $module) {
    $ci = & get_instance();
    $html = '<span class="actions">';
    // $html .='<a href="'.  base_url().'subscriber/edit/'.$id.'"><img src="'.  base_url().'assets/image2wbmp(image)ages/edit.png"/></a>';
    //$html .='<a href="'.  base_url().'subscriber/delete/'.$id.'"><img src="'.  base_url().'assets/images/delete.png"/></a>';
    $html = '<a href="' . base_url() . $module . '/edit/' . $id . '" style="cursor:pointer;"><i class="fa fa-pencil" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS' . $id . '" style="width: 16px; height: 16px;"></i></a>';
    $html.='</span>';

    return $html;
}

function getStatusIcon($id, $status, $module) {
    $html = '';
    if ($status == 1) {
        $html = '<a href="' . base_url() . $module . '/changestatus/' . $id . '/' . $status . '" style="cursor:pointer;"><i class="fa fa-minus-circle" data-s="18" data-n="check-circle" data-c="green" data-hc="0" id="' . rand() . '" style="width: 16px; height: 16px;"></i></a>';
    } else {
        $html = '<a href="' . base_url() . $module . '/changestatus/' . $id . '/' . $status . '" style="cursor:pointer;"><i class="fa fa-check-circle" data-s="18" data-n="check-circle" data-c="red" data-hc="0" id="' . rand() . '" style="width: 16px; height: 16px;"></i></a>';
    }
    return $html;
}

function get_list($id, $module) {
    $html = '<span class="actions">';
    $html = '<a href="' . base_url() . $module . '/couponlist/' . $id . '" style="cursor:pointer;"><i class="fa fa-list-alt" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS' . $id . '" style="width: 16px; height: 16px;"></i></a>';
    $html.='</span>';

    return $html;
}

function get_vendor_buttons($id, $module, $module2) {
    $ci = & get_instance();
    $html = '<span class="actions">';
    $html .= '<a href="' . base_url() . $module . '/edit/' . $id . '" style="cursor:pointer;"><i class="fa fa-pencil" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS' . $id . '" style="width: 16px; height: 16px;"></i></a>';
    $html .= '<a href="' . base_url() . $module2 . '/' . $id . '" style="cursor:pointer;"><i class="fa fa-list-alt" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS' . $id . '" style="width: 16px; height: 16px;"></i></a>';
    $html.='</span>';

    return $html;
}

function get_vendorgallery_buttons($vendorId, $id, $module) {
    $ci = & get_instance();
    $html = '<span class="actions">';
    $html = '<a href="' . base_url() . $module . '/edit/' .$vendorId.'/'. $id . '" style="cursor:pointer;"><i class="fa fa-pencil" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS' . $id . '" style="width: 16px; height: 16px;"></i></a>';
    $html.='</span>';

    return $html;
}

function get_photo($name, $path) {
    $ci = & get_instance();
    $html = '<img src="' . $path . $name.'" style="width:100px;"/>';
    return $html;
}