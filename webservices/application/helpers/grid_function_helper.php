<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('get_search_data')) {
    /**
     *
     * get_grid_data: it's used to get list of teacher
     *
     * @param 
     * @return array
     *
     */
    function get_search_data($aColumns = array()) {
    	$SearchType = 'ORLIKE';
    	/*
    	 * Paging
    	 */
    	$per_page =  10;
    	$offset = 0;
    		
    	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
    	{
    		$per_page =  $_GET['iDisplayLength'];
    		$offset = $_GET['iDisplayStart'];
    	}
    	
    	/*
    	 * Ordering
    	 */
    	$order_by = ""; 
    	$sort_order = "";
    	if ( isset( $_GET['iSortCol_0'] ) )
    	{
    		$order_by = "";
    		$sort_order = "";
    		
    		for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
    		{
    			if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
    			{
    				$order_by = $aColumns[ intval( $_GET['iSortCol_'.$i] ) ];
    				$sort_order = $_GET['sSortDir_'.$i];
    			}
    		}
    	}
    	
    	/*
    	 * Filtering
    	 * NOTE this does not match the built-in DataTables filtering which does it
    	 * word by word on any field. It's possible to do here, but concerned about efficiency
    	 * on very large tables, and MySQL's regex functionality is very limited
    	 */
    	$search_data = array();
    	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
    	{
    		$sWhere = "WHERE (";
    		for ( $i=0 ; $i<count($aColumns) ; $i++ )
    		{
    			$search_data[$aColumns[$i]] = $_GET['sSearch'];
    		}
    		$SearchType = 'ORLIKE';
    	}
    	
    	/* Individual column filtering */
    	for ( $i=0 ; $i<count($aColumns) ; $i++ )
    	{
    		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' && $_GET['sSearch_'.$i] != '~')
    		{
    			$search_data[$aColumns[$i]] = $_GET['sSearch_'.$i];
    			$SearchType = 'ANDLIKE';
    		}
    	}
    	$data = array();
    	$data['order_by'] = $order_by;
    	$data['sort_order'] = $sort_order;
    	$data['search_data'] = $search_data;
    	$data['per_page'] = $per_page;
    	$data['offset'] = $offset;
    	$data['SearchType'] = $SearchType;
    	return $data;
    }
    
    
}

if (!function_exists('grid_update_data')) {
	function grid_update_data($whrid_column,$id,$columnName,$value,$table){
		$ci =& get_instance();
		$data = array();
		if ($columnName == 'password'){
			$nonce = md5(uniqid(mt_rand(), true));
			$data = array(
					'password' => hash_password($value, $nonce),
					'nonce' => $nonce
			);
	
		}
		else{
			$data = array(
					$columnName    => $value);
		}
		 
		$ci->db->where($whrid_column, $id);
		$ci->db->update($table, $data);
	
		if ($columnName == 'academic_status' && $value == "Withdrawn")
			return "update_weekly_attendance";
		if($columnName != 'campus_id' && $columnName != 'campus')
			echo "success";
	}
}

if (!function_exists('grid_add_data')) {
	function grid_add_data($data = array(),$table){
		$ci =& get_instance();
                
		$ci->db->insert($table, $data);
		 
		$lastinsertid = $ci->db->insert_id();
		if ($ci->db->affected_rows() == 1) {
			return $lastinsertid;
		}
		return false;
		 
	}
}

if (!function_exists('grid_data_updates')) {
	function grid_data_updates($data = array(),$table,$wher_column_name,$id){
		$ci =& get_instance();
                
		$ci->db->where($wher_column_name, $id);
		$ci->db->update($table, $data);
		
		if($ci->db->affected_rows() == 1) {
                    return true;
		}
		return false;
	}
}

if (!function_exists('assoc_cond_data_updates')) {
	function assoc_cond_data_updates($data = array(),$table,$where = array()){
		$ci =& get_instance();
		$ci->db->where($where);
		$ci->db->update($table, $data);

		if($ci->db->affected_rows() == 1) {
			return true;
		}
		return false;
	}
}

if (!function_exists('get_update_data')) {
	function get_update_data($table,$where = array()){
		$ci =& get_instance();
		$ci->db->select('*');
		$ci->db->from($table);
		if(isset($where) && !empty($where)){
		$ci->db->where($where);
		}
		$query = $ci->db->get();
		
		if($query->num_rows() > 0) {			
			return $query->row_array();
		}
	}
}

if (!function_exists('get_table_fields')) {
	function get_table_fields($table){
		$ci =& get_instance();
		$fields = $ci->db->list_fields($table);
		
		if($fields) {
			return $fields;
		}
		return false;
	}
}

if (!function_exists('update_active_inactive')) {
	function update_active_inactive($tablename,$id, $status, $mode) {
		$ci =& get_instance();
		$idArr = array();
		if($mode == 'all'){
			$idArr = explode(',',$id);
		}
		$data = array('Status'    => $status);
                
		$flag = false;
		if(isset($idArr) && !empty($idArr) && $mode == 'all'){
			$ci->db->where_in('id',$idArr);
			$flag = true;
		}
		if($id && $flag == false){
			$ci->db->where('id', $id);
			$flag = true;
		}
		if($flag){
			$ci->db->update($tablename, $data);
		}else{
			return false;
		}
		 
	
		if($ci->db->affected_rows() == 1) {
			return true;
		}
		return false;
	}
}
if (!function_exists('deleteData')) {
	function deleteData($tablename,$id,$mode = '') {
		$ci =& get_instance();
		$idArr = array();
		if($mode == 'all'){
			$idArr = explode(',',$id);
		}
		 
		$flag = false;
		if(isset($idArr) && !empty($idArr) && $mode == 'all'){
			$ci->db->where_in('Id',$idArr);
			$flag = true;
		}
		if($id && $flag == false){
			$ci->db->where('Id', $id);
			$flag = true;
		}
		if($flag){
			if($tablename == TBL_USER){
				$ci->db->update($tablename,array('Status' => 'Deleted'));
			}else{
				$ci->db->delete($tablename);
			}			
		}else{
			return false;
		}
		 
		if($ci->db->affected_rows() == 1) {
			return true;
		}
		return false;
	}
}

if (!function_exists('or_like_search')) {
	function or_like_search($data) {
		if(!empty($data)) {
			$i=0;
			foreach ($data as $key=>$value){
				if($i == 0){
					// pass the first element of the array
					$sub = '('.$key.' LIKE \'%' . $value . '%\' ';
				}else{
					//$this->db->or_like('Linkname', $search_string_multiple);
					$sub .= 'OR ' . $key . ' LIKE \'%' . $value . '%\' ';
				}
				$i++;
			}
			$sub .= ')';
			return $sub;
		}
		return false;
	}
}

if (!function_exists('and_like_search')) {
	function and_like_search($data) {
		if(!empty($data)) {
			$i=0;
			$sub = '';
			$querystr = array();
			$query = '';
			foreach ($data as $key=>$value){
				if($i == 0){
					$sub = '( ';									
				}else{
					$sub = 'AND ';					
				}
				
				if(strtoupper($value['Operator']) == 'LIKE'){
					$querystr[] = $value['Field'] . ' LIKE \'%' . $value['Value'] . '%\' ';
					
				}else if(strtoupper($value['Operator']) == 'RANGE'){
					if(isset($value['Condition']) && !empty($value['Condition'])){
						foreach ($value['Condition'] as $val){
							$querystr[] = $val['Field']." ".$val['Operator']." '". $val['Value']."' ";
						}
					}
					
				}else{
					$querystr[] = $value['Field']." ".$value['Operator']." '". $value['Value']."' ";
				}
				
				$i++;
			}
			
			if(isset($querystr) && !empty($querystr)){
				$query = '( '.implode(' AND ', $querystr).' )';
			}
			return $query;
		}
		return false;
	}
}

if (!function_exists('get_curr_date')) {
	function get_curr_datetime($format = 'Y-m-d H:i:s') {
		return date($format,strtotime(date('Y-m-d H:i:s')));
	}
}

if (!function_exists('get_format_date')) {
	function get_format_date($date,$format = 'Y-m-d') {
		if($date){
			return date($format,strtotime($date));
		}
		return false;
	}
}

if (!function_exists('get_format_datetime')) {
	function get_format_datetime($date,$format = 'd-m-Y h:i:s a') {
		if($date){
			return date($format,strtotime($date));
		}
		return false;
	}
}
/*if (!function_exists('and_like_search')) {
	function and_like_search($data) {
		if(!empty($data)) {
			$i=0;
			foreach ($data as $key=>$value){
				if($i == 0){
					// pass the first element of the array
					$sub = '('.$key.' LIKE \'%' . $value . '%\' ';
				}else{
					//$this->db->or_like('Linkname', $search_string_multiple);
					$sub .= 'AND ' . $key . ' LIKE \'%' . $value . '%\' ';
				}
				$i++;
			}
			$sub .= ')';
			return $sub;
		}
		return false;
	}
}*/
/* End of file grid_function_helper.php */
/* Location: ./application/helpers/grid_function_helper.php */