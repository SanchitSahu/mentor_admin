<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_helper
{
    function __construct()
    {
        parent::__construct();
$this->load->model('vendor_model', '', TRUE);
    }

    function checkLogin($email_id,$password,$user_type)
    {
        if($user_type === '0')
        {
            $this->db->select('sc.*,sp.vPictureURLPath');
            $this->db->from('skin_customerdetails as sc');
            $this->db->join('skin_photo as sp','sp.iPictureURLId = sc.iPictureURLId',"left");
            $this->db->where('vEmail',$email_id);
            $this->db->where('vPassword',$password);
            $get_customer_details = $this->db->get()->row_array();

            if(count($get_customer_details) > 0)
            {
                return $get_customer_details;
            }
            else
            {
                return 'no records found';
            }
        }
        else if($user_type === '1')
        {
            $this->db->select('sv.*,sp.vPictureURLPath');
            $this->db->from('skin_vendordetails as sv');
            $this->db->join('skin_photo as sp','sp.iPictureURLId = sv.iPictureURLId',"left");
            $this->db->where('vEmail',$email_id);
            $this->db->where('vPassword',$password);
            $get_customer_details = $this->db->get()->row_array();

            if(!count($get_customer_details) > 0)
            {
                return $get_customer_details;
            }
            else
            {
                return 'no records found';
            }
        }
    }

    function forgotPassword($vEmail,$encrypted_random_password,$decrypted_random_password)
    {
        $skin_vendordetails = array();
        $skin_vendordetails['vPassword'] = $encrypted_random_password;
        $this->db->where('vEmail',$vEmail);
        $this->db->update('skin_vendordetails',$skin_vendordetails);

        $skin_customerdetails = array();
        $skin_customerdetails['vPassword'] = $encrypted_random_password;
        $this->db->where('vEmail',$vEmail);
        $this->db->update('skin_customerdetails',$skin_customerdetails);

        $to = trim($vEmail);
        $Subject = "SkinSecret : Reset Password";
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= 'From: SkinSecret <noreply@SkinSecret.com> X-Mailer: PHP/' . phpversion();
        $htmlMail = "Hello :".$vEmail."\r\n";
        $htmlMail .= "Your SkinSecret Login Password : ".$decrypted_random_password."";

        mail($to,$Subject,$htmlMail,$headers);
        return 1;
    }

    function changePassword($email,$encrypted_old_password_to_match,$encrypted_new_password)
    {
        $skin_vendordetails = array();
        $skin_vendordetails['vPassword'] = $encrypted_new_password;
        $this->db->where('vEmail',$email);
        $this->db->where('vPassword',$encrypted_old_password_to_match);
        $this->db->update('skin_vendordetails',$skin_vendordetails);

        $skin_customerdetails = array();
        $skin_customerdetails['vPassword'] = $encrypted_new_password;
        $this->db->where('vEmail',$email);
        $this->db->where('vPassword',$encrypted_old_password_to_match);
        $this->db->update('skin_customerdetails',$skin_customerdetails);

        return 1;
    }
}
