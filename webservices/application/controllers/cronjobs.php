<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cronjobs extends CI_Controller {

	
    public function __construct() {
    	parent::__construct();
        $this->load->model('push_notification_model');
        $this->load->helper('push_notification');        
	}
        
	public function send_notification()
	{
		$PushData = $this->push_notification_model->get_push_notification('all',array('NotificationSend' => 'No'));
		
		if(isset($PushData) && !empty($PushData)){
			foreach ($PushData as $Notifiy){
				$NotificationType 	= $Notifiy['NotificationType'];
				$DeviceType 		= $Notifiy['DeviceType'];
				$Message 			= $Notifiy['Message'];
				$SendToUserId 		= $Notifiy['SendToUserId'];
				$userid = '';
				if($SendToUserId > 0){
					$userid = $SendToUserId; 
				}
				
				$OtherPararm = array();
				if($Notifiy['OtherPararm']){
					$Notifiy['OtherPararm'] = (array) json_decode($Notifiy['OtherPararm']);
					if(is_array($Notifiy['OtherPararm'])){
						$OtherPararm = $Notifiy['OtherPararm'];
					}
				}
				$OtherPararm['send_to_user_id'] = $userid; 
				$result = send_push_notification($NotificationType, $Message,$DeviceType,$userid,$OtherPararm);
				
				if($result == true){
					$data = array();
					$data['NotificationSend'] 	= 'Yes';
					$data['SendDateTime'] 		= get_curr_datetime();
					if(grid_data_updates($data,'pushnotification','Id',$Notifiy['Id'])){
						//echo "PushNotification send";
					}
				}
			}
		}else{
			echo 'Notification request not found';
		}
		exit;
	}		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */