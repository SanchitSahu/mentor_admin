<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//$this->load->view('welcome_message');
		$this->load->library('twilio');
		$newTest	=	$this->twilio->load();
		//print_r($newTest);exit;
		try{
			$message = $newTest->account->messages->create(array(
				"From"	=>	"+15304412345",
				//"To"	=>	"+918128365306",
				"To"	=>	"+919898799718",
				"Body"	=>	"Test khushbu message!",
			));
			echo "Sent message {$message->sid}";
		}
		catch (Services_Twilio_RestException $e) {
			echo $e->getMessage();
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */