<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

define("EMAil_LOGIN_INFO_ABSOLUTE_ID", 2);
define("EMAIL_CHANGE_PASSWORD_ABSOLUTE_ID", 3);
define("EMAIL_PROFILE_UPDATE_ABSOLUTE_ID", 4);
define("EMAIL_MEETING_REQUEST_ABSOLUTE_ID", 5);
define("EMAIL_NEW_MEETING_DATE_AND_TIME_ABSOLUTE_ID", 6);
define("EMAIL_MEETING_CANCELLED_ABSOLUTE_ID", 7);
define("EMAIL_RESCHEDULE_REQUEST_ABSOLUTE_ID", 8);
define("EMAIL_MEETING_DATE_AND_TIME_ABSOLUTE_ID", 9);
define("EMAIL_MISSED_MEETING_ABSOLUTE_ID", 10);
define("EMAIL_MISSED_MEETING_CANCELLED_ABSOLUTE_ID", 11);

Class mentorServiceDev extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('topic_model', '', TRUE);
        $this->load->model('mentee_model', '', TRUE);
        $this->load->model('meetingtype_model', '', TRUE);
        $this->load->model('skills_model', '', TRUE);
        $this->load->model('mentor_model', '', TRUE);
        $this->load->model('emailtemplate_model', '', TRUE);
        ini_set('memory_limit', '-1');
        //ini_set('display_errors', "ON");
        //error_reporting(E_ALL);

        $this->load->library('email');
        $this->load->library('upload');
        $this->load->helper('url');
        $this->data['base_upload'] = $this->config->item('base_upload');
        $this->data['base_upload_path'] = $this->config->item('base_upload_path');
        $this->data['base_upload_url'] = $this->config->item('base_upload_url');

        $res = loadDatabase();
        if ($res != 0) {
            $x = $this->load->database($res, TRUE);
            $this->db = $x;
        } else {
            redirect("400.shtml");
            exit;
        }
        $db_name = $this->db->database;
        
        ini_set('memory_limit', '-1');
    }

    function index() {
//echo 'dfsdfs';exit;
        ini_set('display_errors', "ON");
        error_reporting(E_ALL);
        header('Content-type: application/json');
        $data = file_get_contents('php://input');

        if ($data == null) {
            //http://localhost/mentorApp/webservices/index.php/mentorService
            //$data = '{"method":"deleteAccount","body":{"iCustomerId":"2"}}';
            //$data = '{"method":"getTopic","body":{}}';
            //$data = '{"method":"getSubTopic","body":{}}';
            //$data = '{"method":"getMentee","body":{}}';
            //$data = '{"method":"getMentee","body":{"MentorID":"1"}}';
            //$data = '{"method":"getMeetingType","body":{}}';
            //$data = '{"method":"getSkills","body":{}}';
            //$data = '{"method":"getMentorActionTaken","body":{}}';
            //$data = '{"method":"getMenteeActionTaken","body":{}}';
            //$data = '{"method":"registerUser","body":{"Email":"nerikar.krunal@gmail.com","Password":"","AccessToken":"","UserType":"0"}}';
            //$data = '{"method":"checkLogin","body":{"Email":"username","Password":"password"}}';
            //		{"method":"checkLogin","body":{"UserName":"Avinash","Password":"avinash"}}
            //$data = '{"method":"forgotPassword","body":{"Email":"email_id"}}';
            //$data = '{"method":"changePassword","body":{"Email":"krunal@gmail.com","OldPassword":"123krunal","NewPassword":"Test","UserType":"0"}}';
            //$data = '{"method":"submitMeetingInfo","body":{"MeetingID":"1","MentorID":"1","MenteeID":"1","MeetingStartDatetime":"2015-07-10 09:42:02","MeetingEndDatetime":"2015-07-10 10:42:02","MeetingTypeID":"1","MeetingTopicID":"1","MeetingSubTopicID":"1","MeetingPlaceID":"0","MenteeActionIDs":"1,2","MentorActionIDs":"1,2","MeetingElapsedTime":"2.5","MeetingFeedback":"testing","MentorActionItemDone":"1,2","MenteeActionItemDone":"1"}}';
            //$data = '{"method":"submitMeetingInfo","body":{"MeetingID":"1","MentorID":"19","MenteeID":"19","MeetingStartDatetime":"2015-07-10 09:42:02","MeetingEndDatetime":"2015-07-10 10:42:02","MeetingTypeID":"1","MeetingTopicID":"1","MeetingSubTopicID":"1","MeetingPlaceID":"0","MenteeActionIDs":"1,2","MentorActionIDs":"1,2","MeetingElapsedTime":"2.5","MeetingFeedback":"testing","MentorActionItemDone":"1,2","MenteeActionItemDone":"1","SessionStatus":"2"}}'	            
            //$data = '{"method":"menteeComment","body":{"MentorID":"1","MenteeID":"1","MeetingID":"2","MenteeComment":"test","CareC":"1","CareA":"1","CareR":"1","CareE":"1"}}';
            //$data = '{"method":"mentorComment","body":{"MentorID":"1","MenteeID":"1","MeetingID":"2","MentorComment":"1","SatisfactionIndex":"1"}}';
            //$data = '{"method":"addSkill","body":{"UserID":"1","UserType":"0","SkillID":"1,2"}}';
            //$data = '{"method":"getMeetingHistory","body":{"UserId":"1","UserType":"0","CurrentPage":"1","PageSize":"10"}}';
            //$data = '{"method":"getMeetingHistory","body" {"UserId":"1","UserType":"0","CurrentPage":"1","PageSize":"10","MentorComment":"10","SatisfactionIndex":"10","MenteeComment":"10","CareC":"10","CareA":"10","CareR":"10","CareE":"10"}}';
            //$data = '{"method":"getMentor","body":{}}';
            //$data = '{"method":"getMentor","body":{"MenteeID":"1"}}';
            //$data = '{"method":"mentorInvitation","body":{"MentorID":"1","MenteeID":"1","TopicID":"1","MenteeComments":"khushbu"}}';
            //$data = '{"method":"getMentorPendingRequest","body":{"MentorID":"1"}}';
            //$data = '{"method":"mentorResponse","body":{"InvitationId":"1","Status":"Accepted","Comments":"test....."}}';
            //$data = '{"method":"editProfile","body":{"UserId":"1","UserType":"0","UserName":"dasf","PhoneNumber":"9812345678","Pass":"124562","Email":"reg.charney+mentor1@entrebahn.com"}}';
            //$data = '{"method":"updateMenteeactionsItemsWithDone","body":{"MenteeID":"1","MeetingID":"0","ActionId":"1","MenteeActionItemDone":"1,2"}}';
            //$data	= '{"method":"getMentorForInvitation","body":{"MenteeID":"1"}}	'
            //$data = '{"method":"startEndMeetingSession","body":{"SessionStatus":"1","DateTime":"","MentorID":"1","MenteeID":"1","SessionStatus":"1"}}	'
            //$data	= '{"method":"getMentorWaitScreen","body":{"MenteeID":"1"}}' 
            //$data	= '{"method":"getMettingWaitScreenInfo","body":{"MentorID":"1","MenteeID":"1"}}'
            //$data	= '{"method":"getConfig","body":{}}' 
            //$data	= '{"method":"mentorIamge","body":{"MentorID":"1"}}'
            //$data	= '{"method":"updateMentorIamge","body":{"MentorID":"1"}}'
            //$data	= '{"method":"deleteMentorIamge","body":{"MentorID":"1"}}'
        }

        if ($data != '') {
            $data = $this->checkvalidjson($data);
            $method = $data->method;
            $json = $data->body;
        } else {
            if ($_POST["method"] == "editProfile") {
                $this->editProfile($_POST);
            }
            $method = $this->input->post('method');
            $json = array(
                'UserID' => $this->input->post('UserID'),
                'UserType' => $this->input->post('UserType'),
                'PictureType' => $this->input->post('PictureType'),
            );
        }


        switch ($method) {
            case 'getSchoolList':
                $this->getSchoolList();
                break;
            case 'getSourceList':
                $this->getSourceList($json);
                break;
            case 'getNewMeetingDetails':
                $this->getNewMeetingDetails($json);
                break;
            case 'updateMenteeactionsItemsWithDone':
                $this->updateMenteeactionsItemsWithDone($json);
                break;
            case 'editProfile':
                $this->editProfile($json);
                break;
            case 'getProfile':
                $this->getProfile($json);
                break;
            case 'mentorResponse':
                $this->mentorResponse($json);
                break;
            case 'getMentorPendingRequest':
                $this->getMentorPendingRequest($json);
                break;
            case 'getMentorPendingMeeting':
                $this->getMentorPendingMeeting($json);
                break;
            case 'mentorInvitation':
                $this->mentorInvitation($json);
                break;
            case 'getMeetingHistory':
                $this->getMeetingHistory($json);
                break;
            case 'addSkill':
                $this->addSkill($json);
                break;
            case 'mentorComment':
                $this->mentorComment($json);
                break;
            case 'menteeComment':
                $this->menteeComment($json);
                break;
            case 'submitMeetingInfo':
                $this->submitMeetingInfo($json);
                break;
            case 'changePassword':
                $this->changePassword($json);
                break;
            case 'forgotPassword':
                $this->forgotPassword($json);
                break;
            case 'registerUser':
                $this->registerUser($json);
                break;
            case 'getTopic':
                $this->getTopic();
                break;
            case 'getMentor':
                $this->getMentor($json);
                break;
            case 'getSubTopic':
                $this->getSubTopic();
                break;
            case 'getMentee':
                $this->getMentee($json);
                break;
            case 'getMeetingType':
                $this->getMeetingType();
                break;
            case 'getMeetingPlace':
                $this->getMeetingPlace();
                break;
            case 'getSkills':
                $this->getSkills();
                break;
            case 'getMentorActionTaken':
                $this->getMentorActionTaken();
                break;
            case 'getMenteeActionTaken':
                $this->getMenteeActionTaken();
                break;
            case 'checkLogin':
                $this->checkLogin($json);
                break;
            case 'addUploadPics':
                $this->addUploadPics($json);
                break;
            case 'getMentorForInvitation':
                $this->getMentorForInvitation($json);
                break;
            case 'startEndMeetingSession':
                $this->startEndMeetingSession($json);
                break;
            case 'getMentorWaitScreen':
                $this->getMentorWaitScreen($json);
                break;
            case 'getMettingWaitScreenInfo':
                $this->getMettingWaitScreenInfo($json);
                break;
            case 'getConfig':
                $this->getConfig($json);
                break;
            case 'mentorIamge':
                $this->mentorIamge($json);
                break;
            case 'updateMentorIamge':
                $this->updateMentorIamge($json);
                break;
            case 'deleteMentorIamge':
                $this->deleteMentorIamge($json);
                break;
			case 'rescheduleMeeting':
				$this->rescheduleMeeting($json);
                break;
			case 'cancelMeeting':
				$this->cancelMeeting($json);
                break;
			case 'getHelpText':
				$this->getHelpText($json);
                break;
			case 'getMentorWaitScreenAccepted':
                $this->getMentorWaitScreenAccepted($json);
                break;	
			case 'getMentorWaitScreenRejected':
                $this->getMentorWaitScreenRejected($json);
                break;
			case 'getMentorWaitScreenPending':
                $this->getMentorWaitScreenPending($json);
                break;
            case 'getDeclineResponses' :
                $this->getDeclineResponses($json);
                break;
            case 'getAcceptResponses' :
                $this->getAcceptResponses($json);
                break;
            case 'acceptRescheduledMeetingByMentor' :
                $this->acceptRescheduledMeetingByMentor($json);
                break;
            case 'acceptAcceptedMeetingByMentor':
                $this->acceptAcceptedMeetingByMentor($json);
                break;
            case 'getUserNotes':
                $this->getUserNotes($json);
                break;
            case 'addUserNote':
                $this->addUserNote($json);
                break;
            case 'deleteUserNote':
                $this->deleteUserNote($json);
                break;
            case 'getMenteeForInvitation':
                $this->getMenteeForInvitation($json);
                break;
            case 'generatePassword':
                $this->generatePassword($json);
                break;
			case 'decryptPassword':
                $this->decryptPassword($json);
                break;
            case 'test_message':
                $this->test_message($json);
                break;
            default:
                $this->default_fun();
                break;
        }
    }

    function default_fun() {
        $response = array();
        $response['default']['Error'] = 1;
        $response['default']['Message'] = 'No methods found';
        echo json_encode($response);
        exit();
    }
    
    function test_message($data) {
        $this->load->library('twilio');
        $newTest	=	$this->twilio->load();
        //print_r($newTest);exit;
        try{
            $message = $newTest->account->messages->create(array(
                "From"	=>	TWILIO_FROM_NUMBER,
                
                "To"	=>	$data->number,
                "Body"	=>	"This is a test message from Twilio. Testing the Twilio Integration" ,
            ));
            echo "Sent message {$message->sid}";
        } catch (Services_Twilio_RestException $e) {
            echo $e->getMessage();exit;
        }
    }
    
    function generatePassword($data) {
        $response['password'] = fnEncrypt($data->password, $this->config->item('mentorKey'));
        echo json_encode($response);
        exit();
    }
	
	function decryptPassword($data) {
        $response['password'] = fnDecrypt($data->password, $this->config->item('mentorKey'));
        echo json_encode($response);
        exit();
    }
    
    function addUserNote($data) {
        if ($data->UserID == '') {
            $response['addUserNote']['Error'] = 2;
            $response['addUserNote']['Message'] = 'UserID not found';
        } else if ($data->UserType == '') {
            $response['addUserNote']['Error'] = 2;
            $response['addUserNote']['Message'] = 'UserType not found';
        } else {
            $results = $this->mentor_model->addUserNote($data);
            if (isset($results[0]->NoteID) && $results[0]->NoteID == 0) {
                $response['addUserNote']['Error'] = 0;
                $response['addUserNote']['Message'] = "Note Updated Successfully.";
                $response['addUserNote']['data'] = $results;
            } else if (isset($results[0]->NoteID) && $results[0]->NoteID > 0) {
                $response['addUserNote']['Error'] = 0;
                $response['addUserNote']['Message'] = "Note Saved Successfully.";
                $response['addUserNote']['data'] = $results;
            } else {
                $response['addUserNote']['Error'] = 1;
                $response['addUserNote']['Message'] = 'Error occured while saving note. Try again.';
            }
        }
        echo json_encode($response);
        exit();
    }
    
    function deleteUserNote($data) {
        if ($data->NoteID == '') {
            $response['deleteUserNote']['Error'] = 2;
            $response['deleteUserNote']['Message'] = 'NoteID not found';
        } else {
            $results = $this->mentor_model->deleteUserNote($data->NoteID);
            if ($results) {
                $response['deleteUserNote']['Error'] = 0;
                $response['deleteUserNote']['Message'] = "Note Deleted Successfully.";
            } else {
                $response['deleteUserNote']['Error'] = 1;
                $response['deleteUserNote']['Message'] = 'Error occured while deleting note. Try again.';
            }
        }
        echo json_encode($response);
        exit();
    }
    
    function getUserNotes($data) {
        if ($data->UserID == '') {
            $response['getUserNotes']['Error'] = 2;
            $response['getUserNotes']['Message'] = 'UserID not found';
        } else if ($data->UserType == '') {
            $response['getUserNotes']['Error'] = 2;
            $response['getUserNotes']['Message'] = 'UserType not found';
        } else {
            $results = $this->mentor_model->getUserNotes($data->UserID, $data->UserType);
            if (isset($results[0]->NoteID) && $results[0]->NoteID != "") {
                $response['getUserNotes']['Error'] = 0;
                $response['getUserNotes']['Message'] = "User notes found.";
                $response['getUserNotes']['data'] = $results;
            } else {
                $response['getUserNotes']['Error'] = 1;
                $response['getUserNotes']['Message'] = 'User notes not found';
            }
        }
        echo json_encode($response);
        exit();
    }
    
    function acceptAcceptedMeetingByMentor($data){
        $response = array();
        if ($data->InvitationID == '') {
            $response['acceptAcceptedMeetingByMentor']['Error'] = 2;
            $response['acceptAcceptedMeetingByMentor']['Message'] = 'Invitation Id not found';
        } else {
            $emailData = array();
            $arrSearch = array(
                    '##MEETING_TYPE##',
                    "##MEETING_DATE##",
                    "##MEETING_TIME##",
                    "##MAIN_TOPIC##",
                    '##MENTEE_NAME##'
                );
            $arrReplace = array(
                    $data->MeetingTypeName,
                    date('m-d-Y',strtotime($data->MeetingDateTime)),
                    date('H:i',strtotime($data->MeetingDateTime)),
                    $data->TopicDescription,
                    $data->MenteeName
                );
            //$body = "##MENTEE_NAME## has confirmed to attend the ##MEETING_TYPE## meeting on ##MEETING_DATE## at ##MEETING_TIME## to discuss ##MAIN_TOPIC##.";
            $body = "##MENTEE_NAME## has confirmed that they will attend the ##MEETING_TYPE## meeting on ##MEETING_DATE## at ##MEETING_TIME## to discuss ##MAIN_TOPIC##";
            $body = str_replace($arrSearch, $arrReplace, $body);
            $subject = "Meeting Confirmation Notification";
                
            $emailData['toEmail'] = $data->MentorEmail;

            /*send message */
            $this->load->library('twilio');
            $newTest	=	$this->twilio->load();
            try{
                $message = $newTest->account->messages->create(
                    array(
                        "From"	=>	TWILIO_FROM_NUMBER,
                        "To"	=>	$data->MentorPhone,
                        "Body"	=>	$body
                    )
                );
            } catch (Services_Twilio_RestException $e) {
                //echo $e->getMessage();exit;
            }

            $emailData['subject'] = $subject;
            $emailData['body'] = $body;

            $results = send_mail($emailData);
            
            if ($results === true) {
                $response['acceptAcceptedMeetingByMentor']['Error'] = 0;
                $response['acceptAcceptedMeetingByMentor']['Message'] = "Confirmation send successfully";
            } else {
                $response['acceptAcceptedMeetingByMentor']['Error'] = 1;
                $response['acceptAcceptedMeetingByMentor']['Message'] = 'Something is not right here try again later!';
            }
        }
        echo json_encode($response);
        exit();
    }
    
    function acceptRescheduledMeetingByMentor($data){
        $response = array();
        if ($data->InvitationID == '') {
            $response['acceptRescheduledMeetingByMentor']['Error'] = 2;
            $response['acceptRescheduledMeetingByMentor']['Message'] = 'Invitation Id not found';
        } else {
            $results = $this->mentor_model->acceptRescheduledMeetingByMentor($data->InvitationID);
            if ($results === true) {
                $response['acceptRescheduledMeetingByMentor']['Error'] = 0;
                $response['acceptRescheduledMeetingByMentor']['Message'] = "Reschedule request Accepted successfully";
            } else {
                $response['acceptRescheduledMeetingByMentor']['Error'] = 1;
                $response['acceptRescheduledMeetingByMentor']['Message'] = 'Something is not right here try again later!';
            }
        }
        echo json_encode($response);
        exit();
    }
    
    function getSchoolList() {
        $response = array();
        $results = $this->mentor_model->getSchoolList();
        if (isset($results[0]->subdomain_id) && $results[0]->subdomain_id != "") {
            $response['getSchoolList']['Error'] = 0;
            $response['getSchoolList']['Message'] = "School details found.";
            $response['getSchoolList']['data'] = $results;
        } else {
            $response['getSchoolList']['Error'] = 1;
            $response['getSchoolList']['Message'] = 'School details not found';
        }
        echo json_encode($response);
        exit();
    }

    function getNewMeetingDetails($data) {
        $response = array();
        if ($data->MentorId == '') {
            $response['meetingDetailsResponse']['Error'] = 2;
            $response['meetingDetailsResponse']['Message'] = 'Mentor ID not found';
        } else {
            $results = $this->mentor_model->getNewMeetingDetails($data->MentorId);
            if (isset($results[0]->MenteeId) && $results[0]->MenteeId != "") {
                $response['meetingDetailsResponse']['Error'] = 0;
                $response['meetingDetailsResponse']['Message'] = "New meeting details found.";
                $response['meetingDetailsResponse']['data'] = $results;
            } else {
                $response['meetingDetailsResponse']['Error'] = 1;
                $response['meetingDetailsResponse']['Message'] = 'New Meeting details not found';
            }
        }
        echo json_encode($response);
        exit();
    }
    
    function updateMenteeactionsItemsWithDone($data) {
        $response = array();
        if ($data->MenteeID == '') {
            $response['updateMenteeactionsItemsWithDone']['Error'] = 2;
            $response['updateMenteeactionsItemsWithDone']['Message'] = 'Mentee not found';
        } else if ($data->MeetingID == '') {
            $response['updateMenteeactionsItemsWithDone']['Error'] = 2;
            $response['updateMenteeactionsItemsWithDone']['Message'] = 'Meeting not found';
        } else if ($data->ActionId == '') {
            $response['updateMenteeactionsItemsWithDone']['Error'] = 2;
            $response['updateMenteeactionsItemsWithDone']['Message'] = 'Action not found';
        } else {
            if ($data->MeetingID > 0) {
                $this->db->where(array('MenteeID' => $data->MenteeID, 'MeetingID' => $data->MeetingID));
                $this->db->delete('menteeactionstaken');
            }
            $Actionids = explode(',', $data->ActionId);
            foreach ($Actionids as $val) {
                $menteeResults = $this->mentor_model->updateMenteeactionsItemsWithDoneDetail($data->MenteeID, $data->MeetingID, $val);
            }
            $results = $this->mentor_model->updateMeetingMenteeactionsItemsWithDoneDetail($data->MeetingID, $data->MenteeActionItemDone);
            if (isset($results[0]->Id) && $results[0]->Id != "") {
                $response['updateMenteeactionsItemsWithDone']['Error'] = 0;
                $response['updateMenteeactionsItemsWithDone']['Message'] = 'Mentee action items updated successfully';
                $response['updateMenteeactionsItemsWithDone']['data'] = $results[0];
            } else if (isset($results[0]->Message) && $results[0]->Message != "") {
                $response['updateMenteeactionsItemsWithDone']['Error'] = 2;
                $response['updateMenteeactionsItemsWithDone']['Message'] = $results[0]->Message;
            } else {
                $response['updateMenteeactionsItemsWithDone']['Error'] = 1;
                $response['updateMenteeactionsItemsWithDone']['Message'] = 'Something is not right here try again later!' .
			' (__FILE__@__LINE__ in __FUNCTION__)';
            }
        }
        echo json_encode($response);
        exit();
    }

    function getProfile($data) {
        $response = array();
        if ($data->UserId == '') {
            $response['getProfile']['Error'] = 2;
            $response['getProfile']['Message'] = 'User not found';
        } else if ($data->UserType == '') {
            $response['getProfile']['Error'] = 2;
            $response['getProfile']['Message'] = 'UserType not found';
        } else {
            $userData = $this->mentor_model->getProfile($data->UserId, $data->UserType);
			$userData[0]->Password	=	fnDecrypt($userData[0]->Password, $this->config->item('mentorKey'));
            if (isset($userData[0]->Id) && $userData[0]->Id != "") {
                $response['getProfile']['Error'] = 0;
                $response['getProfile']['Message'] = 'Profile Data';
                $response['getProfile']['data'] = $userData[0];
            } else if (isset($userData[0]->Message) && $userData[0]->Message != "") {
                $response['getProfile']['Error'] = 2;
                $response['getProfile']['Message'] = $userData[0]->Message;
            } else {
                $response['getProfile']['Error'] = 1;
            }
        }
        echo json_encode($response);
        exit();
    }
    
    
    function editProfile($data) {
        $response = array();
        if ($_POST['UserId'] == '') {
            $response['editProfile']['Error'] = 2;
            $response['editProfile']['Message'] = 'User not found';
        } else if ($_POST['UserType'] == '') {
            $response['editProfile']['Error'] = 2;
            $response['editProfile']['Message'] = 'UserType not found';
        } else if (!isset($_FILES) && count($_FILES) <= 0) {
            $response['editProfile']['Error'] = 2;
            $response['editProfile']['Message'] = 'Image not uploaded';
        } else {
            $resArray = array();
            $Path = "";
            if (!empty($_FILES)) {
                foreach ($_FILES as $val) {
                    $microtime = microtime();
                    $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/mentor/assets/admin/images/admin/";

                    $mime = explode('/', $val['type'][0]);

                    $Path = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $mime[1];
                    $target_dir2 = $target_dir . $Path;
                    move_uploaded_file($val["tmp_name"][0], $target_dir2);
                }
            }
            if ($Path == "") {
                $userData = $this->mentor_model->getProfileDetail($_POST['UserId'], $_POST['UserType']);
                $Path = $userData['MentorImage'];
            }

            $results = $this->mentor_model->editProfileImage($_POST['UserId'], $_POST['UserType'], $Path);
            if (isset($results[0]->Id) && $results[0]->Id != "") {
                $response['editProfile']['Error'] = 0;
                $response['editProfile']['Message'] = 'Profile updated successfully';
                $response['editProfile']['data'] = $results[0];
            } else if (isset($results[0]->Message) && $results[0]->Message != "") {
                $response['editProfile']['Error'] = 2;
                $response['editProfile']['Message'] = $results[0]->Message;
            } else {
                $response['editProfile']['Error'] = 1;
            }
        }
        echo json_encode($response);
        exit();
    }

    function mentorResponse($data) {
        $response = array();
        if ($data->InvitationId == '') {
            $response['mentorResponse']['Error'] = 2;
            $response['mentorResponse']['Message'] = 'Invitation not found';
        } else if ($data->Status == '') {
            $response['mentorResponse']['Error'] = 2;
            $response['mentorResponse']['Message'] = 'Status not found';
        } else {
            $results = $this->mentor_model->mentorResponseDetail($data);
            if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "") {
				
				$res	=	$this->mentor_model->mentorResponseEmail($data);
				//print_r($res);exit;
				if(isset($data->Status) && $data->Status == 'Rejected')
                    $data->Status = 'Declined';
				
				if(isset($data->RescheduleComments)){
					$emailtemplate = $this->emailtemplate_model->get_emailtempate_details(EMAIL_RESCHEDULE_REQUEST_ABSOLUTE_ID);
					if ($emailtemplate) {
						$emailData = array();
  
                        $emailData['toEmail'] = $res[0]->MenteeEmail;
						$emailData['cc'] = $res[0]->MentorEmail;
                        
                        $arrSearch = array('##FIRST_NAME##',"##EMAIL##","##MENTEE_NAME##","##TopicDescription##","##REASON##","##NEWDATE##","##NEWTIME##");
                        $arrReplace = array(ucwords($results[0]->MenteeName),$results[0]->MenteeEmail,ucwords($results[0]->MenteeName),$results[0]->TopicDescription,$data->RescheduleComments,$new_date,$new_time);
      
    
                        $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                        $emailData['subject'] = $subject;
    
                        $body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                        $emailData['body'] = $body;
    
    
                        send_mail($emailData);
						
						/*send message */
						$this->load->library('twilio');
						$newTest	=	$this->twilio->load();
						//print_r($newTest);exit;
						try{
							$message = $newTest->account->messages->create(array(
								"From"	=>	TWILIO_FROM_NUMBER,
								"To"	=>	$res[0]->MenteePhone,
								"Body"	=>	"upcoming Meeting with {$res[0]->MenteeName} to discuss {$res[0]->TopicDescription} has been rescheduled" ,
							));
						//	echo "Sent message {$message->sid}";
						} catch (Services_Twilio_RestException $e) {
							//echo $e->getMessage();exit;
						}
					}
				} else
                {
					if($data->Status != 'Declined') {
						$newdate_timestamp = strtotime($data->MeetingDateTime);
                        $new_date = date('M j, Y', $newdate_timestamp); 
                        $new_time = date('g:i A', $newdate_timestamp); 

						
						$emailtemplate = $this->emailtemplate_model->get_emailtempate_details(EMAIL_MEETING_DATE_AND_TIME_ABSOLUTE_ID);
						if ($emailtemplate) {
                            $emailData = array();
    
                            $emailData['toEmail'] = $res[0]->MenteeEmail;
                            $emailData['cc'] = $res[0]->MentorEmail;
                            
                            $arrSearch = array('##FIRST_NAME##',"##EMAIL##","##MENTOR_NAME##","##TopicDescription##","##STATUS##","##DATE##","##TIME##");
                            $arrReplace = array(ucwords($res[0]->MenteeName),$res[0]->MenteeEmail,ucwords($res[0]->MentorName),$res[0]->TopicDescription,$data->Status,$new_date,$new_time);
          
    
                            $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                            $emailData['subject'] = $subject;
    
                            $body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                            $emailData['body'] = $body;
    
    
                            send_mail($emailData);
                            
                            /*send message */
                            $this->load->library('twilio');
                            $newTest	=	$this->twilio->load();
                            //print_r($newTest);exit;
                            try{
                                $message = $newTest->account->messages->create(array(
                                    "From"	=>	TWILIO_FROM_NUMBER,
                                    //"To"	=>	"+918128365306",
                                    "To"	=>	$res[0]->MenteePhone,
                                    "Body"	=>	"upcoming Meeting with {$res[0]->MentorName} to discuss {$res[0]->TopicDescription} has been {$data->Status}" ,
                                ));
                            //	echo "Sent message {$message->sid}";
                            } catch (Services_Twilio_RestException $e) {
                                //echo $e->getMessage();exit;
                            }
                        }
                    }
				}
				
					
				
				
                $response['mentorResponse']['Error'] = 0;
                if($data->Status == 'Rejected')
                    $data->Status = 'Declined';
                $response['mentorResponse']['Message'] = 'Invitation ' . $data->Status . ' successfully';
                $response['mentorResponse']['data'] = $results;
            } else if (isset($results[0]->Message) && $results[0]->Message != "") {
                $response['mentorResponse']['Error'] = 2;
                $response['mentorResponse']['Message'] = $results[0]->Message;
            } else {
                $response['mentorResponse']['Error'] = 1;
                $response['mentorResponse']['Message'] = 'Something is not right here try again later!' .
			' (__FILE__@__LINE__ in __FUNCTION__)';
            }
        }
        
        echo json_encode($response);
        exit();
    }

    function getMentorPendingRequest($data) {
        $response = array();
        if ($data->MentorID == '') {
            $response['getMentorPendingRequest']['Error'] = 2;
            $response['getMentorPendingRequest']['Message'] = 'Mentor not found';
        } else {
            $results = $this->mentor_model->getMentorPendingRequestDetail($data->MentorID);
            
            $resultPending = $this->meetingtype_model->getPendingDetail($data->MentorID);
            $resultPendingMeeting = $this->meetingtype_model->getMentorPendingMeetingCount($data->MentorID);
            
			
			
            $response['getMentorPendingRequest']['pending'] = $resultPending[0]->sum1;
            $response['getMentorPendingRequest']['pendingMeeting'] = (string) $resultPendingMeeting;
            if (isset($results[0]->MenteeID) && $results[0]->MenteeID != "") {
				foreach($results as $val){
					if($val->Status == 'Rescheduled')
						$val->inviteTime = date("Y-m-d H:i", strtotime($val->MeetingDateTime));
					else
						$val->inviteTime = date("Y-m-d H:i", strtotime($val->inviteTime));
				}
                $response['getMentorPendingRequest']['Error'] = 0;
                $response['getMentorPendingRequest']['Message'] = 'Invitation found successfully';
                $response['getMentorPendingRequest']['data'] = $results;
            } else if (isset($results[0]->Message) && $results[0]->Message != "") {
                $response['getMentorPendingRequest']['Error'] = 2;
                $response['getMentorPendingRequest']['Message'] = $results[0]->Message;
            } else {
                $response['getMentorPendingRequest']['Error'] = 1;
                $response['getMentorPendingRequest']['Message'] = 'Something is not right here try again later!' .
			' (__FILE__@__LINE__ in __FUNCTION__)';
            }
        }
        echo json_encode($response);
        exit();
    }
    
    function getMentorPendingMeeting($data){
        $response = array();
        $resultPending = $this->meetingtype_model->getPendingDetail($data->MentorID);
        $resultPendingMeeting = $this->meetingtype_model->getMentorPendingMeetingCount($data->MentorID);
        $response['getMentorPendingMeeting']['pending'] = $resultPending[0]->sum1;
        $response['getMentorPendingMeeting']['pendingMeeting'] = (string) $resultPendingMeeting;
        if ($data->MentorID == '') {
            $response['getMentorPendingMeeting']['Error'] = 2;
            $response['getMentorPendingMeeting']['Message'] = 'Mentor not found';
        } else {
            $results = $this->mentor_model->getMentorPendingMeetingDetail($data->MentorID);
            if (isset($results[0]->MenteeID) && $results[0]->MenteeID != "") {
                $response['getMentorPendingMeeting']['Error'] = 0;
                $response['getMentorPendingMeeting']['Message'] = 'Mentor pending meeting found successfully';
                $response['getMentorPendingMeeting']['data'] = $results;
            } else if (isset($results[0]->Message) && $results[0]->Message != "") {
                $response['getMentorPendingMeeting']['Error'] = 2;
                $response['getMentorPendingMeeting']['Message'] = $results[0]->Message;
            } else {
                $response['getMentorPendingMeeting']['Error'] = 1;
                $response['getMentorPendingMeeting']['Message'] = 'Something is not right here try again later!' .
			' (__FILE__@__LINE__ in __FUNCTION__)';
            }
        }
        echo json_encode($response);
        exit();
    }

    function mentorInvitation($data) {
        $response = array();
        $this->load->library('email');
        if ($data->MentorID == '') {
            $response['mentorInvitation']['Error'] = 2;
            $response['mentorInvitation']['Message'] = 'Mentor not found';
        } else if ($data->MenteeID == '') {
            $response['mentorInvitation']['Error'] = 2;
            $response['mentorInvitation']['Message'] = 'Mentee not found';
        } else if ($data->TopicID == '') {
            $response['mentorInvitation']['Error'] = 2;
            $response['mentorInvitation']['Message'] = 'Topic not found';
        } else if ($data->MenteeComments == '') {
            $response['mentorInvitation']['Error'] = 2;
            $response['mentorInvitation']['Message'] = 'Mentee comments not found';
        } else {
            //print_r($data);exit;
			$config = $this->mentor_model->getConfig();
			
			if($config[0]->expiryTime == 0 || $config[0]->expiryTime == "") $expiryTime = 1;
			else $expiryTime	=	$config[0]->expiryTime;
			$day = (int) ($expiryTime/24);
            $hours = $expiryTime%24;
			$dayText = '';
			$hourText = '';
			if(!empty($day)){ 
				$dayText = $day . (($day < 2) ? " day": " days");
			}
			
			if(!empty($hours)){
				$hourText = $hours . (($hours < 2) ? " hour": " hours");
			}
			$expiryTime	=	$dayText . " " .$hourText;
							
            $results = $this->mentor_model->mentorInvitationDetail($data->MentorID, $data->MenteeID, $data->TopicID, $data->MenteeComments, $data->MenteeEmail, $data->MenteePhone, $data->SmsCapable);
            if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "") {

                $response = array();

                $results1 = $this->mentor_model->checkInvitationDetail($data->MentorID, $data->MenteeID);
                //print_r($results1);exit;
                If (isset($results1[0]->InvitationID) && $results1[0]->InvitationID > 0) {
                    // SEND EMAIL TO CUSTOMER 
/* XXXXXXXXXXXXX Use of "magic" numbers, like 5, is almost unforgivable.
   XXXXXXXXXXXXX If you are using absolute IDs, use DEFINEs that explain
   XXXXXXXXXXXXX the dependency. For example: EMAIL_MEETING_REQUEST_ABSOLUTE_ID
*/
                    $emailtemplate = $this->emailtemplate_model->get_emailtempate_details(EMAIL_MEETING_REQUEST_ABSOLUTE_ID);
/* XXXXXXXXXXXXX */
                    //print_r($emailtemplate);exit;
                    if ($emailtemplate) {
                        $emailData = array();

                        $emailData['toEmail'] = $results1[0]->MentorEmail;
                        $emailData['cc'] = $results1[0]->MenteeEmail;
                        $arrSearch = array('##FIRST_NAME##', "##EMAIL##","##EXPIRYDATE##");
                        $arrReplace = array($results1[0]->MentorName,$results1[0]->MentorEmail,$expiryTime);

                        $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                        $emailData['subject'] = $subject;


                        $body = $results1[0]->MenteeName . " has asked for a meeting to discuss " . $results1[0]->TopicDescription . " related issues with you. Please use your MELS App to accept or decline the invitation in the next ".$expiryTime.", otherwise the invitation will expire.";
                        //$body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                        $emailData['body'] = $body;


                        send_mailInvitation($emailData);

                        //$results123 = $this->mentor_model->addinviteList($data->MentorID, $data->MenteeID);
                    }
					
					
					/*send message */
					$this->load->library('twilio');
					$newTest	=	$this->twilio->load();
					//print_r($newTest);exit;
					try{
						$message = $newTest->account->messages->create(array(
							"From"	=>	TWILIO_FROM_NUMBER,
							//"To"	=>	"+918128365306",
							"To"	=>	$results1[0]->MentorPhone,
							"Body"	=>	$results1[0]->MenteeName . " has asked for a meeting to discuss " . $results1[0]->TopicDescription . " related issues with you. Please use your MELS App to accept or decline the invitation in the next ".$expiryTime.", otherwise the invitation will expire.",
						));
					//	echo "Sent message {$message->sid}";
					} catch (Services_Twilio_RestException $e) {
						//echo $e->getMessage();exit;
					}
		
                } else {
                    $response['mentorInvitation']['Error'] = 1;
                    $response['mentorInvitation']['Message'] = 'Failed to send E-mail.';
                }
				$response['mentorInvitation']['Error'] = 0;
                $response['mentorInvitation']['Message'] = 'Invitation sent successfully';
                $response['mentorInvitation']['data'] = $results[0];
            } else if (isset($results[0]->Message) && $results[0]->Message != "") {
                $response['mentorInvitation']['Error'] = 2;
                $response['mentorInvitation']['Message'] = $results[0]->Message;
            } else {
                $response['mentorInvitation']['Error'] = 1;
                $response['mentorInvitation']['Message'] = 'Something is not right here try again later!' .
			' (__FILE__@__LINE__ in __FUNCTION__)';
            }
        }
        echo json_encode($response);
        exit();
    }
    
    function menteeInvitation($data) {
        $response = array();
        $this->load->library('email');
        if ($data->MentorID == '') {
            $response['menteeInvitation']['Error'] = 2;
            $response['menteeInvitation']['Message'] = 'Mentor not found';
        } else if ($data->MenteeID == '') {
            $response['menteeInvitation']['Error'] = 2;
            $response['menteeInvitation']['Message'] = 'Mentee not found';
        } else if ($data->TopicID == '') {
            $response['menteeInvitation']['Error'] = 2;
            $response['menteeInvitation']['Message'] = 'Topic not found';
        } else if ($data->MenteeComments == '') {
            $response['menteeInvitation']['Error'] = 2;
            $response['menteeInvitation']['Message'] = 'Mentee comments not found';
        } else if ($data->MeetingDateTime == '') {
            $response['menteeInvitation']['Error'] = 2;
            $response['menteeInvitation']['Message'] = 'Meeting Date and Time not found';
        } else {
            //print_r($data);exit;
			$config = $this->mentor_model->getConfig();
			
			if($config[0]->expiryTime == 0 || $config[0]->expiryTime == "") $expiryTime = 1;
			else $expiryTime	=	$config[0]->expiryTime;
			$day = (int) ($expiryTime/24);
            $hours = $expiryTime%24;
			$dayText = '';
			$hourText = '';
			if(!empty($day)){ 
				$dayText = $day . (($day < 2) ? " day": " days");
			}
			
			if(!empty($hours)){
				$hourText = $hours . (($hours < 2) ? " hour": " hours");
			}
			$expiryTime	=	$dayText . " " .$hourText;
							
            $results = $this->mentor_model->mentorInvitationDetail($data->MentorID, $data->MenteeID, $data->TopicID, $data->MenteeComments, $data->MenteeEmail, $data->MenteePhone, $data->SmsCapable);
            if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "") {

                $response = array();

                $results1 = $this->mentor_model->checkInvitationDetail($data->MentorID, $data->MenteeID);
                //print_r($results1);exit;
                If (isset($results1[0]->InvitationID) && $results1[0]->InvitationID > 0) {
                    // SEND EMAIL TO CUSTOMER 
/* XXXXXXXXXXXXX Use of "magic" numbers, like 5, is almost unforgivable.
   XXXXXXXXXXXXX If you are using absolute IDs, use DEFINEs that explain
   XXXXXXXXXXXXX the dependency. For example: EMAIL_MEETING_REQUEST_ABSOLUTE_ID
*/
                    $emailtemplate = $this->emailtemplate_model->get_emailtempate_details(EMAIL_MEETING_REQUEST_ABSOLUTE_ID);
/* XXXXXXXXXXXXX */
                    //print_r($emailtemplate);exit;
                    if ($emailtemplate) {
                        $emailData = array();

                        $emailData['toEmail'] = $results1[0]->MentorEmail;
                        $emailData['cc'] = $results1[0]->MenteeEmail;
                        $arrSearch = array('##FIRST_NAME##', "##EMAIL##","##EXPIRYDATE##");
                        $arrReplace = array($results1[0]->MentorName,$results1[0]->MentorEmail,$expiryTime);

                        $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                        $emailData['subject'] = $subject;


                        $body = $results1[0]->MenteeName . " has asked for a meeting to discuss " . $results1[0]->TopicDescription . " related issues with you. Please use your MELS App to accept or decline the invitation in the next ".$expiryTime.", otherwise the invitation will expire.";
                        //$body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                        $emailData['body'] = $body;


                        send_mailInvitation($emailData);

                        //$results123 = $this->mentor_model->addinviteList($data->MentorID, $data->MenteeID);
                    }
					
					
					/*send message */
					$this->load->library('twilio');
					$newTest	=	$this->twilio->load();
					//print_r($newTest);exit;
					try{
						$message = $newTest->account->messages->create(array(
							"From"	=>	TWILIO_FROM_NUMBER,
							//"To"	=>	"+918128365306",
							"To"	=>	$results1[0]->MentorPhone,
							"Body"	=>	$results1[0]->MenteeName . " has asked for a meeting to discuss " . $results1[0]->TopicDescription . " related issues with you. Please use your MELS App to accept or decline the invitation in the next ".$expiryTime.", otherwise the invitation will expire.",
						));
					//	echo "Sent message {$message->sid}";
					} catch (Services_Twilio_RestException $e) {
						//echo $e->getMessage();exit;
					}
		
                } else {
                    $response['menteeInvitation']['Error'] = 1;
                    $response['menteeInvitation']['Message'] = 'Failed to send E-mail.';
                }
				$response['menteeInvitation']['Error'] = 0;
                $response['menteeInvitation']['Message'] = 'Invitation sent successfully';
                $response['menteeInvitation']['data'] = $results[0];
            } else if (isset($results[0]->Message) && $results[0]->Message != "") {
                $response['menteeInvitation']['Error'] = 2;
                $response['menteeInvitation']['Message'] = $results[0]->Message;
            } else {
                $response['menteeInvitation']['Error'] = 1;
                $response['menteeInvitation']['Message'] = 'Something is not right here try again later!' .
			' (__FILE__@__LINE__ in __FUNCTION__)';
            }
        }
        echo json_encode($response);
        exit();
    }

    function getMeetingHistory($data) {
        //error_reporting(E_ALL);
        //ini_set("display_errors","ON");
        $response = array();
        if ($data->UserId == '') {
            $response['getMeetingHistory']['Error'] = 2;
            $response['getMeetingHistory']['Message'] = 'User not found';
        } else if ($data->UserType == '') {
            $response['getMeetingHistory']['Error'] = 2;
            $response['getMeetingHistory']['Message'] = 'User Type not found';
        } else if ($data->CurrentPage == '') {
            $response['getMeetingHistory']['Error'] = 2;
            $response['getMeetingHistory']['Message'] = 'Current Page not found';
        } else if ($data->PageSize == '') {
            $response['getMeetingHistory']['Error'] = 2;
            $response['getMeetingHistory']['Message'] = 'Page Size not found';
        } else {

            $results = $this->meetingtype_model->getMeetingHistoryDetail($data->UserId, $data->UserType, $data->CurrentPage, $data->PageSize);
            $resultPending = $this->meetingtype_model->getPendingDetail($data->UserId);
            $resultPendingMeeting = $this->meetingtype_model->getMentorPendingMeetingCount($data->UserId);
            //print_r($resultPending);exit;            
            if (empty($results)) {
                $response['getMeetingHistory']['Error'] = 1;
                $response['getMeetingHistory']['Message'] = 'No Meeting History Found!	';
                $response['getMeetingHistory']['pending'] = $resultPending[0]->sum1;
                $response['getMeetingHistory']['pendingMeeting'] = (string)$resultPendingMeeting;
            } else {

                if (isset($results[0]->MeetingID) && $results[0]->MeetingID != "") {
                    foreach($results as &$eachResult) {
                        $eachResult->average = $eachResult->SatisfactionIndex;
                        if($eachResult->average == 0 || $eachResult->average == '') {
                            $eachResult->average = 0;
                            $eachResult->averageText = 'Not Rated';
                        } else if($eachResult->average == 1) {
                            $eachResult->averageText = 'Very Poor';
                        } else if($eachResult->average == 2) {
                            $eachResult->averageText = 'Poor';
                        } else if($eachResult->average == 3) {
                            $eachResult->averageText = 'Average';
                        } else if($eachResult->average == 4) {
                            $eachResult->averageText = 'Good';
                        } else if($eachResult->average == 5) {
                            $eachResult->averageText = 'Very Good';
                        }
                    }
                    $response['getMeetingHistory']['Error'] = 0;
                    $response['getMeetingHistory']['Message'] = 'Meeting history found successfully';
                    $response['getMeetingHistory']['data'] = $results;
                    $response['getMeetingHistory']['pending'] = $resultPending[0]->sum1;
                    $response['getMeetingHistory']['pendingMeeting'] = $resultPendingMeeting;
                } else if (isset($results[0]->Message) && $results[0]->Message != "") {
                    $response['getMeetingHistory']['Error'] = 2;
                    $response['getMeetingHistory']['Message'] = $results[0]->Message;
                    $response['getMeetingHistory']['pending'] = $resultPending[0]->sum1;
                    $response['getMeetingHistory']['pendingMeeting'] = (string)$resultPendingMeeting;
                } else {
                    $response['getMeetingHistory']['Error'] = 1;
                    $response['getMeetingHistory']['Message'] = 'Something is not right here try again later!' .
				' (__FILE__@__LINE__ in __FUNCTION__)';
                    $response['getMeetingHistory']['pending'] = $resultPending[0]->sum1;
                    $response['getMeetingHistory']['pendingMeeting'] = (string)$resultPendingMeeting;
                
                }
            }
        }
        echo json_encode($response);
        exit();
    }

    function addSkill($data) {
        $response = array();
        if ($data->UserId == '') {
            $response['addSkill']['Error'] = 2;
            $response['addSkill']['Message'] = 'User not found';
        } else if ($data->UserType == '') {
            $response['addSkill']['Error'] = 2;
            $response['addSkill']['Message'] = 'UserType not found';
        } else if ($data->UserName == '') {
            $response['addSkill']['Error'] = 2;
            $response['addSkill']['Message'] = 'UserName not found';
        } /*else if ($data->PhoneNumber == '') {
            $response['addSkill']['Error'] = 2;
            $response['addSkill']['Message'] = 'PhoneNumber not found';
        } else if ($data->Pass == '') {
            $response['addSkill']['Error'] = 2;
            $response['addSkill']['Message'] = 'Pass not found';
        }*/ else if ($data->Email == '') {
            $response['addSkill']['Error'] = 2;
            $response['addSkill']['Message'] = 'Email not found';
        } else if ($data->SkillID == '') {
            $response['addSkill']['Error'] = 2;
            $response['addSkill']['Message'] = 'Skill not found';
        } else {
			if(isset($data->Pass)) $Pass=	$data->Pass;else $Pass="";
            $profileUpdateResult = $this->mentor_model->editProfileDetail($data->UserId, $data->UserType, $data->UserName, $data->PhoneNumber, $Pass, $data->Email,$data->DisableSMS,$data->SourceID);
            if (isset($profileUpdateResult[0]->Id) && $profileUpdateResult[0]->Id != "") {
                $userData = $this->mentor_model->getProfileDetail($data->UserId, $data->UserType);
                $emailtemplate = $this->emailtemplate_model->get_emailtempate_details(EMAIL_PROFILE_UPDATE_ABSOLUTE_ID);
                if ($emailtemplate) {
                    $emailData = array();
                    $emailData['toEmail'] = $userData['Email'];

                    $arrSearch = array('##FIRST_NAME##', '##EMAIL_ADDRESS##', '##PASSWORD##', '##USER_TYPE##');
                    $arrReplace = array($userData['Name'], $userData['Email'], $userData['AccessToken'], $userData['UserType']);

                    $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                    $emailData['subject'] = $subject;

                    $body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                    $emailData['body'] = $body;
                    send_mail($emailData);
                }


                if ($data->UserType == 0) {
                    $this->db->where(array('MentorID' => $data->UserId));
                    $this->db->delete('mentorskillset');
                } else {
                    $this->db->where(array('MenteeID' => $data->UserId));
                    $this->db->delete('menteeskill');
                }

                $skillIds = explode(',', $data->SkillID);
                foreach ($skillIds as $val) {
                    $results = $this->skills_model->addSkillDetail($data->UserId, $data->UserType, $val);
                }

                if (isset($results[0]->Skills) && $results[0]->Skills != "") {
                    $response['addSkill']['Error'] = 0;
                    $response['addSkill']['Message'] = 'Mentee skill added successfully';
                    $profileUpdateResult[0]->Skills = $results[0]->Skills;
					$profileUpdateResult[0]->Password	= fnDecrypt($profileUpdateResult[0]->Password, $this->config->item('mentorKey'));
                    $response['addSkill']['data'] = $profileUpdateResult[0];
                    //$response['addSkill']['data2'] = $results[0];
                } else if (isset($results[0]->Message) && $results[0]->Message != "") {
                    $response['addSkill']['Error'] = 2;
                    $response['addSkill']['Message'] = $results[0]->Message;
                } else {
                    $response['addSkill']['Error'] = 1;
                    $response['addSkill']['Message'] = 'Error has occurred. Please try again later.';
                }
            } else if (isset($profileUpdateResult[0]->Message) && $profileUpdateResult[0]->Message != "") {
                $response['addSkill']['Error'] = 2;
                $response['addSkill']['Message'] = $profileUpdateResult[0]->Message;
            } else {
                $response['addSkill']['Error'] = 1;
            }
        }
        echo json_encode($response);
        exit();
    }

    function mentorComment($data) {
        $response = array();
        $SUBTOPICID = array();
        if ($data->MentorID == '') {
            $response['mentorComment']['Error'] = 2;
            $response['mentorComment']['Message'] = 'Mentor not found';
        } else if ($data->MenteeID == '') {
            $response['mentorComment']['Error'] = 2;
            $response['mentorComment']['Message'] = 'Mentee not found';
        } else if ($data->MeetingID == '') {
            $response['mentorComment']['Error'] = 2;
            $response['mentorComment']['Message'] = 'Meeting not found';
        } else if ($data->SatisfactionIndex == '') {
            $response['mentorComment']['Error'] = 2;
            $response['mentorComment']['Message'] = 'Satisfaction rating missing';
        } else {

            $results = $this->mentor_model->updateMentorCommentDetail($data);
            
            if ($data->MeetingID > 0) {
                $this->db->where(array('MentorID' => $data->MentorID, 'MeetingID' => $data->MeetingID));
                $this->db->delete('mentoractionstaken');

                $this->db->where(array('MentorID' => $data->MentorID, 'MeetingID' => $data->MeetingID));
                $this->db->delete('mentor_subTopicsIDs');
            }
            
            if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "" && isset($data->MentorActionIDs) && $data->MentorActionIDs != "") {
                $mentorids = explode(',', $data->MentorActionIDs);
                foreach ($mentorids as $val) {
                    $mentorResults = $this->mentor_model->addMentoractionstakenDetail($data->MentorID, $results[0]->InsertedId, $val);
                }
            }

            if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "" && isset($data->MeetingSubTopicID) && $data->MeetingSubTopicID != "") {
                $mentorids = explode(',', $data->MeetingSubTopicID);
                foreach ($mentorids as $val) {
                    $mentorResults = $this->mentor_model->addMentorsubtopicsIDs($data->MentorID, $results[0]->InsertedId, $val);
                }
                $mentorSubIdResults = $this->mentor_model->getMentorsubtopicsIDs($results[0]->InsertedId);

                foreach ($mentorSubIdResults as $val) {
                    $ID = $val->MentorSubTopicsIDsId;
                    array_push($SUBTOPICID, $ID);
                }

                $mentorSubIdResultsIds = join(',', $SUBTOPICID);
                $addMentorSubId = $this->mentor_model->updateMentorsubtopicsIDs($mentorSubIdResultsIds, $results[0]->InsertedId);
            }
            
            
            if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "") {
                $response['mentorComment']['Error'] = 0;
                $response['mentorComment']['Message'] = 'Mentor review form saved';
                $response['mentorComment']['data'] = $results[0];
            } else if (isset($results[0]->Message) && $results[0]->Message != "") {
                $response['mentorComment']['Error'] = 2;
                $response['mentorComment']['Message'] = $results[0]->Message;
            } else {
                $response['mentorComment']['Error'] = 1;
                $response['mentorComment']['Message'] = 'Something is not right here try again later!' .
			' (__FILE__@__LINE__ in __FUNCTION__)';
            }
        }
        echo json_encode($response);
        exit();
    }

    function menteeComment($data) {
        $response = array();
        if ($data->MentorID == '') {
            $response['menteeComment']['Error'] = 2;
            $response['menteeComment']['Message'] = 'Mentor not found';
        } else if ($data->MenteeID == '') {
            $response['menteeComment']['Error'] = 2;
            $response['menteeComment']['Message'] = 'Mentee not found';
        } else if ($data->MeetingID == '') {
            $response['menteeComment']['Error'] = 2;
            $response['menteeComment']['Message'] = 'Meeting not found';
        } else if ($data->CareC == '') {
            $response['menteeComment']['Error'] = 2;
            $response['menteeComment']['Message'] = 'Satisfaction rating for CLEAR is missing';
        } else if ($data->CareA == '') {
            $response['menteeComment']['Error'] = 2;
            $response['menteeComment']['Message'] = 'Satisfaction rating for APPLICABLE is missing';
        } else if ($data->CareR == '') {
            $response['menteeComment']['Error'] = 2;
            $response['menteeComment']['Message'] = 'Satisfaction rating for RELEVANT is missing';
        } else if ($data->CareE == '') {
            $response['menteeComment']['Error'] = 2;
            $response['menteeComment']['Message'] = 'Satisfaction rating for EXECUTABLE is missing';
        } else {

            $results = $this->mentee_model->menteeCommentDetail($data->MentorID, $data->MenteeID, $data->MeetingID, $data->MenteeComment, $data->CareC, $data->CareA, $data->CareR, $data->CareE);
        
            if ($data->MeetingID > 0) {
                $this->db->where(array('MenteeID' => $data->MenteeID, 'MeetingID' => $data->MeetingID));
                $this->db->delete('menteeactionstaken');
            }
            
            $Actionids = explode(',', $data->MenteeActionItemID);
            foreach ($Actionids as $val) {
                $menteeResults = $this->mentor_model->updateMenteeactionsItemsWithDoneDetail($data->MenteeID, $data->MeetingID, $val);
            }
            $this->mentor_model->updateMeetingMenteeactionsItemsWithDoneDetail($data->MeetingID, $data->MenteeActionItemIDDone);
            
            
            if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "") {
                $response['menteeComment']['Error'] = 0;
                $response['menteeComment']['Message'] = 'Mentee Review Form saved';
                $response['menteeComment']['data'] = $results[0];
            } else if (isset($results[0]->Message) && $results[0]->Message != "") {
                $response['menteeComment']['Error'] = 2;
                $response['menteeComment']['Message'] = $results[0]->Message;
            } else {
                $response['menteeComment']['Error'] = 1;
                $response['menteeComment']['Message'] = 'Something is not right here try again later!' .
			' (__FILE__@__LINE__ in __FUNCTION__)';
            }
        }
        echo json_encode($response);
        exit();
    }

    function submitMeetingInfo($data) {
        $response = array();
        $SUBTOPICID = array();
        if ($data->MeetingID == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Meeting not found';
        } else if ($data->MentorID == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Mentor not found';
        } else if ($data->MenteeID == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Mentee not found';
        } else if ($data->MeetingStartDatetime == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Meeting Start Date time not found';
        } else if ($data->MeetingEndDatetime == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Meeting End Date time not found';
        } else if ($data->MeetingTypeID == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Meeting Type not found';
        } else if ($data->MeetingTopicID == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Meeting Topic not found';
        } else if ($data->MeetingPlaceID == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Meeting Place not found';
        } else {

            /* khushbu 20-8 */
            $results2 = $this->mentor_model->getMeetingSessionIDforSubmit($data->MenteeID, $data->MentorID, $data->MeetingID);
            //print_r($results2);exit;
            if (!empty($results2)) {
                if ($results2[0]->Meeting_status == 2 || $results2[0]->Meeting_status == 3 || $results2[0]->Meeting_status == 1) {
                    $MeetingID = $results2[0]->MeetingID;

                    $results = $this->mentor_model->submitMeetingInfoDetail($MeetingID, $data->MentorID, $data->MenteeID, $data->MeetingStartDatetime, $data->MeetingEndDatetime, $data->MeetingTypeID, $data->MeetingTopicID, $data->MeetingSubTopicID, $data->MeetingPlaceID, $data->MenteeActionIDs, $data->MeetingElapsedTime, $data->MeetingFeedback, @$data->MentorActionItemDone, @$data->MenteeActionItemDone);

                    if ($data->MeetingID > 0) {
                        $this->db->where(array('MenteeID' => $data->MenteeID, 'MeetingID' => $MeetingID));
                        $this->db->delete('menteeactionstaken');

                        $this->db->where(array('MentorID' => $data->MentorID, 'MeetingID' => $MeetingID));
                        $this->db->delete('mentoractionstaken');

                        $this->db->where(array('MentorID' => $data->MentorID, 'MeetingID' => $MeetingID));
                        $this->db->delete('mentor_subTopicsIDs');
                    }

                    if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "" && isset($data->MenteeActionIDs) && $data->MenteeActionIDs != "") {
                        $menteeids = explode(',', $data->MenteeActionIDs);
                        foreach ($menteeids as $val) {
                            $menteeResults = $this->mentor_model->addMenteeactionstakenDetail($data->MenteeID, $results[0]->InsertedId, $val);
                        }
                    }
                    if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "" && isset($data->MentorActionIDs) && $data->MentorActionIDs != "") {
                        $mentorids = explode(',', $data->MentorActionIDs);
                        foreach ($mentorids as $val) {
                            $mentorResults = $this->mentor_model->addMentoractionstakenDetail($data->MentorID, $results[0]->InsertedId, $val);
                        }
                    }
					
					if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "" && isset($data->MentorComment) && $data->MentorComment != "") {
                        $MentorCommentResults = $this->mentor_model->mentorCommentDetail($data->MentorID, $data->MenteeID, $results[0]->InsertedId, $data->MentorComment, $SatisfactionIndex =0);
                    }

                    if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "" && isset($data->MeetingSubTopicID) && $data->MeetingSubTopicID != "") {
                        $mentorids = explode(',', $data->MeetingSubTopicID);
                        foreach ($mentorids as $val) {
                            $mentorResults = $this->mentor_model->addMentorsubtopicsIDs($data->MentorID, $results[0]->InsertedId, $val);
                        }
                        $mentorSubIdResults = $this->mentor_model->getMentorsubtopicsIDs($results[0]->InsertedId);
                        //print_r($mentorSubIdResults);exit;


                        foreach ($mentorSubIdResults as $val) {
                            $ID = $val->MentorSubTopicsIDsId;
                            array_push($SUBTOPICID, $ID);
                        }
                        //echo "<pre>";print_r($SUBTOPICID);
                        $mentorSubIdResultsIds = join(',', $SUBTOPICID);
                        $addMentorSubId = $this->mentor_model->updateMentorsubtopicsIDs($mentorSubIdResultsIds, $results[0]->InsertedId);
                    } else {
                        $response['submitMeetingInfo']['Error'] = 1;
                        $response['submitMeetingInfo']['Message'] = 'No Data Found';
                    }
                }

                /* added by khushbu 18-8-15 */
            }
            //$results = $this->mentor_model->submitMeetingInfoDetail($data->MeetingID, $data->MentorID, $data->MenteeID, $data->MeetingStartDatetime, $data->MeetingEndDatetime, $data->MeetingTypeID, $data->MeetingTopicID, $data->MeetingSubTopicID, $data->MeetingPlaceID, $data->MenteeActionIDs, $data->MeetingElapsedTime, $data->MeetingFeedback, @$data->MentorActionItemDone, @$data->MenteeActionItemDone);

            /* 20-8 */


            /* added by khushbu 18-8-15 */


            if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "") {
                $response['submitMeetingInfo']['Error'] = 0;
                $response['submitMeetingInfo']['Message'] = 'Meeting info added successfully';
                $response['submitMeetingInfo']['data'] = $results[0];
            } else
            if (isset($results[0]->Message) && $results[0]->Message != "") {
                $response['submitMeetingInfo']['Error'] = 2;
                $response['submitMeetingInfo']['Message'] = $results[0]->Message;
            } else {
                $response['submitMeetingInfo']['Error'] = 1;
                $response['submitMeetingInfo']['Message'] = 'Something is not right here try again later!' .
			' (__FILE__@__LINE__ in __FUNCTION__)';
            }
        }
        echo json_encode($response);
        exit();
    }

    function getTopic() {
        $response = array();
        $results = $this->topic_model->getTopicDetail();
        if (isset($results[0]->TopicID) && $results[0]->TopicID != "") {
            $response['getTopic']['Error'] = 0;
            $response['getTopic']['Message'] = "Topic details found.";
            $response['getTopic']['data'] = $results;
        } else {
            $response['getTopic']['Error'] = 2;
            $response['getTopic']['Message'] = "Topic details not found.";
        }
        echo json_encode($response);
        exit();
    }
    
    function getSourceList($data) {
        $response = array();
        $results = $this->mentor_model->getSourceDetail($data->usertype);
        
        if (isset($results[0]->SourceID) && $results[0]->SourceID != "") {
            $response['getSourceList']['Error'] = 0;
            $response['getSourceList']['Message'] = "Source Names found.";
            $response['getSourceList']['data'] = $results;
        } else {
            $response['getSourceList']['Error'] = 2;
            $response['getSourceList']['Message'] = "Source Names not found.";
        }
        echo json_encode($response);
        exit();
    }

    function getMentor($data) {
        $response = array();
        if ($data->MenteeID == '') {
            $response['getMentor']['Error'] = 2;
            $response['getMentor']['Message'] = 'Mentee not found';
        } else {
            $results = $this->mentor_model->getMentorDetail($data->MenteeID);
            if (isset($results[0]->MentorID) && $results[0]->MentorID != "") {
                $response['getMentor']['Error'] = 0;
                $response['getMentor']['Message'] = "Mentor details found.";
                $response['getMentor']['data'] = $results;
            } else {
                $response['getMentor']['Error'] = 2;
                $response['getMentor']['Message'] = "Mentor details not found.";
            }
        }

        echo json_encode($response);
        exit();
    }

    function getSubTopic() {
        $response = array();
        $results = $this->topic_model->getSubTopicDetail();
        if (isset($results[0]->TopicID) && $results[0]->TopicID != "") {
            $response['getSubTopic']['Error'] = 0;
            $response['getSubTopic']['Message'] = "Sub Topic details found.";
            $response['getSubTopic']['data'] = $results;
        } else {
            $response['getSubTopic']['Error'] = 2;
            $response['getSubTopic']['Message'] = "Sub Topic details not found.";
        }
        echo json_encode($response);
        exit();
    }

    function getMentee($data) {
        $response = array();

        if ($data->MentorID == '') {
            $response['getMentee']['Error'] = 2;
            $response['getMentee']['Message'] = 'Mentor not found';
        } else {
            $results = $this->mentee_model->getMenteeDetail($data->MentorID);
            if (isset($results[0]->MenteeID) && $results[0]->MenteeID != "") {
                $response['getMentee']['Error'] = 0;
                $response['getMentee']['Message'] = "Mentee details found.";
                $response['getMentee']['data'] = $results;
            } else {
                $response['getMentee']['Error'] = 2;
                $response['getMentee']['Message'] = "Mentee details not found.";
            }
        }

        echo json_encode($response);
        exit();
    }

    function getMeetingType() {
        $response = array();
        $results = $this->meetingtype_model->getMeetingTypeDetail();
        if (isset($results[0]->MeetingTypeID) && $results[0]->MeetingTypeID != "") {
            $response['getMeetingType']['Error'] = 0;
            $response['getMeetingType']['Message'] = "Meeting type details found.";
            $response['getMeetingType']['data'] = $results;
        } else {
            $response['getMeetingType']['Error'] = 2;
            $response['getMeetingType']['Message'] = "Meeting type details not found.";
        }
        echo json_encode($response);
        exit();
    }
    
    function getMeetingPlace() {
        $response = array();
        $results = $this->meetingtype_model->getMeetingPlaceDetail();
        if (isset($results[0]->MeetingPlaceID) && $results[0]->MeetingPlaceID != "") {
            $response['getMeetingPlace']['Error'] = 0;
            $response['getMeetingPlace']['Message'] = "Meeting place details found.";
            $response['getMeetingPlace']['data'] = $results;
        } else {
            $response['getMeetingPlace']['Error'] = 2;
            $response['getMeetingPlace']['Message'] = "Meeting place details not found.";
        }
        echo json_encode($response);
        exit();
    }

    function getSkills() {
        $response = array();
        $results = $this->skills_model->getSkillsDetail();
        if (isset($results[0]->SkillID) && $results[0]->SkillID != "") {
            $response['getSkills']['Error'] = 0;
            $response['getSkills']['Message'] = "skill details found.";
            $response['getSkills']['data'] = $results;
        } else {
            $response['getSkills']['Error'] = 2;
            $response['getSkills']['Message'] = "skill details not found.";
        }
        echo json_encode($response);
        exit();
    }

    function getMentorActionTaken() {
        $response = array();
        $results = $this->mentor_model->getMentorActionTakenDetail();
        if (isset($results[0]->MentorActionID) && $results[0]->MentorActionID != "") {
            $response['getMentorActionTaken']['Error'] = 0;
            $response['getMentorActionTaken']['Message'] = "Mentor defined actions found.";
            $response['getMentorActionTaken']['data'] = $results;
        } else {
            $response['getMentorActionTaken']['Error'] = 2;
            $response['getMentorActionTaken']['Message'] = "Mentor defined actions not found.";
        }
        echo json_encode($response);
        exit();
    }

    function getMenteeActionTaken() {
        $response = array();
        $results = $this->mentee_model->getMenteeActionTakenDetail();
        if (isset($results[0]->MenteeActionID) && $results[0]->MenteeActionID != "") {
            $response['getMenteeActionTaken']['Error'] = 0;
            $response['getMenteeActionTaken']['Message'] = "Mentee actiontaken details found.";
            $response['getMenteeActionTaken']['data'] = $results;
        } else {
            $response['getMenteeActionTaken']['Error'] = 2;
            $response['getMenteeActionTaken']['Message'] = "Mentee actiontaken details not found.";
        }
        echo json_encode($response);
        exit();
    }

    function checkLogin($data) {
        $response = array();
        /* if ($data->Email == '') {
          $response['checkLogin']['Error'] = 2;
          $response['checkLogin']['Message'] = 'Email not found';
          } */
        if ($data->UserName == '') {
            $response['checkLogin']['Error'] = 2;
            $response['checkLogin']['Message'] = 'User Name not found';
        } else if ($data->Password == '') {
            $response['checkLogin']['Error'] = 2;
            $response['checkLogin']['Message'] = 'Password not found';
        } else {

            $results = $this->mentor_model->checkLoginDetail($data->UserName, $data->Password);
            //print_r($results);exit;
            if (isset($results[0]->Id) && $results[0]->Id != "") {
                $response['checkLogin']['Error'] = 0;
                $response['checkLogin']['Message'] = 'Login successfully';
				$results[0]->Password	=	fnDecrypt($results[0]->Password, $this->config->item('mentorKey'));
                $response['checkLogin']['data'] = $results[0];
            } else if (isset($results[0]->Message) && $results[0]->Message != "") {
                $response['checkLogin']['Error'] = 2;
                $response['checkLogin']['Message'] = $results[0]->Message;
            } else {
                $response['checkLogin']['Error'] = 1;
                $response['checkLogin']['Message'] = 'Something is not right here try again later!' .
			' (__FILE__@__LINE__ in __FUNCTION__)';
            }
        }
        echo json_encode($response);
        exit();
    }

    function registerUser($data) {
        $response = array();
        if ($data->Email == '') {
            $response['registerUser']['Error'] = 2;
            $response['registerUser']['Message'] = 'Email not found';
        } else if ($data->AccessToken == '') {
            $response['registerUser']['Error'] = 2;
            $response['registerUser']['Message'] = 'Access Token not found';
        } else if ($data->UserType == '') {
            $response['registerUser']['Error'] = 2;
            $response['registerUser']['Message'] = 'User Type not found';
        } else {

            $results = $this->mentor_model->registerUserDetail($data->Email, $data->AccessToken, $data->UserType);

            if (isset($results[0]->Id) && $results[0]->Id != "") {
                $response['registerUser']['Error'] = 0;
                $response['registerUser']['Message'] = 'Registered successfully';
                $response['registerUser']['data'] = $results[0];
                // SEND EMAIL TO CUSTOMER

                $emailtemplate = $this->emailtemplate_model->get_emailtempate_details(EMAIL_LOGIN_INFO_ABSOLUTE_ID);
                if ($emailtemplate) {
                    $emailData = array();
                    if ($data->UserType == "0") { # FOR MENTOR
                        $emailData['toEmail'] = $results[0]->MentorEmail;

                        $arrSearch = array('##FIRST_NAME##', '##EMAIL_ADDRESS##', '##PASSWORD##', '##USER_TYPE##');
                        $arrReplace = array($results[0]->MentorName, $results[0]->MentorEmail, $results[0]->AccessToken, 'Mentor');
                    } else if ($data->UserType == "1") { # FOR MENTEE
                        $emailData['toEmail'] = $results[0]->MenteeEmail;

                        $arrSearch = array('##FIRST_NAME##', '##EMAIL_ADDRESS##', '##PASSWORD##', '##USER_TYPE##');
                        $arrReplace = array($results[0]->MenteeName, $results[0]->MenteeEmail, $results[0]->AccessToken, 'Mentee');
                    }

                    $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                    $emailData['subject'] = $subject;

                    $body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                    $emailData['body'] = $body;

                    send_mail($emailData);
                }

                // END SEND EMAIL TO CUSTOMER
            } else if (isset($results[0]->Message) && $results[0]->Message != "") {
                $response['registerUser']['Error'] = 2;
                $response['registerUser']['Message'] = $results[0]->Message;
            } else {
                $response['registerUser']['Error'] = 1;
                $response['registerUser']['Message'] = 'Something is not right here try again later!' .
			' (__FILE__@__LINE__ in __FUNCTION__)';
            }
        }
        echo json_encode($response);
        exit();
    }

    function forgotPassword($data) {
        $response = array();
        if ($data->Email == '') {
            $response['forgotPassword']['Error'] = 2;
            $response['forgotPassword']['Message'] = 'Email not found';
        } else {
            $results = $this->mentor_model->forgotPasswordDetail($data->Email);
            If (isset($results[0]->Id) && $results[0]->Id > 0) {
                // SEND EMAIL TO CUSTOMER 
                $emailtemplate = $this->emailtemplate_model->get_emailtempate_details(EMAIL_CHANGE_PASSWORD_ABSOLUTE_ID);
                if ($emailtemplate) {
                    $emailData = array();
					$mentorId = $this->mentor_model->getMentorId($results[0]->UserType,$data->Email);
					//echo $results[0]->UserType ." ". $data->Email;
				//	print_r($mentorId);exit;
					$UserId=	($results[0]->UserType == 0)? $mentorId[0]->MentorID : $mentorId[0]->MenteeId;
					if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on"){$ssl_set = "s";} else{$ssl_set = "";}
					$base_url = 'http'.$ssl_set.'://'.$_SERVER['HTTP_HOST']."/mentor/";
					$link	=	$base_url .'webservices/index.php/mentorService/resetPassword/'. $UserId .'/'.$results[0]->UserType;
					$resetLink =	"<a href='$link'>Reset Password</a>";
                    if ($results[0]->UserType == '0') {	
                        $emailData['toEmail'] = $results[0]->MentorEmail;
                        $arrSearch = array('##FIRST_NAME##', "##EMAIL##", "##PASSWORD##","##RESETLINK##");
                        $arrReplace = array(ucwords($results[0]->MentorName), $results[0]->MentorName, $results[0]->Password,$resetLink);
                    } else if ($results[0]->UserType == '1') {
                        $emailData['toEmail'] = $results[0]->MenteeEmail;
                        $arrSearch = array('##FIRST_NAME##', "##EMAIL##", "##PASSWORD##","##RESETLINK##");
                        $arrReplace = array(ucwords($results[0]->MenteeName), $results[0]->MenteeName, $results[0]->Password,$resetLink);
                    }

                    $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                    $emailData['subject'] = $subject;

                    $body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                    $emailData['body'] = $body;

					//var_dump($emailData);
                    send_mail($emailData);
                }
                // END SEND EMAIL TO CUSTOMER 
                $response['forgotPassword']['Error'] = 0;
                $response['forgotPassword']['Message'] = 'Password reset instructions have been emailed to the address you gave.';
            } else if (isset($results[0]->Message) && $results[0]->Message != "") {
                $response['forgotPassword']['Error'] = 2;
                $response['forgotPassword']['Message'] = $results[0]->Message;
            } else {
                $response['forgotPassword']['Error'] = 1;
                $response['forgotPassword']['Message'] = 'Something is not right here try again later!' .
			' (__FILE__@__LINE__ in __FUNCTION__)';
            }
        }
        echo json_encode($response);
        exit();
    }

    function changePassword($data) {
        $response = array();
        if ($data->Email == '') {
            $response['changePassword']['Error'] = 2;
            $response['changePassword']['Message'] = 'Email not found';
        } else if ($data->NewPassword == '') {
            $response['changePassword']['Error'] = 2;
            $response['changePassword']['Message'] = 'New Password not found';
        } else if ($data->OldPassword == '') {
            $response['changePassword']['Error'] = 2;
            $response['changePassword']['Message'] = 'Old Password not found';
        } else if ($data->UserType == '') {
            $response['changePassword']['Error'] = 2;
            $response['changePassword']['Message'] = 'User Type not found';
        } else {
            $results = $this->mentor_model->changePasswordDetail($data->Email, $data->OldPassword, $data->NewPassword, $data->UserType);
            if ((isset($results[0]->MentorContactInfoID) && $results[0]->MentorContactInfoID > 0) || (isset($results[0]->MenteeContactID) && $results[0]->MenteeContactID > 0)) {

                // SEND EMAIL
                $emailtemplate = $this->emailtemplate_model->get_emailtempate_details(EMAIL_CHANGE_PASSWORD_ABSOLUTE_ID);
                if ($emailtemplate) {
                    $emailData = array();
                    if ($data->UserType == '0') {
                        $emailData['toEmail'] = $results[0]->MentorEmail;

                        $arrSearch = array('##FIRST_NAME##', "##EMAIL##", "##PASSWORD##");
                        $arrReplace = array(ucwords($results[0]->MentorName), $results[0]->MentorEmail, fnDecrypt($results[0]->Password, $this->config->item('mentorKey')));
                    } else if ($data->UserType == '1') {
                        $emailData['toEmail'] = $results[0]->MenteeEmail;

                        $arrSearch = array('##FIRST_NAME##', "##EMAIL##", "##PASSWORD##");
                        $arrReplace = array(ucwords($results[0]->MenteeName), $results[0]->MenteeEmail, fnDecrypt($results[0]->Password, $this->config->item('mentorKey')));
                    }
                    $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                    $emailData['subject'] = $subject;

                    $body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                    $emailData['body'] = $body;


                    send_mail($emailData);
                }
                // END SEND EMAIL

                $response['changePassword']['Error'] = 0;
                $response['changePassword']['Message'] = 'Password changed successfully.';
            } else {
                $response['changePassword']['Error'] = 1;
                $response['changePassword']['Message'] = 'Something is not right here try again later!' .
			' (__FILE__@__LINE__ in __FUNCTION__)';
            }
        }
        echo json_encode($response);
        exit();
    }

    function upload($application_code) {
        /* $file_path = "/var/www/html/medialytix/uploads/abtest/ab_test_temp/";

          $file_path = $file_path . basename( $_FILES['uploaded_file']['name']);
          if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $file_path))
          {
          echo "success";
          }
          else
          {
          echo "fail to upload.. no files provided";
          } */
//$absolute_path.'/uploads/abtest/default_ab_test_parameters/'.$data->vApplicationCode.'/'. $experiment_parameter['iABTestParameterValue']

        move_uploaded_file($_FILES["file"]["tmp_name"], "/var/www/html/medialytix/uploads/abtest/default_ab_test_parameters/" . $application_code . '/' . $_FILES["file"]["name"]);
        exit;
        if ((($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/pjpeg") || ($_FILES["file"]["type"] == "image/png")) && ($_FILES["file"]["size"] < 20000000000)) {
            if ($_FILES["file"]["error"] > 0) {
                echo "Return Code: " . $_FILES["file"]["error"] . "
     ";
            } else {
                echo "Upload: " . $_FILES["file"]["name"] . "
     ";
                echo "Type: " . $_FILES["file"]["type"] . "
     ";
                echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb
     ";
                echo "Temp file: " . $_FILES["file"]["tmp_name"] . "
     ";

                if (file_exists("/var/www/html/medialytix/uploads/abtest/ab_test_temp/" . $_FILES["file"]["name"])) {
                    echo $_FILES["file"]["name"] . " already exists. ";
                } else {
                    move_uploaded_file($_FILES["file"]["tmp_name"], "/var/www/html/medialytix/uploads/abtest/ab_test_temp/" . $_FILES["file"]["name"]);
                    echo "Stored in: " . "/var/www/html/medialytix/uploads/abtest/ab_test_temp/" . $_FILES["file"]["name"];
                }
            }
        } else {
            echo "Invalid file";
        }

        exit;
    }

    function escape_string($var) {
        if (is_array($var)) {
            foreach ($var as &$data) {
                if (is_array($data)) {
                    $data = $this->escape_string($data);
                } else {
                    $data = addslashes($data);
                }
            }
        } else {
            $var = addslashes($var);
        }
        return $var;
    }
    
    function checkvalidjson($json) {
        //echo "1st";print_r($json);
        $obj = json_decode(($json), TRUE);
        //echo "2nd";print_r($obj);
        $obj = $this->escape_string($obj);
        //echo "3rd";print_r($obj);exit;
        if (is_null($obj)) {
            $response['error'] = 1;
            $response['data'] = "Invalid Json format";
            echo json_encode($response);
            exit();
        } else {
            $data = json_encode($obj);
            $data = json_decode($data);
            return $data;
        }
    }

    function getCountry() {

        $country_list = $this->users_model->getCountry();
        $response['getCountry']['Error'] = 0;
        $response['getCountry']['data'] = $country_list;
        echo json_encode($response);
        exit();
    }

    function getStates() {

        $state_list = $this->users_model->getStates();
        $response['getStates']['Error'] = 0;
        $response['getStates']['data'] = $state_list;
        echo json_encode($response);
        exit();
    }

    /*
     *   @params : UserID 
     *   @params : UserType  0 = customer, 1 = vendor, 2 = employee  
     *   @params : PictureType  0 = profilepic, 1 = gallery
     *
     */

    function addUploadPics($data) {
        $response = array();
        if (isset($data['UserType']) && $data['UserType'] != "") {
            switch ($data['UserType']) {
                case '0':  // customer
                    if ($data['PictureType'] == 0) {  // 
                        if ($_FILES['ImageData']['error'] == 0) {
                            $pathMain = CUSTOMER_LOGO_PATH;
                            $pathThumb = CUSTOMER_LOGO_THUMB_PATH;
                            $imageNameTime = time();
                            $filename = $imageNameTime . "_" . $data['UserID'];
                            $result = do_upload("ImageData", $pathMain, $filename);

                            if ($result['status'] == 1) {

                                // RESIZE ORIGINAL IMAGE TO THUMB 150X150
                                $uploadedFileName = $result['upload_data']['file_name'];
                                resize_image($pathMain . $uploadedFileName, $pathThumb . $uploadedFileName, CUSTOMER_LOGO_THUMB_WIDTH, CUSTOMER_LOGO_THUMB_HEIGHT);

                                $this->db->where('iCustomerId', $data['UserID']);
                                $result = $this->db->update('customerdetails', array('iPictureURLId' => $uploadedFileName));

                                // RESIZE ORIGINAL IMAGE TO 500X500
                                resize_image($pathMain . $uploadedFileName, $pathMain . $uploadedFileName, CUSTOMER_LOGO_WIDTH, CUSTOMER_LOGO_HEIGHT);

                                $results = $this->customer_model->getCustomerDetail($data['UserID']);

                                $response['addUploadPics']['Error'] = 0;
                                $response['addUploadPics']['Message'] = 'Profile Pic Uploaded Successfully.';
                                $response['addUploadPics']['data'] = $results[0];
                            } else {
                                $response['addUploadPics']['Error'] = 1;
                                $response['addUploadPics']['Message'] = 'Something is wrong.' .
					' (__FILE__@__LINE__ in __FUNCTION__)';
                            }
                        } else {
                            $response['addUploadPics']['Error'] = 1;
                            $response['addUploadPics']['Message'] = 'Something is wrong.' .
					' (__FILE__@__LINE__ in __FUNCTION__)';
                        }
                    } elseif ($data->PictureType == 1) {

                        /*  if($_FILES['vLogoURL']['error'] == 0 ){
                          $pathMain  = CUSTOMER_LOGO_PATH;
                          $pathThumb = CUSTOMER_LOGO_THUMB_PATH;
                          $imageNameTime = time();
                          $filename = $imageNameTime."_".$id;
                          $result = do_upload("vLogoURL", $pathMain, $filename);

                          if ($result['status'] == 1) {

                          // RESIZE ORIGINAL IMAGE TO THUMB 150X150
                          $uploadedFileName = $result['upload_data']['file_name'];
                          resize_image($pathMain . $uploadedFileName, $pathThumb . $uploadedFileName, CUSTOMER_LOGO_THUMB_WIDTH, CUSTOMER_LOGO_THUMB_HEIGHT);
                          $this->db->where('iVendorId', $id);
                          $result = $this->db->update('vendordetails', array('vLogoURL' => $uploadedFileName ));

                          // RESIZE ORIGINAL IMAGE TO 500X500
                          resize_image($pathMain . $uploadedFileName, $pathMain . $uploadedFileName, CUSTOMER_LOGO_WIDTH, CUSTOMER_LOGO_HEIGHT);
                          }
                          } */
                    }

                    break;
                case '1':   // vendor 

                    /* $id = $data->UserID;  
                      if($_FILES['vLogoURL']['error'] == 0 ){
                      $pathMain  = VENDOR_LOGO_PATH;
                      $pathThumb = VENDOR_LOGO_THUMB_PATH;
                      $imageNameTime = time();
                      $filename = $imageNameTime."_".$id;
                      $result = do_upload("vLogoURL", $pathMain, $filename);

                      if ($result['status'] == 1) {

                      // RESIZE ORIGINAL IMAGE TO THUMB 150X150
                      $uploadedFileName = $result['upload_data']['file_name'];
                      resize_image($pathMain . $uploadedFileName, $pathThumb . $uploadedFileName, VENDOR_LOGO_THUMB_WIDTH, VENDOR_LOGO_THUMB_HEIGHT);
                      $this->db->where('iVendorId', $id);
                      $result = $this->db->update('vendordetails', array('vLogoURL' => $uploadedFileName ));

                      // RESIZE ORIGINAL IMAGE TO 500X500
                      resize_image($pathMain . $uploadedFileName, $pathMain . $uploadedFileName, VENDOR_LOGO_WIDTH, VENDOR_LOGO_HEIGHT);
                      }
                      } */
                    break;
                case '2':   // employee 
                    break;
                default:
                    break;
            }
        } else {
            $response['addUploadPics']['Error'] = 1;
            $response['addUploadPics']['Message'] = 'Something is wrong.' .
		' (__FILE__@__LINE__ in __FUNCTION__)';
        }
        echo json_encode($response);
        exit();
    }

    /* khushbu 19-8-15 */

    function getMentorForInvitation($data) {
        $response = array();
        if ($data->MenteeID == '') {
            $response['getMentorForInvitation']['Error'] = 2;
            $response['getMentorForInvitation']['Message'] = 'Mentee not found';
        } else {
            $resArray = array();
			$results = $this->mentor_model->getMentorWaitScreenStatus($data->MenteeID,'Pending');
			$response['getMentorForInvitation']['AcceptedCount'] = count($this->mentor_model->getMentorWaitScreenStatus($data->MenteeID,'Accepted'));
			$response['getMentorForInvitation']['DeclinedCount'] = count($this->mentor_model->getMentorWaitScreenStatus($data->MenteeID,'Rejected'));
			$response['getMentorForInvitation']['PendingCount'] = count($results);
			
            $results = $this->mentor_model->getMentorForInvitation();
            //print_r($results);exit;
            if (isset($results) && (!empty($results))) {
                foreach ($results as $val) {
                    $res = $this->mentor_model->getMentorForInvitationStatus($val->MentorID, $data->MenteeID);
                    if(isset($res[0])){
                        array_push($resArray, $res[0]);
                    }
                }
                if(isset($resArray) && !empty($resArray)){
                    $response['getMentorForInvitation']['Error'] = 0;
                    $response['getMentorForInvitation']['Message'] = 'Mantor details';
                    $response['getMentorForInvitation']['data'] = $resArray;
                } else {
                    $response['getMentorForInvitation']['Error'] = 2;
                    $response['getMentorForInvitation']['Message'] = 'No Mentor found';
                    $response['getMentorForInvitation']['data'] = '';
                }
            } else {
                $response['getMentorForInvitation']['Error'] = 1;
                $response['getMentorForInvitation']['Message'] = 'Something is not right here try again later!' .
			' (__FILE__@__LINE__ in __FUNCTION__)';
            }
        }
        echo json_encode($response);
        exit();
    }
    
    function getMenteeForInvitation($data) {
        $response = array();
        if ($data->MentorID == '') {
            $response['getMenteeForInvitation']['Error'] = 2;
            $response['getMenteeForInvitation']['Message'] = 'Mentor not found';
        } else {
            $resArray = array();
			
            $resultPending = $this->meetingtype_model->getPendingDetail($data->MentorID);
            $resultPendingMeeting = $this->meetingtype_model->getMentorPendingMeetingCount($data->MentorID);
            $response['getMenteeForInvitation']['pending'] = $resultPending[0]->sum1;
            $response['getMenteeForInvitation']['pendingMeeting'] = (string) $resultPendingMeeting;
			
            $results = $this->mentor_model->getMenteeForInvitation();
            //print_r($results);exit;
            if (isset($results) && (!empty($results))) {
                foreach ($results as $val) {
                    $res = $this->mentor_model->getMenteeForInvitationStatus($val->MenteeID, $data->MentorID);
                    if(isset($res[0])){
                        array_push($resArray, $res[0]);
                    }
                }
                if(isset($resArray) && !empty($resArray)){
                    $response['getMenteeForInvitation']['Error'] = 0;
                    $response['getMenteeForInvitation']['Message'] = 'Mentee details';
                    $response['getMenteeForInvitation']['data'] = $resArray;
                } else {
                    $response['getMenteeForInvitation']['Error'] = 2;
                    $response['getMenteeForInvitation']['Message'] = 'No Mentee found';
                    $response['getMenteeForInvitation']['data'] = '';
                }
            } else {
                $response['getMentorForInvitation']['Error'] = 1;
                $response['getMentorForInvitation']['Message'] = 'Something is not right here try again later!' .
			' (__FILE__@__LINE__ in __FUNCTION__)';
            }
        }
        echo json_encode($response);
        exit();
    }

    function startEndMeetingSession($data) {
        $response = array();
        if ($data->MenteeID == '') {
            $response['startEndMeetingSession']['Error'] = 2;
            $response['startEndMeetingSession']['Message'] = 'Mentee not found';
        } else if ($data->MentorID == '') {
            $response['startEndMeetingSession']['Error'] = 2;
            $response['startEndMeetingSession']['Message'] = 'Mentor not found';
        } else if ($data->DateTime == '') {
            $response['startEndMeetingSession']['Error'] = 2;
            $response['startEndMeetingSession']['Message'] = 'DateTime not found';
        } else if ($data->SessionStatus == '') {
            $response['startEndMeetingSession']['Error'] = 2;
            $response['startEndMeetingSession']['Message'] = 'SessionStatus not found';
        } else {
            $resArray = array();
            $results = $this->mentor_model->getMeetingSessionID($data->MenteeID, $data->MentorID, $data->MainTopicID);
            // print_r($results);exit;
            if (empty($results)) {
                //	echo "new";exit;
                //if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "") {
                $res = $this->mentor_model->addStartMeetingSession($data->MenteeID, $data->MentorID, $data->DateTime, $data->SessionStatus, $data->MeetingTypeID, $data->MeetingPlaceID, $data->MainTopicID);
                $response['startEndMeetingSession']['Error'] = 0;
                $response['startEndMeetingSession']['Message'] = 'Mantor details';
                $response['startEndMeetingSession']['data'] = $res;
            } else {

                if ($results[0]->Meeting_status == 1 && $data->SessionStatus == 1) {
                    //	echo "1";exit;
                    $res = $this->mentor_model->updateStartMeetingSession($data->DateTime, $data->SessionStatus, $data->MentorID, $data->MenteeID, $data->MeetingTypeID, $data->MeetingPlaceID, $data->MainTopicID);
                    $response['startEndMeetingSession']['Error'] = 0;
                    $response['startEndMeetingSession']['Message'] = 'Mantor details';
                    $response['startEndMeetingSession']['data'] = $results;
                } else if ($results[0]->Meeting_status == 1 && $data->SessionStatus == 2) {
                    //echo "sdf".$data->SessionStatus;exit;
                    $res = $this->mentor_model->updateEndMeetingSession($data->DateTime, $data->SessionStatus, $data->MentorID, $data->MenteeID, $data->ElapsedTime);
                    $response['startEndMeetingSession']['Error'] = 0;
                    $response['startEndMeetingSession']['Message'] = 'Mantor details';
                    $response['startEndMeetingSession']['data'] = $results;
                } else if ($results[0]->Meeting_status == 2 && $data->SessionStatus == 1) {
                    $res = $this->mentor_model->updateStartMeetingSession($data->DateTime, $data->SessionStatus, $data->MentorID, $data->MenteeID, $data->MeetingTypeID, $data->MeetingPlaceID, $data->MainTopicID);
                    $response['startEndMeetingSession']['Error'] = 0;
                    $response['startEndMeetingSession']['Message'] = 'Mantor details';
                    $response['startEndMeetingSession']['data'] = $results;
                } else {
                    $res = $this->mentor_model->updateEndMeetingSession($data->DateTime, $data->SessionStatus, $data->MentorID, $data->MenteeID, $data->ElapsedTime);
                    $response['startEndMeetingSession']['Error'] = 0;
                    $response['startEndMeetingSession']['Message'] = 'Mantor details';
                    $response['startEndMeetingSession']['data'] = $results;
                }


                /* 21-8-15 */
                if ($results[0]->Meeting_status == 3) {
                    echo "no ";
                    exit;
                    $res = $this->mentor_model->addStartMeetingSession($data->MenteeID, $data->MentorID, $data->DateTime, $data->SessionStatus);
                    $response['startEndMeetingSession']['Error'] = 0;
                    $response['startEndMeetingSession']['Message'] = 'Mantor details';
                    $response['startEndMeetingSession']['data'] = $results;
                }
            }
        }
        echo json_encode($response);
        exit();
    }

    /* khushbu 19-8-15 */
    /* khushbu 21-8-15 */

    function getMentorWaitScreen($data) {
        $response = array();
        if ($data->MenteeID == '') {
            $response['getMentorWaitScreen']['Error'] = 2;
            $response['getMentorWaitScreen']['Message'] = 'Mentee not found';
        } else {
            $resArray = array();
            $results = $this->mentor_model->getMentorWaitScreen($data->MenteeID);
            //print_r($results);exit;
            //print_r($results);
            if (isset($results) && (!empty($results))) {
                foreach ($results as $val) {
                    $res = $this->mentor_model->getMentorWaitScreenDetails($val->MentorID,$data->MenteeID,$val->InvitationId);
                     
                    $res['inviteTime'] = date("Y-m-d H:i", strtotime($res['inviteTime']));
                    array_push($resArray, $res);
                }
                //print_r($resArray);exit;

                $response['getMentorWaitScreen']['Error'] = 0;
                $response['getMentorWaitScreen']['Message'] = 'Mantor details';
                $response['getMentorWaitScreen']['data'] = $resArray;
            } else {
                $response['getMentorWaitScreen']['Error'] = 2;
                $response['getMentorWaitScreen']['Message'] = 'No invitations found.';
            }
        }
        echo json_encode($response);
        exit();
    }

    function getMettingWaitScreenInfo($data) {
        $response = array();
        if ($data->MenteeID == '') {
            $response['getMettingWaitScreenInfo']['Error'] = 2;
            $response['getMettingWaitScreenInfo']['Message'] = 'Mentee not found';
        } else if ($data->MentorID == '') {
            $response['getMettingWaitScreenInfo']['Error'] = 2;
            $response['getMettingWaitScreenInfo']['Message'] = 'Mentor not found';
        } else {
            $resArray = array();
            $results = $this->mentor_model->getMettingWaitScreenInfo($data->MentorID, $data->MenteeID);
            if (!empty($results)) {
                $response['getMettingWaitScreenInfo']['Error'] = 0;
                $response['getMettingWaitScreenInfo']['Message'] = 'Mantor details';
                $response['getMettingWaitScreenInfo']['data'] = $results;
            } else {
                $response['getMettingWaitScreenInfo']['Error'] = 1;
                $response['getMettingWaitScreenInfo']['Message'] = 'Meeting has not started yet!';
            }
        }
        echo json_encode($response);
        exit();
    }

    function getConfig($data) {
        $response = array();

        $resArray = array();
        $results = $this->mentor_model->getConfig();
        if (!empty($results)) {
            $response['getConfig']['Error'] = 0;
            $response['getConfig']['Message'] = 'Configuration Details';
            $response['getConfig']['data'] = $results;
        } else {
            $response['getConfig']['Error'] = 1;
            $response['getConfig']['Message'] = 'No configuration';
        }

        echo json_encode($response);
        exit();
    }

    function mentorIamge($data) {
        $response = array();

        /* if ($_FILES['name'] == '') {
          $response['mentorIamge']['Error'] = 2;
          $response['mentorIamge']['Message'] = 'Mentor Image not found';
          } else */
        if ($_POST['MentorID'] == '') {
            $response['mentorIamge']['Error'] = 2;
            $response['mentorIamge']['Message'] = 'Mentor ID not found';
        } else {
            $resArray = array();
            //echo "here";print_r($_POST);print_r($_FILES);exit;
            if (!empty($_FILES)) {
                $Path = "";
                foreach ($_FILES as $val) {
                    //print_r($val);
                    $microtime = microtime();
                    $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/mentor/assets/admin/images/admin/";

                    $fileName = $val["name"][0];
                    //print_r($val["tmp_name"]);exit;
                    $extension = explode(".", $fileName);

                    $Path = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                    $target_dir2 = $target_dir . $Path;
                    move_uploaded_file($val["tmp_name"][0], $target_dir2);
                }

                $results = $this->mentor_model->setMentorIamge($_POST['MentorID'], $Path);
            }
            if (!empty($results)) {
                $response['mentorIamge']['Error'] = 0;
                $response['mentorIamge']['Message'] = 'Mantor Image set';
                $response['mentorIamge']['data'] = $results;
            } else {
                $response['mentorIamge']['Error'] = 1;
                $response['mentorIamge']['Message'] = 'Mentor Image not uploaded';
            }
        }
        echo json_encode($response);
        exit();
    }

    function updateMentorIamge($data) {
        $response = array();

        /* if ($_FILES['name'] == '') {
          $response['mentorIamge']['Error'] = 2;
          $response['mentorIamge']['Message'] = 'Mentor Image not found';
          } else */ if ($data->MentorID == '') {
            $response['updateMentorIamge']['Error'] = 2;
            $response['updateMentorIamge']['Message'] = 'Mentor ID not found';
        } else {
            $resArray = array();
            if (!empty($_FILES)) {
                $microtime = microtime();
                $target_dir = base_url() . "/assets/admin/images/";
                $fileName = $val["name"];
                $extension = explode(".", $fileName);
                $Path = "";

                $Path = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                $target_dir2 = $target_dir . $Path;
                move_uploaded_file($val["tmp_name"], $target_dir2);
                $Pathi = "";
                //array_push($mediaArr,$Path);
                $results = $this->mentor_model->setMentorIamge($data->MentorID, $Path);
            }
            if (!empty($results)) {
                $response['updateMentorIamge']['Error'] = 0;
                $response['updateMentorIamge']['Message'] = 'Mantor Image set';
                $response['updateMentorIamge']['data'] = $results;
            } else {
                $response['updateMentorIamge']['Error'] = 1;
                $response['updateMentorIamge']['Message'] = 'Mentor Image not uploaded';
            }
        }
        echo json_encode($response);
        exit();
    }

    function deleteMentorIamge($data) {
        $response = array();

        /* if ($_FILES['name'] == '') {
          $response['mentorIamge']['Error'] = 2;
          $response['mentorIamge']['Message'] = 'Mentor Image not found';
          } else */ if ($data->MentorID == '') {
            $response['deleteMentorIamge']['Error'] = 2;
            $response['deleteMentorIamge']['Message'] = 'Mentor ID not found';
        } else {
            $Path = "";
            $results = $this->mentor_model->setMentorIamge($data->MentorID, $Path);
        }
        if (!empty($results)) {
            $response['deleteMentorIamge']['Error'] = 0;
            $response['deleteMentorIamge']['Message'] = 'Mantor Image deleted';
            $response['deleteMentorIamge']['data'] = $results;
        } else {
            $response['deleteMentorIamge']['Error'] = 1;
            $response['deleteMentorIamge']['Message'] = 'Mentor Image not deleted';
        }
        echo json_encode($response);
        exit();
    }

	function rescheduleMeeting($data){
		$response = array();
		/*if ($data->MentorID == '') {
            $response['rescheduleMeeting']['Error'] = 2;
            $response['rescheduleMeeting']['Message'] = 'Mentor ID not found';
        } else if ($data->Status == '') {
            $response['rescheduleMeeting']['Error'] = 2;
            $response['rescheduleMeeting']['Message'] = 'Status not found';
        } else if ($data->Comments == '') {
            $response['rescheduleMeeting']['Error'] = 2;
            $response['rescheduleMeeting']['Message'] = 'Comments not found';
        } else if ($data->MeetingPlaceID == '') {
            $response['rescheduleMeeting']['Error'] = 2;
            $response['rescheduleMeeting']['Message'] = 'Meeting Place ID not found';
        } else  if ($data->MeetingDateTime == '') {
            $response['rescheduleMeeting']['Error'] = 2;
            $response['rescheduleMeeting']['Message'] = 'Meeting Date Time not found';
        } /*else if ($data->MeetingTypeID == '') {
            $response['rescheduleMeeting']['Error'] = 2;
            $response['rescheduleMeeting']['Message'] = 'Meeting Type ID not found';
        } else */if ($data->InvitationId == '') {
            $response['rescheduleMeeting']['Error'] = 2;
            $response['rescheduleMeeting']['Message'] = 'Invitation Id not found';
        } /*else if ($data->MenteeID == '') {
            $response['rescheduleMeeting']['Error'] = 2;
            $response['rescheduleMeeting']['Message'] = 'Mentee ID not found';
        } */else if (!isset($data->UserType) || $data->UserType == '') {
            $response['rescheduleMeeting']['Error'] = 2;
            $response['rescheduleMeeting']['Message'] = 'User Type not found';//0 mentor 1 mentee
        }else {
            $Path = "";
            $results = $this->mentor_model->rescheduleMeeting($data);
			$TopicDescription	=	$results[0]->TopicDescription;
			//print_r($results);exit;
			if (!empty($results)) {
				$response['rescheduleMeeting']['Error'] = 0;
				$response['rescheduleMeeting']['Message'] = 'Request to reschedule meeting sent successfully';
				$response['rescheduleMeeting']['data'] = $results;
				
				if($data->UserType ==0){
					$emailtemplate = $this->emailtemplate_model->get_emailtempate_details(EMAIL_NEW_MEETING_DATE_AND_TIME_ABSOLUTE_ID);
                    if ($emailtemplate) {
                        $emailData = array();
                        
                        $oldtime_timestamp = strtotime($results[0]->oldDate);
                        $old_date = date('M j, Y', $oldtime_timestamp); 
                        $old_time = date('g:i A', $oldtime_timestamp); 
                        
                        
                        $newdate_timestamp = strtotime($data->MeetingDateTime);
                        $new_date = date('M j, Y', $newdate_timestamp); 
                        $new_time = date('g:i A', $newdate_timestamp); 
                        
                        
                        $emailData['toEmail'] = $results[0]->MenteeEmail;
						$emailData['cc'] = $results[0]->MentorEmail;
                        
                        $arrSearch = array('##FIRST_NAME##',"##EMAIL##","##MENTOR_NAME##","##OLDDATE##","##OLDTIME##","##NEWDATE##","##NEWTIME##","##TopicDescription##","##REASON##","##MEETINGTYPE##","##MEETINGPLACE##");
                        $arrReplace = array(ucwords($results[0]->MenteeName),$results[0]->MenteeEmail,ucwords($results[0]->MentorName),$old_date,$old_time,$new_date,$new_time,$results[0]->TopicDescription,$data->RescheduleComments,$results[0]->MeetingTypeName,$results[0]->MeetingPlaceName);
      
    
                        $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                        $emailData['subject'] = $subject;
    
                        $body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                        $emailData['body'] = $body;
    
    
                        send_mail($emailData);
						
						/*send message */
						$this->load->library('twilio');
						$newTest	=	$this->twilio->load();
						//print_r($newTest);exit;
						try{
							$message = $newTest->account->messages->create(array(
								"From"	=>	TWILIO_FROM_NUMBER,
								//"To"	=>	"+918128365306",
								"To"	=>	$results[0]->MenteePhone,
								"Body"	=>	"Your upcoming Meeting with {$results[0]->MentorName} to discuss  $TopicDescription has been rescheduled from $old_date at $old_time to $new_date at $new_time",
							));
						//	echo "Sent message {$message->sid}";
						} catch (Services_Twilio_RestException $e) {
							//echo $e->getMessage();exit;
						}
                    }
				}else{
					$emailtemplate = $this->emailtemplate_model->get_emailtempate_details(EMAIL_RESCHEDULE_REQUEST_ABSOLUTE_ID);
					$newdate_timestamp = strtotime($results[0]->oldDate);
					$new_date = date('M j, Y', $newdate_timestamp); 
					$new_time = date('g:i A', $newdate_timestamp); 
					
                    if ($emailtemplate) {
                        $emailData = array();
  
                        $emailData['toEmail'] = $results[0]->MentorEmail;
						$emailData['cc'] = $results[0]->MenteeEmail;
						
                        
                        $arrSearch = array('##FIRST_NAME##',"##EMAIL##","##MENTEE_NAME##","##TopicDescription##","##REASON##","##NEWDATE##","##NEWTIME##");
                        $arrReplace = array(ucwords($results[0]->MenteeName),$results[0]->MenteeEmail,ucwords($results[0]->MenteeName),$results[0]->TopicDescription,$data->RescheduleComments,$new_date,$new_time);
      
    
                        $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                        $emailData['subject'] = $subject;
    
                        $body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                        $emailData['body'] = $body;
    
    
                        send_mail($emailData);
						
						/*send message */
						$this->load->library('twilio');
						$newTest	=	$this->twilio->load();
						//print_r($newTest);exit;
						try{
							$message = $newTest->account->messages->create(array(
								"From"	=>	TWILIO_FROM_NUMBER,
								//"To"	=>	"+918128365306",
								"To"	=>	$results[0]->MentorPhone,
								"Body"	=>	"Your upcoming Meeting with {$results[0]->MenteeName} to discuss $TopicDescription has been rescheduled" ,
							));
						//	echo "Sent message {$message->sid}";
						} catch (Services_Twilio_RestException $e) {
							//echo $e->getMessage();exit;
						}
                    }
				}
				
				
			} else {
				$response['rescheduleMeeting']['Error'] = 1;
				$response['rescheduleMeeting']['Message'] = 'No invitation found';
			}
        }
        
        echo json_encode($response);
        exit();
	}
	
	function cancelMeeting($data){
		$response = array();
		if ($data->cancellationReason == '') {
            $response['cancelMeeting']['Error'] = 2;
            $response['cancelMeeting']['Message'] = 'Cancellation Reason not found';
        } else if ($data->InvitationId == '') {
            $response['cancelMeeting']['Error'] = 2;
            $response['cancelMeeting']['Message'] = 'Invitation Id not found';
        } else if ($data->UserType == '') {
            $response['cancelMeeting']['Error'] = 2;
            $response['cancelMeeting']['Message'] = 'User Type not found';
        } else {
            $Path = "";
            $results = $this->mentor_model->cancelMeeting($data);
			if (!empty($results) && $results[0]->InsertedId != 0) {
				$response['cancelMeeting']['Error'] = 0;
				$response['cancelMeeting']['Message'] = 'Meeting Cancelled';
				$response['cancelMeeting']['data'] = $results;
				
				$oldtime_timestamp = strtotime($results[0]->oldDate);
				$old_date = date('M j, Y', $oldtime_timestamp); 
				$old_time = date('g:i A', $oldtime_timestamp); 
				
				$emailtemplate = $this->emailtemplate_model->get_emailtempate_details(EMAIL_MEETING_CANCELLED_ABSOLUTE_ID);
                if ($emailtemplate) {
                    $emailData = array();
                    if ($data->UserType == '0') {
						if($results[0]->Status == "Pending"){
							$body	=	"Hello ".ucwords($results[0]->MenteeName).",

							Your pending Meeting invitation with {ucwords($results[0]->MentorName)} to discuss $results[0]->TopicDescription has been cancelled, given reason is '$data->cancellationReason'.


							If you have any questions or queries then please contact to our Support team.

							Thanks.
							Support Team.";
							$subject = ucwords($results[0]->MenteeName).", Meeting Cancelled";
						}else{
							$arrSearch = array('##FIRST_NAME##', "##EMAIL##", "##NAME##","##OLDDATE##","##OLDTIME##","##REASON##","##TopicDescription##");
							$arrReplace = array(ucwords($results[0]->MenteeName), $results[0]->MentorEmail,ucwords($results[0]->MentorName),$old_date,$old_time,$data->cancellationReason,$results[0]->TopicDescription);
							
							$body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
							$subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
						}
						$emailData['toEmail'] = $results[0]->MenteeEmail;

						/*send message */
						$this->load->library('twilio');
						$newTest	=	$this->twilio->load();
						//print_r($newTest);exit;
						try{
							$message = $newTest->account->messages->create(array(
								"From"	=>	TWILIO_FROM_NUMBER,
								//"To"	=>	"+918128365306",
								"To"	=>	$results[0]->MenteePhone,
								"Body"	=>	"Your pending Meeting invitation with ". ucwords($results[0]->MentorName)." to discuss ".$results[0]->TopicDescription." has been cancelled" ,
							));
						//	echo "Sent message {$message->sid}";
						} catch (Services_Twilio_RestException $e) {
							//echo $e->getMessage();exit;
						}
						
                    } else if ($data->UserType == '1') {
						if($results[0]->Status == "Pending"){
							$body	=	"Hello ".ucwords($results[0]->MentorName).",

						Your pending Meeting invitation with ".ucwords($results[0]->MenteeName)." to discuss {$results[0]->TopicDescription} has been cancelled. The reason given is '{$data->cancellationReason}'.


							If you have any questions or queries then please contact to our Support team.

							Thanks.
							The MELS Support Team.";
							$subject = ucwords($results[0]->MentorName).", Meeting Cancelled";
						}else{
							$emailData['toEmail'] = $results[0]->MentorEmail;
							$arrSearch = array('##FIRST_NAME##', "##EMAIL##", "##NAME##","##OLDDATE##","##OLDTIME##","##REASON##","##TopicDescription##");
							$arrReplace = array(ucwords($results[0]->MentorName), $results[0]->MenteeEmail,ucwords($results[0]->MenteeName),$old_date,$old_time,$data->cancellationReason,$results[0]->TopicDescription);
							
							$body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
							$subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
						}
						$emailData['toEmail'] = $results[0]->MentorEmail;
						
						/*send message */
						$this->load->library('twilio');
						$newTest	=	$this->twilio->load();
						//print_r($newTest);exit;
						try{
							$message = $newTest->account->messages->create(array(
								"From"	=>	TWILIO_FROM_NUMBER,
								//"To"	=>	"+918128365306",
								"To"	=>	$results[0]->MentorPhone,
								"Body"	=>	"Your pending Meeting invitation with ". ucwords($results[0]->MenteeName)." to discuss ".$results[0]->TopicDescription." has been cancelled.",
							));
						//	echo "Sent message {$message->sid}";
						} catch (Services_Twilio_RestException $e) {
							//echo $e->getMessage();exit;
						}
                    }
                    $emailData['subject'] = $subject;
                    $emailData['body'] = $body;


                    send_mail($emailData);
                }
				
			} else {
				$response['cancelMeeting']['Error'] = 1;
				$response['cancelMeeting']['Message'] = 'Invitation Id not found';
			}
        }
        
        echo json_encode($response);
        exit();
	}
	
	function getHelpText($data){
		$response = array();
		if ($data->UserType == '') {
            $response['getHelpText']['Error'] = 2;
            $response['getHelpText']['Message'] = 'User Type not found';
        } else {
            $Path = "";
            $results = $this->mentor_model->getHelpText($data->UserType);
			if (!empty($results)) {
				$response['getHelpText']['Error'] = 0;
				$response['getHelpText']['Message'] = 'Help Found';
				$response['getHelpText']['data'] = $results;
			} else {
				$response['getHelpText']['Error'] = 1;
				$response['getHelpText']['Message'] = 'Help not found';
			}
        }
        
        echo json_encode($response);
        exit();
	}
	
	function getMentorWaitScreenPending($data) {
        $response = array();
        if ($data->MenteeID == '') {
            $response['getMentorWaitScreenPending']['Error'] = 2;
            $response['getMentorWaitScreenPending']['Message'] = 'Mentee not found';
        } else {
            $resArray = array();
            $results = $this->mentor_model->getMentorWaitScreenStatus($data->MenteeID,'Pending');
			$response['getMentorWaitScreenPending']['AcceptedCount'] = count($this->mentor_model->getMentorWaitScreenStatus($data->MenteeID,'Accepted'));
			$response['getMentorWaitScreenPending']['DeclinedCount'] = count($this->mentor_model->getMentorWaitScreenStatus($data->MenteeID,'Rejected'));
			$response['getMentorWaitScreenPending']['PendingCount'] = count($results);
            //print_r($results);exit;
            //print_r($results);
            if (isset($results) && (!empty($results))) {
                foreach ($results as $val) {
                    $res = $this->mentor_model->getMentorWaitScreenDetails($val->MentorID,$data->MenteeID,$val->InvitationId);
                     
					if($res['Status'] == 'Rescheduled')
						$res['inviteTime'] = date("Y-m-d H:i", strtotime($res['MeetingDateTime']));
					else
						$res['inviteTime'] = date("Y-m-d H:i", strtotime($res['inviteTime'])); 
                    
                    array_push($resArray, $res);
                }
                //print_r($resArray);exit;

                $response['getMentorWaitScreenPending']['Error'] = 0;
                $response['getMentorWaitScreenPending']['Message'] = 'Mentor details';
                $response['getMentorWaitScreenPending']['data'] = $resArray;
            } else {
                $response['getMentorWaitScreenPending']['Error'] = 2;
                $response['getMentorWaitScreenPending']['Message'] = 'No pending invitations found.';
            }
        }
        echo json_encode($response);
        exit();
    }
	
	function getMentorWaitScreenRejected($data) {
        $response = array();
        if ($data->MenteeID == '') {
            $response['getMentorWaitScreenRejected']['Error'] = 2;
            $response['getMentorWaitScreenRejected']['Message'] = 'Mentee not found';
        } else {
            $resArray = array();
            $results = $this->mentor_model->getMentorWaitScreenStatus($data->MenteeID,'Rejected');
			$response['getMentorWaitScreenRejected']['AcceptedCount'] = count($this->mentor_model->getMentorWaitScreenStatus($data->MenteeID,'Accepted'));
			$response['getMentorWaitScreenRejected']['DeclinedCount'] = count($results);
			$response['getMentorWaitScreenRejected']['PendingCount'] = count($this->mentor_model->getMentorWaitScreenStatus($data->MenteeID,'Pending'));
            //print_r($results);exit;
            if (isset($results) && (!empty($results))) {
                foreach ($results as $val) {
                    $res = $this->mentor_model->getMentorWaitScreenDetails($val->MentorID,$data->MenteeID,$val->InvitationId);
					//echo "<pre>";print_r($data);exit;
                     
                    $res['inviteTime'] = date("Y-m-d H:i", strtotime($res['inviteTime']));
                    $res['AcceptOrRejectTime'] = date("Y-m-d H:i", strtotime($res['AcceptOrRejectTime']));
                    array_push($resArray, $res);
                }
                //print_r($resArray);exit;

                $response['getMentorWaitScreenRejected']['Error'] = 0;
                $response['getMentorWaitScreenRejected']['Message'] = 'Mentor details';
                $response['getMentorWaitScreenRejected']['data'] = $resArray;
            } else {
                $response['getMentorWaitScreenRejected']['Error'] = 2;
                $response['getMentorWaitScreenRejected']['Message'] = 'No declined invitations found.';
            }
        }
        echo json_encode($response);
        exit();
    }
	
	function getMentorWaitScreenAccepted($data) {
        $response = array();
        if ($data->MenteeID == '') {
            $response['getMentorWaitScreenAccepted']['Error'] = 2;
            $response['getMentorWaitScreenAccepted']['Message'] = 'Mentee not found';
        } else {
            $resArray = array();
            $results = $this->mentor_model->getMentorWaitScreenStatus($data->MenteeID,'Accepted');
			$response['getMentorWaitScreenAccepted']['AcceptedCount'] = count($results);
			$response['getMentorWaitScreenAccepted']['DeclinedCount'] = count($this->mentor_model->getMentorWaitScreenStatus($data->MenteeID,'Rejected'));
			$response['getMentorWaitScreenAccepted']['PendingCount'] = count($this->mentor_model->getMentorWaitScreenStatus($data->MenteeID,'Pending'));
            //print_r($results);exit;
            //print_r($results);
            if (isset($results) && (!empty($results))) {
                foreach ($results as $val) {
                    $res = $this->mentor_model->getMentorWaitScreenDetails($val->MentorID,$data->MenteeID,$val->InvitationId);
                     
					//if($res['Status'] == 'Rescheduled')
						$res['inviteTime'] = date("Y-m-d H:i", strtotime($res['MeetingDateTime']));
					/*else
						$res['inviteTime'] = date("Y-m-d H:i", strtotime($res['inviteTime']));*/
                    
                    array_push($resArray, $res);
                }
                //print_r($resArray);exit;

                $response['getMentorWaitScreenAccepted']['Error'] = 0;
                $response['getMentorWaitScreenAccepted']['Message'] = 'Mentor details';
                $response['getMentorWaitScreenAccepted']['data'] = $resArray;
            } else {
                $response['getMentorWaitScreenAccepted']['Error'] = 2;
                $response['getMentorWaitScreenAccepted']['Message'] = 'No accepted invitations found.';
            }
        }
        echo json_encode($response);
        exit();
    }
    
    public function getAcceptResponses(){
        $response = array();
        $results = $this->mentor_model->getAcceptResponses();
        if (isset($results[0]->ResponseID) && $results[0]->ResponseID != "") {
            $response['getAcceptResponses']['Error'] = 0;
            $response['getAcceptResponses']['Message'] = "Accept Responses found.";
            $response['getAcceptResponses']['data'] = $results;
        } else {
            $response['getAcceptResponses']['Error'] = 1;
            $response['getAcceptResponses']['Message'] = 'Accept Responses not found';
        }
        echo json_encode($response);
        exit();
    }
    
    public function getDeclineResponses(){
        $response = array();
        $results = $this->mentor_model->getDeclineResponses();
        if (isset($results[0]->ResponseID) && $results[0]->ResponseID != "") {
            $response['getDeclineResponses']['Error'] = 0;
            $response['getDeclineResponses']['Message'] = "Decline Responses found.";
            $response['getDeclineResponses']['data'] = $results;
        } else {
            $response['getDeclineResponses']['Error'] = 1;
            $response['getDeclineResponses']['Message'] = 'Decline Responses not found';
        }
        echo json_encode($response);
        exit();
    }
	
	public function resetPassword($id,$UserType){
		$data['id']	=	$id;
		$data['UserType']	=	$UserType;
		$this->load->view('updatePassword',$data);
		
	}
	
	public function updateMentorpassword() {
		//$this->load->model('admin/employee_model');
        if ($this->input->post()) {
			$id = $this->input->post('mainId');
			$UserType	=	$this->input->post('UserType');
			$password = $this->input->post('password');
			$password = fnEncrypt($password, $this->config->item('mentorKey'));
			
			//print_r($id);exit;
			if(empty($id)){
				echo "Something went wrong try again..." .
					' (__FILE__@__LINE__ in __FUNCTION__)';
			}else{		
				if($UserType == 0){
					$data = $this->mentor_model->resetpassword($id,$password,$UserType);
				}else{
					$data = $this->mentor_model->resetpassword($id,$password,$UserType);
					//echo "here";print_r($userDetails);exit;
				}

				if ($data== true) {
					//$this->session->set_flashdata('success', 'Password reset successfully');
					//redirect('/admin');
					$this->load->view('updatePassword');
					//echo "Password reset successfully";
				}else{
					echo "There is some problem in sending email. Please try again later!";
				}
			}
			
        }
    }
    
    function launchApp($device) {
        $this->redirectToPlayStore($device);
    }
    
    function redirectToPlayStore($device) {
        if($device == 'Android'){
            //header('Location: https://play.google.com/store/apps/details?id=com.melstm');
            header('Location: market://details?id=com.melstm');
            die();
        } else if($device == 'iOS') {
            header('Location: https://itunes.apple.com/us/app/melstm/id1070132202?ls=1&mt=8');
            die();
        } else {
            die();
        }
    }
}
