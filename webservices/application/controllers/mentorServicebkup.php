<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting();

class mentorService extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('topic_model', '', TRUE);
        $this->load->model('mentee_model', '', TRUE);
        $this->load->model('meetingtype_model', '', TRUE);
        $this->load->model('skills_model', '', TRUE);
        $this->load->model('mentor_model', '', TRUE);
        $this->load->model('emailtemplate_model', '', TRUE);
        ini_set('memory_limit', '-1');
        ini_set('display_errors', 1);
        error_reporting(E_ALL);

        $this->load->library('email');
        $this->load->library('upload');
        $this->load->helper('url');
        $this->data['base_upload'] = $this->config->item('base_upload');
        $this->data['base_upload_path'] = $this->config->item('base_upload_path');
        $this->data['base_upload_url'] = $this->config->item('base_upload_url');

        ini_set('memory_limit', '-1');
    }

    function index() {
//echo 'dfsdfs';exit;
        error_reporting(E_ALL);
        header('Content-type: application/json');
        $data = file_get_contents('php://input');

        if ($data == null) {

            //$data = '{"method":"deleteAccount","body":{"iCustomerId":"2"}}';

            //$data = '{"method":"getTopic","body":{}}';
            //$data = '{"method":"getSubTopic","body":{}}';
            //$data = '{"method":"getMentee","body":{}}';
            //$data = '{"method":"getMentee","body":{"MentorID":"1"}}';


            //$data = '{"method":"getMeetingType","body":{}}';
            //$data = '{"method":"getSkills","body":{}}';
            //$data = '{"method":"getMentorActionTaken","body":{}}';
            //$data = '{"method":"getMenteeActionTaken","body":{}}';
            //$data = '{"method":"registerUser","body":{"Email":"nerikar.krunal@gmail.com","Password":"","AccessToken":"","UserType":"0"}}';
            //$data = '{"method":"checkLogin","body":{"Email":"username","Password":"password"}}';

	//		{"method":"checkLogin","body":{"UserName":"Avinash","Password":"avinash"}}

            //$data = '{"method":"forgotPassword","body":{"Email":"email_id"}}';
            //$data = '{"method":"changePassword","body":{"Email":"krunal@gmail.com","OldPassword":"123krunal","NewPassword":"Test","UserType":"0"}}';
            //$data = '{"method":"submitMeetingInfo","body":{"MeetingID":"1","MentorID":"1","MenteeID":"1","MeetingStartDatetime":"2015-07-10 09:42:02","MeetingEndDatetime":"2015-07-10 10:42:02","MeetingTypeID":"1","MeetingTopicID":"1","MeetingSubTopicID":"1","MeetingPlaceID":"0","MenteeActionIDs":"1,2","MentorActionIDs":"1,2","MeetingElapsedTime":"2.5","MeetingFeedback":"testing","MentorActionItemDone":"1,2","MenteeActionItemDone":"1"}}';
					
				//$data = '{"method":"submitMeetingInfo","body":{"MeetingID":"1","MentorID":"19","MenteeID":"19","MeetingStartDatetime":"2015-07-10 09:42:02","MeetingEndDatetime":"2015-07-10 10:42:02","MeetingTypeID":"1","MeetingTopicID":"1","MeetingSubTopicID":"1","MeetingPlaceID":"0","MenteeActionIDs":"1,2","MentorActionIDs":"1,2","MeetingElapsedTime":"2.5","MeetingFeedback":"testing","MentorActionItemDone":"1,2","MenteeActionItemDone":"1","SessionStatus":"2"}}'	            
            
            //$data = '{"method":"menteeComment","body":{"MentorID":"1","MenteeID":"1","MeetingID":"2","MenteeComment":"test","CareC":"1","CareA":"1","CareR":"1","CareE":"1"}}';
            //$data = '{"method":"mentorComment","body":{"MentorID":"1","MenteeID":"1","MeetingID":"2","MentorComment":"1","SatisfactionIndex":"1"}}';
            //$data = '{"method":"addSkill","body":{"UserID":"1","UserType":"0","SkillID":"1,2"}}';
            //$data = '{"method":"getMeetingHistory","body":{"UserId":"1","UserType":"0","CurrentPage":"1","PageSize":"10"}}';
            //$data = '{"method":"getMeetingHistory","body" {"UserId":"1","UserType":"0","CurrentPage":"1","PageSize":"10","MentorComment":"10","SatisfactionIndex":"10","MenteeComment":"10","CareC":"10","CareA":"10","CareR":"10","CareE":"10"}}';
             //$data = '{"method":"getMentor","body":{}}';
             //$data = '{"method":"getMentor","body":{"MenteeID":"1"}}';



             //$data = '{"method":"mentorInvitation","body":{"MentorID":"1","MenteeID":"1","TopicID":"1","MenteeComments":"khushbu"}}';
             //$data = '{"method":"getMentorPendingRequest","body":{"MentorID":"1"}}';
             //$data = '{"method":"mentorResponse","body":{"InvitationId":"1","Status":"Accepted","Comments":"test....."}}';
             //$data = '{"method":"editProfile","body":{"UserId":"1","UserType":"0","UserName":"dasf","PhoneNumber":"9812345678","Pass":"124562","Email":"reg.charney+mentor1@entrebahn.com"}}';
             //$data = '{"method":"updateMenteeactionsItemsWithDone","body":{"MenteeID":"1","MeetingID":"0","ActionId":"1","MenteeActionItemDone":"1,2"}}';
        		 //$data	= '{"method":"getMentorForInvitation","body":{"MenteeID":"1"}}	'
        		 //$data = '{"method":"startEndMeetingSession","body":{"SessionStatus":"1","DateTime":"","MentorID":"1","MenteeID":"1","SessionStatus":"1"}}	'
       		 //$data	= '{"method":"getMentorWaitScreen","body":{"MenteeID":"1"}}' 
       		 //$data	= '{"method":"getMettingWaitScreenInfo","body":{"MentorID":"1","MenteeID":"1"}}'
       		 
        }

        if ($data != '') {
            $data = $this->checkvalidjson($data);
            $method = $data->method;
            $json = $data->body;
        } else {
            $method = $this->input->post('method');
            $json = array(
                'UserID' => $this->input->post('UserID'),
                'UserType' => $this->input->post('UserType'),
                'PictureType' => $this->input->post('PictureType'),
            );
        }


        switch ($method) {
            case 'updateMenteeactionsItemsWithDone':
                $this->updateMenteeactionsItemsWithDone($json);
                break;
            case 'editProfile':
                $this->editProfile($json);
                break;
            case 'mentorResponse':
                $this->mentorResponse($json);
                break;
            case 'getMentorPendingRequest':
                $this->getMentorPendingRequest($json);
                break;
            case 'mentorInvitation':
                $this->mentorInvitation($json);
                break;
            case 'getMeetingHistory':
                $this->getMeetingHistory($json);
                break;
            case 'addSkill':
                $this->addSkill($json);
                break;
            case 'mentorComment':
                $this->mentorComment($json);
                break;
            case 'menteeComment':
                $this->menteeComment($json);
                break;
            case 'submitMeetingInfo':
                $this->submitMeetingInfo($json);
                break;
            case 'changePassword':
                $this->changePassword($json);
                break;
            case 'forgotPassword':
                $this->forgotPassword($json);
                break;
            case 'registerUser':
                $this->registerUser($json);
                break;
            case 'getTopic':
                $this->getTopic();
                break;
            case 'getMentor':
                $this->getMentor($json);
                break;   
            case 'getSubTopic':
                $this->getSubTopic();
                break;
            case 'getMentee':
                $this->getMentee($json);
                break;
            case 'getMeetingType':
                $this->getMeetingType();
                break;
            case 'getSkills':
                $this->getSkills();
                break;
            case 'getMentorActionTaken':
                $this->getMentorActionTaken();
                break;
            case 'getMenteeActionTaken':
                $this->getMenteeActionTaken();
                break;
            case 'checkLogin':
                $this->checkLogin($json);
                break;
            case 'addUploadPics':
                $this->addUploadPics($json);
                break;
            case 'getMentorForInvitation':
                $this->getMentorForInvitation($json);
                break;  
            case 'startEndMeetingSession':
                $this->startEndMeetingSession($json);
                break;   
            case 'getMentorWaitScreen':
                $this->getMentorWaitScreen($json);
                break;    
            case 'getMettingWaitScreenInfo':
                $this->getMettingWaitScreenInfo($json);
                break;            
            default:
                $this->default_fun();
                break;
        }
    }

    function default_fun() {
        $response = array();
        $response['default']['Error'] = 1;
        $response['default']['Message'] = 'No methods found';
        echo json_encode($response);
        exit();
    }

    function updateMenteeactionsItemsWithDone($data) {
        $response = array();
        if ($data->MenteeID == '') {
            $response['updateMenteeactionsItemsWithDone']['Error'] = 2;
            $response['updateMenteeactionsItemsWithDone']['Message'] = 'Mentee not found';
        } else if ($data->MeetingID == '') {
            $response['updateMenteeactionsItemsWithDone']['Error'] = 2;
            $response['updateMenteeactionsItemsWithDone']['Message'] = 'Meeting not found';
        } else if ($data->ActionId == '') {
            $response['updateMenteeactionsItemsWithDone']['Error'] = 2;
            $response['updateMenteeactionsItemsWithDone']['Message'] = 'Action not found';
        } else {
            if ($data->MeetingID > 0) {
                $this->db->where(array('MenteeID'=> $data->MenteeID, 'MeetingID'=> $data->MeetingID));
                $this->db->delete('menteeactionstaken'); 
            }
            $Actionids = explode(',',$data->ActionId);
            foreach($Actionids as $val){
                   $menteeResults = $this->mentor_model->updateMenteeactionsItemsWithDoneDetail($data->MenteeID, $data->MeetingID, $val);
            }
            $results = $this->mentor_model->updateMeetingMenteeactionsItemsWithDoneDetail($data->MeetingID, $data->MenteeActionItemDone);
            if (isset($results[0]->Id) && $results[0]->Id != "") {
	    	$response['updateMenteeactionsItemsWithDone']['Error'] = 0;
                $response['updateMenteeactionsItemsWithDone']['Message'] = 'Mentee action items updated successfully';
                $response['updateMenteeactionsItemsWithDone']['data'] = $results[0];
	    } else if (isset($results[0]->Message) && $results[0]->Message != ""){
	    	$response['updateMenteeactionsItemsWithDone']['Error'] = 2;
                $response['updateMenteeactionsItemsWithDone']['Message'] = $results[0]->Message;
	    } else {
	    	$response['updateMenteeactionsItemsWithDone']['Error'] = 1;
                $response['updateMenteeactionsItemsWithDone']['Message'] = 'Error has  occurred. Please try again later.';
	    }
        }
        echo json_encode($response);
        exit();
    }

    function editProfile($data) {
        $response = array();
        if ($data->UserId == '') {
            $response['editProfile']['Error'] = 2;
            $response['editProfile']['Message'] = 'User not found';
        } else {
            $results = $this->mentor_model->editProfileDetail($data->UserId, $data->UserType, $data->UserName, $data->PhoneNumber, $data->Pass,$data->Email,$data->ImageData);
            if (isset($results[0]->Id) && $results[0]->Id != "") {
	    	$response['editProfile']['Error'] = 0;
                $response['editProfile']['Message'] = 'Profile updated successfully';
                $response['editProfile']['data'] = $results[0];

                $userData = $this->mentor_model->getProfileDetail($data->UserId,$data->UserType);

                // SEND EMAIL TO MENTOR
    		$emailtemplate =  $this->emailtemplate_model->get_emailtempate_details(4);
                    if($emailtemplate){
                        $emailData = array();
                        $emailData['toEmail'] = $userData['Email'];
                        
                        $arrSearch = array('##FIRST_NAME##', '##EMAIL_ADDRESS##','##PASSWORD##', '##USER_TYPE##');
                        $arrReplace = array($userData['Name'], $userData['Email'], $userData['AccessToken'], $userData['UserType']);
                        
                        $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                        $emailData['subject'] = $subject;
                        
                        $body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                        $emailData['body'] = $body;
                        send_mail($emailData);
                    }
                // END SEND EMAIL TO MENTOR

	    } else if (isset($results[0]->Message) && $results[0]->Message != ""){
	    	$response['editProfile']['Error'] = 2;
                $response['editProfile']['Message'] = $results[0]->Message;
	    } else {
	    	$response['editProfile']['Error'] = 1;
                $response['editProfile']['Message'] = 'Error has occurred. Please try again later.';
	    }
        }
        echo json_encode($response);
        exit();
    }

    function mentorResponse($data) {
        $response = array();
        if ($data->InvitationId == '') {
            $response['mentorResponse']['Error'] = 2;
            $response['mentorResponse']['Message'] = 'Invitation not found';
        } else if ($data->Status == '') {
            $response['mentorResponse']['Error'] = 2;
            $response['mentorResponse']['Message'] = 'Status not found';
        } else {
            $results = $this->mentor_model->mentorResponseDetail($data->InvitationId, $data->Status, $data->Comments);
            if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "") {
	    	$response['mentorResponse']['Error'] = 0;
                $response['mentorResponse']['Message'] = 'Mentor invitation '.$data->Status.' successfully';
                $response['mentorResponse']['data'] = $results;
	    } else if (isset($results[0]->Message) && $results[0]->Message != ""){
	    	$response['mentorResponse']['Error'] = 2;
                $response['mentorResponse']['Message'] = $results[0]->Message;
	    } else {
	    	$response['mentorResponse']['Error'] = 1;
                $response['mentorResponse']['Message'] = 'Error has occurred. Please try again later.';
	    }
        }
        echo json_encode($response);
        exit();
    }

    function getMentorPendingRequest($data) {
        $response = array();
        if ($data->MentorID == '') {
            $response['getMentorPendingRequest']['Error'] = 2;
            $response['getMentorPendingRequest']['Message'] = 'Mentor not found';
        } else {
            $results = $this->mentor_model->getMentorPendingRequestDetail($data->MentorID);
            if (isset($results[0]->MenteeID) && $results[0]->MenteeID != "") {
	    	$response['getMentorPendingRequest']['Error'] = 0;
                $response['getMentorPendingRequest']['Message'] = 'Mentor invitation found successfully';
                $response['getMentorPendingRequest']['data'] = $results;
	    } else if (isset($results[0]->Message) && $results[0]->Message != ""){
	    	$response['getMentorPendingRequest']['Error'] = 2;
                $response['getMentorPendingRequest']['Message'] = $results[0]->Message;
	    } else {
	    	$response['getMentorPendingRequest']['Error'] = 1;
                $response['getMentorPendingRequest']['Message'] = 'Error has occurred. Please try again later.';
	    }
        }
        echo json_encode($response);
        exit();
    }

    function mentorInvitation($data) {
        $response = array();
	$this->load->library('email');
        if ($data->MentorID == '') {
            $response['mentorInvitation']['Error'] = 2;
            $response['mentorInvitation']['Message'] = 'Mentor not found';
        } else if ($data->MenteeID == '') {
            $response['mentorInvitation']['Error'] = 2;
            $response['mentorInvitation']['Message'] = 'Mentee not found';
        } else if ($data->TopicID == '') {
            $response['mentorInvitation']['Error'] = 2;
            $response['mentorInvitation']['Message'] = 'Topic not found';
        }  else if ($data->MenteeComments == '') {
            $response['mentorInvitation']['Error'] = 2;
            $response['mentorInvitation']['Message'] = 'Mentee comments not found';
        } else {
            $results = $this->mentor_model->mentorInvitationDetail($data->MentorID, $data->MenteeID,$data->TopicID,$data->MenteeComments);
            if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "") {

				$response = array();
				
				$results1 = $this->mentor_model->checkInvitationDetail($data->MentorID, $data->MenteeID);
				//print_r($results1);exit;
				If (isset($results1[0]->MeetingID) && $results1[0]->MeetingID > 0) {
					// SEND EMAIL TO CUSTOMER 
					$emailtemplate = $this->emailtemplate_model->get_emailtempate_details(5);
					//print_r($emailtemplate);exit;
					if ($emailtemplate) {
						$emailData = array();
						
						$emailData['toEmail']	= $results1[0]->MentorEmail;
						$emailData['cc'] 		= $results1[0]->MenteeEmail;
						$arrSearch = array('##FIRST_NAME##', "##EMAIL##", "##PASSWORD##");
						//$arrReplace = array($results1[0]->MentorEmail, $results1[0]->AccessToken);
						
						$subject = str_replace($arrSearch, $results1[0]->MentorEmail, $emailtemplate['EmailTemplateTitle']);
						$emailData['subject'] = $subject;

												
						$body	=	"Mentee ".$results1[0]->MenteeName." has asked for a meeting to discuss ".$results1[0]->TopicDescription." related issues with you. Please use your MELS App to accept or decline the invitation in the next 10 days, otherwise the invitation will expire.";
						//$body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
						$emailData['body'] = $body;


						send_mailInvitation($emailData);
						
						//$results123 = $this->mentor_model->addinviteList($data->MentorID, $data->MenteeID);
						
					}		
				}else{
					$response['mentorInvitation']['Error'] = 1;
					$response['mentorInvitation']['Message'] = 'Failed to send E-mail.';
				}
                $response['mentorInvitation']['Message'] = 'Mentor invitation sent successfully';
                $response['mentorInvitation']['data'] = $results[0];
			} else if (isset($results[0]->Message) && $results[0]->Message != ""){
				$response['mentorInvitation']['Error'] = 2;
					$response['mentorInvitation']['Message'] = $results[0]->Message;
			} else {
					$response['mentorInvitation']['Error'] = 1;
					$response['mentorInvitation']['Message'] = 'Error has occurred. Please try again later.';
			}
        }
        echo json_encode($response);
        exit();
    }

    function getMeetingHistory($data) {
        $response = array();
        if ($data->UserId == '') {
            $response['getMeetingHistory']['Error'] = 2;
            $response['getMeetingHistory']['Message'] = 'User not found';
        } else if ($data->UserType == '') {
            $response['getMeetingHistory']['Error'] = 2;
            $response['getMeetingHistory']['Message'] = 'User Type not found';
        } else if ($data->CurrentPage == '') {
            $response['getMeetingHistory']['Error'] = 2;
            $response['getMeetingHistory']['Message'] = 'Current Page not found';
        } else if ($data->PageSize == '') {
            $response['getMeetingHistory']['Error'] = 2;
            $response['getMeetingHistory']['Message'] = 'Page Size not found';
        } else {

            $results = $this->meetingtype_model->getMeetingHistoryDetail($data->UserId, $data->UserType, $data->CurrentPage, $data->PageSize);
	    $resultPending	= $this->meetingtype_model->getPendingDetail($data->UserId);
				//print_r($resultPending);exit;            
            if(empty($results)){
            		$response['getMeetingHistory']['Error'] = 1;
                	$response['getMeetingHistory']['Message'] = 'No Meeting History Found!	';
            }else{
            
					if (isset($results[0]->MeetingID) && $results[0]->MeetingID != "") {
	    				$response['getMeetingHistory']['Error'] = 0;
                	$response['getMeetingHistory']['Message'] = 'Meeting history found successfully';
                	$response['getMeetingHistory']['data'] = $results;
			$response['getMeetingHistory']['pending'] = $resultPending[0]->sum1;
	    			} else if (isset($results[0]->Message) && $results[0]->Message != ""){
	    				$response['getMeetingHistory']['Error'] = 2;
               				$response['getMeetingHistory']['Message'] = $results[0]->Message;
	   			} else {
	    				$response['getMeetingHistory']['Error'] = 1;
                	$response['getMeetingHistory']['Message'] = 'Error has occurred. Please try again later.';
	    			}            
            }
            
            
        }
        echo json_encode($response);
        exit();
    }

    function addSkill($data) {
        $response = array();
        if ($data->UserID == '') {
            $response['addSkill']['Error'] = 2;
            $response['addSkill']['Message'] = 'User not found';
        } else if ($data->UserType == '') {
            $response['addSkill']['Error'] = 2;
            $response['addSkill']['Message'] = 'User Type not found';
        } else if ($data->SkillID == '') {
            $response['addSkill']['Error'] = 2;
            $response['addSkill']['Message'] = 'Skill not found';
        } else {
            if ($data->UserType == 0) {
                $this->db->where(array('MentorID'=> $data->UserID));
                $this->db->delete('mentorskillset'); 
            } else {
                $this->db->where(array('MenteeID'=> $data->UserID));
                $this->db->delete('menteeskill'); 
            }
            $skillIds = explode(',',$data->SkillID);
            foreach($skillIds as $val){
               $results = $this->skills_model->addSkillDetail($data->UserID, $data->UserType, $val);
            }
            
            if (isset($results[0]->Skills) && $results[0]->Skills != "") {
	    	$response['addSkill']['Error'] = 0;
                $response['addSkill']['Message'] = 'Mentee skill added successfully';
                $response['addSkill']['data'] = $results[0];
	    } else if (isset($results[0]->Message) && $results[0]->Message != ""){
	    	$response['addSkill']['Error'] = 2;
                $response['addSkill']['Message'] = $results[0]->Message;
	    } else {
	    	$response['addSkill']['Error'] = 1;
                $response['addSkill']['Message'] = 'Error has occurred. Please try again later.';
	    }
        }
        echo json_encode($response);
        exit();
    }

    function mentorComment($data) {
        $response = array();
        if ($data->MentorID == '') {
            $response['mentorComment']['Error'] = 2;
            $response['mentorComment']['Message'] = 'Mentor not found';
        } else if ($data->MenteeID == '') {
            $response['mentorComment']['Error'] = 2;
            $response['mentorComment']['Message'] = 'Mentee not found';
        } else if ($data->MeetingID == '') {
            $response['mentorComment']['Error'] = 2;
            $response['mentorComment']['Message'] = 'Meeting not found';
        } else if ($data->SatisfactionIndex == '') {
            $response['mentorComment']['Error'] = 2;
            $response['mentorComment']['Message'] = 'Satisfaction rating missing';
        } else {

            $results = $this->mentor_model->mentorCommentDetail($data->MentorID, $data->MenteeID, $data->MeetingID, $data->MentorComment, $data->SatisfactionIndex);

            if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "") {
	    	$response['mentorComment']['Error'] = 0;
                $response['mentorComment']['Message'] = 'Mentor review form saved';
                $response['mentorComment']['data'] = $results[0];
	    } else if (isset($results[0]->Message) && $results[0]->Message != ""){
	    	$response['mentorComment']['Error'] = 2;
                $response['mentorComment']['Message'] = $results[0]->Message;
	    } else {
	    	$response['mentorComment']['Error'] = 1;
                $response['mentorComment']['Message'] = 'Error has occurred. Please try again later.';
	    }
        }
        echo json_encode($response);
        exit();
    }

    function menteeComment($data) {
        $response = array();
        if ($data->MentorID == '') {
            $response['menteeComment']['Error'] = 2;
            $response['menteeComment']['Message'] = 'Mentor not found';
        } else if ($data->MenteeID == '') {
            $response['menteeComment']['Error'] = 2;
            $response['menteeComment']['Message'] = 'Mentee not found';
        } else if ($data->MeetingID == '') {
            $response['menteeComment']['Error'] = 2;
            $response['menteeComment']['Message'] = 'Meeting not found';
        } else if ($data->CareC == '') {
            $response['menteeComment']['Error'] = 2;
            $response['menteeComment']['Message'] = 'Satisfaction rating for CLEAR is missing';
        } else if ($data->CareA == '') {
            $response['menteeComment']['Error'] = 2;
            $response['menteeComment']['Message'] = 'Satisfaction rating for APPLICABLE is missing';
        } else if ($data->CareR == '') {
            $response['menteeComment']['Error'] = 2;
            $response['menteeComment']['Message'] = 'Satisfaction rating for RELEVANT is missing';
        } else if ($data->CareE == '') {
            $response['menteeComment']['Error'] = 2;
            $response['menteeComment']['Message'] = 'Satisfaction rating for EXECUTABLE is missing';
        } else {

            $results = $this->mentee_model->menteeCommentDetail($data->MentorID, $data->MenteeID, $data->MeetingID, $data->MenteeComment, $data->CareC, $data->CareA, $data->CareR, $data->CareE);

            if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "") {
	    	$response['menteeComment']['Error'] = 0;
                $response['menteeComment']['Message'] = 'Mentee Review Form saved';
                $response['menteeComment']['data'] = $results[0];
	    } else if (isset($results[0]->Message) && $results[0]->Message != ""){
	    	$response['menteeComment']['Error'] = 2;
                $response['menteeComment']['Message'] = $results[0]->Message;
	    } else {
	    	$response['menteeComment']['Error'] = 1;
                $response['menteeComment']['Message'] = 'Error has occurred. Please try again later.';
	    }
        }
        echo json_encode($response);
        exit();
    }

    function submitMeetingInfo($data) {
        $response = array();
        $SUBTOPICID	=	array();
        if ($data->MeetingID == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Meeting not found';
        } else if ($data->MentorID == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Mentor not found';
        } else if ($data->MenteeID == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Mentee not found';
        } else if ($data->MeetingStartDatetime == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Meeting Start Date time not found';
        } else if ($data->MeetingEndDatetime == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Meeting End Date time not found';
        } else if ($data->MeetingTypeID == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Meeting Type not found';
        } else if ($data->MeetingTopicID == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Meeting Topic not found';
        } else if ($data->MeetingSubTopicID == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Meeting Sub Topic not found';
        } else if ($data->MeetingPlaceID == '') {
            $response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = 'Meeting Place not found';
        } else {
        	
            /*khushbu 20-8*/      	
					$results2 = $this->mentor_model->getMeetingSessionIDforSubmit($data->MenteeID,$data->MentorID,$data->MeetingID);	
					//print_r($results2);exit;
					if(!empty($results2)){
						if ($results2[0]->Meeting_status == 2 || $results2[0]->Meeting_status == 3) {
							$MeetingID	=	$results2[0]->MeetingID;
						
						$results = $this->mentor_model->submitMeetingInfoDetail($MeetingID, $data->MentorID, $data->MenteeID, $data->MeetingStartDatetime, $data->MeetingEndDatetime, $data->MeetingTypeID, $data->MeetingTopicID, $data->MeetingSubTopicID, $data->MeetingPlaceID, $data->MenteeActionIDs, $data->MeetingElapsedTime, $data->MeetingFeedback, @$data->MentorActionItemDone, @$data->MenteeActionItemDone);
							//print_r($results);
						
							if ($data->MeetingID > 0) {
								$this->db->where(array('MenteeID'=> $data->MenteeID, 'MeetingID'=>$MeetingID));
								$this->db->delete('menteeactionstaken'); 

								$this->db->where(array('MentorID'=> $data->MentorID, 'MeetingID'=>$MeetingID));
								$this->db->delete('mentoractionstaken'); 
								
								$this->db->where(array('MentorID'=> $data->MentorID, 'MeetingID'=>$MeetingID));
								$this->db->delete('mentor_subTopicsIDs'); 
							 }
							 
							 
						if(isset($results[0]->InsertedId) && $results[0]->InsertedId != "" && isset($data->MenteeActionIDs) && $data->MenteeActionIDs != "") {
							$menteeids = explode(',',$data->MenteeActionIDs);
							foreach($menteeids as $val){
							   $menteeResults = $this->mentor_model->addMenteeactionstakenDetail($data->MenteeID, $results[0]->InsertedId, $val);
							}
						}
						if(isset($results[0]->InsertedId) && $results[0]->InsertedId != "" && isset($data->MentorActionIDs) && $data->MentorActionIDs != "") {
							$mentorids = explode(',',$data->MentorActionIDs);
							foreach($mentorids as $val){
							   $mentorResults = $this->mentor_model->addMentoractionstakenDetail($data->MentorID, $results[0]->InsertedId, $val);
							}
						}
					 
						if(isset($results[0]->InsertedId) && $results[0]->InsertedId != "" && isset($data->MeetingSubTopicID) && $data->MeetingSubTopicID != "") {
							$mentorids = explode(',',$data->MeetingSubTopicID);
							foreach($mentorids as $val){
							$mentorResults = $this->mentor_model->addMentorsubtopicsIDs($data->MentorID, $results[0]->InsertedId, $val);
						}
						$mentorSubIdResults = $this->mentor_model->getMentorsubtopicsIDs($results[0]->InsertedId);  
							
						 
						foreach($mentorSubIdResults as $val){
								$ID	=	$val->MentorSubTopicsIDsId;
								array_push($SUBTOPICID,$ID);					 
						} 	
						//echo "<pre>";print_r($SUBTOPICID);
						$mentorSubIdResultsIds	= join(',', $SUBTOPICID);
						$addMentorSubId = $this->mentor_model->updateMentorsubtopicsIDs($mentorSubIdResultsIds,$results[0]->InsertedId);    
					}else{
						$response['submitMeetingInfo']['Error'] = 1;
						$response['submitMeetingInfo']['Message'] = 'No Data Found';
					}
			}
            
            /* added by khushbu 18-8-15 */					
					
					}
            	//$results = $this->mentor_model->submitMeetingInfoDetail($data->MeetingID, $data->MentorID, $data->MenteeID, $data->MeetingStartDatetime, $data->MeetingEndDatetime, $data->MeetingTypeID, $data->MeetingTopicID, $data->MeetingSubTopicID, $data->MeetingPlaceID, $data->MenteeActionIDs, $data->MeetingElapsedTime, $data->MeetingFeedback, @$data->MentorActionItemDone, @$data->MenteeActionItemDone);
				
					/*20-8 */
            
            
            /* added by khushbu 18-8-15*/
            
            
	    if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "") {
	    	$response['submitMeetingInfo']['Error'] = 0;
                $response['submitMeetingInfo']['Message'] = 'Meeting info added successfully';
                $response['submitMeetingInfo']['data'] = $results[0];
	    } else 
	    if (isset($results[0]->Message) && $results[0]->Message != ""){
	    	$response['submitMeetingInfo']['Error'] = 2;
            $response['submitMeetingInfo']['Message'] = $results[0]->Message;
	    } else {
	    	$response['submitMeetingInfo']['Error'] = 1;
            $response['submitMeetingInfo']['Message'] = 'Error has occurred. Please try again later.';
	    }
					
				
        }
        echo json_encode($response);
        exit();
    }

    function getTopic() {
        $response = array();
        $results = $this->topic_model->getTopicDetail();
       if (isset($results[0]->TopicID) && $results[0]->TopicID != "") {
            $response['getTopic']['Error'] = 0;
            $response['getTopic']['Message'] = "Topic details found.";
            $response['getTopic']['data'] = $results;
           
        } else {
            $response['getTopic']['Error'] = 2;
            $response['getTopic']['Message'] = "Topic details not found.";
        }
        echo json_encode($response);
        exit();
    }
    
    
    function getMentor($data) {
        $response = array();
		if ($data->MenteeID == '') {
            $response['getMentor']['Error'] = 2;
            $response['getMentor']['Message'] = 'Mentee not found';
        }else {
			$results = $this->mentor_model->getMentorDetail($data->MenteeID);
			if (isset($results[0]->MentorID) && $results[0]->MentorID != "") {
				$response['getMentor']['Error'] = 0;
				$response['getMentor']['Message'] = "Mentor details found.";
				$response['getMentor']['data'] = $results;
			} else {
				$response['getMentor']['Error'] = 2;
				$response['getMentor']['Message'] = "Mentor details not found.";
			}
		}
        
        echo json_encode($response);
        exit();
    }

    function getSubTopic() {
        $response = array();
        $results = $this->topic_model->getSubTopicDetail();
        if (isset($results[0]->TopicID) && $results[0]->TopicID != "") {
            $response['getSubTopic']['Error'] = 0;
            $response['getSubTopic']['Message'] = "Sub Topic details found.";
            $response['getSubTopic']['data'] = $results;
        } else {
            $response['getSubTopic']['Error'] = 2;
            $response['getSubTopic']['Message'] = "Sub Topic details not found.";
        }
        echo json_encode($response);
        exit();
    }

    function getMentee($data) {
        $response = array();
		
		if ($data->MentorID == '') {
            $response['getMentee']['Error'] = 2;
            $response['getMentee']['Message'] = 'Mentor not found';
        }else {
			$results = $this->mentee_model->getMenteeDetail($data->MentorID);
			if (isset($results[0]->MenteeID) && $results[0]->MenteeID != "") {
				$response['getMentee']['Error'] = 0;
				$response['getMentee']['Message'] = "Mentee details found.";
				$response['getMentee']['data'] = $results;
			} else {
				$response['getMentee']['Error'] = 2;
				$response['getMentee']['Message'] = "Mentee details not found.";
			}
		}
        
        echo json_encode($response);
        exit();
    }

    function getMeetingType() {
        $response = array();
        $results = $this->meetingtype_model->getMeetingTypeDetail();
        if (isset($results[0]->MeetingTypeID) && $results[0]->MeetingTypeID != "") {
            $response['getMeetingType']['Error'] = 0;
            $response['getMeetingType']['Message'] = "Meeting type details found.";
            $response['getMeetingType']['data'] = $results;
        } else {
            $response['getMeetingType']['Error'] = 2;
            $response['getMeetingType']['Message'] = "Meeting type details not found.";
        }
        echo json_encode($response);
        exit();
    }

    function getSkills() {
        $response = array();
        $results = $this->skills_model->getSkillsDetail();
        if (isset($results[0]->SkillID) && $results[0]->SkillID != "") {
            $response['getSkills']['Error'] = 0;
            $response['getSkills']['Message'] = "skill details found.";
            $response['getSkills']['data'] = $results;
        } else {
            $response['getSkills']['Error'] = 2;
            $response['getSkills']['Message'] = "skill details not found.";
        }
        echo json_encode($response);
        exit();
    }

    function getMentorActionTaken() {
        $response = array();
        $results = $this->mentor_model->getMentorActionTakenDetail();
        if (isset($results[0]->MentorActionID) && $results[0]->MentorActionID != "") {
            $response['getMentorActionTaken']['Error'] = 0;
            $response['getMentorActionTaken']['Message'] = "Mentor actiontaken details found.";
            $response['getMentorActionTaken']['data'] = $results;
        } else {
            $response['getMentorActionTaken']['Error'] = 2;
            $response['getMentorActionTaken']['Message'] = "Mentor actiontaken details not found.";
        }
        echo json_encode($response);
        exit();
    }

    function getMenteeActionTaken() {
        $response = array();
        $results = $this->mentee_model->getMenteeActionTakenDetail();
        if (isset($results[0]->MenteeActionID) && $results[0]->MenteeActionID != "") {
            $response['getMenteeActionTaken']['Error'] = 0;
            $response['getMenteeActionTaken']['Message'] = "Mentee actiontaken details found.";
            $response['getMenteeActionTaken']['data'] = $results;
        } else {
            $response['getMenteeActionTaken']['Error'] = 2;
            $response['getMenteeActionTaken']['Message'] = "Mentee actiontaken details not found.";
        }
        echo json_encode($response);
        exit();
    }

    function checkLogin($data) {
        $response = array();
        /*if ($data->Email == '') {
            $response['checkLogin']['Error'] = 2;
            $response['checkLogin']['Message'] = 'Email not found';
        } */
	if ($data->UserName == '') {
            $response['checkLogin']['Error'] = 2;
            $response['checkLogin']['Message'] = 'User Name not found';
        } else if ($data->Password == '') {
            $response['checkLogin']['Error'] = 2;
            $response['checkLogin']['Message'] = 'Password not found';
        } else {

            $results = $this->mentor_model->checkLoginDetail($data->UserName, $data->Password);
            if (isset($results[0]->Id) && $results[0]->Id != "") {
                $response['checkLogin']['Error'] = 0;
                $response['checkLogin']['Message'] = 'Login successfully';
                $response['checkLogin']['data'] = $results[0];
	    } else if (isset($results[0]->Message) && $results[0]->Message != ""){
	    	$response['checkLogin']['Error'] = 2;
                $response['checkLogin']['Message'] = $results[0]->Message;
	    } else {
	    	$response['checkLogin']['Error'] = 1;
                $response['checkLogin']['Message'] = 'Error has occurred. Please try again later.';
	    }
        }
        echo json_encode($response);
        exit();
    }

    function registerUser($data) {
        $response = array();
        if ($data->Email == '') {
            $response['registerUser']['Error'] = 2;
            $response['registerUser']['Message'] = 'Email not found';
        } else if ($data->AccessToken == '') {
            $response['registerUser']['Error'] = 2;
            $response['registerUser']['Message'] = 'Access Token not found';
        } else if ($data->UserType == '') {
            $response['registerUser']['Error'] = 2;
            $response['registerUser']['Message'] = 'User Type not found';
        } else {

            $results = $this->mentor_model->registerUserDetail($data->Email, $data->AccessToken, $data->UserType);

	    if (isset($results[0]->Id) && $results[0]->Id != "") {
	    	$response['registerUser']['Error'] = 0;
                $response['registerUser']['Message'] = 'Registered successfully';
                $response['registerUser']['data'] = $results[0];
                // SEND EMAIL TO CUSTOMER
                
                    $emailtemplate = $this->emailtemplate_model->get_emailtempate_details(2);
                    if ($emailtemplate) {
                        $emailData = array();
                        if($data->UserType == "0") { # FOR MENTOR
	                        $emailData['toEmail'] = $results[0]->MentorEmail;
				
	                        $arrSearch = array('##FIRST_NAME##', '##EMAIL_ADDRESS##', '##PASSWORD##','##USER_TYPE##');
	                        $arrReplace = array($results[0]->MentorName, $results[0]->MentorEmail, $results[0]->AccessToken, 'Mentor');
                        } else if ($data->UserType == "1"){ # FOR MENTEE
	                        $emailData['toEmail'] = $results[0]->MenteeEmail;
	
	                        $arrSearch = array('##FIRST_NAME##', '##EMAIL_ADDRESS##', '##PASSWORD##','##USER_TYPE##');
	                        $arrReplace = array($results[0]->MenteeName, $results[0]->MenteeEmail, $results[0]->AccessToken, 'Mentee');                        
                        }

                        $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                        $emailData['subject'] = $subject;

                        $body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                        $emailData['body'] = $body;

                        send_mail($emailData);
                    }
               
                // END SEND EMAIL TO CUSTOMER
	    } else if (isset($results[0]->Message) && $results[0]->Message != ""){
	    	$response['registerUser']['Error'] = 2;
                $response['registerUser']['Message'] = $results[0]->Message;
	    } else {
	    	$response['registerUser']['Error'] = 1;
                $response['registerUser']['Message'] = 'Error has occurred. Please try again later.';
	    }
        }
        echo json_encode($response);
        exit();
    }

    function forgotPassword($data) {
        $response = array();
        if ($data->Email == '') {
            $response['forgotPassword']['Error'] = 2;
            $response['forgotPassword']['Message'] = 'Email not found';
        } else {
            $results = $this->mentor_model->forgotPasswordDetail($data->Email);
            If (isset($results[0]->Id) && $results[0]->Id > 0) {
                // SEND EMAIL TO CUSTOMER 
                    $emailtemplate = $this->emailtemplate_model->get_emailtempate_details(3);
                    if ($emailtemplate) {
                        $emailData = array();
                        if ($results[0]->UserTypeVal == '0'){
                           $emailData['toEmail'] = $results[0]->MentorEmail;
                           $arrSearch = array('##FIRST_NAME##', "##EMAIL##", "##PASSWORD##");
                           $arrReplace = array(ucwords($results[0]->MentorName), $results[0]->MentorName, $results[0]->Password);
                        } else if ($results[0]->UserTypeVal == '1') {
                           $emailData['toEmail'] = $results[0]->MenteeEmail;
                           $arrSearch = array('##FIRST_NAME##', "##EMAIL##", "##PASSWORD##");
                           $arrReplace = array(ucwords($results[0]->MenteeName), $results[0]->MenteeName, $results[0]->Password);
                        }

                        $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                        $emailData['subject'] = $subject;

                        $body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                        $emailData['body'] = $body;


                        send_mail($emailData);
                    }
                // END SEND EMAIL TO CUSTOMER 
                $response['forgotPassword']['Error'] = 0;
                $response['forgotPassword']['Message'] = 'Password has reset and sent to registered email address.';
            } else if (isset($results[0]->Message) && $results[0]->Message != "") {
                $response['forgotPassword']['Error'] = 2;
                $response['forgotPassword']['Message'] = $results[0]->Message;
            } else {
                $response['forgotPassword']['Error'] = 1;
                $response['forgotPassword']['Message'] = 'Error has occurred. Please try again later.';
            }
        }
        echo json_encode($response);
        exit();
    }

    function changePassword($data) {
        $response = array();
        if ($data->Email == '') {
            $response['changePassword']['Error'] = 2;
            $response['changePassword']['Message'] = 'Email not found';
        } else if ($data->NewPassword == '') {
            $response['changePassword']['Error'] = 2;
            $response['changePassword']['Message'] = 'New Password not found';
        } else if ($data->OldPassword == '') {
            $response['changePassword']['Error'] = 2;
            $response['changePassword']['Message'] = 'Old Password not found';
        } else if ($data->UserType == '') {
            $response['changePassword']['Error'] = 2;
            $response['changePassword']['Message'] = 'User Type not found';
        } else {
            $results = $this->mentor_model->changePasswordDetail($data->Email, fnEncrypt($data->OldPassword, $this->config->item('mentorKey')), fnEncrypt($data->NewPassword, $this->config->item('mentorKey')), $data->UserType);
            if ((isset($results[0]->MentorContactInfoID) && $results[0]->MentorContactInfoID > 0) || (isset($results[0]->MenteeContactID) && $results[0]->MenteeContactID > 0)) {

                // SEND EMAIL
                    $emailtemplate = $this->emailtemplate_model->get_emailtempate_details(3);
                    if ($emailtemplate) {
                        $emailData = array();
                        if ($data->UserType == '0'){
                           $emailData['toEmail'] = $results[0]->MentorEmail;

                           $arrSearch = array('##FIRST_NAME##', "##EMAIL##", "##PASSWORD##");
                           $arrReplace = array(ucwords($results[0]->MentorName), $results[0]->MentorEmail, fnDecrypt($results[0]->Password, $this->config->item('mentorKey')));
                        } else if ($data->UserType == '1') {
                           $emailData['toEmail'] = $results[0]->MenteeEmail;

                           $arrSearch = array('##FIRST_NAME##', "##EMAIL##", "##PASSWORD##");
                           $arrReplace = array(ucwords($results[0]->MenteeName), $results[0]->MenteeEmail, fnDecrypt($results[0]->Password, $this->config->item('mentorKey')));
                        }
                        $subject = str_replace($arrSearch, $arrReplace, $emailtemplate['EmailTemplateTitle']);
                        $emailData['subject'] = $subject;

                        $body = str_replace($arrSearch, $arrReplace, $emailtemplate['Content']);
                        $emailData['body'] = $body;


                        send_mail($emailData);
                    }
                // END SEND EMAIL

                $response['changePassword']['Error'] = 0;
                $response['changePassword']['Message'] = 'Password changed successfully.';
            } else {
                $response['changePassword']['Error'] = 1;
                $response['changePassword']['Message'] = 'Error has occurred. Please try again later.';
            }
        }
        echo json_encode($response);
        exit();
    }

    function upload($application_code) {
        /* $file_path = "/var/www/html/medialytix/uploads/abtest/ab_test_temp/";

          $file_path = $file_path . basename( $_FILES['uploaded_file']['name']);
          if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $file_path))
          {
          echo "success";
          }
          else
          {
          echo "fail to upload.. no files provided";
          } */
//$absolute_path.'/uploads/abtest/default_ab_test_parameters/'.$data->vApplicationCode.'/'. $experiment_parameter['iABTestParameterValue']

        move_uploaded_file($_FILES["file"]["tmp_name"], "/var/www/html/medialytix/uploads/abtest/default_ab_test_parameters/" . $application_code . '/' . $_FILES["file"]["name"]);
        exit;
        if ((($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/pjpeg") || ($_FILES["file"]["type"] == "image/png")) && ($_FILES["file"]["size"] < 20000000000)) {
            if ($_FILES["file"]["error"] > 0) {
                echo "Return Code: " . $_FILES["file"]["error"] . "
     ";
            } else {
                echo "Upload: " . $_FILES["file"]["name"] . "
     ";
                echo "Type: " . $_FILES["file"]["type"] . "
     ";
                echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb
     ";
                echo "Temp file: " . $_FILES["file"]["tmp_name"] . "
     ";

                if (file_exists("/var/www/html/medialytix/uploads/abtest/ab_test_temp/" . $_FILES["file"]["name"])) {
                    echo $_FILES["file"]["name"] . " already exists. ";
                } else {
                    move_uploaded_file($_FILES["file"]["tmp_name"], "/var/www/html/medialytix/uploads/abtest/ab_test_temp/" . $_FILES["file"]["name"]);
                    echo "Stored in: " . "/var/www/html/medialytix/uploads/abtest/ab_test_temp/" . $_FILES["file"]["name"];
                }
            }
        } else {
            echo "Invalid file";
        }

        exit;
    }

    function checkvalidjson($json) {
        $obj = json_decode(stripslashes($json), TRUE);

        if (is_null($obj)) {
            $response['error'] = 1;
            $response['data'] = "Invalid Json format";
            echo json_encode($response);
            exit();
        } else {
            $data = json_decode($json);
            return $data;
        }
    }

    function getCountry() {

        $country_list = $this->users_model->getCountry();
        $response['getCountry']['Error'] = 0;
        $response['getCountry']['data'] = $country_list;
        echo json_encode($response);
        exit();
    }

    function getStates() {

        $state_list = $this->users_model->getStates();
        $response['getStates']['Error'] = 0;
        $response['getStates']['data'] = $state_list;
        echo json_encode($response);
        exit();
    }

    /*
     *   @params : UserID 
     *   @params : UserType  0 = customer, 1 = vendor, 2 = employee  
     *   @params : PictureType  0 = profilepic, 1 = gallery
     *
     */

    function addUploadPics($data) {
        $response = array();
        if (isset($data['UserType']) && $data['UserType'] != "") {
            switch ($data['UserType']) {
                case '0':  // customer
                    if ($data['PictureType'] == 0) {  // 
                        if ($_FILES['ImageData']['error'] == 0) {
                            $pathMain = CUSTOMER_LOGO_PATH;
                            $pathThumb = CUSTOMER_LOGO_THUMB_PATH;
                            $imageNameTime = time();
                            $filename = $imageNameTime . "_" . $data['UserID'];
                            $result = do_upload("ImageData", $pathMain, $filename);

                            if ($result['status'] == 1) {

                                // RESIZE ORIGINAL IMAGE TO THUMB 150X150
                                $uploadedFileName = $result['upload_data']['file_name'];
                                resize_image($pathMain . $uploadedFileName, $pathThumb . $uploadedFileName, CUSTOMER_LOGO_THUMB_WIDTH, CUSTOMER_LOGO_THUMB_HEIGHT);

                                $this->db->where('iCustomerId', $data['UserID']);
                                $result = $this->db->update('customerdetails', array('iPictureURLId' => $uploadedFileName));

                                // RESIZE ORIGINAL IMAGE TO 500X500
                                resize_image($pathMain . $uploadedFileName, $pathMain . $uploadedFileName, CUSTOMER_LOGO_WIDTH, CUSTOMER_LOGO_HEIGHT);

                                $results = $this->customer_model->getCustomerDetail($data['UserID']);

                                $response['addUploadPics']['Error'] = 0;
                                $response['addUploadPics']['Message'] = 'Profile Pic Uploaded Successfully.';
                                $response['addUploadPics']['data'] = $results[0];
                            } else {
                                $response['addUploadPics']['Error'] = 1;
                                $response['addUploadPics']['Message'] = 'Something is wrong.';
                            }
                        } else {
                            $response['addUploadPics']['Error'] = 1;
                            $response['addUploadPics']['Message'] = 'Something is wrong.';
                        }
                    } elseif ($data->PictureType == 1) {

                        /*  if($_FILES['vLogoURL']['error'] == 0 ){
                          $pathMain  = CUSTOMER_LOGO_PATH;
                          $pathThumb = CUSTOMER_LOGO_THUMB_PATH;
                          $imageNameTime = time();
                          $filename = $imageNameTime."_".$id;
                          $result = do_upload("vLogoURL", $pathMain, $filename);

                          if ($result['status'] == 1) {

                          // RESIZE ORIGINAL IMAGE TO THUMB 150X150
                          $uploadedFileName = $result['upload_data']['file_name'];
                          resize_image($pathMain . $uploadedFileName, $pathThumb . $uploadedFileName, CUSTOMER_LOGO_THUMB_WIDTH, CUSTOMER_LOGO_THUMB_HEIGHT);
                          $this->db->where('iVendorId', $id);
                          $result = $this->db->update('vendordetails', array('vLogoURL' => $uploadedFileName ));

                          // RESIZE ORIGINAL IMAGE TO 500X500
                          resize_image($pathMain . $uploadedFileName, $pathMain . $uploadedFileName, CUSTOMER_LOGO_WIDTH, CUSTOMER_LOGO_HEIGHT);
                          }
                          } */
                    }

                    break;
                case '1':   // vendor 

                    /* $id = $data->UserID;  
                      if($_FILES['vLogoURL']['error'] == 0 ){
                      $pathMain  = VENDOR_LOGO_PATH;
                      $pathThumb = VENDOR_LOGO_THUMB_PATH;
                      $imageNameTime = time();
                      $filename = $imageNameTime."_".$id;
                      $result = do_upload("vLogoURL", $pathMain, $filename);

                      if ($result['status'] == 1) {

                      // RESIZE ORIGINAL IMAGE TO THUMB 150X150
                      $uploadedFileName = $result['upload_data']['file_name'];
                      resize_image($pathMain . $uploadedFileName, $pathThumb . $uploadedFileName, VENDOR_LOGO_THUMB_WIDTH, VENDOR_LOGO_THUMB_HEIGHT);
                      $this->db->where('iVendorId', $id);
                      $result = $this->db->update('vendordetails', array('vLogoURL' => $uploadedFileName ));

                      // RESIZE ORIGINAL IMAGE TO 500X500
                      resize_image($pathMain . $uploadedFileName, $pathMain . $uploadedFileName, VENDOR_LOGO_WIDTH, VENDOR_LOGO_HEIGHT);
                      }
                      } */
                    break;
                case '2':   // employee 
                    break;
                default:
                    break;
            }
        } else {
            $response['addUploadPics']['Error'] = 1;
            $response['addUploadPics']['Message'] = 'Something is wrong.';
        }
        echo json_encode($response);
        exit();
    }
    
    /* khushbu 19-8-15 */
    function getMentorForInvitation($data) {
        $response = array();
        if ($data->MenteeID == '') {
            $response['getMentorForInvitation']['Error'] = 2;
            $response['getMentorForInvitation']['Message'] = 'Mentee not found';
        } else {
				$resArray	=	array();
            $results = $this->mentor_model->getMentorForInvitation();
				//print_r($results);exit;
           if (isset($results) && (!empty($results))) {
           			foreach($results as $val){
							   $res	=	 $this->mentor_model->getMentorForInvitationStatus($val->MentorID,$data->MenteeID);
							   //print_r($res[0]);
							   array_push($resArray,$res[0]);         			
           			}
           		
	    				$response['getMentorForInvitation']['Error'] 	= 0;
                	$response['getMentorForInvitation']['Message']  = 'Mantor details';
                	$response['getMentorForInvitation']['data'] 		= $resArray;
	    		}  else {
	    				$response['getMentorForInvitation']['Error'] = 1;
               	$response['getMentorForInvitation']['Message'] = 'Error has occurred. Please try again later.';
	    		}
        } 
        echo json_encode($response);
        exit();
    }
    
    function startEndMeetingSession($data) {
        $response = array();
        if ($data->MenteeID == '') {
            $response['startEndMeetingSession']['Error'] = 2;
            $response['startEndMeetingSession']['Message'] = 'Mentee not found';
        } else if ($data->MentorID == '') {
            $response['startEndMeetingSession']['Error'] = 2;
            $response['startEndMeetingSession']['Message'] = 'Mentor not found';
        } else if ($data->DateTime == '') {
            $response['startEndMeetingSession']['Error'] = 2;
            $response['startEndMeetingSession']['Message'] = 'DateTime not found';
        } else if ($data->SessionStatus == '') {
            $response['startEndMeetingSession']['Error'] = 2;
            $response['startEndMeetingSession']['Message'] = 'SessionStatus not found';
        }  else {
				$resArray	=	array();
           $results = $this->mentor_model->getMeetingSessionID($data->MenteeID,$data->MentorID);
          	// print_r($results);exit;
           if (empty($results)) {
           			//	echo "new";exit;
           		//if (isset($results[0]->InsertedId) && $results[0]->InsertedId != "") {
						$res	=	 $this->mentor_model->addStartMeetingSession($data->MenteeID,$data->MentorID,$data->DateTime,$data->SessionStatus);          		
           	   	$response['startEndMeetingSession']['Error'] 	= 0;
               	$response['startEndMeetingSession']['Message']  = 'Mantor details';
              	 	$response['startEndMeetingSession']['data'] 		= $results;
           }else {
           	
           			if($results[0]->Meeting_status==1 && $data->SessionStatus == 1){
           				//	echo "1";exit;
	    					$res	=	 $this->mentor_model->updateStartMeetingSession($data->DateTime,$data->SessionStatus,$data->MentorID,$data->MenteeID);          		
           	   		$response['startEndMeetingSession']['Error'] 	= 0;
               		$response['startEndMeetingSession']['Message']  = 'Mantor details';
              	 		$response['startEndMeetingSession']['data'] 		= $results;
              	 	}else if($results[0]->Meeting_status==1 && $data->SessionStatus == 2){
              	 		//echo "sdf".$data->SessionStatus;exit;
							$res	=	 $this->mentor_model->updateEndMeetingSession($data->DateTime,$data->SessionStatus,$data->MentorID,$data->MenteeID);          		
           	   		$response['startEndMeetingSession']['Error'] 	= 0;
               		$response['startEndMeetingSession']['Message']  = 'Mantor details';
              	 		$response['startEndMeetingSession']['data'] 		= $results;              	 	
              	 	}else if($results[0]->Meeting_status==2 && $data->SessionStatus == 1){
              	 		$res	=	 $this->mentor_model->updateStartMeetingSession($data->DateTime,$data->SessionStatus,$data->MentorID,$data->MenteeID);          		
           	   		$response['startEndMeetingSession']['Error'] 	= 0;
               		$response['startEndMeetingSession']['Message']  = 'Mantor details';
              	 		$response['startEndMeetingSession']['data'] 		= $results;
              	 	}else{
							 $res	=	 $this->mentor_model->updateEndMeetingSession($data->DateTime,$data->SessionStatus,$data->MentorID,$data->MenteeID);          		
           	   		$response['startEndMeetingSession']['Error'] 	= 0;
               		$response['startEndMeetingSession']['Message']  = 'Mantor details';
              	 		$response['startEndMeetingSession']['data'] 		= $results;              	 	
              	 	}
              	 	
              	 	
              	 	/* 21-8-15 */
	    			if($results[0]->Meeting_status==3){
	    				echo "no ";exit;
						$res	=	 $this->mentor_model->addStartMeetingSession($data->MenteeID,$data->MentorID,$data->DateTime,$data->SessionStatus);          		
           	  		$response['startEndMeetingSession']['Error'] 	= 0;
               	$response['startEndMeetingSession']['Message']  = 'Mantor details';
              		$response['startEndMeetingSession']['data'] 		= $results;	    		
	    			}
	    		}  
	    		
	    		
	    		
	    		
        } 
        echo json_encode($response);
        exit();
    }
    
    
     /* khushbu 19-8-15 */
     /*khushbu 21-8-15*/
     function getMentorWaitScreen($data) {
     		 $response = array();
        if ($data->MenteeID == '') {
            $response['getMentorWaitScreen']['Error'] = 2;
            $response['getMentorWaitScreen']['Message'] = 'Mentee not found';
        } else {
				$resArray	=	array();
            $results = $this->mentor_model->getMentorWaitScreen($data->MenteeID);
				//print_r($results);exit;
           if (isset($results) && (!empty($results))) {
           			foreach($results as $val){
							   $res	=	 $this->mentor_model->getMentorWaitScreenDetails($val->MentorID);
							  // print_r($res);
							   array_push($resArray,$res);         			
           			}
           		
	    				$response['getMentorWaitScreen']['Error'] 	= 0;
                	$response['getMentorWaitScreen']['Message']  = 'Mantor details';
                	$response['getMentorWaitScreen']['data'] 		= $resArray;
	    		}  else {
	    				$response['getMentorWaitScreen']['Error'] = 1;
               	$response['getMentorWaitScreen']['Message'] = 'Error has been occurred please try again later.';
	    		}
        } 
        echo json_encode($response);
        exit();
     }
     
     
     function getMettingWaitScreenInfo($data) {
     	  $response = array();
        if ($data->MenteeID == '') {
            $response['getMettingWaitScreenInfo']['Error'] = 2;
            $response['getMettingWaitScreenInfo']['Message'] = 'Mentee not found';
        } else if ($data->MentorID == '') {
            $response['getMettingWaitScreenInfo']['Error'] = 2;
            $response['getMettingWaitScreenInfo']['Message'] = 'Mentor not found';
        } else {
				$resArray	=	array();
            $results = $this->mentor_model->getMettingWaitScreenInfo($data->MentorID,$data->MenteeID);
            if(!empty($results)){
					$response['getMettingWaitScreenInfo']['Error'] 	= 0;
           	 	$response['getMettingWaitScreenInfo']['Message']  = 'Mantor details';
            	$response['getMettingWaitScreenInfo']['data'] 		= $results;
	    		}else {
	    				$response['getMettingWaitScreenInfo']['Error'] = 1;
               	$response['getMettingWaitScreenInfo']['Message'] = 'Meeting has not started yet!';
	    		}  
	     }
        echo json_encode($response);
        exit();
     }

}
