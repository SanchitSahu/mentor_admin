DELIMITER $$

USE `rajat_sp`$$

DROP PROCEDURE IF EXISTS `usp_CheckLogin`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `usp_CheckLogin`(IN `Username` VARCHAR(255), IN `AccessToken` VARCHAR(255))
BEGIN
	DECLARE ResultCount INT DEFAULT 0;
	DECLARE Message VARCHAR(255);
	DECLARE UserTypeFlag INT DEFAULT 0;
	DECLARE UserTypeVal INT DEFAULT 0;
	SET UserTypeFlag = (SELECT COUNT(mci.MentorID) FROM mentor_mentor mci WHERE mci.MentorName = Username);
	IF (UserTypeFlag > 0) THEN
	        SET UserTypeVal = 0;
	END IF;
	IF (UserTypeFlag = 0) THEN
		SET UserTypeFlag = (SELECT COUNT(mc.MenteeID) FROM mentor_mentee mc WHERE mc.MenteeName = Username);
		IF (UserTypeFlag > 0) THEN
			SET UserTypeVal = 1;
		END IF;
	END IF;
	IF (UserTypeFlag > 0) THEN
		IF (UserTypeVal = 0) THEN # FOR MENTOR
			SET ResultCount = (SELECT COUNT(mci.MentorContactInfoID) FROM mentor_mentorcontactinfo mci ,mentor_mentor mm WHERE mm.MentorName = Username AND mci.Password = AccessToken AND mci.MentorStatus = 'Active');
			IF (ResultCount > 0) THEN
				SELECT m.MentorID AS Id, m.MentorName, mci.MentorPhone, mci.MentorEmail, mci.MentorStatus, UserTypeVal AS UserType,
				IFNULL(FN_GetSkills(m.MentorID,'0'),"") AS Skills, PASSWORD
				FROM mentor_mentor m 
				LEFT JOIN mentor_mentorcontactinfo mci ON mci.MentorID = m.MentorID
				WHERE m.MentorName = Username AND mci.Password = AccessToken AND mci.MentorStatus = 'Active';
			ELSE
				SET Message='Username and/or password do not match. Please try again.';
				SELECT Message;
			END IF;
		ELSEIF (UserTypeVal = 1) THEN # FOR MENTEE
			SET ResultCount = (SELECT COUNT(mc.MenteeContactID) FROM mentor_menteecontact mc
            LEFT JOIN mentor_mentee mm1 ON mm1.MenteeName = Username AND mc.Password = AccessToken AND mc.MenteeStatus = 'Active');
			IF (ResultCount > 0) THEN
				SELECT m.MenteeID AS Id, m.MenteeName, mc.MenteePhone, mc.MenteeEmail, mc.MenteeStatus, UserTypeVal AS UserType,
				IFNULL(FN_GetSkills(m.MenteeID,'1'),"") AS Skills, PASSWORD
				FROM mentor_mentee m
				LEFT JOIN mentor_menteecontact mc ON mc.MenteeId = m.MenteeID
				WHERE m.MenteeName = Username AND mc.Password = AccessToken AND mc.MenteeStatus = 'Active';
			ELSE
				SET Message='Username and/or password do not match. Please try again.';
				SELECT Message;
			END IF;
		END IF;
	ELSE
		SET Message='Username and/or password do not match. Please try again.';
		SELECT Message;
	END IF;
END$$

DELIMITER ;